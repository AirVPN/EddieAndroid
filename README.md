# AirVPN Eddie 3.2.1 for Android

* The only Android application officially developed by AirVPN

* Full WireGuard support

* Full OpenVPN CHACHA20-POLY1305, AES-GCM-128 and AES-GCM-256 support

* Free and open source application based on WireGuard version 2e0774f
  and OpenVPN3-AirVPN version 3.11

* Exclusive, best effort VPN lock system to prevent traffic leak in case
  of network error and/or compromised connection

* Battery-conscious application

* Low RAM footprint

* Settings for comfortable user interface in system, night and day modes

* Ergonomic and friendly interface

* Full compatibility with Android 5.1 and up (phone, tablet and TV)

* Full integration with AirVPN and ability to import profiles to connect
  to other VPN services

* Full Android TV compatibility including D-Pad support. Mouse emulation
  is not required.

* Ability to start and connect the application at device boot

* Option to define which apps must have traffic inside or outside the VPN
  tunnel through white and black list

* Enhanced security thanks to locally stored encrypted data through optional
  master password

* Quick one-tap connection and smart, fully automated server selection

* Smart server selection with custom settings

* Manual server selection

* Smart attempts to bypass OpenVPN blocks featuring protocol and server
  fail-over

* Enhancements aimed to increase accessibility and comfort to visually
  impaired persons

* AirVPN servers sorting options

* Customizable "Favorite", "Forbidden" and "Default" servers

* WireGuard and OpenVPN mimetype support to import profiles from external
  applications

* VPN profiles can be exported both to internal database and external file or
  app

* Localization in simplified and traditional Chinese, Danish, Dutch, English,
  French, German, Italian, Portuguese, Russian, Spanish and Turkish.

* GPS Spoofing

With AirVPN you can keep your Internet traffic hidden from the eyes of your ISP
and from any malicious entity wiretapping your line, connect safely even via a
public Internet hotspot, unblock geo-restricted websites, bypass web sites
blocks and protect the integrity of your communications.

Thanks to AirVPN, Eddie protects your Android device traffic. Eddie can be used
even with any other VPN service based on Wireguard or OpenVPN.
