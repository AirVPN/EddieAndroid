// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 3 October 2018 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseExpandableListAdapter;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Document;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class ConnectAirVPNServerFragment extends Fragment implements NetworkStatusListener, EddieEventListener
{
    private final int ACTIVITY_RESULT_SERVER_SETTINGS = 1002;
    private final int FRAGMENT_ID = 5002;

    public enum GroupType
    {
        HEADER,
        GROUP,
        COUNTRY
    }

    public enum ItemType
    {
        SERVER,
        COUNTRY
    };

    public enum ConnectionMode
    {
        SERVER,
        COUNTRY,
        CONTINENT
    };

    public enum ExportMode
    {
        DATABASE,
        FILE
    };

    private AirVPNUser airVPNUser = null;
    private AirVPNManifest airVPNManifest = null;
    private EddieEvent eddieEvent = null;
    private static VPNManager vpnManager = null;
    private SupportTools supportTools = null;
    private NetworkStatusReceiver networkStatusReceiver = null;
    private SettingsManager settingsManager = null;
    private CountryContinent countryContinent = null;
    private VPNProfileDatabase vpnProfileDatabase = null;

    private Button btnInfo = null;
    private Button btnSettings = null;
    private Button btnSearch = null;
    private Button btnReloadManifest = null;
    private Button btnOk = null;
    private Button btnCancel = null;
    private Button btnReset = null;

    private LinearLayout llVpnType = null, llFilterSettings = null;
    private ImageView imgVpnType = null, imgVpnCycle = null;
    private TextView txtFilterSettings = null;

    AlertDialog.Builder dialogBuilder = null;
    AlertDialog searchDialog = null;
    private EditText edtKey = null;

    private ExpandableListView elvAirVPNServer = null;
    private AirVPNServerExpandableListAdapter serverListAdapter = null;

    private ArrayList<Group> groupList = null;
    private ArrayList<String> countryWhitelist = null;
    private ArrayList<String> countryBlacklist = null;
    private String defaultCountry = "";
    private boolean connectionInProgress = false;

    private HashMap<String, String> profileInfo = null;

    private ListItem pendingServerConnection = null;
    private ListItem referenceServerItem = null;
    ListItem listItem = null;

    private static String searchKey = "";

    public void setVpnManager(VPNManager v)
    {
        vpnManager = v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        supportTools = EddieApplication.supportTools();
        settingsManager = EddieApplication.settingsManager();
        countryContinent = EddieApplication.countryContinent();

        vpnProfileDatabase = new VPNProfileDatabase(getActivity());

        networkStatusReceiver = EddieApplication.networkStatusReceiver();

        networkStatusReceiver.subscribeListener(this);

        eddieEvent = EddieApplication.eddieEvent();

        eddieEvent.subscribeListener(this);

        airVPNUser = EddieApplication.airVPNUser();

        airVPNManifest = EddieApplication.airVPNManifest();

        connectionInProgress = false;
    }

    @Override
    public void onResume()
    {
        super.onResume();

        refreshFragment();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        if(networkStatusReceiver != null)
            networkStatusReceiver.unsubscribeListener(this);

        if(eddieEvent != null)
            eddieEvent.unsubscribeListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View fragmentView = inflater.inflate(R.layout.fragment_airvpn_server_list_layout, container, false);

        llVpnType = (LinearLayout)fragmentView.findViewById(R.id.airvpn_vpn_type);
        imgVpnType = (ImageView)fragmentView.findViewById(R.id.img_vpn_type);
        imgVpnCycle = (ImageView)fragmentView.findViewById(R.id.img_vpn_cycle);

        llFilterSettings = (LinearLayout)fragmentView.findViewById(R.id.filter_settings);
        txtFilterSettings = (TextView)fragmentView.findViewById(R.id.txt_filter_settings);

        elvAirVPNServer = (ExpandableListView)fragmentView.findViewById(R.id.airvpn_server_list);

        btnInfo = (Button)fragmentView.findViewById(R.id.airvpn_server_info);
        btnSettings = (Button)fragmentView.findViewById(R.id.airvpn_server_settings);
        btnSearch = (Button)fragmentView.findViewById(R.id.airvpn_server_search);
        btnReloadManifest = (Button)fragmentView.findViewById(R.id.airvpn_server_reload_manifest);

        elvAirVPNServer.setOnChildClickListener(new ExpandableListView.OnChildClickListener()
        {
            @Override
            public boolean onChildClick(ExpandableListView expandablelistview, View clickedView, int groupPosition, int childPosition, long childId)
            {
                ListItem server = groupList.get(groupPosition).getItemList().get(childPosition);

                if(server.getType() == ItemType.SERVER)
                    startServerConnection(server);

                return true;
            }
        });

        elvAirVPNServer.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos, long id)
            {
                if(airVPNUser.isUserValid())
                    return false;
                else
                {
                    supportTools.infoDialog(R.string.airvpn_longpress_no_login, true);

                    return true;
                }
            }
        });

        createGroupList();

        serverListAdapter = new AirVPNServerExpandableListAdapter(getContext(), groupList);

        elvAirVPNServer.setAdapter(serverListAdapter);

        elvAirVPNServer.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener()
        {
            @Override
            public void onGroupExpand(int groupPosition)
            {
                if(groupPosition == 0)
                {
                    if(groupList.get(groupPosition).getItemList().size() == 0)
                        supportTools.infoDialog(R.string.airvpn_default_add_to_group, true);
                }
                else if(groupPosition == 1 || groupPosition == 2)
                {
                    if(groupList.get(groupPosition).getItemList().size() == 0)
                        supportTools.infoDialog(R.string.airvpn_server_add_to_group, true);
                }
            }
        });

        if(elvAirVPNServer != null)
        {
            if(airVPNUser.isUserValid())
                registerForContextMenu(elvAirVPNServer);
            else
                unregisterForContextMenu(elvAirVPNServer);
        }

        btnInfo.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                showStatus();
            }
        });

        btnInfo.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                if(!btnInfo.isEnabled())
                    return;

                if(hasFocus)
                    btnInfo.setBackgroundResource(R.drawable.info_focus);
                else
                    btnInfo.setBackgroundResource(R.drawable.info);
            }
        });

        btnInfo.setAccessibilityDelegate(new View.AccessibilityDelegate()
        {
            @Override
            public void sendAccessibilityEvent(View host, int eventType)
            {
                super.sendAccessibilityEvent(host, eventType);

                btnInfo.setContentDescription(getString(R.string.accessibility_connection_info));
            }
        });

        btnSettings.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                Intent settingsActivityIntent = new Intent(getContext(), AirVPNServerSettingsActivity.class);

                startActivityForResult(settingsActivityIntent, ACTIVITY_RESULT_SERVER_SETTINGS);
            }
        });

        btnSettings.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                if(!btnSettings.isEnabled())
                    return;

                if(hasFocus)
                    btnSettings.setBackgroundResource(R.drawable.settings_focus);
                else
                    btnSettings.setBackgroundResource(R.drawable.settings);
            }
        });

        btnSettings.setAccessibilityDelegate(new View.AccessibilityDelegate()
        {
            @Override
            public void sendAccessibilityEvent(View host, int eventType)
            {
                super.sendAccessibilityEvent(host, eventType);

                btnSettings.setContentDescription(getString(R.string.accessibility_server_settings));
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                final Runnable uiRunnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        searchDialog();
                    }
                };

                SupportTools.runOnUiActivity(getActivity(), uiRunnable);
            }
        });

        btnSearch.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                if(!btnSearch.isEnabled())
                    return;

                if(hasFocus)
                {
                    if(searchKey.isEmpty())
                        btnSearch.setBackgroundResource(R.drawable.search_focus);
                    else
                        btnSearch.setBackgroundResource(R.drawable.search_on_focus);
                }
                else
                {
                    if(searchKey.isEmpty())
                        btnSearch.setBackgroundResource(R.drawable.search);
                    else
                        btnSearch.setBackgroundResource(R.drawable.search_on);
                }
            }
        });

        btnSearch.setAccessibilityDelegate(new View.AccessibilityDelegate()
        {
            @Override
            public void sendAccessibilityEvent(View host, int eventType)
            {
                super.sendAccessibilityEvent(host, eventType);

                btnSearch.setContentDescription(getString(R.string.accessibility_search_server));
            }
        });

        btnReloadManifest.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                if(airVPNManifest != null)
                {
                    Runnable runnable = new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            supportTools.showProgressDialog(R.string.airvpn_server_refresh_manifest);
                        }
                    };

                    SupportTools.runOnUiActivity(EddieApplication.mainActivity(), runnable);

                    airVPNManifest.refreshManifestFromAirVPN();
                }
            }
        });

        btnReloadManifest.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                if(!btnReloadManifest.isEnabled())
                    return;

                if(hasFocus)
                    btnReloadManifest.setBackgroundResource(R.drawable.refresh_servers_focus);
                else
                    btnReloadManifest.setBackgroundResource(R.drawable.refresh_servers);
            }
        });

        btnReloadManifest.setAccessibilityDelegate(new View.AccessibilityDelegate()
        {
            @Override
            public void sendAccessibilityEvent(View host, int eventType)
            {
                super.sendAccessibilityEvent(host, eventType);

                btnReloadManifest.setContentDescription(getString(R.string.accessibility_reload_manifest));
            }
        });

        llVpnType.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                if(settingsManager.getAirVPNDefaultVPNType().equals(SettingsManager.VPN_TYPE_OPENVPN))
                    settingsManager.setAirVPNDefaultVPNType(SettingsManager.VPN_TYPE_WIREGUARD);
                else
                    settingsManager.setAirVPNDefaultVPNType(SettingsManager.VPN_TYPE_OPENVPN);

                showConnectionInfo();
            }
        });

        showConnectionInfo();

        if(airVPNUser.isUserValid() && NetworkStatusReceiver.isNetworkConnected())
            supportTools.setButtonStatus(btnReloadManifest, SupportTools.ShowMode.ENABLED);
        else
            supportTools.setButtonStatus(btnReloadManifest, SupportTools.ShowMode.HIDDEN);

        return fragmentView;
    }

    @Override
    public void setMenuVisibility(boolean visible)
    {
        super.setMenuVisibility(visible);

        if(visible)
            refreshFragment();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        MenuItem item = null, itemAddOpenVPNProfile = null, itemOpenVPNExport = null;
        MenuItem itemAddWireGuardProfile = null, itemWireGuardExport = null;
        ListItem listItem = null;
        int menuResource = 0;

        super.onCreateContextMenu(menu, v, menuInfo);

        if(!airVPNUser.isUserValid())
            return;

        if(v.getId() == R.id.airvpn_server_list)
        {
            MenuInflater inflater = getActivity().getMenuInflater();

            ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo)menuInfo;

            int type = ExpandableListView.getPackedPositionType(info.packedPosition);
            int group = ExpandableListView.getPackedPositionGroup(info.packedPosition);
            int child = ExpandableListView.getPackedPositionChild(info.packedPosition);

            if(group == 0)
            {
                // Startup

                if(child < 0)
                {
                    if(groupList.get(0).getItemList().size() > 0)
                        menuResource = R.menu.empty_default_item_menu;
                }
                else
                    menuResource = R.menu.default_item_menu;
            }
            else if(group == 1)
            {
                // Favorite

                if(child < 0)
                {
                    if(groupList.get(1).getItemList().size() > 0)
                        menuResource = R.menu.empty_whitelist_server_menu;
                }
                else
                {
                    if(groupList.get(group).getItemList().get(child).getType() == ItemType.SERVER)
                        menuResource = R.menu.whitelist_server_menu;
                    else
                        menuResource = R.menu.whitelist_country_menu;
                }
            }
            else if(group == 2)
            {
                // Forbidden

                if(child < 0)
                {
                    if(groupList.get(2).getItemList().size() > 0)
                        menuResource = R.menu.empty_blacklist_server_menu;
                }
                else
                {
                    if(groupList.get(group).getItemList().get(child).getType() == ItemType.SERVER)
                        menuResource = R.menu.blacklist_server_menu;
                    else
                        menuResource = R.menu.blacklist_country_menu;
                }
            }
            else if(group == 3)
            {
                menuResource = R.menu.continent_menu;
            }
            else if(group > 3)
            {
                // Countries and servers

                if(child >= 0)
                {
                    // Server item

                    listItem = groupList.get(group).getItemList().get(child);

                    if(listItem != null)
                    {
                        if(listItem.isFavorite())
                            menuResource = R.menu.whitelist_server_menu;
                        else if(listItem.isForbidden())
                            menuResource = R.menu.blacklist_server_menu;
                        else
                            menuResource = R.menu.server_menu;
                    }
                }
                else
                {
                    // Country item

                    String countryCode = groupList.get(group).getCountryCode();

                    if(countryCode != null)
                    {
                        if(settingsManager.isAirVPNCountryWhitelisted(countryCode))
                            menuResource = R.menu.whitelist_country_menu;
                        else if(settingsManager.isAirVPNCountryBlacklisted(countryCode))
                            menuResource = R.menu.blacklist_country_menu;
                        else
                            menuResource = R.menu.country_menu;
                    }
                }
            }

            if(menuResource != 0)
            {
                inflater.inflate(menuResource, menu);

                for(int i = 0; i < menu.size(); i++)
                {
                    item = menu.getItem(i);

                    if(item != null)
                    {
                        Intent intent = new Intent();
                        intent.putExtra("fragment_id", FRAGMENT_ID);

                        item.setIntent(intent);
                    }
                }

                if(group == 3)
                {
                    HashMap<String, AirVPNManifest.ContinentStats> continentStats = (HashMap<String, AirVPNManifest.ContinentStats>)airVPNManifest.getContinentStats().clone();

                    if(continentStats != null)
                    {
                        for(Map.Entry<String, AirVPNManifest.ContinentStats> entry : continentStats.entrySet())
                        {
                            switch(entry.getKey())
                            {
                                case "EUR":
                                {
                                    item = menu.findItem(R.id.context_menu_connect_europe);
                                    itemAddOpenVPNProfile = menu.findItem(R.id.context_menu_add_europe_to_openvpn_profiles);
                                    itemOpenVPNExport = menu.findItem(R.id.context_menu_export_openvpn_profile_europe);
                                    itemAddWireGuardProfile = menu.findItem(R.id.context_menu_add_europe_to_wireguard_profiles);
                                    itemWireGuardExport = menu.findItem(R.id.context_menu_export_wireguard_profile_europe);
                                }
                                break;

                                case "AFR":
                                {
                                    item = menu.findItem(R.id.context_menu_connect_africa);
                                    itemAddOpenVPNProfile = menu.findItem(R.id.context_menu_add_africa_to_openvpn_profiles);
                                    itemOpenVPNExport = menu.findItem(R.id.context_menu_export_openvpn_profile_africa);
                                    itemAddWireGuardProfile = menu.findItem(R.id.context_menu_add_africa_to_wireguard_profiles);
                                    itemWireGuardExport = menu.findItem(R.id.context_menu_export_wireguard_profile_africa);
                                }
                                break;

                                case "NAM":
                                case "SAM":
                                {
                                    item = menu.findItem(R.id.context_menu_connect_america);
                                    itemAddOpenVPNProfile = menu.findItem(R.id.context_menu_add_america_to_openvpn_profiles);
                                    itemOpenVPNExport = menu.findItem(R.id.context_menu_export_openvpn_profile_america);
                                    itemAddWireGuardProfile = menu.findItem(R.id.context_menu_add_america_to_wireguard_profiles);
                                    itemWireGuardExport = menu.findItem(R.id.context_menu_export_wireguard_profile_america);
                                }
                                break;

                                case "ASI":
                                {
                                    item = menu.findItem(R.id.context_menu_connect_asia);
                                    itemAddOpenVPNProfile = menu.findItem(R.id.context_menu_add_asia_to_openvpn_profiles);
                                    itemOpenVPNExport = menu.findItem(R.id.context_menu_export_openvpn_profile_asia);
                                    itemAddWireGuardProfile = menu.findItem(R.id.context_menu_add_asia_to_wireguard_profiles);
                                    itemWireGuardExport = menu.findItem(R.id.context_menu_export_wireguard_profile_asia);
                                }
                                break;

                                case "OCE":
                                {
                                    item = menu.findItem(R.id.context_menu_connect_oceania);
                                    itemAddOpenVPNProfile = menu.findItem(R.id.context_menu_add_oceania_to_openvpn_profiles);
                                    itemOpenVPNExport = menu.findItem(R.id.context_menu_export_openvpn_profile_oceania);
                                    itemAddWireGuardProfile = menu.findItem(R.id.context_menu_add_oceania_to_wireguard_profiles);
                                    itemWireGuardExport = menu.findItem(R.id.context_menu_export_wireguard_profile_oceania);
                                }
                                break;

                                default:
                                {
                                    item = null;
                                }
                                break;
                            }

                            if(item != null)
                            {
                                item.setVisible(true);
                                itemAddOpenVPNProfile.setVisible(true);
                                itemOpenVPNExport.setVisible(true);
                                itemAddWireGuardProfile.setVisible(true);
                                itemWireGuardExport.setVisible(true);
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem menuItem)
    {
        boolean processed = false;

        Intent intent = menuItem.getIntent();

        if(intent != null && intent.getIntExtra("fragment_id", -1) != FRAGMENT_ID)
            return false;

        ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo)menuItem.getMenuInfo();

        int type = ExpandableListView.getPackedPositionType(info.packedPosition);
        int group = ExpandableListView.getPackedPositionGroup(info.packedPosition);
        int child = ExpandableListView.getPackedPositionChild(info.packedPosition);

        if(child >= 0)
            listItem = groupList.get(group).getItemList().get(child);

        switch(menuItem.getItemId())
        {
            case R.id.context_menu_empty_default_list:
            {
                emptyDefaultList();

                processed = true;
            }
            break;

            case R.id.context_menu_remove_startup:
            {
                if(listItem != null)
                {
                    if(listItem.type == ItemType.SERVER)
                        removeStartupServer();
                    else if(listItem.type == ItemType.COUNTRY)
                        removeStartupCountry();
                }

                processed = true;
            }
            break;

            case R.id.context_menu_connect_server:
            {
                final Runnable uiRunnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        connectServer(listItem, ConnectionMode.SERVER, "");
                    }
                };

                SupportTools.runOnUiActivity(getActivity(), uiRunnable);

                processed = true;
            }
            break;

            case R.id.context_menu_add_favorite_server:
            {
                addServerToFavorites(listItem);

                processed = true;
            }
            break;

            case R.id.context_menu_remove_favorite_server:
            {
                removeServerFromFavorites(listItem);

                processed = true;
            }
            break;

            case R.id.context_menu_set_startup_server:
            {
                setStartupServer(listItem);

                processed = true;
            }
            break;

            case R.id.context_menu_add_server_to_openvpn_profiles:
            case R.id.context_menu_export_server_to_openvpn_profiles:
            {
                final Runnable uiRunnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        ExportMode exportMode;

                        if(menuItem.getItemId() == R.id.context_menu_add_server_to_openvpn_profiles)
                            exportMode = ExportMode.DATABASE;
                        else
                            exportMode = ExportMode.FILE;

                        exportVPNProfile(listItem, SettingsManager.VPN_TYPE_OPENVPN, ConnectionMode.SERVER, "", exportMode);
                    }
                };

                SupportTools.runOnUiActivity(getActivity(), uiRunnable);

                processed = true;
            }
            break;

            case R.id.context_menu_add_server_to_wireguard_profiles:
            case R.id.context_menu_export_server_to_wireguard_profiles:
            {
                final Runnable uiRunnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        ExportMode exportMode;

                        if(menuItem.getItemId() == R.id.context_menu_add_server_to_wireguard_profiles)
                            exportMode = ExportMode.DATABASE;
                        else
                            exportMode = ExportMode.FILE;

                        exportVPNProfile(listItem, SettingsManager.VPN_TYPE_WIREGUARD, ConnectionMode.SERVER, "", exportMode);
                    }
                };

                SupportTools.runOnUiActivity(getActivity(), uiRunnable);

                processed = true;
            }
            break;

            case R.id.context_menu_connect_country:
            {
                final Runnable uiRunnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        listItem = groupList.get(group).getItemList().get(0);

                        connectServer(listItem, ConnectionMode.COUNTRY, listItem.getCountryCode());
                    }
                };

                SupportTools.runOnUiActivity(getActivity(), uiRunnable);

                processed = true;
            }
            break;

            case R.id.context_menu_add_favorite_country:
            {
                addCountryToFavorites(groupList.get(group).getCountryCode());

                processed = true;
            }
            break;

            case R.id.context_menu_remove_favorite_country:
            {
                String countryCode = "";

                if(group == 1) // Favorite group
                    countryCode = groupList.get(group).getItemList().get(child).getCountryCode();
                else
                    countryCode = groupList.get(group).getCountryCode();

                removeCountryFromFavorites(countryCode);

                processed = true;
            }
            break;

            case R.id.context_menu_set_startup_country:
            {
                setStartupCountry(groupList.get(group).getCountryCode());

                processed = true;
            }
            break;

            case R.id.context_menu_add_country_to_openvpn_profiles:
            case R.id.context_menu_export_country_to_openvpn_profiles:
            {
                final Runnable uiRunnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        ExportMode exportMode;
                        String countryCode = "";

                        if(menuItem.getItemId() == R.id.context_menu_add_country_to_openvpn_profiles)
                            exportMode = ExportMode.DATABASE;
                        else
                            exportMode = ExportMode.FILE;

                        if(group == 1) // Favorite group
                            countryCode = groupList.get(group).getItemList().get(child).getCountryCode();
                        else
                            countryCode = groupList.get(group).getCountryCode();

                        listItem = groupList.get(group).getItemList().get(0);

                        exportVPNProfile(listItem, SettingsManager.VPN_TYPE_OPENVPN, ConnectionMode.COUNTRY, countryCode, exportMode);
                    }
                };

                SupportTools.runOnUiActivity(getActivity(), uiRunnable);

                processed = true;
            }
            break;

            case R.id.context_menu_add_country_to_wireguard_profiles:
            case R.id.context_menu_export_country_to_wireguard_profiles:
            {
                final Runnable uiRunnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        ExportMode exportMode;
                        String countryCode = "";

                        if(menuItem.getItemId() == R.id.context_menu_add_country_to_wireguard_profiles)
                            exportMode = ExportMode.DATABASE;
                        else
                            exportMode = ExportMode.FILE;

                        if(group == 1) // Favorite group
                            countryCode = groupList.get(group).getItemList().get(child).getCountryCode();
                        else
                            countryCode = groupList.get(group).getCountryCode();

                        listItem = groupList.get(group).getItemList().get(0);

                        exportVPNProfile(listItem, SettingsManager.VPN_TYPE_WIREGUARD, ConnectionMode.COUNTRY, countryCode, exportMode);
                    }
                };

                SupportTools.runOnUiActivity(getActivity(), uiRunnable);

                processed = true;
            }
            break;

            case R.id.context_menu_connect_earth:
            {
                if(referenceServerItem != null)
                {
                    final Runnable uiRunnable = new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            connectServer(referenceServerItem, ConnectionMode.CONTINENT, CountryContinent.EARTH);
                        }
                    };

                    SupportTools.runOnUiActivity(getActivity(), uiRunnable);
                }

                processed = true;
            }
            break;

            case R.id.context_menu_add_earth_to_openvpn_profiles:
            case R.id.context_menu_export_openvpn_profile_earth:
            {
                final Runnable uiRunnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        ExportMode exportMode;

                        if(menuItem.getItemId() == R.id.context_menu_add_earth_to_openvpn_profiles)
                            exportMode = ExportMode.DATABASE;
                        else
                            exportMode = ExportMode.FILE;

                        if(referenceServerItem != null)
                            exportVPNProfile(referenceServerItem, SettingsManager.VPN_TYPE_OPENVPN, ConnectionMode.CONTINENT, CountryContinent.EARTH, exportMode);
                    }
                };

                SupportTools.runOnUiActivity(getActivity(), uiRunnable);

                processed = true;
            }
            break;

            case R.id.context_menu_add_earth_to_wireguard_profiles:
            case R.id.context_menu_export_wireguard_profile_earth:
            {
                final Runnable uiRunnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        ExportMode exportMode;

                        if(menuItem.getItemId() == R.id.context_menu_add_earth_to_wireguard_profiles)
                            exportMode = ExportMode.DATABASE;
                        else
                            exportMode = ExportMode.FILE;

                        if(referenceServerItem != null)
                            exportVPNProfile(referenceServerItem, SettingsManager.VPN_TYPE_WIREGUARD, ConnectionMode.CONTINENT, CountryContinent.EARTH, exportMode);
                    }
                };

                SupportTools.runOnUiActivity(getActivity(), uiRunnable);

                processed = true;
            }
            break;

            case R.id.context_menu_connect_europe:
            {
                if(referenceServerItem != null)
                {
                    final Runnable uiRunnable = new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            listItem = referenceServerItem;

                            connectServer(listItem, ConnectionMode.CONTINENT, CountryContinent.EUROPE);
                        }
                    };

                    SupportTools.runOnUiActivity(getActivity(), uiRunnable);
                }

                processed = true;
            }
            break;

            case R.id.context_menu_add_europe_to_openvpn_profiles:
            case R.id.context_menu_export_openvpn_profile_europe:
            {
                final Runnable uiRunnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        ExportMode exportMode;

                        if(menuItem.getItemId() == R.id.context_menu_add_europe_to_openvpn_profiles)
                            exportMode = ExportMode.DATABASE;
                        else
                            exportMode = ExportMode.FILE;

                        if(referenceServerItem != null)
                            exportVPNProfile(referenceServerItem, SettingsManager.VPN_TYPE_OPENVPN, ConnectionMode.CONTINENT, CountryContinent.EUROPE, exportMode);
                    }
                };

                SupportTools.runOnUiActivity(getActivity(), uiRunnable);

                processed = true;
            }
            break;

            case R.id.context_menu_add_europe_to_wireguard_profiles:
            case R.id.context_menu_export_wireguard_profile_europe:
            {
                final Runnable uiRunnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        ExportMode exportMode;

                        if(menuItem.getItemId() == R.id.context_menu_add_europe_to_wireguard_profiles)
                            exportMode = ExportMode.DATABASE;
                        else
                            exportMode = ExportMode.FILE;

                        if(referenceServerItem != null)
                            exportVPNProfile(referenceServerItem, SettingsManager.VPN_TYPE_WIREGUARD, ConnectionMode.CONTINENT, CountryContinent.EUROPE, exportMode);
                    }
                };

                SupportTools.runOnUiActivity(getActivity(), uiRunnable);

                processed = true;
            }
            break;

            case R.id.context_menu_connect_africa:
            {
                if(referenceServerItem != null)
                {
                    final Runnable uiRunnable = new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            listItem = referenceServerItem;

                            connectServer(listItem, ConnectionMode.CONTINENT, CountryContinent.AFRICA);
                        }
                    };

                    SupportTools.runOnUiActivity(getActivity(), uiRunnable);
                }

                processed = true;
            }
            break;

            case R.id.context_menu_add_africa_to_openvpn_profiles:
            case R.id.context_menu_export_openvpn_profile_africa:
            {
                final Runnable uiRunnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        ExportMode exportMode;

                        if(menuItem.getItemId() == R.id.context_menu_add_africa_to_openvpn_profiles)
                            exportMode = ExportMode.DATABASE;
                        else
                            exportMode = ExportMode.FILE;

                        if(referenceServerItem != null)
                            exportVPNProfile(referenceServerItem, SettingsManager.VPN_TYPE_OPENVPN, ConnectionMode.CONTINENT, CountryContinent.AFRICA, exportMode);
                    }
                };

                SupportTools.runOnUiActivity(getActivity(), uiRunnable);

                processed = true;
            }
            break;

            case R.id.context_menu_add_africa_to_wireguard_profiles:
            case R.id.context_menu_export_wireguard_profile_africa:
            {
                final Runnable uiRunnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        ExportMode exportMode;

                        if(menuItem.getItemId() == R.id.context_menu_add_africa_to_wireguard_profiles)
                            exportMode = ExportMode.DATABASE;
                        else
                            exportMode = ExportMode.FILE;

                        if(referenceServerItem != null)
                            exportVPNProfile(referenceServerItem, SettingsManager.VPN_TYPE_WIREGUARD, ConnectionMode.CONTINENT, CountryContinent.AFRICA, exportMode);
                    }
                };

                SupportTools.runOnUiActivity(getActivity(), uiRunnable);

                processed = true;
            }
            break;

            case R.id.context_menu_connect_america:
            {
                if(referenceServerItem != null)
                {
                    final Runnable uiRunnable = new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            listItem = referenceServerItem;

                            connectServer(listItem, ConnectionMode.CONTINENT, CountryContinent.AMERICA);
                        }
                    };

                    SupportTools.runOnUiActivity(getActivity(), uiRunnable);
                }

                processed = true;
            }
            break;

            case R.id.context_menu_add_america_to_openvpn_profiles:
            case R.id.context_menu_export_openvpn_profile_america:
            {
                final Runnable uiRunnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        ExportMode exportMode;

                        if(menuItem.getItemId() == R.id.context_menu_add_america_to_openvpn_profiles)
                            exportMode = ExportMode.DATABASE;
                        else
                            exportMode = ExportMode.FILE;

                        if(referenceServerItem != null)
                            exportVPNProfile(referenceServerItem, SettingsManager.VPN_TYPE_OPENVPN, ConnectionMode.CONTINENT, CountryContinent.AMERICA, exportMode);
                    }
                };

                SupportTools.runOnUiActivity(getActivity(), uiRunnable);

                processed = true;
            }
            break;

            case R.id.context_menu_add_america_to_wireguard_profiles:
            case R.id.context_menu_export_wireguard_profile_america:
            {
                final Runnable uiRunnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        ExportMode exportMode;

                        if(menuItem.getItemId() == R.id.context_menu_add_america_to_wireguard_profiles)
                            exportMode = ExportMode.DATABASE;
                        else
                            exportMode = ExportMode.FILE;

                        if(referenceServerItem != null)
                            exportVPNProfile(referenceServerItem, SettingsManager.VPN_TYPE_WIREGUARD, ConnectionMode.CONTINENT, CountryContinent.AMERICA, exportMode);
                    }
                };

                SupportTools.runOnUiActivity(getActivity(), uiRunnable);

                processed = true;
            }
            break;

            case R.id.context_menu_connect_asia:
            {
                if(referenceServerItem != null)
                {
                    final Runnable uiRunnable = new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            listItem = referenceServerItem;

                            connectServer(listItem, ConnectionMode.CONTINENT, CountryContinent.ASIA);
                        }
                    };

                    SupportTools.runOnUiActivity(getActivity(), uiRunnable);
                }

                processed = true;
            }
            break;

            case R.id.context_menu_add_asia_to_openvpn_profiles:
            case R.id.context_menu_export_openvpn_profile_asia:
            {
                final Runnable uiRunnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        ExportMode exportMode;

                        if(menuItem.getItemId() == R.id.context_menu_add_asia_to_openvpn_profiles)
                            exportMode = ExportMode.DATABASE;
                        else
                            exportMode = ExportMode.FILE;

                        if(referenceServerItem != null)
                            exportVPNProfile(referenceServerItem, SettingsManager.VPN_TYPE_OPENVPN, ConnectionMode.CONTINENT, CountryContinent.ASIA, exportMode);
                    }
                };

                SupportTools.runOnUiActivity(getActivity(), uiRunnable);

                processed = true;
            }
            break;

            case R.id.context_menu_add_asia_to_wireguard_profiles:
            case R.id.context_menu_export_wireguard_profile_asia:
            {
                final Runnable uiRunnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        ExportMode exportMode;

                        if(menuItem.getItemId() == R.id.context_menu_add_asia_to_wireguard_profiles)
                            exportMode = ExportMode.DATABASE;
                        else
                            exportMode = ExportMode.FILE;

                        if(referenceServerItem != null)
                            exportVPNProfile(referenceServerItem, SettingsManager.VPN_TYPE_WIREGUARD, ConnectionMode.CONTINENT, CountryContinent.ASIA, exportMode);
                    }
                };

                SupportTools.runOnUiActivity(getActivity(), uiRunnable);

                processed = true;
            }
            break;

            case R.id.context_menu_connect_oceania:
            {
                if(referenceServerItem != null)
                {
                    final Runnable uiRunnable = new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            listItem = referenceServerItem;

                            connectServer(listItem, ConnectionMode.CONTINENT, CountryContinent.OCEANIA);
                        }
                    };

                    SupportTools.runOnUiActivity(getActivity(), uiRunnable);
                }

                processed = true;
            }
            break;

            case R.id.context_menu_add_oceania_to_openvpn_profiles:
            case R.id.context_menu_export_openvpn_profile_oceania:
            {
                final Runnable uiRunnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        ExportMode exportMode;

                        if(menuItem.getItemId() == R.id.context_menu_add_oceania_to_openvpn_profiles)
                            exportMode = ExportMode.DATABASE;
                        else
                            exportMode = ExportMode.FILE;

                        if(referenceServerItem != null)
                            exportVPNProfile(referenceServerItem, SettingsManager.VPN_TYPE_OPENVPN, ConnectionMode.CONTINENT, CountryContinent.OCEANIA, exportMode);
                    }
                };

                SupportTools.runOnUiActivity(getActivity(), uiRunnable);

                processed = true;
            }
            break;

            case R.id.context_menu_add_oceania_to_wireguard_profiles:
            case R.id.context_menu_export_wireguard_profile_oceania:
            {
                final Runnable uiRunnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        ExportMode exportMode;

                        if(menuItem.getItemId() == R.id.context_menu_add_oceania_to_wireguard_profiles)
                            exportMode = ExportMode.DATABASE;
                        else
                            exportMode = ExportMode.FILE;

                        if(referenceServerItem != null)
                            exportVPNProfile(referenceServerItem, SettingsManager.VPN_TYPE_WIREGUARD, ConnectionMode.CONTINENT, CountryContinent.OCEANIA, exportMode);
                    }
                };

                SupportTools.runOnUiActivity(getActivity(), uiRunnable);

                processed = true;
            }
            break;

            case R.id.context_menu_empty_favorite_list:
            {
                emptyFavoriteList();

                processed = true;
            }
            break;

            case R.id.context_menu_add_forbidden_server:
            {
                addServerToForbidden(listItem);

                processed = true;
            }
            break;

            case R.id.context_menu_remove_forbidden_server:
            {
                removeServerFromForbidden(listItem);

                processed = true;
            }
            break;

            case R.id.context_menu_add_forbidden_country:
            {
                addCountryToForbidden(groupList.get(group).getCountryCode());

                processed = true;
            }
            break;

            case R.id.context_menu_remove_forbidden_country:
            {
                String countryCode = "";

                if(group == 2) // Forbidden group
                    countryCode = groupList.get(group).getItemList().get(child).getCountryCode();
                else
                    countryCode = groupList.get(group).getCountryCode();

                removeCountryFromForbidden(countryCode);

                processed = true;
            }
            break;

            case R.id.context_menu_empty_forbidden_list:
            {
                emptyForbiddenList();

                processed = true;
            }
            break;

            default:
            {
                processed = super.onContextItemSelected(menuItem);
            }
            break;
        }

        return processed;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode)
        {
            case ACTIVITY_RESULT_SERVER_SETTINGS:
            {
                if(resultCode == RESULT_OK)
                    refreshServerList();
            }
            break;

            default:
            {
            }
            break;
        }
    }

    private void refreshFragment()
    {
        if(airVPNUser.isUserValid())
        {
            if(NetworkStatusReceiver.isNetworkConnected())
                supportTools.setButtonStatus(btnReloadManifest, SupportTools.ShowMode.ENABLED);

            if(elvAirVPNServer != null)
                registerForContextMenu(elvAirVPNServer);
        }
        else
        {
            supportTools.setButtonStatus(btnReloadManifest, SupportTools.ShowMode.HIDDEN);

            if(elvAirVPNServer != null)
                unregisterForContextMenu(elvAirVPNServer);
        }

        if(btnSearch != null && searchKey != null)
        {
            if(searchKey.isEmpty())
                btnSearch.setBackgroundResource(R.drawable.search);
            else
                btnSearch.setBackgroundResource(R.drawable.search_on);
        }

        showConnectionInfo();

        refreshServerList();
    }

    private void addServerToFavorites(ListItem server)
    {
        if(server == null)
            return;

        ArrayList<String> serverList = settingsManager.getAirVPNServerWhitelist();

        serverList.add(server.getName());

        settingsManager.setAirVPNServerWhitelist(serverList);

        refreshServerList();
    }

    private void removeServerFromFavorites(ListItem server)
    {
        if(server == null)
            return;

        ArrayList<String> serverList = settingsManager.getAirVPNServerWhitelist();

        serverList.remove(server.getName());

        settingsManager.setAirVPNServerWhitelist(serverList);

        refreshServerList();
    }

    private void setStartupServer(ListItem server)
    {
        if(server == null)
            return;

        settingsManager.setStartupAirVPNServer(server.getName());

        refreshServerList();
    }

    private void removeStartupServer()
    {
        settingsManager.removeStartupAirVPNServer();

        refreshServerList();
    }

    private void exportVPNProfile(ListItem server, String vpnType, ConnectionMode connectionMode, String countryCode, ExportMode exportMode)
    {
        String vpnProfileName = "", newProfileName = "", serverHost = "", fileExt = "", protocol = "", tlsMode = "";
        VPNProfileDatabase.VPNProfile dbVPNProfile = null;
        VPNProfileDatabase.ProtocolType proto = VPNProfileDatabase.ProtocolType.UNKNOWN;
        boolean connectIPv6 = false, profileNameExists = false, createCountryProfile = false;
        int port;
        HashMap<String, String> vpnProfile = null;

        if(!supportTools.confirmationDialog(R.string.airvpn_export_vpn_profile_warning))
            return;

        if(server == null || server.getName().isEmpty() == true)
            return;

        protocol = settingsManager.getAirVPNDefaultOpenVPNProtocol();

        port = settingsManager.getAirVPNDefaultOpenVPNPort();

        tlsMode = settingsManager.getAirVPNDefaultTLSMode();

        switch(vpnType)
        {
            case SettingsManager.VPN_TYPE_OPENVPN:
            {
                fileExt = "ovpn";
            }
            break;

            case SettingsManager.VPN_TYPE_WIREGUARD:
            {
                fileExt = "conf";
            }
            break;

            default:
            {
                fileExt = "---";
            }
            break;
        }

        switch(settingsManager.getAirVPNDefaultIPVersion())
        {
            case SettingsManager.AIRVPN_IP_VERSION_4:
            {
                connectIPv6 = false;
            }
            break;

            case SettingsManager.AIRVPN_IP_VERSION_6:
            {
                connectIPv6 = true;
            }
            break;

            case SettingsManager.AIRVPN_IP_VERSION_4_OVER_6:
            {
                connectIPv6 = true;
            }
            break;

            default:
            {
                connectIPv6 = false;
            }
            break;
        }

        switch(connectionMode)
        {
            case SERVER:
            {
                vpnProfileName = String.format("AirVPN_%s-%s_%s_%s-%d.%s", server.getCountryCode(), server.getLocation(), server.getName(), protocol, port, fileExt);

                serverHost = server.getName();

                createCountryProfile = false;
            }
            break;

            case COUNTRY:
            {
                vpnProfileName = String.format("AirVPN_%s_%s-%d.%s", countryContinent.getCountryName(countryCode), protocol, port, fileExt);

                serverHost = countryCode;

                createCountryProfile = true;
            }
            break;

            case CONTINENT:
            {
                countryCode = countryCode.substring(0, 1).toUpperCase(Locale.US) + countryCode.substring(1).toLowerCase(Locale.US);

                vpnProfileName = String.format("AirVPN_%s_%s-%d.%s", countryCode, protocol, port, fileExt);

                serverHost = countryCode;

                createCountryProfile = true;
            }
            break;

            default:
            {
                return;
            }
        }

        vpnProfile = createVpnProfile(serverHost, createCountryProfile, true, vpnType);

        if(vpnProfile.get("status").equals("ok") == false)
        {
            EddieLogger.error("ConnectAirVPNServerFragment.exportVPNProfile(): vpnProfile is empty");

            supportTools.infoDialog(R.string.connection_error, true);

            return;
        }

        if(exportMode == ExportMode.DATABASE)
        {
            profileNameExists = vpnProfileDatabase.exists(vpnProfileName);

            if(profileNameExists)
            {
                newProfileName = vpnProfileName;

                while(profileNameExists)
                {
                    newProfileName = supportTools.getTextOptionDialog(getContext(), R.string.vpn_profile_name_exists, vpnProfileName, SupportTools.EditOption.DO_NOT_ALLOW_EMPTY_FIELD);

                    if(!vpnProfileName.equals(newProfileName))
                        profileNameExists = vpnProfileDatabase.exists(newProfileName);
                    else
                        return;
                }

                vpnProfileName = newProfileName;
            }

            dbVPNProfile = vpnProfileDatabase.new VPNProfile();

            dbVPNProfile.setName(vpnProfileName);

            if(vpnProfile.get("type").equals(SettingsManager.VPN_TYPE_OPENVPN) == true)
                dbVPNProfile.setType(VPNProfileDatabase.ProfileType.OPENVPN);
            else if(vpnProfile.get("type").equals(SettingsManager.VPN_TYPE_WIREGUARD) == true)
                dbVPNProfile.setType(VPNProfileDatabase.ProfileType.WIREGUARD);
            else
                dbVPNProfile.setType(VPNProfileDatabase.ProfileType.UNKNOWN);

            dbVPNProfile.setIpAddress(vpnProfile.get("host"));

            dbVPNProfile.setPort(port);

            switch(protocol)
            {
                case "UDP":
                {
                    proto = VPNProfileDatabase.ProtocolType.UDPv4;
                }
                break;

                case "TCP":
                {
                    proto = VPNProfileDatabase.ProtocolType.TCPv4;
                }
                break;

                case "UDP6":
                {
                    proto = VPNProfileDatabase.ProtocolType.UDPv6;
                }
                break;

                case "TCP6":
                {
                    proto = VPNProfileDatabase.ProtocolType.TCPv6;
                }
                break;
            }

            dbVPNProfile.setProtocol(proto);

            dbVPNProfile.setProfile(vpnProfile.get("profile"));

            switch(connectionMode)
            {
                case SERVER:
                {
                    dbVPNProfile.setAirVPNServerName(server.getName());
                    dbVPNProfile.setAirVPNServerLocation(server.getLocation());
                    dbVPNProfile.setAirVPNServerCountry(countryContinent.getCountryName(server.getCountryCode()));
                }
                break;

                case COUNTRY:
                case CONTINENT:
                {
                    dbVPNProfile.setAirVPNServerName(supportTools.getAirVpnCountryHostname(countryCode, tlsMode, connectIPv6));

                    dbVPNProfile.setAirVPNServerLocation("");

                    if(connectionMode == ConnectionMode.COUNTRY)
                        dbVPNProfile.setAirVPNServerCountry(countryContinent.getCountryName(countryCode));
                    else
                        dbVPNProfile.setAirVPNServerCountry("");
                }
                break;
            }

            dbVPNProfile.setBoot(false);

            vpnProfileDatabase.setProfile(vpnProfileName, dbVPNProfile);

            Toast.makeText(getActivity(), String.format(getResources().getString(R.string.airvpn_vpn_profile_added), vpnProfileName), Toast.LENGTH_LONG).show();
        }
        else
            supportTools.sharePlainText(vpnProfile.get("profile"), vpnProfileName, R.string.context_menu_export_openvpn_profile_cap);
    }

    private void addCountryToFavorites(String countryCode)
    {
        if(countryCode == null || countryCode.isEmpty())
            return;

        ArrayList<String> countryList = settingsManager.getAirVPNCountryWhitelist();

        countryList.add(countryCode);

        settingsManager.setAirVPNCountryWhitelist(countryList);

        refreshServerList();
    }

    private void removeCountryFromFavorites(String countryCode)
    {
        if(countryCode == null || countryCode.isEmpty())
            return;

        ArrayList<String> countryList = settingsManager.getAirVPNCountryWhitelist();

        countryList.remove(countryCode);

        settingsManager.setAirVPNCountryWhitelist(countryList);

        refreshServerList();
    }

    private void addServerToForbidden(ListItem server)
    {
        if(server == null)
            return;

        ArrayList<String> serverList = settingsManager.getAirVPNServerBlacklist();

        serverList.add(server.getName());

        settingsManager.setAirVPNServerBlacklist(serverList);

        refreshServerList();
    }

    private void removeServerFromForbidden(ListItem server)
    {
        if(server == null)
            return;

        ArrayList<String> serverList = settingsManager.getAirVPNServerBlacklist();

        serverList.remove(server.getName());

        settingsManager.setAirVPNServerBlacklist(serverList);

        refreshServerList();
    }

    private void addCountryToForbidden(String countryCode)
    {
        if(countryCode == null || countryCode.isEmpty())
            return;

        ArrayList<String> countryList = settingsManager.getAirVPNCountryBlacklist();

        countryList.add(countryCode);

        settingsManager.setAirVPNCountryBlacklist(countryList);

        refreshServerList();
    }

    private void removeCountryFromForbidden(String countryCode)
    {
        if(countryCode == null || countryCode.isEmpty())
            return;

        ArrayList<String> countryList = settingsManager.getAirVPNCountryBlacklist();

        countryList.remove(countryCode);

        settingsManager.setAirVPNCountryBlacklist(countryList);

        refreshServerList();
    }

    private void setStartupCountry(String countryCode)
    {
        if(countryCode == null || countryCode.isEmpty())
            return;

        settingsManager.setStartupAirVPNCountry(countryCode);

        refreshServerList();
    }

    private void removeStartupCountry()
    {
        settingsManager.removeStartupAirVPNCountry();

        refreshServerList();
    }

    private void emptyFavoriteList()
    {
        if(!supportTools.confirmationDialog(R.string.airvpn_confirm_empty_favorite_list))
            return;

        settingsManager.setAirVPNServerWhitelist(new ArrayList<String>());

        settingsManager.setAirVPNCountryWhitelist(new ArrayList<String>());

        refreshServerList();
    }

    private void emptyForbiddenList()
    {
        if(!supportTools.confirmationDialog(R.string.airvpn_confirm_empty_forbidden_list))
            return;

        settingsManager.setAirVPNServerBlacklist(new ArrayList<String>());

        settingsManager.setAirVPNCountryBlacklist(new ArrayList<String>());

        refreshServerList();
    }

    private void emptyDefaultList()
    {
        settingsManager.removeStartupAirVPNServer();

        settingsManager.removeStartupAirVPNCountry();

        refreshServerList();
    }

    private void showStatus()
    {
        HashMap<String, String> currentProfile = vpnManager.vpn().getProfileInfo();
        String serverDescription = "", networkStatus = "", statusMessage = "";

        if(airVPNUser.isUserValid())
        {
            statusMessage += String.format("<font color='%s'>%s</font><br><br>", supportTools.hexColorCode(R.color.titleColor), String.format(getString(R.string.logged_in_as_user), airVPNUser.getUserName()));

            statusMessage += String.format("<font color='%s'>%s</font><br><br>", supportTools.hexColorCode(R.color.titleColor), airVPNUser.getExpirationText());
        }

        if(currentProfile != null && vpnManager.isVpnConnectionStopped() == false)
        {
            if(!currentProfile.get("description").equals(""))
                serverDescription = String.format("AirVPN %s (%s)", currentProfile.get("description"), currentProfile.get("server"));
            else
            {
                serverDescription = currentProfile.get("server");

                if(vpnManager.vpn().getConnectionMode() == VPN.ConnectionMode.OPENVPN_PROFILE)
                    serverDescription += " (" + getResources().getString(R.string.conn_type_openvpn_profile) + ")";
                else if(vpnManager.vpn().getConnectionMode() == VPN.ConnectionMode.WIREGUARD_PROFILE)
                    serverDescription += " (" + getResources().getString(R.string.conn_type_wireguard_profile) + ")";
            }
        }

        if(NetworkStatusReceiver.getNetworkStatus() == NetworkStatusReceiver.Status.CONNECTED)
            networkStatus = String.format(Locale.getDefault(), getString(R.string.conn_status_connected), NetworkStatusReceiver.getNetworkDescription());
        else
            networkStatus = getString(R.string.conn_status_not_available);

        statusMessage += String.format("<font color='%s'>%s</font> <font color='%s'>%s</font><br>", supportTools.hexColorCode(R.color.titleColor), getString(R.string.conn_network_status_cap), supportTools.hexColorCode(R.color.textColor), networkStatus);

        statusMessage += String.format("<font color='%s'>%s</font> <font color='%s'>%s</font><br>", supportTools.hexColorCode(R.color.titleColor), getString(R.string.conn_vpn_status_cap), supportTools.hexColorCode(R.color.textColor), getString(vpnManager.vpn().connectionStatusResourceDescription(vpnManager.vpn().getConnectionStatus())));

        if(!serverDescription.isEmpty())
        {
            statusMessage += "<br>";

            statusMessage += String.format(Locale.getDefault(), getString(R.string.connected_to_server), serverDescription);
        }

        supportTools.infoDialogHtml(statusMessage, true);
    }

    private synchronized void createGroupList()
    {
        Group group = null, airVPNGroup = null;
        ListItem itemList = null;
        ArrayList<String> serverWhitelist = null, serverBlacklist = null;
        String defaultServer = "", defaultIpVersion = "";
        ArrayList<Integer> cipherList = null;
        boolean includeServer = true, countryGroupAdded = false;
        int countedServers = 0, countedUsers = 0;
        long countedBandWidth = 0, countedMaxBandWidth = 0;

        if(airVPNManifest == null)
            return;

        referenceServerItem = null;

        if(!settingsManager.getAirVPNOpenVPNCipher().equals(SettingsManager.AIRVPN_CIPHER_SERVER))
        {
            CipherDatabase cipherDatabase = new CipherDatabase();

            if(cipherDatabase != null)
                cipherList = cipherDatabase.getMatchingCiphersArrayList(settingsManager.getAirVPNOpenVPNCipher(), CipherDatabase.CipherType.DATA);
        }

        countryWhitelist = settingsManager.getAirVPNCountryWhitelist();
        countryBlacklist = settingsManager.getAirVPNCountryBlacklist();

        groupList = new ArrayList<Group>();

        group = new Group();

        group.setType(GroupType.GROUP);
        group.setIcon(R.drawable.default_item);
        group.setName(getString(R.string.airvpn_startup_server));
        group.setCountryCode("");
        group.setServers(0);
        group.setUsers(0);

        defaultServer = settingsManager.getStartupAirvpnServer();
        defaultIpVersion = settingsManager.getAirVPNDefaultIPVersion();

        if(defaultIpVersion.equals(SettingsManager.AIRVPN_IP_VERSION_4_OVER_6))
            defaultIpVersion = SettingsManager.AIRVPN_IP_VERSION_4;

        if(defaultServer.isEmpty() == false)
        {
            AirVPNServer airVPNServer = airVPNManifest.getServerByName(defaultServer);

            if(airVPNServer != null)
            {
                includeServer = false;

                if(airVPNServer.getSupportIPv4() == true && defaultIpVersion.equals(SettingsManager.AIRVPN_IP_VERSION_4))
                    includeServer = true;
                else if(airVPNServer.getSupportIPv6() == true && defaultIpVersion.equals(SettingsManager.AIRVPN_IP_VERSION_6))
                    includeServer = true;

                if(airVPNServer.isAvailable(cipherList) && includeServer == true)
                {
                    itemList = new ListItem();

                    itemList.setType(ItemType.SERVER);
                    itemList.setName(airVPNServer.getName());
                    itemList.setCountryCode(airVPNServer.getCountryCode());
                    itemList.setIcon(getCountryFlagResource(airVPNServer.getCountryCode()));
                    itemList.setLocation(airVPNServer.getLocation());
                    itemList.setServers(0);
                    itemList.setUsers(airVPNServer.getUsers());
                    itemList.setScore(airVPNServer.getScore());
                    itemList.setBandWidth(airVPNServer.getBandWidth());
                    itemList.setMaxBandWidth(airVPNServer.getMaxBandWidth());
                    itemList.setFavorite(false);
                    itemList.setForbidden(false);
                    itemList.setStartupItem(true);
                    itemList.setWarningOpen(airVPNServer.getWarningOpen());
                    itemList.setWarningClosed(airVPNServer.getWarningClosed());

                    referenceServerItem = itemList;

                    group.addItem(itemList);

                    group.setServers(group.getServers() + 1);
                    group.setUsers(group.getUsers() + airVPNServer.getUsers());
                    group.setBandWidth(group.getBandWidth() + airVPNServer.getBandWidth());
                    group.setMaxBandWidth(group.getMaxBandWidth() + airVPNServer.getMaxBandWidth());
                }
            }
        }

        defaultCountry = settingsManager.getStartupAirVPNCountry();

        if(defaultCountry.isEmpty() == false)
        {
            HashMap<String, AirVPNManifest.CountryStats> countryStats = (HashMap<String, AirVPNManifest.CountryStats>)airVPNManifest.getCountryStats().clone();

            if(countryStats != null)
            {
                AirVPNManifest.CountryStats country = countryStats.get(defaultCountry);

                if(country != null)
                {
                    itemList = new ListItem();

                    itemList.setType(ItemType.COUNTRY);
                    itemList.setName(countryContinent.getCountryName(country.getCountryISOCode()));
                    itemList.setCountryCode(country.getCountryISOCode());
                    itemList.setIcon(getCountryFlagResource(country.getCountryISOCode()));
                    itemList.setLocation("");
                    itemList.setServers(country.getServers());
                    itemList.setUsers(country.getUsers());
                    itemList.setBandWidth(country.getBandWidth());
                    itemList.setMaxBandWidth(country.getMaxBandWidth());
                    itemList.setFavorite(false);
                    itemList.setForbidden(false);
                    itemList.setStartupItem(true);
                    itemList.setWarningOpen("");
                    itemList.setWarningOpen("");

                    group.addItem(itemList);

                    group.setServers(group.getServers() + country.getServers());
                    group.setUsers(group.getUsers() + country.getUsers());
                    group.setBandWidth(group.getBandWidth() + country.getBandWidth());
                    group.setMaxBandWidth(group.getMaxBandWidth() + country.getMaxBandWidth());
                }
            }
        }

        groupList.add(group);

        group = new Group();

        group.setType(GroupType.GROUP);
        group.setIcon(R.drawable.star_icon_on);
        group.setName(getString(R.string.airvpn_server_whitelist));
        group.setCountryCode("");
        group.setServers(0);
        group.setUsers(0);

        serverWhitelist = settingsManager.getAirVPNServerWhitelist();

        if(serverWhitelist != null && serverWhitelist.size() > 0)
        {
            for(String name : serverWhitelist)
            {
                AirVPNServer airVPNServer = airVPNManifest.getServerByName(name);

                if(airVPNServer != null)
                {
                    includeServer = false;

                    if(airVPNServer.getSupportIPv4() == true && defaultIpVersion.equals(SettingsManager.AIRVPN_IP_VERSION_4))
                        includeServer = true;
                    else if(airVPNServer.getSupportIPv6() == true && defaultIpVersion.equals(SettingsManager.AIRVPN_IP_VERSION_6))
                        includeServer = true;

                    if(airVPNServer.isAvailable(cipherList) && includeServer == true)
                    {
                        itemList = new ListItem();

                        itemList.setType(ItemType.SERVER);
                        itemList.setName(airVPNServer.getName());
                        itemList.setCountryCode(airVPNServer.getCountryCode());
                        itemList.setIcon(getCountryFlagResource(airVPNServer.getCountryCode()));
                        itemList.setLocation(airVPNServer.getLocation());
                        itemList.setServers(0);
                        itemList.setUsers(airVPNServer.getUsers());
                        itemList.setScore(airVPNServer.getScore());
                        itemList.setBandWidth(airVPNServer.getBandWidth());
                        itemList.setMaxBandWidth(airVPNServer.getMaxBandWidth());
                        itemList.setFavorite(true);
                        itemList.setForbidden(false);
                        itemList.setStartupItem(false);
                        itemList.setWarningOpen(airVPNServer.getWarningOpen());
                        itemList.setWarningClosed(airVPNServer.getWarningClosed());

                        if(referenceServerItem == null)
                            referenceServerItem = itemList;

                        group.addItem(itemList);

                        group.setServers(group.getServers() + 1);
                        group.setUsers(group.getUsers() + airVPNServer.getUsers());
                        group.setBandWidth(group.getBandWidth() + airVPNServer.getBandWidth());
                        group.setMaxBandWidth(group.getMaxBandWidth() + airVPNServer.getMaxBandWidth());
                    }
                }
            }
        }

        if(countryWhitelist != null && countryWhitelist.size() > 0)
        {
            HashMap<String, AirVPNManifest.CountryStats> countryStats = (HashMap<String, AirVPNManifest.CountryStats>)airVPNManifest.getCountryStats().clone();

            if(countryStats != null)
            {
                for(String countryCode : countryWhitelist)
                {
                    AirVPNManifest.CountryStats country = countryStats.get(countryCode);

                    if(country != null)
                    {
                        itemList = new ListItem();

                        itemList.setType(ItemType.COUNTRY);
                        itemList.setName(countryContinent.getCountryName(country.getCountryISOCode()));
                        itemList.setCountryCode(country.getCountryISOCode());
                        itemList.setIcon(getCountryFlagResource(country.getCountryISOCode()));
                        itemList.setLocation("");
                        itemList.setServers(country.getServers());
                        itemList.setUsers(country.getUsers());
                        itemList.setBandWidth(country.getBandWidth());
                        itemList.setMaxBandWidth(country.getMaxBandWidth());
                        itemList.setFavorite(true);
                        itemList.setForbidden(false);
                        itemList.setStartupItem(false);
                        itemList.setWarningOpen("");
                        itemList.setWarningOpen("");

                        group.addItem(itemList);

                        group.setServers(group.getServers() + country.getServers());
                        group.setUsers(group.getUsers() + country.getUsers());
                        group.setBandWidth(group.getBandWidth() + country.getBandWidth());
                        group.setMaxBandWidth(group.getMaxBandWidth() + country.getMaxBandWidth());
                    }
                }
            }
        }

        group.sortItemList();

        groupList.add(group);

        group = new Group();

        group.setType(GroupType.GROUP);
        group.setIcon(R.drawable.blacklist_icon_on);
        group.setName(getString(R.string.airvpn_server_blacklist));
        group.setCountryCode("");
        group.setServers(0);
        group.setUsers(0);
        group.setBandWidth(0);
        group.setMaxBandWidth(0);

        serverBlacklist = settingsManager.getAirVPNServerBlacklist();

        if(serverBlacklist != null && serverBlacklist.size() > 0)
        {
            for(String name : serverBlacklist)
            {
                AirVPNServer airVPNServer = airVPNManifest.getServerByName(name);

                if(airVPNServer != null)
                {
                    itemList = new ListItem();

                    itemList.setType(ItemType.SERVER);
                    itemList.setName(airVPNServer.getName());
                    itemList.setCountryCode(airVPNServer.getCountryCode());
                    itemList.setIcon(getCountryFlagResource(airVPNServer.getCountryCode()));
                    itemList.setLocation(airVPNServer.getLocation());
                    itemList.setServers(0);
                    itemList.setUsers(airVPNServer.getUsers());
                    itemList.setScore(airVPNServer.getScore());
                    itemList.setBandWidth(airVPNServer.getBandWidth());
                    itemList.setMaxBandWidth(airVPNServer.getMaxBandWidth());
                    itemList.setFavorite(false);
                    itemList.setForbidden(true);
                    itemList.setStartupItem(false);
                    itemList.setWarningOpen(airVPNServer.getWarningOpen());
                    itemList.setWarningClosed(airVPNServer.getWarningClosed());

                    group.addItem(itemList);

                    group.setServers(group.getServers() + 1);
                    group.setUsers(group.getUsers() + airVPNServer.getUsers());
                    group.setBandWidth(group.getBandWidth() + airVPNServer.getBandWidth());
                    group.setMaxBandWidth(group.getMaxBandWidth() + airVPNServer.getMaxBandWidth());
                }
            }
        }

        if(countryBlacklist != null && countryBlacklist.size() > 0)
        {
            HashMap<String, AirVPNManifest.CountryStats> countryStats = (HashMap<String, AirVPNManifest.CountryStats>)airVPNManifest.getCountryStats().clone();

            if(countryStats != null)
            {
                for(String countryCode : countryBlacklist)
                {
                    AirVPNManifest.CountryStats country = countryStats.get(countryCode);

                    if(country != null)
                    {
                        itemList = new ListItem();

                        itemList.setType(ItemType.COUNTRY);
                        itemList.setName(countryContinent.getCountryName(country.getCountryISOCode()));
                        itemList.setCountryCode(country.getCountryISOCode());
                        itemList.setIcon(getCountryFlagResource(country.getCountryISOCode()));
                        itemList.setLocation("");
                        itemList.setServers(country.getServers());
                        itemList.setUsers(country.getUsers());
                        itemList.setBandWidth(country.getBandWidth());
                        itemList.setMaxBandWidth(country.getMaxBandWidth());
                        itemList.setFavorite(false);
                        itemList.setForbidden(true);
                        itemList.setStartupItem(false);
                        itemList.setWarningOpen("");
                        itemList.setWarningClosed("");

                        group.addItem(itemList);

                        group.setServers(group.getServers() + country.getServers());
                        group.setUsers(group.getUsers() + country.getUsers());
                        group.setBandWidth(group.getBandWidth() + country.getBandWidth());
                        group.setMaxBandWidth(group.getMaxBandWidth() + country.getMaxBandWidth());
                    }
                }
            }
        }

        group.sortItemList();

        groupList.add(group);

        airVPNGroup = new Group();

        airVPNGroup.setType(GroupType.HEADER);
        airVPNGroup.setIcon(R.drawable.icon);
        airVPNGroup.setName(getString(R.string.airvpn_server_countries));
        airVPNGroup.setCountryCode("");
        airVPNGroup.setServers(0);
        airVPNGroup.setUsers(0);
        airVPNGroup.setBandWidth(0);
        airVPNGroup.setMaxBandWidth(0);

        groupList.add(airVPNGroup);

        if(airVPNManifest.getCountryStats() != null)
        {
            ArrayList<String> countryNames = new ArrayList<String>();

            HashMap<String, AirVPNManifest.CountryStats> countryStats = (HashMap<String, AirVPNManifest.CountryStats>)airVPNManifest.getCountryStats().clone();

            if(countryStats != null)
            {
                for(Map.Entry<String, AirVPNManifest.CountryStats> pair : countryStats.entrySet())
                {
                    String name = countryContinent.getCountryName(pair.getKey());

                    if(name != null && !name.isEmpty())
                        countryNames.add(name);
                }

                Collator collator = Collator.getInstance(Locale.getDefault());

                collator.setStrength(Collator.PRIMARY);

                Collections.sort(countryNames, collator);

                for(String name : countryNames)
                {
                    AirVPNManifest.CountryStats country = countryStats.get(countryContinent.getCountryCode(name));

                    group = new Group();

                    group.setType(GroupType.COUNTRY);
                    group.setIcon(getCountryFlagResource(country.getCountryISOCode()));
                    group.setName(countryContinent.getCountryName(country.getCountryISOCode()));
                    group.setCountryCode(country.getCountryISOCode());
                    group.setServers(country.getServers());
                    group.setUsers(country.getUsers());
                    group.setBandWidth(country.getBandWidth());
                    group.setMaxBandWidth(country.getMaxBandWidth());

                    ArrayList<AirVPNServer> serverList = airVPNManifest.getServerListByCountry(country.getCountryISOCode());

                    if(serverList != null)
                    {
                        countedServers = 0;
                        countedUsers = 0;
                        countedBandWidth = 0;
                        countedMaxBandWidth = 0;

                        for(int i = 0; i < serverList.size(); i++)
                        {
                            AirVPNServer airVPNServer = serverList.get(i);

                            includeServer = false;

                            if(airVPNServer.getSupportIPv4() == true && defaultIpVersion.equals(SettingsManager.AIRVPN_IP_VERSION_4))
                                includeServer = true;
                            else if(airVPNServer.getSupportIPv6() == true && defaultIpVersion.equals(SettingsManager.AIRVPN_IP_VERSION_6))
                                includeServer = true;

                            if(airVPNServer.isAvailable(cipherList) && includeServer == true)
                            {
                                if(searchKey.isEmpty())
                                    includeServer = true;
                                else
                                {
                                    includeServer = false;

                                    if(airVPNServer.getName().toLowerCase(Locale.US).contains(searchKey.toLowerCase(Locale.US)))
                                        includeServer = true;

                                    if(airVPNServer.getLocation().toLowerCase(Locale.US).contains(searchKey.toLowerCase(Locale.US)))
                                        includeServer = true;

                                    if(countryContinent.getCountryName(airVPNServer.getCountryCode()).toLowerCase(Locale.US).contains(searchKey.toLowerCase(Locale.US)))
                                        includeServer = true;
                                }

                                if(includeServer)
                                {
                                    itemList = new ListItem();

                                    itemList.setType(ItemType.SERVER);
                                    itemList.setName(airVPNServer.getName());
                                    itemList.setCountryCode(airVPNServer.getCountryCode());
                                    itemList.setIcon(getCountryFlagResource(airVPNServer.getCountryCode()));
                                    itemList.setLocation(airVPNServer.getLocation());
                                    itemList.setServers(0);
                                    itemList.setUsers(airVPNServer.getUsers());
                                    itemList.setScore(airVPNServer.getScore());
                                    itemList.setBandWidth(airVPNServer.getBandWidth());
                                    itemList.setMaxBandWidth(airVPNServer.getMaxBandWidth());

                                    if(serverWhitelist.contains(itemList.getName()))
                                        itemList.setFavorite(true);
                                    else
                                        itemList.setFavorite(false);

                                    if(serverBlacklist.contains(itemList.getName()))
                                        itemList.setForbidden(true);
                                    else
                                        itemList.setForbidden(false);

                                    if(itemList.getName().equals(defaultServer))
                                        itemList.setStartupItem(true);
                                    else
                                        itemList.setStartupItem(false);

                                    itemList.setWarningOpen(airVPNServer.getWarningOpen());
                                    itemList.setWarningClosed(airVPNServer.getWarningClosed());

                                    if(referenceServerItem == null)
                                        referenceServerItem = itemList;

                                    group.addItem(itemList);

                                    countryGroupAdded = true;

                                    if(cipherList != null)
                                    {
                                        countedServers++;
                                        countedUsers += airVPNServer.getUsers();
                                        countedBandWidth += airVPNServer.getBandWidth();
                                        countedMaxBandWidth += airVPNServer.getMaxBandWidth();
                                    }
                                }
                            }
                        }

                        if(cipherList != null)
                        {
                            group.setServers(countedServers);
                            group.setUsers(countedUsers);
                            group.setBandWidth(countedBandWidth);
                            group.setMaxBandWidth(countedMaxBandWidth);
                        }

                        group.sortItemList();
                    }

                    if(group.getItemList().size() > 0)
                    {
                        airVPNGroup.setServers(airVPNGroup.getServers() + group.getServers());
                        airVPNGroup.setUsers(airVPNGroup.getUsers() + group.getUsers());
                        airVPNGroup.setBandWidth(airVPNGroup.getBandWidth() + group.getBandWidth());
                        airVPNGroup.setMaxBandWidth(airVPNGroup.getMaxBandWidth() + group.getMaxBandWidth());

                        groupList.add(group);
                    }
                }
            }
        }

        if(!countryGroupAdded)
            supportTools.infoDialog(R.string.airvpn_server_not_found, true);
    }

    private void startServerConnection(final ListItem server)
    {
        if(server.getType() != ItemType.SERVER)
            return;

        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                if(server.isForbidden())
                {
                    supportTools.infoDialog(R.string.airvpn_server_forbidden_connection, true);

                    return;
                }

                if(airVPNUser.validateUserLogin(getActivity()) == false)
                {
                    pendingServerConnection = server;

                    return;
                }

                connectServer(server, ConnectionMode.SERVER, "");
            }
        };

        SupportTools.runOnUiActivity(getActivity(), uiRunnable);
    }

    private void connectServer(ListItem server, ConnectionMode connectionMode, String countryCode)
    {
        boolean doCountryConnection = false;
        AirVPNManifest.Mode manifestMode = null;
        String progressMessage = "", serverName = "", serverDescription = "", message = "", logMessage = "", countryDescription = "";
        HashMap<String, String> vpnProfile = null;
        int port = 0;

        pendingServerConnection = null;

        if(server.getType() != ItemType.SERVER || settingsManager == null || airVPNUser == null)
            return;

        if(supportTools.isNetworkConnectionActive() == false)
        {
            EddieLogger.error("ConnectAirVPNServerFragment.connectServer(): Network connection is not available");

            supportTools.infoDialog(R.string.network_is_not_available, true);

            return;
        }

        if(supportTools.waitForManifest(true) == false)
        {
            EddieLogger.error("ConnectAirVPNServerFragment.connectServer(): AirVPN Manifest is not available");

            supportTools.infoDialog(R.string.manifest_download_error, true);

            return;
        }

        AirVPNServer airVPNServer = airVPNManifest.getServerByName(server.getName());

        if(airVPNServer == null)
        {
            EddieLogger.error("ConnectAirVPNServerFragment.connectServer(): airVPNServer is null");

            return;
        }

        if(connectionMode == ConnectionMode.SERVER)
        {
            doCountryConnection = false;

            serverDescription = airVPNManifest.getFullServerDescription(airVPNServer.getName());

            serverName = airVPNServer.getName();
        }
        else
        {
            doCountryConnection = true;

            if(countryCode.isEmpty())
                countryCode = airVPNServer.getCountryCode();

            if(connectionMode == ConnectionMode.COUNTRY)
                countryDescription = countryContinent.getCountryName(airVPNServer.getCountryCode());
            else
                countryDescription = countryContinent.getContinentName(countryCode);

            serverDescription = String.format(getResources().getString(R.string.airvpn_best_country_server_cap), countryDescription);

            serverName = countryCode;
        }

        vpnProfile = createVpnProfile(serverName, doCountryConnection, false);

        if(vpnProfile.get("status").equals("ok") == false)
        {
            EddieLogger.error("ConnectAirVPNServerFragment.connectServer(): vpnProfile is empty");

            supportTools.infoDialog(R.string.connection_error, true);

            return;
        }

        if(vpnProfile.get("host").isEmpty() == true)
        {
            EddieLogger.error("ConnectAirVPNServerFragment.connectServer(): host is empty");

            return;
        }

        profileInfo = new HashMap<String, String>();

        profileInfo.put("mode", String.format(Locale.getDefault(), "%d", VPN.ConnectionMode.AIRVPN_SERVER.getValue()));
        profileInfo.put("vpn_type", settingsManager.getAirVPNDefaultVPNType());
        profileInfo.put("name", "airvpn_server_connect");
        profileInfo.put("profile", "airvpn_server_connect");
        profileInfo.put("status", "ok");
        profileInfo.put("description", serverDescription);
        profileInfo.put("server", vpnProfile.get("host"));
        profileInfo.put("port", vpnProfile.get("port"));
        profileInfo.put("protocol", vpnProfile.get("protocol"));

        try
        {
            port = Integer.parseInt(vpnProfile.get("port"));
        }
        catch(NumberFormatException e)
        {
            port = -1;
        }

        if(connectionMode == ConnectionMode.SERVER)
            progressMessage = String.format(getResources().getString(R.string.airvpn_server_try_connection), airVPNServer.getName(), airVPNServer.getLocation(), countryContinent.getCountryName(airVPNServer.getCountryCode()), vpnProfile.get("type"), vpnProfile.get("protocol"), port);
        else
            progressMessage = String.format(getResources().getString(R.string.airvpn_country_try_connection), countryDescription, vpnProfile.get("type"), vpnProfile.get("protocol"), port);

        if(vpnManager.isVpnConnectionStarted() == true || vpnManager.isVpnConnectionPaused() == true || vpnManager.isVpnConnectionLocked() == true)
        {
            HashMap<String, String> currentProfile = vpnManager.vpn().getProfileInfo();

            serverDescription = "";

            if(currentProfile != null)
            {
                if(!profileInfo.get("server").isEmpty())
                    serverDescription = vpnManager.vpn().getServerDescription(false);
                else
                    serverDescription = "";
            }

            message = String.format(Locale.getDefault(), getResources().getString(R.string.airvpn_server_connection_warning), serverDescription);
            message += " ";

            if(connectionMode == ConnectionMode.SERVER)
                message += String.format(Locale.getDefault(), getResources().getString(R.string.airvpn_server_confirm_connection), server.getName(), server.getLocation(), countryContinent.getCountryName(server.getCountryCode()));
            else
                message += String.format(Locale.getDefault(), getResources().getString(R.string.airvpn_country_confirm_connection), countryDescription);

            if(supportTools.confirmationDialog(message))
            {
                try
                {
                    vpnManager.stopConnection();

                    vpnManager.vpn().setPendingProfileInfo(profileInfo);
                    vpnManager.vpn().setPendingVpnProfile(vpnProfile.get("profile"));
                    vpnManager.vpn().setPendingProgressMessage(progressMessage);

                    return;
                }
                catch(Exception e)
                {
                    EddieLogger.error("ConnectAirVPNServerFragment.connectServer(): vpnManager.stop() exception: %s", e.getMessage());
                }
            }
            else
                return;
        }
        else
        {
            if(settingsManager.isAirVPNConfirmServerConnectionEnabled() == true)
            {
                if(connectionMode == ConnectionMode.SERVER)
                    message = String.format(Locale.getDefault(), getResources().getString(R.string.airvpn_server_confirm_connection), server.getName(), server.getLocation(), countryContinent.getCountryName(server.getCountryCode()));
                else
                    message = String.format(Locale.getDefault(), getResources().getString(R.string.airvpn_country_confirm_connection), countryDescription);

                if(!supportTools.confirmationDialog(message))
                    return;
            }
        }

        if(connectionMode == ConnectionMode.SERVER)
            logMessage = String.format(Locale.getDefault(), "Trying connection to AirVPN server %s in %s, %s", server.getName(), server.getLocation(), countryContinent.getCountryName(server.getCountryCode()));
        else
            logMessage = String.format(Locale.getDefault(), "Start connection to best AirVPN server in %s", countryDescription);

        logMessage += String.format(Locale.getDefault(), " - %s, Protocol %s, Port %d", settingsManager.getAirVPNDefaultVPNType(), vpnProfile.get("protocol"), port);

        EddieLogger.info(logMessage);

        if(settingsManager.getAirVPNDefaultVPNType().equals(SettingsManager.VPN_TYPE_OPENVPN) == true)
            manifestMode = EddieApplication.airVPNManifest().getMode(settingsManager.getAirVPNOpenVPNMode());
        else if(settingsManager.getAirVPNDefaultVPNType().equals(SettingsManager.VPN_TYPE_WIREGUARD) == true)
            manifestMode = EddieApplication.airVPNManifest().getMode(settingsManager.getAirVPNWireGuardMode());
        else
            manifestMode = null;

        if(manifestMode != null)
            EddieLogger.info(String.format(Locale.getDefault(), "Using %s manifest mode '%s'", manifestMode.getVpnType(), manifestMode.getTitle()));

        EddieLogger.info(String.format(Locale.getDefault(), "Using user key '%s'", vpnProfile.get("userKey")));

        supportTools.showConnectionProgressDialog(progressMessage);

        vpnManager.vpn().setType(VPN.Type.fromString(settingsManager.getAirVPNDefaultVPNType()));

        vpnManager.vpn().setProfileInfo(profileInfo);

        vpnManager.vpn().setConnectionMode(VPN.ConnectionMode.AIRVPN_SERVER);

        vpnManager.vpn().setConnectionModeDescription(getResources().getString(R.string.conn_type_airvpn_server));

        vpnManager.vpn().setUserProfileDescription(vpnProfile.get("userKey"));

        vpnManager.vpn().setUserName(airVPNUser.getUserName());

        vpnManager.vpn().setVpnProfile(vpnProfile.get("profile"));

        startConnection(vpnProfile.get("profile"), settingsManager.getAirVPNDefaultVPNType());
    }

    private void startConnection(String openVPNProfile, String vpnType)
    {
        String profileString = "";

        connectionInProgress = false;

        if(vpnManager == null)
        {
            EddieLogger.error("ConnectAirVPNServerFragment.startConnection(): vpnManager is null");

            return;
        }

        if(openVPNProfile.equals(""))
            return;

        vpnManager.clearProfile();

        vpnManager.setProfile(openVPNProfile);

        if(vpnType.equals(SettingsManager.VPN_TYPE_OPENVPN) == true)
        {
            profileString = settingsManager.getOvpn3CustomDirectives().trim();

            if(profileString.length() > 0)
                vpnManager.addProfileString(profileString);
        }

        vpnManager.startConnection();

        connectionInProgress = true;
    }

    private HashMap<String, String> createVpnProfile(String server)
    {
        return createVpnProfile(server, false, false, "", "", "", "", -1, "", "");
    }

    private HashMap<String, String> createVpnProfile(String server, boolean create_country_profile, boolean use_country_fqdn)
    {
        return createVpnProfile(server, create_country_profile, use_country_fqdn, "", "", "", "", -1, "", "");
    }

    private HashMap<String, String> createVpnProfile(String server, boolean create_country_profile, boolean use_country_fqdn, String type)
    {
        return createVpnProfile(server, create_country_profile, use_country_fqdn, type, "", "", "", -1, "", "");
    }

    private HashMap<String, String> createVpnProfile(String serverName, boolean create_country_profile, boolean use_country_fqdn, String type, String userKey, String family, String proto, int p, String tls, String cipher)
    {
        AirVPNServerProvider airVPNServerProvider = null;
        ArrayList<AirVPNServer> airVPNServerList;
        ArrayList<String> list = new ArrayList<String>();
        AirVPNManifest.Mode manifestMode = null;
        int entry = 0, port = 0;
        boolean connectIPv6 = false, mode6to4 = false;
        String serverIp = "", serverProfile, countryCode = "", protocol = "";
        String userKeyName = "", tlsMode = "", vpnType = "", ipVersion = "", allowedIPs = "";
        HashMap<Integer, String> serverEntryIP = null;
        HashMap<String, String> profile = new HashMap<String, String>();

        profile.put("status", "error");

        if(airVPNUser == null || serverName.isEmpty() == true)
            return profile;

        vpnType = (type.isEmpty() ? settingsManager.getAirVPNDefaultVPNType() : type);

        userKeyName = (userKey.isEmpty() ? airVPNUser.getCurrentKey() : userKey);

        if(vpnType.equals(SettingsManager.VPN_TYPE_OPENVPN) == true)
        {
            if(settingsManager.getAirVPNOpenVPNMode().equals(SettingsManager.AIRVPN_OPENVPN_MODE_DEFAULT) == false)
            {
                manifestMode = EddieApplication.airVPNManifest().getMode(settingsManager.getAirVPNOpenVPNMode());

                tlsMode = manifestMode.getTlsMode();
            }
            else
                tlsMode = (tls.isEmpty() ? settingsManager.getAirVPNDefaultTLSMode() : tls);
        }
        else if(vpnType.equals(SettingsManager.VPN_TYPE_WIREGUARD) == true)
            tlsMode = SettingsManager.OPENVPN_TLS_MODE_AUTH;
        else
            tlsMode = (tls.isEmpty() ? settingsManager.getAirVPNDefaultTLSMode() : tls);

        ipVersion = (family.isEmpty() ? settingsManager.getAirVPNDefaultIPVersion() : family);

        switch(ipVersion)
        {
            case SettingsManager.AIRVPN_IP_VERSION_4:
            {
                connectIPv6 = false;
                mode6to4 = false;
            }
            break;

            case SettingsManager.AIRVPN_IP_VERSION_6:
            {
                connectIPv6 = true;
                mode6to4 = false;
            }
            break;

            case SettingsManager.AIRVPN_IP_VERSION_4_OVER_6:
            {
                connectIPv6 = true;
                mode6to4 = true;
            }
            break;

            default:
            {
                connectIPv6 = false;
                mode6to4 = false;
            }
            break;
        }

        if(vpnType.equals(SettingsManager.VPN_TYPE_OPENVPN) == true)
        {
            if(settingsManager.getAirVPNOpenVPNMode().equals(SettingsManager.AIRVPN_OPENVPN_MODE_DEFAULT) == false)
            {
                manifestMode = EddieApplication.airVPNManifest().getMode(settingsManager.getAirVPNOpenVPNMode());

                entry = manifestMode.getEntryIndex();
            }
            else
            {
                if(tlsMode.equals(SettingsManager.OPENVPN_TLS_MODE_AUTH) == true)
                    entry = 0;
                else
                    entry = 2;
            }
        }
        else if(vpnType.equals(SettingsManager.VPN_TYPE_WIREGUARD) == true)
        {
            if(settingsManager.getAirVPNWireGuardMode().equals(SettingsManager.AIRVPN_WIREGUARD_MODE_DEFAULT) == false)
            {
                manifestMode = EddieApplication.airVPNManifest().getMode(settingsManager.getAirVPNWireGuardMode());

                entry = manifestMode.getEntryIndex();
            }
            else
                entry = 0;
        }
        else
            entry = 0;

        if(create_country_profile == false)
        {
            AirVPNServer airVPNServer = airVPNManifest.getServerByName(serverName);

            if(airVPNServer == null)
                return profile;

            if(ipVersion.equals(SettingsManager.AIRVPN_IP_VERSION_6) == true)
                serverEntryIP = airVPNServer.getEntryIPv6();
            else
                serverEntryIP = airVPNServer.getEntryIPv4();

            if(serverEntryIP == null)
            {
                EddieLogger.error("ConnectAirVPNServerFragment.createVpnProfile(): serverEntryIP is null.");

                return profile;
            }

            serverIp = serverEntryIP.get(entry);
        }
        else
        {
            countryCode = serverName.toUpperCase();

            if(countryContinent.isContinent(countryCode) == true)
            {
                if(countryCode.length() <= 3 && countryCode.equalsIgnoreCase(CountryContinent.EARTH) == false)
                    countryCode = countryContinent.getContinentName(countryCode);
            }
            else
            {
                if(countryContinent.getCountryName(countryCode) == "")
                    countryCode = countryContinent.getCountryCode(countryCode);
            }

            if(countryCode == "")
                return profile;

            if(use_country_fqdn == true)
                serverIp = supportTools.getAirVpnCountryHostname(countryCode, tlsMode, connectIPv6);
            else
            {
                airVPNServerProvider = new AirVPNServerProvider();

                if(airVPNServerProvider == null)
                {
                    EddieLogger.error("createVpnProfile(): Cannot create AirVPNServerProvider object.");

                    return profile;
                }

                airVPNServerProvider.reset();

                airVPNServerProvider.setUserCountry(airVPNUser.getUserCountry());

                list.clear();

                if(countryContinent.isContinent(serverName) == false)
                    list.add(countryCode);
                else
                {
                    if(serverName.length() != 3)
                        serverName = countryContinent.getContinentCode(serverName);

                    list.add(serverName);
                }

                airVPNServerProvider.setCountryWhitelist(list);

                airVPNServerProvider.setTlsMode(((tlsMode.equals(SettingsManager.OPENVPN_TLS_MODE_AUTH) == true) ? AirVPNServerProvider.TLSMode.TLS_AUTH : AirVPNServerProvider.TLSMode.TLS_CRYPT));

                switch(ipVersion)
                {
                    case SettingsManager.AIRVPN_IP_VERSION_4:
                    {
                        airVPNServerProvider.setSupportIPv4(true);
                        airVPNServerProvider.setSupportIPv6(false);
                    }
                    break;

                    case SettingsManager.AIRVPN_IP_VERSION_6:
                    {
                        airVPNServerProvider.setSupportIPv4(true);
                        airVPNServerProvider.setSupportIPv6(true);
                    }
                    break;

                    case SettingsManager.AIRVPN_IP_VERSION_4_OVER_6:
                    {
                        airVPNServerProvider.setSupportIPv4(true);
                        airVPNServerProvider.setSupportIPv6(true);
                    }
                    break;

                    default:
                    {
                        airVPNServerProvider.setSupportIPv4(true);
                        airVPNServerProvider.setSupportIPv6(false);
                    }
                    break;
                }

                airVPNServerList = airVPNServerProvider.getFilteredServerList();

                if(airVPNServerList.size() > 0)
                {
                    if(ipVersion.equals(SettingsManager.AIRVPN_IP_VERSION_6) == true)
                        serverEntryIP = airVPNServerList.get(0).getEntryIPv6();
                    else
                        serverEntryIP = airVPNServerList.get(0).getEntryIPv4();
                }
                else
                    return profile;

                if(serverEntryIP == null)
                {
                    EddieLogger.error("ConnectAirVPNServerFragment.createVpnProfile(): serverEntryIP is null.");

                    return profile;
                }

                serverIp = serverEntryIP.get(entry);
            }
        }

        if(vpnType.equals(SettingsManager.VPN_TYPE_OPENVPN) == true)
        {
            if(settingsManager.getAirVPNOpenVPNMode().equals(SettingsManager.AIRVPN_OPENVPN_MODE_DEFAULT) == false)
            {
                manifestMode = EddieApplication.airVPNManifest().getMode(settingsManager.getAirVPNOpenVPNMode());

                port = manifestMode.getPort();
                protocol = manifestMode.getProtocol();
            }
            else
            {
                if(p <= 0 || p >= 65535)
                    port = settingsManager.getAirVPNDefaultOpenVPNPort();
                else
                    port = p;

                if(proto.isEmpty() == true)
                    protocol = settingsManager.getAirVPNDefaultOpenVPNProtocol();
                else
                    protocol = "";
            }

            if(cipher.isEmpty() == true)
                cipher = settingsManager.getAirVPNOpenVPNCipher();

            serverProfile = airVPNUser.getOpenVPNProfile(userKeyName, serverIp, port, protocol, tlsMode, cipher, connectIPv6, mode6to4, false, "");
        }
        else if(vpnType.equals(SettingsManager.VPN_TYPE_WIREGUARD) == true)
        {
            allowedIPs = "";

            if(settingsManager.getAirVPNWireGuardMode().equals(SettingsManager.AIRVPN_WIREGUARD_MODE_DEFAULT) == false)
            {
                manifestMode = EddieApplication.airVPNManifest().getMode(settingsManager.getAirVPNWireGuardMode());

                port = manifestMode.getPort();
                protocol = manifestMode.getProtocol();
            }
            else
            {
                if(p <= 0 || p >= 65535)
                    port = settingsManager.getAirVPNDefaultWireGuardPort();
                else
                    port = p;

                protocol = SettingsManager.AIRVPN_PROTOCOL_UDP;
            }

            if(connectIPv6 == false || mode6to4 == true)
                allowedIPs = "0.0.0.0/0";

            if(connectIPv6 == true || mode6to4 == true)
            {
                if(allowedIPs.isEmpty() == false)
                    allowedIPs += ", ";

                allowedIPs += "::/0";
            }

            serverProfile = airVPNUser.getWireGuardProfile(userKeyName, serverIp, port, 0, 0, allowedIPs, 15, connectIPv6, mode6to4, false, "");
        }
        else
            serverProfile = "ERROR: Unknown vpn type";

        profile.put("status", "ok");
        profile.put("type", vpnType);
        profile.put("userKey", userKeyName);
        profile.put("host", serverIp);
        profile.put("port", String.valueOf(port));
        profile.put("protocol", protocol);
        profile.put("tlsMode", tlsMode);
        profile.put("mode", ipVersion);
        profile.put("profile", serverProfile);

        return profile;
    }

    public class AirVPNServerExpandableListAdapter extends BaseExpandableListAdapter
    {
        private Context context;
        private ArrayList<Group> groupList;

        private class GroupListViewHolder
        {
            public ImageView imgIndicator;
            public ImageView imgIcon;
            public ImageView imgLoad;
            public TextView txtTitle;
            public TextView txtServers;
            public TextView txtLoad;
            public TextView txtUsers;
            public TextView txtBandWidth;
            public TextView txtMaxBandWidth;
        }

        private class CountryListViewHolder extends Object
        {
            public ImageView imgIndicator;
            public ImageView imgFlag;
            public ImageView imgLoad;
            public ImageView imgFavorite;
            public TextView txtName;
            public TextView txtServers;
            public TextView txtLoad;
            public TextView txtUsers;
            public TextView txtBandWidth;
            public TextView txtMaxBandWidth;
        }

        private class ItemListViewHolder extends Object
        {
            public ItemType type;
            public ImageView imgFlag;
            public ImageView imgLoad;
            public ImageView imgFavorite;
            public TextView txtName;
            public TextView txtLocation;
            public TextView txtLoad;
            public TextView txtScore;
            public TextView txtBandWidth;
            public TextView txtMaxBandWidth;
            public TextView txtUsers;
            public TextView txtServers;
            public LinearLayout llServerWarning;
            public TextView txtServerWarning;
        }

        public AirVPNServerExpandableListAdapter(Context c, ArrayList<Group> g)
        {
            context = c;
            groupList = g;
        }

        public void dataSet(ArrayList<Group> g)
        {
            groupList = g;

            if(groupList != null)
                notifyDataSetChanged();
        }

        @Override
        public Object getGroup(int groupPosition)
        {
            return groupList.get(groupPosition);
        }

        @Override
        public int getGroupCount()
        {
            return groupList.size();
        }

        @Override
        public long getGroupId(int groupPosition)
        {
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent)
        {
            Group group = (Group)getGroup(groupPosition);
            GroupListViewHolder groupListViewHolder = null;
            CountryListViewHolder countryListViewHolder = null;
            int drawable = R.drawable.icon;
            GroupType groupType = group.getType();

            switch(groupType)
            {
                case HEADER:
                case GROUP:
                {
                    if(convertView != null)
                    {
                        if(convertView.getTag() instanceof GroupListViewHolder)
                            groupListViewHolder = (GroupListViewHolder)convertView.getTag();
                        else
                            groupListViewHolder = null;
                    }

                    if(groupListViewHolder == null)
                    {
                        groupListViewHolder = new GroupListViewHolder();

                        LayoutInflater infalInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                        convertView = infalInflater.inflate(R.layout.airvpn_server_listview_group_item, null);

                        groupListViewHolder.imgIcon = (ImageView)convertView.findViewById(R.id.img_group_icon);
                        groupListViewHolder.imgIndicator = (ImageView)convertView.findViewById(R.id.img_group_indicator);
                        groupListViewHolder.imgLoad = (ImageView)convertView.findViewById(R.id.img_group_load);
                        groupListViewHolder.txtTitle = (TextView)convertView.findViewById(R.id.txt_group_title);
                        groupListViewHolder.txtServers = (TextView)convertView.findViewById(R.id.txt_group_servers);
                        groupListViewHolder.txtLoad = (TextView)convertView.findViewById(R.id.txt_group_load);
                        groupListViewHolder.txtUsers = (TextView)convertView.findViewById(R.id.txt_group_users);
                        groupListViewHolder.txtBandWidth = (TextView)convertView.findViewById(R.id.txt_group_bandwidth);
                        groupListViewHolder.txtMaxBandWidth = (TextView)convertView.findViewById(R.id.txt_group_maxbandwidth);

                        convertView.setTag(groupListViewHolder);
                    }

                    groupListViewHolder.txtServers.setTypeface(null, Typeface.NORMAL);
                    groupListViewHolder.txtServers.setText(String.format(Locale.getDefault(), "%d", group.getServers()));

                    groupListViewHolder.imgLoad.setImageDrawable(context.getDrawable(getLoadIconResource(group.getLoad())));

                    groupListViewHolder.txtLoad.setTypeface(null, Typeface.NORMAL);
                    groupListViewHolder.txtLoad.setText(String.format(Locale.getDefault(), "%d%%", group.getLoad()));

                    groupListViewHolder.txtUsers.setTypeface(null, Typeface.NORMAL);
                    groupListViewHolder.txtUsers.setText(String.format(Locale.getDefault(), "%d", group.getUsers()));

                    groupListViewHolder.txtBandWidth.setTypeface(null, Typeface.NORMAL);
                    groupListViewHolder.txtBandWidth.setText(supportTools.formatTransferRate(group.getEffectiveBandWidth(), false));

                    groupListViewHolder.txtMaxBandWidth.setTypeface(null, Typeface.NORMAL);
                    groupListViewHolder.txtMaxBandWidth.setText(supportTools.formatTransferRate(group.getMaxBandWidth() * SupportTools.ONE_DECIMAL_MEGA, false));

                    if(groupType == GroupType.GROUP)
                    {
                        if(isExpanded)
                            drawable = R.drawable.arrow_down;
                        else
                            drawable = R.drawable.arrow_right;

                        groupListViewHolder.imgIndicator.setVisibility(View.VISIBLE);
                        groupListViewHolder.imgIndicator.setImageDrawable(context.getDrawable(drawable));
                    }
                    else
                    {
                        groupListViewHolder.imgIndicator.setVisibility(View.GONE);
                        groupListViewHolder.imgIndicator.setImageResource(android.R.color.transparent);
                    }

                    groupListViewHolder.imgIcon.setImageDrawable(context.getDrawable(group.getIcon()));

                    groupListViewHolder.txtTitle.setTypeface(null, Typeface.BOLD);
                    groupListViewHolder.txtTitle.setText(group.getName());
                }
                break;

                case COUNTRY:
                {
                    if(convertView != null)
                    {
                        if(convertView.getTag() instanceof CountryListViewHolder)
                            countryListViewHolder = (CountryListViewHolder)convertView.getTag();
                        else
                            countryListViewHolder = null;
                    }

                    if(countryListViewHolder == null)
                    {
                        countryListViewHolder = new CountryListViewHolder();

                        LayoutInflater infalInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                        convertView = infalInflater.inflate(R.layout.airvpn_server_listview_country_group_item, null);

                        countryListViewHolder.imgIndicator = (ImageView)convertView.findViewById(R.id.img_country_indicator);
                        countryListViewHolder.imgFlag = (ImageView)convertView.findViewById(R.id.img_country_flag);
                        countryListViewHolder.imgLoad = (ImageView)convertView.findViewById(R.id.img_country_load);
                        countryListViewHolder.imgFavorite = (ImageView)convertView.findViewById(R.id.img_country_favorite);
                        countryListViewHolder.txtName = (TextView)convertView.findViewById(R.id.txt_country_name);
                        countryListViewHolder.txtServers = (TextView)convertView.findViewById(R.id.txt_country_servers);
                        countryListViewHolder.txtLoad = (TextView)convertView.findViewById(R.id.txt_country_load);
                        countryListViewHolder.txtUsers = (TextView)convertView.findViewById(R.id.txt_country_users);
                        countryListViewHolder.txtBandWidth = (TextView)convertView.findViewById(R.id.txt_country_bandwidth);
                        countryListViewHolder.txtMaxBandWidth = (TextView)convertView.findViewById(R.id.txt_country_maxbandwidth);

                        convertView.setTag(countryListViewHolder);
                    }

                    if(isExpanded)
                        drawable = R.drawable.arrow_down;
                    else
                        drawable = R.drawable.arrow_right;

                    countryListViewHolder.imgIndicator.setImageDrawable(context.getDrawable(drawable));

                    countryListViewHolder.imgFlag.setImageDrawable(context.getDrawable(group.getIcon()));

                    countryListViewHolder.txtName.setTypeface(null, Typeface.BOLD);
                    countryListViewHolder.txtName.setText(group.getName());

                    countryListViewHolder.txtServers.setTypeface(null, Typeface.NORMAL);
                    countryListViewHolder.txtServers.setText(String.format(Locale.getDefault(), "%d", group.getServers()));

                    countryListViewHolder.imgLoad.setImageDrawable(context.getDrawable(getLoadIconResource(group.getLoad())));

                    countryListViewHolder.txtLoad.setTypeface(null, Typeface.NORMAL);
                    countryListViewHolder.txtLoad.setText(String.format(Locale.getDefault(), "%d%%", group.getLoad()));

                    countryListViewHolder.txtUsers.setTypeface(null, Typeface.NORMAL);
                    countryListViewHolder.txtUsers.setText(String.format(Locale.getDefault(), "%d", group.getUsers()));

                    countryListViewHolder.txtBandWidth.setTypeface(null, Typeface.NORMAL);
                    countryListViewHolder.txtBandWidth.setText(supportTools.formatTransferRate(group.getEffectiveBandWidth(), false));

                    countryListViewHolder.txtMaxBandWidth.setTypeface(null, Typeface.NORMAL);
                    countryListViewHolder.txtMaxBandWidth.setText(supportTools.formatTransferRate(group.getMaxBandWidth() * SupportTools.ONE_DECIMAL_MEGA, false));

                    if(group.getCountryCode().equals(defaultCountry))
                    {
                        countryListViewHolder.imgFavorite.setVisibility(View.VISIBLE);

                        countryListViewHolder.imgFavorite.setImageDrawable(context.getDrawable(R.drawable.default_item));
                    }
                    else if(countryWhitelist.contains(group.getCountryCode()))
                    {
                        countryListViewHolder.imgFavorite.setVisibility(View.VISIBLE);

                        countryListViewHolder.imgFavorite.setImageDrawable(context.getDrawable(R.drawable.star_icon_on));
                    }
                    else if(countryBlacklist.contains(group.getCountryCode()))
                    {
                        countryListViewHolder.imgFavorite.setVisibility(View.VISIBLE);

                        countryListViewHolder.imgFavorite.setImageDrawable(context.getDrawable(R.drawable.blacklist_icon_on));
                    }
                    else
                    {
                        countryListViewHolder.imgFavorite.setVisibility(View.GONE);
                        countryListViewHolder.imgFavorite.setImageResource(android.R.color.transparent);
                    }
                }
                break;
            }

            return convertView;
        }

        @Override
        public Object getChild(int groupPosition, int childPosititon)
        {
            return groupList.get(groupPosition).getItemList().get(childPosititon);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition)
        {
            return childPosition;
        }

        @Override
        public int getChildrenCount(int groupPosition)
        {
            return groupList.get(groupPosition).getItemList().size();
        }

        @Override
        public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent)
        {
            ItemListViewHolder itemListViewHolder = null;
            ListItem itemList = (ListItem)getChild(groupPosition, childPosition);
            ItemType itemType = itemList.getType();

            if(convertView != null)
                itemListViewHolder = (ItemListViewHolder)convertView.getTag();

            switch(itemType)
            {
                case SERVER:
                {
                    if(itemListViewHolder == null || itemListViewHolder.type != ItemType.SERVER)
                    {
                        itemListViewHolder = new ItemListViewHolder();

                        LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                        convertView = infalInflater.inflate(R.layout.airvpn_server_listview_server_item, null);

                        itemListViewHolder.type = ItemType.SERVER;
                        itemListViewHolder.txtName = (TextView)convertView.findViewById(R.id.txt_server_name);
                        itemListViewHolder.imgFlag = (ImageView)convertView.findViewById(R.id.img_server_flag);
                        itemListViewHolder.imgLoad = (ImageView)convertView.findViewById(R.id.img_server_load);
                        itemListViewHolder.imgFavorite = (ImageView)convertView.findViewById(R.id.img_server_favorite);
                        itemListViewHolder.txtLocation = (TextView)convertView.findViewById(R.id.txt_server_location);
                        itemListViewHolder.txtLoad = (TextView)convertView.findViewById(R.id.txt_server_load);
                        itemListViewHolder.txtServers = null;
                        itemListViewHolder.txtUsers = (TextView)convertView.findViewById(R.id.txt_server_users);
                        itemListViewHolder.txtScore = (TextView)convertView.findViewById(R.id.txt_server_score);
                        itemListViewHolder.txtBandWidth = (TextView)convertView.findViewById(R.id.txt_server_bandwidth);
                        itemListViewHolder.txtMaxBandWidth = (TextView)convertView.findViewById(R.id.txt_server_maxbandwidth);
                        itemListViewHolder.llServerWarning = (LinearLayout)convertView.findViewById(R.id.server_warning_layout);
                        itemListViewHolder.txtServerWarning = (TextView)convertView.findViewById(R.id.txt_server_warning);

                        convertView.setTag(itemListViewHolder);
                    }
                }
                break;

                case COUNTRY:
                {
                    if(convertView != null)
                        itemListViewHolder = (ItemListViewHolder)convertView.getTag();

                    if(itemListViewHolder == null || itemListViewHolder.type != ItemType.COUNTRY)
                    {
                        itemListViewHolder = new ItemListViewHolder();

                        LayoutInflater infalInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                        convertView = infalInflater.inflate(R.layout.airvpn_server_listview_country_group_item, null);

                        ImageView imgIndicator = (ImageView)convertView.findViewById(R.id.img_country_indicator);
                        imgIndicator.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.country_icon));
                        imgIndicator.setVisibility(View.VISIBLE);

                        itemListViewHolder.type = ItemType.COUNTRY;
                        itemListViewHolder.txtName = (TextView)convertView.findViewById(R.id.txt_country_name);
                        itemListViewHolder.imgFlag = (ImageView)convertView.findViewById(R.id.img_country_flag);
                        itemListViewHolder.imgLoad = (ImageView)convertView.findViewById(R.id.img_country_load);
                        itemListViewHolder.imgFavorite = (ImageView)convertView.findViewById(R.id.img_country_favorite);
                        itemListViewHolder.txtLocation = null;
                        itemListViewHolder.txtLoad = (TextView)convertView.findViewById(R.id.txt_country_load);
                        itemListViewHolder.txtServers = (TextView)convertView.findViewById(R.id.txt_country_servers);
                        itemListViewHolder.txtUsers = (TextView)convertView.findViewById(R.id.txt_country_users);
                        itemListViewHolder.txtBandWidth = (TextView)convertView.findViewById(R.id.txt_country_bandwidth);
                        itemListViewHolder.txtMaxBandWidth = (TextView)convertView.findViewById(R.id.txt_country_maxbandwidth);

                        convertView.setTag(itemListViewHolder);
                    }
                }
                break;
            }

            itemListViewHolder.txtName.setTypeface(null, Typeface.BOLD);
            itemListViewHolder.txtName.setText(itemList.getName());

            itemListViewHolder.imgFlag.setImageDrawable(context.getDrawable(itemList.getIcon()));

            if(itemType == ItemType.SERVER)
            {
                itemListViewHolder.txtLocation.setTypeface(null, Typeface.NORMAL);
                itemListViewHolder.txtLocation.setText(itemList.getLocation());
            }

            itemListViewHolder.imgLoad.setImageDrawable(context.getDrawable(getLoadIconResource(itemList.getLoad())));

            itemListViewHolder.txtLoad.setTypeface(null, Typeface.NORMAL);
            itemListViewHolder.txtLoad.setText(String.format(Locale.getDefault(), "%d%%", itemList.getLoad()));

            if(itemType == ItemType.COUNTRY)
            {
                itemListViewHolder.txtServers.setTypeface(null, Typeface.NORMAL);
                itemListViewHolder.txtServers.setText(String.format(Locale.getDefault(), "%d", itemList.getServers()));
            }

            itemListViewHolder.txtUsers.setTypeface(null, Typeface.NORMAL);
            itemListViewHolder.txtUsers.setText(String.format(Locale.getDefault(), "%d", itemList.getUsers()));

            if(itemType == ItemType.SERVER)
            {
                itemListViewHolder.txtScore.setTypeface(null, Typeface.NORMAL);

                if(itemList.getWarningOpen().isEmpty() == true && itemList.getWarningClosed().isEmpty() == true)
                    itemListViewHolder.txtScore.setText(String.format(Locale.getDefault(), "%d", itemList.getScore()));
                else
                    itemListViewHolder.txtScore.setText("--");
            }

            itemListViewHolder.txtBandWidth.setTypeface(null, Typeface.NORMAL);
            itemListViewHolder.txtBandWidth.setText(supportTools.formatTransferRate(itemList.getEffectiveBandWidth(), false));

            itemListViewHolder.txtMaxBandWidth.setTypeface(null, Typeface.NORMAL);
            itemListViewHolder.txtMaxBandWidth.setText(supportTools.formatTransferRate(itemList.getMaxBandWidth() * SupportTools.ONE_DECIMAL_MEGA, false));

            if(itemList.isFavorite() || itemList.isForbidden() || itemList.isDefaultItem())
            {
                itemListViewHolder.imgFavorite.setVisibility(View.VISIBLE);

                if(itemList.isDefaultItem())
                    itemListViewHolder.imgFavorite.setImageDrawable(context.getDrawable(R.drawable.default_item));
                if(itemList.isFavorite())
                    itemListViewHolder.imgFavorite.setImageDrawable(context.getDrawable(R.drawable.star_icon_on));
                else if(itemList.isForbidden())
                    itemListViewHolder.imgFavorite.setImageDrawable(context.getDrawable(R.drawable.blacklist_icon_on));
            }
            else
            {
                itemListViewHolder.imgFavorite.setVisibility(View.GONE);
                itemListViewHolder.imgFavorite.setImageResource(android.R.color.transparent);
            }

            if(itemType == ItemType.SERVER)
            {
                if(itemList.getWarningOpen().isEmpty() == true && itemList.getWarningClosed().isEmpty() == true)
                {
                    itemListViewHolder.llServerWarning.setVisibility(View.GONE);
                    itemListViewHolder.txtServerWarning.setText("");

                }
                else
                {
                    itemListViewHolder.llServerWarning.setVisibility(View.VISIBLE);

                    if(itemList.getWarningOpen().isEmpty() == false)
                        itemListViewHolder.txtServerWarning.setText(itemList.getWarningOpen());
                    else
                        itemListViewHolder.txtServerWarning.setText(itemList.getWarningClosed());
                }
            }

            return convertView;
        }
        @Override
        public boolean hasStableIds()
        {
            return false;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition)
        {
            return true;
        }
    }

    private int getCountryFlagResource(String id)
    {
        id = "flag_" + id.toLowerCase(Locale.US);

        int res = getContext().getResources().getIdentifier(id, "drawable", getContext().getPackageName());

        return res;
    }

    private int getLoadIconResource(int load)
    {
        if(load < 0)
            load = 0;

        if(load > 100)
            load = 100;

        int iconId = (load * 8) / 100;

        int res = getContext().getResources().getIdentifier(String.format(Locale.getDefault(),"load_icon_%d", iconId), "drawable", getContext().getPackageName());

        if(res == 0)
            res = R.drawable.load_icon_0;

        return res;
    }

    private void searchDialog()
    {
        TextView txtDialogTitle = null;

        btnOk = null;
        btnCancel = null;
        btnReset = null;

        final Handler dialogHandler = new Handler(new Handler.Callback()
        {
            @Override
            public boolean handleMessage(Message mesg)
            {
                throw new RuntimeException();
            }
        });

        dialogBuilder = new AlertDialog.Builder(getContext());

        View content = LayoutInflater.from(getContext()).inflate(R.layout.edit_option_dialog, null);

        txtDialogTitle = (TextView)content.findViewById(R.id.title);
        edtKey = (EditText)content.findViewById(R.id.key);

        edtKey.setText(searchKey);
        edtKey.setSelection(edtKey.getText().length());

        btnOk = (Button)content.findViewById(R.id.btn_ok);
        btnCancel = (Button)content.findViewById(R.id.btn_cancel);
        btnReset = (Button)content.findViewById(R.id.btn_service);

        btnOk.setText(R.string.search);

        btnReset.setText(R.string.airvpn_server_reset_button);
        btnReset.setVisibility(View.VISIBLE);

        supportTools.setButtonStatus(btnOk, SupportTools.ShowMode.DISABLED);

        supportTools.setButtonStatus(btnCancel, SupportTools.ShowMode.ENABLED);

        btnOk.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                searchKey = edtKey.getText().toString();

                searchDialog.dismiss();

                dialogHandler.sendMessage(dialogHandler.obtainMessage());
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                searchDialog.dismiss();

                dialogHandler.sendMessage(dialogHandler.obtainMessage());
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                searchKey = "";

                searchDialog.dismiss();

                dialogHandler.sendMessage(dialogHandler.obtainMessage());
            }
        });

        edtKey.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void afterTextChanged(Editable s)
            {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                supportTools.setButtonStatus(btnOk, edtKey.getText().toString().isEmpty() == true ? SupportTools.ShowMode.DISABLED : SupportTools.ShowMode.ENABLED);
            }
        });

        txtDialogTitle.setText(R.string.airvpn_server_search_dialog);

        dialogBuilder.setTitle("");
        dialogBuilder.setView(content);

        searchDialog = dialogBuilder.create();
        searchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        searchDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        if(getActivity().isFinishing())
            return;

        searchDialog.show();

        try
        {
            Looper.loop();
        }
        catch(RuntimeException e)
        {
        }

        refreshServerList();

        if(searchKey.isEmpty())
            btnSearch.setBackgroundResource(R.drawable.search);
        else
            btnSearch.setBackgroundResource(R.drawable.search_on);
    }

    private void refreshServerList()
    {
        createGroupList();

        if(serverListAdapter != null)
            serverListAdapter.dataSet(groupList);

        showConnectionInfo();
    }

    private void showConnectionInfo()
    {
        String txtFilter = "";

        if(imgVpnType != null)
        {
            if(settingsManager.getAirVPNDefaultVPNType().equals(SettingsManager.VPN_TYPE_OPENVPN))
                imgVpnType.setImageResource(R.drawable.openvpn_logo);
            else
                imgVpnType.setImageResource(R.drawable.wireguard_logo);
        }

        if(llVpnType != null && imgVpnCycle != null)
        {
            if(vpnManager.vpn().getConnectionStatus() == VPN.Status.NOT_CONNECTED || vpnManager.vpn().getConnectionStatus() == VPN.Status.CONNECTION_CANCELED || vpnManager.vpn().getConnectionStatus() == VPN.Status.CONNECTION_REVOKED_BY_SYSTEM || vpnManager.vpn().getConnectionStatus() == VPN.Status.CONNECTION_ERROR)
            {
                llVpnType.setClickable(true);

                imgVpnCycle.setVisibility(View.VISIBLE);
            }
            else
            {
                llVpnType.setClickable(false);

                imgVpnCycle.setVisibility(View.GONE);
            }
        }

        if(llFilterSettings != null && txtFilterSettings != null)
        {
            txtFilter = "";

            if(!settingsManager.getAirVPNOpenVPNCipher().equals(SettingsManager.AIRVPN_CIPHER_SERVER) && settingsManager.getAirVPNDefaultVPNType().equals(SettingsManager.VPN_TYPE_OPENVPN))
                txtFilter = getString(R.string.conn_stats_cipher_name_cap) + " " + settingsManager.getAirVPNOpenVPNCipher();

            if(txtFilter.isEmpty() == false)
                txtFilter += System.getProperty("line.separator");

            txtFilter += getString(R.string.airvpn_server_settings_ip_version_title) + ": ";

            switch(settingsManager.getAirVPNDefaultIPVersion())
            {
                case SettingsManager.AIRVPN_IP_VERSION_4:
                {
                    txtFilter += "IPv4";
                }
                break;

                case SettingsManager.AIRVPN_IP_VERSION_6:
                {
                    txtFilter += "IPv6";
                }
                break;

                case SettingsManager.AIRVPN_IP_VERSION_4_OVER_6:
                {
                    txtFilter += "IPv6 over IPv4";
                }
                break;

                default:
                {
                    txtFilter += "??";
                }
                break;
            }

            if(settingsManager.getAirVPNDefaultVPNType().equals(SettingsManager.VPN_TYPE_OPENVPN))
            {
                txtFilter += " - " + getString(R.string.conn_protocol_cap) + " " + settingsManager.getAirVPNDefaultOpenVPNProtocol();

                txtFilter += " - " + getString(R.string.conn_port_cap) + " " + settingsManager.getAirVPNDefaultOpenVPNPort();
            }
            else if(settingsManager.getAirVPNDefaultVPNType().equals(SettingsManager.VPN_TYPE_WIREGUARD))
                txtFilter += " - " + getString(R.string.conn_port_cap) + " " + settingsManager.getAirVPNDefaultWireGuardPort();

            txtFilterSettings.setText(txtFilter);
        }
    }

    private class Group
    {
        private GroupType type;
        private int icon;
        private String name;
        private String countryCode;
        private int servers;
        private int users;
        private long bandWidth;
        private long maxBandWidth;
        private ArrayList<ListItem> itemList;
        private String sortMode = "", sortBy = "";

        private Comparator<ListItem> comparatorServerName = new Comparator<ListItem>()
        {
            @Override
            public int compare(ListItem s1, ListItem s2)
            {
                int result;

                if(sortMode.equals(SettingsManager.AIRVPN_SERVER_SORT_MODE_ASCENDING))
                    result = s1.getName().compareToIgnoreCase(s2.getName());
                else
                    result = s2.getName().compareToIgnoreCase(s1.getName());

                return result;
            }
        };

        private Comparator<ListItem> comparatorServerLocation = new Comparator<ListItem>()
        {
            @Override
            public int compare(ListItem s1, ListItem s2)
            {
                int result;

                if(sortMode.equals(SettingsManager.AIRVPN_SERVER_SORT_MODE_ASCENDING))
                    result = s1.getLocation().compareToIgnoreCase(s2.getLocation());
                else
                    result = s2.getLocation().compareToIgnoreCase(s1.getLocation());

                return result;
            }
        };

        private Comparator<ListItem> comparatorServerScore = new Comparator<ListItem>()
        {
            @Override
            public int compare(ListItem s1, ListItem s2)
            {
                int result = 0;
                AirVPNServer serv1 = airVPNManifest.getServerByName(s1.getName());
                AirVPNServer serv2 = airVPNManifest.getServerByName(s2.getName());

                if(serv1 != null && serv2 != null)
                {
                    if(sortMode.equals(SettingsManager.AIRVPN_SERVER_SORT_MODE_ASCENDING))
                        result = serv1.getScore() - serv2.getScore();
                    else
                        result = serv2.getScore() - serv1.getScore();
                }

                return result;
            }
        };

        private Comparator<ListItem> comparatorServerLoad = new Comparator<ListItem>()
        {
            @Override
            public int compare(ListItem s1, ListItem s2)
            {
                int result;

                if(sortMode.equals(SettingsManager.AIRVPN_SERVER_SORT_MODE_ASCENDING))
                    result = s1.getLoad() - s2.getLoad();
                else
                    result = s2.getLoad() - s1.getLoad();

                return result;
            }
        };

        private Comparator<ListItem> comparatorServerUsers = new Comparator<ListItem>()
        {
            @Override
            public int compare(ListItem s1, ListItem s2)
            {
                int result;

                if(sortMode.equals(SettingsManager.AIRVPN_SERVER_SORT_MODE_ASCENDING))
                    result = s1.getUsers() - s2.getUsers();
                else
                    result = s2.getUsers() - s1.getUsers();

                return result;
            }
        };

        private Comparator<ListItem> comparatorServerBandwidth = new Comparator<ListItem>()
        {
            @Override
            public int compare(ListItem s1, ListItem s2)
            {
                int result;

                if(sortMode.equals(SettingsManager.AIRVPN_SERVER_SORT_MODE_ASCENDING))
                    result = (int)(s1.getBandWidth() - s2.getBandWidth());
                else
                    result = (int)(s2.getBandWidth() - s1.getBandWidth());

                return result;
            }
        };

        private Comparator<ListItem> comparatorServerMaxBandwidth = new Comparator<ListItem>()
        {
            @Override
            public int compare(ListItem s1, ListItem s2)
            {
                int result;

                if(sortMode.equals(SettingsManager.AIRVPN_SERVER_SORT_MODE_ASCENDING))
                    result = (int)(s1.getMaxBandWidth() - s2.getMaxBandWidth());
                else
                    result = (int)(s2.getMaxBandWidth() - s1.getMaxBandWidth());

                return result;
            }
        };

        Group()
        {
            type = GroupType.GROUP;
            name = "";
            countryCode = "";
            servers = 0;
            users = 0;
            bandWidth = 0;
            maxBandWidth = 0;
            itemList = new ArrayList<ListItem>();
        }

        public void setType(GroupType t)
        {
            type = t;
        }

        public GroupType getType()
        {
            return type;
        }

        public void setIcon(int i)
        {
            icon = i;
        }

        public int getIcon()
        {
            return icon;
        }

        public void setName(String n)
        {
            name = n;
        }

        public String getName()
        {
            return name;
        }

        public void setCountryCode(String c)
        {
            countryCode = c;
        }

        public String getCountryCode()
        {
            return countryCode;
        }

        public void setServers(int s)
        {
            servers = s;
        }

        public int getServers()
        {
            return servers;
        }

        public void setUsers(int u)
        {
            users = u;
        }

        public int getUsers()
        {
            return users;
        }

        public long getBandWidth()
        {
            return bandWidth;
        }

        public void setBandWidth(long b)
        {
            bandWidth = b;
        }

        public long getEffectiveBandWidth()
        {
            return (2 * (bandWidth * 8));
        }

        public long getMaxBandWidth()
        {
            return maxBandWidth;
        }

        public void setMaxBandWidth(long b)
        {
            maxBandWidth = b;
        }

        public void addItem(ListItem s)
        {
            itemList.add(s);
        }

        public ArrayList<ListItem> getItemList()
        {
            return itemList;
        }

        public int getLoad()
        {
            return supportTools.getTrafficLoad(bandWidth, maxBandWidth);
        }

        public void sortItemList()
        {
            sortBy = settingsManager.getAirVPNSortBy();
            sortMode = settingsManager.getAirVPNSortMode();

            switch(sortBy)
            {
                case SettingsManager.AIRVPN_SERVER_SORT_BY_NAME:
                {
                    Collections.sort(itemList, comparatorServerName);
                }
                break;

                case SettingsManager.AIRVPN_SERVER_SORT_BY_LOCATION:
                {
                    Collections.sort(itemList, comparatorServerLocation);
                }
                break;

                case SettingsManager.AIRVPN_SERVER_SORT_BY_SCORE:
                {
                    Collections.sort(itemList, comparatorServerScore);
                }
                break;

                case SettingsManager.AIRVPN_SERVER_SORT_BY_LOAD:
                {
                    Collections.sort(itemList, comparatorServerLoad);
                }
                break;

                case SettingsManager.AIRVPN_SERVER_SORT_BY_USERS:
                {
                    Collections.sort(itemList, comparatorServerUsers);
                }
                break;

                case SettingsManager.AIRVPN_SERVER_SORT_BY_BANDWIDTH:
                {
                    Collections.sort(itemList, comparatorServerBandwidth);
                }
                break;

                case SettingsManager.AIRVPN_SERVER_SORT_BY_MAX_BANDWIDTH:
                {
                    Collections.sort(itemList, comparatorServerMaxBandwidth);
                }
                break;

                default:
                {
                    Collections.sort(itemList, comparatorServerName);
                }
                break;
            }
        }
    }

    private class ListItem
    {
        private ItemType type;
        private String name;
        private int icon;
        private String countryCode;
        private String location;
        private int servers;
        private int users;
        private int score;
        private long bandWidth;
        private long maxBandWidth;
        private boolean favorite;
        private boolean forbidden;
        private boolean defaultItem;
        private String warningOpen;
        private String warningClosed;

        ListItem()
        {
            type = ItemType.SERVER;
            name = "";
            icon = 0;
            countryCode = "";
            location = "";
            servers = 0;
            users = 0;
            score = 0;
            bandWidth = 0;
            maxBandWidth = 0;
            favorite = false;
            forbidden = false;
            defaultItem = false;
            warningOpen = "";
            warningClosed = "";
        }

        public void setType(ItemType t)
        {
            type = t;
        }

        public ItemType getType()
        {
            return type;
        }

        public void setName(String n)
        {
            name = n;
        }

        public String getName()
        {
            return name;
        }

        public void setIcon(int i)
        {
            icon = i;
        }

        public int getIcon()
        {
            return icon;
        }

        public void setCountryCode(String c)
        {
            countryCode = c;
        }

        public String getCountryCode()
        {
            return countryCode;
        }

        public void setLocation(String l)
        {
            location = l;
        }

        public String getLocation()
        {
            return location;
        }

        public void setServers(int s)
        {
            servers = s;
        }

        public int getServers()
        {
            return servers;
        }

        public void setUsers(int u)
        {
            users = u;
        }

        public int getUsers()
        {
            return users;
        }

        public void setScore(int s)
        {
            score = s;
        }

        public int getScore()
        {
            return score;
        }
        public long getBandWidth()
        {
            return bandWidth;
        }

        public void setBandWidth(long b)
        {
            bandWidth = b;
        }

        public long getEffectiveBandWidth()
        {
            return (2 * (bandWidth * 8));
        }

        public long getMaxBandWidth()
        {
            return maxBandWidth;
        }

        public void setMaxBandWidth(long b)
        {
            maxBandWidth = b;
        }

        public void setFavorite(boolean f)
        {
            favorite = f;
        }

        public boolean isFavorite()
        {
            return favorite;
        }

        public void setForbidden(boolean f)
        {
            forbidden = f;
        }

        public boolean isForbidden()
        {
            return forbidden;
        }

        public void setStartupItem(boolean f)
        {
            defaultItem = f;
        }

        public boolean isDefaultItem()
        {
            return defaultItem;
        }

        public void setWarningOpen(String s)
        {
            warningOpen = s;
        }

        public String getWarningOpen()
        {
            return warningOpen;
        }

        public void setWarningClosed(String s)
        {
            warningClosed = s;
        }

        public String getWarningClosed()
        {
            return warningClosed;
        }

        public int getLoad()
        {
            return supportTools.getTrafficLoad(bandWidth, maxBandWidth);
        }
    }

    // Eddie events

    public void onVpnConnectionStatsChanged(final VPNConnectionStats connectionData)
    {
    }

    public void onVpnStatusChanged(VPN.Status status, String message)
    {
    }

    public void onVpnAuthFailed(final VPNEvent oe)
    {
    }

    public void onVpnError(final VPNEvent oe)
    {
        if(vpnManager.vpn().getConnectionMode() == VPN.ConnectionMode.AIRVPN_SERVER && connectionInProgress == true)
        {
            supportTools.dismissConnectionProgressDialog();

            supportTools.infoDialog(R.string.connection_error, true);

            connectionInProgress = false;
        }
    }

    public void onVpnReconnect(final VPNEvent oe)
    {
    }

    public void onMasterPasswordChanged()
    {
    }

    public void onAirVPNLogin()
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                if(NetworkStatusReceiver.isNetworkConnected())
                    supportTools.setButtonStatus(btnReloadManifest, SupportTools.ShowMode.ENABLED);

                if(elvAirVPNServer != null)
                    registerForContextMenu(elvAirVPNServer);

                if(pendingServerConnection != null)
                {
                    final Runnable uiRunnable = new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            connectServer(pendingServerConnection, ConnectionMode.SERVER, "");
                        }
                    };

                    SupportTools.runOnUiActivity(getActivity(), uiRunnable);
                }
            }
        };

        SupportTools.runOnUiActivity(getActivity(), runnable);
    }

    public void onAirVPNLoginFailed(String message)
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                supportTools.setButtonStatus(btnReloadManifest, SupportTools.ShowMode.HIDDEN);

                if(elvAirVPNServer != null)
                    unregisterForContextMenu(elvAirVPNServer);
            }
        };

        SupportTools.runOnUiActivity(getActivity(), runnable);
    }

    public void onAirVPNLogout()
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                supportTools.setButtonStatus(btnReloadManifest, SupportTools.ShowMode.HIDDEN);

                if(elvAirVPNServer != null)
                    unregisterForContextMenu(elvAirVPNServer);
            }
        };

        SupportTools.runOnUiActivity(getActivity(), runnable);
    }

    public void onAirVPNUserDataChanged()
    {
    }

    public void onAirVPNUserProfileReceived(Document document)
    {
    }

    public void onAirVPNUserProfileDownloadError()
    {
    }

    public void onAirVPNUserProfileChanged()
    {
    }

    public void onAirVPNManifestReceived(Document document)
    {
    }

    public void onAirVPNManifestDownloadError()
    {
        supportTools.dismissProgressDialog();

        if(NetworkStatusReceiver.isNetworkConnected())
        {
            supportTools.infoDialog(String.format("%s%s", getResources().getString(R.string.manifest_download_error), getResources().getString(R.string.bootstrap_server_error)), true);

            EddieLogger.error("Error while retrieving AirVPN manifest from server");
        }
    }

    public void onAirVPNManifestChanged()
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                createGroupList();

                if(elvAirVPNServer != null)
                {
                    serverListAdapter = new AirVPNServerExpandableListAdapter(getContext(), groupList);

                    elvAirVPNServer.setAdapter(serverListAdapter);

                    showConnectionInfo();
                }

                supportTools.dismissProgressDialog();
            }
        };

        SupportTools.runOnUiActivity(getActivity(), runnable);
    }

    public void onAirVPNIgnoredManifestDocumentRequest()
    {
    }

    public void onAirVPNIgnoredUserDocumentRequest()
    {
    }

    public void onAirVPNRequestError(String message)
    {
    }

    public void onCancelConnection()
    {
    }

    public void onSaveState()
    {
    }

    public void onRestoreState(Bundle bundle)
    {
    }

    // NetworkStatusReceiver

    public void onNetworkStatusNotAvailable()
    {
        if(btnReloadManifest != null)
            supportTools.setButtonStatus(btnReloadManifest, SupportTools.ShowMode.HIDDEN);
    }

    public void onNetworkStatusConnected()
    {
        if(btnReloadManifest != null && airVPNUser.isUserValid())
            supportTools.setButtonStatus(btnReloadManifest, SupportTools.ShowMode.ENABLED);
    }

    public void onNetworkStatusIsConnecting()
    {
    }

    public void onNetworkStatusIsDisconnecting()
    {
    }

    public void onNetworkStatusSuspended()
    {
    }

    public void onNetworkStatusNotConnected()
    {
    }

    public void onNetworkTypeChanged()
    {
    }
}