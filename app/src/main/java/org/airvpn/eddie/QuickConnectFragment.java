// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 3 October 2018 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.os.Build;
import android.os.Bundle;
import android.Manifest;

import androidx.fragment.app.Fragment;

import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.w3c.dom.Document;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;

public class QuickConnectFragment extends Fragment implements NetworkStatusListener, EddieEventListener
{
    public enum Status
    {
        IDLE,
        CONNECTION_IN_PROGRESS,
        CONNECTED
    }

    private final int FRAGMENT_ID = 5001;
    private final String AIRVPN_CONNECTION_SEQUENCE_FILE_NAME = "connection_sequence.csv";

    private class ConnectionScheme
    {
        private String protocol;
        private int port;
        private int entry;

        void setProtocol(String p)
        {
            protocol = p;
        }

        String getProtocol()
        {
            return protocol;
        }

        void setPort(int p)
        {
            port = p;
        }

        int getPort()
        {
            return port;
        }

        void setEntry(int e)
        {
            entry = e;
        }

        int getEntry()
        {
            return entry;
        }
    }

    private SupportTools supportTools = null;
    private EddieEvent eddieEvent = null;
    private static VPNManager vpnManager = null;
    private SettingsManager settingsManager = null;
    private NetworkStatusReceiver networkStatusReceiver = null;
    private CountryContinent countryContinent = null;

    private Button btnQuickConnect = null;
    private TextView txtConnectionStatus = null, txtAirVPNSubscriptionStatus = null;
    private TextView txtVpnStatus = null, txtNetworkStatus = null, txtServerCipher = null;
    private ImageView imgVpnType = null, imgVpnCycle = null;
    private LinearLayout llVpnType = null, llUserKey = null, llServerCipher = null;
    private Spinner spnAirVPNKey = null;

    private AirVPNUser airVPNUser = null;
    private AirVPNServerProvider airVPNServerProvider = null;

    private ArrayList<AirVPNServer> airVPNServerList = null;

    private Status currentStatus = Status.IDLE;

    private ArrayList<ConnectionScheme> connectionSchemeList = null;

    private int currentServerIndex = 0, currentConnectionSchemeIndex = 0;
    private static String selectedUserKey = "";

    private VPN.Status lastVpnStatus = VPN.Status.NOT_CONNECTED;
    private boolean preparingNextServerConnection = false;
    private boolean waitForManifest = false;

    private String connectionStatus = "";
    private HashMap<String, String> profileInfo = null;

    public void setVpnManager(VPNManager v)
    {
        vpnManager = v;
    }

    private boolean doInitialCheckPermissions = true;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        supportTools = EddieApplication.supportTools();
        settingsManager = EddieApplication.settingsManager();
        countryContinent = EddieApplication.countryContinent();

        networkStatusReceiver = EddieApplication.networkStatusReceiver();

        networkStatusReceiver.subscribeListener(this);

        eddieEvent = EddieApplication.eddieEvent();

        eddieEvent.subscribeListener(this);

        airVPNUser = EddieApplication.airVPNUser();

        airVPNServerProvider = new AirVPNServerProvider();

        setConnectButton();

        loadConnectionSchemes();

        if(settingsManager.isAirVPNAutologinEnabled() == true)
        {
            final Runnable uiRunnable = new Runnable()
            {
                @Override
                public void run()
                {
                    airVPNUser.validateUserLogin(getActivity());
                }
            };

            SupportTools.runOnUiActivity(getActivity(), uiRunnable);
        }

        doInitialCheckPermissions = true;

        waitForManifest = false;
    }

    @Override
    public void onResume()
    {
        super.onResume();

        updateConnectionStatus();

        setConnectButton();

        if(doInitialCheckPermissions == true)
        {
            checkPermissions();

            doInitialCheckPermissions = false;
        }
    }

    @Override
    public void setMenuVisibility(boolean visible)
    {
        super.setMenuVisibility(visible);

        if(visible)
            updateConnectionStatus();

        setConnectButton();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        if(networkStatusReceiver != null)
            networkStatusReceiver.unsubscribeListener(this);

        if(eddieEvent != null)
            eddieEvent.unsubscribeListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View fragmentView = inflater.inflate(R.layout.fragment_quick_connect_layout, container, false);

        btnQuickConnect = (Button)fragmentView.findViewById(R.id.quick_connect_button);
        txtConnectionStatus = (TextView)fragmentView.findViewById(R.id.connection_status);
        llVpnType = (LinearLayout)fragmentView.findViewById(R.id.vpn_type);
        imgVpnType = (ImageView)fragmentView.findViewById(R.id.img_vpn_type);
        imgVpnCycle = (ImageView)fragmentView.findViewById(R.id.img_vpn_cycle);
        llUserKey = (LinearLayout)fragmentView.findViewById(R.id.user_key);
        spnAirVPNKey = (Spinner)fragmentView.findViewById(R.id.spn_user_key);
        llServerCipher = (LinearLayout)fragmentView.findViewById(R.id.server_cipher);
        txtServerCipher = (TextView)fragmentView.findViewById(R.id.txt_server_cipher);

        btnQuickConnect.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                waitForManifest = false;

                if(AirVPNManifest.getManifestType() == AirVPNManifest.ManifestType.NOT_SET || AirVPNManifest.getManifestType() == AirVPNManifest.ManifestType.PROCESSING)
                {
                    supportTools.showProgressDialog(R.string.airvpn_server_refresh_manifest);

                    waitForManifest = true;

                    return;
                }

                VPN.Status vpnStatus = vpnManager.vpn().getConnectionStatus();

                if(vpnStatus == VPN.Status.NOT_CONNECTED || vpnStatus == VPN.Status.CONNECTION_REVOKED_BY_SYSTEM || vpnStatus == VPN.Status.CONNECTION_ERROR || vpnStatus == VPN.Status.CONNECTION_CANCELED || vpnStatus == VPN.Status.UNDEFINED)
                    quickConnectToAirVPN();
                else
                    disconnectCurrentProfile();
            }
        });

        btnQuickConnect.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                 if(btnQuickConnect.isEnabled() == false)
                     return;

                if(hasFocus)
                {
                    if(vpnManager.vpn().getConnectionStatus() == VPN.Status.CONNECTED)
                        btnQuickConnect.setBackgroundResource(R.drawable.quick_connect_off);
                    else
                        btnQuickConnect.setBackgroundResource(R.drawable.quick_connect_on);
                }
                else
                {
                    if(vpnManager.vpn().getConnectionStatus() == VPN.Status.CONNECTED)
                        btnQuickConnect.setBackgroundResource(R.drawable.quick_connect_on);
                    else
                        btnQuickConnect.setBackgroundResource(R.drawable.quick_connect_off);
                }
            }
        });

        btnQuickConnect.setAccessibilityDelegate(new View.AccessibilityDelegate()
        {
            @Override
            public void sendAccessibilityEvent(View host, int eventType)
            {
                super.sendAccessibilityEvent(host, eventType);

                btnQuickConnect.setContentDescription(connectionStatus);
            }
        });

        llVpnType.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                if(settingsManager.getAirVPNDefaultVPNType().equals(SettingsManager.VPN_TYPE_OPENVPN))
                {
                    settingsManager.setAirVPNDefaultVPNType(SettingsManager.VPN_TYPE_WIREGUARD);
                    imgVpnType.setImageResource(R.drawable.wireguard_logo);
                    llServerCipher.setVisibility(View.GONE);
                }
                else
                {
                    settingsManager.setAirVPNDefaultVPNType(SettingsManager.VPN_TYPE_OPENVPN);
                    imgVpnType.setImageResource(R.drawable.openvpn_logo);

                    if(settingsManager.getAirVPNOpenVPNCipher().equals(SettingsManager.AIRVPN_CIPHER_SERVER))
                        llServerCipher.setVisibility(View.GONE);
                    else
                    {
                        llServerCipher.setVisibility(View.VISIBLE);
                        txtServerCipher.setText(settingsManager.getAirVPNOpenVPNCipher());
                    }
                }
            }
        });

        spnAirVPNKey.setAccessibilityDelegate(new View.AccessibilityDelegate()
        {
            @Override
            public void sendAccessibilityEvent(View host, int eventType)
            {
                super.sendAccessibilityEvent(host, eventType);

                spnAirVPNKey.setContentDescription(String.format("%s %s", getString(R.string.accessibility_selected_airvpn_key), spnAirVPNKey.getSelectedItem().toString()));
            }
        });

        setupUserKeySpinner(selectedUserKey);

        llUserKey.setVisibility(View.GONE);

        txtAirVPNSubscriptionStatus = (TextView)fragmentView.findViewById(R.id.airvpn_subscription_status);

        txtVpnStatus = (TextView)fragmentView.findViewById(R.id.vpn_connection_status);
        txtVpnStatus.setText(getResources().getString(vpnManager.vpn().connectionStatusResourceDescription(vpnManager.vpn().getConnectionStatus())));

        if(settingsManager.getAirVPNDefaultVPNType().equals(SettingsManager.VPN_TYPE_OPENVPN))
            imgVpnType.setImageResource(R.drawable.openvpn_logo);
        else
            imgVpnType.setImageResource(R.drawable.wireguard_logo);

        txtNetworkStatus = (TextView)fragmentView.findViewById(R.id.network_connection_status);

        if(NetworkStatusReceiver.getNetworkStatus() == NetworkStatusReceiver.Status.CONNECTED)
            txtNetworkStatus.setText(String.format(Locale.getDefault(), getResources().getString(R.string.conn_status_connected), NetworkStatusReceiver.getNetworkDescription()));
        else
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));

        if(!settingsManager.getAirVPNOpenVPNCipher().equals(SettingsManager.AIRVPN_CIPHER_SERVER) && settingsManager.getAirVPNDefaultVPNType().equals(SettingsManager.VPN_TYPE_OPENVPN))
        {
            llServerCipher.setVisibility(View.VISIBLE);
            txtServerCipher.setText(settingsManager.getAirVPNOpenVPNCipher());
        }
        else
            llServerCipher.setVisibility(View.GONE);

        setConnectButton();

        return fragmentView;
    }

    public Status getCurrentStatus()
    {
        return currentStatus;
    }

    private void updateStatusBox()
    {
        if(txtAirVPNSubscriptionStatus != null)
        {
            if(airVPNUser.isUserValid())
            {
                txtAirVPNSubscriptionStatus.setText(airVPNUser.getExpirationText());

                txtAirVPNSubscriptionStatus.setVisibility(View.VISIBLE);
            }
            else
                txtAirVPNSubscriptionStatus.setVisibility(View.GONE);
        }

        if(txtNetworkStatus != null)
        {
            if(NetworkStatusReceiver.getNetworkStatus() == NetworkStatusReceiver.Status.CONNECTED)
                txtNetworkStatus.setText(String.format(Locale.getDefault(), getResources().getString(R.string.conn_status_connected), NetworkStatusReceiver.getNetworkDescription()));
            else
                txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));
        }

        if(imgVpnType != null)
        {
            if(settingsManager.getAirVPNDefaultVPNType().equals(SettingsManager.VPN_TYPE_OPENVPN))
                imgVpnType.setImageResource(R.drawable.openvpn_logo);
            else
                imgVpnType.setImageResource(R.drawable.wireguard_logo);
        }

        if(llVpnType != null && imgVpnCycle != null)
        {
            if(vpnManager.vpn().getConnectionStatus() == VPN.Status.NOT_CONNECTED || vpnManager.vpn().getConnectionStatus() == VPN.Status.CONNECTION_CANCELED || vpnManager.vpn().getConnectionStatus() == VPN.Status.CONNECTION_REVOKED_BY_SYSTEM || vpnManager.vpn().getConnectionStatus() == VPN.Status.CONNECTION_ERROR)
            {
                llVpnType.setClickable(true);

                imgVpnCycle.setVisibility(View.VISIBLE);
            }
            else
            {
                llVpnType.setClickable(false);

                imgVpnCycle.setVisibility(View.GONE);
            }
        }

        if(txtVpnStatus != null)
            txtVpnStatus.setText(getResources().getString(vpnManager.vpn().connectionStatusResourceDescription(vpnManager.vpn().getConnectionStatus())));

        if(txtConnectionStatus != null)
            txtConnectionStatus.setText(connectionStatus);

        selectedUserKey = airVPNUser.getCurrentKey();

        if(airVPNUser.isUserValid())
            setupUserKeySpinner(selectedUserKey);
        else
        {
            if(llUserKey != null)
                llUserKey.setVisibility(View.GONE);
        }

        if(llServerCipher != null)
        {
            if(!settingsManager.getAirVPNOpenVPNCipher().equals(SettingsManager.AIRVPN_CIPHER_SERVER) && settingsManager.getAirVPNDefaultVPNType().equals(SettingsManager.VPN_TYPE_OPENVPN))
            {
                llServerCipher.setVisibility(View.VISIBLE);

                if(txtServerCipher != null)
                    txtServerCipher.setText(settingsManager.getAirVPNOpenVPNCipher());
            }
            else
                llServerCipher.setVisibility(View.GONE);
        }

        setConnectButton();
    }

    private void quickConnectToAirVPN()
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                AirVPNServerProvider.TLSMode tlsMode;

                if(settingsManager.getAirVPNDefaultVPNType().equals(SettingsManager.VPN_TYPE_OPENVPN) == true)
                {
                    if(settingsManager.getAirVPNOpenVPNMode().equals(SettingsManager.AIRVPN_OPENVPN_MODE_DEFAULT) == true)
                    {
                        if(settingsManager.getAirVPNQuickConnectMode().equals(SettingsManager.AIRVPN_QUICK_CONNECT_MODE_AUTO) == true)
                            tlsMode = AirVPNServerProvider.TLSMode.TLS_CRYPT;
                        else
                            tlsMode = (settingsManager.getAirVPNDefaultTLSMode() == SettingsManager.OPENVPN_TLS_MODE_CRYPT) ? AirVPNServerProvider.TLSMode.TLS_CRYPT : AirVPNServerProvider.TLSMode.TLS_AUTH;
                    }
                    else
                        tlsMode = (EddieApplication.airVPNManifest().getMode(settingsManager.getAirVPNOpenVPNMode()).getTlsMode() == SettingsManager.OPENVPN_TLS_MODE_CRYPT) ? AirVPNServerProvider.TLSMode.TLS_CRYPT : AirVPNServerProvider.TLSMode.TLS_AUTH;
                }
                else
                    tlsMode = AirVPNServerProvider.TLSMode.TLS_AUTH;

                if(airVPNUser.validateUserLogin(getActivity()))
                {
                    btnQuickConnect.setBackgroundResource(R.drawable.quick_connect_on);

                    airVPNServerProvider.reset();

                    airVPNServerProvider.setUserCountry(airVPNUser.getUserCountry());

                    airVPNServerProvider.setTlsMode(tlsMode);

                    if(settingsManager.getAirVPNQuickConnectMode().equals(SettingsManager.AIRVPN_QUICK_CONNECT_MODE_AUTO))
                    {
                        airVPNServerProvider.setSupportIPv4(true);
                        airVPNServerProvider.setSupportIPv6(false);
                    }
                    else
                    {
                        switch(settingsManager.getAirVPNDefaultIPVersion())
                        {
                            case SettingsManager.AIRVPN_IP_VERSION_4:
                            {
                                airVPNServerProvider.setSupportIPv4(true);
                                airVPNServerProvider.setSupportIPv6(false);
                            }
                            break;

                            case SettingsManager.AIRVPN_IP_VERSION_6:
                            case SettingsManager.AIRVPN_IP_VERSION_4_OVER_6:
                            {
                                airVPNServerProvider.setSupportIPv4(true);
                                airVPNServerProvider.setSupportIPv6(true);
                            }
                            break;

                            default:
                            {
                                airVPNServerProvider.setSupportIPv4(true);
                                airVPNServerProvider.setSupportIPv6(false);
                            }
                            break;
                        }
                    }

                    airVPNServerList = airVPNServerProvider.getFilteredServerList();

                    currentServerIndex = 0;
                    currentConnectionSchemeIndex = 0;

                    currentStatus = Status.CONNECTION_IN_PROGRESS;

                    preparingNextServerConnection = true;

                    connectToNextServer();
                }
            }
        };

        SupportTools.runOnUiActivity(getActivity(), runnable);
    }

    private void disconnectCurrentProfile()
    {
        VPN.Status vpnStatus = vpnManager.vpn().getConnectionStatus();

        if(vpnStatus != VPN.Status.CONNECTING && vpnStatus != VPN.Status.CONNECTED && vpnStatus != VPN.Status.PAUSED_BY_USER && vpnStatus != VPN.Status.PAUSED_BY_SYSTEM && vpnStatus != VPN.Status.LOCKED)
            return;

        final Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                if(!supportTools.confirmationDialog(R.string.conn_confirm_disconnection))
                    return;

                try
                {
                    vpnManager.stopConnection();
                }
                catch(Exception e)
                {
                    connectionStatus = e.getMessage();

                    txtConnectionStatus.setText(connectionStatus);

                    EddieLogger.error(connectionStatus);
                }
            }
        };

        SupportTools.runOnUiActivity(getActivity(), runnable);
    }

    private void connectToNextServer()
    {
        AirVPNServer server = null;
        HashMap<Integer, String> serverEntryIP = null;
        ConnectionScheme connectionScheme = null;
        AirVPNManifest.Mode manifestMode = null;
        String protocol = "", tlsMode = "", cipher = "", allowedIPs = "";
        int port = 0;
        int entry = 0;
        boolean connectIPv6 = false, mode6to4 = false;
        String vpnProfile = "", userKeyName = "";

        if(currentStatus != Status.CONNECTION_IN_PROGRESS)
            return;

        if(supportTools.isNetworkConnectionActive() == false)
        {
            EddieLogger.error("QuickConnectFragment.connectToNextServer(): Network connection is not available");

            final Runnable uiRunnable = new Runnable()
            {
                @Override
                public void run()
                {
                    supportTools.infoDialog(R.string.network_is_not_available, true);

                    currentStatus = Status.IDLE;

                    setConnectButton();
                }
            };

            SupportTools.runOnUiActivity(getActivity(), uiRunnable);

            return;
        }

        if(supportTools.waitForManifest(true) == false)
        {
            EddieLogger.error("QuickConnectFragment.connectToNextServer(): AirVPN Manifest is not available");

            final Runnable uiRunnable = new Runnable()
            {
                @Override
                public void run()
                {
                    supportTools.infoDialog(R.string.manifest_download_error, true);

                    currentStatus = Status.IDLE;

                    setConnectButton();
                }
            };

            SupportTools.runOnUiActivity(getActivity(), uiRunnable);

            return;
        }

        if(airVPNServerList == null)
        {
            EddieLogger.error("QuickConnectFragment.connectToNextServer(): airVPNServerList is null");

            final Runnable uiRunnable = new Runnable()
            {
                @Override
                public void run()
                {
                    supportTools.infoDialog(R.string.quick_connect_end_of_server_list, true);

                    currentStatus = Status.IDLE;

                    setConnectButton();
                }
            };

            SupportTools.runOnUiActivity(getActivity(), uiRunnable);

            return;
        }

        vpnManager.reserveVpnConnection();

        if(currentServerIndex > airVPNServerList.size() - 1)
        {
            EddieLogger.error("QuickConnectFragment.startConnection(): End of server list reached. No server available for connection.");

            connectionStatus = getResources().getString(R.string.quick_connect_end_of_server_list);

            final Runnable uiRunnable = new Runnable()
            {
                @Override
                public void run()
                {
                    supportTools.dismissConnectionProgressDialog();

                    if(txtConnectionStatus != null)
                        txtConnectionStatus.setText(connectionStatus);

                    supportTools.infoDialog(connectionStatus, true);

                    currentStatus = Status.IDLE;

                    setConnectButton();
                }
            };

            SupportTools.runOnUiActivity(getActivity(), uiRunnable);

            vpnManager.stopConnection();

            return;
        }

        if(spnAirVPNKey != null && spnAirVPNKey.getCount() > 0 && spnAirVPNKey.getSelectedItem() != null)
            userKeyName = spnAirVPNKey.getSelectedItem().toString();
        else
        {
            ArrayList<String> keyNames = airVPNUser.getUserKeyNames();

            if(keyNames != null && keyNames.size() > 0)
                userKeyName = keyNames.get(0);
            else
            {
                connectionStatus = getResources().getString(R.string.quick_connect_user_has_no_airvpn_key);

                txtConnectionStatus.setText(connectionStatus);

                EddieLogger.error("QuickConnectFragment.connectToNextServer(): No user key defined. Cannot connect to AirVPN.");

                currentStatus = Status.IDLE;

                vpnManager.cancelVpnConnectionReservation();

                return;
            }
        }

        server = airVPNServerList.get(currentServerIndex);

        if(airVPNServerProvider.getTlsMode().equals(AirVPNServerProvider.TLSMode.TLS_AUTH) || settingsManager.getAirVPNDefaultVPNType().equals(SettingsManager.VPN_TYPE_WIREGUARD))
        {
            tlsMode = SettingsManager.OPENVPN_TLS_MODE_AUTH;

            entry = 0;
        }
        else
        {
            tlsMode = SettingsManager.OPENVPN_TLS_MODE_CRYPT;

            entry = 2;
        }

        if(settingsManager.getAirVPNQuickConnectMode().equals(SettingsManager.AIRVPN_QUICK_CONNECT_MODE_AUTO) && settingsManager.getAirVPNDefaultVPNType().equals(SettingsManager.VPN_TYPE_OPENVPN))
        {
            if(connectionSchemeList == null)
            {
                EddieLogger.error("QuickConnectFragment.startConnection(): connectionSchemeList is null");

                vpnManager.cancelVpnConnectionReservation();

                return;
            }

            connectionScheme = connectionSchemeList.get(currentConnectionSchemeIndex);

            protocol = connectionScheme.getProtocol();

            port = connectionScheme.getPort();

            entry += connectionScheme.getEntry();

            currentConnectionSchemeIndex++;

            if(currentConnectionSchemeIndex > connectionSchemeList.size() - 1)
            {
                currentConnectionSchemeIndex = 0;

                currentServerIndex++;
            }
        }
        else
        {
            switch(settingsManager.getAirVPNDefaultVPNType())
            {
                case SettingsManager.VPN_TYPE_OPENVPN:
                {
                    protocol = settingsManager.getAirVPNDefaultOpenVPNProtocol();

                    port = settingsManager.getAirVPNDefaultOpenVPNPort();
                }
                break;

                case SettingsManager.VPN_TYPE_WIREGUARD:
                {
                    protocol = SettingsManager.AIRVPN_PROTOCOL_UDP;

                    port = settingsManager.getAirVPNDefaultWireGuardPort();
                }
                break;
            }

            currentServerIndex++;
        }

        switch(settingsManager.getAirVPNDefaultIPVersion())
        {
            case SettingsManager.AIRVPN_IP_VERSION_4:
            {
                serverEntryIP = server.getEntryIPv4();

                connectIPv6 = false;
                mode6to4 = false;
            }
            break;

            case SettingsManager.AIRVPN_IP_VERSION_6:
            {
                serverEntryIP = server.getEntryIPv6();

                protocol += "6";

                connectIPv6 = true;
                mode6to4 = false;
            }
            break;

            case SettingsManager.AIRVPN_IP_VERSION_4_OVER_6:
            {
                serverEntryIP = server.getEntryIPv4();

                connectIPv6 = true;
                mode6to4 = true;
            }
            break;

            default:
            {
                serverEntryIP = server.getEntryIPv4();

                connectIPv6 = false;
                mode6to4 = false;
            }
            break;
        }

        if(serverEntryIP == null)
        {
            connectionStatus = getResources().getString(R.string.connection_error);

            txtConnectionStatus.setText(connectionStatus);

            EddieLogger.error("QuickConnectFragment.connectToNextServer(): serverEntryIP is null.");

            currentStatus = Status.IDLE;

            vpnManager.cancelVpnConnectionReservation();

            return;
        }

        try
        {
            switch(settingsManager.getAirVPNDefaultVPNType())
            {
                case SettingsManager.VPN_TYPE_OPENVPN:
                {
                    if(settingsManager.getAirVPNOpenVPNMode().equals(SettingsManager.AIRVPN_OPENVPN_MODE_DEFAULT) == false)
                    {
                        manifestMode = EddieApplication.airVPNManifest().getMode(settingsManager.getAirVPNOpenVPNMode());

                        port = manifestMode.getPort();
                        protocol = manifestMode.getProtocol();
                        entry = manifestMode.getEntryIndex();
                        tlsMode = manifestMode.getTlsMode();
                    }

                    vpnProfile = airVPNUser.getOpenVPNProfile(userKeyName, serverEntryIP.get(entry), port, protocol, tlsMode, settingsManager.getAirVPNOpenVPNCipher(), connectIPv6, mode6to4, false, "");
                }
                break;

                case SettingsManager.VPN_TYPE_WIREGUARD:
                {
                    allowedIPs = "";

                    if(connectIPv6 == false || mode6to4 == true)
                        allowedIPs = "0.0.0.0/0";

                    if(connectIPv6 == true || mode6to4 == true)
                    {
                        if(allowedIPs.isEmpty() == false)
                            allowedIPs += ", ";

                        allowedIPs += "::/0";
                    }

                    if(settingsManager.getAirVPNWireGuardMode().equals(SettingsManager.AIRVPN_WIREGUARD_MODE_DEFAULT) == false)
                    {
                        manifestMode = EddieApplication.airVPNManifest().getMode(settingsManager.getAirVPNWireGuardMode());

                        port = manifestMode.getPort();
                        entry = manifestMode.getEntryIndex();
                    }

                    vpnProfile = airVPNUser.getWireGuardProfile(userKeyName, serverEntryIP.get(entry), port, 0, 0, allowedIPs, 15, connectIPv6, mode6to4, false, "");
                }
                break;

                default:
                {
                    vpnProfile = "";
                }
            }
        }
        catch(Exception e)
        {
            vpnProfile = "";
        }

        if(settingsManager.isAirVPNServerWhitelisted(server.getName()))
        {
            EddieLogger.info(String.format(Locale.getDefault(), "Trying to quick connect favorite AirVPN server %s in %s, %s - %s, Protocol %s, Port %d", server.getName(), server.getLocation(), countryContinent.getCountryName(server.getCountryCode()), settingsManager.getAirVPNDefaultVPNType(), protocol, port));

            connectionStatus = String.format(getResources().getString(R.string.quick_connect_try_favorite_connection), server.getName(), server.getLocation(), countryContinent.getCountryName(server.getCountryCode()), settingsManager.getAirVPNDefaultVPNType(), protocol, port);
        }
        else
        {
            EddieLogger.info(String.format(Locale.getDefault(), "Trying to quick connect AirVPN server %s in %s, %s - %s, Protocol %s, Port %d", server.getName(), server.getLocation(), countryContinent.getCountryName(server.getCountryCode()), settingsManager.getAirVPNDefaultVPNType(), protocol, port));

            connectionStatus = String.format(getResources().getString(R.string.quick_connect_try_connection), server.getName(), server.getLocation(), countryContinent.getCountryName(server.getCountryCode()), settingsManager.getAirVPNDefaultVPNType(), protocol, port);
        }

        if(manifestMode != null)
            EddieLogger.info(String.format(Locale.getDefault(), "Using %s manifest mode '%s'", manifestMode.getVpnType(), manifestMode.getTitle()));

        EddieLogger.info(String.format(Locale.getDefault(), "Using user key '%s'", userKeyName));

        if(vpnProfile.isEmpty())
        {
            EddieLogger.error("QuickConnectFragment.connectToNextServer(): vpnProfile is empty");

            final Runnable uiRunnable = new Runnable()
            {
                @Override
                public void run()
                {
                    supportTools.infoDialog(R.string.quick_connect_empty_profile, true);

                    currentStatus = Status.IDLE;

                    setConnectButton();
                }
            };

            SupportTools.runOnUiActivity(getActivity(), uiRunnable);

            vpnManager.cancelVpnConnectionReservation();

            return;
        }

        final Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                if(supportTools.isConnectionProgressDialogShown() == true)
                    supportTools.setConnectionProgressDialogMessage(connectionStatus);
                else
                    supportTools.showConnectionProgressDialog(connectionStatus);

                if(txtConnectionStatus != null)
                    txtConnectionStatus.setText(connectionStatus);
            }
        };

        SupportTools.runOnUiActivity(getActivity(), runnable);

        profileInfo = new HashMap<String, String>();

        profileInfo.put("mode", String.format(Locale.getDefault(), "%d", VPN.ConnectionMode.QUICK_CONNECT.getValue()));
        profileInfo.put("vpn_type", settingsManager.getAirVPNDefaultVPNType());
        profileInfo.put("name", "quick_connect");
        profileInfo.put("profile", "quick_connect");
        profileInfo.put("status", "ok");
        profileInfo.put("description", AirVPNManifest.getFullServerDescription(server.getName()));
        profileInfo.put("server", serverEntryIP.get(entry));
        profileInfo.put("port", String.format(Locale.getDefault(), "%d", port));
        profileInfo.put("protocol", protocol);

        vpnManager.vpn().setType(VPN.Type.fromString(settingsManager.getAirVPNDefaultVPNType()));

        vpnManager.vpn().setProfileInfo(profileInfo);

        vpnManager.vpn().setConnectionMode(VPN.ConnectionMode.QUICK_CONNECT);

        vpnManager.vpn().setConnectionModeDescription(getResources().getString(R.string.conn_type_quick_connect));

        vpnManager.vpn().setUserProfileDescription(userKeyName);

        vpnManager.vpn().setUserName(airVPNUser.getUserName());

        startConnection(vpnProfile, settingsManager.getAirVPNDefaultVPNType());

        preparingNextServerConnection = false;
    }

    private void startConnection(String profile, String vpnType)
    {
        String profileString = "";

        if(vpnManager == null)
        {
            EddieLogger.error("QuickConnectFragment.startConnection(): vpnManager is null");

            return;
        }

        if(profile.isEmpty())
        {
            connectionStatus = getResources().getString(R.string.quick_connect_empty_profile);

            txtConnectionStatus.setText(connectionStatus);

            return;
        }

        vpnManager.clearProfile();

        vpnManager.setProfile(profile);

        if(vpnType.equals(SettingsManager.VPN_TYPE_OPENVPN) == true)
        {
            profileString = settingsManager.getOvpn3CustomDirectives().trim();

            if(profileString.length() > 0)
                vpnManager.addProfileString(profileString);
        }

        vpnManager.startConnection();
    }

    private void tryNextServerConnection()
    {
        if(currentStatus != Status.CONNECTION_IN_PROGRESS)
            return;

        preparingNextServerConnection = true;

        EddieLogger.error("Failed to connect server %s.", vpnManager.vpn().getServerDescription(true));

        vpnManager.stopConnection();

        // Next server will be tried when vpnManager.stop() has fully completed
        // and VPN.status will be set to NOT_CONNECTED
        // This is managed in onVpnStatusChanged()
    }

    private void updateConnectionStatus()
    {
        updateConnectionStatus(vpnManager.vpn().getConnectionStatus());
    }

    private void updateConnectionStatus(VPN.Status vpnStatus)
    {
        AirVPNServer server = null;

        setConnectButton();

        if(!isAdded())
            return;

        if(txtConnectionStatus != null)
        {
            switch(vpnStatus)
            {
                case UNDEFINED:
                case NOT_CONNECTED:
                case CONNECTION_REVOKED_BY_SYSTEM:
                {
                    if((airVPNUser.masterPassword().isEmpty() && settingsManager.isMasterPasswordEnabled() == true) || !airVPNUser.isUserValid())
                    {
                        connectionStatus = getResources().getString(R.string.quick_connect_login);

                        if(llUserKey != null)
                            llUserKey.setVisibility(View.GONE);
                    }
                    else
                    {
                        if(currentStatus != Status.CONNECTION_IN_PROGRESS || airVPNUser.isUserValid() == false)
                            connectionStatus = String.format(getResources().getString(R.string.quick_connect_start_connection), airVPNUser.getUserName());

                        if(llUserKey != null)
                            llUserKey.setVisibility(View.VISIBLE);
                    }

                    if(currentStatus != Status.CONNECTION_IN_PROGRESS)
                        currentStatus = Status.IDLE;
                }
                break;

                case CONNECTED:
                {
                    HashMap<String, String> currentProfile = vpnManager.vpn().getProfileInfo();

                    if(currentProfile != null)
                    {
                        if(!currentProfile.get("server").isEmpty())
                            connectionStatus = vpnManager.vpn().getServerDescription();
                        else
                            connectionStatus = getResources().getString(R.string.vpn_status_connected);
                    }
                    else
                        connectionStatus = getResources().getString(R.string.vpn_status_connected);

                    connectionStatus = String.format(Locale.getDefault(), getResources().getString(R.string.connected_to_server), connectionStatus);

                    connectionStatus += ". " + getResources().getString(R.string.quick_connect_tap_button_to_disconnect);

                    currentStatus = Status.CONNECTED;
                }
            }

            lastVpnStatus = vpnStatus;
        }

        if(((airVPNUser.masterPassword().isEmpty() && settingsManager.isMasterPasswordEnabled() == true) || !airVPNUser.isUserValid()) && (vpnStatus == VPN.Status.NOT_CONNECTED || vpnStatus == VPN.Status.CONNECTION_REVOKED_BY_SYSTEM))
        {
            if(airVPNUser.isLoginValid() == true)
                connectionStatus = getResources().getString(R.string.quick_connect_login);
            else
                connectionStatus = getResources().getString(R.string.quick_connect_login_error);

            if(llUserKey != null)
                llUserKey.setVisibility(View.GONE);
        }

        if(!connectionStatus.isEmpty())
            txtConnectionStatus.setText(connectionStatus);

        updateStatusBox();
    }

    public void updateVpnStatus(String vpnStatus)
    {
        if(txtVpnStatus != null)
            txtVpnStatus.setText(vpnStatus);

        updateConnectionStatus(vpnManager.vpn().getConnectionStatus());
    }

    public void setConnectButton()
    {
        SupportTools.ShowMode mode = SupportTools.ShowMode.DISABLED;

        if(btnQuickConnect == null)
            return;

        switch(vpnManager.vpn().getConnectionStatus())
        {
            case CONNECTING:
            case CONNECTED:
            case PAUSED_BY_USER:
            case PAUSED_BY_SYSTEM:
            case LOCKED:
            {
                mode = SupportTools.ShowMode.ENABLED;

                btnQuickConnect.setBackgroundResource(R.drawable.quick_connect_on);

                if(spnAirVPNKey != null)
                    spnAirVPNKey.setEnabled(false);
            }
            break;

            case DISCONNECTING:
            case NOT_CONNECTED:
            case CONNECTION_REVOKED_BY_SYSTEM:
            case CONNECTION_ERROR:
            case UNDEFINED:
            {
                mode = SupportTools.ShowMode.ENABLED;

                btnQuickConnect.setBackgroundResource(R.drawable.quick_connect_off);

                if(spnAirVPNKey != null)
                    spnAirVPNKey.setEnabled(true);
            }
            break;

            default:
            {
                mode = SupportTools.ShowMode.ENABLED;
            }
            break;
        }

        if(vpnManager.isVpnConnectionStarted() == true)
        {
            mode = SupportTools.ShowMode.ENABLED;

            btnQuickConnect.setBackgroundResource(R.drawable.quick_connect_on);

            if(spnAirVPNKey != null)
                spnAirVPNKey.setEnabled(false);
        }
        else
        {
            mode = SupportTools.ShowMode.ENABLED;

            btnQuickConnect.setBackgroundResource(R.drawable.quick_connect_off);

            if(spnAirVPNKey != null)
                spnAirVPNKey.setEnabled(true);
        }

        if(!NetworkStatusReceiver.isNetworkConnected())
        {
            mode = SupportTools.ShowMode.DISABLED;

            if(spnAirVPNKey != null)
                spnAirVPNKey.setEnabled(false);
        }

        SupportTools.setButtonStatus(btnQuickConnect, mode);
    }

    private void setupUserKeySpinner(String selectedItem)
    {
        if(llUserKey == null || spnAirVPNKey == null)
            return;

        ArrayList<String> keyNames = airVPNUser.getUserKeyNames();
        int selectedPosition = 0;

        Collections.sort(keyNames);

        if(keyNames != null && keyNames.size() > 0 && llUserKey != null && spnAirVPNKey != null)
        {
            ArrayList<String> items = new ArrayList<String>();

            for(String profile : keyNames)
                items.add(profile);

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, items);

            if(spnAirVPNKey != null)
            {
                spnAirVPNKey.setAdapter(adapter);

                for(int i = 0; i < items.size(); i++)
                {
                    if(spnAirVPNKey.getItemAtPosition(i).toString().equals(selectedItem))
                        selectedPosition = i;
                }

                spnAirVPNKey.setSelection(selectedPosition);

                spnAirVPNKey.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
                {
                    @Override
                    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id)
                    {
                        selectedUserKey = spnAirVPNKey.getItemAtPosition(position).toString();

                        airVPNUser.setCurrentKey(getActivity(), selectedUserKey);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parentView)
                    {
                    }
                });
            }

            if(llUserKey != null)
                llUserKey.setVisibility(View.VISIBLE);
        }
        else
        {
            if(llUserKey != null)
                llUserKey.setVisibility(View.GONE);
        }
    }

    void loadConnectionSchemes()
    {
        AssetManager assetManager = getContext().getAssets();
        InputStream inputStream = null;
        ConnectionScheme connectionScheme = null;
        int intVal = 0;

        try
        {
            inputStream = assetManager.open(AIRVPN_CONNECTION_SEQUENCE_FILE_NAME);

            if(inputStream != null)
            {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                String line = "";
                String row[] = null;

                connectionSchemeList = new ArrayList<ConnectionScheme>();

                while((line = bufferedReader.readLine()) != null)
                {
                    connectionScheme = new ConnectionScheme();

                    row = line.split(",");

                    if(row != null && row.length == 3)
                    {
                        connectionScheme.setProtocol(row[0]);

                        try
                        {
                            intVal = Integer.parseInt(row[1]);
                        }
                        catch(NumberFormatException e)
                        {
                            intVal = 0;
                        }

                        connectionScheme.setPort(intVal);

                        try
                        {
                            intVal = Integer.parseInt(row[2]);
                        }
                        catch(NumberFormatException e)
                        {
                            intVal = 0;
                        }

                        connectionScheme.setEntry(intVal);

                        connectionSchemeList.add(connectionScheme);
                    }
                }
            }
        }
        catch(Exception e)
        {
            EddieLogger.warning("QuickConnectFragment.loadConnectionSchemes(): %s not found.", AIRVPN_CONNECTION_SEQUENCE_FILE_NAME);
        }
        finally
        {
            try
            {
                inputStream.close();
            }
            catch(Exception e)
            {
            }
        }
    }

    private void checkPermissions()
    {
        final Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
                {
                    if(EddieApplication.applicationContext().checkSelfPermission(Manifest.permission.POST_NOTIFICATIONS) != PackageManager.PERMISSION_GRANTED)
                    {
                        EddieLogger.warning("Notifications are not granted to the app");

                        if(supportTools.confirmationDialog(R.string.notifications_permissions_not_granted_warning) == true)
                        {
                            String channelId = getResources().getString(R.string.notification_channel_id);

                            Intent channelSettingsIntent = new Intent(Settings.ACTION_CHANNEL_NOTIFICATION_SETTINGS);

                            if(channelSettingsIntent != null)
                            {
                                channelSettingsIntent.putExtra(Settings.EXTRA_APP_PACKAGE, getContext().getPackageName());
                                channelSettingsIntent.putExtra(Settings.EXTRA_CHANNEL_ID, channelId);
                                channelSettingsIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

                                getContext().startActivity(channelSettingsIntent);
                            }
                        }
                    }
                }

                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N && SupportTools.isTVDevice() == false)
                {
                    if(settingsManager.isAlwaysOnVpnSet() == false)
                    {
                        if(settingsManager.isStartVpnAtStartupEnabled() == true)
                        {
                            EddieLogger.warning("'VPN Always on' is not granted to the app");

                            if(supportTools.confirmationDialog(R.string.vpn_always_on_not_granted_warning) == true)
                            {
                                Intent vpnSettingsIntent = new Intent(Settings.ACTION_VPN_SETTINGS);

                                if(vpnSettingsIntent != null)
                                    getContext().startActivity(vpnSettingsIntent);
                            }
                        }
                    }
                    else
                    {
                        if(settingsManager.isVpnLockdownOn() == true && settingsManager.isVpnLockEnabled() && vpnManager.vpn().getType() == VPN.Type.OPENVPN)
                        {
                            supportTools.infoDialog(R.string.vpn_lockdown_warning, true);

                            settingsManager.setVpnLock(false);

                            EddieLogger.warning("System's \"Block connections without VPN\" is enabled. Eddie's VPN Lock will be disabled.");
                        }
                    }
                }
            }
        };

        SupportTools.runOnUiActivity(getActivity(), runnable);
    }

    // Eddie events

    public void onVpnConnectionStatsChanged(final VPNConnectionStats connectionStats)
    {
    }

    public void onVpnStatusChanged(final VPN.Status vpnStatus, final String message)
    {
        if((vpnStatus == VPN.Status.NOT_CONNECTED || vpnStatus == VPN.Status.CONNECTION_REVOKED_BY_SYSTEM) && currentStatus != Status.CONNECTION_IN_PROGRESS)
            currentStatus = Status.IDLE;

        if(preparingNextServerConnection == true && currentStatus == Status.CONNECTION_IN_PROGRESS && vpnStatus == VPN.Status.NOT_CONNECTED)
            connectToNextServer();

        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                updateVpnStatus(message);

                setConnectButton();
            }
        };

        SupportTools.runOnUiActivity(getActivity(), runnable);
    }

    public void onVpnAuthFailed(final VPNEvent oe)
    {
        synchronized(this)
        {
            if(preparingNextServerConnection == false && currentStatus == Status.CONNECTION_IN_PROGRESS)
            {
                preparingNextServerConnection = true;

                tryNextServerConnection();
            }
            else
            {
                Runnable runnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        setConnectButton();
                    }
                };

                SupportTools.runOnUiActivity(getActivity(), runnable);
            }
        }
    }

    public void onVpnError(final VPNEvent oe)
    {
        synchronized(this)
        {
            if(vpnManager.vpn().getConnectionMode() == VPN.ConnectionMode.QUICK_CONNECT && preparingNextServerConnection == false && currentStatus == Status.CONNECTION_IN_PROGRESS)
            {
                preparingNextServerConnection = true;

                tryNextServerConnection();
            }
            else
            {
                Runnable runnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        setConnectButton();
                    }
                };

                SupportTools.runOnUiActivity(getActivity(), runnable);
            }
        }
    }

    public void onVpnReconnect(final VPNEvent oe)
    {
    }

    public void onMasterPasswordChanged()
    {
    }

    public void onAirVPNLogin()
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                connectionStatus = String.format(getResources().getString(R.string.quick_connect_start_connection), airVPNUser.getUserName());

                if(txtConnectionStatus != null)
                    txtConnectionStatus.setText(connectionStatus);

                if(airVPNUser != null)
                {
                    selectedUserKey = airVPNUser.getCurrentKey();

                    setupUserKeySpinner(selectedUserKey);
                }

                if(txtAirVPNSubscriptionStatus != null)
                {
                    txtAirVPNSubscriptionStatus.setVisibility(View.VISIBLE);

                    txtAirVPNSubscriptionStatus.setText(airVPNUser.getExpirationText());
                }

                currentStatus = Status.IDLE;
            }
        };

        SupportTools.runOnUiActivity(getActivity(), runnable);
    }

    public void onAirVPNLoginFailed(String message)
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                connectionStatus = getResources().getString(R.string.quick_connect_login_error);

                if(message != null && message.isEmpty() == false)
                {
                    connectionStatus += " (" + message + ")";

                    EddieLogger.error("AirVPN Login Failed: %s", message);
                }

                txtConnectionStatus.setText(connectionStatus);

                txtAirVPNSubscriptionStatus.setVisibility(View.GONE);

                currentStatus = Status.IDLE;
            }
        };

        SupportTools.runOnUiActivity(getActivity(), runnable);
    }

    public void onAirVPNLogout()
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                if(llUserKey != null)
                    llUserKey.setVisibility(View.GONE);

                connectionStatus = getResources().getString(R.string.quick_connect_login);

                if(txtConnectionStatus != null)
                    txtConnectionStatus.setText(connectionStatus);

                if(txtAirVPNSubscriptionStatus != null)
                    txtAirVPNSubscriptionStatus.setVisibility(View.GONE);

                currentStatus = Status.IDLE;
            }
        };

        SupportTools.runOnUiActivity(getActivity(), runnable);
    }

    public void onAirVPNUserDataChanged()
    {
    }

    public void onAirVPNUserProfileReceived(Document document)
    {
    }

    public void onAirVPNUserProfileDownloadError()
    {
        supportTools.dismissProgressDialog();
    }

    public void onAirVPNUserProfileChanged()
    {
    }

    public void onAirVPNManifestReceived(Document document)
    {
    }

    public void onAirVPNManifestDownloadError()
    {
    }

    public void onAirVPNManifestChanged()
    {
        if(supportTools.isProgressDialogShown() == true)
        {
            supportTools.dismissProgressDialog();

            if(waitForManifest == true)
                btnQuickConnect.callOnClick();
        }
    }

    public void onAirVPNIgnoredManifestDocumentRequest()
    {
    }

    public void onAirVPNIgnoredUserDocumentRequest()
    {
    }

    public void onAirVPNRequestError(String message)
    {
    }

    public void onCancelConnection()
    {
        currentStatus = Status.IDLE;

        preparingNextServerConnection = false;

        vpnManager.stopConnection();

        setConnectButton();

        updateConnectionStatus(vpnManager.vpn().getConnectionStatus());
    }

    public void onSaveState()
    {
    }

    public void onRestoreState(Bundle bundle)
    {
        setConnectButton();
    }

    // NetworkStatusReceiver

    public void onNetworkStatusNotAvailable()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));

        if(btnQuickConnect != null)
            SupportTools.setButtonStatus(btnQuickConnect, SupportTools.ShowMode.DISABLED);

        if(spnAirVPNKey != null)
            spnAirVPNKey.setEnabled(false);

        if(txtConnectionStatus != null)
            txtConnectionStatus.setText(R.string.network_is_not_available);
    }

    public void onNetworkStatusConnected()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(String.format(Locale.getDefault(), getResources().getString(R.string.conn_status_connected), NetworkStatusReceiver.getNetworkDescription()));

        if(btnQuickConnect != null)
            SupportTools.setButtonStatus(btnQuickConnect, SupportTools.ShowMode.ENABLED);

        if(spnAirVPNKey != null && vpnManager.vpn() != null)
        {
            if(vpnManager.vpn().getConnectionStatus() == VPN.Status.CONNECTED)
                spnAirVPNKey.setEnabled(false);
            else
                spnAirVPNKey.setEnabled(true);
        }

        if(txtConnectionStatus != null)
            txtConnectionStatus.setText(connectionStatus);
    }

    public void onNetworkStatusIsConnecting()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));
    }

    public void onNetworkStatusIsDisconnecting()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));
    }

    public void onNetworkStatusSuspended()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));
    }

    public void onNetworkStatusNotConnected()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_disconnected));
    }

    public void onNetworkTypeChanged()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_disconnected));
    }
}