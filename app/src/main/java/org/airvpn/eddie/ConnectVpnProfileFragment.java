// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 3 October 2018 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import android.view.ContextMenu;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class ConnectVpnProfileFragment extends Fragment implements NetworkStatusListener, EddieEventListener
{
    private final int FRAGMENT_ID = 5003;

    private static VPNManager vpnManager = null;
    private NetworkStatusReceiver networkStatusReceiver = null;

    private ScrollView scrollView = null;

    private Button btnSelectProfile = null, btnConnectProfile = null, btnDeleteProfile = null;
    private ImageButton btnSelectAll = null;

    private TextView txtVpnProfilesCap = null, txtProfileFileName = null;
    private TextView txtVpnType = null, txtServerName = null, txtServerPort = null, txtServerProtocol = null;
    private TextView txtVpnStatus = null, txtNetworkStatus = null;

    private LinearLayout llServerInfo = null;

    private ListView lvVpnProfiles = null;

    private Uri profileUri = null;
    private String profileData = "", currentVpnStatusDescription = "";
    private HashMap<String, String> profileInfo = null;

    private boolean loadExternalUriProfile = false, deleteProfileEnabled = false, selectAll = true;

    private SupportTools supportTools = null;
    private EddieEvent eddieEvent = null;
    private SettingsManager settingsManager = null;
    private VPNProfileDatabase vpnProfileDatabase = null;

    public void setVpnManager(VPNManager v)
    {
        vpnManager = v;
    }

    public class ProfileAdapter extends ArrayAdapter<String>
    {
        public ProfileAdapter(Context context, ArrayList<String> profiles)
        {
            super(context, 0, profiles);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            VPNProfileDatabase.VPNProfile vpnProfile = null;
            String profile = getItem(position);

            if(convertView == null)
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.vpn_profile_item, parent, false);

            TextView txtProfile = (TextView)convertView.findViewById(R.id.profile_name);
            ImageView imgBoot = (ImageView)convertView.findViewById(R.id.profile_boot);
            ImageView imgIcon = (ImageView)convertView.findViewById(R.id.profile_icon);

            txtProfile.setText(profile);

            vpnProfile = vpnProfileDatabase.getProfile(profile);

            if(vpnProfile != null)
            {
                if(vpnProfile.getBoot() == true)
                    imgBoot.setVisibility(View.VISIBLE);
                else
                    imgBoot.setVisibility(View.GONE);

                switch(vpnProfile.getType())
                {
                    case OPENVPN:
                    {
                        imgIcon.setImageResource(R.drawable.openvpn);
                        imgIcon.setVisibility(View.VISIBLE);
                    }
                    break;

                    case WIREGUARD:
                    {
                        imgIcon.setImageResource(R.drawable.wireguard);
                        imgIcon.setVisibility(View.VISIBLE);
                    }
                    break;

                    default:
                    {
                        imgIcon.setVisibility(View.GONE);
                    }
                    break;
                }
            }
            else
            {
                imgBoot.setVisibility(View.GONE);
                imgIcon.setVisibility(View.GONE);
            }

            return convertView;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        supportTools = EddieApplication.supportTools();
        settingsManager = EddieApplication.settingsManager();

        vpnManager = EddieApplication.vpnManager();

        vpnProfileDatabase = new VPNProfileDatabase(getActivity());

        networkStatusReceiver = EddieApplication.networkStatusReceiver();

        networkStatusReceiver.subscribeListener(this);

        eddieEvent = EddieApplication.eddieEvent();

        eddieEvent.subscribeListener(this);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        if(networkStatusReceiver != null)
            networkStatusReceiver.unsubscribeListener(this);

        if(eddieEvent != null)
            eddieEvent.unsubscribeListener(this);
    }

    @Override
    public void onResume()
    {
        super.onResume();

        if(txtNetworkStatus != null)
        {
            if (NetworkStatusReceiver.getNetworkStatus() == NetworkStatusReceiver.Status.CONNECTED)
                txtNetworkStatus.setText(String.format(Locale.getDefault(), getResources().getString(R.string.conn_status_connected), NetworkStatusReceiver.getNetworkDescription()));
            else
                txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));
        }

        if(txtVpnStatus != null)
            txtVpnStatus.setText(currentVpnStatusDescription);

        if(lvVpnProfiles != null)
            setupProfileListView();

        setConnectButton();
    }

    @Override
    public void setUserVisibleHint(boolean isVisible)
    {
        super.setUserVisibleHint(isVisible);

        if(isVisible && lvVpnProfiles != null)
            setupProfileListView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View fragmentView = inflater.inflate(R.layout.fragment_vpn_profile_layout, container, false);

        if(fragmentView != null)
        {
            scrollView = (ScrollView)fragmentView.findViewById(R.id.scroll_view);

            btnSelectProfile = (Button)fragmentView.findViewById(R.id.select_profile_btn);

            btnSelectProfile.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View arg0)
                {
                    onClickSelectProfileButton();
                }
            });

            txtVpnProfilesCap = (TextView)fragmentView.findViewById(R.id.vpn_profiles_title);

            txtProfileFileName = (TextView)fragmentView.findViewById(R.id.profile_filename);
            txtProfileFileName.setText(getResources().getString(R.string.conn_no_profile));

            llServerInfo = (LinearLayout)fragmentView.findViewById(R.id.server_info_layout);

            txtVpnType = (TextView)fragmentView.findViewById(R.id.profile_type);
            txtVpnType.setText("");

            txtServerName = (TextView)fragmentView.findViewById(R.id.profile_server);
            txtServerName.setText("");

            txtServerPort = (TextView)fragmentView.findViewById(R.id.profile_port);
            txtServerPort.setText("");

            txtServerProtocol = (TextView)fragmentView.findViewById(R.id.profile_protocol);
            txtServerProtocol.setText("");

            btnConnectProfile = (Button)fragmentView.findViewById(R.id.connect_profile_btn);

            btnConnectProfile.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View arg0)
                {
                    if(profileInfo == null)
                    {
                        supportTools.setButtonStatus(btnConnectProfile, SupportTools.ShowMode.DISABLED);

                        return;
                    }

                    final Runnable uiRunnable = new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            connectVPNProfile(profileInfo.get("name"));
                        }
                    };

                    SupportTools.runOnUiActivity(getActivity(), uiRunnable);
                }
            });

            btnDeleteProfile = (Button)fragmentView.findViewById(R.id.delete_profile_btn);

            btnDeleteProfile.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View arg0)
                {
                    onClickDeleteProfileButton();
                }
            });

            btnSelectAll = (ImageButton)fragmentView.findViewById(R.id.select_all_btn);

            btnSelectAll.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View arg0)
                {
                    onClickSelectAllButton();
                }
            });

            if(settingsManager.isStartVpnAtStartupEnabled())
                restoreLastProfile();
            else
            {
                txtProfileFileName.setText(getResources().getString(R.string.conn_no_profile));

                llServerInfo.setVisibility(View.GONE);
            }

            if(currentVpnStatusDescription.isEmpty())
                currentVpnStatusDescription = getResources().getString(vpnManager.vpn().connectionStatusResourceDescription(vpnManager.vpn().getConnectionStatus()));

            txtVpnStatus = (TextView)fragmentView.findViewById(R.id.vpn_connection_status);
            txtVpnStatus.setText(currentVpnStatusDescription);

            txtNetworkStatus = (TextView)fragmentView.findViewById(R.id.network_connection_status);

            if(NetworkStatusReceiver.getNetworkStatus() == NetworkStatusReceiver.Status.CONNECTED)
                txtNetworkStatus.setText(String.format(Locale.getDefault(), getResources().getString(R.string.conn_status_connected), NetworkStatusReceiver.getNetworkDescription()));
            else
                txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));

            lvVpnProfiles = (ListView)fragmentView.findViewById(R.id.vpn_profiles);

            lvVpnProfiles.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
            {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
                {
                    if(!lvVpnProfiles.hasFocus())
                        return;

                    View itemView = lvVpnProfiles.getChildAt(position);

                    if(itemView == null)
                        return;

                    try
                    {
                        Display display = requireActivity().getWindowManager().getDefaultDisplay();
                        Point size = new Point();

                        display.getSize(size);

                        int ypos = itemView.getTop() + lvVpnProfiles.getTop() - (size.y / 2);

                        scrollView.scrollTo(0, ypos);
                    }
                    catch(NullPointerException e)
                    {
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent)
                {
                }
            });

            lvVpnProfiles.setOnItemClickListener(new AdapterView.OnItemClickListener()
            {
                @Override
                public void onItemClick(final AdapterView<?>adapter, final View v, final int position, final long id)
                {
                    final Runnable uiRunnable = new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            connectVPNProfile(adapter.getItemAtPosition(position).toString());
                        }
                    };

                    SupportTools.runOnUiActivity(getActivity(), uiRunnable);
                }
            });

            if(lvVpnProfiles != null)
                registerForContextMenu(lvVpnProfiles);

            setConnectButton();

            setupProfileListView();

            if(loadExternalUriProfile == true)
            {
                Runnable runnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        selectVPNProfile(profileUri);
                    }
                };

                SupportTools.runOnUiActivity(getActivity(), runnable);

                loadExternalUriProfile = false;
            }
        }

        return fragmentView;
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);

        if(v.getId() == R.id.vpn_profiles)
        {
            MenuInflater inflater = getActivity().getMenuInflater();

            inflater.inflate(R.menu.vpn_profile_menu, menu);

            for(int i = 0; i < menu.size(); i++)
            {
                MenuItem item = menu.getItem(i);

                if(item != null)
                {
                    final AdapterView.AdapterContextMenuInfo adapterMenuInfo = (AdapterView.AdapterContextMenuInfo)menuInfo;
                    final String listViewItemValue = lvVpnProfiles.getAdapter().getItem(adapterMenuInfo.position).toString();

                    VPNProfileDatabase.VPNProfile vpnProfile = vpnProfileDatabase.getProfile(listViewItemValue);

                    if(vpnProfile != null)
                    {
                        if(item.getItemId() == R.id.context_menu_set_boot_vpn_profile)
                        {
                            if(vpnProfile.getBoot() == true)
                                item.setVisible(false);
                            else
                                item.setVisible(true);
                        }
                        else if(item.getItemId() == R.id.context_menu_unset_boot_vpn_profile)
                        {
                            if(vpnProfile.getBoot() == true)
                                item.setVisible(true);
                            else
                                item.setVisible(false);
                        }
                        else if(item.getItemId() == R.id.context_menu_export_openvpn_profile)
                        {
                            if(vpnProfile.getType() == VPNProfileDatabase.ProfileType.OPENVPN)
                                item.setVisible(true);
                            else
                                item.setVisible(false);
                        }
                        else if(item.getItemId() == R.id.context_menu_export_wireguard_profile)
                        {
                            if(vpnProfile.getType() == VPNProfileDatabase.ProfileType.WIREGUARD)
                                item.setVisible(true);
                            else
                                item.setVisible(false);
                        }
                        else
                            item.setVisible(true);
                    }
                    else
                        item.setVisible(false);

                    Intent intent = new Intent();
                    intent.putExtra("fragment_id", FRAGMENT_ID);

                    item.setIntent(intent);
                }
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem menuItem)
    {
        boolean processed = false;

        Intent intent = menuItem.getIntent();

        if(intent != null && intent.getIntExtra("fragment_id", -1) != FRAGMENT_ID)
            return false;

        AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();
        final String listViewItemValue = lvVpnProfiles.getAdapter().getItem(menuInfo.position).toString();

        switch(menuItem.getItemId())
        {
            case R.id.context_menu_connect_vpn_profile:
            {
                connectVPNProfile(listViewItemValue);

                processed = true;
            }
            break;

            case R.id.context_menu_rename_vpn_profile:
            {
                Runnable runnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        renameVpnProfile(listViewItemValue);
                    }
                };

                SupportTools.runOnUiActivity(getActivity(), runnable);

                processed = true;
            }
            break;

            case R.id.context_menu_delete_vpn_profile:
            {
                Runnable runnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        deleteVpnProfile(listViewItemValue);
                    }
                };

                SupportTools.runOnUiActivity(getActivity(), runnable);

                processed = true;
            }
            break;

            case R.id.context_menu_set_boot_vpn_profile:
            {
                setBootVpnProfile(listViewItemValue);

                processed = true;
            }
            break;

            case R.id.context_menu_unset_boot_vpn_profile:
            {
                unsetBootVpnProfile(listViewItemValue);

                processed = true;
            }
            break;

            case R.id.context_menu_export_openvpn_profile:
            case R.id.context_menu_export_wireguard_profile:
            {
                Runnable runnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        exportVpnProfile(listViewItemValue);
                    }
                };

                SupportTools.runOnUiActivity(getActivity(), runnable);

                processed = true;
            }
            break;
        }

        return processed;
    }

    private void setupProfileListView()
    {
        ArrayList<String> profileList = vpnProfileDatabase.getProfileNameList(VPNProfileDatabase.SortMode.SORT_NAMES);

        ProfileAdapter profileAdapter = new ProfileAdapter(getActivity(), profileList);

        if(profileAdapter.getCount() > 0)
        {
            lvVpnProfiles.setVisibility(View.VISIBLE);

            txtVpnProfilesCap.setText(String.format(Locale.getDefault(), getResources().getString(R.string.vpn_profiles_cap), profileAdapter.getCount()));

            lvVpnProfiles.setAdapter(profileAdapter);

            expandListViewHeight(lvVpnProfiles);

            btnDeleteProfile.setText(String.format(Locale.getDefault(), getResources().getString(R.string.delete_profiles), " ..."));

            btnDeleteProfile.setVisibility(View.VISIBLE);

            btnSelectAll.setVisibility(View.GONE);
        }
        else
        {
            lvVpnProfiles.setVisibility(View.GONE);

            txtVpnProfilesCap.setText(R.string.no_vpn_profiles);

            btnDeleteProfile.setVisibility(View.GONE);

            btnSelectAll.setVisibility(View.GONE);
        }
    }

    public void loadExternalProfile(Uri profile)
    {
        profileUri = profile;

        loadExternalUriProfile = true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(data != null)
        {
            Runnable runnable = new Runnable()
            {
                @Override
                public void run()
                {
                    loadExternalUriProfile = false;

                    if(data.getClipData() != null)
                    {
                        int count = data.getClipData().getItemCount();
                        int ndx = 0;

                        while(ndx < count)
                        {
                            profileUri = data.getClipData().getItemAt(ndx).getUri();

                            selectVPNProfile(profileUri);

                            ndx++;
                        }
                    }
                    else if(data.getData() != null)
                    {
                        profileUri = data.getData();

                        selectVPNProfile(profileUri);
                    }
                }
            };

            SupportTools.runOnUiActivity(getActivity(), runnable);
        }
    }

    private void onClickSelectProfileButton()
    {
        Intent fileChooserIntent = new Intent();

        if(fileChooserIntent != null)
        {
            fileChooserIntent.setAction(Intent.ACTION_GET_CONTENT);
            fileChooserIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            fileChooserIntent.setType("*/*");

            startActivityForResult(Intent.createChooser(fileChooserIntent, getResources().getString(R.string.conn_select_profile_cap)), MainActivity.ACTIVITY_RESULT_FILE_CHOOSER);
        }
        else
            supportTools.infoDialog(R.string.cannot_open_file_chooser, true);
    }

    private void onClickDeleteProfileButton()
    {
        View v;
        CheckBox cb;
        String profileName;
        int i;

        if(deleteProfileEnabled == false)
        {
            for(i = 0; i < lvVpnProfiles.getCount(); i++)
            {
                v = lvVpnProfiles.getChildAt(i);

                cb = (CheckBox)v.findViewById(R.id.profile_select);

                cb.setVisibility(View.VISIBLE);

                cb.setChecked(false);

                cb.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        updateDeleteProfileButton();
                    }
                });
            }

            deleteProfileEnabled = true;

            selectAll = true;

            btnDeleteProfile.setText(String.format(Locale.getDefault(), getResources().getString(R.string.delete_profiles), " (0)"));

            btnSelectAll.setVisibility(View.VISIBLE);
        }
        else
        {
            if(countSelectedProfiles() > 0)
            {
                if(supportTools.confirmationDialog(String.format(Locale.getDefault(), getResources().getString(R.string.confirm_delete_profiles), countSelectedProfiles())) == true)
                {
                    for(i = 0; i < lvVpnProfiles.getCount(); i++)
                    {
                        v = lvVpnProfiles.getChildAt(i);

                        cb = (CheckBox)v.findViewById(R.id.profile_select);

                        if(cb.isChecked() == true)
                        {
                            profileName = lvVpnProfiles.getAdapter().getItem(i).toString();

                            vpnProfileDatabase.deleteProfile(profileName);
                        }
                    }
                }
            }

            profileInfo = null;

            deleteProfileEnabled = false;

            btnDeleteProfile.setText(String.format(Locale.getDefault(), getResources().getString(R.string.delete_profiles), " ..."));

            btnSelectAll.setVisibility(View.GONE);

            setupProfileListView();

            showVpnProfileInfo();
        }
    }

    private void onClickSelectAllButton()
    {
        View v;
        CheckBox cb;

        if(deleteProfileEnabled == false)
            return;

        for(int i = 0; i < lvVpnProfiles.getCount(); i++)
        {
            v = lvVpnProfiles.getChildAt(i);

            cb = (CheckBox)v.findViewById(R.id.profile_select);

            cb.setChecked(selectAll);
        }

        selectAll = !selectAll;

        updateDeleteProfileButton();
    }

    private void updateDeleteProfileButton()
    {
        btnDeleteProfile.setText(String.format(Locale.getDefault(), getResources().getString(R.string.delete_profiles), String.format(Locale.getDefault(), " (%d)", countSelectedProfiles())));
    }

    private int countSelectedProfiles()
    {
        View v = null;
        CheckBox cb = null;
        int count = 0;

        if(deleteProfileEnabled == false)
            return 0;

        for(int i = 0; i < lvVpnProfiles.getCount(); i++)
        {
            v = lvVpnProfiles.getChildAt(i);

            cb = (CheckBox)v.findViewById(R.id.profile_select);

            if(cb.isChecked() == true)
                count++;
        }

        return count;
    }

    public void selectVPNProfile(Uri profile)
    {
        if(profile == null)
            return;

        profileInfo = supportTools.getVPNProfile(profile);

        if(profileInfo != null)
        {
            if(profileInfo.get("status").equals("ok"))
            {
                profileData = profileInfo.get("profile");

                if(profileInfo.containsKey("name") == true)
                    txtProfileFileName.setText(profileInfo.get("name"));
                else
                    txtProfileFileName.setText("???");

                if(profileInfo.containsKey("vpn_type") == true)
                    txtVpnType.setText(profileInfo.get("vpn_type"));
                else
                    txtVpnType.setText("???");

                if(profileInfo.containsKey("server") == true)
                {
                    txtServerName.setText(profileInfo.get("server"));

                    profileInfo.put("description", AirVPNManifest.getFullServerDescriptionByIP(profileInfo.get("server")));
                }
                else
                {
                    txtServerName.setText("???");

                    profileInfo.put("description", "");
                }

                if(profileInfo.containsKey("port") == true)
                    txtServerPort.setText(profileInfo.get("port"));
                else
                    txtServerPort.setText("???");

                if(profileInfo.containsKey("protocol") == true)
                    txtServerProtocol.setText(profileInfo.get("protocol"));
                else
                    txtServerProtocol.setText("???");

                llServerInfo.setVisibility(View.VISIBLE);

                supportTools.setButtonStatus(btnConnectProfile, SupportTools.ShowMode.ENABLED);

                addVPNProfile(profileInfo, profileData);
            }
            else
            {
                int errMsg = 0;

                if(profileInfo.containsKey("status") == true)
                {
                    if(profileInfo.get("status").equals("not_found"))
                        errMsg = R.string.conn_profile_not_found;
                    else if(profileInfo.get("status").equals("invalid"))
                        errMsg = R.string.conn_profile_is_invalid;
                    else if(profileInfo.get("status").equals("no_permission"))
                        errMsg = R.string.conn_profile_no_permission;

                    supportTools.infoDialog(errMsg, true);
                }

                profileData = "";

                profileInfo.put("mode", String.format(Locale.getDefault(), "%d", VPN.ConnectionMode.UNKNOWN.getValue()));
                profileInfo.put("vpn_type", VPN.Type.UNKNOWN.toString());
                profileInfo.put("name", "");
                profileInfo.put("description", "");
                profileInfo.put("server", "");
                profileInfo.put("port", "");
                profileInfo.put("protocol", "");

                txtProfileFileName.setText(getResources().getString(R.string.conn_no_profile));

                llServerInfo.setVisibility(View.GONE);
            }

            supportTools.saveLastConnectedProfile(profileInfo, profileData);

            switch(VPN.Type.fromString(profileInfo.get("vpn_type")))
            {
                case OPENVPN:
                {
                    vpnManager.vpn().setType(VPN.Type.OPENVPN);
                    vpnManager.vpn().setConnectionMode(VPN.ConnectionMode.OPENVPN_PROFILE);
                    vpnManager.vpn().setConnectionModeDescription(getResources().getString(R.string.conn_type_openvpn_profile));
                }
                break;

                case WIREGUARD:
                {
                    vpnManager.vpn().setType(VPN.Type.WIREGUARD);
                    vpnManager.vpn().setConnectionMode(VPN.ConnectionMode.WIREGUARD_PROFILE);
                    vpnManager.vpn().setConnectionModeDescription(getResources().getString(R.string.conn_type_wireguard_profile));
                }
                break;

                default:
                {
                    vpnManager.vpn().setType(VPN.Type.UNKNOWN);
                    vpnManager.vpn().setConnectionMode(VPN.ConnectionMode.UNKNOWN);
                    vpnManager.vpn().setConnectionModeDescription("");
                }
                break;
            }

            vpnManager.vpn().setProfileInfo(profileInfo);

            vpnManager.vpn().setUserProfileDescription("");

            vpnManager.vpn().setUserName("");
        }
    }

    private void restoreLastProfile()
    {
        profileData = settingsManager.getLastVPNProfile();

        profileInfo = settingsManager.getSystemLastProfileInfo();

        showVpnProfileInfo();
    }

    private String getVPNProfileName(String profileName)
    {
        boolean nameIsValid = false;
        String newProfileName = profileName;
        int dialogTitle = -1;

        if(profileName == null)
            return "";

        dialogTitle = R.string.vpn_profile_rename;

        while(!nameIsValid)
        {
            newProfileName = supportTools.getTextOptionDialog(getActivity(), dialogTitle, profileName, SupportTools.EditOption.DO_NOT_ALLOW_EMPTY_FIELD);

            if(!profileName.equals(newProfileName))
            {
                if(vpnProfileDatabase.exists(newProfileName))
                    dialogTitle = R.string.vpn_profile_name_exists;
                else
                    nameIsValid = true;
            }
            else
                nameIsValid = true;
        }

        return newProfileName;
    }

    private boolean addVPNProfile(HashMap<String, String> pInfo, String pData)
    {
        boolean profileNameExists = false;
        VPNProfileDatabase.VPNProfile vpnProfile = null;
        String profileName = "", newProfileName = "";
        int value;
        VPNProfileDatabase.ProtocolType proto = VPNProfileDatabase.ProtocolType.UNKNOWN;

        if(pInfo == null || pInfo.get("name") == null || pData == null || pData.isEmpty())
            return false;

        if(vpnProfileDatabase == null || pInfo.get("name").isEmpty() || pInfo.get("name").equals("???"))
            return false;

        profileName = pInfo.get("name");

        profileNameExists = vpnProfileDatabase.exists(profileName);

        if(profileNameExists)
        {
            while(profileNameExists)
            {
                newProfileName = supportTools.getTextOptionDialog(getActivity(), R.string.vpn_profile_name_exists, profileName, SupportTools.EditOption.DO_NOT_ALLOW_EMPTY_FIELD);

                if(!profileName.equals(newProfileName))
                    profileNameExists = vpnProfileDatabase.exists(newProfileName);
                else
                    return false;
            }

            pInfo.put("name", newProfileName);
        }

        vpnProfile = vpnProfileDatabase.new VPNProfile();

        vpnProfile.setName(pInfo.get("name"));

        switch(pInfo.get("vpn_type"))
        {
            case "OpenVPN":
            {
                vpnProfile.setType(VPNProfileDatabase.ProfileType.OPENVPN);
            }
            break;

            case "WireGuard":
            {
                vpnProfile.setType(VPNProfileDatabase.ProfileType.WIREGUARD);
            }
            break;

            default:
            {
                vpnProfile.setType(VPNProfileDatabase.ProfileType.UNKNOWN);
            }
            break;
        }

        vpnProfile.setIpAddress(pInfo.get("server"));

        try
        {
            value = Integer.parseInt(pInfo.get("port"));
        }
        catch(NumberFormatException e)
        {
            value = 0;
        }

        vpnProfile.setPort(value);

        switch(pInfo.get("protocol").toUpperCase(Locale.US))
        {
            case "UDP":
            {
                proto = VPNProfileDatabase.ProtocolType.UDPv4;
            }
            break;

            case "TCP":
            {
                proto = VPNProfileDatabase.ProtocolType.TCPv4;
            }
            break;

            case "UDP6":
            {
                proto = VPNProfileDatabase.ProtocolType.UDPv6;
            }
            break;

            case "TCP6":
            {
                proto = VPNProfileDatabase.ProtocolType.TCPv6;
            }
            break;

            default:
            {
                proto = VPNProfileDatabase.ProtocolType.UNKNOWN;
            }
            break;
        }

        vpnProfile.setProtocol(proto);

        vpnProfile.setProfile(pData);

        AirVPNServer airVPNServer = AirVPNManifest.getServerByIP(pInfo.get("server"));

        if(airVPNServer != null)
        {
            vpnProfile.setAirVPNServerName(airVPNServer.getName());
            vpnProfile.setAirVPNServerLocation(airVPNServer.getLocation());
            vpnProfile.setAirVPNServerCountry(CountryContinent.getCountryName(airVPNServer.getCountryCode()));
        }
        else
        {
            vpnProfile.setAirVPNServerName("");
            vpnProfile.setAirVPNServerLocation("");
            vpnProfile.setAirVPNServerCountry("");
        }

        vpnProfile.setBoot(false);

        vpnProfileDatabase.setProfile(pInfo.get("name"), vpnProfile);

        setupProfileListView();

        return true;
    }

    private void renameVpnProfile(String profileName)
    {
        if(profileName == null || profileName.isEmpty())
            return;

        String newProfileName = getVPNProfileName(profileName);

        vpnProfileDatabase.renameProfile(profileName, newProfileName);

        setupProfileListView();
    }

    private void deleteVpnProfile(String profileName)
    {
        if(profileName == null || profileName.isEmpty())
            return;

        if(!supportTools.confirmationDialog(String.format(getResources().getString(R.string.vpn_profile_delete), profileName)))
            return;

        vpnProfileDatabase.deleteProfile(profileName);

        setupProfileListView();

        profileInfo = null;

        showVpnProfileInfo();
    }

    private void connectVPNProfile(String profileName)
    {
        String progressMessage = "";

        VPN.Status currentConnectionStatus = vpnManager.vpn().getConnectionStatus();

        if(profileName == null || profileName.isEmpty() || !NetworkStatusReceiver.isNetworkConnected())
            return;

        VPNProfileDatabase.VPNProfile vpnProfile = vpnProfileDatabase.getProfile(profileName);

        if(vpnProfile == null)
        {
            supportTools.infoDialog(R.string.vpn_profile_not_found, true);

            return;
        }

        profileData = vpnProfile.getProfile();

        profileInfo = new HashMap<String, String>();

        profileInfo.put("mode", String.format(Locale.getDefault(), "%d", vpnProfile.getType() == VPNProfileDatabase.ProfileType.OPENVPN ? VPN.ConnectionMode.OPENVPN_PROFILE.getValue() : VPN.ConnectionMode.WIREGUARD_PROFILE.getValue()));
        profileInfo.put("name", vpnProfile.getName());
        profileInfo.put("vpn_type", vpnProfile.getType().toString());
        profileInfo.put("description", vpnProfile.getAirVPNServerDescription());
        profileInfo.put("server", vpnProfile.getIpAddress());
        profileInfo.put("port", String.format("%d", vpnProfile.getPort()));
        profileInfo.put("protocol", vpnProfile.getProtocol().toString());

        txtProfileFileName.setText(profileInfo.get("name"));
        txtVpnType.setText(profileInfo.get("vpn_type"));
        txtServerName.setText(profileInfo.get("server"));
        txtServerPort.setText(profileInfo.get("port"));
        txtServerProtocol.setText(profileInfo.get("protocol"));

        showVpnProfileInfo();

        progressMessage = String.format(getResources().getString(R.string.conn_try_connection), profileInfo.get("server"), profileInfo.get("vpn_type"), profileInfo.get("protocol"), profileInfo.get("port"));

        if(currentConnectionStatus == VPN.Status.CONNECTING || currentConnectionStatus == VPN.Status.CONNECTED || currentConnectionStatus == VPN.Status.PAUSED_BY_USER || currentConnectionStatus == VPN.Status.PAUSED_BY_SYSTEM || currentConnectionStatus == VPN.Status.LOCKED)
        {
            HashMap<String, String> currentProfile = vpnManager.vpn().getProfileInfo();
            String message = "", serverDescription = "";

            if(currentProfile != null)
            {
                if(currentProfile.containsKey("description") && !currentProfile.get("description").isEmpty())
                    serverDescription = String.format("AirVPN %s (%s)", currentProfile.get("description"), currentProfile.get("server"));
                else
                    serverDescription = currentProfile.get("server");
            }

            message = String.format(Locale.getDefault(), getResources().getString(R.string.airvpn_server_connection_warning), serverDescription);
            message += " ";
            message += String.format(Locale.getDefault(), getResources().getString(R.string.vpn_connect_profile), profileInfo.get("vpn_type"), profileName);

            if(supportTools.confirmationDialog(message))
            {
                try
                {
                    vpnManager.stopConnection();

                    vpnManager.vpn().setPendingProfileInfo(profileInfo);
                    vpnManager.vpn().setPendingVpnProfile(profileData);
                    vpnManager.vpn().setPendingProgressMessage(progressMessage);

                    return;
                }
                catch(Exception e)
                {
                    EddieLogger.error("ConnectAirVPNServerFragment.connectServer(): vpnManager.stop() exception: %s", e.getMessage());

                    return;
                }
            }
            else
                return;
        }
        else
        {
            if(!supportTools.confirmationDialog(String.format(Locale.getDefault(), getResources().getString(R.string.vpn_connect_profile), profileInfo.get("vpn_type"), profileName)))
                return;
        }

        supportTools.showConnectionProgressDialog(progressMessage);

        onStartConnection();
    }

    private void showVpnProfileInfo()
    {
        String profileName = "";

        if(profileInfo != null && profileInfo.size() > 0 && profileData != null && !profileData.equals(""))
        {
            if(profileInfo.containsKey("name"))
                profileName = profileInfo.get("name");

            if(!profileName.equals("quick_connect") && !profileName.equals("airvpn_server_connect"))
            {
                txtProfileFileName.setText(profileInfo.get("name"));

                if(profileInfo.containsKey("vpn_type"))
                    txtVpnType.setText(profileInfo.get("vpn_type"));
                else
                    txtVpnType.setText(getResources().getString(R.string.conn_status_unknown));

                if(profileInfo.containsKey("server"))
                    txtServerName.setText(profileInfo.get("server"));
                else
                    txtServerName.setText(getResources().getString(R.string.conn_status_unknown));

                if(profileInfo.containsKey("port"))
                    txtServerPort.setText(profileInfo.get("port"));
                else
                    txtServerPort.setText(getResources().getString(R.string.conn_status_unknown));

                if(profileInfo.containsKey("protocol"))
                    txtServerProtocol.setText(profileInfo.get("protocol"));
                else
                    txtServerProtocol.setText(getResources().getString(R.string.conn_status_unknown));

                llServerInfo.setVisibility(View.VISIBLE);

                if(vpnManager.vpn().getConnectionStatus() == VPN.Status.CONNECTED)
                    supportTools.setButtonStatus(btnConnectProfile, SupportTools.ShowMode.HIDDEN);
                else
                    supportTools.setButtonStatus(btnConnectProfile, SupportTools.ShowMode.ENABLED);
            }
            else
            {
                txtProfileFileName.setText(getResources().getString(R.string.conn_no_profile));

                llServerInfo.setVisibility(View.GONE);
            }
        }
        else
        {
            txtProfileFileName.setText(getResources().getString(R.string.conn_no_profile));

            llServerInfo.setVisibility(View.GONE);
        }
    }

    private void setBootVpnProfile(String profileName)
    {
        if(profileName == null || profileName.isEmpty())
            return;

        vpnProfileDatabase.setBoot(profileName);

        setupProfileListView();
    }

    private void unsetBootVpnProfile(String profileName)
    {
        if(profileName == null || profileName.isEmpty())
            return;

        vpnProfileDatabase.unsetBoot(profileName);

        setupProfileListView();
    }

    private void exportVpnProfile(String profileName)
    {
        int res = 0;

        if(profileName == null || profileName.isEmpty())
            return;

        VPNProfileDatabase.VPNProfile vpnProfile = vpnProfileDatabase.getProfile(profileName);

        if(vpnProfile == null)
        {
            supportTools.infoDialog(R.string.vpn_profile_not_found, true);

            return;
        }

        if(vpnProfile.getType() == VPNProfileDatabase.ProfileType.OPENVPN)
            res = R.string.context_menu_export_openvpn_profile_cap;
        else if(vpnProfile.getType() == VPNProfileDatabase.ProfileType.WIREGUARD)
            res = R.string.context_menu_export_wireguard_profile_cap;
        else
        {
            EddieLogger.error("ConnectVpnProfileFragment.exportVpnProfile(): Unknown VPN Profile type %s", vpnProfile.getType());

            return;
        }

        supportTools.sharePlainText(vpnProfile.getProfile(), vpnProfile.getName(), res);
    }

    private void onStartConnection()
    {
        String progressMessage = "";

        VPN.Status currentConnectionStatus = vpnManager.vpn().getConnectionStatus();

        if(profileData.equals(""))
        {
            supportTools.infoDialog(R.string.conn_no_profile_selected, true);

            return;
        }

        supportTools.saveLastConnectedProfile(profileInfo, profileData);

        vpnManager.vpn().setType(VPN.Type.fromString(profileInfo.get("vpn_type")));

        vpnManager.vpn().setProfileInfo(profileInfo);

        switch(vpnManager.vpn().getType())
        {
            case OPENVPN:
            {
                vpnManager.vpn().setConnectionMode(VPN.ConnectionMode.OPENVPN_PROFILE);

                vpnManager.vpn().setConnectionModeDescription(getResources().getString(R.string.conn_type_openvpn_profile));
            }
            break;

            case WIREGUARD:
            {
                vpnManager.vpn().setConnectionMode(VPN.ConnectionMode.WIREGUARD_PROFILE);

                vpnManager.vpn().setConnectionModeDescription(getResources().getString(R.string.conn_type_wireguard_profile));
            }
            break;
        }

        vpnManager.vpn().setUserProfileDescription("");

        vpnManager.vpn().setUserName("");

        try
        {
            startConnection();
        }
        catch(Exception e)
        {
            EddieLogger.error("ConnectVpnProfileFragment.onStartConnection() exception: %s", e.getMessage());
        }
    }

    public void updateVpnStatus(String vpnStatus)
    {
        if(txtVpnStatus != null)
            txtVpnStatus.setText(vpnStatus);

        currentVpnStatusDescription = vpnStatus;

        setConnectButton();
    }

    private void startConnection()
    {
        if(vpnManager == null)
        {
            EddieLogger.error("ConnectVpnProfileFragment.startConnection(): vpnManager is null");

            return;
        }

        if(profileData.equals(""))
        {
            supportTools.infoDialog(R.string.conn_no_profile_selected, true);

            return;
        }

        if(profileInfo.containsKey("name") == true)
            txtProfileFileName.setText(profileInfo.get("name"));
        else
            txtProfileFileName.setText("???");

        EddieLogger.info(String.format(Locale.getDefault(), "Trying to connect user %s profile '%s'", vpnManager.vpn().getType().toString(), (profileInfo.containsKey("name") == true) ? profileInfo.get("name") : "Unknown"));

        vpnManager.clearProfile();

        vpnManager.setProfile(profileData);

        if(vpnManager.vpn().getType() == VPN.Type.OPENVPN)
        {
            String profileString = settingsManager.getOvpn3CustomDirectives().trim();

            if(profileString.length() > 0)
                vpnManager.addProfileString(profileString);
        }

        vpnManager.startConnection();
    }

    public void setConnectButton()
    {
        SupportTools.ShowMode mode = SupportTools.ShowMode.DISABLED;

        if(btnConnectProfile == null)
            return;

        VPN.Status status = vpnManager.vpn().getConnectionStatus();

        switch(status)
        {
            case CONNECTED:
            case CONNECTING:
            case DISCONNECTING:
            case PAUSED_BY_USER:
            case PAUSED_BY_SYSTEM:
            case LOCKED:
            {
                mode = SupportTools.ShowMode.HIDDEN;
            }
            break;

            case NOT_CONNECTED:
            case CONNECTION_REVOKED_BY_SYSTEM:
            case CONNECTION_ERROR:
            case UNDEFINED:
            {
                mode = SupportTools.ShowMode.ENABLED;
            }
            break;

            default:
            {
                mode = SupportTools.ShowMode.ENABLED;
            }
            break;
        }

        if(!NetworkStatusReceiver.isNetworkConnected())
            mode = SupportTools.ShowMode.DISABLED;

        supportTools.setButtonStatus(btnConnectProfile, mode);
    }

    private void expandListViewHeight(ListView listView)
    {
        ListAdapter listAdapter = listView.getAdapter();

        if(listAdapter != null)
        {
            int items = listAdapter.getCount();

            int itemsHeight = 0;

            for(int pos = 0; pos < items; pos++)
            {
                View item = listAdapter.getView(pos, null, listView);

                item.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)); // (View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);

                itemsHeight += item.getMeasuredHeight();
            }

            int dividersHeight = listView.getDividerHeight() * (items - 1);

            ViewGroup.LayoutParams params = listView.getLayoutParams();

            params.height = itemsHeight + dividersHeight;

            listView.setLayoutParams(params);

            listView.requestLayout();
        }
    }

    // Eddie events

    public void onVpnConnectionStatsChanged(final VPNConnectionStats connectionData)
    {
    }

    public void onVpnStatusChanged(final VPN.Status vpnStatus, final String message)
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                updateVpnStatus(message);
            }
        };

        SupportTools.runOnUiActivity(getActivity(), runnable);
    }

    public void onVpnAuthFailed(final VPNEvent oe)
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                setConnectButton();

                updateVpnStatus(getResources().getString(R.string.conn_auth_failed));
            }
        };

        SupportTools.runOnUiActivity(getActivity(), runnable);
    }

    public void onVpnError(final VPNEvent oe)
    {
    }

    public void onVpnReconnect(final VPNEvent oe)
    {
    }

    public void onMasterPasswordChanged()
    {
    }

    public void onAirVPNLogin()
    {
    }

    public void onAirVPNLoginFailed(String message)
    {
    }

    public void onAirVPNLogout()
    {
    }

    public void onAirVPNUserDataChanged()
    {
    }

    public void onAirVPNUserProfileReceived(Document document)
    {
    }

    public void onAirVPNUserProfileDownloadError()
    {
    }

    public void onAirVPNUserProfileChanged()
    {
    }

    public void onAirVPNManifestReceived(Document document)
    {
    }

    public void onAirVPNManifestDownloadError()
    {
    }

    public void onAirVPNManifestChanged()
    {
    }

    public void onAirVPNIgnoredManifestDocumentRequest()
    {
    }

    public void onAirVPNIgnoredUserDocumentRequest()
    {
    }

    public void onAirVPNRequestError(String message)
    {
    }

    public void onCancelConnection()
    {
    }

    public void onSaveState()
    {
    }

    public void onRestoreState(Bundle bundle)
    {
    }

    // NetworkStatusReceiver

    public void onNetworkStatusNotAvailable()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));

        setConnectButton();
    }

    public void onNetworkStatusConnected()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(String.format(Locale.getDefault(), getResources().getString(R.string.conn_status_connected), NetworkStatusReceiver.getNetworkDescription()));

        setConnectButton();
    }

    public void onNetworkStatusIsConnecting()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));

        setConnectButton();
    }

    public void onNetworkStatusIsDisconnecting()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));

        setConnectButton();
    }

    public void onNetworkStatusSuspended()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));

        setConnectButton();
    }

    public void onNetworkStatusNotConnected()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_disconnected));

        setConnectButton();
    }

    public void onNetworkTypeChanged()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_disconnected));

        setConnectButton();
    }
}