package org.airvpn.eddie;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.provider.ProviderProperties;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;

import java.util.List;

public class MockLocation implements LocationListener
{
    private final int defaultInterval = 1000;
    private String providerName = "";
    private LocationManager locationManager = null;
    Location location = null;
    private CountryContinent.CountryCoordinates coordinates = null;
    private Runnable runnable = null;
    private Thread thread = null;
    private boolean threadIsRunning = false;
    private int interval = defaultInterval, minInterval = 100;

    public MockLocation(String provider)
    {
        init(provider, defaultInterval);
    }

    public MockLocation(String provider, int i)
    {
        init(provider, i);
    }

    @SuppressLint("MissingPermission")
    private void init(String provider, int i)
    {
        providerName = provider;

        locationManager = (LocationManager)EddieApplication.applicationContext().getSystemService(Context.LOCATION_SERVICE);

        delete();

        locationManager.addTestProvider(providerName, false, false, false, false, false, true, true, ProviderProperties.POWER_USAGE_LOW, ProviderProperties.ACCURACY_FINE);

        locationManager.setTestProviderEnabled(providerName, true);

        locationManager.requestLocationUpdates(providerName, 0, 0, this);

        location = new Location(providerName);

        runnable = null;
        thread = null;

        threadIsRunning = false;
        coordinates = new CountryContinent.CountryCoordinates();

        if(i < minInterval)
            interval = defaultInterval;
        else
            interval = i;
    }

    @Override
    protected void finalize() throws Throwable
    {
        try
        {
            delete();
        }
        finally
        {
            super.finalize();
        }
    }

    public int getInterval()
    {
        return interval;
    }

    public void setInterval(int i)
    {
        if(i <= 0)
            return;

        interval = i;
    }

    public void setLocation(CountryContinent.CountryCoordinates coords)
    {
        if(location == null)
        {
            EddieLogger.error("MockLocation: location is null.");

            return;
        }

        if(coords == null)
        {
            EddieLogger.error("MockLocation: coordinates are null.");

            return;
        }

        location.setLatitude(coords.getLatitude());
        location.setLongitude(coords.getLongitude());
        location.setAltitude(1);
        location.setTime(System.currentTimeMillis());
        location.setSpeed(0.01F);
        location.setBearing(1F);
        location.setAccuracy(1F);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            location.setBearingAccuracyDegrees(0.1F);

            location.setVerticalAccuracyMeters(0.1F);

            location.setSpeedAccuracyMetersPerSecond(0.01F);
        }

        location.setElapsedRealtimeNanos(SystemClock.elapsedRealtimeNanos());

        try
        {
            locationManager.setTestProviderLocation(providerName, location);
        }
        catch(Exception e)
        {
            EddieLogger.error("MockLocation: provider %s has been revoked.", providerName);

            return;
        }

        if(coordinates.getLatitude() != coords.getLatitude() || coordinates.getLongitude() != coords.getLongitude())
            EddieLogger.info("Set mock position to latitude %f, longitude %f (%s) for %s provider", coords.getLatitude(), coords.getLongitude(), CountryContinent.getCountryName(coords.getCountryCode()), providerName);

        coordinates = coords;

        if(thread == null)
        {
            runnable = new Runnable()
            {
                @Override
                public void run()
                {
                    int i = interval;

                    EddieLogger.info("Mock location thread for %s provider started successfully. Mock interval is %d milliseconds.", providerName, i);

                    threadIsRunning = true;

                    while(threadIsRunning == true)
                    {
                        setLocation(coordinates);

                        if(i != interval)
                        {
                            EddieLogger.info("Mock interval for %s provider is now %d milliseconds.", providerName, i);

                            i = interval;
                        }

                        SystemClock.sleep(i);
                    }

                    EddieLogger.info("Mock location thread for %s provider successfully terminated", providerName);
                }
            };

            thread = SupportTools.startThread(runnable);
        }
    }

    public void delete()
    {
        threadIsRunning = false;

        try
        {
            locationManager.removeTestProvider(providerName);
        }
        catch(Exception e)
        {
        }

        thread = null;
        runnable = null;
    }

    @Override
    public void onFlushComplete(int requestCode)
    {
    }

    @Override
    public void onLocationChanged(List<Location> locations)
    {
    }

    @Override
    public void onProviderDisabled(String provider)
    {
    }

    @Override
    public void onProviderEnabled(String provider)
    {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras)
    {
    }

    @Override
    public void onLocationChanged(Location location)
    {
        if(location != null)
        {
            if((location.getLatitude() != coordinates.getLatitude() || location.getLongitude() != coordinates.getLongitude()) && threadIsRunning == true)
            {
                EddieLogger.warning("GPS location has changed for provider %s. Resetting to mock position.", location.getProvider());

                setLocation(coordinates);
            }
        }
    }
}
