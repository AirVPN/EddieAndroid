// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 20 June 2018 - author: ProMIND - initial release. Based on revised code from com.eddie.android. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.system.OsConstants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class OpenVPNTunnel extends VPNTunnel
{
    private OpenVPNClient openVPNClient = null;
    private VPNEvent vpnEvent = null;

    private int maxUdpPartialSendErrors = 5;
    private int udpPartialSendErrorCount = 0;

    public OpenVPNTunnel(VPNService service)
    {
        super(service, Type.OPENVPN);

        udpPartialSendErrorCount = 0;
    }

    @Override
    protected void finalize() throws Throwable
    {
        try
        {
            if(openVPNClient != null)
                openVPNClient.destroy(this);
        }
        catch(Exception e)
        {
            EddieLogger.error("OpenVPNTunnel.finalize(): Exception: ", e);
        }
        finally
        {
            try
            {
                super.finalize();
            }
            catch(Exception e)
            {
                EddieLogger.error("OpenVPNTunnel.finalize(): Exception: ", e);
            }
        }
    }

    private boolean onTunBuilderNew()
    {
        boolean result = false;
        VPNContext newVpnContext = null;

        EddieLogger.debug("OpenVPNTunnel.onTunBuilderNew()");

        try
        {
            newVpnContext = new VPNContext(vpnService);

            if(newVpnContext != null)
            {
                vpnContext.push(newVpnContext);

                result = true;
            }
            else
            {
                EddieLogger.error("OpenVPNTunnel.onTunBuilderNew(): cannot create a new VPNContext.");

                result = false;
            }
        }
        catch(Exception e)
        {
            EddieLogger.error("OpenVPNTunnel.onTunBuilderNew()", e);

            result = false;
        }

        return result;
    }

    private boolean onTunBuilderSetLayer(int layer)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderSetLayer(layer: %d)", layer);

        return true;
    }

    private boolean onTunBuilderSetRemoteAddress(String address, boolean ipv6)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderSetRemoteAddress(address: %s, ipv6: %s)", address, Boolean.toString(ipv6));

        return true;
    }

    private boolean onTunBuilderAddAddress(String address, int prefix_length, String gateway, boolean ipv6, boolean net30)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderAddAddress(address: %s, prefix_length: %s, gateway: %s, ipv6: %s, net30: %s)", address, prefix_length, gateway, Boolean.toString(ipv6), Boolean.toString(net30));

        try
        {
            getActiveContext().getBuilder().addAddress(address, prefix_length);
        }
        catch(Exception e)
        {
            EddieLogger.error("OpenVPNTunnel.onTunBuilderAddAddress(): cannot add address %s - prefix_length %d", address, prefix_length);

            return false;
        }

        return true;
    }

    private boolean onTunBuilderSetRouteMetricDefault(int metric)
    {
        EddieLogger.debug("OpenVPNTunnel.OnTunBuilderSetRouteMetricDefault(metric: %d)", metric);

        return true;
    }

    private boolean onTunBuilderRerouteGW(boolean ipv4, boolean ipv6, int flags)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuildeRerouteGW(ipv4: %s, ipv6: %s, flags: %d)", Boolean.toString(ipv4), Boolean.toString(ipv6), flags);

        return true;
    }

    private boolean onTunBuilderAddRoute(String address, int prefix_length, int metric, boolean ipv6)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderAddRoute(address: %s, prefix_length: %d, metric: %d, ipv6: %s)", address, prefix_length, metric, Boolean.toString(ipv6));

        try
        {
            getActiveContext().getBuilder().addRoute(address, prefix_length);
        }
        catch(Exception e)
        {
            EddieLogger.error("OpenVPNTunnel.onTunBuilderAddRoute(): cannot add route %s - prefix_length %d - metric %d - ipv6 %s", address, prefix_length, metric, Boolean.toString(ipv6));

            return false;
        }

        return true;
    }

    private boolean onTunBuilderExcludeRoute(String address, int prefix_length, int metric, boolean ipv6)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderExcludeRoute(address: %s, prefix_length: %d, metric: %d, ipv6: %s)", address, prefix_length, metric, Boolean.toString(ipv6));

        return true;
    }

    private boolean onTunBuilderAddDNSServer(String address, boolean ipv6)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderAddDNSServer(address: %s, ipv6: %s)", address, Boolean.toString(ipv6));

        try
        {
            getActiveContext().addDNSServer(address, ipv6);

            vpnManager.vpn().addDnsEntry(address, ipv6 ? IPAddress.IPFamily.IPv6 : IPAddress.IPFamily.IPv4);

            return true;
        }
        catch(Exception e)
        {
            EddieLogger.error("OpenVPNTunnel.onTunBuilderAddDNSServer()", e);
        }

        return false;
    }

    private boolean onTunBuilderAddSearchDomain(String domain)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderAddSearchDomain(domain: %s)", domain);

        try
        {
            getActiveContext().getBuilder().addSearchDomain(domain);
        }
        catch(Exception e)
        {
            EddieLogger.error("OpenVPNTunnel.onTunBuilderAddSearchDomain()", e);

            return false;
        }

        return true;
    }

    private boolean onTunBuilderSetMTU(int mtu)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderSetMTU(mtu=%d)", mtu);

        try
        {
            getActiveContext().setMTU(mtu);

            return true;
        }
        catch(Exception e)
        {
            EddieLogger.error("OpenVPNTunnel.onTunBuilderSetMTU()", e);
        }

        return false;
    }

    private boolean onTunBuilderSetSessionName(String name)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderSetSessionName(name: %s)", name);

        try
        {
            getActiveContext().getBuilder().setSession(name);
        }
        catch(Exception e)
        {
            EddieLogger.error("OpenVPNTunnel.onTunBuilderSetSessionName()", e);

            return false;
        }

        return true;
    }

    private boolean onTunBuilderAddProxyBypass(String bypass_host)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderAddProxyBypass(bypass_host: %s)", bypass_host);

        return true;
    }

    private boolean onTunBuilderSetProxyAutoConfigUrl(String url)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderSetProxyAutoConfigUrl(url: %s)", url);

        return true;
    }

    private boolean onTunBuilderSetProxyHttp(String host, int port)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderSetProxyHttp(host: %s, port: %d)", host, port);

        return true;
    }

    private boolean onTunBuilderSetProxyHttps(String host, int port)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderSetProxyHttps(host: %s, port: %d)", host, port);

        return true;
    }

    private boolean onTunBuilderAddWinsServer(String address)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderAddWinsServer(address: %s)", address);

        return true;
    }

    private boolean onTunBuilderSetAllowFamily(int af, boolean allow)
    {
        String afInet = "";

        if(af == OsConstants.AF_INET)
            afInet = "AF_INET4";
        else if(af == OsConstants.AF_INET6)
            afInet = "AF_INET6";
        else
            afInet = "??";

        EddieLogger.debug("OpenVPNTunnel.onTunBuilderSetAllowFamily(af: %s, allow: %s)", afInet, Boolean.toString(allow));

        try
        {
            getActiveContext().allowFamily(af, allow);

            return true;
        }
        catch(Exception e)
        {
            EddieLogger.error("OpenVPNTunnel.onTunBuilderSetAllowFamily()", e);
        }

        return false;
    }

    private boolean onTunBuilderSetAdapterDomainSuffix(String name)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderSetAdapterDomainSuffix(name: %s)", name);

        return true;
    }

    private int onTunBuilderEstablish()
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderEstablish()");

        ParcelFileDescriptor fileDescriptor = null;

        try
        {
            fileDescriptor = getActiveContext().establish();
        }
        catch(Exception e)
        {
            EddieLogger.error("OpenVPNTunnel.onTunBuilderEstablish(): %s", e);

            return 0;
        }

        if(fileDescriptor == null)
        {
            EddieLogger.error("OpenVPNTunnel.onTunBuilderEstablish(): VPNService.Builder.establish() failed");

            return 0;
        }

        vpnService.handleThreadStarted();

        return fileDescriptor.detachFd();
    }

    private boolean onTunBuilderPersist()
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderPersist()");

        return true;
    }

    private void onTunBuilderEstablishLite()
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderEstablishLite()");
    }

    private void onTunBuilderTeardown(boolean disconnect)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderTeardown(disconnect: %s)", Boolean.toString(disconnect));
    }

    private boolean onSocketProtect(int socket)
    {
        boolean result = false;

        EddieLogger.debug("VPNTunnel.onSocketProtect(socket: %d)", socket);

        try
        {
            result = vpnService.protect(socket);
        }
        catch(Exception e)
        {
            EddieLogger.error("VPNTunnel.onSocketProtect()", e);
        }

        return result;
    }

    private void onConnectAttach()
    {
        EddieLogger.debug("OpenVPNTunnel.onConnectAttach()");
    }

    private void onConnectPreRun()
    {
        EddieLogger.debug("OpenVPNTunnel.onConnectPreRun()");
    }

    private void onConnectRun()
    {
        EddieLogger.debug("OpenVPNTunnel.onConnectRun()");
    }

    private void onConnectSessionStop()
    {
        EddieLogger.debug("OpenVPNTunnel.onConnectSessionStop()");
    }

    private void onEvent(Object event)
    {
        VPNEvent oe = (VPNEvent)event;
        VPNConnectionStats connectionStats = (VPNConnectionStats)oe.data;

        try
        {
            switch(oe.type)
            {
                case VPNEvent.MESSAGE:
                case VPNEvent.INFO:
                {
                    EddieLogger.info("%s (%d): %s", oe.name, oe.type, oe.info);
                }
                break;

                case VPNEvent.WARN:
                {
                    EddieLogger.warning("OpenVPN %s (%d): %s", oe.name, oe.type, oe.info);
                }
                break;

                case VPNEvent.ERROR:
                case VPNEvent.FATAL_ERROR:
                {
                    if(quickConnectFailureError(oe.name))
                    {
                        vpnService.changeStatus(VPN.Status.CONNECTION_ERROR);

                        eddieEvent.onVpnError(oe);

                        return;
                    }
                    else if(ignoredError(oe.name))
                    {
                        EddieLogger.warning("OpenVPN %s (%d): %s", oe.name, oe.type, oe.info);
                    }
                    else
                    {
                        // It seems OpenVPN is having BIG troubles with the connection
                        // In order to prevent worse conditions, try to lock the VPN (in case it is possible)

                        EddieLogger.error("OpenVPN Fatal Error %s (%d): %s", oe.name, oe.type, oe.info);

                        if(vpnManager.vpn().getConnectionStatus() == VPN.Status.NOT_CONNECTED || vpnManager.vpn().getConnectionStatus() == VPN.Status.CONNECTING || vpnManager.vpn().getConnectionStatus() == VPN.Status.UNDEFINED)
                        {
                            vpnService.changeStatus(VPN.Status.CONNECTION_ERROR);

                            alertNotification(vpnService.getResources().getString(R.string.connection_error));

                            eddieEvent.onVpnError(oe);
                        }
                        else if(Build.VERSION.SDK_INT < Build.VERSION_CODES.N)
                        {
                            if(isVPNLockEnabled)
                            {
                                networkStatusChanged(Action.LOCK);

                                vpnService.changeStatus(VPN.Status.LOCKED);

                                alertNotification(vpnService.getResources().getString(R.string.connection_vpn_error));

                                eddieEvent.onVpnError(oe);
                            }
                            else
                                EddieLogger.warning(vpnLockDisabledLogWarning);
                        }

                        return;
                    }
                }
                break;

                case VPNEvent.FORMAL_WARNING:
                {
                    if(quickConnectFailureError(oe.name))
                    {
                        eddieEvent.onVpnError(oe);

                        return;
                    }
                    else if(ignoredError(oe.name))
                    {
                        EddieLogger.warning("OpenVPN %s (%d): %s", oe.name, oe.type, oe.info);
                    }
                    else
                    {
                        // It seems OpenVPN is having troubles with the connection
                        // In order to prevent worse conditions, lock the VPN

                        EddieLogger.error("OpenVPN %s (%d): %s", oe.name, oe.type, oe.info);

                        if(vpnManager.vpn().getConnectionStatus() == VPN.Status.NOT_CONNECTED || vpnManager.vpn().getConnectionStatus() == VPN.Status.CONNECTING || vpnManager.vpn().getConnectionStatus() == VPN.Status.UNDEFINED)
                        {
                            vpnEvent = new VPNEvent();

                            vpnEvent.type = VPNEvent.FATAL_ERROR;
                            vpnEvent.notifyUser = true;
                            vpnEvent.name = oe.name;
                            vpnEvent.info = oe.info;

                            eddieEvent.onVpnError(vpnEvent);

                            alertNotification(vpnService.getResources().getString(R.string.connection_error));
                        }
                        else if(isVPNLockEnabled)
                        {
                            networkStatusChanged(Action.LOCK);

                            vpnService.changeStatus(VPN.Status.LOCKED);

                            alertNotification(vpnService.getResources().getString(R.string.connection_vpn_formal_warning));
                        }
                        else
                            EddieLogger.warning(vpnLockDisabledLogWarning);

                        return;
                    }
                }
                break;

                case VPNEvent.UDP_PARTIAL_SEND_ERROR:
                {
                    udpPartialSendErrorCount++;

                    if(udpPartialSendErrorCount >= maxUdpPartialSendErrors)
                    {
                        // It seems OpenVPN is having troubles with the UDP connection
                        // In order to prevent worse conditions, lock the VPN

                        EddieLogger.error("OpenVPN %s (%d): %s - Reached maximum limit for this error (%d)", oe.name, oe.type, oe.info, maxUdpPartialSendErrors);

                        if(vpnManager.vpn().getConnectionStatus() == VPN.Status.NOT_CONNECTED || vpnManager.vpn().getConnectionStatus() == VPN.Status.CONNECTING || vpnManager.vpn().getConnectionStatus() == VPN.Status.UNDEFINED)
                        {
                            vpnEvent = new VPNEvent();

                            vpnEvent.type = VPNEvent.FATAL_ERROR;
                            vpnEvent.notifyUser = true;
                            vpnEvent.name = oe.name;
                            vpnEvent.info = oe.info;

                            eddieEvent.onVpnError(vpnEvent);

                            alertNotification(vpnService.getResources().getString(R.string.connection_error));
                        }
                        else if(isVPNLockEnabled)
                        {
                            networkStatusChanged(Action.LOCK);

                            vpnService.changeStatus(VPN.Status.LOCKED);

                            alertNotification(vpnService.getResources().getString(R.string.connection_vpn_formal_warning));
                        }
                        else
                            EddieLogger.warning(vpnLockDisabledLogWarning);
                    }
                    else
                        EddieLogger.warning("OpenVPN %s (%d): %s", oe.name, oe.type, oe.info);
                }
                break;

                case VPNEvent.AUTH_FAILED:
                {
                    EddieLogger.error("OpenVPN Authentication Failed: %s (%d): %s", oe.name, oe.type, oe.info);

                    eddieEvent.onVpnAuthFailed(oe);
                }
                break;

                case VPNEvent.TUN_ERROR:
                case VPNEvent.CLIENT_RESTART:
                case VPNEvent.CERT_VERIFY_FAIL:
                case VPNEvent.TLS_VERSION_MIN:
                case VPNEvent.CLIENT_HALT:
                case VPNEvent.CLIENT_SETUP:
                case VPNEvent.CONNECTION_TIMEOUT:
                case VPNEvent.INACTIVE_TIMEOUT:
                case VPNEvent.DYNAMIC_CHALLENGE:
                case VPNEvent.PROXY_NEED_CREDS:
                case VPNEvent.PROXY_ERROR:
                case VPNEvent.TUN_SETUP_FAILED:
                case VPNEvent.TUN_IFACE_CREATE:
                case VPNEvent.TUN_IFACE_DISABLED:
                case VPNEvent.EPKI_ERROR:
                case VPNEvent.EPKI_INVALID_ALIAS:
                {
                    if(oe.type == VPNEvent.CONNECTION_TIMEOUT && vpnManager.vpn().getConnectionMode() == VPN.ConnectionMode.QUICK_CONNECT)
                    {
                        eddieEvent.onVpnError(oe);

                        return;
                    }

                    // These OpenVPN events may cause a fatal error
                    // In order to prevent worse conditions, lock the VPN

                    EddieLogger.error("OpenVPN %s (%d): %s", oe.name, oe.type, oe.info);

                    if(vpnManager.vpn().getConnectionStatus() == VPN.Status.NOT_CONNECTED || vpnManager.vpn().getConnectionStatus() == VPN.Status.CONNECTING || vpnManager.vpn().getConnectionStatus() == VPN.Status.UNDEFINED)
                    {
                        vpnEvent = new VPNEvent();

                        vpnEvent.type = VPNEvent.FATAL_ERROR;
                        vpnEvent.notifyUser = true;
                        vpnEvent.name = oe.name;
                        vpnEvent.info = oe.info;

                        eddieEvent.onVpnError(vpnEvent);

                        alertNotification(vpnService.getResources().getString(R.string.connection_error));
                    }
                    else if(isVPNLockEnabled)
                    {
                        networkStatusChanged(Action.LOCK);

                        vpnService.changeStatus(VPN.Status.LOCKED);

                        alertNotification(vpnService.getResources().getString(R.string.connection_vpn_formal_warning));

                        eddieEvent.onVpnError(oe);
                    }
                    else
                        EddieLogger.warning(vpnLockDisabledLogWarning);
                }
                break;

                case VPNEvent.CONNECTED:
                {
                    String serverInfo = "", dnsInfo = "";
                    HashMap<String, String> currentProfile = vpnManager.vpn().getProfileInfo();

                    vpnService.changeStatus(VPN.Status.CONNECTED);

                    updateNotification(VPN.Status.CONNECTED);

                    switch(vpnManager.vpn().getConnectionMode())
                    {
                        case AIRVPN_SERVER:
                        case QUICK_CONNECT:
                        {
                            if(currentProfile != null && currentProfile.containsKey("description"))
                                serverInfo = String.format(" (AirVPN server %s) %s", currentProfile.get("description"), currentProfile.get("server"));
                            else
                                serverInfo = String.format(" (AirVPN server) %s", currentProfile.get("server"));
                        }
                        break;

                        case OPENVPN_PROFILE:
                        {
                            serverInfo = String.format(" server %s (OpenVPN profile) %s", currentProfile.get("description"), currentProfile.get("server"));
                        }
                        break;

                        case WIREGUARD_PROFILE:
                        {
                            serverInfo = String.format(" server %s (WireGuard profile) %s", currentProfile.get("description"), currentProfile.get("server"));
                        }
                        break;

                        case BOOT_CONNECT:
                        {
                            serverInfo = String.format(" server %s (VPN startup connection) %s", currentProfile.get("description"), currentProfile.get("server"));
                        }
                        break;

                        default:
                        {
                            serverInfo = "";
                        }
                        break;
                    }

                    serverInfo += " (" + currentProfile.get("vpn_type") + ")";

                    if(connectionStats.vpnIp4.isEmpty())
                        connectionStats.vpnIp4 = "--";

                    if(connectionStats.vpnIp6.isEmpty())
                        connectionStats.vpnIp6 = "--";

                    if(connectionStats.gw4.isEmpty())
                        connectionStats.gw4 = "--";

                    if(connectionStats.gw6.isEmpty())
                        connectionStats.gw6 = "--";

                    EddieLogger.info("CONNECTED to VPN%s" + System.getProperty("line.separator") + "Defined: %d" + System.getProperty("line.separator") + "User: %s" + System.getProperty("line.separator") + "Server Host: %s" + System.getProperty("line.separator") + "Server Port: %s" + System.getProperty("line.separator") + "Server Protocol: %s" + System.getProperty("line.separator") + "Server IP: %s" + System.getProperty("line.separator") + "VPN IPv4: %s" + System.getProperty("line.separator") + "VPN IPv6: %s" + System.getProperty("line.separator") + "Gateway IPv4: %s" + System.getProperty("line.separator") + "Gateway IPv6: %s" + System.getProperty("line.separator") + "Tunnel Name: %s" + System.getProperty("line.separator") + "Topology: %s" + System.getProperty("line.separator") + "Cipher: %s" + System.getProperty("line.separator") + "Ping: %d seconds" + System.getProperty("line.separator") + "Ping restart: %d seconds",
                            serverInfo,
                            connectionStats.defined,
                            connectionStats.user,
                            connectionStats.serverHost,
                            connectionStats.serverPort,
                            connectionStats.serverProto,
                            connectionStats.serverIp,
                            connectionStats.vpnIp4,
                            connectionStats.vpnIp6,
                            connectionStats.gw4,
                            connectionStats.gw6,
                            connectionStats.tunName,
                            connectionStats.topology,
                            connectionStats.cipher,
                            connectionStats.ping,
                            connectionStats.pingRestart);

                    ArrayList<IPAddress> dnsEntry = vpnManager.vpn().getDns();

                    if(dnsEntry.size() > 0)
                    {
                        for(IPAddress dns : dnsEntry)
                        {
                            dnsInfo = "DNS IPv";

                            if(dns.getIpFamily() == IPAddress.IPFamily.IPv4)
                                dnsInfo += "4";
                            else
                                dnsInfo += "6";

                            dnsInfo += ": ";

                            dnsInfo += dns.getIpAddress();

                            EddieLogger.info(dnsInfo);
                        }
                    }

                    vpnManager.vpn().setVpnConnectionStats(connectionStats);

                    eddieEvent.onVpnConnectionStatsChanged(connectionStats);

                    udpPartialSendErrorCount = 0;
                }
                break;

                case VPNEvent.DISCONNECTED:
                {
                    EddieLogger.warning("OpenVPN %s (%d): %s", oe.name, oe.type, oe.info);

                    EddieLogger.info(vpnManager.vpn().getFormattedStats());

                    vpnService.changeStatus(VPN.Status.NOT_CONNECTED);
                }
                break;

                case VPNEvent.TRANSPORT_ERROR:
                case VPNEvent.RELAY_ERROR:
                {
                    EddieLogger.error("OpenVPN %s (%d): %s", oe.name, oe.type, oe.info);

                    eddieEvent.onVpnError(oe);
                }
                break;

                default:
                {
                    EddieLogger.debug("OpenVPN Event - type: %d, name: %s, info: %s", oe.type, oe.name, oe.info);
                }
                break;
            }
        }
        catch(Exception e)
        {
            EddieLogger.error("OpenVPNTunnel.onEvent() Exception: %s", e);
        }
    }

    private String getEventContent(VPNEvent oe)
    {
        String name = oe.name;
        String info = oe.info;

        if(info.isEmpty())
            return name;

        return name + ": " + info;
    }

    public VPN.Status handleScreenChanged(boolean active)
    {
        if(vpnManager.vpn().getConnectionStatus() == VPN.Status.LOCKED)
            return vpnManager.vpn().getConnectionStatus();

        if(openVPNClient == null)
            return vpnManager.vpn().getConnectionStatus();

        if(active)
        {
            if(vpnManager.vpn().getConnectionStatus() == VPN.Status.PAUSED_BY_SYSTEM)
            {
                EddieLogger.info("Screen is now on. Trying to resume VPN");

                vpnManager.vpn().clearDnsEntries();

                EddieLibraryResult result = openVPNClient.resume(this);

                if(result.code == EddieLibraryResult.SUCCESS)
                {
                    EddieLogger.info("Successfully resumed VPN");

                    vpnManager.vpn().setConnectionStatus(VPN.Status.CONNECTED);
                }
                else
                {
                    EddieLogger.error(String.format(Locale.getDefault(), "Failed to resume VPN. %s", result.description));

                    vpnEvent = new VPNEvent();

                    vpnEvent.type = VPNEvent.FATAL_ERROR;
                    vpnEvent.notifyUser = true;
                    vpnEvent.name = "Failed to resume VPN";
                    vpnEvent.info = result.description;

                    eddieEvent.onVpnError(vpnEvent);
                }
            }
        }
        else
        {
            if(settingsManager.isSystemPauseVpnWhenScreenIsOff() && vpnManager.vpn().getConnectionStatus() == VPN.Status.CONNECTED)
            {
                EddieLogger.info("Screen is now off. Trying to pause VPN");

                EddieLibraryResult result = openVPNClient.pause(this, "Screen is off");

                if(result.code == EddieLibraryResult.SUCCESS)
                {
                    EddieLogger.info("Successfully paused VPN");

                    vpnManager.vpn().setConnectionStatus(VPN.Status.PAUSED_BY_SYSTEM);
                }
                else
                {
                    EddieLogger.error(String.format(Locale.getDefault(), "Failed to pause VPN. %s", result.description));

                    vpnEvent = new VPNEvent();

                    vpnEvent.type = VPNEvent.FATAL_ERROR;
                    vpnEvent.notifyUser = true;
                    vpnEvent.name = "Failed to pause VPN";
                    vpnEvent.info = result.description;

                    eddieEvent.onVpnError(vpnEvent);
                }
            }
        }

        updateNotification(vpnManager.vpn().getConnectionStatus());

        return vpnManager.vpn().getConnectionStatus();
    }

    public void networkStatusChanged(Action action)
    {
        if(vpnManager.vpn().getConnectionStatus() == VPN.Status.LOCKED || openVPNClient == null)
            return;

        switch(action)
        {
            case USER_RESUME:
            {
                if(vpnManager.vpn().getConnectionStatus() == VPN.Status.PAUSED_BY_USER || vpnManager.vpn().getConnectionStatus() == VPN.Status.PAUSED_BY_SYSTEM)
                {
                    EddieLogger.info("%s. Trying to resume VPN", "User requested to resume VPN");

                    vpnManager.vpn().setConnectionStatus(VPN.Status.CONNECTED);

                    vpnManager.vpn().clearDnsEntries();

                    EddieLibraryResult result = openVPNClient.resume(this);

                    if(result.code == EddieLibraryResult.SUCCESS)
                        EddieLogger.info("Successfully resumed VPN");
                    else
                    {
                        EddieLogger.error(String.format(Locale.getDefault(), "Failed to resume VPN. %s", result.description));

                        vpnEvent = new VPNEvent();

                        vpnEvent.type = VPNEvent.FATAL_ERROR;
                        vpnEvent.notifyUser = true;
                        vpnEvent.name = "Failed to resume VPN";
                        vpnEvent.info = result.description;

                        eddieEvent.onVpnError(vpnEvent);
                    }
                }
            }
            break;

            case SYSTEM_RESUME:
            {
                if(vpnManager.vpn().getConnectionStatus() == VPN.Status.PAUSED_BY_SYSTEM)
                {
                    EddieLogger.info("%s. Trying to resume VPN", "Network status has changed");

                    vpnManager.vpn().setConnectionStatus(VPN.Status.CONNECTED);

                    vpnManager.vpn().clearDnsEntries();

                    EddieLibraryResult result = openVPNClient.resume(this);

                    if(result.code == EddieLibraryResult.SUCCESS)
                        EddieLogger.info("Successfully resumed VPN");
                    else
                    {
                        EddieLogger.error(String.format(Locale.getDefault(), "Failed to resume VPN. %s", result.description));

                        vpnEvent = new VPNEvent();

                        vpnEvent.type = VPNEvent.FATAL_ERROR;
                        vpnEvent.notifyUser = true;
                        vpnEvent.name = "Failed to resume VPN";
                        vpnEvent.info = result.description;

                        eddieEvent.onVpnError(vpnEvent);
                    }
                }
            }
            break;

            case USER_PAUSE:
            case SYSTEM_PAUSE:
            case NETWORK_TYPE_CHANGED:
            {
                String pauseReason = "";

                if(vpnManager.isVpnConnectionPaused() == false)
                {
                    if(action == Action.USER_PAUSE)
                        pauseReason = "User requested to pause VPN";
                    else
                        pauseReason = "Network status has changed";

                    EddieLogger.info("%s. Trying to pause VPN", pauseReason);

                    EddieLibraryResult result = openVPNClient.pause(this, pauseReason);

                    if(result.code == EddieLibraryResult.SUCCESS)
                    {
                        EddieLogger.info("Successfully paused VPN");

                        if(action == Action.USER_PAUSE)
                            vpnManager.vpn().setConnectionStatus(VPN.Status.PAUSED_BY_USER);
                        else
                            vpnManager.vpn().setConnectionStatus(VPN.Status.PAUSED_BY_SYSTEM);
                    }
                    else
                    {
                        EddieLogger.error(String.format(Locale.getDefault(), "Failed to pause VPN. %s", result.description));

                        vpnEvent = new VPNEvent();

                        vpnEvent.type = VPNEvent.FATAL_ERROR;
                        vpnEvent.notifyUser = true;
                        vpnEvent.name = "Failed to pause VPN";
                        vpnEvent.info = result.description;

                        eddieEvent.onVpnError(vpnEvent);
                    }
                }
            }
            break;

            case LOCK:
            {
                EddieLogger.info("VPN error detected. Locking VPN");

                EddieLibraryResult result = openVPNClient.pause(this, "Lock VPN");

                if(result.code == EddieLibraryResult.SUCCESS)
                {
                    EddieLogger.info("Successfully locked VPN");

                    vpnManager.vpn().setConnectionStatus(VPN.Status.LOCKED);
                }
                else
                {
                    EddieLogger.error(String.format(Locale.getDefault(), "Failed to lock VPN. %s", result.description));

                    vpnEvent = new VPNEvent();

                    vpnEvent.type = VPNEvent.FATAL_ERROR;
                    vpnEvent.notifyUser = true;
                    vpnEvent.name = "Failed to lock VPN";
                    vpnEvent.info = result.description;

                    eddieEvent.onVpnError(vpnEvent);
                }
            }
            break;
        }

        eddieEvent.onVpnStatusChanged(vpnManager.vpn().getConnectionStatus(), vpnService.getResources().getString(vpnManager.vpn().connectionStatusResourceDescription(vpnManager.vpn().getConnectionStatus())));

        updateNotification(vpnManager.vpn().getConnectionStatus());
    }

    public synchronized void init() throws Exception
    {
        if(openVPNClient != null)
            throw new Exception("OpenVPNTunnel.init(): Client already initialized");

        openVPNClient = OpenVPNClient.create(this);

        if(openVPNClient == null)
            throw new Exception("OpenVPNTunnel.init(): Failed to create a new OpenVPN client");
    }

    public synchronized VPNTransportStats getTransportStats() throws Exception
    {
        if(openVPNClient == null)
            return null;

        VPNTransportStats vpnTransportStats = openVPNClient.getTransportStats();

        if(vpnTransportStats != null && vpnTransportStats.resultCode != EddieLibrary.SUCCESS)
        {
            String errMsg = String.format(Locale.getDefault(), "OpenVPNTunnel.getTransportStats(): Failed to get OpenVPN transport stats. %s", vpnTransportStats.resultDescription);

            EddieLogger.error(errMsg);

            throw new Exception(errMsg);
        }

        return vpnTransportStats;
    }

    protected synchronized boolean loadProfile(String profile, boolean isString) throws Exception
    {
        EddieLibraryResult result;
        String errMsg;

        if(openVPNClient == null)
            throw new Exception("OpenVPNTunnel.loadProfile(): Client is not initialized");

        if(isString)
        {
            result = openVPNClient.loadProfileString(this, profile);

            if(result.code != EddieLibraryResult.SUCCESS)
            {
                errMsg = String.format(Locale.getDefault(), "OpenVPNTunnel.loadProfile(): Failed to load profile string. %s", result.description);

                EddieLogger.error(errMsg);

                throw new Exception(errMsg);
            }
        }
        else
        {
            result = openVPNClient.loadProfileFile(this, profile);

            if(result.code != EddieLibrary.SUCCESS)
            {
                errMsg = String.format(Locale.getDefault(), "OpenVPNTunnel.loadProfile(): Failed to load profile file. %s", result.description);

                EddieLogger.error(errMsg);

                throw new Exception(errMsg);
            }
        }

        return (result.code == EddieLibrary.SUCCESS);
    }

    private static String toOptionValue(boolean value)
    {
        return value ? "true" : "false";
    }

    private static String toOptionValue(int value)
    {
        return String.format(Locale.getDefault(), "%d", value);
    }

    private void doSetOption(String option, String value) throws Exception
    {
        EddieLibraryResult result = openVPNClient.setOption(this, option, value);

        if(result.code != EddieLibrary.SUCCESS)
        {
            String errMsg = String.format(Locale.getDefault(), "OpenVPNTunnel.doSetOption(): Failed to set option '%s' with value '%s'. %s", option, value, result.description);

            EddieLogger.error(errMsg);

            throw new Exception(errMsg);
        }
    }

    public synchronized void bindOptions() throws Exception
    {
        String value = "";

        if(openVPNClient == null)
            throw new Exception("OpenVPNTunnel.bindOptions(): Client is not initialized");

        if(settingsManager.getAirVPNOpenVPNMode().equals(SettingsManager.AIRVPN_OPENVPN_MODE_DEFAULT) == true)
            value = settingsManager.getOvpn3TLSMinVersion();
        else
        {
            value = EddieApplication.airVPNManifest().getMode(settingsManager.getAirVPNOpenVPNMode()).getTlsVersion();

            if(value.isEmpty() == true)
                value = settingsManager.getOvpn3TLSMinVersion();
        }

        doSetOption(SettingsManager.OVPN3_OPTION_TLS_MIN_VERSION_NATIVE, value);

        doSetOption(SettingsManager.OVPN3_OPTION_PROTOCOL_NATIVE, "");
        doSetOption(SettingsManager.OVPN3_OPTION_ALLOWUAF_NATIVE, settingsManager.getOvpn3AllowUnusedAddressFamilies());
        doSetOption(SettingsManager.OVPN3_OPTION_TIMEOUT_NATIVE, settingsManager.getOvpn3Timeout());
        doSetOption(SettingsManager.OVPN3_OPTION_TUN_PERSIST_NATIVE, toOptionValue(settingsManager.isOvpn3TunPersist()));
        doSetOption(SettingsManager.OVPN3_OPTION_COMPRESSION_MODE_NATIVE, settingsManager.getOvpn3CompressionMode());
        doSetOption(SettingsManager.OVPN3_OPTION_USERNAME_NATIVE, settingsManager.getOvpn3Username());
        doSetOption(SettingsManager.OVPN3_OPTION_PASSWORD_NATIVE, settingsManager.getOvpn3Password());
        doSetOption(SettingsManager.OVPN3_OPTION_SYNCHRONOUS_DNS_LOOKUP_NATIVE, toOptionValue(settingsManager.isOvpn3SynchronousDNSLookup()));
        doSetOption(SettingsManager.OVPN3_OPTION_AUTOLOGIN_SESSIONS_NATIVE, toOptionValue(settingsManager.isOvpn3AutologinSessions()));
        doSetOption(SettingsManager.OVPN3_OPTION_DISABLE_CLIENT_CERT_NATIVE, toOptionValue(settingsManager.isOvpn3DisableClientCert()));
        doSetOption(SettingsManager.OVPN3_OPTION_SSL_DEBUG_LEVEL_NATIVE, settingsManager.getOvpn3SSLDebugLevel());
        doSetOption(SettingsManager.OVPN3_OPTION_PRIVATE_KEY_PASSWORD_NATIVE, settingsManager.getOvpn3PrivateKeyPassword());
        doSetOption(SettingsManager.OVPN3_OPTION_DEFAULT_KEY_DIRECTION_NATIVE, settingsManager.getOvpn3DefaultKeyDirection());
        doSetOption(SettingsManager.OVPN3_OPTION_TLS_CERT_PROFILE_NATIVE, settingsManager.getOvpn3TLSCertProfile());

        if(settingsManager.isSystemProxyEnabled())
        {
            doSetOption(SettingsManager.OVPN3_OPTION_PROXY_HOST_NATIVE, settingsManager.getOvpn3ProxyHost());
            doSetOption(SettingsManager.OVPN3_OPTION_PROXY_PORT_NATIVE, settingsManager.getOvpn3ProxyPort());
            doSetOption(SettingsManager.OVPN3_OPTION_PROXY_USERNAME_NATIVE, settingsManager.getOvpn3ProxyUsername());
            doSetOption(SettingsManager.OVPN3_OPTION_PROXY_PASSWORD_NATIVE, settingsManager.getOvpn3ProxyPassword());
            doSetOption(SettingsManager.OVPN3_OPTION_PROXY_ALLOW_CLEARTEXT_AUTH_NATIVE, toOptionValue(settingsManager.isOvpn3ProxyAllowCleartextAuth()));
        }
    }

    public void run()
    {
        if(openVPNClient == null)
        {
            String errMsg = "OpenVPNTunnel.run(): OpenVPN client is not initialized";

            EddieLogger.error(errMsg);

            vpnService.handleConnectionError();
        }

        EddieLibraryResult result = openVPNClient.start(this);

        if(result.code != EddieLibrary.SUCCESS)
        {
            String errMsg = String.format(Locale.getDefault(), "OpenVPNTunnel.run(): Failed to start OpenVPN client. %s", result.description);

            EddieLogger.error(errMsg);

            vpnService.handleConnectionError();
        }
    }

    public synchronized void cleanup() throws Exception
    {
        if(openVPNClient == null)
        {
            String errMsg = "OpenVPNTunnel.cleanup(): OpenVPN client is not initialized";

            EddieLogger.error(errMsg);

            throw new Exception(errMsg);
        }

        try
        {
            EddieLibraryResult result = openVPNClient.stop(this);

            if(result.code != EddieLibrary.SUCCESS)
            {
                String errMsg = String.format(Locale.getDefault(), "OpenVPNTunnel.cleanup(): Failed to stop OpenVPN client. %s", result.description);

                EddieLogger.error(errMsg);

                throw new Exception(errMsg);
            }
        }
        finally
        {
            clearContexts();
        }

        openVPNClient = null;
    }

    public synchronized void onConnectionDataChanged(VPNConnectionStats connectionData)
    {
        EddieLogger.debug("OpenVPNTunnel.onConnectionDataChanged()");
    }

    private boolean quickConnectFailureError(String s)
    {
        boolean quickConnectError = false;
        String[] quickConnectErrors = new String[]{ "CONNECTION_TIMEOUT",
                                                    "HANDSHAKE_TIMEOUT",
                                                    "AUTH_FAILED",
                                                    "NETWORK_EOF_ERROR",
                                                    "NETWORK_RECV_ERROR",
                                                    "TRANSPORT_ERROR",
                                                    "TLS_VERSION_MIN",
                                                    "TLS_AUTH_FAIL",
                                                    "CERT_VERIFY_FAIL",
                                                    "PEM_PASSWORD_FAIL",
                                                    "TCP_CONNECT_ERROR",
                                                    "UDP_CONNECT_ERROR",
                                                    "CLIENT_SETUP"
                                                  };

        for(int i = 0; i < quickConnectErrors.length && quickConnectError == false; i++)
        {
            if(s.equals(quickConnectErrors[i]))
                quickConnectError = true;
        }

        return quickConnectError;
    }

    private boolean ignoredError(String s)
    {
        boolean ignoreWarning = false;
        String[] ignoredKeys = new String[]{ "PKTID_INVALID",
                                             "PKTID_BACKTRACK",
                                             "PKTID_EXPIRE",
                                             "PKTID_REPLAY",
                                             "PKTID_TIME_BACKTRACK"
                                           };

        for(int i = 0; i < ignoredKeys.length && ignoreWarning == false; i++)
        {
            if(s.equals(ignoredKeys[i]))
                ignoreWarning = true;
        }

        return ignoreWarning;
    }
}
