// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 20 June 2018 - author: ProMIND - initial release. Based on revised code from com.eddie.android. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.LocationManager;
import android.net.VpnService;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

import org.w3c.dom.Document;

public class VPNManager implements MessageHandlerListener, ServiceConnection, EddieEventListener
{
    public static final int VPN_REQUEST_PERMISSION = 3000;

    private Context appContext = null;
    private Intent vpnServiceIntent = null;
    private boolean vpnServiceBoundToAppContext = false;
    private Messenger serviceMessenger = null;
    private Messenger clientMessenger = null;
    private String vpnProfile = "";
    private ArrayList<String> vpnProfileStrings = new ArrayList<String>();
    private SupportTools supportTools = null;
    private SettingsManager settingsManager = null;
    private EddieEvent eddieEvent = null;
    private ArrayList<String> localIPv4Routes = null, localIPv6Routes = null;
    private static VPN vpn = null;
    private int currentReconnectionRetry = 0, maxReconnectionRetries = 0;
    private boolean reconnectionPending = false;

    private MockLocation gpsMockLocation = null;
    private MockLocation networkMockLocation = null;
    private MockLocation fusedMockLocation = null;
    private CountryContinent.CountryCoordinates gpsSpoofingCoordinates = null;

    public VPNManager(Context context)
    {
        setContext(context);

        vpn = new VPN();

        supportTools = EddieApplication.supportTools();

        settingsManager = EddieApplication.settingsManager();

        eddieEvent = EddieApplication.eddieEvent();

        eddieEvent.subscribeListener(this);

        localIPv4Routes = supportTools.getLocalIPs(false);

        localIPv6Routes = supportTools.getLocalIPs(true);

        vpn.setConnectionStatus(VPN.Status.NOT_CONNECTED);

        vpnServiceIntent = null;

        gpsMockLocation = null;
        networkMockLocation = null;
        fusedMockLocation = null;
        gpsSpoofingCoordinates = null;

        currentReconnectionRetry = 0;
        maxReconnectionRetries = 0;
        reconnectionPending = false;
    }

    @Override
    protected void finalize() throws Throwable
    {
        try
        {
            if(vpnServiceBoundToAppContext == true)
                unbindService(true);

            if(eddieEvent != null)
                eddieEvent.unsubscribeListener(this);
        }
        catch(Exception e)
        {
            EddieLogger.error("VPNManager.finalize() Exception: %s", e);
        }
        finally
        {
            try
            {
                super.finalize();
            }
            catch(Exception e)
            {
                EddieLogger.error("VPNManager.finalize(): super() exception: %s", e);
            }
        }
    }

    public void setContext(Context context)
    {
        if(vpnServiceBoundToAppContext == true)
            unbindService(true);

        appContext = context;
    }

    public VPN vpn()
    {
        return vpn;
    }

    public boolean isVpnPermissionGranted()
    {
        return(VpnService.prepare(EddieApplication.applicationContext()) == null);
    }

    public boolean isVpnConnectionReserved()
    {
        return (vpn.getConnectionStatus() == VPN.Status.RESERVED);
    }

    public boolean isVpnConnectionStarted()
    {
        return (vpn.getConnectionStatus() == VPN.Status.CONNECTING || vpn.getConnectionStatus() == VPN.Status.CONNECTED);
    }

    public boolean isVpnConnectionStopped()
    {
        return (isVpnConnectionStarted() == false && isVpnConnectionPaused() == false);
    }

    public boolean isVpnConnectionPaused()
    {
        return (vpn.getConnectionStatus() == VPN.Status.PAUSED_BY_SYSTEM || vpn.getConnectionStatus() == VPN.Status.PAUSED_BY_USER);
    }

    public boolean isVpnConnectionLocked()
    {
        return (vpn.getConnectionStatus() == VPN.Status.LOCKED);
    }

    public void reserveVpnConnection()
    {
        if(vpn.getConnectionStatus() != VPN.Status.NOT_CONNECTED && vpn.getConnectionStatus() != VPN.Status.UNDEFINED)
        {
            EddieLogger.error("VPN connection reservation is not permitted now. (VPN is %s)", vpn.connectionStatusDescription());

            return;
        }

        EddieLogger.info("VPN connection has been reserved");

        vpn.setConnectionStatus(VPN.Status.RESERVED);
    }

    public void cancelVpnConnectionReservation()
    {
        if(vpn.getConnectionStatus() != VPN.Status.RESERVED)
        {
            EddieLogger.error("VPN connection is not currently reserved. (VPN is %s)", vpn.connectionStatusDescription());

            return;
        }

        EddieLogger.info("VPN connection reservation has been cancelled");

        vpn.setConnectionStatus(VPN.Status.NOT_CONNECTED);
    }

    public void setProfile(String profile)
    {
        vpnProfile = profile;
    }

    public void addProfileString(String str)
    {
        if(str.isEmpty())
            return;

        vpnProfileStrings.add(str);
    }

    public CountryContinent.CountryCoordinates getGpsSpoofingCoordinates()
    {
        return gpsSpoofingCoordinates;
    }

    public boolean isGpsSpoofingEnabled()
    {
        return (gpsSpoofingCoordinates != null);
    }

    public void clearProfile()
    {
        vpnProfile = "";

        vpnProfileStrings.clear();
    }

    public boolean startConnection()
    {
        boolean result = false;
        String logEntry;

        if(appContext == null)
        {
            EddieLogger.error("VPNManager.start(): appContext is null.");

            return false;
        }

        if(isVpnConnectionStarted() == true)
        {
            EddieLogger.error("VPNManager: start command ignored. Connection already started.");

            return false;
        }
        else if(isVpnConnectionPaused() == true)
        {
            EddieLogger.error("VPNManager: start command ignored. Connection is paused.");

            return false;
        }

        currentReconnectionRetry = 0;
        maxReconnectionRetries = 0;
        reconnectionPending = false;

        if(vpnServiceBoundToAppContext == true)
            unbindService(true);

        vpnServiceBoundToAppContext = false;

        vpn.clearDnsEntries();

        vpnServiceIntent = new Intent(appContext, VPNService.class);

        onStatusChanged(VPN.Status.CONNECTING, appContext.getResources().getString(vpn.connectionStatusResourceDescription(VPN.Status.CONNECTING)));

        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.N)
        {
            logEntry = "VPN Lock is ";

            if(settingsManager.isVpnLockEnabled())
                logEntry += "enabled";
            else
                logEntry += "disabled";

            EddieLogger.info(logEntry);
        }

        logEntry = "Local networks are ";

        if(settingsManager.areLocalNetworksExcluded() && NetworkStatusReceiver.getNetworkType() != NetworkStatusReceiver.NetworkType.MOBILE)
            logEntry += "exempted from";
        else
            logEntry += "tunneled into";

        logEntry += " the VPN";

        if(settingsManager.areLocalNetworksExcluded() && NetworkStatusReceiver.getNetworkType() == NetworkStatusReceiver.NetworkType.MOBILE)
            logEntry += " (Device is currently connected to mobile network therefore local networks are tunneled by default)";

        EddieLogger.info(logEntry);

        if(vpn.getType() == VPN.Type.OPENVPN && settingsManager.getAirVPNOpenVPNCipher().equals(SettingsManager.AIRVPN_CIPHER_SERVER) == false && (vpn.getConnectionMode() == VPN.ConnectionMode.AIRVPN_SERVER || vpn.getConnectionMode() == VPN.ConnectionMode.QUICK_CONNECT))
        {
            logEntry = "Selected cipher: " + settingsManager.getAirVPNOpenVPNCipher();

            EddieLogger.info(logEntry);
        }

        try
        {
            handleActivityStart();

            result = true;
        }
        catch(Exception e)
        {
            EddieLogger.error("VPNManager.start() exception: %s", e);

            onStatusChanged(VPN.Status.NOT_CONNECTED, e.getMessage());

            vpnServiceIntent = null;

            vpnServiceBoundToAppContext = false;

            result = false;
        }

        return result;
    }

    public void stopConnection()
    {
        if(isVpnConnectionStopped() == true)
        {
            EddieLogger.error("VPNManager: stop command ignored. Connection already stopped.");

            onStatusChanged(VPN.Status.NOT_CONNECTED, appContext.getResources().getString(vpn.connectionStatusResourceDescription(VPN.Status.NOT_CONNECTED)));

            return;
        }

        reconnectionPending = false;

        try
        {
            onStatusChanged(VPN.Status.DISCONNECTING, appContext.getResources().getString(vpn.connectionStatusResourceDescription(VPN.Status.DISCONNECTING)));

            sendStopMessage();

            handleActivityStop();
        }
        catch(Exception e)
        {
            EddieLogger.error("VPNManager.stop() exception: %s", e);

            onStatusChanged(VPN.Status.NOT_CONNECTED, e.getMessage());
        }

        vpn.clearDnsEntries();

        onStatusChanged(VPN.Status.NOT_CONNECTED, appContext.getResources().getString(vpn.connectionStatusResourceDescription(VPN.Status.NOT_CONNECTED)));

        vpnServiceIntent = null;

        vpnServiceBoundToAppContext = false;
    }

    public boolean pauseConnection()
    {
        boolean result;

        if(vpn.getType() != VPN.Type.OPENVPN)
        {
            EddieLogger.error("VPNManager: pause command ignored. VPN type is not OpenVPN");

            return false;
        }

        if(isVpnConnectionPaused() == true)
        {
            EddieLogger.error("VPNManager: pause command ignored. Connection already paused");

            return false;
        }

        try
        {
            sendPauseMessage();

            result = true;
        }
        catch(Exception e)
        {
            EddieLogger.error("VPNManager.pause() exception: %s", e);

            onStatusChanged(VPN.Status.NOT_CONNECTED, e.getMessage());

            result = false;
        }

        return result;
    }

    public boolean resumeConnection()
    {
        boolean result = false;

        if(vpn.getType() != VPN.Type.OPENVPN)
        {
            EddieLogger.error("VPNManager: resume command ignored. VPN type is not OpenVPN");

            return false;
        }

        if(isVpnConnectionPaused() == false)
        {
            EddieLogger.error("VPNManager: resume command ignored. Connection is not paused");

            return false;
        }

        try
        {
            sendResumeMessage();

            result = true;
        }
        catch(Exception e)
        {
            EddieLogger.error("VPNManager.resume() exception: %s", e);

            onStatusChanged(VPN.Status.NOT_CONNECTED, e.getMessage());

            result = false;
        }

        return result;
    }

    public void handleActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode != VPN_REQUEST_PERMISSION)
            return;

        if(resultCode == RESULT_OK)
            doBindService();
        else
        {
            onStatusChanged(VPN.Status.CONNECTION_CANCELED, appContext.getResources().getString(vpn.connectionStatusResourceDescription(VPN.Status.CONNECTION_CANCELED)));

            EddieLogger.error("VPNManager: Connection cancelled by the system (Maybe another app has \"VPN Always On\" permission granted?)");

            vpnServiceIntent = null;

            vpnServiceBoundToAppContext = false;
        }
    }

    public void handleActivityStart()
    {
        bindService();
    }

    public void handleActivityStop()
    {
        if(vpnServiceBoundToAppContext == true)
            unbindService(true);
    }

    private void onStatusChanged(VPN.Status status, String message)
    {
        vpn.setConnectionStatus(status);

        if(eddieEvent != null)
            eddieEvent.onVpnStatusChanged(status, message);
    }

    public void onNullBinding(ComponentName name)
    {
        EddieLogger.error("VPNManager.onNullBinding(): Cannot bind VPN service to app: %s", name.toString());

        vpnServiceBoundToAppContext = false;

        onStatusChanged(VPN.Status.NOT_CONNECTED, appContext.getResources().getString(vpn.connectionStatusResourceDescription(VPN.Status.NOT_CONNECTED)));
    }

    public void onBindingDied(ComponentName name)
    {
        EddieLogger.error("VPNManager.onBindingDied(): Binding died during VPN prepare: %s", name.toString());

        vpnServiceIntent = null;

        vpnServiceBoundToAppContext = false;

        onStatusChanged(VPN.Status.NOT_CONNECTED, appContext.getResources().getString(vpn.connectionStatusResourceDescription(VPN.Status.NOT_CONNECTED)));
    }

    public void onServiceConnected(ComponentName name, IBinder service)
    {
        clientMessenger = new Messenger(new MessageHandler(this));
        serviceMessenger = new Messenger(service);

        sendBindMessage(true);

        sendStartMessage();

        vpnServiceBoundToAppContext = true;
    }

    public void onServiceDisconnected(ComponentName name)
    {
        vpnServiceBoundToAppContext = false;

        vpnServiceIntent = null;
    }

    private void sendStartMessage()
    {
        Bundle bundle = null;

        if(appContext == null || vpnServiceIntent == null)
        {
            EddieLogger.error("VPNManager.sendStartMessage(): appContext and vpnServiceIntent are not initialized.");

            onStatusChanged(VPN.Status.NOT_CONNECTED, appContext.getResources().getString(R.string.connection_vpn_error));

            return;
        }

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            appContext.startForegroundService(vpnServiceIntent);
        else
            appContext.startService(vpnServiceIntent);

        bundle = createProfileBundle();

        if(bundle == null)
        {
            EddieLogger.error("VPNManager.sendStartMessage(): cannot create a bundle.");

            onStatusChanged(VPN.Status.NOT_CONNECTED, appContext.getResources().getString(R.string.connection_vpn_error));

            return;
        }

        sendMessage(Message.obtain(null, VPNService.MSG_START), bundle);
    }

    public Bundle createProfileBundle()
    {
        String routeSpec[], routeIP;
        long maskIP;

        if(vpnProfile.isEmpty() == true)
            return null;

        String profile = "";

        if(vpn.getType() == VPN.Type.OPENVPN)
        {
            // Exclude local networks from OpenVPN

            if(settingsManager.areLocalNetworksExcluded() && NetworkStatusReceiver.getNetworkType() != NetworkStatusReceiver.NetworkType.MOBILE)
            {
                for(String route : localIPv4Routes)
                {
                    routeSpec = route.split("/");

                    try
                    {
                        maskIP = 0xffffffffL << (32 - Integer.parseInt(routeSpec[1]));
                    }
                    catch(NumberFormatException e)
                    {
                        maskIP = 0xffffffffL;
                    }

                    routeIP = supportTools.longToIP(supportTools.IPToLong(routeSpec[0]) & maskIP);

                    vpnProfileStrings.add(String.format("route %s %s net_gateway", routeIP, supportTools.longToIP(maskIP)));
                }
            }
        }

        if(vpnProfileStrings.size() == 0)
            profile = vpnProfile + "\n";
        else
        {
            boolean directivesAdded = false;

            vpnProfile = vpnProfile.replace("\r\n", "\n");

            String[] lines = vpnProfile.split("\n");

            for(String curLine : lines)
            {
                if(directivesAdded == false && !curLine.equals("") && curLine.charAt(0) == '<')
                {
                    for(String profileString : vpnProfileStrings)
                        profile += profileString + "\n";

                    directivesAdded = true;
                }

                profile += curLine + "\n";
            }
        }

        profile = profile.replace("\r\n", "\n");

        if(profile.isEmpty() == true)
            return null;

        Bundle data = new Bundle();

        data.putString(VPNService.COMMAND_PROFILE, profile);

        return data;
    }

    private void sendStopMessage()
    {
        sendMessage(Message.obtain(null, VPNService.MSG_STOP), null);
    }

    private void sendPauseMessage()
    {
        sendMessage(Message.obtain(null, VPNService.MSG_PAUSE), null);
    }

    private void sendResumeMessage()
    {
        sendMessage(Message.obtain(null, VPNService.MSG_RESUME), null);
    }

    private void sendBindMessage(boolean bind)
    {
        sendMessage(Message.obtain(null, VPNService.MSG_BIND, bind ? VPNService.MSG_BIND_ARG_ADD : VPNService.MSG_BIND_ARG_REMOVE, 0), null);
    }

    private void sendMessage(Message msg, Bundle payload)
    {
        if(serviceMessenger == null)
        {
            if(EddieApplication.isVisible())
                supportTools.infoDialog(String.format(Locale.getDefault(), appContext.getResources().getString(R.string.conn_cannot_start_vpnservice)), true);

            EddieLogger.error("VPNManager.sendMessage(): serviceMessenger is null");

            onStatusChanged(VPN.Status.NOT_CONNECTED, appContext.getResources().getString(R.string.conn_cannot_start_vpnservice));

            return;
        }

        msg.replyTo = clientMessenger;

        if(payload != null)
            msg.setData(payload);

        try
        {
            serviceMessenger.send(msg);
        }
        catch(RemoteException e)
        {
            EddieLogger.error("VPNManager.sendMessage() exception: %s", e);
        }
    }

    private void bindService()
    {
        Intent intent = VpnService.prepare(EddieApplication.applicationContext());

        if(intent != null)
        {
            Activity activity = (Activity)appContext;

            if(activity != null)
                activity.startActivityForResult(intent, VPN_REQUEST_PERMISSION);
            else
                EddieLogger.error("VPNManager.bindService(): Failed to cast Context to Activity");
        }
        else
            handleActivityResult(VPN_REQUEST_PERMISSION, Activity.RESULT_OK, null);
    }

    private void doBindService()
    {
        if(appContext == null || vpnServiceIntent == null)
        {
            EddieLogger.error("VPNManager.sendStartMessage(): appContext and vpnServiceIntent are not initialized.");

            onStatusChanged(VPN.Status.NOT_CONNECTED, appContext.getResources().getString(R.string.connection_vpn_error));

            stopConnection();

            return;
        }

        try
        {
            if(!appContext.bindService(vpnServiceIntent, this, Context.BIND_AUTO_CREATE | Context.BIND_ALLOW_ACTIVITY_STARTS))
                throw new Exception("VPNManager.doBindService(): Failed to bind service");
        }
        catch(Exception e)
        {
            onStatusChanged(VPN.Status.NOT_CONNECTED, e.getMessage());

            vpnServiceIntent = null;

            vpnServiceBoundToAppContext = false;
        }
    }

    private void unbindService(boolean unbind)
    {
        if(clientMessenger != null)
        {
            if(unbind)
                sendBindMessage(false);

            clientMessenger = null;
        }

        if(vpnServiceBoundToAppContext == true)
        {
            try
            {
                if(unbind == true)
                {
                    appContext.unbindService(this);

                    vpnServiceIntent = null;
                }
            }
            catch(Exception e)
            {
                onStatusChanged(VPN.Status.NOT_CONNECTED, appContext.getResources().getString(vpn.connectionStatusResourceDescription(VPN.Status.NOT_CONNECTED)));

                vpnServiceIntent = null;
            }
        }

        vpnServiceBoundToAppContext = false;
    }

    public synchronized void onMessage(Message msg)
    {
        if(msg == null)
            return;

        switch(msg.what)
        {
            case VPNService.MSG_STATUS:
            {
                VPN.Status status = VPN.Status.fromValue(msg.arg1);

                onStatusChanged(status, msg.getData().getString(VPNService.MESSAGE_TEXT));
            }
            break;

            case VPNService.MSG_REVOKE:
            {
                if(vpnServiceBoundToAppContext == true)
                    unbindService(true);
            }
            break;
        }
    }

    private void startMockingLocation()
    {
        AirVPNServer server = null;
        HashMap<String, String> profileInfo = null;
        CountryContinent.CountryCoordinates mockCoordinates = null;

        if(isGpsSpoofingEnabled() == true)
            return;

        try
        {
            if(gpsMockLocation != null)
                gpsMockLocation.delete();

            if(networkMockLocation != null)
                networkMockLocation.delete();

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.S && fusedMockLocation != null)
                fusedMockLocation.delete();

            try
            {
                gpsMockLocation = new MockLocation(LocationManager.GPS_PROVIDER, settingsManager.getGpsSpoofingInterval());
            }
            catch(Exception e)
            {
                EddieLogger.error("MainActivity.startMockingLocation(): %s (%s provider)", e.getMessage(), LocationManager.GPS_PROVIDER);

                gpsMockLocation = null;
            }

            try
            {
                networkMockLocation = new MockLocation(LocationManager.NETWORK_PROVIDER, settingsManager.getGpsSpoofingInterval());
            }
            catch(Exception e)
            {
                EddieLogger.error("MainActivity.startMockingLocation(): %s (%s provider)", e.getMessage(), LocationManager.NETWORK_PROVIDER);

                networkMockLocation = null;
            }

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.S)
            {
                try
                {
                    fusedMockLocation = new MockLocation(LocationManager.FUSED_PROVIDER, settingsManager.getGpsSpoofingInterval());
                }
                catch(Exception e)
                {
                    EddieLogger.error("MainActivity.startMockingLocation(): %s (%s provider)", e.getMessage(), LocationManager.FUSED_PROVIDER);

                    fusedMockLocation = null;
                }
            }

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.S)
            {
                if(gpsMockLocation == null && networkMockLocation == null && fusedMockLocation == null)
                {
                    supportTools.infoDialog(R.string.gps_spoofing_mock_is_not_granted, true);

                    return;
                }
            }
            else
            {
                if(gpsMockLocation == null && networkMockLocation == null)
                {
                    supportTools.infoDialog(R.string.gps_spoofing_mock_is_not_granted, true);

                    return;
                }
            }

            profileInfo = vpn().getProfileInfo();

            if(profileInfo != null)
            {
                if(profileInfo.containsKey("server"))
                {
                    server = EddieApplication.airVPNManifest().getServerByIP(profileInfo.get("server"));

                    if(server != null)
                        mockCoordinates = CountryContinent.getCountryCoordinates(server.getCountryCode());
                    else
                        mockCoordinates = CountryContinent.getRandomCountryCoordinates();
                }
                else
                    mockCoordinates = CountryContinent.getRandomCountryCoordinates();
            }
            else
                mockCoordinates = CountryContinent.getRandomCountryCoordinates();

            try
            {
                if(gpsMockLocation != null)
                    gpsMockLocation.setLocation(mockCoordinates);

                if(networkMockLocation != null)
                    networkMockLocation.setLocation(mockCoordinates);

                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.S && fusedMockLocation != null)
                    fusedMockLocation.setLocation(mockCoordinates);
            }
            catch(Exception e)
            {
                supportTools.infoDialog(R.string.gps_spoofing_mock_is_not_granted, true);

                EddieLogger.error("MainActivity.startMockingLocation(): %s", e.getMessage());

                stopMockingLocation();
            }

        }
        catch(SecurityException e)
        {
            supportTools.infoDialog(R.string.gps_spoofing_mock_is_not_granted, true);

            EddieLogger.error("MainActivity.startMockingLocation(): %s", e.getMessage());

            stopMockingLocation();
        }
        catch(IllegalArgumentException e)
        {
            EddieLogger.error("MainActivity.startMockingLocation(): %s", e.getMessage());
        }
        catch(Exception e)
        {
            EddieLogger.error("MainActivity.startMockingLocation(): %s", e.getMessage());
        }

        gpsSpoofingCoordinates = mockCoordinates;
    }

    private void stopMockingLocation()
    {
        try
        {
            if(gpsMockLocation != null)
                gpsMockLocation.delete();

            if(networkMockLocation != null)
                networkMockLocation.delete();

            if(fusedMockLocation != null)
                fusedMockLocation.delete();

            gpsMockLocation = null;
            networkMockLocation = null;
            fusedMockLocation = null;
        }
        catch(SecurityException e)
        {
            EddieLogger.error("MainActivity.stopMockingLocation(): %s", e.getMessage());
        }
        catch(Exception e)
        {
            EddieLogger.error("MainActivity.stopMockingLocation(): %s", e.getMessage());
        }

        gpsSpoofingCoordinates = null;
    }

    // Eddie events

    public void onVpnConnectionStatsChanged(final VPNConnectionStats connectionStats)
    {
    }

    public void onVpnStatusChanged(final VPN.Status status, final String message)
    {
        if(status == VPN.Status.NOT_CONNECTED)
        {
            eddieEvent.onVpnConnectionStatsChanged(null);

            if(reconnectionPending == true && isVpnConnectionStopped())
            {
                String reconnTag = "";

                if(maxReconnectionRetries == SettingsManager.SYSTEM_OPTION_VPN_RECONNECTION_RETRIES_INFINITE)
                    reconnTag = "(infinite)";
                else
                    reconnTag = String.format("%s", maxReconnectionRetries);

                EddieLogger.info("Trying to reconnect VPN. Attempt %d out of %s", currentReconnectionRetry, reconnTag);

                startConnection();
            }
        }
        else if(status == VPN.Status.CONNECTED)
        {
            reconnectionPending = false;

            currentReconnectionRetry = 0;
        }

        if(settingsManager.isGpsSpoofingEnabled() == true)
        {
            if(status == VPN.Status.CONNECTED)
                startMockingLocation();
            else if(status == VPN.Status.NOT_CONNECTED)
                stopMockingLocation();
        }
    }

    public void onVpnAuthFailed(final VPNEvent oe)
    {
        try
        {
            stopConnection();
        }
        catch(Exception e)
        {
            EddieLogger.error("VPNManager.vpnAuthFailed() exception: %s", e.getMessage());
        }
    }

    public void onVpnError(final VPNEvent oe)
    {
        if(oe.type == VPNEvent.DISCONNECTED || oe.type == VPNEvent.FATAL_ERROR || oe.type == VPNEvent.ERROR)
        {
            if(oe.type == VPNEvent.DISCONNECTED)
            {
                String message = "VPNManager.onVpnError(): Unexpected VPN disconnection detected.";

                if(oe.info.isEmpty() == false)
                    message += "(" + oe.info + ")";

                EddieLogger.error(message);
            }
            else
                EddieLogger.error(oe.info);

            if(settingsManager.isVpnReconnectEnabled() && settingsManager.isVpnLockEnabled() == false)
            {
                EddieLogger.info("Waiting for VPN to fully disconnect before attempting reconnection.");

                maxReconnectionRetries = settingsManager.getVpnReconnectionRetries();

                if(currentReconnectionRetry < maxReconnectionRetries || maxReconnectionRetries == SettingsManager.SYSTEM_OPTION_VPN_RECONNECTION_RETRIES_INFINITE)
                {
                    currentReconnectionRetry++;

                    // Set reconnectionPending status. Request will be completed in
                    // onVpnStatusChanged() as soon as VPN status is set to NOT_CONNECTED
                    // at the end of vpnManager.stop() execution

                    reconnectionPending = true;
                }
                else
                {
                    EddieLogger.info("WARNING: Maximum reconnection attempts reached.");

                    reconnectionPending = false;
                }
            }

            try
            {
                if(isVpnConnectionStarted())
                    stopConnection();
            }
            catch(Exception e)
            {
                EddieLogger.error("VPNManager.onVpnError() exception: %s", e.getMessage());
            }
        }
    }

    public void onVpnReconnect(final VPNEvent oe)
    {
        if(oe.type == VPNEvent.RECONNECTING)
        {
            try
            {
                if(isVpnConnectionStarted())
                {
                    EddieLogger.info(oe.info);

                    reconnectionPending = true;

                    stopConnection();
                }
            }
            catch(Exception e)
            {
                EddieLogger.error("VPNManager.onVpnError() exception: %s", e.getMessage());
            }
        }
    }

    public void onMasterPasswordChanged()
    {
    }

    public void onAirVPNLogin()
    {
    }

    public void onAirVPNLoginFailed(String message)
    {
    }

    public void onAirVPNLogout()
    {
    }

    public void onAirVPNUserDataChanged()
    {
    }

    public void onAirVPNUserProfileReceived(Document document)
    {
    }

    public void onAirVPNUserProfileDownloadError()
    {
    }

    public void onAirVPNUserProfileChanged()
    {
    }

    public void onAirVPNManifestReceived(Document document)
    {
    }

    public void onAirVPNManifestDownloadError()
    {
    }

    public void onAirVPNManifestChanged()
    {
    }

    public void onAirVPNIgnoredManifestDocumentRequest()
    {
    }

    public void onAirVPNIgnoredUserDocumentRequest()
    {
    }

    public void onAirVPNRequestError(String message)
    {
    }

    public void onCancelConnection()
    {
        try
        {
            stopConnection();
        }
        catch(Exception e)
        {
            EddieLogger.error("VPNManager.onCancelConnection(): %s", e.getMessage());
        }
    }

    public void onSaveState()
    {
    }

    public void onRestoreState(Bundle bundle)
    {
    }
}
