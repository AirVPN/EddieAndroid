// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 13 October 2018 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Build;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

public class CountryContinent
{
    private final static String COUNTRY_CONTINENT_FILE_NAME = "country_continent.csv";
    private final static String COUNTRY_NAMES_FILE_NAME = "country_names";
    private final static String COUNTRY_COORDINATES_FILE_NAME = "country_coordinates.csv";

    private static HashMap<String, String> countryContinent = null;
    private static HashMap<String, String> countryName = null;
    private static HashMap<String, CountryCoordinates> countryCoordinate = null;

    public final static String EARTH = "EARTH";
    public final static String AFRICA = "AFR";
    public final static String AMERICA = "AME";
    public final static String NORTH_AMERICA = "NAM";
    public final static String SOUTH_AMERICA = "SAM";
    public final static String ASIA = "ASI";
    public final static String EUROPE = "EUR";
    public final static String OCEANIA = "OCE";
    public final static String ANTARCTICA = "ANT";

    public static class CountryCoordinates
    {
        CountryCoordinates()
        {
            countryCode = "";
            latitude = 999;
            longitude = 999;
        }

        private String countryCode;
        private double latitude;
        private double longitude;

        public String getCountryCode()
        {
            return countryCode;
        }

        public double getLatitude()
        {
            return latitude;
        }

        public double getLongitude()
        {
            return longitude;
        }
    }

    CountryContinent()
    {
        if(countryContinent == null)
            loadData();
    }

    private static void loadData()
    {
        Context appContext = EddieApplication.applicationContext();
        String countryFileName = "";
        AssetManager assetManager = appContext.getAssets();
        InputStream inputStream = null;

        try
        {
            inputStream = assetManager.open(COUNTRY_CONTINENT_FILE_NAME);

            if(inputStream != null)
            {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                String line = "";
                String row[] = null;

                countryContinent = new HashMap<String, String>();

                while((line = bufferedReader.readLine()) != null)
                {
                    row = line.split(",");

                    if(row != null && row.length == 2)
                        countryContinent.put(row[0], row[1]);
                }
            }
        }
        catch(Exception e)
        {
            EddieLogger.warning("CountryContinent(): %s not found.", COUNTRY_CONTINENT_FILE_NAME);
        }
        finally
        {
            try
            {
                inputStream.close();
            }
            catch(Exception e)
            {
            }
        }

        try
        {
            String locale = "en_US";

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                locale = appContext.getResources().getConfiguration().getLocales().get(0).toString();
            else
                locale = appContext.getResources().getConfiguration().locale.toString();

            if(!locale.equals("zh_RCN") && !locale.equals("zh_RTW"))
            {
                locale = locale.substring(0, 2);

                if(locale.equals("zh"))
                    locale = "zh_RCN";
            }

            countryFileName = String.format("%s.%s", COUNTRY_NAMES_FILE_NAME, locale);

            if(Arrays.asList(appContext.getResources().getAssets().list("")).contains(countryFileName) == false)
                countryFileName = COUNTRY_NAMES_FILE_NAME + ".en";

            inputStream = assetManager.open(countryFileName);

            if(inputStream != null)
            {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                String line = "";
                String row[] = null;

                countryName = new HashMap<String, String>();

                while((line = bufferedReader.readLine()) != null)
                {
                    row = line.split(",");

                    if(row != null && row.length == 2)
                        countryName.put(row[0], row[1]);
                }
            }
        }
        catch(Exception e)
        {
            EddieLogger.warning("CountryContinent(): %s not found.", countryFileName);
        }
        finally
        {
            try
            {
                inputStream.close();
            }
            catch(Exception e)
            {
            }
        }

        try
        {
            inputStream = assetManager.open(COUNTRY_COORDINATES_FILE_NAME);

            if(inputStream != null)
            {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                String line = "";
                String row[] = null;

                countryCoordinate = new HashMap<String, CountryCoordinates>();

                while((line = bufferedReader.readLine()) != null)
                {
                    row = line.split(",");

                    if(row != null && row.length == 3)
                    {
                        CountryCoordinates coord = new CountryCoordinates();

                        coord.countryCode = row[0];

                        try
                        {
                            coord.latitude = Double.parseDouble(row[1]);
                        }
                        catch(NumberFormatException e)
                        {
                            coord.latitude = 0;
                        }

                        try
                        {
                            coord.longitude = Double.parseDouble(row[2]);
                        }
                        catch(NumberFormatException e)
                        {
                            coord.longitude = 0;
                        }

                        countryCoordinate.put(row[0], coord);
                    }
                }
            }
        }
        catch(Exception e)
        {
            EddieLogger.warning("CountryContinent(): %s not found.", COUNTRY_COORDINATES_FILE_NAME);
        }
        finally
        {
            try
            {
                inputStream.close();
            }
            catch(Exception e)
            {
            }
        }
    }

    public static void reload()
    {
        loadData();
    }

    public static String getCountryContinent(String countryCode)
    {
        if(countryContinent == null)
            return "";

        String continent = "";

        countryCode = countryCode.toUpperCase();

        if(countryContinent.containsKey(countryCode))
            continent = countryContinent.get(countryCode);

        return continent;
    }

    public static String getCountryName(String countryCode)
    {
        String name = "";

        if(countryName == null)
            return "";

        countryCode = countryCode.toUpperCase();

        if(countryName.containsKey(countryCode))
            name = countryName.get(countryCode);

        return name;
    }

    public static String getCountryCode(String countryName)
    {
        if(CountryContinent.countryName == null)
            return "";

        String code = "";

        for(Map.Entry<String, String> entry : CountryContinent.countryName.entrySet())
        {
            if(entry.getValue().equals(countryName))
                code = entry.getKey();
        }

        return code;
    }

    public static CountryCoordinates getCountryCoordinates(String countryCode)
    {
        CountryCoordinates coords = null;

        if(countryCode == null || countryCode.isEmpty() == true)
            return new CountryCoordinates();

        countryCode = countryCode.toUpperCase();

        if(countryCoordinate.containsKey(countryCode))
            coords = countryCoordinate.get(countryCode);

        return coords;
    }

    public static CountryCoordinates getRandomCountryCoordinates()
    {
        CountryCoordinates coords = null;
        List<CountryCoordinates> cc = new ArrayList<CountryCoordinates>(countryCoordinate.values());
        int ndx = new Random().nextInt(cc.size() - 1);

        coords = countryCoordinate.get(cc.get(ndx).getCountryCode());

        return coords;
    }

    public static TreeMap<String, String> getTreeMapCountry()
    {
        TreeMap<String, String> treeMap = new TreeMap<String, String>();

        for(Map.Entry entry : countryName.entrySet())
            treeMap.put(entry.getValue().toString(), entry.getKey().toString());

        return treeMap;
    }

    public static int countryCount()
    {
        return countryName.size();
    }

    public static TreeMap<String, String> getTreeMapContinent()
    {
        TreeMap<String, String> treeMap = new TreeMap<String, String>();

        for(Map.Entry entry : countryContinent.entrySet())
            treeMap.put(entry.getValue().toString(), entry.getKey().toString());

        return treeMap;
    }

    public static int continentCount()
    {
        return countryContinent.size();
    }

    public static String getContinentCode(String continentName)
    {
        String code;

        if(continentName.equalsIgnoreCase(EddieApplication.applicationContext().getResources().getString(R.string.context_menu_start_connection_earth_cap)) == true)
            code = CountryContinent.EARTH;
        else if(continentName.equalsIgnoreCase(EddieApplication.applicationContext().getResources().getString(R.string.context_menu_start_connection_europe_cap)) == true)
            code = CountryContinent.EUROPE;
        else if(continentName.equalsIgnoreCase(EddieApplication.applicationContext().getResources().getString(R.string.context_menu_start_connection_africa_cap)) == true)
            code = CountryContinent.AFRICA;
        else if(continentName.equalsIgnoreCase(EddieApplication.applicationContext().getResources().getString(R.string.context_menu_start_connection_asia_cap)) == true)
            code = CountryContinent.ASIA;
        else if(continentName.equalsIgnoreCase(EddieApplication.applicationContext().getResources().getString(R.string.context_menu_start_connection_america_cap)) == true)
            code = CountryContinent.AMERICA;
        else if(continentName.equalsIgnoreCase(EddieApplication.applicationContext().getResources().getString(R.string.context_menu_start_connection_oceania_cap)) == true)
            code = CountryContinent.OCEANIA;
        else
            code = "???";

        return code;
    }

    public static String getContinentName(String continentCode)
    {
        int res;

        switch(continentCode)
        {
            case CountryContinent.EARTH:
            {
                res = R.string.context_menu_start_connection_earth_cap;
            }
            break;

            case CountryContinent.EUROPE:
            {
                res = R.string.context_menu_start_connection_europe_cap;
            }
            break;

            case CountryContinent.AFRICA:
            {
                res = R.string.context_menu_start_connection_africa_cap;
            }
            break;

            case CountryContinent.ASIA:
            {
                res = R.string.context_menu_start_connection_asia_cap;
            }
            break;

            case CountryContinent.AMERICA:
            {
                res = R.string.context_menu_start_connection_america_cap;
            }
            break;

            case CountryContinent.OCEANIA:
            {
                res = R.string.context_menu_start_connection_oceania_cap;
            }
            break;

            default:
            {
                res = R.string.context_menu_start_connection_earth_cap;
            }
            break;
        }

        return EddieApplication.applicationContext().getResources().getString(res);
    }

    public static boolean isContinent(String code)
    {
        return code.equalsIgnoreCase(EARTH) ||
               code.equalsIgnoreCase(EUROPE) ||
               code.equalsIgnoreCase(AFRICA) ||
               code.equalsIgnoreCase(ASIA) ||
               code.equalsIgnoreCase(AMERICA) ||
               code.equalsIgnoreCase(NORTH_AMERICA) ||
               code.equalsIgnoreCase(SOUTH_AMERICA) ||
               code.equalsIgnoreCase(OCEANIA) ||
               code.equalsIgnoreCase(ANTARCTICA);
    }
}
