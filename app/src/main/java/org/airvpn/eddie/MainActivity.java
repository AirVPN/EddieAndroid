// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 20 June 2018 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.view.GravityCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements NetworkStatusListener, EddieEventListener
{
    public static final int ACTIVITY_RESULT_FILE_CHOOSER = 1000;
    public static final int ACTIVITY_RESULT_SETTINGS = 1001;

    public static final int QUICK_CONNECT_FRAGMENT = 0;
    public static final int SERVER_LIST_FRAGMENT = 1;
    public static final int CONNECT_OPENVPN_PROFILE_FRAGMENT = 2;
    public static final int CONNECTION_INFO_FRAGMENT = 3;

    private AirVPNUser airVPNUser = null;
    private AirVPNManifest airVPNManifest = null;

    private boolean userRequestedDisconnection = false;
    private boolean onGoingConnection = false;
    private boolean reconnectionPending = false;

    private SupportTools supportTools = null;

    private SettingsManager settingsManager = null;

    private QuickConnectFragment quickConnectFragment = null;
    private ConnectAirVPNServerFragment connectAirVPNServerFragment = null;
    private ConnectVpnProfileFragment connectVpnProfileFragment = null;
    private ConnectionInfoFragment connectionInfoFragment = null;

    private static VPNManager vpnManager = null;
    private EddieEvent eddieEvent = null;
    private Toolbar toolbar = null;
    private TabLayout tabLayout = null;
    private ViewPager viewPager = null;
    private ViewPagerAdapter viewPagerAdapter = null;
    private DrawerLayout drawer = null;
    private ActionBarDrawerToggle drawerToggle = null;
    private NavigationView navigationView = null;
    private NetworkStatusReceiver networkStatusReceiver = null;
    private Menu appMenu = null;
    private MenuItem menuItemLogout = null;
    private Timer timerAirVPNManifestRefresh = null;
    private Intent tileServiceIntent = null;

    private static VPN.Status lastVpnStatus = VPN.Status.UNDEFINED;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        EddieApplication.setMainActivity(this);

        supportTools = EddieApplication.supportTools();
        settingsManager = EddieApplication.settingsManager();

        supportTools.setLocale(getBaseContext());

        setContentView(R.layout.main_activity_layout);

        vpnManager = EddieApplication.vpnManager();

        vpnManager.setContext(this);

        networkStatusReceiver = EddieApplication.networkStatusReceiver();

        networkStatusReceiver.subscribeListener(this);

        eddieEvent = EddieApplication.eddieEvent();

        eddieEvent.subscribeListener(this);

        toolbar = (Toolbar)findViewById(R.id.toolbar);

        toolbar.setTitle("");

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager)findViewById(R.id.viewpager);

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        quickConnectFragment = new QuickConnectFragment();
        connectAirVPNServerFragment = new ConnectAirVPNServerFragment();
        connectVpnProfileFragment = new ConnectVpnProfileFragment();
        connectionInfoFragment = new ConnectionInfoFragment();

        viewPagerAdapter.addFragment(quickConnectFragment, getResources().getString(R.string.fragment_quick_connect_cap));
        viewPagerAdapter.addFragment(connectAirVPNServerFragment, getResources().getString(R.string.fragment_server_connect_cap));
        viewPagerAdapter.addFragment(connectVpnProfileFragment, getResources().getString(R.string.fragment_vpn_connect_cap));
        viewPagerAdapter.addFragment(connectionInfoFragment, getResources().getString(R.string.fragment_connection_info_cap));

        viewPager.setAdapter(viewPagerAdapter);

        quickConnectFragment.setVpnManager(vpnManager);

        connectAirVPNServerFragment.setVpnManager(vpnManager);

        connectVpnProfileFragment.setVpnManager(vpnManager);

        connectionInfoFragment.setVpnManager(vpnManager);
        connectionInfoFragment.hideConnectionStatus();

        tabLayout = (TabLayout)findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        drawer = (DrawerLayout)findViewById(R.id.drawer_layout);

        drawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

        navigationView = (NavigationView)findViewById(R.id.nav_view);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener()
        {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item)
            {
                navigationViewItemSelected(item);

                return true;
            }
        });

        airVPNUser = EddieApplication.airVPNUser();
        airVPNManifest = EddieApplication.airVPNManifest();
        timerAirVPNManifestRefresh = null;

        appMenu = navigationView.getMenu();

        menuItemLogout = appMenu.findItem(R.id.nav_logout);

        if(airVPNUser.isUserValid())
            menuItemLogout.setEnabled(true);
        else
            menuItemLogout.setEnabled(false);

        if(SupportTools.isTVDevice() == true)
        {
            tabLayout.selectTab(tabLayout.getTabAt(0));

            tabLayout.requestFocus();
        }

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
        {
            tileServiceIntent = new Intent(getApplicationContext(), AirVPNTileService.class);

            startService(tileServiceIntent);
        }

        importOpenVPNProfile(getIntent());

        onNewIntent(getIntent());
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        if(quickConnectFragment != null && quickConnectFragment.isAdded())
            getSupportFragmentManager().putFragment(outState,"quickConnectFragment", quickConnectFragment);

        if(connectAirVPNServerFragment != null && connectAirVPNServerFragment.isAdded())
            getSupportFragmentManager().putFragment(outState,"connectAirVPNServerFragment", connectAirVPNServerFragment);

        if(connectVpnProfileFragment != null && connectVpnProfileFragment.isAdded())
            getSupportFragmentManager().putFragment(outState,"connectOpenVpnProfileFragment", connectVpnProfileFragment);

        if(connectionInfoFragment != null && connectionInfoFragment.isAdded())
            getSupportFragmentManager().putFragment(outState,"connectionInfoFragment", connectionInfoFragment);

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle inState)
    {
        super.onRestoreInstanceState(inState);

        quickConnectFragment = (QuickConnectFragment)getSupportFragmentManager().getFragment(inState,"quickConnectFragment");
        connectAirVPNServerFragment = (ConnectAirVPNServerFragment)getSupportFragmentManager().getFragment(inState,"connectAirVPNServerFragment");
        connectVpnProfileFragment = (ConnectVpnProfileFragment)getSupportFragmentManager().getFragment(inState,"connectOpenVpnProfileFragment");
        connectionInfoFragment = (ConnectionInfoFragment)getSupportFragmentManager().getFragment(inState,"connectionInfoFragment");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode)
        {
            case ACTIVITY_RESULT_SETTINGS:
            {
                if(resultCode == RESULT_OK)
                {
                    VPN.Status currentConnectionStatus = vpnManager.vpn().getConnectionStatus();

                    if(currentConnectionStatus == VPN.Status.CONNECTED || currentConnectionStatus == VPN.Status.PAUSED_BY_USER || currentConnectionStatus == VPN.Status.PAUSED_BY_SYSTEM || currentConnectionStatus == VPN.Status.LOCKED)
                        supportTools.infoDialog(R.string.settings_changed, true);
                }
            }
            break;

            case VPNManager.VPN_REQUEST_PERMISSION:
            {
                if(vpnManager != null)
                    vpnManager.handleActivityResult(requestCode, resultCode, data);
            }
            break;

            default:
            {
            }
            break;
        }
    }

    @Override
    public void onStart()
    {
        super.onStart();

        if(settingsManager.getSystemApplicationTheme().equals(SettingsManager.SYSTEM_OPTION_APPLICATION_THEME_SYSTEM) == false)
        {
            switch(settingsManager.getSystemApplicationTheme())
            {
                case "Dark":
                {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                }
                break;

                case "Light":
                {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                }
                break;

                default:
                {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
                }
                break;
            }
        }
    }

    @Override
    public void onStop()
    {
        super.onStop();
    }

    @Override
    public void onPause()
    {
        super.onPause();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        if(networkStatusReceiver != null)
            networkStatusReceiver.unsubscribeListener(this);

        if(eddieEvent != null)
            eddieEvent.unsubscribeListener(this);

        if(vpnManager.vpn().getConnectionStatus() == VPN.Status.CONNECTED && connectionInfoFragment != null)
            connectionInfoFragment.stopVPNStats();
    }

    @Override
    protected void onResume()
    {
        Bundle vpnState = null;

        super.onResume();

        if(connectionInfoFragment != null)
            connectionInfoFragment.updateConnectionData(vpnManager.vpn().getVpnConnectionStats());

        if(drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);

        supportTools.removeShareFile();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        switch(newConfig.uiMode & Configuration.UI_MODE_NIGHT_MASK)
        {
            case Configuration.UI_MODE_NIGHT_YES:
            {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            }
            break;

            case Configuration.UI_MODE_NIGHT_NO:
            {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            }
            break;

            default:
            {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
            }
            break;
        }

        supportTools.setLocale(getBaseContext());

        recreate();

        super.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        boolean retcode = false;

        switch(keyCode)
        {
            case KeyEvent.KEYCODE_DPAD_LEFT:
            {
                View v = tabLayout.findFocus();
                boolean b = drawer.hasFocus();

                if(v != null)
                {
                    if(drawer.isDrawerOpen(GravityCompat.START) == false && ((TabLayout.TabView) v).getTab().getPosition() == 0)
                    {
                        drawer.openDrawer(GravityCompat.START);

                        navigationView.requestFocus();

                        retcode = true;
                    }
                    else
                        retcode = false;
                }
                else if(Build.VERSION.SDK_INT > Build.VERSION_CODES.O)
                {
                    if(drawer.isDrawerOpen(GravityCompat.START) == false)
                    {
                        drawer.openDrawer(GravityCompat.START);

                        navigationView.requestFocus();

                        retcode = true;
                    }
                    else
                        retcode = false;
                }
                else
                    retcode = false;
            }
            break;

            case KeyEvent.KEYCODE_DPAD_RIGHT:
            {
                if(drawer.isDrawerOpen(GravityCompat.START) == true)
                {
                    drawer.closeDrawer(GravityCompat.START);

                    tabLayout.selectTab(tabLayout.getTabAt(0));
                    tabLayout.requestFocus();

                    retcode = true;
                }
                else
                    retcode = false;
            }
            break;

            case KeyEvent.KEYCODE_BACK:
            {
                if(drawer.isDrawerOpen(GravityCompat.START))
                {
                    drawer.closeDrawer(GravityCompat.START);

                    tabLayout.selectTab(tabLayout.getTabAt(0));

                    tabLayout.requestFocus();
                }
                else
                    EddieApplication.mainActivity().finish();

                retcode = true;
            }
            break;

            default:
            {
                retcode = false;
            }
            break;
        }

        if(retcode == false)
            retcode = super.onKeyDown(keyCode, event);

        return retcode;
    }

    @Override
    public void onNewIntent(Intent newIntent)
    {
        super.onNewIntent(newIntent);

        if(newIntent != null)
        {
            switch(newIntent.getIntExtra(VPNService.INTENT_ACTION, -1))
            {
                case VPNService.MSG_PAUSE:
                {
                    vpnManager.pauseConnection();
                }
                break;

                case VPNService.MSG_RESUME:
                {
                    vpnManager.resumeConnection();
                }
                break;

                case VPNService.MSG_STOP:
                {
                    vpnManager.stopConnection();
                }
                break;

                default:
                {
                }
                break;
            }
        }
    }

    public void setVpnManager(VPNManager v)
    {
        vpnManager = v;

        if(quickConnectFragment != null)
            quickConnectFragment.setVpnManager(vpnManager);

        if(connectAirVPNServerFragment != null)
            connectAirVPNServerFragment.setVpnManager(vpnManager);

        if(connectVpnProfileFragment != null)
            connectVpnProfileFragment.setVpnManager(vpnManager);

        if(connectionInfoFragment != null)
            connectionInfoFragment.setVpnManager(vpnManager);
    }

    protected void navigationViewItemSelected(MenuItem menuItem)
    {
        drawer.closeDrawers();

        switch(menuItem.getItemId())
        {
            case R.id.nav_quick_connect:
            {
                if(viewPager != null)
                    viewPager.setCurrentItem(QUICK_CONNECT_FRAGMENT, true);
            }
            break;

            case R.id.nav_server_list:
            {
                if(viewPager != null)
                    viewPager.setCurrentItem(SERVER_LIST_FRAGMENT, true);
            }
            break;

            case R.id.nav_openvpn_connect:
            {
                if(viewPager != null)
                    viewPager.setCurrentItem(CONNECT_OPENVPN_PROFILE_FRAGMENT, true);
            }
            break;

            case R.id.nav_connection_info:
            {
                if(viewPager != null)
                    viewPager.setCurrentItem(CONNECTION_INFO_FRAGMENT, true);
            }
            break;

            case R.id.nav_log:
            {
                Intent logActivityIntent = new Intent(this, LogActivity.class);

                logActivityIntent.putExtra("ViewMode", LogActivity.ViewMode.DEFAULT);

                startActivity(logActivityIntent);
            }
            break;

            case R.id.nav_settings:
            {
                Intent settingsActivityIntent = new Intent(this, SettingsActivity.class);

                startActivityForResult(settingsActivityIntent, ACTIVITY_RESULT_SETTINGS);
            }
            break;

            case R.id.nav_logout:
            {
                airVPNUser.logout(this);
            }
            break;

            case R.id.nav_about:
            {
                Intent aboutActivityIntent = new Intent(this, WebViewerActivity.class);

                aboutActivityIntent.putExtra("icon", android.R.drawable.ic_menu_info_details);
                aboutActivityIntent.putExtra("title", getResources().getString(R.string.about_title));
                aboutActivityIntent.putExtra("uri", getResources().getString(R.string.local_about_page));
                aboutActivityIntent.putExtra("html", "");

                startActivity(aboutActivityIntent);
            }
            break;

            case R.id.nav_website:
            {
                Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.eddie_url)));

                try
                {
                    startActivity(webIntent);
                }
                catch(ActivityNotFoundException e)
                {
                    EddieLogger.error("MainActivity.navigationViewItemSelected(): No helper app found to open external link.");

                    supportTools.infoDialog(getString(R.string.cannot_open_web_browser), true);
                }
            }
            break;
        }
    }

    private void importOpenVPNProfile(Intent intent)
    {
        if(intent == null)
            return;

        Uri intentUri = intent.getData();

        if(intentUri == null)
            return;

        connectVpnProfileFragment.loadExternalProfile(intentUri);

        viewPager.setCurrentItem(CONNECT_OPENVPN_PROFILE_FRAGMENT, true);
    }

    private synchronized void updateConnectionStatus(VPN.Status vpnStatus, String message)
    {
        AirVPNServer server = null;
        String serverDescription = "";
        HashMap<String, String> profileInfo = null;

        if(vpnStatus == lastVpnStatus)
            return;

        profileInfo = vpnManager.vpn().getProfileInfo();

        if(profileInfo != null)
        {
            if(!profileInfo.get("server").isEmpty())
                serverDescription = vpnManager.vpn().getServerDescription();
            else
                serverDescription = "";
        }
        else
            serverDescription = "";

        switch(vpnStatus)
        {
            case CONNECTED:
            {
                supportTools.dismissConnectionProgressDialog();

                if(profileInfo != null && profileInfo.containsKey("server") == true)
                    supportTools.infoDialog(String.format(Locale.getDefault(), getResources().getString(R.string.connection_success), serverDescription, profileInfo.get("vpn_type"), NetworkStatusReceiver.getNetworkDescription()), false);

                if(vpnManager.vpn().getConnectionMode() == VPN.ConnectionMode.OPENVPN_PROFILE || vpnManager.vpn().getConnectionMode() == VPN.ConnectionMode.WIREGUARD_PROFILE || vpnManager.vpn().getConnectionMode() == VPN.ConnectionMode.BOOT_CONNECT)
                {
                    settingsManager.setSystemLastProfileIsConnected(true);

                    supportTools.saveLastConnectedProfile(profileInfo, vpnManager.vpn().getVpnProfile());
                }
                else if(vpnManager.vpn().getConnectionMode() == VPN.ConnectionMode.QUICK_CONNECT || vpnManager.vpn().getConnectionMode() == VPN.ConnectionMode.AIRVPN_SERVER)
                {
                    settingsManager.setSystemLastProfileIsConnected(true);

                    supportTools.saveLastConnectedProfile(profileInfo, vpnManager.vpn().getVpnProfile());
                }
                else
                {
                    settingsManager.setSystemLastProfileIsConnected(false);

                    supportTools.saveLastConnectedProfile(null, null);
                }

                viewPager.setCurrentItem(CONNECTION_INFO_FRAGMENT, true);

                menuItemLogout.setEnabled(false);

                userRequestedDisconnection = false;

                vpnManager.vpn().setPendingProfileInfo(null);
                vpnManager.vpn().setPendingVpnProfile("");
                vpnManager.vpn().setPendingProgressMessage("");

                onGoingConnection = false;
                reconnectionPending = false;
            }
            break;

            case NOT_CONNECTED:
            case CONNECTION_REVOKED_BY_SYSTEM:
            case CONNECTION_ERROR:
            case CONNECTION_CANCELED:
            {
                HashMap<String, String> pendingProfileInfo = vpnManager.vpn().getPendingProfileInfo();
                String pendingOpenVpnProfile = vpnManager.vpn().getPendingVpnProfile();
                VPN.ConnectionMode connectionMode = VPN.ConnectionMode.UNKNOWN;

                if(vpnStatus == VPN.Status.CONNECTION_REVOKED_BY_SYSTEM || vpnStatus == VPN.Status.CONNECTION_CANCELED)
                {
                    supportTools.dismissConnectionProgressDialog();

                    onGoingConnection = false;
                }
                else if(vpnStatus == VPN.Status.NOT_CONNECTED && vpnManager.vpn().getConnectionMode() != VPN.ConnectionMode.QUICK_CONNECT)
                    supportTools.dismissConnectionProgressDialog();

                settingsManager.setSystemLastProfileIsConnected(false);

                if(airVPNUser.isUserValid())
                    menuItemLogout.setEnabled(true);
                else
                    menuItemLogout.setEnabled(false);

                if(pendingProfileInfo != null)
                {
                    vpnManager.vpn().setType(VPN.Type.fromString(pendingProfileInfo.get("vpn_type")));

                    vpnManager.vpn().setProfileInfo(pendingProfileInfo);

                    try
                    {
                        int mode = Integer.parseInt(pendingProfileInfo.get("mode"));

                        connectionMode = VPN.ConnectionMode.fromValue(mode);
                    }
                    catch(NumberFormatException e)
                    {
                        connectionMode = VPN.ConnectionMode.UNKNOWN;
                    }

                    switch(connectionMode)
                    {
                        case AIRVPN_SERVER:
                        {
                            vpnManager.vpn().setConnectionMode(VPN.ConnectionMode.AIRVPN_SERVER);

                            vpnManager.vpn().setConnectionModeDescription(getResources().getString(R.string.conn_type_airvpn_server));

                            vpnManager.vpn().setUserProfileDescription(airVPNUser.getCurrentKey());

                            vpnManager.vpn().setUserName(airVPNUser.getUserName());
                        }
                        break;

                        case OPENVPN_PROFILE:
                        {
                            vpnManager.vpn().setConnectionMode(VPN.ConnectionMode.OPENVPN_PROFILE);

                            vpnManager.vpn().setConnectionModeDescription(getResources().getString(R.string.conn_type_openvpn_profile));

                            vpnManager.vpn().setUserProfileDescription("");

                            vpnManager.vpn().setUserName("");
                        }
                        break;

                        case WIREGUARD_PROFILE:
                        {
                            vpnManager.vpn().setConnectionMode(VPN.ConnectionMode.WIREGUARD_PROFILE);

                            vpnManager.vpn().setConnectionModeDescription(getResources().getString(R.string.conn_type_wireguard_profile));

                            vpnManager.vpn().setUserProfileDescription("");

                            vpnManager.vpn().setUserName("");
                        }
                        break;

                        default:
                        {
                            return;
                        }
                    }

                    supportTools.showConnectionProgressDialog(vpnManager.vpn().getPendingProgressMessage());

                    startConnection(pendingOpenVpnProfile);

                    vpnManager.vpn().setPendingProfileInfo(null);
                    vpnManager.vpn().setPendingVpnProfile("");
                    vpnManager.vpn().setPendingProgressMessage("");

                    onGoingConnection = true;
                }
                else
                {
                    switch(vpnStatus)
                    {
                        case NOT_CONNECTED:
                        {
                            if(onGoingConnection == false)
                                supportTools.infoDialog(String.format(Locale.getDefault(), getResources().getString(R.string.connection_disconnected), serverDescription, System.getProperty("line.separator") + vpnManager.vpn().getFormattedStats()), false);
                        }
                        break;

                        case CONNECTION_REVOKED_BY_SYSTEM:
                        {
                            supportTools.infoDialog(R.string.connection_revoked, false);
                        }
                        break;

                        case CONNECTION_ERROR:
                        {
                        }
                        break;

                        case CONNECTION_CANCELED:
                        {
                            supportTools.infoDialog(R.string.connection_canceled, true);
                        }
                        break;

                        default:
                        {
                            supportTools.infoDialog(String.format(Locale.getDefault(), getResources().getString(R.string.connection_disconnected), serverDescription, System.getProperty("line.separator")+ vpnManager.vpn().getFormattedStats()), false);
                        }
                        break;
                    }

                    if(vpnManager.vpn().getConnectionMode() != VPN.ConnectionMode.QUICK_CONNECT)
                        onGoingConnection = false;
                }
            }
            break;

            case CONNECTING:
            {
                onGoingConnection = true;
            }
            break;

            case DISCONNECTING:
            {
                userRequestedDisconnection = true;
            }
            break;

            case PAUSED_BY_USER:
            case PAUSED_BY_SYSTEM:
            {
                supportTools.infoDialog(getResources().getString(R.string.connection_paused), false);

                settingsManager.setSystemLastProfileIsConnected(true);
            }
            break;

            case LOCKED:
            {
                settingsManager.setSystemLastProfileIsConnected(true);
            }
            break;

            default:
            {
                settingsManager.setSystemLastProfileIsConnected(false);
            }
            break;
        }

        lastVpnStatus = vpnStatus;
    }

    private void startConnection(String openVPNProfile)
    {
        if(vpnManager == null)
        {
            EddieLogger.error("MainActivity.startConnection(): vpnManager is null");

            return;
        }

        if(openVPNProfile.equals(""))
            return;

        vpnManager.clearProfile();

        vpnManager.setProfile(openVPNProfile);

        String profileString = settingsManager.getOvpn3CustomDirectives().trim();

        if(profileString.length() > 0)
            vpnManager.addProfileString(profileString);

        vpnManager.startConnection();
    }

    private void vpnAuthFailed(VPNEvent oe)
    {
        supportTools.waitInfoDialog(R.string.conn_auth_failed);

        try
        {
            vpnManager.stopConnection();
        }
        catch(Exception e)
        {
            EddieLogger.error("MainActivity.vpnAuthFailed() exception: %s", e.getMessage());
        }

        supportTools.dismissConnectionProgressDialog();
    }

    private void startAirVPNManifestRefresh()
    {
        if(!AirVPNUser.isUserValid())
            return;

        int updateInterval = 0;

        if(airVPNManifest != null)
            airVPNManifest = EddieApplication.airVPNManifest();

        if(timerAirVPNManifestRefresh != null)
            timerAirVPNManifestRefresh.cancel();

        timerAirVPNManifestRefresh = new Timer();

        updateInterval = airVPNManifest.getNextUpdateIntervalMinutes() * 60000; // Minutes to milliseconds

        timerAirVPNManifestRefresh.schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                airVPNManifest.refreshManifestFromAirVPN();
            }
        }, updateInterval, updateInterval);
    }

    public void stopAirVPNManifestRefresh()
    {
        if(timerAirVPNManifestRefresh != null)
            timerAirVPNManifestRefresh.cancel();

        timerAirVPNManifestRefresh = null;
    }

    // Eddie events

    public void onVpnConnectionStatsChanged(final VPNConnectionStats connectionStats)
    {
    }

    public void onVpnStatusChanged(final VPN.Status status, final String message)
    {
        Bundle vpnState = null;

        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                updateConnectionStatus(status, message);
            }
        };

        SupportTools.runOnUiActivity(this, runnable);
    }

    public void onVpnAuthFailed(final VPNEvent oe)
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                vpnAuthFailed(oe);
            }
        };

        SupportTools.runOnUiActivity(this, runnable);
    }

    public void onVpnError(final VPNEvent oe)
    {
        if(onGoingConnection == true)
        {
            supportTools.dismissConnectionProgressDialog();

            supportTools.infoDialog(getString(R.string.connection_error), true);

            vpnManager.stopConnection();
        }
    }

    public void onVpnReconnect(final VPNEvent oe)
    {
    }

    public void onMasterPasswordChanged()
    {
    }

    public void onAirVPNLogin()
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                String logoutMenuItemTitle = getResources().getString(R.string.menu_logout_cap);

                logoutMenuItemTitle += " " + airVPNUser.getUserName();

                menuItemLogout.setEnabled(true);

                menuItemLogout.setTitle(logoutMenuItemTitle);
            }
        };

        SupportTools.runOnUiActivity(this, runnable);

        if(AirVPNUser.isUserValid())
            startAirVPNManifestRefresh();
    }

    public void onAirVPNLoginFailed(String message)
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                menuItemLogout.setEnabled(false);

                menuItemLogout.setTitle(getResources().getString(R.string.menu_logout_cap));
            }
        };

        SupportTools.runOnUiActivity(this, runnable);
    }

    public void onAirVPNLogout()
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                menuItemLogout.setEnabled(false);

                menuItemLogout.setTitle(getResources().getString(R.string.menu_logout_cap));
            }
        };

        SupportTools.runOnUiActivity(this, runnable);

        stopAirVPNManifestRefresh();
    }

    public void onAirVPNUserDataChanged()
    {
    }

    public void onAirVPNUserProfileReceived(Document document)
    {
    }

    public void onAirVPNUserProfileDownloadError()
    {
    }

    public void onAirVPNUserProfileChanged()
    {
    }

    public void onAirVPNManifestReceived(Document document)
    {
    }

    public void onAirVPNManifestDownloadError()
    {
    }

    public void onAirVPNManifestChanged()
    {
        if(airVPNManifest == null)
            return;

        ArrayList<AirVPNManifest.Message> message = airVPNManifest.getMessages();
        ArrayList<Integer> messageId = new ArrayList<Integer>();
        AirVPNManifest.Message m = null;
        String htmlDocument = "";
        int nMessage = 0;
        long utcTimestamp = System.currentTimeMillis() / 1000;

        if(message != null && message.isEmpty() == false)
        {
            htmlDocument = "<html><body><br>" + System.getProperty("line.separator");

            for(int i = 0; i < message.size(); i++)
            {
                m = message.get(i);

                if(!m.isShown() && utcTimestamp >= m.getFromUTCTimeTS() && utcTimestamp <= m.getToUTCTimeTS() && m.isToBeShownAgain() == true)
                {
                    if(i > 0)
                        htmlDocument += "<br><br><hr><br><br>" + System.getProperty("line.separator");

                    htmlDocument += "<div>" + System.getProperty("line.separator");

                    if(!m.getHtml().isEmpty())
                        htmlDocument += m.getHtml();
                    else
                    {
                        htmlDocument += "<font style='font-size:20px'>";
                        htmlDocument += m.getText() + "<br><br>";

                        htmlDocument += "<a href='" + m.getUrl() + "'>" + m.getLink() + "</a></font><br>";
                    }

                    htmlDocument += "</div>" + System.getProperty("line.separator");

                    airVPNManifest.setMessageShown(i);

                    messageId.add(i);

                    nMessage++;
                }
            }

            htmlDocument += "</body></html>" + System.getProperty("line.separator");

            if(nMessage > 0)
            {
                EddieLogger.info("Displayed %d AirVPN manifest messages", nMessage);

                Intent messageIntent = new Intent(this, WebViewerActivity.class);

                messageIntent.putExtra("icon", android.R.drawable.ic_menu_info_details);
                messageIntent.putExtra("title", getResources().getString(R.string.eddie));
                messageIntent.putExtra("uri", "");
                messageIntent.putExtra("html", htmlDocument);
                messageIntent.putExtra("manifestMessageID", messageId);

                startActivity(messageIntent);
            }
        }
    }

    public void onAirVPNIgnoredManifestDocumentRequest()
    {
    }

    public void onAirVPNIgnoredUserDocumentRequest()
    {
    }

    public void onAirVPNRequestError(String message)
    {
    }

    public void onCancelConnection()
    {
    }

    public void onSaveState()
    {
    }

    public void onRestoreState(Bundle bundle)
    {
    }

    // NetworkStatusReceiver

    public void onNetworkStatusNotAvailable()
    {
    }

    public void onNetworkStatusConnected()
    {
    }

    public void onNetworkStatusIsConnecting()
    {
    }

    public void onNetworkStatusIsDisconnecting()
    {
    }

    public void onNetworkStatusSuspended()
    {
    }

    public void onNetworkStatusNotConnected()
    {
    }

    public void onNetworkTypeChanged()
    {
    }
}
