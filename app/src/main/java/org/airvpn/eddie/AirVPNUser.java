// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 10 October 2018 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import androidx.appcompat.app.AlertDialog;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class AirVPNUser implements NetworkStatusListener, EddieEventListener
{
    public static final String AIRVPN_USER_DATA_FILE_NAME = "AirVPNUser.dat";
    public static final String AIRVPN_USER_PROFILE_FILE_NAME = "AirVPNUserProfile.dat";
    private final String AIRVPN_USER_DATA = "AirVPNUser";
    private final String AIRVPN_USER_NAME_ITEM = "UserName";
    private final String AIRVPN_USER_PASSWORD_ITEM = "Password";
    private final String AIRVPN_USER_CURRENT_KEY = "CurrentUserKey";

    public enum CheckPasswordMode
    {
        ASK_PASSWORD,
        USE_CURRENT_PASSWORD
    }

    public enum UserProfileType
    {
        NOT_SET,
        PROCESSING,
        STORED,
        FROM_SERVER
    }

    public enum UserCountryStatus
    {
        NOT_SET,
        PROCESSING,
        SET
    }

    public class UserKey
    {
        private String name;
        private String certificate;
        private String privateKey;
        private String wireguardPrivateKey;
        private String wireguardPresharedKey;
        private String wireguardIPv4;
        private String wireguardIPv6;
        private String wireguardDnsIPv4;
        private String wireguardDnsIPv6;

        public String getName()
        {
            return name;
        }

        public void setName(String n)
        {
            name = n;
        }

        public String getCertificate()
        {
            return certificate;
        }

        public void setCertificate(String c)
        {
            certificate = c;
        }

        public String getPrivateKey()
        {
            return privateKey;
        }

        public void setPrivateKey(String pk)
        {
            privateKey = pk;
        }

        public String getWireGuardPrivateKey()
        {
            return wireguardPrivateKey;
        }

        public void setWireGuardPrivateKey(String pk)
        {
            wireguardPrivateKey = pk;
        }

        public String getWireGuardPresharedKey()
        {
            return wireguardPresharedKey;
        }

        public void setWireGuardPresharedKey(String s)
        {
            wireguardPresharedKey = s;
        }

        public String getWireGuardIPv4()
        {
            return wireguardIPv4;
        }

        public void setWireGuardIPv4(String s)
        {
            wireguardIPv4 = s;
        }

        public String getWireGuardIPv6()
        {
            return wireguardIPv6;
        }

        public void setWireGuardIPv6(String s)
        {
            wireguardIPv6 = s;
        }

        public String getWireGuardDnsIPv4()
        {
            return wireguardDnsIPv4;
        }

        public void setWireGuardDnsIPv4(String s)
        {
            wireguardDnsIPv4 = s;
        }

        public String getWireGuardDnsIPv6()
        {
            return wireguardDnsIPv6;
        }

        public void setWireGuardDnsIPv6(String s)
        {
            wireguardDnsIPv6 = s;
        }
    }

    private static Document airVPNUserProfileDocument = null;

    private SupportTools supportTools = null;
    private SettingsManager settingsManager = null;
    private static NetworkStatusReceiver networkStatusReceiver = null;
    private static EddieEvent eddieEvent = null;

    private DocumentBuilderFactory documentBuilderFactory = null;
    private DocumentBuilder documentBuilder = null;
    private static Document userAirVPNLoginDocument = null;
    private InetAddress inetAddress = null;

    private Button btnOk = null;
    private Button btnCancel = null;
    private EditText edtUserName = null;
    private EditText edtUserPassword = null;
    private CheckBox chkRememberMe = null;
    boolean loginResult = false;

    AlertDialog.Builder dialogBuilder = null;
    AlertDialog passwordDialog = null;
    AlertDialog loginDialog = null;
    private EditText edtKey = null;
    String editValue = "", countryHostname = "";

    private static String masterPassword = "";
    private static String airVPNUserName = "";
    private static String airVPNUserPassword = "";
    private static String airVPNUserCurrentKey = "";

    private static UserProfileType userProfileType = UserProfileType.NOT_SET;
    private static UserCountryStatus userCountryStatus = UserCountryStatus.NOT_SET;

    private static boolean userIsValid;
    private static boolean userLoginFailed;
    private static String expirationDate;
    private static Date airVPNSubscriptionExpirationDate;
    private static int daysToExpiration;
    private static String certificateAuthorityCertificate;
    private static String tlsAuthKey;
    private static String tlsCryptKey;
    private static String sshKey;
    private static String sshPpk;
    private static String sslCertificate;
    private static String wireguardPublicKey;
    private static HashMap<String, UserKey> userKey;

    private static String userCountry = "";
    private static float userLatitude = 0;
    private static float userLongitude = 0;

    public AirVPNUser()
    {
        supportTools = EddieApplication.supportTools();
        settingsManager = EddieApplication.settingsManager();

        if(networkStatusReceiver == null)
        {
            networkStatusReceiver = EddieApplication.networkStatusReceiver();

            networkStatusReceiver.subscribeListener(this);
        }

        if(eddieEvent == null)
        {
            eddieEvent = EddieApplication.eddieEvent();

            eddieEvent.subscribeListener(this);
        }

        if(userCountryStatus == UserCountryStatus.NOT_SET)
        {
            if(supportTools.isNetworkConnectionActive() == true && (EddieApplication.vpnManager().vpn().getConnectionStatus() == VPN.Status.NOT_CONNECTED))
                new getUserLocation().execute();
        }

        if(userAirVPNLoginDocument == null)
            initializeUserData();

        if(airVPNUserProfileDocument == null)
            loadUserProfile();
    }

    @Override
    protected void finalize() throws Throwable
    {
        try
        {
            if(networkStatusReceiver != null)
                networkStatusReceiver.unsubscribeListener(this);

            if(eddieEvent != null)
                eddieEvent.unsubscribeListener(this);
        }
        catch(Exception e)
        {
            EddieLogger.warning("AirVPNUser.finalize() Exception: %s", e);
        }
        finally
        {
            try
            {
                super.finalize();
            }
            catch(Exception e)
            {
                EddieLogger.error("AirVPNUser.finalize() Exception: %s", e);
            }
        }
    }

    public static boolean isDataEncrypted()
    {
        boolean result = false;

        try
        {
            result = SupportTools.isFileXML(AIRVPN_USER_DATA_FILE_NAME);
        }
        catch(FileNotFoundException e)
        {
            return false;
        }

        return !result;
    }

    public static boolean isProfileEncrypted()
    {
        boolean result = false;

        try
        {
            result = SupportTools.isFileXML(AIRVPN_USER_PROFILE_FILE_NAME);
        }
        catch(FileNotFoundException e)
        {
            return false;
        }

        return !result;
    }

    private void initializeUserData()
    {
        try
        {
            documentBuilderFactory = DocumentBuilderFactory.newInstance();

            documentBuilder = documentBuilderFactory.newDocumentBuilder();
        }
        catch(ParserConfigurationException e)
        {
            userAirVPNLoginDocument = null;

            return;
        }

        userAirVPNLoginDocument = documentBuilder.newDocument();

        if(userAirVPNLoginDocument != null)
        {
            Element rootElement = userAirVPNLoginDocument.createElement(AIRVPN_USER_DATA);
            userAirVPNLoginDocument.appendChild(rootElement);

            Element user = userAirVPNLoginDocument.createElement(AIRVPN_USER_NAME_ITEM);
            user.setNodeValue("");
            rootElement.appendChild(user);

            Element password = userAirVPNLoginDocument.createElement(AIRVPN_USER_PASSWORD_ITEM);
            password.setNodeValue("");
            rootElement.appendChild(password);

            Element currentKey = userAirVPNLoginDocument.createElement(AIRVPN_USER_CURRENT_KEY);
            currentKey.setNodeValue("");
            rootElement.appendChild(currentKey);
        }

        airVPNUserName = "";
        airVPNUserPassword = "";
        airVPNUserCurrentKey = "";

        initializeUserProfileData();
    }

    private void initializeUserProfileData()
    {
        userIsValid = false;
        userLoginFailed = false;
        expirationDate = "";
        daysToExpiration = 0;
        certificateAuthorityCertificate = "";
        tlsAuthKey = "";
        tlsCryptKey = "";
        sshKey = "";
        sshPpk = "";
        sslCertificate = "";
        wireguardPublicKey = "";
        userKey = null;
    }

    public boolean loadUserCredentials()
    {
        File userDataFile;

        if(isDataEncrypted() == true)
            return false;

        userDataFile = new File(EddieApplication.context().getFilesDir(), AIRVPN_USER_DATA_FILE_NAME);

        if(!userDataFile.exists() || userDataFile.isDirectory())
            return false;

        userAirVPNLoginDocument = supportTools.loadXmlFileToDocument(AIRVPN_USER_DATA_FILE_NAME);

        if(userAirVPNLoginDocument == null)
            return false;

        airVPNUserName = getUserLoginDocumentItem(AIRVPN_USER_NAME_ITEM);

        airVPNUserPassword = getUserLoginDocumentItem(AIRVPN_USER_PASSWORD_ITEM);

        airVPNUserCurrentKey = getUserLoginDocumentItem(AIRVPN_USER_CURRENT_KEY);

        return true;
    }

    public void saveUserCredentials(Context context)
    {
        setUserLoginDocumentItem(context, AIRVPN_USER_NAME_ITEM, airVPNUserName);
        setUserLoginDocumentItem(context, AIRVPN_USER_PASSWORD_ITEM, airVPNUserPassword);
        setUserLoginDocumentItem(context, AIRVPN_USER_CURRENT_KEY, airVPNUserCurrentKey);
    }

    public void forgetAirVPNCredentials(Context context)
    {
        setUserLoginDocumentItem(context, AIRVPN_USER_NAME_ITEM, "");
        setUserLoginDocumentItem(context, AIRVPN_USER_PASSWORD_ITEM, "");
        setUserLoginDocumentItem(context, AIRVPN_USER_CURRENT_KEY, "");
    }

    private void saveUserAirLoginDocument(Context context)
    {
        if(userAirVPNLoginDocument == null)
            return;

        if(!masterPassword.isEmpty() && settingsManager.isMasterPasswordEnabled() == true)
        {
            if(!supportTools.encryptXmlDocumentToFile(userAirVPNLoginDocument, masterPassword, AIRVPN_USER_DATA_FILE_NAME))
                supportTools.infoDialog(context, R.string.login_user_data_save_error, true);
        }
        else if(settingsManager.isMasterPasswordEnabled() == false)
        {
            if(!supportTools.saveXmlDocumentToFile(userAirVPNLoginDocument, AIRVPN_USER_DATA_FILE_NAME))
                supportTools.infoDialog(context, R.string.login_user_data_save_error, true);
        }
    }

    public static String masterPassword()
    {
        return masterPassword;
    }

    public static String getUserCountry()
    {
        return userCountry;
    }

    public static float getUserLatitude()
    {
        return userLatitude;
    }

    public static float getUserLongitude()
    {
        return userLongitude;
    }

    public static boolean isUserValid()
    {
        return userIsValid;
    }

    public static boolean isLoginValid()
    {
        return !userLoginFailed;
    }

    public static Date getExpirationDate()
    {
        return airVPNSubscriptionExpirationDate;
    }

    public static void setExpirationDate(Date e)
    {
        airVPNSubscriptionExpirationDate = e;
    }

    public static int getDaysToExpiration()
    {
        return daysToExpiration;
    }

    public static void setDaysToExpiration(int d)
    {
        daysToExpiration = d;
    }

    public String getExpirationText()
    {
        return String.format(Locale.getDefault(), EddieApplication.applicationContext().getString(R.string.airvpn_subscription_status), daysToExpiration, DateFormat.getDateInstance(DateFormat.LONG, Locale.getDefault()).format(airVPNSubscriptionExpirationDate));
    }

    public static String getCertificateAuthorityCertificate()
    {
        return certificateAuthorityCertificate;
    }

    public static void setCertificateAuthorityCertificate(String c)
    {
        certificateAuthorityCertificate = c;
    }

    public static String getTlsAuthKey()
    {
        return tlsAuthKey;
    }

    public static void setTlsAuthKey(String t)
    {
        tlsAuthKey = t;
    }

    public static String getTlsCryptKey()
    {
        return tlsCryptKey;
    }

    public static void setTlsCryptKey(String t)
    {
        tlsCryptKey = t;
    }

    public static String getSshKey()
    {
        return sshKey;
    }

    public static void setSshKey(String s)
    {
        sshKey = s;
    }

    public static String getSshPpk()
    {
        return sshPpk;
    }

    public static void setSshPpk(String s)
    {
        sshPpk = s;
    }

    public static String getSslCertificate()
    {
        return sslCertificate;
    }

    public static void setSslCertificate(String s)
    {
        sslCertificate = s;
    }

    public static String getWireGuardPublicKey()
    {
        return wireguardPublicKey;
    }

    public static void setWireGuardPublicKey(String s)
    {
        wireguardPublicKey = s;
    }

    public static HashMap<String, UserKey> getUserKeys()
    {
        return userKey;
    }

    public static void setUserKeys(HashMap<String, UserKey> u)
    {
        userKey = u;
    }

    public UserKey getUserKey(String name)
    {
        UserKey key = null;

        if(userKey == null)
            return null;

        if(userKey.containsKey(name))
            key = userKey.get(name);

        return key;
    }

    public void setUserKey(String name, UserKey u)
    {
        if(userKey == null)
            userKey = new HashMap<String, UserKey>();

        userKey.put(name, u);
    }

    public void addUserKey(String name, UserKey key)
    {
        if(userKey == null)
            userKey = new HashMap<String, UserKey>();

        if(name.isEmpty() || key == null)
            return;

        userKey.put(name, key);
    }

    public ArrayList<String> getUserKeyNames()
    {
        ArrayList<String> keyNames = new ArrayList<String>();

        if(userKey != null)
        {
            for(String key : userKey.keySet())
                keyNames.add(key);
        }

        return keyNames;
    }

    public static UserProfileType getUserProfileType()
    {
        return userProfileType;
    }

    public boolean checkMasterPassword(Context context, CheckPasswordMode checkPasswordMode)
    {
        boolean result = false;
        int pwHashCode = settingsManager.getAirVPNMasterPasswordHashCode();

        if(settingsManager.isMasterPasswordEnabled() == false)
            return true;

        if(pwHashCode == -1)
        {
            setMasterPassword(context);

            if(!masterPassword.isEmpty())
                return true;
            else
                return false;
        }

        if(!masterPassword.isEmpty() && checkPasswordMode == CheckPasswordMode.USE_CURRENT_PASSWORD)
            result = true;
        else
        {
            masterPassword = getMasterPassword(context, context.getResources().getString(R.string.login_enter_master_password));

            if(masterPassword.isEmpty())
                result = false;
            else
            {
                result = true;

                if(eddieEvent != null)
                    eddieEvent.onMasterPasswordChanged();
            }
        }

        return result;
    }

    public void setMasterPassword(Context context)
    {
        int pwHashCode = settingsManager.getAirVPNMasterPasswordHashCode();
        String password = "", pwd1 = "x", pwd2 = "";
        boolean valid = false;

        if(masterPassword.isEmpty() && pwHashCode != -1)
        {
            password = getMasterPassword(context, context.getResources().getString(R.string.login_enter_master_password));

            if(password.isEmpty())
                return;

            masterPassword = password;

            if(eddieEvent != null)
                eddieEvent.onMasterPasswordChanged();
        }

        valid = false;

        while(!valid)
        {
            pwd1 = getPasswordDialog(context, context.getResources().getString(R.string.login_enter_new_master_password));

            if(pwd1.isEmpty())
            {
                supportTools.waitInfoDialog(context, R.string.login_new_master_password_not_set);

                return;
            }

            pwd2 = getPasswordDialog(context, context.getResources().getString(R.string.login_confirm_new_master_password));

            if(pwd2.isEmpty())
            {
                supportTools.waitInfoDialog(context, R.string.login_new_master_password_not_set);

                return;
            }

            if(!pwd1.equals(pwd2))
                supportTools.waitInfoDialog(context, R.string.login_master_password_do_not_match);
            else
            {
                masterPassword = pwd1;

                settingsManager.setAirVPNMasterPasswordHashCode(masterPassword.hashCode());

                supportTools.waitInfoDialog(context, R.string.login_new_master_password_set);

                valid = true;

                supportTools.encryptXmlDocumentToFile(userAirVPNLoginDocument, masterPassword, AIRVPN_USER_DATA_FILE_NAME);

                if(eddieEvent != null)
                    eddieEvent.onMasterPasswordChanged();
            }
        }
    }

    public String getMasterPassword(Context context, String dialogTitle)
    {
        String password = "";
        boolean valid = false;
        int pwHashCode = settingsManager.getAirVPNMasterPasswordHashCode();

        while(!valid)
        {
            password = getPasswordDialog(context, dialogTitle);

            if(!password.isEmpty())
            {
                if(password.hashCode() != pwHashCode)
                    supportTools.waitInfoDialog(context, R.string.login_incorrect_master_password);
                else
                {
                    valid = true;

                    userAirVPNLoginDocument = supportTools.decryptFileToXmlDocument(AIRVPN_USER_DATA_FILE_NAME, password);

                    if(userAirVPNLoginDocument != null)
                    {
                        airVPNUserName = getUserLoginDocumentItem(AIRVPN_USER_NAME_ITEM);

                        airVPNUserPassword = getUserLoginDocumentItem(AIRVPN_USER_PASSWORD_ITEM);

                        airVPNUserCurrentKey = getUserLoginDocumentItem(AIRVPN_USER_CURRENT_KEY);

                        if(airVPNUserCurrentKey.isEmpty() == true)
                            airVPNUserCurrentKey = getCurrentKey();

                        if(airVPNUserName.isEmpty() == false && airVPNUserPassword.isEmpty() == false)
                        {
                            supportTools.showProgressDialog(context, String.format(EddieApplication.context().getResources().getString(R.string.login_to_airvpn), airVPNUserName));

                            loadUserProfile();
                        }
                    }
                }
            }
            else
                valid = true;
        }

        return password;
    }

    private String getPasswordDialog(Context context, String dialogTitle)
    {
        TextView txtDialogTitle = null;

        btnOk = null;
        btnCancel = null;

        final Handler dialogHandler = new Handler(new Handler.Callback()
        {
            @Override
            public boolean handleMessage(Message mesg)
            {
                throw new RuntimeException();
            }
        });

        dialogBuilder = new AlertDialog.Builder(context);

        View content = LayoutInflater.from(context).inflate(R.layout.edit_option_dialog, null);

        txtDialogTitle = (TextView)content.findViewById(R.id.title);
        edtKey = (EditText)content.findViewById(R.id.key);

        edtKey.setText("");

        edtKey.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);

        btnOk = (Button)content.findViewById(R.id.btn_ok);
        btnCancel = (Button)content.findViewById(R.id.btn_cancel);

        SupportTools.setButtonStatus(btnOk, SupportTools.ShowMode.DISABLED);

        SupportTools.setButtonStatus(btnCancel, SupportTools.ShowMode.ENABLED);

        btnOk.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                editValue = edtKey.getText().toString();

                passwordDialog.dismiss();

                dialogHandler.sendMessage(dialogHandler.obtainMessage());
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                editValue = "";

                passwordDialog.dismiss();

                dialogHandler.sendMessage(dialogHandler.obtainMessage());
            }
        });

        edtKey.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void afterTextChanged(Editable s)
            {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                SupportTools.setButtonStatus(btnOk, edtKey.getText().toString().isEmpty() == true ? SupportTools.ShowMode.DISABLED : SupportTools.ShowMode.ENABLED);
            }
        });

        txtDialogTitle.setText(dialogTitle);

        dialogBuilder.setTitle("");
        dialogBuilder.setView(content);

        passwordDialog = dialogBuilder.create();
        passwordDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        try
        {
            passwordDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
        catch(NullPointerException e)
        {
        }

        if(context instanceof Activity)
        {
            if(((Activity)context).isFinishing())
                return "";
        }

        passwordDialog.show();

        try
        {
            Looper.loop();
        }
        catch(RuntimeException e)
        {
        }

        return editValue;
    }

    public boolean validateUserLogin(Context context)
    {
        if(userIsValid)
            return true;

        if(masterPassword.isEmpty() && settingsManager.isMasterPasswordEnabled() == true)
        {
            int pwHashCode = settingsManager.getAirVPNMasterPasswordHashCode();

            if(pwHashCode == -1)
                setMasterPassword(context);
            else
                masterPassword = getMasterPassword(context, context.getResources().getString(R.string.login_enter_master_password));

            if(masterPassword.isEmpty())
                return false;
            else
            {
                if(eddieEvent != null)
                    eddieEvent.onMasterPasswordChanged();
            }
        }
        else if(settingsManager.isMasterPasswordEnabled() == false)
            userAirVPNLoginDocument = supportTools.loadXmlFileToDocument(AIRVPN_USER_DATA_FILE_NAME);

        if(userAirVPNLoginDocument != null && settingsManager.isAirVPNRememberMe() && userLoginFailed == false)
        {
            airVPNUserName = getUserLoginDocumentItem(AIRVPN_USER_NAME_ITEM);

            airVPNUserPassword = getUserLoginDocumentItem(AIRVPN_USER_PASSWORD_ITEM);

            airVPNUserCurrentKey = getUserLoginDocumentItem(AIRVPN_USER_CURRENT_KEY);
        }

        if((airVPNUserName.isEmpty() && airVPNUserPassword.isEmpty()) || userLoginFailed == true)
        {
            if(showLoginDialog(context))
            {
                supportTools.showProgressDialog(context, String.format(EddieApplication.context().getResources().getString(R.string.login_to_airvpn), airVPNUserName));

                loadUserProfile();
            }
        }
        else
        {
            supportTools.showProgressDialog(context, String.format(EddieApplication.context().getResources().getString(R.string.login_to_airvpn), airVPNUserName));

            loadUserProfile();
        }

        return userIsValid;
    }

    public boolean areCredendialsUsable()
    {
        if(userIsValid == true)
            return true;

        if(isDataEncrypted() == true)
            return false;

        if(loadUserCredentials() == true)
            loadUserProfile();

        return (airVPNUserName.isEmpty() == false && airVPNUserPassword.isEmpty() == false);
    }

    public boolean logout(Context context)
    {
        userIsValid = false;
        userLoginFailed = false;

        EddieLogger.info(String.format("User %s Logged out from AirVPN", airVPNUserName.replace("%","%%")));

        airVPNUserName = "";
        airVPNUserPassword = "";
        airVPNUserCurrentKey = "";

        forgetAirVPNCredentials(context);

        eddieEvent.onAirVPNLogout();

        return true;
    }

    public static String getUserName()
    {
        return airVPNUserName;
    }

    public void setUserName(Context context, String u)
    {
        loadUserCredentials();

        airVPNUserName = u;

        if(settingsManager.isAirVPNRememberMe())
            saveUserCredentials(context);

        loadUserProfile();
    }

    public static String getUserPassword()
    {
        return airVPNUserPassword;
    }

    public void setUserPassword(Context context, String p)
    {
        loadUserCredentials();

        airVPNUserPassword = p;

        if(settingsManager.isAirVPNRememberMe())
            saveUserCredentials(context);

        loadUserProfile();
    }

    public static String getCurrentKey()
    {
        if(airVPNUserCurrentKey.isEmpty() && userKey != null && userKey.isEmpty() == false)
        {
            String firstKey = userKey.keySet().toArray()[0].toString();

            airVPNUserCurrentKey = userKey.get(firstKey).getName();
        }

        return airVPNUserCurrentKey;
    }

    public void setCurrentKey(Context context, String p)
    {
        airVPNUserCurrentKey = p;

        setUserLoginDocumentItem(context, AIRVPN_USER_CURRENT_KEY, p);
    }

    public void reloadUserLocation()
    {
        if(userCountryStatus != UserCountryStatus.PROCESSING)
            new getUserLocation().execute();
    }

    private String getUserLoginDocumentItem(String item)
    {
        if(userAirVPNLoginDocument == null)
            return "";

        return supportTools.getXmlItemValue(userAirVPNLoginDocument, item);
    }

    private void setUserLoginDocumentItem(Context context, String item, String value)
    {
        if(userAirVPNLoginDocument == null)
            return;

        NodeList itemList = userAirVPNLoginDocument.getElementsByTagName(item);

        if(itemList != null && itemList.getLength() > 0)
            itemList.item(0).setTextContent(value);
        else
        {
            NodeList nodeList = userAirVPNLoginDocument.getElementsByTagName(AIRVPN_USER_DATA);

            Element element = userAirVPNLoginDocument.createElement(item);

            if(nodeList != null && element != null)
            {
                element.setNodeValue(value);

                nodeList.item(0).appendChild(element);
            }
        }

        saveUserAirLoginDocument(context);
    }

    private class getUserLocation extends AsyncTask<Void, Boolean, Boolean>
    {
        @Override
        protected void onPreExecute()
        {
        }

        @Override
        protected Boolean doInBackground(Void... p)
        {
            BufferedReader bufferedReader = null;
            StringBuffer httpData = null;
            Document userData = null;

            if(userCountryStatus == UserCountryStatus.PROCESSING)
                return true;

            if(settingsManager.getAirVPNCurrentLocalCountry().equals(SettingsManager.AIRVPN_CURRENT_LOCAL_COUNTRY_DEFAULT) == false)
            {
                userCountry = settingsManager.getAirVPNCurrentLocalCountry();

                EddieLogger.info("Current local country is set to %s (%s)", userCountry, CountryContinent.getCountryName(userCountry));

                return true;
            }

            try
            {
                userCountryStatus = UserCountryStatus.PROCESSING;

                EddieLogger.info("Requesting user IP and country to AirVPN ipleak.net via secure connection");

                URL url = new URL("https://ipleak.net/xml/");

                HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.setConnectTimeout(SupportTools.HTTP_CONNECTION_TIMEOUT);
                httpURLConnection.setReadTimeout(SupportTools.HTTP_READ_TIMEOUT);

                if(httpURLConnection.getResponseCode() != HttpURLConnection.HTTP_OK)
                    return false;

                bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                httpData = new StringBuffer();

                String line = "";

                while ((line = bufferedReader.readLine()) != null)
                    httpData.append(line);

                userData = supportTools.stringToXmlDocument(httpData.toString());

                if(userData != null)
                {
                    userCountry = supportTools.getXmlItemValue(userData, "country_code");

                    try
                    {
                        userLatitude = Float.parseFloat(supportTools.getXmlItemValue(userData, "latitude"));
                    }
                    catch(NumberFormatException e)
                    {
                        userLatitude = 0;
                    }

                    try
                    {
                        userLongitude = Float.parseFloat(supportTools.getXmlItemValue(userData, "longitude"));
                    }
                    catch(NumberFormatException e)
                    {
                        userLongitude = 0;
                    }

                    EddieLogger.info("AirVPN ipleak.net: Current user local country is %s (%s)", userCountry, CountryContinent.getCountryName(userCountry));
                }
            }
            catch(SocketTimeoutException e)
            {
                EddieLogger.warning("AirVPNUser.getUserLocation() Cannot connect to AirVPN ipleak.net");

                return false;
            }
            catch(ConnectException e)
            {
                EddieLogger.warning("AirVPNUser.getUserLocation() Cannot connect to AirVPN ipleak.net");

                return false;
            }
            catch(UnknownHostException e)
            {
                EddieLogger.error("AirVPNUser.getUserLocation() Unknown host: %s", e);

                return false;
            }
            catch(Exception e)
            {
                EddieLogger.error("AirVPNUser.getUserLocation() Exception: %s", e);

                return false;
            }
            finally
            {
                if(bufferedReader != null)
                {
                    try
                    {
                        bufferedReader.close();
                    }
                    catch(Exception e)
                    {
                        EddieLogger.error("AirVPNUser.getUserLocation() Exception: %s", e);

                        return false;
                    }
                }
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            if(result)
            {
                userCountryStatus = UserCountryStatus.SET;

                eddieEvent.onAirVPNUserDataChanged();
            }
            else
            {
                EddieLogger.warning("AirVPNUser.getUserLocation(): Error while getting user's IP and country");

                userCountryStatus = UserCountryStatus.NOT_SET;
            }
        }
    }

    private boolean showLoginDialog(Context context)
    {
        loginResult = false;

        btnOk = null;
        btnCancel = null;

        final Handler dialogHandler = new Handler(new Handler.Callback()
        {
            @Override
            public boolean handleMessage(Message mesg)
            {
                throw new RuntimeException();
            }
        });

        dialogBuilder = new AlertDialog.Builder(context);

        View content = LayoutInflater.from(context).inflate(R.layout.login_dialog_layout, null);

        edtUserName = (EditText)content.findViewById(R.id.edt_login_username);
        edtUserPassword = (EditText)content.findViewById(R.id.edt_login_password);

        edtUserName.setText("");
        edtUserPassword.setText("");

        btnOk = (Button)content.findViewById(R.id.btn_login);
        btnCancel = (Button)content.findViewById(R.id.btn_cancel_login);

        chkRememberMe = (CheckBox)content.findViewById(R.id.chk_login_remember_me);

        chkRememberMe.setChecked(settingsManager.isAirVPNRememberMe());

        SupportTools.setButtonStatus(btnCancel, SupportTools.ShowMode.ENABLED);
        SupportTools.setButtonStatus(btnOk, SupportTools.ShowMode.DISABLED);

        btnOk.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                loginResult = true;

                airVPNUserName = edtUserName.getText().toString();
                airVPNUserPassword = edtUserPassword.getText().toString();

                settingsManager.setAirVPNRememberMe(chkRememberMe.isChecked());

                userAirVPNLoginDocument = documentBuilder.newDocument();

                if(userAirVPNLoginDocument != null)
                {
                    Element rootElement = userAirVPNLoginDocument.createElement(AIRVPN_USER_DATA);
                    userAirVPNLoginDocument.appendChild(rootElement);

                    Element user = userAirVPNLoginDocument.createElement(AIRVPN_USER_NAME_ITEM);
                    user.setNodeValue(airVPNUserName);
                    rootElement.appendChild(user);

                    Element password = userAirVPNLoginDocument.createElement(AIRVPN_USER_PASSWORD_ITEM);
                    password.setNodeValue(airVPNUserPassword);
                    rootElement.appendChild(password);

                    Element currentKey = userAirVPNLoginDocument.createElement(AIRVPN_USER_CURRENT_KEY);
                    currentKey.setNodeValue("");
                    rootElement.appendChild(currentKey);
                }

                if(chkRememberMe.isChecked())
                    saveUserCredentials(context);
                else
                    forgetAirVPNCredentials(context);

                loginDialog.dismiss();

                dialogHandler.sendMessage(dialogHandler.obtainMessage());
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                loginResult = false;

                loginDialog.dismiss();

                dialogHandler.sendMessage(dialogHandler.obtainMessage());
            }
        });

        edtUserName.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void afterTextChanged(Editable s)
            {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if(!edtUserName.getText().toString().isEmpty() && !edtUserPassword.getText().toString().isEmpty())
                    SupportTools.setButtonStatus(btnOk, SupportTools.ShowMode.ENABLED);
                else
                    SupportTools.setButtonStatus(btnOk, SupportTools.ShowMode.DISABLED);
            }
        });

        edtUserPassword.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void afterTextChanged(Editable s)
            {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if(!edtUserName.getText().toString().isEmpty() && !edtUserPassword.getText().toString().isEmpty())
                    SupportTools.setButtonStatus(btnOk, SupportTools.ShowMode.ENABLED);
                else
                    SupportTools.setButtonStatus(btnOk, SupportTools.ShowMode.DISABLED);
            }
        });

        dialogBuilder.setTitle(EddieApplication.context().getResources().getString(R.string.login_dialog_title));

        dialogBuilder.setView(content);

        loginDialog = dialogBuilder.create();
        loginDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        if(context instanceof Activity)
        {
            if(((Activity)context).isFinishing())
                return false;
        }

        loginDialog.show();

        try
        {
            Looper.loop();
        }
        catch(RuntimeException e)
        {
        }

        return loginResult;
    }

    private void loadUserProfile()
    {
        if(userProfileType == UserProfileType.PROCESSING)
            return;

        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                Document userProfileDocument = null;
                File userProfileFile = null;

                if((!airVPNUserName.isEmpty() || !airVPNUserPassword.isEmpty()) && supportTools.isNetworkConnectionActive() == true)
                {
                    if(!AirVPNManifest.getRsaPublicKeyModulus().isEmpty() && !AirVPNManifest.getRsaPublicKeyExponent().isEmpty())
                    {
                        HashMap<String, String> parameters = new HashMap<String, String>();

                        parameters.put("login", airVPNUserName);
                        parameters.put("password", airVPNUserPassword);
                        parameters.put("act", "user");

                        if(settingsManager.getAirVPNDefaultIPVersion().equals(SettingsManager.AIRVPN_IP_VERSION_6))
                            SupportTools.setBootServerIPv6Mode(true);
                        else
                            SupportTools.setBootServerIPv6Mode(false);

                        SupportTools.RequestAirVPNDocument userProfileRequest = supportTools.new RequestAirVPNDocument();

                        userProfileRequest.execute(parameters);
                    }
                }
                else
                {
                    userProfileFile = new File(EddieApplication.context().getFilesDir(), AIRVPN_USER_PROFILE_FILE_NAME);

                    if(userProfileFile.exists())
                    {
                        if(!masterPassword().isEmpty() && settingsManager.isMasterPasswordEnabled() == true)
                            userProfileDocument = supportTools.decryptFileToXmlDocument(AIRVPN_USER_PROFILE_FILE_NAME, masterPassword);
                        else if(settingsManager.isMasterPasswordEnabled() == false)
                            userProfileDocument = supportTools.loadXmlFileToDocument(AIRVPN_USER_PROFILE_FILE_NAME);
                        else
                        {
                            userProfileType = UserProfileType.NOT_SET;

                            userIsValid = false;

                            return;
                        }

                        if(airVPNUserName.isEmpty() && airVPNUserPassword.isEmpty())
                        {
                            userProfileType = UserProfileType.NOT_SET;

                            userIsValid = false;

                            return;
                        }
                        else
                            userIsValid = true;

                        userProfileType = UserProfileType.STORED;

                        processXmlUserProfile(userProfileDocument);
                    }
                    else
                    {
                        userProfileType = UserProfileType.NOT_SET;

                        userIsValid = false;
                    }
                }
            }
        };

        SupportTools.startThread(runnable);
    }

    private void processXmlUserProfile(Document userProfileDocument)
    {
        NodeList nodeList = null;
        NamedNodeMap namedNodeMap = null;
        UserProfileType newUserProfileType = userProfileType;
        AirVPNUser.UserKey localUserKey = null;
        String value, logMessage = "";

        if(userProfileDocument == null)
        {
            EddieLogger.error("AirVPNUser.processXmlUserProfile(): userProfileDocument is null.");

            userIsValid = false;

            return;
        }

        newUserProfileType = userProfileType;

        userProfileType = UserProfileType.PROCESSING;

        switch(newUserProfileType)
        {
            case STORED:
            {
                logMessage = "Setting user profile to the locally stored instance";
            }
            break;

            case FROM_SERVER:
            {
                logMessage = "Setting user profile to the instance downloaded from AirVPN server";
            }
            break;

            default:
            {
                logMessage = "User profile is not set.";
            }
            break;
        }

        EddieLogger.info(logMessage);

        if(newUserProfileType == UserProfileType.NOT_SET)
        {
            eddieEvent.onAirVPNUserProfileChanged();

            supportTools.dismissProgressDialog();

            userIsValid = false;

            userProfileType = UserProfileType.NOT_SET;

            return;
        }

        // user attributes

        nodeList = userProfileDocument.getElementsByTagName("user");

        if(nodeList != null && nodeList.getLength() > 0)
        {
            namedNodeMap = nodeList.item(0).getAttributes();

            if(namedNodeMap == null)
            {
                EddieLogger.error("AirVPNUser.processXmlUserProfile(): \"user\" node has no attributes");

                initializeUserProfileData();

                eddieEvent.onAirVPNUserProfileChanged();

                supportTools.dismissProgressDialog();

                userIsValid = false;

                userProfileType = UserProfileType.NOT_SET;

                return;
            }

            String messageAction = supportTools.getXmlItemNodeValue(namedNodeMap, "message_action");

            if(messageAction.isEmpty() == false)
            {
                userIsValid = false;

                userProfileType = UserProfileType.NOT_SET;

                eddieEvent.onAirVPNLoginFailed(supportTools.getXmlItemNodeValue(namedNodeMap, "message"));

                return;
            }

            if(!airVPNUserName.equals(supportTools.getXmlItemNodeValue(namedNodeMap, "login")))
            {
                userIsValid = false;

                userProfileType = UserProfileType.NOT_SET;

                eddieEvent.onAirVPNLoginFailed("AirVPNUser.processXmlUserProfile(): user name in profile data does not match to current user");

                return;
            }

            expirationDate = supportTools.getXmlItemNodeValue(namedNodeMap, "expirationdate");
            certificateAuthorityCertificate = supportTools.convertXmlEntities(supportTools.getXmlItemNodeValue(namedNodeMap, "ca"));
            tlsAuthKey = supportTools.convertXmlEntities(supportTools.getXmlItemNodeValue(namedNodeMap, "ta"));
            tlsCryptKey = supportTools.convertXmlEntities(supportTools.getXmlItemNodeValue(namedNodeMap, "tls_crypt"));
            sshKey = supportTools.convertXmlEntities(supportTools.getXmlItemNodeValue(namedNodeMap, "ssh_key"));
            sshPpk = supportTools.convertXmlEntities(supportTools.getXmlItemNodeValue(namedNodeMap, "ssh_ppk"));
            sslCertificate = supportTools.convertXmlEntities(supportTools.getXmlItemNodeValue(namedNodeMap, "ssl_crt"));
            wireguardPublicKey = supportTools.convertXmlEntities(supportTools.getXmlItemNodeValue(namedNodeMap, "wg_public_key"));

            if(!expirationDate.isEmpty())
            {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date now = new Date(System.currentTimeMillis());

                try
                {
                    airVPNSubscriptionExpirationDate = format.parse(expirationDate);

                    daysToExpiration = (int)TimeUnit.DAYS.convert(airVPNSubscriptionExpirationDate.getTime() - now.getTime(), TimeUnit.MILLISECONDS);
                }
                catch(ParseException e)
                {
                    airVPNSubscriptionExpirationDate = new Date();

                    daysToExpiration = 0;
                }
            }
            else
                daysToExpiration = 0;

            // user's keys (profiles)

            nodeList = userProfileDocument.getElementsByTagName("key");

            if(nodeList != null)
            {
                for(int position = 0; position < nodeList.getLength(); position++)
                {
                    localUserKey = new AirVPNUser.UserKey();

                    namedNodeMap = nodeList.item(position).getAttributes();

                    if(namedNodeMap != null)
                    {
                        value = supportTools.getXmlItemNodeValue(namedNodeMap, "name");

                        if(!value.isEmpty())
                        {
                            localUserKey.name = value;
                            localUserKey.certificate = supportTools.convertXmlEntities(supportTools.getXmlItemNodeValue(namedNodeMap, "crt"));
                            localUserKey.privateKey = supportTools.convertXmlEntities(supportTools.getXmlItemNodeValue(namedNodeMap, "key"));
                            localUserKey.wireguardPrivateKey = supportTools.convertXmlEntities(supportTools.getXmlItemNodeValue(namedNodeMap, "wg_private_key"));
                            localUserKey.wireguardPresharedKey = supportTools.convertXmlEntities(supportTools.getXmlItemNodeValue(namedNodeMap, "wg_preshared"));
                            localUserKey.wireguardIPv4 = supportTools.convertXmlEntities(supportTools.getXmlItemNodeValue(namedNodeMap, "wg_ipv4"));
                            localUserKey.wireguardIPv6 = supportTools.convertXmlEntities(supportTools.getXmlItemNodeValue(namedNodeMap, "wg_ipv6"));
                            localUserKey.wireguardDnsIPv4 = supportTools.convertXmlEntities(supportTools.getXmlItemNodeValue(namedNodeMap, "wg_dns_ipv4"));
                            localUserKey.wireguardDnsIPv6 = supportTools.convertXmlEntities(supportTools.getXmlItemNodeValue(namedNodeMap, "wg_dns_ipv6"));

                            addUserKey(value, localUserKey);
                        }
                        else
                            EddieLogger.warning("AirVPNUser.processXmlUserProfile(): found unnamed \"key\" in user profile");
                    }
                    else
                        EddieLogger.warning("AirVPNUser.processXmlUserProfile(): found \"key\" node in user profile with no attributes");
                }
            }
            else
                EddieLogger.error("AirVPNUser.processXmlUserProfile(): User has no defined \"keys\"");
        }
        else
        {
            EddieLogger.error("AirVPNUser.processXmlUserProfile(): \"user\" node not found");

            initializeUserProfileData();

            eddieEvent.onAirVPNUserProfileChanged();

            supportTools.dismissProgressDialog();

            userIsValid = false;

            userProfileType = UserProfileType.NOT_SET;

            return;
        }

        userProfileType = newUserProfileType;

        airVPNUserProfileDocument = userProfileDocument;

        if(eddieEvent != null)
            eddieEvent.onAirVPNUserProfileChanged();

        if(!masterPassword.isEmpty() && settingsManager.isMasterPasswordEnabled() == true)
        {
            if(!supportTools.encryptXmlDocumentToFile(airVPNUserProfileDocument, AirVPNUser.masterPassword(), AIRVPN_USER_PROFILE_FILE_NAME))
                EddieLogger.error("Error while saving AirVPN user profile to local storage");
        }
        else if(settingsManager.isMasterPasswordEnabled() == false)
        {
            if(!supportTools.saveXmlDocumentToFile(airVPNUserProfileDocument, AIRVPN_USER_PROFILE_FILE_NAME))
                EddieLogger.error("Error while saving AirVPN user profile to local storage");
        }

        userIsValid = true;

        eddieEvent.onAirVPNLogin();
    }

    public String getOpenVPNProfile(String userKeyName, String server, int port, String proto, String tlsMode, String cipher, boolean connectIPv6, boolean mode6to4, boolean createCountryProfile, String countryCode)
    {
        String openVpnProfile = "";

        if(userKeyName.isEmpty() || !userKey.containsKey(userKeyName))
        {
            EddieLogger.error(String.format("AirVPNUser.getOpenVPNProfile(): wrong user key name \"%s\"", userKeyName));

            return "";
        }

        if(server == null || server.isEmpty())
        {
            EddieLogger.error("AirVPNUser.getOpenVPNProfile(): server is empty");

            return "";
        }

        if(port < 1 || port > 65535)
        {
            EddieLogger.error(String.format("AirVPNUser.getOpenVPNProfile(): illegal port number %d", port));

            return "";
        }

        proto = proto.toLowerCase(Locale.US);

        if(!proto.equals("udp") && !proto.equals("tcp") && !proto.equals("udp6") && !proto.equals("tcp6"))
        {
            EddieLogger.error(String.format("AirVPNUser.getOpenVPNProfile(): illegal prptocol %s", proto));

            return "";

        }

        tlsMode = tlsMode.toLowerCase(Locale.US);

        if(!tlsMode.equals(SettingsManager.OPENVPN_TLS_MODE_AUTH) && !tlsMode.equals(SettingsManager.OPENVPN_TLS_MODE_CRYPT))
        {
            EddieLogger.error(String.format("AirVPNUser.getOpenVPNProfile(): illegal tls mode %s", tlsMode));

            return "";
        }

        if(createCountryProfile)
            server = supportTools.getAirVpnCountryHostname(countryCode, tlsMode, connectIPv6);

        openVpnProfile = "client\ndev tun\n";

        openVpnProfile += String.format(Locale.getDefault(), "remote %s %d" + System.getProperty("line.separator"), server, port);

        openVpnProfile += "proto " + proto + "\n";

        openVpnProfile += "setenv UV_IPV6 ";

        if(mode6to4 == true || connectIPv6 == true)
            openVpnProfile += "yes";
        else
            openVpnProfile += "no";

        openVpnProfile += "\nresolv-retry infinite\n" +
                "nobind\n" +
                "persist-key\n" +
                "persist-tun\n" +
                "verb 3\n" +
                "push-peer-info\n";

        if(proto.equals("udp"))
            openVpnProfile += "explicit-exit-notify 5\n";

        openVpnProfile += "remote-cert-tls server\n";

        if(cipher.equals(SettingsManager.AIRVPN_CIPHER_SERVER))
            openVpnProfile += "data-ciphers CHACHA20-POLY1305:AES-256-GCM:AES-128-GCM\n";
        else
        {
            openVpnProfile += "data-ciphers " + cipher + "\n";
            openVpnProfile += "ncp-disable\n";
        }

        if(tlsMode.equals(SettingsManager.OPENVPN_TLS_MODE_CRYPT))
            openVpnProfile += "auth sha512\n";

        if(tlsMode.equals(SettingsManager.OPENVPN_TLS_MODE_AUTH))
            openVpnProfile += "key-direction 1\n";

        if(certificateAuthorityCertificate.isEmpty())
        {
            EddieLogger.error("AirVPNUser.getOpenVPNProfile(): Certificate authority certificate (ca) is empty");

            return "";
        }

        openVpnProfile += "<ca>\n" + certificateAuthorityCertificate + "\n</ca>\n";

        UserKey profile = userKey.get(userKeyName);

        openVpnProfile += "<cert>\n" + profile.certificate + "\n</cert>\n";

        openVpnProfile += "<key>\n" + profile.privateKey + "\n</key>\n";

        if(tlsMode.equals("tls-auth"))
            openVpnProfile += "<tls-auth>\n" + tlsAuthKey + "\n</tls-auth>\n";
        else if(tlsMode.equals("tls-crypt"))
            openVpnProfile += "<tls-crypt>\n" + tlsCryptKey + "\n</tls-crypt>\n";

        return openVpnProfile;
    }

    public String getWireGuardProfile(String userKeyName, String server, int port, int listenPort, int fwMark, String allowedIPs, int persistentKeepalive, boolean connectIPv6, boolean mode6to4, boolean createCountryProfile, String countryCode)
    {
        UserKey userProfile;
        String wireguardProfile = "", address = "", dns = "";

        if(userKeyName.isEmpty() || !userKey.containsKey(userKeyName))
        {
            EddieLogger.error(String.format("AirVPNUser.getWireGuardProfile(): wrong user key name \"%s\"", userKeyName));

            return "";
        }

        if(server == null || server.isEmpty())
        {
            EddieLogger.error("AirVPNUser.getWireGuardProfile(): server is empty");

            return "";
        }

        if(port < 1 || port > 65535)
        {
            EddieLogger.error(String.format("AirVPNUser.getWireGuardProfile(): illegal port number %d", port));

            return "";
        }

        if(createCountryProfile)
            server = supportTools.getAirVpnCountryHostname(countryCode, SettingsManager.OPENVPN_TLS_MODE_AUTH, connectIPv6);

        userProfile = userKey.get(userKeyName);

        wireguardProfile = "[Interface]\n";

        address = "";

        if(connectIPv6 == false || mode6to4 == true)
            address = userProfile.wireguardIPv4;

        if(connectIPv6 == true || mode6to4 == true)
        {
            if(address.isEmpty() == false)
                address += ", ";

            address += userProfile.wireguardIPv6;
        }

        wireguardProfile += "Address = " + address;

        wireguardProfile += "\nPrivateKey = " + userProfile.wireguardPrivateKey + "\n";

        dns = "";

        if(connectIPv6 == false || mode6to4 == true)
            dns = userProfile.wireguardDnsIPv4;

        if(connectIPv6 == true || mode6to4 == true)
        {
            if(dns.isEmpty() == false)
                dns += ", ";

            dns += userProfile.wireguardDnsIPv6;
        }

        wireguardProfile += "DNS = " + dns + "\n";

        if(listenPort > 0)
            wireguardProfile += "ListenPort = " + listenPort + "\n";

        if(fwMark > 0)
            wireguardProfile += "FwMark = " + fwMark + "\n";

        wireguardProfile += "\n[Peer]\n";

        wireguardProfile += "PublicKey = " + wireguardPublicKey + "\n";
        wireguardProfile += "PresharedKey = " + userProfile.wireguardPresharedKey + "\n";
        wireguardProfile += "Endpoint = ";

        if(server.indexOf(':') > -1)
            wireguardProfile += "[";

        wireguardProfile += server;

        if(server.indexOf(':') > -1)
            wireguardProfile += "]";

        wireguardProfile += ":" + port + "\n";

        wireguardProfile += "AllowedIPs = " + allowedIPs + "\n";
        wireguardProfile += "PersistentKeepalive = " + persistentKeepalive + "\n";

        return wireguardProfile;
    }

    // Eddie events

    public void onVpnConnectionStatsChanged(final VPNConnectionStats connectionData)
    {
    }

    public void onVpnStatusChanged(VPN.Status status, String message)
    {
    }

    public void onVpnAuthFailed(final VPNEvent oe)
    {
    }

    public void onVpnError(final VPNEvent oe)
    {
    }

    public void onVpnReconnect(final VPNEvent oe)
    {
    }

    public void onMasterPasswordChanged()
    {
    }

    public void onAirVPNLogin()
    {
        supportTools.dismissProgressDialog();

        EddieLogger.info(String.format("Logged in to AirVPN as user %s", airVPNUserName.replace("%","%%")));

        userLoginFailed = false;
    }

    public void onAirVPNLoginFailed(String message)
    {
        String loginMessage = "";

        supportTools.dismissProgressDialog();

        if(message == null)
            loginMessage = "";
        else
            loginMessage = message;

        if(loginMessage.isEmpty())
            EddieLogger.error(String.format("Failed login to AirVPN as user %s", airVPNUserName.replace("%","%%")));
        else
            EddieLogger.error(String.format("Failed login to AirVPN as user %s (%s)", airVPNUserName.replace("%","%%"), loginMessage));

        airVPNUserName = "";
        airVPNUserPassword = "";

        userIsValid = false;
        userLoginFailed = true;
    }

    public void onAirVPNLogout()
    {
        userLoginFailed = false;
    }

    public void onAirVPNUserDataChanged()
    {
    }

    public void onAirVPNUserProfileReceived(final Document document)
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                userProfileType = UserProfileType.FROM_SERVER;

                processXmlUserProfile(document);
            }
        };

        supportTools.startThread(runnable);
    }

    public void onAirVPNUserProfileDownloadError()
    {
        userIsValid = false;

        supportTools.dismissProgressDialog();

        supportTools.infoDialog(String.format("%s%s", EddieApplication.context().getResources().getString(R.string.user_profile_download_error), EddieApplication.context().getResources().getString(R.string.bootstrap_server_error)), true);

        EddieLogger.error("Error while retrieving AirVPN user profile from server");
    }

    public void onAirVPNUserProfileChanged()
    {
    }

    public void onAirVPNManifestReceived(final Document document)
    {
    }

    public void onAirVPNManifestDownloadError()
    {
    }

    public void onAirVPNManifestChanged()
    {
    }

    public void onAirVPNIgnoredManifestDocumentRequest()
    {
    }

    public void onAirVPNIgnoredUserDocumentRequest()
    {
        supportTools.dismissProgressDialog();

        if(airVPNUserName.isEmpty() || airVPNUserPassword.isEmpty())
        {
            userIsValid = false;
            userLoginFailed = true;

            eddieEvent.onAirVPNLoginFailed("");
        }
    }

    public void onAirVPNRequestError(String message)
    {
    }

    public void onCancelConnection()
    {
    }

    public void onSaveState()
    {
    }

    public void onRestoreState(Bundle bundle)
    {
    }

    // NetworkStatusReceiver

    public void onNetworkStatusNotAvailable()
    {
    }

    public void onNetworkStatusConnected()
    {
    }

    public void onNetworkStatusIsConnecting()
    {
    }

    public void onNetworkStatusIsDisconnecting()
    {
    }

    public void onNetworkStatusSuspended()
    {
    }

    public void onNetworkStatusNotConnected()
    {
    }

    public void onNetworkTypeChanged()
    {
        if(userCountryStatus != UserCountryStatus.PROCESSING)
        {
            if(supportTools.isNetworkConnectionActive() == true && (EddieApplication.vpnManager().vpn().getConnectionStatus() == VPN.Status.NOT_CONNECTED))
                new getUserLocation().execute();
        }
    }
}
