// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 20 June 2018 - author: ProMIND - initial release. Based on revised code from com.eddie.android. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.Manifest;
import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.provider.Settings;

import java.util.Locale;

public class EddieApplication extends Application
{
    public static String LOG_TAG = "Eddie.Android";

    private static boolean initialized = false;
    private static Context context = null, baseContext = null, applicationContext = null;
    private static Locale deviceLocale = Locale.getDefault();
    private static SettingsManager settingsManager = null;
    private static EddieLogger eddieLogger = null;
    private static SupportTools supportTools = null;
    private static MainActivity mainActivity = null;
    private static CountryContinent countryContinent = null;
    private static AirVPNManifest airVPNManifest = null;
    private static AirVPNUser airVPNUser = null;
    private static NetworkStatusReceiver networkStatusReceiver = null;
    private static EddieEvent eddieEvent = null;
    private static ActivityManager.RunningAppProcessInfo appProcessInfo = null;
    private static VPNManager vpnManager = null;

    public static boolean isInitialized()
    {
        return initialized;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();

        if(!initialized)
        {
            context = this;
            baseContext = getBaseContext();
            applicationContext = getApplicationContext();

            appProcessInfo = new ActivityManager.RunningAppProcessInfo();

            settingsManager = new SettingsManager();

            if(AirVPNManifest.isEncrypted() || AirVPNUser.isDataEncrypted() || AirVPNUser.isProfileEncrypted())
                settingsManager.setEnableMasterPassword(true);
            else
            {
                settingsManager.setEnableMasterPassword(false);

                settingsManager.setAirVPNMasterPasswordHashCode(-1);
            }

            networkStatusReceiver = new NetworkStatusReceiver(this);

            eddieEvent = new EddieEvent();

            supportTools = new SupportTools();

            eddieLogger = new EddieLogger(applicationContext);

            vpnManager = new VPNManager(context);

            String localeCode = settingsManager.getSystemApplicationLanguage();

            if(!localeCode.isEmpty())
                EddieLogger.info("Application language overridden to %s", localeCode);

            supportTools.setLocale(baseContext);

            countryContinent = new CountryContinent();

            this.registerReceiver(networkStatusReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

            if(EddieLogger.isNewLogFile())
            {
                EddieLogger.info("Eddie Version Code: %s", BuildConfig.VERSION_CODE);
                EddieLogger.info("Eddie Version Name: %s", BuildConfig.VERSION_NAME);
                EddieLogger.info("Initializing Eddie for Android native library");
                EddieLogger.info("%s - %s", EddieLibrary.qualifiedName(), EddieLibrary.releaseDate());
                EddieLogger.info("Eddie Library API level: %s", EddieLibrary.apiLevel());
                EddieLogger.info("Architecture: %s", EddieLibrary.architecture());
                EddieLogger.info("Platform: %s", EddieLibrary.platform());
                EddieLogger.info("%s", EddieLibrary.openVPNInfo());
                EddieLogger.info("%s", EddieLibrary.openVPNCopyright());
                EddieLogger.info("SSL Library Version: %s", EddieLibrary.sslLibraryVersion());
                EddieLogger.info("WireGuard Version: %s", EddieLibrary.wireGuardVersion());

                EddieLogger.info("Manufacturer: %s", Build.MANUFACTURER);
                EddieLogger.info("Model: %s", Build.MODEL);
                EddieLogger.info("Device: %s", Build.DEVICE);
                EddieLogger.info("Brand: %s", Build.BRAND);
                EddieLogger.info("Product: %s", Build.PRODUCT);
                EddieLogger.info("Host: %s", Build.HOST);
                EddieLogger.info("Display: %s", Build.DISPLAY);
                EddieLogger.info("Android API Level: %d", Build.VERSION.SDK_INT);
                EddieLogger.info("Android Version Release: %s", Build.VERSION.RELEASE);
                EddieLogger.info("Fingerprint: %s", Build.FINGERPRINT);
                EddieLogger.info("OS Name: %s", System.getProperty("os.name"));
                EddieLogger.info("OS Architecture: %s", System.getProperty("os.arch"));
                EddieLogger.info("OS Version: %s", System.getProperty("os.version"));
                EddieLogger.info("Tags: %s", Build.TAGS);
                EddieLogger.info("Rooted device: %s", SupportTools.isDeviceRooted());
                EddieLogger.info("Developer Options Enabled: %s", (Settings.Secure.getInt(this.getContentResolver(), Settings.Global.DEVELOPMENT_SETTINGS_ENABLED , 0) == 0) ? "No" : "Yes");
                EddieLogger.info("Android TV: %s", (SupportTools.isTVDevice() == true) ? "Yes" : "No");

                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                {
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
                        EddieLogger.info("Notification permission: %s", (applicationContext.checkSelfPermission(Manifest.permission.POST_NOTIFICATIONS) == PackageManager.PERMISSION_GRANTED) ? "Granted" : "Not granted");

                    EddieLogger.info("Coarse Localization Permission: %s", (applicationContext.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) ? "Granted" : "Not granted");
                    EddieLogger.info("Fine Localization Permission: %s", (applicationContext.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) ? "Granted" : "Not granted");
                }
            }

            EddieLibraryResult result = EddieLibrary.init(appGuiVersion());

            if(result.code == EddieLibrary.SUCCESS)
            {
                initialized = true;

                EddieLogger.info("Eddie Library: initialization succeeded");
            }
            else
                EddieLogger.error("Eddie Library: initialization failed. %s", result.description);

            airVPNManifest = new AirVPNManifest();

            airVPNUser = new AirVPNUser();

            vpnManager.vpn().setConnectionStatus(VPN.Status.NOT_CONNECTED);

            if(Settings.Secure.getInt(this.getContentResolver(), Settings.Global.DEVELOPMENT_SETTINGS_ENABLED , 0) == 0 || supportTools.isLocationPermissionGranted() == false)
            {
                if(settingsManager.isGpsSpoofingEnabled() == true)
                {
                    if(Settings.Secure.getInt(this.getContentResolver(), Settings.Global.DEVELOPMENT_SETTINGS_ENABLED , 0) == 0)
                        EddieLogger.error("Developer options are disabled. Disabling GPS spooofing.");
                    else
                        EddieLogger.error("Location permissions is not granted. Disabling GPS spooofing.");
                }

                settingsManager.setGpsSpoofing(false);
            }
        }
    }

    public static boolean isVisible()
    {
        boolean visible = false;

        ActivityManager.getMyMemoryState(appProcessInfo);

        if(appProcessInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND || appProcessInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_VISIBLE)
            visible = true;
        else
            visible = false;

        return visible;
    }

    public static Context context()
    {
        return context;
    }

    public static Context baseContext()
    {
        return baseContext;
    }

    public static Context applicationContext()
    {
        return applicationContext;
    }

    public static Locale deviceLocale()
    {
        return deviceLocale;
    }

    public static SettingsManager settingsManager()
    {
        return settingsManager;
    }

    public static EddieLogger logger()
    {
        return eddieLogger;
    }

    public static SupportTools supportTools()
    {
        return supportTools;
    }

    public static VPNManager vpnManager()
    {
        return vpnManager;
    }

    public static AirVPNManifest airVPNManifest()
    {
        return airVPNManifest;
    }

    public static AirVPNUser airVPNUser()
    {
        return airVPNUser;
    }

    public static CountryContinent countryContinent()
    {
        return countryContinent;
    }

    public static NetworkStatusReceiver networkStatusReceiver()
    {
        return networkStatusReceiver;
    }

    public static EddieEvent eddieEvent()
    {
        return eddieEvent;
    }

    public static MainActivity mainActivity()
    {
        if(mainActivity == null)
            mainActivity = new MainActivity();

        return mainActivity;
    }

    public static void setMainActivity(MainActivity a)
    {
        mainActivity = a;
    }

    public static String appGuiVersion()
    {
        return "AirVPN Eddie for Android " + BuildConfig.VERSION_NAME;
    }

    @Override
    public void onTerminate()
    {
        if(initialized)
        {
            initialized = false;

            if(vpnManager().isVpnConnectionStarted() == true)
                vpnManager().stopConnection();

            EddieLibraryResult result = EddieLibrary.cleanUp();

            if(result.code != EddieLibrary.SUCCESS)
                EddieLogger.error("Eddie Library: OpenVPN cleanup failed. %s", result.description);

            result = EddieLibrary.disposeOpenVPNClient(null);

            if(result.code != EddieLibrary.SUCCESS)
                EddieLogger.error("Eddie Library: Failed to dispose OpenVPN client. '%s'", result.description);

            if(networkStatusReceiver != null)
                this.unregisterReceiver(networkStatusReceiver);
        }

        super.onTerminate();
    }
}
