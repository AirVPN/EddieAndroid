// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 3 October 2018 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class ConnectionInfoFragment extends Fragment implements NetworkStatusListener, EddieEventListener
{
    private final int FRAGMENT_ID = 5004;

    private SupportTools supportTools = null;
    private static VPNManager vpnManager = null;
    private NetworkStatusReceiver networkStatusReceiver = null;
    private EddieEvent eddieEvent = null;

    private Button btnDisconnect = null, btnPauseConnection = null, btnResumeConnection = null, btnLog = null;
    private TextView txtVpnStatus = null, txtNetworkStatus = null, txtGpsSpoofingStatus = null;
    private TextView txtCsConnectionType = null, txtCsAirVPNMode = null, txtCsAirVPNKey = null, txtCsAirVPNUser = null, txtCsServerName = null;
    private TextView txtCsServerHostCap = null, txtCsServerHost = null, txtCsServerPort = null, txtCsServerProto = null, txtCsServerIp = null;
    private TextView txtCsVpnIp4 = null, txtCsVpnIp6 = null, txtCsGwIp4 = null, txtCsGwIp6 = null;
    private TextView txtCsDnsCap = null, txtCsDns = null, txtCsTunnelName = null, txtCsCipherName = null, txtCsDigest = null;
    private TextView txtStatsInTotal = null, txtStatsInRate = null;
    private TextView txtStatsMaxInRate = null, txtSessionTime = null, txtTotalTime = null;
    private TextView txtStatsOutTotal = null, txtStatsOutRate = null, txtStatsMaxOutRate = null;
    private ImageView imgVpnType = null;
    private LinearLayout llVpnNotConnected = null, llVpnInfoLayout = null;
    private LinearLayout llConnectionData = null, llVpnStats = null;

    private Timer vpnStatTimer = null;
    private VPNTransportStats vpnStats = null;

    public void setVpnManager(VPNManager v)
    {
        vpnManager = v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        supportTools = EddieApplication.supportTools();

        networkStatusReceiver = EddieApplication.networkStatusReceiver();

        networkStatusReceiver.subscribeListener(this);

        eddieEvent = EddieApplication.eddieEvent();

        eddieEvent.subscribeListener(this);
    }

    @Override
    public void onStart()
    {
        super.onStart();
    }

    @Override
    public void onStop()
    {
        super.onStop();

        stopVPNStats();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        if(networkStatusReceiver != null)
            networkStatusReceiver.unsubscribeListener(this);

        if(eddieEvent != null)
            eddieEvent.unsubscribeListener(this);

        stopVPNStats();
    }

    @Override
    public void onResume()
    {
        super.onResume();

        resume();
    }

    @Override
    public void onPause()
    {
        super.onPause();

        stopVPNStats();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View fragmentView = inflater.inflate(R.layout.fragment_connection_info_layout, container, false);

        if(fragmentView != null)
        {
            btnDisconnect = (Button)fragmentView.findViewById(R.id.disconnect_btn);
            btnPauseConnection = (Button)fragmentView.findViewById(R.id.pause_connection_btn);
            btnResumeConnection = (Button)fragmentView.findViewById(R.id.resume_connection_btn);
            btnLog = (Button)fragmentView.findViewById(R.id.log_btn);

            btnDisconnect.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View arg0)
                {
                    final Runnable uiRunnable = new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            stopConnection();
                        }
                    };

                    SupportTools.runOnUiActivity(getActivity(), uiRunnable);
                }
            });

            btnPauseConnection.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View arg0)
                {
                    final Runnable uiRunnable = new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            pauseConnection();
                        }
                    };

                    SupportTools.runOnUiActivity(getActivity(), uiRunnable);
                }
            });

            btnResumeConnection.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View arg0)
                {
                    resumeConnection();
                }
            });

            btnLog.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View arg0)
                {
                    Intent logActivityIntent = new Intent(getContext(), LogActivity.class);

                    logActivityIntent.putExtra("ViewMode", LogActivity.ViewMode.DEFAULT);

                    startActivity(logActivityIntent);
                }
            });

            llVpnNotConnected = (LinearLayout)fragmentView.findViewById(R.id.vpn_not_connected_layout);

            txtVpnStatus = (TextView)fragmentView.findViewById(R.id.vpn_connection_status);
            txtVpnStatus.setText(getResources().getString(vpnManager.vpn().connectionStatusResourceDescription(vpnManager.vpn().getConnectionStatus())));

            txtNetworkStatus = (TextView)fragmentView.findViewById(R.id.network_connection_status);

            if(NetworkStatusReceiver.getNetworkStatus() == NetworkStatusReceiver.Status.CONNECTED)
                txtNetworkStatus.setText(String.format(Locale.getDefault(), getResources().getString(R.string.conn_status_connected), NetworkStatusReceiver.getNetworkDescription()));
            else
                txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));

            txtGpsSpoofingStatus = (TextView)fragmentView.findViewById(R.id.gps_spoofing_status);
            txtGpsSpoofingStatus.setText("");

            llVpnInfoLayout = (LinearLayout)fragmentView.findViewById(R.id.vpn_info_layout);

            llConnectionData = (LinearLayout)fragmentView.findViewById(R.id.connection_data_layout);
            txtCsConnectionType = (TextView)fragmentView.findViewById(R.id.cs_connection_type);
            imgVpnType = (ImageView)fragmentView.findViewById(R.id.cs_vpn_type);
            txtCsAirVPNMode = (TextView)fragmentView.findViewById(R.id.cs_airvpn_mode);
            txtCsAirVPNKey = (TextView)fragmentView.findViewById(R.id.cs_airvpn_key);
            txtCsAirVPNUser = (TextView)fragmentView.findViewById(R.id.cs_airvpn_user);
            txtCsServerName = (TextView)fragmentView.findViewById(R.id.cs_server_name);
            txtCsServerHostCap = (TextView)fragmentView.findViewById(R.id.cs_server_host_cap);
            txtCsServerHost = (TextView)fragmentView.findViewById(R.id.cs_server_host);
            txtCsServerPort = (TextView)fragmentView.findViewById(R.id.cs_server_port);
            txtCsServerProto = (TextView)fragmentView.findViewById(R.id.cs_server_proto);
            txtCsServerIp = (TextView)fragmentView.findViewById(R.id.cs_server_ip);
            txtCsVpnIp4 = (TextView)fragmentView.findViewById(R.id.cs_vpn_ip4);
            txtCsVpnIp6 = (TextView)fragmentView.findViewById(R.id.cs_vpn_ip6);
            txtCsGwIp4 = (TextView)fragmentView.findViewById(R.id.cs_gw_ip4);
            txtCsGwIp6 = (TextView)fragmentView.findViewById(R.id.cs_gw_ip6);
            txtCsDnsCap = (TextView)fragmentView.findViewById(R.id.cs_dns_cap);
            txtCsDns = (TextView)fragmentView.findViewById(R.id.cs_dns);
            txtCsTunnelName = (TextView)fragmentView.findViewById(R.id.cs_tunnel_name);
            txtCsCipherName = (TextView)fragmentView.findViewById(R.id.cs_cipher_name);

            llVpnStats = (LinearLayout)fragmentView.findViewById(R.id.vpn_stats_layout);

            txtStatsInTotal = (TextView)fragmentView.findViewById(R.id.stats_in_total);
            txtStatsInRate = (TextView)fragmentView.findViewById(R.id.stats_in_rate);
            txtStatsMaxInRate = (TextView)fragmentView.findViewById(R.id.stats_max_in_rate);
            txtStatsOutTotal = (TextView)fragmentView.findViewById(R.id.stats_out_total);
            txtStatsOutRate = (TextView)fragmentView.findViewById(R.id.stats_out_rate);
            txtStatsMaxOutRate = (TextView)fragmentView.findViewById(R.id.stats_max_out_rate);
            txtSessionTime = (TextView)fragmentView.findViewById(R.id.stats_session_time);
            txtTotalTime = (TextView)fragmentView.findViewById(R.id.stats_total_time);

            VPN.Status status = vpnManager.vpn().getConnectionStatus();

            switch(status)
            {
                case CONNECTING:
                case CONNECTED:
                case PAUSED_BY_USER:
                case PAUSED_BY_SYSTEM:
                case LOCKED:
                {
                    showConnectionStatus(getResources().getString(vpnManager.vpn().connectionStatusResourceDescription(status)));
                }
                break;

                case DISCONNECTING:
                case NOT_CONNECTED:
                case CONNECTION_REVOKED_BY_SYSTEM:
                {
                    hideConnectionStatus();
                }
                break;

                default:
                {
                    hideConnectionStatus();
                }
                break;
            }
        }

        return fragmentView;
    }

    private void stopConnection()
    {
        if(!supportTools.confirmationDialog(R.string.conn_confirm_disconnection))
            return;

        try
        {
            vpnManager.stopConnection();
        }
        catch(Exception e)
        {
            EddieLogger.error("ConnectionInfoFragment.stopConnection() exception: %s", e.getMessage());
        }
    }

    private void pauseConnection()
    {
        if(vpnManager.vpn().getConnectionStatus() == VPN.Status.CONNECTED)
        {
            vpnManager.pauseConnection();

            SupportTools.setButtonStatus(btnPauseConnection, SupportTools.ShowMode.HIDDEN);

            SupportTools.setButtonStatus(btnResumeConnection, SupportTools.ShowMode.ENABLED);
        }
    }

    private void resumeConnection()
    {
        if((vpnManager.vpn().getConnectionStatus() == VPN.Status.PAUSED_BY_USER || vpnManager.vpn().getConnectionStatus() == VPN.Status.PAUSED_BY_SYSTEM))
        {
            vpnManager.resumeConnection();

            SupportTools.setButtonStatus(btnResumeConnection, SupportTools.ShowMode.HIDDEN);

            SupportTools.setButtonStatus(btnPauseConnection, SupportTools.ShowMode.ENABLED);
        }
    }

    public void showConnectionStatus(String vpnStatusDescription)
    {
        String gpsSpoofingStatus;

        if(llVpnNotConnected != null)
            llVpnNotConnected.setVisibility(View.GONE);

        if(llVpnInfoLayout != null)
            llVpnInfoLayout.setVisibility(View.VISIBLE);

        if(llVpnStats != null)
            llVpnStats.setVisibility(View.VISIBLE);

        if(llConnectionData != null)
            llConnectionData.setVisibility(View.VISIBLE);

        if(vpnManager.vpn().getType() == VPN.Type.OPENVPN)
        {
            if(supportTools.isNetworkConnectionActive() && (vpnManager.vpn().getConnectionStatus() == VPN.Status.CONNECTED || vpnManager.vpn().getConnectionStatus() == VPN.Status.CONNECTING))
            {
                SupportTools.setButtonStatus(btnPauseConnection, SupportTools.ShowMode.ENABLED);

                SupportTools.setButtonStatus(btnResumeConnection, SupportTools.ShowMode.HIDDEN);
            }
            else if(supportTools.isNetworkConnectionActive(true) && (vpnManager.vpn().getConnectionStatus() == VPN.Status.PAUSED_BY_USER || vpnManager.vpn().getConnectionStatus() == VPN.Status.PAUSED_BY_SYSTEM))
            {
                SupportTools.setButtonStatus(btnPauseConnection, SupportTools.ShowMode.HIDDEN);

                SupportTools.setButtonStatus(btnResumeConnection, SupportTools.ShowMode.ENABLED);
            }
            else
            {
                SupportTools.setButtonStatus(btnPauseConnection, SupportTools.ShowMode.HIDDEN);

                SupportTools.setButtonStatus(btnResumeConnection, SupportTools.ShowMode.HIDDEN);
            }
        }
        else
        {
            SupportTools.setButtonStatus(btnPauseConnection, SupportTools.ShowMode.HIDDEN);

            SupportTools.setButtonStatus(btnResumeConnection, SupportTools.ShowMode.HIDDEN);
        }

        if(txtVpnStatus != null)
            txtVpnStatus.setText(vpnStatusDescription);

        if(txtGpsSpoofingStatus != null)
        {
            gpsSpoofingStatus = getResources().getString(vpnManager.isGpsSpoofingEnabled() ? R.string.yes : R.string.no);

            if(vpnManager.isGpsSpoofingEnabled() == true)
                gpsSpoofingStatus += " (" + CountryContinent.getCountryName(vpnManager.getGpsSpoofingCoordinates().getCountryCode()) + ")";

            txtGpsSpoofingStatus.setText(gpsSpoofingStatus);
        }

        if(vpnManager.isVpnConnectionStarted())
        {
            updateConnectionData(vpnManager.vpn().getVpnConnectionStats());

            showVpnStats();
        }
        else
        {
            if(llVpnStats != null)
                llVpnStats.setVisibility(View.GONE);

            if(llConnectionData != null)
                llConnectionData.setVisibility(View.GONE);
        }
    }

    public void resume()
    {
        if(vpnManager.vpn().getConnectionStatus() == VPN.Status.CONNECTED)
        {
            startVPNStats();

            updateConnectionData(vpnManager.vpn().getVpnConnectionStats());
        }
        else
        {
            stopVPNStats();

            if(llConnectionData != null)
                llConnectionData.setVisibility(View.GONE);
        }

        if(txtNetworkStatus != null)
        {
            if(NetworkStatusReceiver.getNetworkStatus() == NetworkStatusReceiver.Status.CONNECTED)
                txtNetworkStatus.setText(String.format(Locale.getDefault(), EddieApplication.applicationContext().getResources().getString(R.string.conn_status_connected), NetworkStatusReceiver.getNetworkDescription()));
            else
                txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));
        }

        if(vpnManager.vpn().getType() == VPN.Type.OPENVPN)
        {
            if(NetworkStatusReceiver.getNetworkStatus() == NetworkStatusReceiver.Status.CONNECTED)
            {
                if((vpnManager.vpn().getConnectionStatus() == VPN.Status.CONNECTED || vpnManager.vpn().getConnectionStatus() == VPN.Status.CONNECTING))
                {
                    SupportTools.setButtonStatus(btnPauseConnection, SupportTools.ShowMode.ENABLED);

                    SupportTools.setButtonStatus(btnResumeConnection, SupportTools.ShowMode.HIDDEN);
                }
                else if(vpnManager.vpn().getConnectionStatus() == VPN.Status.PAUSED_BY_USER || vpnManager.vpn().getConnectionStatus() == VPN.Status.PAUSED_BY_SYSTEM)
                {
                    SupportTools.setButtonStatus(btnPauseConnection, SupportTools.ShowMode.HIDDEN);

                    SupportTools.setButtonStatus(btnResumeConnection, SupportTools.ShowMode.ENABLED);
                }
            }
            else
            {
                SupportTools.setButtonStatus(btnPauseConnection, SupportTools.ShowMode.HIDDEN);

                SupportTools.setButtonStatus(btnResumeConnection, SupportTools.ShowMode.HIDDEN);
            }
        }
        else
        {
            SupportTools.setButtonStatus(btnPauseConnection, SupportTools.ShowMode.HIDDEN);

            SupportTools.setButtonStatus(btnResumeConnection, SupportTools.ShowMode.HIDDEN);
        }
    }

    public void hideConnectionStatus()
    {
        if(llVpnNotConnected != null)
            llVpnNotConnected.setVisibility(View.VISIBLE);

        if(llVpnInfoLayout != null)
            llVpnInfoLayout.setVisibility(View.GONE);

        if(llVpnStats != null)
            llVpnStats.setVisibility(View.GONE);

        if(llConnectionData != null)
            llConnectionData.setVisibility(View.GONE);
    }

    public void updateConnectionData(VPNConnectionStats connectionData)
    {
        ArrayList<IPAddress> dnsEntry = vpnManager.vpn().getDns();
        HashMap<String, String> profileInfo = vpnManager.vpn().getProfileInfo();
        AirVPNServer server = null;
        String dnsInfo, serverHost = "";

        if(llConnectionData == null || connectionData == null)
            return;

        if(profileInfo != null)
        {
            llConnectionData.setVisibility(View.VISIBLE);

            txtCsConnectionType.setText(vpnManager.vpn().getConnectionModeDescription());

            if(profileInfo.get("vpn_type").equals(VPN.Type.OPENVPN.toString()))
                imgVpnType.setImageResource(R.drawable.openvpn_logo);
            else
                imgVpnType.setImageResource(R.drawable.wireguard_logo);

            if(profileInfo.get("vpn_type").equals(VPN.Type.OPENVPN.toString()) && EddieApplication.settingsManager().getAirVPNOpenVPNMode().equals(SettingsManager.AIRVPN_OPENVPN_MODE_DEFAULT) == false)
                txtCsAirVPNMode.setText(EddieApplication.airVPNManifest().getMode(EddieApplication.settingsManager().getAirVPNOpenVPNMode()).getTitle());
            else if(profileInfo.get("vpn_type").equals(VPN.Type.WIREGUARD.toString()) && EddieApplication.settingsManager().getAirVPNWireGuardMode().equals(SettingsManager.AIRVPN_WIREGUARD_MODE_DEFAULT) == false)
                txtCsAirVPNMode.setText(EddieApplication.airVPNManifest().getMode(EddieApplication.settingsManager().getAirVPNWireGuardMode()).getTitle());
            else
                txtCsAirVPNMode.setText(R.string.none);

            if(vpnManager.vpn().getConnectionMode() != VPN.ConnectionMode.OPENVPN_PROFILE || vpnManager.vpn().getConnectionMode() != VPN.ConnectionMode.WIREGUARD_PROFILE)
                txtCsAirVPNKey.setText(vpnManager.vpn().getUserProfileDescription());
            else
            {
                if(profileInfo != null)
                    txtCsAirVPNKey.setText(profileInfo.get("name"));
                else
                    txtCsAirVPNKey.setText("--");
            }

            if(!vpnManager.vpn().getUserName().isEmpty())
                txtCsAirVPNUser.setText(vpnManager.vpn().getUserName());
            else
                txtCsAirVPNUser.setText(connectionData.user);

            if(profileInfo != null)
                txtCsServerName.setText(profileInfo.get("description"));
            else
                txtCsServerName.setText("--");

            if(!connectionData.serverIp.isEmpty())
                serverHost = connectionData.serverIp;
            else
            {
                if(profileInfo.containsKey("server"))
                    serverHost = profileInfo.get("server");
                else
                    serverHost = "--";
            }

            server = EddieApplication.airVPNManifest().getServerByIP(serverHost);

            if(server != null)
                serverHost = EddieApplication.airVPNManifest().getFullServerDescription(server.getName());

            if(profileInfo.get("description").equals(serverHost) == true || connectionData.serverIp.equals(serverHost) == true)
            {
                txtCsServerHostCap.setVisibility(View.GONE);

                txtCsServerHost.setVisibility(View.GONE);
            }
            else
            {
                txtCsServerHostCap.setVisibility(View.VISIBLE);

                txtCsServerHost.setVisibility(View.VISIBLE);

                txtCsServerHost.setText(serverHost);
            }

            txtCsServerIp.setText(connectionData.serverIp);
            txtCsServerPort.setText(connectionData.serverPort);
            txtCsServerProto.setText(connectionData.serverProto);
            txtCsVpnIp4.setText(connectionData.vpnIp4);
            txtCsVpnIp6.setText(connectionData.vpnIp6);
            txtCsGwIp4.setText(connectionData.gw4);
            txtCsGwIp6.setText(connectionData.gw6);

            if(dnsEntry.size() > 0)
            {
                txtCsDnsCap.setVisibility(View.VISIBLE);
                txtCsDns.setVisibility(View.VISIBLE);

                dnsInfo = "";

                for(IPAddress dns : dnsEntry)
                {
                    if(dnsInfo.isEmpty() == false)
                        dnsInfo += System.getProperty("line.separator");

                    dnsInfo += dns.getIpAddress() + " (IPv";

                    if(dns.getIpFamily() == IPAddress.IPFamily.IPv4)
                        dnsInfo += "4";
                    else
                        dnsInfo += "6";

                    dnsInfo += ")";
                }

                txtCsDns.setText(dnsInfo);
            }
            else
            {
                txtCsDnsCap.setVisibility(View.GONE);
                txtCsDns.setVisibility(View.GONE);
            }

            txtCsTunnelName.setText(connectionData.tunName);
            txtCsCipherName.setText(connectionData.cipher);
        }
        else
            llConnectionData.setVisibility(View.GONE);
    }

    public void enableDisconnectButton(boolean enabled)
    {
        if(btnDisconnect != null)
            SupportTools.setButtonStatus(btnDisconnect, enabled == true ? SupportTools.ShowMode.ENABLED : SupportTools.ShowMode.DISABLED);
    }

    public void startVPNStats()
    {
        int updateInterval = VPNService.STATS_UPDATE_INTERVAL_SECONDS * 1000;

        if(vpnStatTimer != null)
            vpnStatTimer.cancel();

        if(llVpnStats != null)
            llVpnStats.setVisibility(View.VISIBLE);

        vpnStatTimer = new Timer();

        vpnStatTimer.schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                 if(EddieApplication.isVisible())
                    updateVPNStats();
            }
        }, updateInterval, updateInterval);
    }

    public void stopVPNStats()
    {
        if(llVpnStats != null)
            llVpnStats.setVisibility(View.GONE);

        if(vpnStatTimer != null)
            vpnStatTimer.cancel();

        vpnStatTimer = null;
    }

    private void updateVPNStats()
    {
        if(getActivity() != null)
        {
            Runnable runnable = new Runnable()
            {
                @Override
                public void run()
                {
                    showVpnStats();
                }
            };

            SupportTools.runOnUiActivity(getActivity(), runnable);
        }
    }

    private void showVpnStats()
    {
        if(llVpnStats == null)
            return;

        llVpnStats.setVisibility(View.VISIBLE);

        vpnStats = vpnManager.vpn().getVpnTransportStats();

        if(txtStatsInTotal != null && vpnStats != null)
            txtStatsInTotal.setText(supportTools.formatDataVolume(vpnStats.bytesIn));

        if(txtStatsInRate != null)
            txtStatsInRate.setText(supportTools.formatTransferRate(vpnManager.vpn().getInRate()));

        if(txtStatsMaxInRate != null)
            txtStatsMaxInRate.setText(supportTools.formatTransferRate(vpnManager.vpn().getMaxInRate()));

        if(txtStatsOutTotal != null && vpnStats != null)
            txtStatsOutTotal.setText(supportTools.formatDataVolume(vpnStats.bytesOut));

        if(txtStatsOutRate != null)
            txtStatsOutRate.setText(supportTools.formatTransferRate(vpnManager.vpn().getOutRate()));

        if(txtStatsMaxOutRate != null)
            txtStatsMaxOutRate.setText(supportTools.formatTransferRate(vpnManager.vpn().getMaxOutRate()));

        if(txtSessionTime != null)
            txtSessionTime.setText(vpnManager.vpn().getFormattedSessionTime());

        if(txtTotalTime != null)
            txtTotalTime.setText(vpnManager.vpn().getFormattedTotalConnectionTime());
    }

    // Eddie events

    public void onVpnConnectionStatsChanged(final VPNConnectionStats connectionStats)
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                updateConnectionData(connectionStats);
            }
        };

        SupportTools.runOnUiActivity(getActivity(), runnable);
    }

    public void onVpnStatusChanged(final VPN.Status status, final String message)
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                String vpnStatusDescription = "";

                enableDisconnectButton((status == VPN.Status.CONNECTING) || (status == VPN.Status.CONNECTED) || (status == VPN.Status.PAUSED_BY_USER) || (status == VPN.Status.PAUSED_BY_SYSTEM) || (status == VPN.Status.LOCKED));

                if(vpnManager.isVpnConnectionStopped())
                    vpnStatusDescription = getResources().getString(R.string.conn_status_initialize);
                else
                    vpnStatusDescription = message;

                switch(status)
                {
                    case CONNECTING:
                    {
                    }
                    break;

                    case CONNECTED:
                    {
                        startVPNStats();

                        showConnectionStatus(vpnStatusDescription);
                    }
                    break;

                    case PAUSED_BY_USER:
                    case PAUSED_BY_SYSTEM:
                    case LOCKED:
                    {
                        stopVPNStats();

                        showConnectionStatus(vpnStatusDescription);
                    }
                    break;

                    case NOT_CONNECTED:
                    case CONNECTION_REVOKED_BY_SYSTEM:
                    {
                        stopVPNStats();

                        hideConnectionStatus();
                    }
                    break;

                    default:
                    {
                        stopVPNStats();

                        hideConnectionStatus();
                    }
                    break;
                }
            }
        };

        SupportTools.runOnUiActivity(getActivity(), runnable);
    }

    public void onVpnAuthFailed(final VPNEvent oe)
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                enableDisconnectButton(false);

                showConnectionStatus(getResources().getString(R.string.conn_auth_failed));
            }
        };

        SupportTools.runOnUiActivity(getActivity(), runnable);
    }

    public void onVpnError(final VPNEvent oe)
    {
    }

    public void onVpnReconnect(final VPNEvent oe)
    {
    }

    public void onMasterPasswordChanged()
    {
    }

    public void onAirVPNLogin()
    {
    }

    public void onAirVPNLoginFailed(String message)
    {
    }

    public void onAirVPNLogout()
    {
    }

    public void onAirVPNUserDataChanged()
    {
    }

    public void onAirVPNUserProfileReceived(Document document)
    {
    }

    public void onAirVPNUserProfileDownloadError()
    {
    }

    public void onAirVPNUserProfileChanged()
    {
    }

    public void onAirVPNManifestReceived(Document document)
    {
    }

    public void onAirVPNManifestDownloadError()
    {
    }

    public void onAirVPNManifestChanged()
    {
    }

    public void onAirVPNIgnoredManifestDocumentRequest()
    {
    }

    public void onAirVPNIgnoredUserDocumentRequest()
    {
    }

    public void onAirVPNRequestError(String message)
    {
    }

    public void onCancelConnection()
    {
    }

    public void onSaveState()
    {
    }

    public void onRestoreState(Bundle bundle)
    {
        resume();
    }

    // NetworkStatusReceiver

    public void onNetworkStatusNotAvailable()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));

        SupportTools.setButtonStatus(btnPauseConnection, SupportTools.ShowMode.HIDDEN);

        SupportTools.setButtonStatus(btnResumeConnection, SupportTools.ShowMode.HIDDEN);
    }

    public void onNetworkStatusConnected()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(String.format(Locale.getDefault(), getResources().getString(R.string.conn_status_connected), NetworkStatusReceiver.getNetworkDescription()));

        if(vpnManager != null)
        {
            if(vpnManager.vpn() != null && vpnManager.vpn().getType() == VPN.Type.OPENVPN)
            {
                if(btnPauseConnection != null && vpnManager.vpn().getConnectionStatus() == VPN.Status.CONNECTED)
                {
                    SupportTools.setButtonStatus(btnPauseConnection, SupportTools.ShowMode.ENABLED);

                    SupportTools.setButtonStatus(btnPauseConnection, SupportTools.ShowMode.HIDDEN);
                }
                else if(btnResumeConnection != null && vpnManager.vpn().getConnectionStatus() == VPN.Status.PAUSED_BY_USER)
                {
                    SupportTools.setButtonStatus(btnPauseConnection, SupportTools.ShowMode.HIDDEN);

                    SupportTools.setButtonStatus(btnResumeConnection, SupportTools.ShowMode.ENABLED);
                }
                else
                {
                    SupportTools.setButtonStatus(btnPauseConnection, SupportTools.ShowMode.HIDDEN);

                    SupportTools.setButtonStatus(btnResumeConnection, SupportTools.ShowMode.HIDDEN);
                }
            }
            else
            {
                SupportTools.setButtonStatus(btnPauseConnection, SupportTools.ShowMode.HIDDEN);

                SupportTools.setButtonStatus(btnResumeConnection, SupportTools.ShowMode.HIDDEN);
            }
        }
    }

    public void onNetworkStatusIsConnecting()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));

        SupportTools.setButtonStatus(btnPauseConnection, SupportTools.ShowMode.HIDDEN);

        SupportTools.setButtonStatus(btnResumeConnection, SupportTools.ShowMode.HIDDEN);
    }

    public void onNetworkStatusIsDisconnecting()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));

        SupportTools.setButtonStatus(btnPauseConnection, SupportTools.ShowMode.HIDDEN);

        SupportTools.setButtonStatus(btnResumeConnection, SupportTools.ShowMode.HIDDEN);
    }

    public void onNetworkStatusSuspended()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));

        SupportTools.setButtonStatus(btnPauseConnection, SupportTools.ShowMode.HIDDEN);

        SupportTools.setButtonStatus(btnResumeConnection, SupportTools.ShowMode.HIDDEN);
    }

    public void onNetworkStatusNotConnected()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_disconnected));

        SupportTools.setButtonStatus(btnPauseConnection, SupportTools.ShowMode.HIDDEN);

        SupportTools.setButtonStatus(btnResumeConnection, SupportTools.ShowMode.HIDDEN);
    }

    public void onNetworkTypeChanged()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_disconnected));

        SupportTools.setButtonStatus(btnPauseConnection, SupportTools.ShowMode.HIDDEN);

        SupportTools.setButtonStatus(btnResumeConnection, SupportTools.ShowMode.HIDDEN);
    }
}
