// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>

package org.airvpn.eddie;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.service.quicksettings.Tile;
import android.service.quicksettings.TileService;

import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

@TargetApi(Build.VERSION_CODES.N)
public class AirVPNTileService extends TileService implements NetworkStatusListener, EddieEventListener
{
    private EddieEvent eddieEvent = null;
    private NetworkStatusReceiver networkStatusReceiver = null;
    private static VPNManager vpnManager = null;

    @SuppressLint("Override")
    @TargetApi(Build.VERSION_CODES.N)
    @Override
    public void onCreate()
    {
        super.onCreate();

        setTileStatus();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        init();

        return START_STICKY;
    }

    @Override
    public void onDestroy()
    {
        if(networkStatusReceiver != null)
            networkStatusReceiver.unsubscribeListener(this);

        if(eddieEvent != null)
            eddieEvent.unsubscribeListener(this);

        super.onDestroy();
    }

    @SuppressLint("Override")
    @TargetApi(Build.VERSION_CODES.N)
    @Override
    public void onClick()
    {
        super.onClick();

        if(vpnManager.isVpnConnectionStarted() == true || vpnManager.isVpnConnectionPaused() == true)
            vpnManager.stopConnection();
        else if(vpnManager.isVpnConnectionStopped() == true)
        {
            Runnable uiRunnable = new Runnable()
            {
                @Override
                public void run()
                {
                    startConnection();
                }
            };

            SupportTools.startThread(uiRunnable);
        }
        else
            EddieLogger.error("AirVPNTileService: Unhandled VPN status.");

        setTileStatus();
    }

    @TargetApi(Build.VERSION_CODES.N)
    @Override
    public IBinder onBind(Intent intent)
    {
        init();

        return super.onBind(intent);
    }

    @TargetApi(Build.VERSION_CODES.N)
    @Override
    public void onTileAdded()
    {
        setTileStatus();
    }

    @TargetApi(Build.VERSION_CODES.N)
    @Override
    public void onStartListening()
    {
        super.onStartListening();

        setTileStatus();
    }

    @TargetApi(Build.VERSION_CODES.N)
    @Override
    public void onStopListening()
    {
        super.onStopListening();
    }

    private void init()
    {
        TileService.requestListeningState(this, new ComponentName(this, AirVPNTileService.class));

        networkStatusReceiver = EddieApplication.networkStatusReceiver();

        networkStatusReceiver.subscribeListener(this);

        eddieEvent = EddieApplication.eddieEvent();

        eddieEvent.subscribeListener(this);

        vpnManager = EddieApplication.vpnManager();

        setTileStatus();
    }

    private void startConnection()
    {
        AirVPNUser airVPNUser = EddieApplication.airVPNUser();
        AirVPNServerProvider airVPNServerProvider = new AirVPNServerProvider();
        AirVPNServer server = null;
        AirVPNManifest.Mode manifestMode = null;
        SettingsManager settingsManager = EddieApplication.settingsManager();
        CountryContinent countryContinent = EddieApplication.countryContinent();
        ArrayList<AirVPNServer> airVPNServerList = null;
        HashMap<String, String> profileInfo = null;
        HashMap<Integer, String> serverEntryIP = null;
        String userKeyName = "", protocol = "", tlsMode = "";
        String allowedIPs = "", vpnProfile = "", profileString = "";
        int port = 0, entry = 0, i = 0, loopWaitMilliSeconds = 2000, userWaitLoops = 10; // 20 seconds
        boolean connectIPv6 = false, mode6to4 = false;

        if(vpnManager.isVpnPermissionGranted() == false)
        {
            EddieLogger.error("AirVPNTileService.startConnection(): VPN permission not granted. Please start an interactive VPN connection and grant VPN permission to the app.");

            return;
        }

        if(airVPNUser == null)
        {
            EddieLogger.error("AirVPNTileService.startConnection(): Cannot start connection. User is not logged in.");

            return;
        }

        if(EddieApplication.supportTools().isNetworkConnectionActive() == false)
        {
            EddieLogger.error("AirVPNTileService.startConnection(): Network connection is not available");

            return;
        }

        if(EddieApplication.supportTools().waitForManifest() == false)
        {
            EddieLogger.error("AirVPNTileService.startConnection(): AirVPN Manifest is not available");

            return;
        }

        vpnManager.setContext(this);

        if(airVPNUser.areCredendialsUsable() == true)
        {
            EddieLogger.info("Trying to login to AirVPN");

            for(i = 0; i < userWaitLoops && AirVPNUser.isUserValid() == false; i++)
            {
                try
                {
                    Thread.sleep(loopWaitMilliSeconds);
                }
                catch(Exception e)
                {
                }
            }
        }

        if(airVPNUser.isUserValid() == true)
        {
            EddieLogger.info("Starting AirVPN connection from quick settings tile");

            airVPNServerProvider.reset();

            airVPNServerProvider.setUserCountry(airVPNUser.getUserCountry());

            if(settingsManager.getAirVPNQuickConnectMode().equals(SettingsManager.AIRVPN_QUICK_CONNECT_MODE_AUTO))
            {
                airVPNServerProvider.setTlsMode(AirVPNServerProvider.TLSMode.TLS_CRYPT);

                airVPNServerProvider.setSupportIPv4(true);
                airVPNServerProvider.setSupportIPv6(false);
            }
            else
            {
                if(settingsManager.getAirVPNDefaultTLSMode().equals(SettingsManager.OPENVPN_TLS_MODE_CRYPT))
                    airVPNServerProvider.setTlsMode(AirVPNServerProvider.TLSMode.TLS_CRYPT);
                else
                    airVPNServerProvider.setTlsMode(AirVPNServerProvider.TLSMode.TLS_AUTH);

                switch(settingsManager.getAirVPNDefaultIPVersion())
                {
                    case SettingsManager.AIRVPN_IP_VERSION_4:
                    {
                        airVPNServerProvider.setSupportIPv4(true);
                        airVPNServerProvider.setSupportIPv6(false);
                    }
                    break;

                    case SettingsManager.AIRVPN_IP_VERSION_6:
                    case SettingsManager.AIRVPN_IP_VERSION_4_OVER_6:
                    {
                        airVPNServerProvider.setSupportIPv4(true);
                        airVPNServerProvider.setSupportIPv6(true);
                    }
                    break;

                    default:
                    {
                        airVPNServerProvider.setSupportIPv4(true);
                        airVPNServerProvider.setSupportIPv6(false);
                    }
                    break;
                }
            }

            airVPNServerList = airVPNServerProvider.getFilteredServerList();

            if(airVPNServerList.size() == 0)
            {
                EddieLogger.error("AirVPNTileService.startConnection(): Cannot start connection. Server list is empty.");

                return;
            }

            userKeyName = airVPNUser.getCurrentKey();

            server = airVPNServerList.get(0);

            if(airVPNServerProvider.getTlsMode().equals(AirVPNServerProvider.TLSMode.TLS_AUTH) || settingsManager.getAirVPNDefaultVPNType().equals(SettingsManager.VPN_TYPE_WIREGUARD))
            {
                tlsMode = SettingsManager.OPENVPN_TLS_MODE_AUTH;

                entry = 0;
            }
            else
            {
                tlsMode = SettingsManager.OPENVPN_TLS_MODE_CRYPT;

                entry = 2;
            }

            switch(settingsManager.getAirVPNDefaultVPNType())
            {
                case SettingsManager.VPN_TYPE_OPENVPN:
                {
                    protocol = settingsManager.getAirVPNDefaultOpenVPNProtocol();

                    port = settingsManager.getAirVPNDefaultOpenVPNPort();
                }
                break;

                case SettingsManager.VPN_TYPE_WIREGUARD:
                {
                    protocol = SettingsManager.AIRVPN_PROTOCOL_UDP;

                    port = settingsManager.getAirVPNDefaultWireGuardPort();
                }
                break;
            }

            switch(settingsManager.getAirVPNDefaultIPVersion())
            {
                case SettingsManager.AIRVPN_IP_VERSION_4:
                {
                    serverEntryIP = server.getEntryIPv4();

                    connectIPv6 = false;
                    mode6to4 = false;
                }
                break;

                case SettingsManager.AIRVPN_IP_VERSION_6:
                {
                    serverEntryIP = server.getEntryIPv6();

                    protocol += "6";

                    connectIPv6 = true;
                    mode6to4 = false;
                }
                break;

                case SettingsManager.AIRVPN_IP_VERSION_4_OVER_6:
                {
                    serverEntryIP = server.getEntryIPv4();

                    connectIPv6 = true;
                    mode6to4 = true;
                }
                break;

                default:
                {
                    serverEntryIP = server.getEntryIPv4();

                    connectIPv6 = false;
                    mode6to4 = false;
                }
                break;
            }

            try
            {
                switch(settingsManager.getAirVPNDefaultVPNType())
                {
                    case SettingsManager.VPN_TYPE_OPENVPN:
                    {
                        if(settingsManager.getAirVPNOpenVPNMode().equals(SettingsManager.AIRVPN_OPENVPN_MODE_DEFAULT) == false)
                        {
                            manifestMode = EddieApplication.airVPNManifest().getMode(settingsManager.getAirVPNOpenVPNMode());

                            port = manifestMode.getPort();
                            protocol = manifestMode.getProtocol();
                            entry = manifestMode.getEntryIndex();
                            tlsMode = manifestMode.getTlsMode();
                        }

                        vpnProfile = airVPNUser.getOpenVPNProfile(userKeyName, serverEntryIP.get(entry), port, protocol, tlsMode, settingsManager.getAirVPNOpenVPNCipher(), connectIPv6, mode6to4, false, "");
                    }
                    break;

                    case SettingsManager.VPN_TYPE_WIREGUARD:
                    {
                        allowedIPs = "";

                        if(connectIPv6 == false || mode6to4 == true)
                            allowedIPs = "0.0.0.0/0";

                        if(connectIPv6 == true || mode6to4 == true)
                        {
                            if(allowedIPs.isEmpty() == false)
                                allowedIPs += ", ";

                            allowedIPs += "::/0";
                        }

                        if(settingsManager.getAirVPNWireGuardMode().equals(SettingsManager.AIRVPN_WIREGUARD_MODE_DEFAULT) == false)
                        {
                            manifestMode = EddieApplication.airVPNManifest().getMode(settingsManager.getAirVPNWireGuardMode());

                            port = manifestMode.getPort();
                            entry = manifestMode.getEntryIndex();
                        }

                        vpnProfile = airVPNUser.getWireGuardProfile(userKeyName, serverEntryIP.get(entry), port, 0, 0, allowedIPs, 15, connectIPv6, mode6to4, false, "");
                    }
                    break;

                    default:
                    {
                        vpnProfile = "";
                    }
                }
            }
            catch(Exception e)
            {
                vpnProfile = "";
            }

            if(settingsManager.isAirVPNServerWhitelisted(server.getName()))
                EddieLogger.info(String.format(Locale.getDefault(), "Trying to quick connect favorite AirVPN server %s in %s, %s - %s, Protocol %s, Port %d", server.getName(), server.getLocation(), countryContinent.getCountryName(server.getCountryCode()), settingsManager.getAirVPNDefaultVPNType(), protocol, port));
            else
                EddieLogger.info(String.format(Locale.getDefault(), "Trying to quick connect AirVPN server %s in %s, %s - %s, Protocol %s, Port %d", server.getName(), server.getLocation(), countryContinent.getCountryName(server.getCountryCode()), settingsManager.getAirVPNDefaultVPNType(), protocol, port));

            if(manifestMode != null)
                EddieLogger.info(String.format(Locale.getDefault(), "Using %s manifest mode '%s'", manifestMode.getVpnType(), manifestMode.getTitle()));

            EddieLogger.info(String.format(Locale.getDefault(), "Using user key '%s'", userKeyName));

            if(vpnProfile.isEmpty())
            {
                EddieLogger.error("AirVPNTileService.startConnection(): vpnProfile is empty");

                return;
            }

            profileInfo = new HashMap<String, String>();

            profileInfo.put("mode", String.format(Locale.getDefault(), "%d", VPN.ConnectionMode.TILE_CONNECT.getValue()));
            profileInfo.put("vpn_type", settingsManager.getAirVPNDefaultVPNType());
            profileInfo.put("name", "tile_connect");
            profileInfo.put("profile", "quick_connect");
            profileInfo.put("status", "ok");
            profileInfo.put("description", AirVPNManifest.getFullServerDescription(server.getName()));
            profileInfo.put("server", serverEntryIP.get(entry));
            profileInfo.put("port", String.format(Locale.getDefault(), "%d", port));
            profileInfo.put("protocol", protocol);

            vpnManager.vpn().setType(VPN.Type.fromString(settingsManager.getAirVPNDefaultVPNType()));

            vpnManager.vpn().setProfileInfo(profileInfo);

            vpnManager.vpn().setConnectionMode(VPN.ConnectionMode.TILE_CONNECT);

            vpnManager.vpn().setConnectionModeDescription(getResources().getString(R.string.conn_type_quick_connect));

            vpnManager.vpn().setUserProfileDescription(userKeyName);

            vpnManager.vpn().setUserName(airVPNUser.getUserName());

            vpnManager.clearProfile();

            vpnManager.setProfile(vpnProfile);

            if(settingsManager.getAirVPNDefaultVPNType().equals(SettingsManager.VPN_TYPE_OPENVPN) == true)
            {
                profileString = settingsManager.getOvpn3CustomDirectives().trim();

                if(profileString.length() > 0)
                    vpnManager.addProfileString(profileString);
            }

            vpnManager.startConnection();
        }
        else
            EddieLogger.error("AirVPNTileService: Cannot start connection. User is not logged in.");
    }

    @TargetApi(Build.VERSION_CODES.N)
    public void setTileStatus()
    {
        String txt;
        Tile tile = getQsTile();

        if(tile == null)
            return;

        txt = EddieApplication.context().getResources().getString(R.string.airvpn);

        txt += ": ";

        if(EddieApplication.supportTools().isNetworkConnectionActive() == false)
        {
            txt += EddieApplication.context().getResources().getString(R.string.network_is_not_available);

            tile.setState(Tile.STATE_INACTIVE);
        }
        else if(EddieApplication.airVPNUser() == null)
        {
            txt += EddieApplication.context().getResources().getString(R.string.user_is_not_logged_in);

            tile.setState(Tile.STATE_INACTIVE);
        }
        else if(EddieApplication.airVPNUser().isUserValid() == false)
        {
            txt += EddieApplication.context().getResources().getString(R.string.user_is_not_logged_in);

            tile.setState(Tile.STATE_INACTIVE);
        }
        else if(vpnManager != null)
        {
            switch(vpnManager.vpn().getConnectionStatus())
            {
                case CONNECTING:
                {
                    tile.setState(Tile.STATE_ACTIVE);

                    txt += EddieApplication.context().getResources().getString(R.string.vpn_status_connecting);
                }
                break;

                case CONNECTED:
                {
                    txt += String.format(Locale.getDefault(), EddieApplication.context().getResources().getString(R.string.conn_status_connected), vpnManager.vpn().getServerDescription(false));

                    tile.setState(Tile.STATE_ACTIVE);
                }
                break;

                case DISCONNECTING:
                {
                    tile.setState(Tile.STATE_INACTIVE);

                    txt += EddieApplication.context().getResources().getString(R.string.vpn_status_disconnecting);
                }
                break;

                case NOT_CONNECTED:
                {
                    tile.setState(Tile.STATE_INACTIVE);

                    txt += EddieApplication.context().getResources().getString(R.string.vpn_status_not_connected);
                }
                break;

                case PAUSED_BY_SYSTEM:
                case PAUSED_BY_USER:
                {
                    tile.setState(Tile.STATE_INACTIVE);

                    txt += EddieApplication.context().getResources().getString(R.string.vpn_status_paused);
                }
                break;

                default:
                {
                    tile.setState(Tile.STATE_INACTIVE);

                    txt += EddieApplication.context().getResources().getString(R.string.conn_status_disconnected);
                }
                break;
            }
        }

        tile.setLabel(txt);

        tile.updateTile();
    }

    // Eddie events

    public void onVpnConnectionStatsChanged(final VPNConnectionStats connectionData)
    {
    }

    public void onVpnStatusChanged(VPN.Status status, String message)
    {
        setTileStatus();
    }

    public void onVpnAuthFailed(final VPNEvent oe)
    {
        setTileStatus();
    }

    public void onVpnError(final VPNEvent oe)
    {
        setTileStatus();
    }

    public void onVpnReconnect(final VPNEvent oe)
    {
        setTileStatus();
    }

    public void onMasterPasswordChanged()
    {
    }

    public void onAirVPNLogin()
    {
        setTileStatus();
    }

    public void onAirVPNLoginFailed(String message)
    {
        setTileStatus();
    }

    public void onAirVPNLogout()
    {
        setTileStatus();
    }

    public void onAirVPNUserDataChanged()
    {
        setTileStatus();
    }

    public void onAirVPNUserProfileReceived(Document document)
    {
        setTileStatus();
    }

    public void onAirVPNUserProfileDownloadError()
    {
    }

    public void onAirVPNUserProfileChanged()
    {
    }

    public void onAirVPNManifestReceived(Document document)
    {
    }

    public void onAirVPNManifestDownloadError()
    {
    }

    public void onAirVPNManifestChanged()
    {
    }

    public void onAirVPNIgnoredManifestDocumentRequest()
    {
    }

    public void onAirVPNIgnoredUserDocumentRequest()
    {
    }

    public void onAirVPNRequestError(String message)
    {
    }

    public void onCancelConnection()
    {
        setTileStatus();
    }

    public void onSaveState()
    {
    }

    public void onRestoreState(Bundle bundle)
    {
    }
    // NetworkStatusReceiver

    public void onNetworkStatusNotAvailable()
    {
    }

    public void onNetworkStatusConnected()
    {
    }

    public void onNetworkStatusIsConnecting()
    {
    }

    public void onNetworkStatusIsDisconnecting()
    {
    }

    public void onNetworkStatusSuspended()
    {
    }

    public void onNetworkStatusNotConnected()
    {
    }

    public void onNetworkTypeChanged()
    {
    }
}