// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 20 June 2018 - author: ProMIND - initial release. Based on revised code from com.eddie.android. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.os.Bundle;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class VPN
{
    private VPNConnectionStats vpnConnectionStats = null;
    private VPNTransportStats vpnTransportStats = null;
    private Type type = Type.UNKNOWN;
    private Status connectionStatus = Status.UNDEFINED;
    private ConnectionMode connectionMode = ConnectionMode.UNKNOWN;
    private long inRate = 0;
    private long outRate = 0;
    private long maxInRate = 0;
    private long maxOutRate = 0;
    private long sessionTimeSeconds = 0;
    private long totalConnectionTimeSeconds = 0;
    private HashMap<String, String> profileInfo = null;
    private HashMap<String, String> pendingProfileInfo = null;
    private String vpnProfile = "";
    private String pendingVpnProfile = "";
    private String pendingProgressMessage = "";
    private String connectionModeDescription = "";
    private String userProfileDescription = "";
    private String userName = "";
    private ArrayList<IPAddress> dnsEntry = null;

    private static final String VPN_STATE_TYPE = "VPN_Type";
    private static final String VPN_STATE_STATUS = "VPN_Status";
    private static final String VPN_STATE_CONNECTION_MODE = "VPN_ConnectionMode";
    private static final String VPN_STATE_CONNECTION_STATS = "VPN_ConnectionStats";
    private static final String VPN_STATE_PROFILE = "VPN_Profile";
    private static final String VPN_STATE_PENDING_PROFILE = "VPN_PendingProfile";
    private static final String VPN_STATE_PENDING_PROGRESS_MESSAGE = "VPN_PendingProgressMessage";
    private static final String VPN_STATE_CONNECTION_MODE_DESCRIPTION = "VPN_ConnectionModeDescription";
    private static final String VPN_STATE_USER_PROFILE_DESCRIPTION = "VPN_UserProfileDescription";
    private static final String VPN_STATE_USER_NAME = "VPN_UserName";
    private static final String VPN_STATE_DNS = "VPN_DNS";

    private static final String SEPARATOR = "#!#";

    public enum Type
    {
        UNKNOWN,
        OPENVPN,
        WIREGUARD;

        @Override
        public String toString()
        {
            String val = "";

            switch(this)
            {
                case OPENVPN:
                {
                    val = "OpenVPN";
                }
                break;

                case WIREGUARD:
                {
                    val = "WireGuard";
                }
                break;

                case UNKNOWN:
                {
                    val = "Unknown";
                }
                break;

                default:
                {
                    val = "Unknown";
                }
                break;
            }

            return val;
        }

        public static Type fromString(String s)
        {
            Type t = UNKNOWN;

            if(s == null)
                return UNKNOWN;

            s = s.toLowerCase(Locale.US);

            switch(s)
            {
                case "openvpn":
                {
                    t = OPENVPN;
                }
                break;

                case "wireguard":
                {
                    t = WIREGUARD;
                }
                break;

                default:
                {
                    t = UNKNOWN;
                }
                break;
            }

            return t;
        }
    }

    public enum Status
    {
        UNDEFINED(-1),
        RESERVED(1),
        CONNECTING(2),
        CONNECTED(3),
        DISCONNECTING(4),
        NOT_CONNECTED(5),
        PAUSED_BY_USER(6),
        PAUSED_BY_SYSTEM(7),
        LOCKED(8),
        CONNECTION_REVOKED_BY_SYSTEM(9),
        CONNECTION_CANCELED(10),
        CONNECTION_ERROR(11);

        private int value = 0;
        private static HashMap statusMap = new HashMap<>();

        Status(int v)
        {
            value = v;
        }

        static
        {
            for(Status status : Status.values())
                statusMap.put(status.value, status);
        }

        public int getValue()
        {
            return value;
        }

        public static Status fromValue(int v)
        {
            Status s = (Status)statusMap.get(v);

            if(s == null)
                s = UNDEFINED;

            return s;
        }

        public String getDescription()
        {
            String s = "";

            switch(value)
            {
                case -1:
                {
                    s = "Undefined";
                }
                break;

                case 1:
                {
                    s = "Reserved";
                }
                break;

                case 2:
                {
                    s = "Connecting";
                }
                break;

                case 3:
                {
                    s = "Connected";
                }
                break;

                case 4:
                {
                    s = "Disconnecting";
                }
                break;

                case 5:
                {
                    s = "Not Connected";
                }
                break;

                case 6:
                {
                    s = "Paused by User";
                }
                break;

                case 7:
                {
                    s = "Paused by System";
                }
                break;

                case 8:
                {
                    s = "Locked";
                }
                break;

                case 9:
                {
                    s = "Connection Revoked by System";
                }
                break;

                case 10:
                {
                    s = "Connection Canceled";
                }
                break;

                case 11:
                {
                    s = "Connection Error";
                }
                break;

                default:
                {
                    s = "Unknown";
                }
                break;
            }

            return s;
        }
    }

    public enum ConnectionMode
    {
        UNKNOWN(-1),
        QUICK_CONNECT(1),
        AIRVPN_SERVER(2),
        OPENVPN_PROFILE(3),
        WIREGUARD_PROFILE(4),
        BOOT_CONNECT(5),
        TILE_CONNECT(6);

        private int value = 0;
        private static HashMap modeMap = new HashMap<>();

        ConnectionMode(int v)
        {
            value = v;
        }

        static
        {
            for(ConnectionMode mode : ConnectionMode.values())
                modeMap.put(mode.value, mode);
        }

        public int getValue()
        {
            return value;
        }

        public static ConnectionMode fromValue(int v)
        {
            ConnectionMode c = (ConnectionMode)modeMap.get(v);

            if(c == null)
                c = UNKNOWN;

            return c;
        }
    }

    public VPN()
    {
        vpnConnectionStats = null;
        vpnTransportStats = null;
        type = Type.UNKNOWN;
        connectionStatus = Status.UNDEFINED;
        connectionMode = ConnectionMode.UNKNOWN;
        inRate = 0;
        outRate = 0;
        maxInRate = 0;
        maxOutRate = 0;
        sessionTimeSeconds = 0;
        totalConnectionTimeSeconds = 0;
        profileInfo = null;
        pendingProfileInfo = null;
        vpnProfile = "";
        pendingVpnProfile = "";
        pendingProgressMessage = "";
        connectionModeDescription = "";
        userProfileDescription = "";
        userName = "";

        dnsEntry = new ArrayList<IPAddress>();

        dnsEntry.clear();
    }

    public String connectionStatusDescription()
    {
        return connectionStatusDescription(connectionStatus);
    }

    public String connectionStatusDescription(Status s)
    {
        return EddieApplication.context().getResources().getString(connectionStatusResourceDescription(s));
    }

    public int connectionStatusResourceDescription(Status s)
    {
        int res = 0;

        switch(s)
        {
            case CONNECTION_ERROR:
            {
                res = R.string.vpn_status_connection_error;
            }
            break;

            case NOT_CONNECTED:
            {
                res = R.string.vpn_status_not_connected;
            }
            break;

            case DISCONNECTING:
            {
                res = R.string.vpn_status_disconnecting;
            }
            break;

            case CONNECTING:
            {
                res = R.string.vpn_status_connecting;
            }
            break;

            case CONNECTED:
            {
                res = R.string.vpn_status_connected;
            }
            break;

            case PAUSED_BY_USER:
            case PAUSED_BY_SYSTEM:
            {
                res = R.string.vpn_status_paused;
            }
            break;

            case LOCKED:
            {
                res = R.string.vpn_status_locked;
            }
            break;

            case CONNECTION_REVOKED_BY_SYSTEM:
            {
                res = R.string.vpn_status_revoked;
            }
            break;

            case CONNECTION_CANCELED:
            {
                res = R.string.vpn_status_canceled;
            }
            break;

            default:
            {
                res = R.string.vpn_status_not_connected;
            }
            break;
        }

        return res;
    }

    public void clearDnsEntries()
    {
        dnsEntry.clear();
    }

    public void addDnsEntry(IPAddress dns)
    {
        if(dns == null || dnsEntry == null)
            return;

        dnsEntry.add(dns);
    }

    public void addDnsEntry(String ipAddress, IPAddress.IPFamily ipFamily)
    {
        IPAddress dns = new IPAddress(ipAddress, ipFamily);

        addDnsEntry(dns);
    }

    public ArrayList<IPAddress> getDns()
    {
        return dnsEntry;
    }

    public void setType(Type t)
    {
        type = t;
    }

    public Type getType()
    {
        return type;
    }

    public void setProfileInfo(HashMap<String, String> p)
    {
        profileInfo = p;
    }

    public HashMap<String, String> getProfileInfo()
    {
        return profileInfo;
    }

    public void setVpnProfile(String p)
    {
        vpnProfile = p;
    }

    public String getVpnProfile()
    {
        return vpnProfile;
    }

    public void setPendingProfileInfo(HashMap<String, String> p)
    {
        pendingProfileInfo = p;
    }

    public HashMap<String, String> getPendingProfileInfo()
    {
        return pendingProfileInfo;
    }

    public void setPendingVpnProfile(String p)
    {
        pendingVpnProfile = p;
    }

    public String getPendingVpnProfile()
    {
        return pendingVpnProfile;
    }

    public void setPendingProgressMessage(String m)
    {
        pendingProgressMessage = m;
    }

    public String getPendingProgressMessage()
    {
        return pendingProgressMessage;
    }

    public void setVpnConnectionStats(VPNConnectionStats stats)
    {
        vpnConnectionStats = stats;
    }

    public VPNConnectionStats getVpnConnectionStats()
    {
        return vpnConnectionStats;
    }

    public void setVpnTransportStats(VPNTransportStats stats)
    {
        vpnTransportStats = stats;
    }

    public VPNTransportStats getVpnTransportStats()
    {
        return vpnTransportStats;
    }

    public void setInRate(long v)
    {
        inRate = v;
    }

    public long getInRate()
    {
        return inRate;
    }

    public void setOutRate(long v)
    {
        outRate = v;
    }

    public long getOutRate()
    {
        return outRate;
    }

    public void setMaxInRate(long v)
    {
        maxInRate = v;
    }

    public long getMaxInRate()
    {
        return maxInRate;
    }

    public void setMaxOutRate(long v)
    {
        maxOutRate = v;
    }

    public long getMaxOutRate()
    {
        return maxOutRate;
    }

    public void setConnectionStatus(Status s)
    {
        connectionStatus = s;
    }

    public Status getConnectionStatus()
    {
        return connectionStatus;
    }

    public void setConnectionMode(ConnectionMode m)
    {
        connectionMode = m;
    }

    public ConnectionMode getConnectionMode()
    {
        return connectionMode;
    }

    public void resetTotalConnectionTime()
    {
        totalConnectionTimeSeconds = 0;
    }

    public void resetSessionTime()
    {
        sessionTimeSeconds = 0;
    }

    public void addSecondsConnectionTime(long s)
    {
        sessionTimeSeconds += s;

        totalConnectionTimeSeconds += s;
    }

    public long getSessionTimeSeconds()
    {
        return sessionTimeSeconds;
    }

    public long getTotalConnectionTimeSeconds()
    {
        return totalConnectionTimeSeconds;
    }

    public String getFormattedSessionTime()
    {
        return getFormattedTime(sessionTimeSeconds);
    }

    public String getFormattedTotalConnectionTime()
    {
        return getFormattedTime(totalConnectionTimeSeconds);
    }

    public void setConnectionModeDescription(String d)
    {
        connectionModeDescription = d;
    }

    public String getConnectionModeDescription()
    {
        return connectionModeDescription;
    }

    public void setUserProfileDescription(String p)
    {
        userProfileDescription = p;
    }

    public String getUserProfileDescription()
    {
        return userProfileDescription;
    }

    public void setUserName(String u)
    {
        userName = u;
    }

    public String getUserName()
    {
        return userName;
    }

    public Bundle getState()
    {
        return appendState(new Bundle());
    }

    public Bundle appendState(Bundle bundle)
    {
        JSONObject jsonObject = null;
        String dnsList = "";

        if(bundle == null)
        {
            EddieLogger.error("VPN.getState(): bundle is null");

            return null;
        }

        bundle.putString(VPN_STATE_TYPE, type.toString());
        bundle.putInt(VPN_STATE_STATUS, connectionStatus.getValue());
        bundle.putInt(VPN_STATE_CONNECTION_MODE, connectionMode.getValue());
        bundle.putString(VPN_STATE_CONNECTION_STATS, vpnConnectionStats != null ? vpnConnectionStats.toString() : "");
        bundle.putString(VPN_STATE_PROFILE, vpnProfile);
        bundle.putString(VPN_STATE_PENDING_PROFILE, pendingVpnProfile);
        bundle.putString(VPN_STATE_PENDING_PROGRESS_MESSAGE, pendingProgressMessage);
        bundle.putString(VPN_STATE_CONNECTION_MODE_DESCRIPTION, connectionModeDescription);
        bundle.putString(VPN_STATE_USER_PROFILE_DESCRIPTION, userProfileDescription);
        bundle.putString(VPN_STATE_USER_NAME, userName);

        if(profileInfo != null)
        {
            jsonObject = new JSONObject(profileInfo);

            bundle.putString("VPN_ProfileInfo", jsonObject.toString());
        }

        if(pendingProfileInfo != null)
        {
            jsonObject = new JSONObject(pendingProfileInfo);

            bundle.putString("VPN_PendingProfileInfo", jsonObject.toString());
        }

        if(dnsEntry != null)
        {
            for(IPAddress ipAddress : dnsEntry)
            {
                if(dnsList.isEmpty() == false)
                    dnsList += SEPARATOR;

                dnsList += ipAddress.toString();
            }
        }

        bundle.putString(VPN_STATE_DNS, dnsList);

        return bundle;
    }

    public void setState(Bundle bundle)
    {
        JSONObject jsonObject = null;
        Iterator<String> jsonKeys = null;
        List<String> item = null;
        String key, dnsList;

        if(bundle == null)
        {
            EddieLogger.error("VPN.setState(): bundle is null");

            return;
        }

        type = VPN.Type.fromString(bundle.getString(VPN_STATE_TYPE, Type.UNKNOWN.toString()));
        connectionStatus = VPN.Status.fromValue(bundle.getInt(VPN_STATE_STATUS, Status.UNDEFINED.value));
        connectionMode = VPN.ConnectionMode.fromValue(bundle.getInt(VPN_STATE_CONNECTION_MODE, ConnectionMode.UNKNOWN.value));
        vpnConnectionStats = new VPNConnectionStats(bundle.getString(VPN_STATE_CONNECTION_STATS, ""));

        vpnProfile = bundle.getString(VPN_STATE_PROFILE, "");
        pendingVpnProfile = bundle.getString(VPN_STATE_PENDING_PROFILE, "");
        pendingProgressMessage = bundle.getString(VPN_STATE_PENDING_PROGRESS_MESSAGE, "");
        connectionModeDescription = bundle.getString(VPN_STATE_CONNECTION_MODE_DESCRIPTION, "");
        userProfileDescription = bundle.getString(VPN_STATE_USER_PROFILE_DESCRIPTION, "");
        userName = bundle.getString(VPN_STATE_USER_NAME, "");

        try
        {
            jsonObject = new JSONObject(bundle.getString("VPN_ProfileInfo"));

            profileInfo = new HashMap<String, String>();

            jsonKeys = jsonObject.keys();

            while(jsonKeys.hasNext())
            {
                key = (String)jsonKeys.next();

                profileInfo.put(key, jsonObject.get(key).toString());
            }
        }
        catch(Exception e)
        {
            profileInfo = null;
        }

        try
        {
            jsonObject = new JSONObject(bundle.getString("VPN_PendingProfileInfo"));

            pendingProfileInfo = new HashMap<String, String>();

            jsonKeys = jsonObject.keys();

            while(jsonKeys.hasNext())
            {
                key = (String)jsonKeys.next();

                pendingProfileInfo.put(key, jsonObject.get(key).toString());
            }
        }
        catch(Exception e)
        {
            pendingProfileInfo = null;
        }

        dnsList = bundle.getString(VPN_STATE_DNS, "");

        if(dnsList != null)
        {
            clearDnsEntries();

            if(dnsList.contains(SEPARATOR))
                item = Arrays.asList(dnsList.split(SEPARATOR));
            else if(dnsList.isEmpty() == false)
            {
                item = new ArrayList<String>();

                item.add(dnsList);
            }
            else
                item = null;

            if(item != null)
            {
                for(String s : item)
                {
                    IPAddress ipAddress = new IPAddress(s);

                    if(ipAddress != null)
                        dnsEntry.add(ipAddress);
                }
            }
        }
    }

    public String getServerDescription()
    {
        return getServerDescription(true);
    }

    public String getServerDescription(boolean addIp)
    {
        String serverDescription = "";
        AirVPNServer server = null;

        if(profileInfo == null || profileInfo.get("server") == null || profileInfo.get("description") == null)
            return "";

        server = EddieApplication.airVPNManifest().getServerByIP(profileInfo.get("server"));

        if(server != null)
        {
            serverDescription = String.format("AirVPN %s", EddieApplication.airVPNManifest().getFullServerDescription(server.getName()));

            if(addIp == true)
                serverDescription += " (" + profileInfo.get("server") + ")";
        }
        else if(!profileInfo.get("description").isEmpty())
            serverDescription = String.format("%s (%s)", profileInfo.get("description"), profileInfo.get("server"));
        else
        {
            serverDescription = profileInfo.get("server");

            if(EddieApplication.vpnManager().vpn().getConnectionMode() == VPN.ConnectionMode.OPENVPN_PROFILE)
                serverDescription += " (" + EddieApplication.context().getResources().getString(R.string.conn_type_openvpn_profile) + ")";
            else if(EddieApplication.vpnManager().vpn().getConnectionMode() == VPN.ConnectionMode.WIREGUARD_PROFILE)
                serverDescription += " (" + EddieApplication.context().getResources().getString(R.string.conn_type_wireguard_profile) + ")";
        }

        return serverDescription;
    }

    public String getFormattedStats()
    {
        String finalStats = "";

        VPNTransportStats vpnStats = getVpnTransportStats();

        if(vpnStats == null)
            return "";

        finalStats = EddieApplication.applicationContext().getResources().getString(R.string.stats_in_cap) + " " + SupportTools.formatDataVolume(vpnStats.bytesIn) + System.getProperty("line.separator");

        finalStats += EddieApplication.applicationContext().getResources().getString(R.string.stats_out_cap) + " " + SupportTools.formatDataVolume(vpnStats.bytesOut) + System.getProperty("line.separator");

        finalStats += EddieApplication.applicationContext().getResources().getString(R.string.stats_max_in_cap) + " " + SupportTools.formatTransferRate(getMaxInRate()) + System.getProperty("line.separator");

        finalStats += EddieApplication.applicationContext().getResources().getString(R.string.stats_max_out_cap) + " " + SupportTools.formatTransferRate(getMaxOutRate()) + System.getProperty("line.separator");

        finalStats += EddieApplication.applicationContext().getResources().getString(R.string.stats_session_time_cap) + " " + getFormattedTotalConnectionTime();

        return finalStats;
    }

    private String getFormattedTime(long s)
    {
        long hours, minutes, seconds;

        hours = s / 3600;
        minutes = (s % 3600) / 60;
        seconds = s % 60;

        return String.format(Locale.getDefault(), "%02d:%02d:%02d", hours, minutes, seconds);
    }
}
