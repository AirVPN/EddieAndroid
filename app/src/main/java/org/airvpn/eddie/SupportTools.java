// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 20 June 2018 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.UiModeManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.InetAddresses;
import android.net.LinkAddress;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.Manifest;
import android.provider.OpenableColumns;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.math.BigInteger;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.SecureRandom;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import static android.content.Context.ACCESSIBILITY_SERVICE;
import static android.content.Context.UI_MODE_SERVICE;

public class SupportTools
{
    public enum EditOptionType
    {
        TEXT,
        PASSWORD,
        NUMERIC,
        IP_PORT,
        IP_ADDRESS_LIST
    }

    public enum EditOption
    {
        ALLOW_EMPTY_FIELD,
        ALLOW_ZERO_VALUE,
        DO_NOT_ALLOW_EMPTY_FIELD,
        DO_NOT_ALLOW_ZERO_VALUE
    }

    public enum ShowMode
    {
        ENABLED,
        DISABLED,
        HIDDEN
    }

    public static class SortableListViewItem
    {
        String code;
        String description;
    }

    public enum AirVpnDocumentRequest
    {
        MANIFEST_DOCUMENT_SUCCESS,
        MANIFEST_DOCUMENT_FAILED,
        MANIFEST_DOCUMENT_IGNORED,
        USER_DOCUMENT_SUCCESS,
        USER_DOCUMENT_FAILED,
        USER_DOCUMENT_IGNORED,
        ERROR
    };

    private static ProgressDialog progressDialog = null;
    private static ProgressDialog connectionProgressDialog = null;
    private boolean dialogConfirm = false;
    private EddieEvent eddieEvent = null;
    private SettingsManager settingsManager = null;
    private String dialogReturnStringValue = "", resolveHostname = "";
    private boolean dialogReturnBooleanValue = false;
    private static boolean  manifestRequestPending = false, userRequestPending = false;
    private static boolean bootServerIPv6Mode = true;
    private InetAddress inetAddress = null;

    private SortableListViewArrayAdapter sortableListViewArrayAdapter = null;
    private ListView sortableListView = null;
    private TextView txtSortableListviewAction = null;
    private int sortableListViewSelectedPosition = -1;

    private AlertDialog.Builder dialogBuilder = null;
    private AlertDialog settingDialog = null;
    private String[] dialogValues;
    private Button btnOk = null;
    private Button btnCancel = null;
    private EditText edtKey = null;

    private File shareFile = null;

    public static final int HTTP_CONNECTION_TIMEOUT = 15000;    // 15 seconds
    public static final int HTTP_READ_TIMEOUT = 15000;          // 30 seconds

    public static final String DEVICE_PLATFORM = "Android";
    public static final String AIRVPN_SERVER_DOCUMENT_VERSION = "293";

    public static final String AIRVPN_ACTION_REQUEST_MANIFEST = "manifest";
    public static final String AIRVPN_ACTION_REQUEST_USER = "user";

    public static final long ONE_DECIMAL_KILO = 1000;
    public static final long ONE_DECIMAL_MEGA = 1000000;
    public static final long ONE_DECIMAL_GIGA = 1000000000;
    public static final long ONE_KILOBYTE = 1024; // Real KB 2^10
    public static final long ONE_MEGABYTE = 1048576; // Real MB 2^20
    public static final long ONE_GIGABYTE = 1073741824; // Real GB 2^30

    public static final String BOOT_PROFILE_TYPE_LAST_ACTIVE_CONNECTION = "last_active_connection";
    public static final String BOOT_PROFILE_TYPE_DEFAULT_VPN_PROFILE = "default_vpn_boot_profile";

    private static final String STATUS_FILE_NAME = "status";
    private static byte[] privateIvBytes = {25, -64, -107, 79, -27, 82, -71, 116, 37, 40, 12, 2, 96, -121, 49, -15};
    private IvParameterSpec privateIvParameterSpec = null;

    private final int defaultNetworkWaitSeconds = 120;
    private final int defaultManifestWaitSeconds = 20;
    private final int loopWaitMilliSeconds = 2000;

    public SupportTools()
    {
        eddieEvent = EddieApplication.eddieEvent();

        settingsManager = EddieApplication.settingsManager();

        bootServerIPv6Mode = false;

        shareFile = null;

        privateIvParameterSpec = new IvParameterSpec(privateIvBytes);
    }

    public static void setBootServerIPv6Mode(boolean enable)
    {
        bootServerIPv6Mode = enable;
    }

    public HashMap<String, String> getVPNProfile(Uri profileUri)
    {
        HashMap<String, String> result = new HashMap<String, String>();
        InputStream inputStream = null;
        Reader reader = null;
        BufferedReader bufferedReader = null;
        ContentResolver contentResolver = null;
        Cursor uriCursor = null;
        String line = "", profile = "", fileName = "";
        String[] item = null;
        int MAX_FILE_SIZE = 200000;
        long fileSize = 0;

        result.put("description", "");

        if(profileUri == null)
        {
            result.put("status", "invalid");

            return result;
        }

        contentResolver = EddieApplication.context().getContentResolver();

        try
        {
            uriCursor = contentResolver.query(profileUri, null, null, null, null);
        }
        catch(RuntimeException e)
        {
            uriCursor = null;
        }

        if(uriCursor != null)
        {
            try
            {
                if(uriCursor.moveToFirst())
                {
                    int columnIndex = 0;

                    columnIndex = uriCursor.getColumnIndex(OpenableColumns.SIZE);

                    if(columnIndex >= 0)
                    {
                        fileSize = uriCursor.getLong(columnIndex);

                        columnIndex = uriCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);

                        if(columnIndex >= 0)
                            fileName = uriCursor.getString(columnIndex);
                        else
                            fileName = "";
                    }
                    else
                        fileSize = 0;
                }
                else
                    fileSize = 0;
            }
            catch(Exception e)
            {
                fileSize = 0;
            }
            finally
            {
                if(uriCursor != null)
                    uriCursor.close();
            }

            if(fileSize == 0 || fileSize > MAX_FILE_SIZE)
            {
                result.put("status", "invalid");

                return result;
            }
        }
        else
            fileName = profileUri.getLastPathSegment();

        try
        {
            inputStream = contentResolver.openInputStream(profileUri);
        }
        catch(FileNotFoundException e)
        {
            result.put("status", "not_found");

            return result;
        }
        catch(SecurityException e)
        {
            result.put("status", "no_permission");

            return result;
        }

        try
        {
            reader = new InputStreamReader(inputStream, "UTF-8");

            bufferedReader = new BufferedReader(reader);

            while((line = bufferedReader.readLine()) != null)
            {
                line = line.trim();

                profile += line + "\n";

                if(line.isEmpty() == false)
                {
                    if(line.indexOf('=') > -1)   // Possible WireGuard specification
                        line = line.replace('=', ' ');

                    line = line.replaceAll(" +", " ");
                    line = line.replaceAll("\t+", " ");

                    item = line.split(" ");

                    item[0] = item[0].toLowerCase(Locale.US);

                    if(item[0].equals("remote"))
                    {
                        if(result.containsKey("server") == false && item.length > 1)
                            result.put("server", item[1]);

                        if(result.containsKey("port") == false && item.length > 2)
                            result.put("port", item[2]);

                        if(result.containsKey("proto") == false && item.length > 3)
                            result.put("protocol", item[3]);

                        result.put("vpn_type", VPN.Type.OPENVPN.toString());
                    }
                    else if(item[0].equals("proto"))
                    {
                        if(result.containsKey("protocol") == false && item.length > 1)
                            result.put("protocol", item[1]);

                        result.put("vpn_type", VPN.Type.OPENVPN.toString());
                    }
                    else if(item[0].equals("port"))
                    {
                        if(result.containsKey("port") == false && item.length > 1)
                            result.put("port", item[1]);

                        result.put("vpn_type", VPN.Type.OPENVPN.toString());
                    }
                    else if(item[0].equals("endpoint"))
                    {
                        String wgAddress = "", wgPort = "";
                        int sep;

                        if(item[1].length() > 0)
                        {
                            sep = item[1].lastIndexOf(':');

                            if(sep < 0)
                            {
                                wgAddress = item[1];
                                wgPort = "";
                            }
                            else
                            {
                                wgAddress = item[1].substring(0, sep);
                                wgPort = item[1].substring(sep + 1);

                                if(wgAddress.indexOf(':') > -1)
                                {
                                    // Cleanup IPv6 address

                                    wgAddress = wgAddress.replace('[', ' ');
                                    wgAddress = wgAddress.replace(']', ' ');

                                    result.put("protocol", "UDP6");
                                }
                                else
                                    result.put("protocol", "UDP");
                            }

                            if(result.containsKey("server") == false)
                                result.put("server", wgAddress);

                            if(result.containsKey("port") == false)
                                result.put("port", wgPort);
                        }

                        result.put("vpn_type", VPN.Type.WIREGUARD.toString());
                    }
                }
            }

            if(bufferedReader != null)
            {
                try
                {
                    bufferedReader.close();
                }
                catch(Throwable t)
                {
                }
            }

            if(reader != null)
            {
                try
                {
                    reader.close();
                }
                catch(Throwable t)
                {
                }
            }

            if(inputStream != null)
            {
                try
                {
                    inputStream.close();
                }
                catch(Throwable t)
                {
                }
            }
        }
        catch(IOException e)
        {
        }

        if(result.containsKey("server") && result.containsKey("port") && result.containsKey("protocol"))
        {
            result.put("name", fileName);
            result.put("profile", profile);
            result.put("status", "ok");
        }
        else
            result.put("status", "invalid");

        return result;
    }

    public void setLocale(Context context)
    {
        String localeCode = settingsManager.getSystemApplicationLanguage();
        Locale locale = null;

        if(context == null)
            return;

        if(!localeCode.isEmpty())
        {
            String language[] = localeCode.split("_");

            if(language.length == 2)
                locale = new Locale(language[0], language[1]);
            else
                locale = new Locale(language[0]);
        }
        else
            locale = EddieApplication.deviceLocale();

        Locale.setDefault(locale);

        setContextLocale(context, locale);

        setContextLocale(EddieApplication.baseContext(), locale);

        setContextLocale(EddieApplication.applicationContext(), locale);
    }

    private void setContextLocale(Context context, Locale locale)
    {
        Configuration configuration = null;
        Resources resources = null;

        resources = context.getResources();

        configuration = resources.getConfiguration();

        configuration.setLocale(locale);

        context.createConfigurationContext(configuration);

        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
    }

    public static boolean isDeviceRooted()
    {
        boolean isTestOS = false, isSuPresent = false;

        isTestOS = android.os.Build.TAGS != null && android.os.Build.TAGS.contains("test-keys");

        String[] suPath = {"/sbin/su",
                           "/system/bin/su",
                           "/system/xbin/su",
                           "/data/local/xbin/su",
                           "/data/local/bin/su",
                           "/system/sd/xbin/su",
                           "/system/bin/failsafe/su",
                           "/data/local/su",
                           "/su/bin/su"};

        for(String path : suPath)
        {
            if(new File(path).exists())
                isSuPresent = true;
        }

        return isTestOS | isSuPresent;
    }

    public InetAddress resolveHostName(String hostname)
    {
        Thread resolveThread = null;

        resolveHostname = hostname;

        resolveThread = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    inetAddress = InetAddress.getByName(resolveHostname);
                }
                catch(Exception e)
                {
                    inetAddress = null;
                }
            }
        });

        resolveThread.start();

        try
        {
            if(resolveThread.isAlive())
                resolveThread.join();
        }
        catch(Exception e)
        {
            inetAddress = null;
        }

        return inetAddress;
    }

    public void showProgressDialog(int resource)
    {
        showProgressDialog(EddieApplication.mainActivity(), EddieApplication.context().getResources().getString(resource));
    }

    public void showProgressDialog(Context context, int resource)
    {
        showProgressDialog(context, context.getResources().getString(resource));
    }

    public void showProgressDialog(String message)
    {
        showProgressDialog(EddieApplication.mainActivity(), message);
    }

    public void showProgressDialog(Context context, String message)
    {
        if(context == null)
            return;

        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                if(progressDialog != null && progressDialog.isShowing())
                {
                    setProgressDialogMessage(message);

                    return;
                }

                progressDialog = new ProgressDialog(context);
                progressDialog.setMessage(message);
                progressDialog.setCancelable(false);

                if(context instanceof Activity)
                {
                    if(((Activity)EddieApplication.mainActivity()).isFinishing())
                        return;
                }

                progressDialog.show();
            }
        };

        runOnUiActivity(EddieApplication.mainActivity(), runnable);
    }

    public void setProgressDialogMessage(int resource)
    {
        setProgressDialogMessage(EddieApplication.mainActivity().getResources().getString(resource));
    }

    public void setProgressDialogMessage(String message)
    {
        if(progressDialog == null || !progressDialog.isShowing())
        {
            showProgressDialog(message);

            return;
        }

        progressDialog.setMessage(message);
    }

    public void dismissProgressDialog()
    {
        try
        {
            if(progressDialog != null)
            {
                if(progressDialog.isShowing())
                    progressDialog.dismiss();

                progressDialog = null;
            }
        }
        catch(Exception e)
        {
        }
    }

    public boolean isProgressDialogShown()
    {
        return (progressDialog != null && progressDialog.isShowing());
    }

    public void showConnectionProgressDialog(String message)
    {
        showConnectionProgressDialog(EddieApplication.mainActivity(), message);
    }

    public void showConnectionProgressDialog(Context context, String message)
    {
        if(context == null)
            return;

        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                if(connectionProgressDialog != null && connectionProgressDialog.isShowing())
                {
                    setConnectionProgressDialogMessage(message);

                    return;
                }

                connectionProgressDialog = new ProgressDialog(context);

                if(connectionProgressDialog == null)
                {
                    EddieLogger.error("SupportTools.showConnectionProgressDialog(): cannot create dialog");

                    return;
                }

                connectionProgressDialog.setMessage(message);
                connectionProgressDialog.setCancelable(false);

                connectionProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, context.getString(R.string.cancel), new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        eddieEvent.onCancelConnection();

                        dismissConnectionProgressDialog();
                    }
                });

                connectionProgressDialog.show();
            }
        };

        runOnUiActivity(EddieApplication.mainActivity(), runnable);
    }

    public void setConnectionProgressDialogMessage(int resource)
    {
        setConnectionProgressDialogMessage(EddieApplication.mainActivity().getResources().getString(resource));
    }

    public void setConnectionProgressDialogMessage(String message)
    {
        if(connectionProgressDialog == null || !connectionProgressDialog.isShowing())
        {
            showConnectionProgressDialog(message);

            return;
        }

        connectionProgressDialog.setMessage(message);
    }

    public void dismissConnectionProgressDialog()
    {
        try
        {
            if(connectionProgressDialog != null && connectionProgressDialog.isShowing())
                connectionProgressDialog.dismiss();
        }
        catch(Exception e)
        {
        }

        connectionProgressDialog = null;
    }

    public boolean isConnectionProgressDialogShown()
    {
        return (connectionProgressDialog != null && connectionProgressDialog.isShowing());
    }

    public void infoDialog(int resource, boolean HighPriority)
    {
        infoDialog(EddieApplication.mainActivity().getResources().getString(resource), HighPriority);
    }

    public void infoDialog(Context context, int resource, boolean HighPriority)
    {
        infoDialog(context, context.getResources().getString(resource), HighPriority);
    }

    public void infoDialog(String message, boolean HighPriority)
    {
        infoDialog(EddieApplication.mainActivity(), message, HighPriority);
    }

    public void infoDialog(Context context, String message, boolean HighPriority)
    {
        if(EddieApplication.isVisible() == false || context == null)
            return;

        if(HighPriority == false && settingsManager.areMessageDialogsEnabled() == false)
            return;

        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

                if(dialogBuilder == null)
                {
                    EddieLogger.error("SupportTools.infoDialog(): cannot create dialogBuilder");

                    return;
                }

                final AlertDialog infoDialog = dialogBuilder.create();

                if(infoDialog == null)
                {
                    EddieLogger.error("SupportTools.infoDialog(): cannot create dialog");

                    return;
                }

                infoDialog.setTitle(context.getResources().getString(R.string.eddie));

                infoDialog.setIcon(android.R.drawable.ic_dialog_info);

                infoDialog.setMessage(message);

                infoDialog.setButton(AlertDialog.BUTTON_POSITIVE, context.getResources().getString(R.string.ok), new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        try
                        {
                            infoDialog.dismiss();
                        }
                        catch(Exception e)
                        {
                        }
                    }
                });

                if(context instanceof Activity)
                {
                    if(((Activity)context).isFinishing())
                        return;
                }

                infoDialog.show();
            }
        };

        runOnUiActivity(EddieApplication.mainActivity(), runnable);
    }

    public void infoDialogHtml(String message, boolean HighPriority)
    {
        infoDialogHtml(EddieApplication.mainActivity(), Html.fromHtml(message), HighPriority);
    }

    public void infoDialogHtml(Spanned message, boolean HighPriority)
    {
        infoDialogHtml(EddieApplication.mainActivity(), message, HighPriority);
    }

    public void infoDialogHtml(Context context, Spanned message, boolean HighPriority)
    {
        if(EddieApplication.isVisible() == false || context == null)
            return;

        if(HighPriority == false && settingsManager.areMessageDialogsEnabled() == false)
            return;

        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

                if(dialogBuilder == null)
                {
                    EddieLogger.error("SupportTools.infoDialogHtml(): cannot create dialogBuilder");

                    return;
                }

                final AlertDialog infoDialog = dialogBuilder.create();

                if(infoDialog == null)
                {
                    EddieLogger.error("SupportTools.infoDialogHtml(): cannot create dialog");

                    return;
                }

                infoDialog.setTitle(context.getResources().getString(R.string.eddie));

                infoDialog.setIcon(android.R.drawable.ic_dialog_info);

                infoDialog.setMessage(message);

                infoDialog.setButton(AlertDialog.BUTTON_POSITIVE, context.getResources().getString(R.string.ok), new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        try
                        {
                            infoDialog.dismiss();
                        }
                        catch(Exception e)
                        {
                        }
                    }
                });

                if(context instanceof Activity)
                {
                    if(((Activity)context).isFinishing())
                        return;
                }

                infoDialog.show();
            }
        };

        runOnUiActivity(EddieApplication.mainActivity(), runnable);
    }

    public void waitInfoDialog(int resource)
    {
        waitInfoDialog(EddieApplication.mainActivity(), EddieApplication.mainActivity().getResources().getString(resource));
    }

    public void waitInfoDialog(Context context, int resource)
    {
        waitInfoDialog(context, context.getResources().getString(resource));
    }

    public void waitInfoDialog(String message)
    {
        waitInfoDialog(EddieApplication.mainActivity(), message);
    }

    public void waitInfoDialog(Context context, String message)
    {
        if(EddieApplication.isVisible() == false || context == null)
            return;

        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                dialogConfirm = false;

                final Handler dialogHandler = new Handler(new Handler.Callback()
                {
                    @Override
                    public boolean handleMessage(Message mesg)
                    {
                        throw new RuntimeException();
                    }
                });

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

                if(dialogBuilder == null)
                {
                    EddieLogger.error("SupportTools.waitInfoDialog(): cannot create dialogBuilder");

                    return;
                }

                final AlertDialog waitInfoDialog = dialogBuilder.create();

                if(waitInfoDialog == null)
                {
                    EddieLogger.error("SupportTools.waitInfoDialog(): cannot create dialog");

                    return;
                }

                waitInfoDialog.setTitle(context.getResources().getString(R.string.eddie));

                waitInfoDialog.setIcon(android.R.drawable.ic_dialog_alert);

                waitInfoDialog.setMessage(message);

                waitInfoDialog.setButton(AlertDialog.BUTTON_POSITIVE, context.getResources().getString(R.string.ok), new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialogConfirm = true;

                        try
                        {
                            waitInfoDialog.dismiss();
                        }
                        catch(Exception e)
                        {
                        }

                        dialogHandler.sendMessage(dialogHandler.obtainMessage());
                    }
                });

                if(context instanceof Activity)
                {
                    if(((Activity)context).isFinishing())
                        return;
                }

                waitInfoDialog.show();

                try
                {
                    Looper.loop();
                }
                catch(RuntimeException e)
                {
                }
            }
        };

        runOnUiActivity(EddieApplication.mainActivity(), runnable);
    }

    public boolean confirmationDialog(int resource)
    {
        return confirmationDialog(EddieApplication.mainActivity(), EddieApplication.mainActivity().getResources().getString(resource), EddieApplication.mainActivity().getResources().getString(R.string.yes), EddieApplication.mainActivity().getResources().getString(R.string.no));
    }

    public boolean confirmationDialog(Context context, int resource)
    {
        return confirmationDialog(context, context.getResources().getString(resource), context.getResources().getString(R.string.yes), context.getResources().getString(R.string.no));
    }

    public boolean confirmationDialog(String message)
    {
        return confirmationDialog(EddieApplication.mainActivity(), message, EddieApplication.mainActivity().getResources().getString(R.string.yes), EddieApplication.mainActivity().getResources().getString(R.string.no));
    }

    public boolean confirmationDialog(Context context, String message)
    {
        return confirmationDialog(context, message, context.getResources().getString(R.string.yes), context.getResources().getString(R.string.no));
    }

    public boolean confirmationDialog(int resource, int positiveButtonResource, int negativeButtonResource)
    {
        return confirmationDialog(EddieApplication.mainActivity(), EddieApplication.mainActivity().getResources().getString(resource), EddieApplication.mainActivity().getResources().getString(positiveButtonResource), EddieApplication.mainActivity().getResources().getString(negativeButtonResource));
    }

    public boolean confirmationDialog(Context context, int resource, int positiveButtonResource, int negativeButtonResource)
    {
        return confirmationDialog(context, context.getResources().getString(resource), context.getResources().getString(positiveButtonResource), context.getResources().getString(negativeButtonResource));
    }

    public boolean confirmationDialog(String message, String positiveButtonText, String negativeButtonText)
    {
        return confirmationDialog(EddieApplication.mainActivity(), message, positiveButtonText, negativeButtonText);
    }

    public boolean confirmationDialog(Context context, String message, String positiveButtonText, String negativeButtonText)
    {
        if(context == null)
            return false;

        dialogConfirm = false;

        final Handler dialogHandler = new Handler(new Handler.Callback()
        {
            @Override
            public boolean handleMessage(Message mesg)
            {
                throw new RuntimeException();
            }
        });

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

        if(dialogBuilder == null)
        {
            EddieLogger.error("SupportTools.confirmationDialog(): cannot create dialog");

            return false;
        }

        final AlertDialog confirmationDialog = dialogBuilder.create();

        if(confirmationDialog == null)
        {
            EddieLogger.error("SupportTools.confirmationDialog(): cannot create dialog");

            return false;
        }

        confirmationDialog.setTitle(context.getResources().getString(R.string.eddie));

        confirmationDialog.setIcon(android.R.drawable.ic_dialog_alert);

        confirmationDialog.setMessage(message);

        confirmationDialog.setButton(AlertDialog.BUTTON_POSITIVE, positiveButtonText, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialogConfirm = true;

                try
                {
                    confirmationDialog.dismiss();
                }
                catch(Exception e)
                {
                }

                dialogHandler.sendMessage(dialogHandler.obtainMessage());
            }
        });

        confirmationDialog.setButton(AlertDialog.BUTTON_NEGATIVE, negativeButtonText, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialogConfirm = false;

                try
                {
                    confirmationDialog.dismiss();
                }
                catch(Exception e)
                {
                }

                dialogHandler.sendMessage(dialogHandler.obtainMessage());
            }
        });

        if(context instanceof Activity)
        {
            if(((Activity)context).isFinishing())
                return false;
        }

        confirmationDialog.show();

        try
        {
            Looper.loop();
        }
        catch(RuntimeException e)
        {
        }

        return dialogConfirm;
    }

    public static String getExceptionDetails(Exception e)
    {
        Writer writer = new StringWriter();

        e.printStackTrace(new PrintWriter(writer));

        String s = writer.toString();

        return s;
    }

    public static Thread startThread(Runnable runnable)
    {
        if(runnable == null)
            return null;

        Thread thread = new Thread(runnable);

        if(thread != null)
        {
            try
            {
                thread.start();
            }
            catch(IllegalThreadStateException e)
            {
                thread = null;
            }
        }

        return thread;
    }

    public static Thread runOnUiActivity(final Activity activity, final Runnable runnable)
    {
        if(activity == null || runnable == null)
            return null;

        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                activity.runOnUiThread(runnable);
            }
        };

        return startThread(uiRunnable);
    }

    public static void setButtonStatus(Button btn, ShowMode mode)
    {
        boolean enabled = (mode == ShowMode.ENABLED);

        if(btn == null)
            return;

        btn.setEnabled(enabled);

        if(mode != ShowMode.HIDDEN)
            btn.setVisibility(View.VISIBLE);
        else
            btn.setVisibility(View.GONE);

        if(enabled)
            btn.setAlpha(1.0f);
        else
            btn.setAlpha(0.4f);
    }

    public static void setImageButtonStatus(ImageButton btn, ShowMode mode)
    {
        boolean enabled = (mode == ShowMode.ENABLED);

        if(btn == null)
            return;

        btn.setEnabled(enabled);

        if(mode != ShowMode.HIDDEN)
            btn.setVisibility(View.VISIBLE);
        else
            btn.setVisibility(View.GONE);

        if(enabled)
            btn.setAlpha(1.0f);
        else
            btn.setAlpha(0.4f);
    }

    public byte[] hashMapToUtf8Bytes(HashMap<String, byte[]> h)
    {
        String output = "";

        for(Map.Entry<String, byte[]> pair : h.entrySet())
        {
            output += Base64.encodeToString(pair.getKey().getBytes(StandardCharsets.UTF_8), Base64.NO_WRAP);

            output += ":";

            output += Base64.encodeToString(pair.getValue(), Base64.NO_WRAP);

            output += "\n";
        }

        return output.getBytes();
    }

    public String xmlDocumentToString(Document doc)
    {
        String xml = "";

        if(doc == null)
            return "";

        try
        {
            StringWriter stringWriter = new StringWriter();
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

            transformer.transform(new DOMSource(doc), new StreamResult(stringWriter));

            xml = stringWriter.toString();
        }
        catch(Exception e)
        {
            xml = "";

            EddieLogger.error("SupportTool.xmlDocumentToString() Exception: %s", e);
        }

        return xml;
    }

    public Document stringToXmlDocument(String data)
    {
        Document document = null;

        try
        {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

            document = documentBuilder.parse(new InputSource(new StringReader(data)));
        }
        catch(Exception e)
        {
            document = null;

            EddieLogger.error("SupportTool.stringToXmlDocument() Exception: %s", e);
        }

        return document;
    }

    public boolean encryptStringToFile(String data, String key, String fileName)
    {
        byte[] encryptedData = null;
        SecretKeySpec secretKey = null;

        if(data.isEmpty() || key.isEmpty() || fileName.isEmpty())
            return false;

        try
        {
            secretKey = create256BitEncryptionKey(key);

            Cipher aesCipher = Cipher.getInstance("AES/GCM/NoPadding");

            aesCipher.init(Cipher.ENCRYPT_MODE, secretKey, privateIvParameterSpec);

            encryptedData = aesCipher.doFinal(data.getBytes());
        }
        catch(Exception e)
        {
            EddieLogger.error("SupportTool.encryptStringToFile() Exception: %s", e);

            return false;
        }

        if(encryptedData != null)
        {
            try
            {
                File dataFile = new File(EddieApplication.context().getFilesDir(), fileName);

                if(!dataFile.exists())
                    dataFile.createNewFile();

                FileOutputStream fileOutputStream = new FileOutputStream(dataFile);

                fileOutputStream.write(encryptedData);
                fileOutputStream.flush();
                fileOutputStream.getFD().sync();
                fileOutputStream.close();
            }
            catch(IOException e)
            {
                EddieLogger.error("SupportTool.encryptStringToFile() Exception: %s", e);

                return false;
            }
        }

        return true;
    }

    public byte[] decryptFileToBytes(String fileName, String key)
    {
        byte[] encryptedData = null, decryptedData = null;
        byte[] buffer = null;

        SecretKeySpec secretKey = null;

        if(key.isEmpty() || fileName.isEmpty())
            return null;

        try
        {
            File dataFile = new File(EddieApplication.context().getFilesDir(), fileName);

            if(!dataFile.exists())
            {
                EddieLogger.warning("SupportTool.decryptFileToBytes(): file %s not found", fileName);

                return null;
            }

            buffer = new byte[0xFFFF];

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(dataFile));

            for(int len = buf.read(buffer, 0, buffer.length); len != -1; len = buf.read(buffer))
                outputStream.write(buffer, 0, len);

            buf.close();

            encryptedData = outputStream.toByteArray();
        }
        catch(Exception e)
        {
            EddieLogger.error("SupportTool.decryptFileToBytes() Exception: %s", e);

            return null;
        }

        try
        {
            secretKey = create256BitEncryptionKey(key);

            Cipher aesDecipher = Cipher.getInstance("AES/GCM/NoPadding");

            aesDecipher.init(Cipher.DECRYPT_MODE, secretKey, privateIvParameterSpec);

            decryptedData = aesDecipher.doFinal(encryptedData);
        }
        catch(Exception e)
        {
            EddieLogger.error("SupportTool.decryptFileToBytes() Exception: %s", e);

            return null;
        }

        return decryptedData;
    }

    public boolean saveXmlDocumentToFile(Document document, String fileName)
    {
        if(document == null || fileName.isEmpty())
            return false;

        String data = xmlDocumentToString(document);

        try
        {
            File dataFile = new File(EddieApplication.context().getFilesDir(), fileName);

            if(!dataFile.exists())
                dataFile.createNewFile();

            FileOutputStream fileOutputStream = new FileOutputStream(dataFile);

            fileOutputStream.write(data.getBytes());
            fileOutputStream.flush();
            fileOutputStream.getFD().sync();
            fileOutputStream.close();
        }
        catch (IOException e)
        {
            EddieLogger.error("SupportTool.saveXmlDocumentToFile() Exception: %s", e);

            return false;
        }

        return true;
    }

    public Document loadXmlFileToDocument(String fileName)
    {
        byte[] fileData = null, buffer = null;

        if(fileName.isEmpty())
            return null;

        File dataFile = new File(EddieApplication.context().getFilesDir(), fileName);

        if(!dataFile.exists())
        {
            EddieLogger.warning("SupportTool.decryptFileToBytes(): file %s not found", fileName);

            return null;
        }

        try
        {
            buffer = new byte[0xFFFF];

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(dataFile));

            for(int len = buf.read(buffer, 0, buffer.length); len != -1; len = buf.read(buffer))
                outputStream.write(buffer, 0, len);

            buf.close();

            fileData = outputStream.toByteArray();
        }
        catch(Exception e)
        {
            EddieLogger.error("SupportTool.decryptFileToBytes() Exception: %s", e);

            return null;
        }

        return stringToXmlDocument(new String(fileData));
    }

    public boolean encryptXmlDocumentToFile(Document document, String key, String fileName)
    {
        if(document == null || key.isEmpty() || fileName.isEmpty())
            return false;

        String data = xmlDocumentToString(document);

        return encryptStringToFile(data, key, fileName);
    }

    public Document decryptFileToXmlDocument(String fileName, String key)
    {
        Document xmlDocument = null;

        if(fileName.isEmpty() || key.isEmpty())
            return null;

        byte[] decryptedData = decryptFileToBytes(fileName, key);

        if(decryptedData != null)
            xmlDocument = stringToXmlDocument(new String(decryptedData));

        return xmlDocument;
    }

    private SecretKeySpec create256BitEncryptionKey(String key)
    {
        String encryptionKey = "";
        SecretKeySpec secretKey = null;
        int keyLen = 32; // 32 bytes, 256 bit

        while(encryptionKey.length() < keyLen)  // 256 bit key
            encryptionKey += key;

        secretKey = new SecretKeySpec(encryptionKey.getBytes(), 0, keyLen, "AES");

        return secretKey;
    }

    public String getXmlItemValue(Document document, String item)
    {
        String value = "";

        try
        {
            NodeList nodeList = document.getElementsByTagName(item);

            if(nodeList != null)
            {
                Element element = (Element) nodeList.item(0);

                if(element != null)
                    value = element.getTextContent();
                else
                    value = "";
            }
        }
        catch(Exception e)
        {
            value = "";
        }

        return convertXmlEntities(value);
    }

    public String getXmlItemNodeValue(NamedNodeMap namedNodeMap, String item)
    {
        String value = "";

        try
        {
            Node node = namedNodeMap.getNamedItem(item);

            if(node != null)
                value = node.getNodeValue();
            else
                value = "";
        }
        catch(Exception e)
        {
            value = "";
        }

        return convertXmlEntities(value);
    }

    public String convertXmlEntities(String xml)
    {
        String value = xml;
        String[] xmlEntity = { "&#10;", "&#13;", "&#38;", "&gt;", "&lt;", "&amp;", "&quot;", "&apos;", "\\'", "\\\"" };
        String[] character = { "\n", "\r", "&", ">", "<", "&", "\"", "'", "'", "\""};

        for(int i = 0; i < xmlEntity.length; i++)
            xml = xml.replaceAll(xmlEntity[i], character[i]);

        return value;
    }

    public static boolean isFileXML(String fileName) throws FileNotFoundException
    {
        byte[] buffer = new byte[5];
        String sBuffer = null;
        boolean isXML = false;

        if(fileName.isEmpty())
            return false;

        File file = new File(EddieApplication.context().getFilesDir(), fileName);

        if(file.exists() == false)
            throw new FileNotFoundException();

        try
        {
            InputStream is = new FileInputStream(file);

            if(is.read(buffer) == buffer.length)
            {
                sBuffer = new String(buffer);

                if(sBuffer.equals("<?xml"))
                    isXML = true;
                else
                    isXML = false;
            }
            else
                isXML = false;

            is.close();
        }
        catch(Exception e)
        {
            isXML = false;
        }

        return isXML;
    }

    public boolean isNetworkConnectionActive()
    {
        return isNetworkConnectionActive(false);
    }

    public boolean isNetworkConnectionActive(boolean ignoreVPN)
    {
        boolean active = false;

        NetworkStatusReceiver.Status networkStatus = NetworkStatusReceiver.getNetworkStatus();
        VPN.Status vpnStatus = EddieApplication.vpnManager().vpn().getConnectionStatus();

        if(networkStatus != NetworkStatusReceiver.Status.CONNECTED)
            return false;

        if(ignoreVPN == false && (vpnStatus == VPN.Status.LOCKED || vpnStatus == VPN.Status.PAUSED_BY_SYSTEM || vpnStatus == VPN.Status.PAUSED_BY_USER))
            active = false;
        else
            active = true;

        return active;
    }

    public boolean waitForNetwork()
    {
        return waitForNetwork(defaultNetworkWaitSeconds, false);
    }

    public boolean waitForNetwork(int waitSeconds)
    {
        return waitForNetwork(waitSeconds, false);
    }

    public boolean waitForNetwork(boolean interactive)
    {
        return waitForNetwork(defaultNetworkWaitSeconds, interactive);
    }

    public boolean waitForNetwork(int waitSeconds, boolean interactive)
    {
        int loops = (waitSeconds * 1000) / loopWaitMilliSeconds;

        if(isNetworkConnectionActive() == false)
        {
            if(interactive == true)
                showProgressDialog(R.string.wait_for_network);

            EddieLogger.info("Waiting for a valid Network connection to be available");

            for(int i = 0; i < loops && isNetworkConnectionActive() == false; i++)
            {
                try
                {
                    Thread.sleep(loopWaitMilliSeconds);
                }
                catch(Exception e)
                {
                }
            }

            if(interactive == true)
                dismissProgressDialog();
        }

        return isNetworkConnectionActive();
    }

    public boolean waitForManifest()
    {
        return waitForManifest(false, defaultManifestWaitSeconds);
    }

    public boolean waitForManifest(boolean interactive)
    {
        return waitForManifest(interactive, defaultManifestWaitSeconds);
    }

    public boolean waitForManifest(boolean interactive, int waitSeconds)
    {
        int loops = (waitSeconds * 1000) / loopWaitMilliSeconds;

        if(AirVPNManifest.getManifestType() == AirVPNManifest.ManifestType.NOT_SET || AirVPNManifest.getManifestType() == AirVPNManifest.ManifestType.PROCESSING || AirVPNManifest.getManifestType() == AirVPNManifest.ManifestType.DEFAULT)
        {
            EddieLogger.info("Waiting for a valid AirVPN manifest to be loaded");

            if(interactive == true)
                showProgressDialog(R.string.wait_for_airvpn_manifest);

            for(int i = 0; i < loops && (AirVPNManifest.getManifestType() == AirVPNManifest.ManifestType.NOT_SET || AirVPNManifest.getManifestType() == AirVPNManifest.ManifestType.PROCESSING || AirVPNManifest.getManifestType() == AirVPNManifest.ManifestType.DEFAULT); i++)
            {
                try
                {
                    Thread.sleep(loopWaitMilliSeconds);
                }
                catch(Exception e)
                {
                }
            }

            if(interactive == true)
                dismissProgressDialog();
        }

        return (AirVPNManifest.getManifestType() != AirVPNManifest.ManifestType.NOT_SET && AirVPNManifest.getManifestType() != AirVPNManifest.ManifestType.PROCESSING);
    }

    public ArrayList<String> getLocalIPs(boolean ipv6)
    {
        LinkProperties linkProperties = null;
        NetworkCapabilities networkCapabilities = null;
        ArrayList<String> localIPs = new ArrayList<String>();

        ConnectivityManager connectivityManager = (ConnectivityManager) EddieApplication.context().getSystemService(Context.CONNECTIVITY_SERVICE);

        Network[] deviceNetworks = connectivityManager.getAllNetworks();

        for(Network network : deviceNetworks)
        {
            linkProperties = connectivityManager.getLinkProperties(network);

            networkCapabilities = connectivityManager.getNetworkCapabilities(network);

            if (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_VPN))
                continue;

            if (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR))
                continue;

            for(LinkAddress linkAddress : linkProperties.getLinkAddresses())
            {
                if((linkAddress.getAddress() instanceof Inet4Address && !ipv6) || (linkAddress.getAddress() instanceof Inet6Address && ipv6))
                    localIPs.add(linkAddress.toString());
            }
        }

        return localIPs;
    }

    public static long IPToLong(String ip)
    {
        String[] octect = ip.split("\\.");

        long longIP = 0;

        for(int i = 0; i < octect.length; i++)
        {
            int power = 3 - i;

            try
            {
                longIP += ((Integer.parseInt(octect[i]) % 256 * Math.pow(256, power)));
            }
            catch(NumberFormatException e)
            {
            }
        }

        return longIP;
    }

    public static String longToIP(long ip)
    {
        return String.format(Locale.getDefault(), "%d.%d.%d.%d", ((ip >> 24 ) & 0xff), ((ip >> 16 ) & 0xff), ((ip >>  8 ) & 0xff), ip & 0xff);
    }

    public static String formatTransferRate(long rate)
    {
        return formatTransferRate(rate, true, false);
    }

    public static String formatTransferRate(long rate, boolean decimal)
    {
        return formatTransferRate(rate, decimal, false);
    }

    public static String formatTransferRate(long rate, boolean decimal, boolean binary)
    {
        String txt = "", floatFormat = "";
        double r = (double)rate;

        if(decimal)
            floatFormat = "%.2f %s";
        else
            floatFormat = "%.0f %s";

        if(binary == false)
        {
            if(rate >= ONE_DECIMAL_GIGA)
                txt = String.format(Locale.getDefault(), floatFormat, r / ONE_DECIMAL_GIGA, "Gbit/s");
            else if(rate >= ONE_DECIMAL_MEGA)
                txt = String.format(Locale.getDefault(), floatFormat, r / ONE_DECIMAL_MEGA, "Mbit/s");
            else if(rate > ONE_DECIMAL_KILO)
                txt = String.format(Locale.getDefault(), floatFormat, r / ONE_DECIMAL_KILO, "Kbit/s");
            else
                txt = String.format(Locale.getDefault(), "%d bit/s", (int) r);
        }
        else
        {
            if(rate >= ONE_GIGABYTE)
                txt = String.format(Locale.getDefault(), floatFormat, r / ONE_GIGABYTE, "Gibit/s");
            else if(rate >= ONE_MEGABYTE)
                txt = String.format(Locale.getDefault(), floatFormat, r / ONE_MEGABYTE, "Mibit/s");
            else if(rate > ONE_KILOBYTE)
                txt = String.format(Locale.getDefault(), floatFormat, r / ONE_KILOBYTE, "Kibit/s");
            else
                txt = String.format(Locale.getDefault(), "%d bit/s", (int) r);
        }

        return txt;
    }

    public static String formatDataVolume(long bytes)
    {
        return formatDataVolume(bytes, true, false);
    }

    public static String formatDataVolume(long bytes, boolean decimal)
    {
        return formatDataVolume(bytes, decimal, false);
    }

    public static String formatDataVolume(long bytes, boolean decimal, boolean binary)
    {
        String txt = "", floatFormat = "";
        double dBytes = (double)bytes;

        if(decimal)
            floatFormat = "%.2f %s";
        else
            floatFormat = "%.0f %s";

        if(binary == false)
        {
            if(bytes >= ONE_DECIMAL_GIGA)
                txt = String.format(Locale.getDefault(), floatFormat, dBytes / ONE_DECIMAL_GIGA, "GB");
            else if(bytes >= ONE_DECIMAL_MEGA)
                txt = String.format(Locale.getDefault(), floatFormat, dBytes / ONE_DECIMAL_MEGA, "MB");
            else if(bytes >= ONE_DECIMAL_KILO)
                txt = String.format(Locale.getDefault(), floatFormat, dBytes / ONE_DECIMAL_KILO, "KB");
            else
                txt = String.format(Locale.getDefault(), "%d bytes", bytes);
        }
        else
        {
            if(bytes >= ONE_GIGABYTE)
                txt = String.format(Locale.getDefault(), floatFormat, dBytes / ONE_GIGABYTE, "GiB");
            else if(bytes >= ONE_MEGABYTE)
                txt = String.format(Locale.getDefault(), floatFormat, dBytes / ONE_MEGABYTE, "MiB");
            else if(bytes >= ONE_KILOBYTE)
                txt = String.format(Locale.getDefault(), floatFormat, dBytes / ONE_KILOBYTE, "KiB");
            else
                txt = String.format(Locale.getDefault(), "%d bytes", bytes);
        }

        return txt;
    }

    public static int getTrafficLoad(long byteBandWidth, long maxMBitBandWidth)
    {
        long bwCur = 2 * ((byteBandWidth * 8) / ONE_DECIMAL_MEGA); // to Mbit/s
        int load = 0;

        if(maxMBitBandWidth > 0)
            load = (int)((bwCur * 100) / maxMBitBandWidth);
        else
            load = 0;

        return load;
    }

    public String getOptionFromListDialog(Context context, int resTitle, int resLabel, int resValue, final String selectedValue)
    {
        return getOptionFromListDialog(context, resTitle, context.getResources().getStringArray(resLabel), context.getResources().getStringArray(resValue), selectedValue);
    }

    public String getOptionFromListDialog(Context context, int resTitle, String Label[], String Value[], final String selectedValue)
    {
        if(context == null)
            return selectedValue;

        dialogValues = Value;

        dialogReturnStringValue = selectedValue;

        final Handler dialogHandler = new Handler(new Handler.Callback()
        {
            @Override
            public boolean handleMessage(Message mesg)
            {
                throw new RuntimeException();
            }
        });

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

        if(dialogBuilder == null)
        {
            EddieLogger.error("SupportTools.getOptionFromListDialog(): cannot create dialog");

            return selectedValue;
        }

        dialogBuilder.setTitle(context.getResources().getString(resTitle));

        int checkedItem = Arrays.asList(dialogValues).indexOf(selectedValue);

        dialogBuilder.setSingleChoiceItems(Label, checkedItem,  new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                dialogReturnStringValue = dialogValues[i];
            }
        });

        dialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
                dialogHandler.sendMessage(dialogHandler.obtainMessage());
            }
        });

        dialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
                dialogReturnStringValue = selectedValue;

                dialogHandler.sendMessage(dialogHandler.obtainMessage());
            }
        });

        settingDialog = dialogBuilder.create();

        if(context instanceof Activity)
        {
            if(((Activity)context).isFinishing())
                return "";
        }

        settingDialog.show();

        try
        {
            Looper.loop();
        }
        catch(RuntimeException e)
        {
        }

        return dialogReturnStringValue;
    }

    public String getTextOptionDialog(Context context, int resTitle, String selectedValue)
    {
        return editOptionDialog(context, resTitle, selectedValue, EditOptionType.TEXT, EditOption.DO_NOT_ALLOW_EMPTY_FIELD);
    }

    public String getTextOptionDialog(Context context, int resTitle, String selectedValue, EditOption editOption)
    {
        return editOptionDialog(context, resTitle, selectedValue, EditOptionType.TEXT, editOption);
    }

    public String getPasswordOptionDialog(Context context, int resTitle, String selectedValue)
    {
        return editOptionDialog(context, resTitle, selectedValue, EditOptionType.PASSWORD, EditOption.DO_NOT_ALLOW_EMPTY_FIELD);
    }

    public String getPasswordOptionDialog(Context context, int resTitle, String selectedValue, EditOption editOption)
    {
        return editOptionDialog(context, resTitle, selectedValue, EditOptionType.PASSWORD, editOption);
    }

    public long getNumericOptionDialog(Context context, int resTitle, long selectedValue)
    {
        return getNumericOptionDialog(context, resTitle, selectedValue, EditOption.DO_NOT_ALLOW_ZERO_VALUE);
    }

    public long getNumericOptionDialog(Context context, int resTitle, long selectedValue, EditOption editOption)
    {
        String value = "";
        long retVal = 0;

        if(context == null)
            return 0;

        value = editOptionDialog(context, resTitle, String.format(Locale.getDefault(), "%d", selectedValue), EditOptionType.NUMERIC, editOption);

        try
        {
            retVal = Long.parseLong(value);
        }
        catch(NumberFormatException e)
        {
            retVal = 0;
        }

        return retVal;
    }

    public String getIPv4AddressOptionDialog(Context context, int resTitle, String selectedValue)
    {
        return editOptionDialog(context, resTitle, selectedValue, EditOptionType.IP_ADDRESS_LIST, EditOption.DO_NOT_ALLOW_EMPTY_FIELD);
    }

    public String getIPv4AddressOptionDialog(Context context, int resTitle, String selectedValue, EditOption editOption)
    {
        return editOptionDialog(context, resTitle, selectedValue, EditOptionType.IP_ADDRESS_LIST, editOption);
    }

    public long getIpPortOptionDialog(Context context, int resTitle, long selectedValue)
    {
        return getIpPortOptionDialog(context, resTitle, selectedValue, EditOption.DO_NOT_ALLOW_ZERO_VALUE);
    }

    public long getIpPortOptionDialog(Context context, int resTitle, long selectedValue, EditOption editOption)
    {
        String value = "";
        long retVal = 0;

        if(context == null)
            return 0;

        value = editOptionDialog(context, resTitle, String.format(Locale.getDefault(), "%d", selectedValue), EditOptionType.IP_PORT, editOption);

        try
        {
            retVal = Long.parseLong(value);
        }
        catch(NumberFormatException e)
        {
            retVal = 0;
        }

        return retVal;
    }

    public String editOptionDialog(Context context, int resTitle, String selectedValue)
    {
        return editOptionDialog(context, resTitle, selectedValue, EditOptionType.TEXT, EditOption.DO_NOT_ALLOW_EMPTY_FIELD);
    }

    public String editOptionDialog(Context context, int resTitle, final String selectedValue, final EditOptionType editType, final EditOption editOption)
    {
        TextView txtDialogTitle = null;

        if(context == null)
            return selectedValue;

        btnOk = null;
        btnCancel = null;

        final Handler dialogHandler = new Handler(new Handler.Callback()
        {
            @Override
            public boolean handleMessage(Message mesg)
            {
                throw new RuntimeException();
            }
        });

        dialogBuilder = new AlertDialog.Builder(context);

        if(dialogBuilder == null)
        {
            EddieLogger.error("SupportTools.editOptionDialog(): cannot create dialogBuilder");

            return selectedValue;
        }

        View content = LayoutInflater.from(context).inflate(R.layout.edit_option_dialog, null);

        txtDialogTitle = (TextView)content.findViewById(R.id.title);
        edtKey = (EditText)content.findViewById(R.id.key);

        edtKey.setText(selectedValue);

        if(editType == EditOptionType.NUMERIC || editType == EditOptionType.IP_PORT)
        {
            edtKey.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);

            edtKey.setGravity(Gravity.RIGHT);

            edtKey.setSelection(edtKey.getText().length());
        }

        if(editType == EditOptionType.PASSWORD)
        {
            edtKey.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);
        }

        edtKey.setSelection(edtKey.getText().length());

        btnOk = (Button)content.findViewById(R.id.btn_ok);
        btnCancel = (Button)content.findViewById(R.id.btn_cancel);

        setButtonStatus(btnOk, editFieldOptionIsValid(editType, selectedValue, editOption) == true ? ShowMode.ENABLED : ShowMode.DISABLED);

        setButtonStatus(btnCancel, ShowMode.ENABLED);

        btnOk.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                int errMsgResource = 0;

                if(editFieldIsValid(editType, edtKey.getText().toString(), editOption) == false)
                {
                    switch(editType)
                    {
                        case IP_ADDRESS_LIST:
                        {
                            errMsgResource = R.string.settings_ip_address_warning;
                        }
                        break;

                        case IP_PORT:
                        {
                            errMsgResource = R.string.settings_ip_port_warning;
                        }
                        break;

                        default:
                        {
                            errMsgResource = R.string.settings_value_warning;
                        }
                        break;
                    }

                    infoDialog(errMsgResource, true);

                    return;
                }

                if(edtKey.getText().length() > 0 || editOption == EditOption.ALLOW_EMPTY_FIELD || editOption == EditOption.ALLOW_ZERO_VALUE)
                    dialogReturnStringValue = edtKey.getText().toString();

                try
                {
                    settingDialog.dismiss();
                }
                catch(Exception e)
                {
                }

                dialogHandler.sendMessage(dialogHandler.obtainMessage());
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                dialogReturnStringValue = selectedValue;

                try
                {
                    settingDialog.dismiss();
                }
                catch(Exception e)
                {
                }

                dialogHandler.sendMessage(dialogHandler.obtainMessage());
            }
        });

        edtKey.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void afterTextChanged(Editable s)
            {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                setButtonStatus(btnOk, editFieldOptionIsValid(editType, edtKey.getText().toString(), editOption) == true ? ShowMode.ENABLED : ShowMode.DISABLED);
            }
        });

        txtDialogTitle.setText(context.getResources().getString(resTitle));

        dialogBuilder.setTitle("");
        dialogBuilder.setView(content);

        settingDialog = dialogBuilder.create();
        settingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        settingDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        if(context instanceof Activity)
        {
            if(((Activity)context).isFinishing())
                return "";
        }

        settingDialog.show();

        try
        {
            Looper.loop();
        }
        catch(RuntimeException e)
        {
        }

        return dialogReturnStringValue.trim();
    }

    private boolean editFieldOptionIsValid(EditOptionType editType, String value, EditOption option)
    {
        boolean isValid = false;

        switch(editType)
        {
            case TEXT:
            case PASSWORD:
            case IP_ADDRESS_LIST:
            {
                if(value.length() > 0 || option == EditOption.ALLOW_EMPTY_FIELD)
                    isValid = true;
                else
                    isValid = false;
            }
            break;

            case NUMERIC:
            case IP_PORT:
            {
                long tVal = 0;

                try
                {
                    tVal = Long.parseLong(value);
                }
                catch(NumberFormatException e)
                {
                    tVal = 0;
                }

                if(tVal > 0 || option == EditOption.ALLOW_ZERO_VALUE)
                    isValid = true;
                else
                    isValid = false;
            }
            break;
        }

        return isValid;
    }

    private boolean editFieldIsValid(EditOptionType editType, String value)
    {
        return editFieldIsValid(editType, value, EditOption.DO_NOT_ALLOW_EMPTY_FIELD);
    }

    private boolean editFieldIsValid(EditOptionType editType, String value, EditOption editOption)
    {
        boolean isValid = false;
        long tVal = 0;

        switch(editType)
        {
            case TEXT:
            {
                isValid = true;
            }
            break;

            case PASSWORD:
            {
                isValid = true;
            }
            break;

            case NUMERIC:
            {
                isValid = true;
            }
            break;

            case IP_ADDRESS_LIST:
            {
                String[] octect = null;
                isValid = true;

                String[] ipAdrressArray = value.split(SettingsManager.DEFAULT_SPLIT_SEPARATOR);

                for(String ipAddress : ipAdrressArray)
                {
                    octect = ipAddress.split("\\.");

                    if(octect.length != 4)
                        isValid = false;

                    for(String val : octect)
                    {
                        try
                        {
                            tVal = Long.parseLong(val);
                        }
                        catch(NumberFormatException e)
                        {
                            tVal = 0;

                            isValid = false;
                        }

                        if(tVal < 0 || tVal > 255)
                            isValid = false;
                    }
                }

                if(value.length() == 0 && editOption == EditOption.ALLOW_EMPTY_FIELD)
                    isValid = true;
            }
            break;

            case IP_PORT:
            {
                try
                {
                    tVal = Long.parseLong(value);
                }
                catch(NumberFormatException e)
                {
                    tVal = 0;

                    isValid = false;
                }

                if(tVal >= 1 && tVal <= 65535)
                    isValid = true;
                else
                    isValid = false;

                if(tVal == 0 && editOption == EditOption.ALLOW_ZERO_VALUE)
                    isValid = true;
            }
            break;

            default:
            {
                isValid = false;
            }
            break;
        }

        return isValid;
    }


    public boolean listOptionDialog(Context context, int resTitle, final ArrayList<SortableListViewItem> listItems)
    {
        btnOk = null;
        btnCancel = null;

        if(listItems.isEmpty() || context == null)
            return false;

        final Handler dialogHandler = new Handler(new Handler.Callback()
        {
            @Override
            public boolean handleMessage(Message mesg)
            {
                throw new RuntimeException();
            }
        });

        dialogBuilder = new AlertDialog.Builder(context);

        if(dialogBuilder == null)
        {
            EddieLogger.error("SupportTools.listOptionDialog(): cannot create dialog");

            return false;
        }

        View content = LayoutInflater.from(context).inflate(R.layout.sortable_listview_option_dialog, null);

        btnOk = (Button)content.findViewById(R.id.btn_ok);
        btnCancel = (Button)content.findViewById(R.id.btn_cancel);
        txtSortableListviewAction = (TextView)content.findViewById(R.id.action);

        dialogBuilder.setTitle(context.getResources().getString(resTitle));
        dialogBuilder.setView(content);

        settingDialog = dialogBuilder.create();
        settingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        sortableListView = (ListView)content.findViewById(R.id.draglist);

        sortableListViewArrayAdapter = new SortableListViewArrayAdapter(context, R.layout.sortable_listview_option_item);
        sortableListViewSelectedPosition = -1;

        for(int i = 0; i < listItems.size(); i++)
        {
            SortableListViewItem item = new SortableListViewItem();

            item.description = listItems.get(i).description;
            item.code = listItems.get(i).code;

            sortableListViewArrayAdapter.add(item);
        }

        setButtonStatus(btnOk, ShowMode.ENABLED);
        setButtonStatus(btnCancel, ShowMode.ENABLED);

        txtSortableListviewAction.setText(R.string.sortable_listview_select_source);
        txtSortableListviewAction.setTextColor(context.getResources().getColor(R.color.titleColor));

        btnOk.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                dialogReturnBooleanValue = true;

                try
                {
                    settingDialog.dismiss();
                }
                catch(Exception e)
                {
                }

                dialogHandler.sendMessage(dialogHandler.obtainMessage());
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                dialogReturnBooleanValue = false;

                try
                {
                    settingDialog.dismiss();
                }
                catch(Exception e)
                {
                }

                dialogHandler.sendMessage(dialogHandler.obtainMessage());
            }
        });

        sortableListView.setAdapter(sortableListViewArrayAdapter);

        sortableListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView parent, View view, int position, long id)
            {
                TextView currentTextView = (TextView)view.findViewById(R.id.title);

                if(sortableListViewSelectedPosition == -1)
                {
                    sortableListViewSelectedPosition = position;

                    currentTextView.setBackgroundColor(context.getResources().getColor(R.color.listViewPick));

                    txtSortableListviewAction.setText(R.string.sortable_listview_select_destination);
                    txtSortableListviewAction.setTextColor(context.getResources().getColor(R.color.red));
                }
                else
                {
                    View clickedView = sortableListViewGetViewAtPosition(sortableListView, sortableListViewSelectedPosition);

                    if(clickedView != null)
                    {
                        TextView txt = (TextView)clickedView.findViewById(R.id.title);

                        txt.setBackgroundColor(Color.TRANSPARENT);
                    }

                    sortableListViewArrayAdapter.move(sortableListViewSelectedPosition, position);

                    sortableListViewSelectedPosition = -1;

                    txtSortableListviewAction.setText(R.string.sortable_listview_select_source);
                    txtSortableListviewAction.setTextColor(context.getResources().getColor(R.color.titleColor));
                }
            }
        });

        if(context instanceof Activity)
        {
            if(((Activity)context).isFinishing())
                return false;
        }

        settingDialog.show();

        try
        {
            Looper.loop();
        }
        catch(RuntimeException e)
        {
        }

        return dialogReturnBooleanValue;
    }

    public ArrayList<SortableListViewItem> getSortableListViewItems()
    {
        ArrayList<SortableListViewItem> listItems = null;

        if(sortableListViewArrayAdapter == null)
            return null;

        return sortableListViewArrayAdapter.getItems();
    }

    public boolean isAccessibilityEnabled()
    {
        AccessibilityManager am = (AccessibilityManager)EddieApplication.context().getSystemService(ACCESSIBILITY_SERVICE);

        return (am.isEnabled() || am.isTouchExplorationEnabled());
    }

    public boolean isLocationPermissionGranted()
    {
        boolean fineLocation = false;
        boolean coarseLocation = false;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            fineLocation = (EddieApplication.context().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);

            coarseLocation = (EddieApplication.context().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED);
        }
        else
        {
            fineLocation = (ContextCompat.checkSelfPermission(EddieApplication.context(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);

            coarseLocation = (ContextCompat.checkSelfPermission(EddieApplication.context(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED);
        }

        return fineLocation & coarseLocation;
    }

    public void saveLastConnectedProfile(HashMap<String, String> profileInfo, String profileData)
    {
        if(profileData == null)
            profileData = "";

        settingsManager.setLastVPNProfile(profileData);

        if(profileInfo == null)
            profileInfo = new HashMap<String, String>();

        settingsManager.setSystemLastProfileInfo(profileInfo);
    }

    public HashMap<String, String> getBootVPNProfile(String profileType)
    {
        String serverDescription = "";
        VPNProfileDatabase vpnProfileDatabase = null;
        HashMap<String, String> profileInfo = null;

        switch(profileType)
        {
            case BOOT_PROFILE_TYPE_DEFAULT_VPN_PROFILE:
            {
                vpnProfileDatabase = new VPNProfileDatabase(EddieApplication.context());

                if(vpnProfileDatabase.getStartupProfile() != null)
                {
                    VPNProfileDatabase.VPNProfile vpnProfile = vpnProfileDatabase.getStartupProfile();

                    profileInfo = new HashMap<String, String>();

                    serverDescription = vpnProfile.getAirVPNServerDescription();

                    if(serverDescription.isEmpty())
                        serverDescription = vpnProfile.getIpAddress();

                    profileInfo.put("mode", String.format(Locale.getDefault(), "%d", VPN.ConnectionMode.BOOT_CONNECT.getValue()));
                    profileInfo.put("vpn_type", vpnProfile.getType().toString());
                    profileInfo.put("name", vpnProfile.getName());
                    profileInfo.put("profile", vpnProfile.getName());
                    profileInfo.put("type", BOOT_PROFILE_TYPE_DEFAULT_VPN_PROFILE);
                    profileInfo.put("status", "ok");
                    profileInfo.put("server", vpnProfile.getIpAddress());
                    profileInfo.put("protocol", vpnProfile.getProtocol().toString());
                    profileInfo.put("port", String.format(Locale.getDefault(), "%d", vpnProfile.getPort()));
                    profileInfo.put("description", String.format(Locale.getDefault(), "%s (VPN startup profile)", serverDescription));
                    profileInfo.put("vpn_profile", vpnProfile.getProfile());
                }
            }
            break;

            case BOOT_PROFILE_TYPE_LAST_ACTIVE_CONNECTION:
            {
                if(settingsManager.isSystemLastProfileIsConnected() == false)
                    return null;

                profileInfo = settingsManager.getSystemLastProfileInfo();

                if(profileInfo == null)
                {
                    profileInfo = new HashMap<String, String>();

                    serverDescription = "";
                }
                else
                {
                    serverDescription = profileInfo.get("description");

                    if(serverDescription.isEmpty())
                        serverDescription = profileInfo.get("server");
                }

                profileInfo.put("mode", String.format(Locale.getDefault(), "%d", VPN.ConnectionMode.BOOT_CONNECT.getValue()));
                profileInfo.put("name", "boot_connect");
                profileInfo.put("profile", "boot_connect");
                profileInfo.put("type", BOOT_PROFILE_TYPE_LAST_ACTIVE_CONNECTION);
                profileInfo.put("status", "ok");
                profileInfo.put("description", String.format("%s (last active connection)", serverDescription));
                profileInfo.put("vpn_profile", settingsManager.getLastVPNProfile());
            }
            break;

            default:
            {
                profileInfo = null;
            }
            break;
        }

        return profileInfo;
    }

    public class RequestAirVPNDocument extends AsyncTask<HashMap<String, String>, Void, AirVpnDocumentRequest>
    {
        private Document airVpnDocument = null;
        private HashMap<String, byte[]> parameters = null;
        private String requestedAction = "";

        @Override
        protected void onPreExecute()
        {
        }

        @Override
        protected AirVpnDocumentRequest doInBackground(HashMap<String, String>... params)
        {
            byte[] iv = null, bytesParamS = null, aesDataIn = null, bytesParamD = null;
            HashMap<String, byte[]> assocParamS = null;
            Cipher rsaCipher = null, aesCipher = null, aesDecipher = null;
            KeyFactory keyFactory = null;
            KeyGenerator keyGenerator = null;
            SecureRandom secureRandom = null;
            RSAPublicKeySpec publicKeySpec = null;
            RSAPublicKey publicKey = null;
            HttpURLConnection httpURLConnection = null;
            DocumentBuilderFactory documentBuilderFactory = null;
            DocumentBuilder documentBuilder = null;
            InputSource inputSource = null;
            ArrayList<AirVPNManifest.BootServer> bootstrapServer = null, bootList = null;
            String rsaModulus = "", rsaExponent = "", urlParameters = "", xmlDocument = "", software = "";
            AirVPNManifest.ManifestType manifestType = AirVPNManifest.ManifestType.NOT_SET;
            boolean documentRetrieved = false;
            AirVpnDocumentRequest operationResult = AirVpnDocumentRequest.ERROR;

            if(params[0] == null)
            {
                EddieLogger.error("RequestAirVPNDocument(): parameters is null");

                return AirVpnDocumentRequest.ERROR;
            }

            parameters = new HashMap<String, byte[]>();

            for(Map.Entry<String, String> pair : params[0].entrySet())
                parameters.put(pair.getKey(), (pair.getValue()).getBytes());

            if(parameters == null || parameters.isEmpty())
            {
                EddieLogger.error("RequestAirVPNDocument(): No parameter provided");

                return AirVpnDocumentRequest.ERROR;
            }

            if(parameters.containsKey("act"))
                requestedAction = new String(parameters.get("act"));
            else
            {
                EddieLogger.error("RequestAirVPNDocument(): No action request provided");

                return AirVpnDocumentRequest.ERROR;
            }

            if(isNetworkConnectionActive() == false)
            {
                EddieLogger.error("Cannot download %s document from AirVPN. Network is not available.", requestedAction);

                return AirVpnDocumentRequest.ERROR;
            }

            if(parameters.containsKey("software") == false)
            {
                software = "EddieAndroid_";
                software += BuildConfig.VERSION_NAME;

                parameters.put("software", software.getBytes());
            }

            if(parameters.containsKey("system") == false)
                parameters.put("system", DEVICE_PLATFORM.getBytes());

            if(parameters.containsKey("arch") == false)
                parameters.put("arch", EddieLibrary.architecture().getBytes());

            if(parameters.containsKey("version") == false)
                parameters.put("version", AIRVPN_SERVER_DOCUMENT_VERSION.getBytes());

            switch(requestedAction)
            {
                case AIRVPN_ACTION_REQUEST_MANIFEST:
                {
                    manifestType = AirVPNManifest.getManifestType();

                    if(manifestType == AirVPNManifest.ManifestType.NOT_SET || manifestType == AirVPNManifest.ManifestType.PROCESSING || manifestRequestPending == true)
                        return AirVpnDocumentRequest.MANIFEST_DOCUMENT_IGNORED;
                    else
                        manifestRequestPending = true;
                }
                break;

                case AIRVPN_ACTION_REQUEST_USER:
                {
                    if(AirVPNUser.getUserProfileType() == AirVPNUser.UserProfileType.PROCESSING || userRequestPending == true)
                        return AirVpnDocumentRequest.USER_DOCUMENT_IGNORED;
                    else
                        userRequestPending = true;
                }
                break;

                default:
                {
                    EddieLogger.error("Unknown AirVPN document %s", requestedAction);

                    return AirVpnDocumentRequest.ERROR;
                }
            }

            EddieLogger.info("Requesting %s document to AirVPN", requestedAction);

            assocParamS = new HashMap<String, byte[]>();

            try
            {
                rsaCipher = Cipher.getInstance("RSA/None/PKCS1Padding");
                aesCipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
                aesDecipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
                keyFactory = KeyFactory.getInstance("RSA");

                keyGenerator = KeyGenerator.getInstance("AES");
                secureRandom = SecureRandom.getInstance("SHA1PRNG");
            }
            catch(Exception e)
            {
                EddieLogger.error("RequestAirVPNDocument() Exception: %s", e);

                if(requestedAction.equals(AIRVPN_ACTION_REQUEST_MANIFEST))
                    operationResult = AirVpnDocumentRequest.MANIFEST_DOCUMENT_FAILED;
                else
                    operationResult = AirVpnDocumentRequest.USER_DOCUMENT_FAILED;

                return operationResult;
            }

            keyGenerator.init(256, secureRandom);

            iv = new byte[aesCipher.getBlockSize()];

            secureRandom.nextBytes(iv);
            SecretKey skey = keyGenerator.generateKey();
            IvParameterSpec ivspec = new IvParameterSpec(iv);

            rsaModulus = AirVPNManifest.getRsaPublicKeyModulus();
            rsaExponent = AirVPNManifest.getRsaPublicKeyExponent();

            if(rsaModulus.isEmpty() || rsaExponent.isEmpty())
            {
                EddieLogger.error("RequestAirVPNDocument(): empty RSA parameters");

                if(requestedAction.equals(AIRVPN_ACTION_REQUEST_MANIFEST))
                    operationResult = AirVpnDocumentRequest.MANIFEST_DOCUMENT_FAILED;
                else
                    operationResult = AirVpnDocumentRequest.USER_DOCUMENT_FAILED;

                return operationResult;
            }

            publicKeySpec = new RSAPublicKeySpec(new BigInteger(1, Base64.decode(rsaModulus.getBytes(), Base64.NO_WRAP)), new BigInteger(1, Base64.decode(rsaExponent.getBytes(), Base64.NO_WRAP)));

            try
            {
                publicKey = (RSAPublicKey)keyFactory.generatePublic(publicKeySpec);
            }
            catch(Exception e)
            {
                EddieLogger.error("RequestAirVPNDocument() Exception: %s", e);

                if(requestedAction.equals(AIRVPN_ACTION_REQUEST_MANIFEST))
                    operationResult = AirVpnDocumentRequest.MANIFEST_DOCUMENT_FAILED;
                else
                    operationResult = AirVpnDocumentRequest.USER_DOCUMENT_FAILED;

                return operationResult;
            }

            assocParamS.put("key", skey.getEncoded());
            assocParamS.put("iv", ivspec.getIV());

            try
            {
                rsaCipher.init(Cipher.ENCRYPT_MODE, publicKey);
            }
            catch(Exception e)
            {
                EddieLogger.error("RequestAirVPNDocument() Exception: %s", e);

                if(requestedAction.equals(AIRVPN_ACTION_REQUEST_MANIFEST))
                    operationResult = AirVpnDocumentRequest.MANIFEST_DOCUMENT_FAILED;
                else
                    operationResult = AirVpnDocumentRequest.USER_DOCUMENT_FAILED;

                return operationResult;
            }

            try
            {
                bytesParamS = rsaCipher.doFinal(hashMapToUtf8Bytes(assocParamS));
            }
            catch(Exception e)
            {
                EddieLogger.error("RequestAirVPNDocument(): %s", e.getMessage());

                if(requestedAction.equals(AIRVPN_ACTION_REQUEST_MANIFEST))
                    operationResult = AirVpnDocumentRequest.MANIFEST_DOCUMENT_FAILED;
                else
                    operationResult = AirVpnDocumentRequest.USER_DOCUMENT_FAILED;

                return operationResult;
            }

            aesDataIn = hashMapToUtf8Bytes(parameters);

            try
            {
                aesCipher.init(Cipher.ENCRYPT_MODE, skey, ivspec);
            }
            catch(Exception e)
            {
                EddieLogger.error("RequestAirVPNDocument() Exception: %s", e.getMessage());

                if(requestedAction.equals(AIRVPN_ACTION_REQUEST_MANIFEST))
                    operationResult = AirVpnDocumentRequest.MANIFEST_DOCUMENT_FAILED;
                else
                    operationResult = AirVpnDocumentRequest.USER_DOCUMENT_FAILED;

                return operationResult;
            }

            try
            {
                bytesParamD = aesCipher.doFinal(aesDataIn);
            }
            catch(Exception e)
            {
                EddieLogger.error("RequestAirVPNDocument() Exception: %s", e.getMessage());

                if(requestedAction.equals(AIRVPN_ACTION_REQUEST_MANIFEST))
                    operationResult = AirVpnDocumentRequest.MANIFEST_DOCUMENT_FAILED;
                else
                    operationResult = AirVpnDocumentRequest.USER_DOCUMENT_FAILED;

                return operationResult;
            }

            try
            {
                urlParameters = "s=";
                urlParameters += URLEncoder.encode(Base64.encodeToString(bytesParamS, Base64.NO_WRAP), "UTF-8");
                urlParameters += "&d=";
                urlParameters += URLEncoder.encode(Base64.encodeToString(bytesParamD, Base64.NO_WRAP), "UTF-8");
            }
            catch(Exception e)
            {
                EddieLogger.error("RequestAirVPNDocument() Exception: %s", e.getMessage());

                if(requestedAction.equals(AIRVPN_ACTION_REQUEST_MANIFEST))
                    operationResult = AirVpnDocumentRequest.MANIFEST_DOCUMENT_FAILED;
                else
                    operationResult = AirVpnDocumentRequest.USER_DOCUMENT_FAILED;

                return operationResult;
            }

            if(!settingsManager.getAirVPNCustomBootstrap().isEmpty())
            {
                ArrayList<AirVPNManifest.BootServer> manifestBootServer;
                int bootServerEntryIndex = 0, schemePosition;
                String path;

                EddieLogger.info("RequestAirVPNDocument(): Connection will use custom bootstrap servers");

                bootList = new ArrayList<AirVPNManifest.BootServer>();

                for(String bootserver : settingsManager.getAirVPNCustomBootstrapList())
                {
                    AirVPNManifest.BootServer customBootServer = new AirVPNManifest.BootServer();

                    if(!bootserver.substring(0, 6).toLowerCase(Locale.US).equals("http://"))
                        bootserver = "http://" + bootserver;

                    customBootServer.setUrl(bootserver);
                    customBootServer.setEntry(bootServerEntryIndex++);

                    schemePosition = bootserver.indexOf("://");

                    if(schemePosition > 0)
                        path = bootserver.substring(schemePosition + 3);
                    else
                        path = bootserver;

                    if(path.indexOf(":") > -1)
                        customBootServer.setIPv6(true);
                    else
                        customBootServer.setIPv6(false);

                    bootList.add(customBootServer);
                }

                manifestBootServer = AirVPNManifest.getBootStrapServerList();

                for(AirVPNManifest.BootServer bs : manifestBootServer)
                {
                    bs.setEntry(bootServerEntryIndex++);

                    bootList.add(bs);
                }
            }
            else
                bootList = AirVPNManifest.getBootStrapServerList();

            if(bootList == null || bootList.isEmpty())
            {
                EddieLogger.error("RequestAirVPNDocument(): Bootstrap Server list is empty");

                if(requestedAction.equals(AIRVPN_ACTION_REQUEST_MANIFEST))
                    operationResult = AirVpnDocumentRequest.MANIFEST_DOCUMENT_FAILED;
                else
                    operationResult = AirVpnDocumentRequest.USER_DOCUMENT_FAILED;

                return operationResult;
            }

            documentRetrieved = false;

            bootstrapServer = new ArrayList<AirVPNManifest.BootServer>();

            Collections.shuffle(bootList);

            if(bootList != null && bootList.isEmpty() == false)
            {
                for(int serverNdx = 0; serverNdx < bootList.size(); serverNdx++)
                {
                    if(bootList.get(serverNdx).getIpv6() == false)
                        bootstrapServer.add(bootList.get(serverNdx));
                    else if(bootServerIPv6Mode == true)
                        bootstrapServer.add(bootList.get(serverNdx));
                }
            }

            Collections.sort(bootstrapServer);

            for(int i = 0; i < bootstrapServer.size() && documentRetrieved == false; i++)
            {
                EddieLogger.info("Trying connection to AirVPN bootstrap server at %s", bootstrapServer.get(i).getUrl());

                try
                {
                    URL url = new URL(bootstrapServer.get(i).getUrl());

                    HttpURLConnection.setFollowRedirects(false);

                    httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setConnectTimeout(HTTP_CONNECTION_TIMEOUT);
                    httpURLConnection.setReadTimeout(HTTP_READ_TIMEOUT);
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    httpURLConnection.setRequestProperty("Content-Length", String.valueOf(urlParameters.getBytes().length));
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.getOutputStream().write(urlParameters.getBytes());

                    int res = httpURLConnection.getResponseCode();

                    if(res == HttpURLConnection.HTTP_OK)
                    {
                        Reader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream(), "UTF-8"));

                        InputStream inputStream = httpURLConnection.getInputStream();

                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        byte[] buffer = new byte[0xFFFF];

                        for(int len = inputStream.read(buffer, 0, buffer.length); len != -1; len = inputStream.read(buffer))
                            byteArrayOutputStream.write(buffer, 0, len);

                        aesDecipher.init(Cipher.DECRYPT_MODE, skey, ivspec);

                        xmlDocument = new String(aesDecipher.doFinal(byteArrayOutputStream.toByteArray()));

                        documentBuilderFactory = DocumentBuilderFactory.newInstance();

                        documentBuilder = documentBuilderFactory.newDocumentBuilder();

                        inputSource = new InputSource(new StringReader(xmlDocument));

                        airVpnDocument = documentBuilder.parse(inputSource);

                        documentRetrieved = true;
                    }
                }
                catch(SocketTimeoutException e)
                {
                    EddieLogger.error("RequestAirVPNDocument(): AirVPN server connection timeout");
                }
                catch(ConnectException e)
                {
                    EddieLogger.error("RequestAirVPNDocument(): Cannot connect to AirVPN server %s", bootstrapServer.get(i).getUrl());
                }
                catch(MalformedURLException e)
                {
                    EddieLogger.error("RequestAirVPNDocument(): Malformed URL at host entry for server %s", bootstrapServer.get(i).getUrl());
                }
                catch(IOException e)
                {
                    EddieLogger.error("RequestAirVPNDocument(): I/O Error while contacting server %s - Exception: %s", bootstrapServer.get(i).getUrl(), e);
                }
                catch(Exception e)
                {
                    EddieLogger.error("RequestAirVPNDocument(): Connection error while contacting server %s - Exception: %s", bootstrapServer.get(i).getUrl(), e);
                }
            }

            if(documentRetrieved)
            {
                if(requestedAction.equals(AIRVPN_ACTION_REQUEST_MANIFEST))
                    operationResult = AirVpnDocumentRequest.MANIFEST_DOCUMENT_SUCCESS;
                else
                    operationResult = AirVpnDocumentRequest.USER_DOCUMENT_SUCCESS;
            }
            else
            {
                EddieLogger.error("RequestAirVPNDocument(): cannot retrieve data from boot server");

                if(requestedAction.equals(AIRVPN_ACTION_REQUEST_MANIFEST))
                    operationResult = AirVpnDocumentRequest.MANIFEST_DOCUMENT_FAILED;
                else
                    operationResult = AirVpnDocumentRequest.USER_DOCUMENT_FAILED;
            }

            return operationResult;
        }

        @Override
        protected void onPostExecute(AirVpnDocumentRequest result)
        {
            switch(result)
            {
                case MANIFEST_DOCUMENT_SUCCESS:
                {
                    if(airVpnDocument != null)
                    {
                        EddieLogger.info("Successfully received %s document from AirVPN", requestedAction);

                        eddieEvent.onAirVPNManifestReceived(airVpnDocument);
                    }
                    else
                    {
                        EddieLogger.info("Received empty %s document from AirVPN", AIRVPN_ACTION_REQUEST_MANIFEST);

                        eddieEvent.onAirVPNManifestDownloadError();
                    }

                    manifestRequestPending = false;
                }
                break;

                case MANIFEST_DOCUMENT_FAILED:
                {
                    EddieLogger.error("RequestAirVPNDocument(): Cannot retrieve document %s from server", AIRVPN_ACTION_REQUEST_MANIFEST);

                    eddieEvent.onAirVPNManifestDownloadError();

                    manifestRequestPending = false;
                }
                break;

                case MANIFEST_DOCUMENT_IGNORED:
                {
                    eddieEvent.onAirVPNIgnoredManifestDocumentRequest();
                }
                break;

                case USER_DOCUMENT_SUCCESS:
                {
                    if(airVpnDocument != null)
                    {
                        EddieLogger.info("Successfully received %s document from AirVPN", requestedAction);

                        eddieEvent.onAirVPNUserProfileReceived(airVpnDocument);
                    }
                    else
                    {
                        EddieLogger.info("Received empty %s document from AirVPN", AIRVPN_ACTION_REQUEST_USER);

                        eddieEvent.onAirVPNUserProfileDownloadError();
                    }

                    userRequestPending = false;
                }
                break;

                case USER_DOCUMENT_FAILED:
                {
                    EddieLogger.error("RequestAirVPNDocument(): Cannot retrieve document %s from server", AIRVPN_ACTION_REQUEST_USER);

                    eddieEvent.onAirVPNUserProfileDownloadError();

                    userRequestPending = false;
                }
                break;

                case USER_DOCUMENT_IGNORED:
                {
                    eddieEvent.onAirVPNIgnoredUserDocumentRequest();
                }
                break;

                case ERROR:
                {
                    String message = "RequestAirVPNDocument(): Request error or malformed request";

                    EddieLogger.info(message);

                    eddieEvent.onAirVPNRequestError(message);
                }
                break;

                default:
                {
                }
                break;
            }
        }
    }

    private class SortableListViewArrayAdapter extends ArrayAdapter<SortableListViewItem>
    {
        private ArrayList<SortableListViewItem> sortableListViewItems = new ArrayList<SortableListViewItem>();
        private LayoutInflater layoutInflater;
        private int layoutTextViewResourceId;

        public SortableListViewArrayAdapter(Context context, int textViewResourceId)
        {
            super(context, textViewResourceId);

            if(context == null)
                return;

            layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            layoutTextViewResourceId = textViewResourceId;
        }

        @Override
        public void add(SortableListViewItem item)
        {
            super.add(item);

            sortableListViewItems.add(item);
        }

        @Override
        public void insert(SortableListViewItem item, int position)
        {
            super.insert(item, position);

            sortableListViewItems.add(position, item);

            sortableListView.invalidateViews();
        }

        @Override
        public void remove(SortableListViewItem item)
        {
            super.remove(item);

            sortableListViewItems.remove(item);

            sortableListView.invalidateViews();
        }

        public void move(int source, int destination)
        {
            SortableListViewItem copyItem;

            if(source < 0 || source >= sortableListViewItems.size() || destination < 0 || destination >= sortableListViewItems.size())
                return;

            if(source == destination)
                return;

            copyItem = sortableListViewItems.get(source);

            if(source < destination)
            {
                for(int i = source; i < destination; i++)
                    sortableListViewItems.set(i, sortableListViewItems.get(i + 1));
            }
            else
            {
                for(int i = source; i > destination; i--)
                    sortableListViewItems.set(i, sortableListViewItems.get(i - 1));
            }

            sortableListViewItems.set(destination, copyItem);

            sortableListView.invalidateViews();
        }

        @Override
        public void clear()
        {
            super.clear();

            sortableListViewItems.clear();

            sortableListView.invalidateViews();
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            View view = convertView;
            TextView txtTitle = null, txtPosition = null;

            if(view == null)
                view = layoutInflater.inflate(layoutTextViewResourceId, null);

            txtTitle = (TextView)view.findViewById(R.id.title);
            txtPosition = (TextView)view.findViewById(R.id.position);

            SortableListViewItem item = sortableListViewItems.get(position);

            if(item != null)
            {
                if(txtTitle != null)
                    txtTitle.setText(item.description);

                if(txtPosition != null)
                    txtPosition.setText(String.valueOf(position + 1));
            }
            return view;
        }

        public ArrayList<SortableListViewItem> getItems()
        {
            return sortableListViewItems;
        }
    }

    private View sortableListViewGetViewAtPosition(ListView listView, int position)
    {
        View view = null;

        if(listView == null)
            return null;

        int firstListItemPosition = listView.getFirstVisiblePosition();
        int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

        if(position < firstListItemPosition || position > lastListItemPosition )
            view = listView.getAdapter().getView(position, null, listView);
        else
            view = sortableListView.getChildAt(position - firstListItemPosition);

        return view;
    }

    public String getAirVpnCountryHostname(String countryCode, String tlsMode, boolean connectIPv6)
    {
        String countryHostname = "";

        if(countryCode.isEmpty() || tlsMode.isEmpty())
            return null;

        countryHostname = countryCode.toLowerCase(Locale.US);

        if(tlsMode.equals(SettingsManager.OPENVPN_TLS_MODE_CRYPT))
            countryHostname += "3";

        if(connectIPv6 && settingsManager.getAirVPNDefaultIPVersion().equals(SettingsManager.AIRVPN_IP_VERSION_6))
            countryHostname += ".ipv6";

        countryHostname += ".vpn.airdns.org";

        return countryHostname;
    }

    public static boolean isValidIpAddress(String ip)
    {
        boolean isValid = false;

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
            isValid = InetAddresses.isNumericAddress(ip);
        else
            isValid = Patterns.IP_ADDRESS.matcher(ip).matches();

        return isValid;
    }

    public static String buildStackTraceString(final StackTraceElement[] elements)
    {
        StringBuilder sb = new StringBuilder();

        if(elements != null && elements.length > 0)
        {
            for(StackTraceElement element : elements)
                sb.append(element.toString() + "\n");
        }

        return sb.toString();
    }

    public static boolean isTVDevice()
    {
        UiModeManager uiModeManager = (UiModeManager)EddieApplication.context().getSystemService(UI_MODE_SERVICE);

        return (uiModeManager.getCurrentModeType() == Configuration.UI_MODE_TYPE_TELEVISION);
    }

    public void sharePlainText(String text, String fileName, int res)
    {
        sharePlainText(text, fileName, EddieApplication.context().getResources().getString(res));
    }

    public void sharePlainText(String text, String fileName, String title)
    {
        String packageName = "";
        FileOutputStream saveFileStream = null;
        Uri uri = null;
        Intent shareIntent = null, chooserIntent = null;
        List<ResolveInfo> resolveInfoList = null;

        shareFile = new File(EddieApplication.context().getExternalFilesDir(null), fileName);

        try
        {
            saveFileStream = new FileOutputStream(shareFile);

            saveFileStream.write(text.getBytes());

            saveFileStream.close();
        }
        catch(Exception e)
        {
            EddieLogger.error("Failed to share file '%s'." + System.getProperty("line.separator") + "%s", fileName, e.getMessage());
        }

        uri = FileProvider.getUriForFile(EddieApplication.context(), EddieApplication.context().getApplicationContext().getPackageName() + ".provider", shareFile);

        shareIntent = new Intent(Intent.ACTION_SEND);

        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        shareIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

        chooserIntent = Intent.createChooser(shareIntent, title);

        resolveInfoList = EddieApplication.applicationContext().getPackageManager().queryIntentActivities(chooserIntent, PackageManager.MATCH_DEFAULT_ONLY);

        for(ResolveInfo resolveInfo : resolveInfoList)
        {
            packageName = resolveInfo.activityInfo.packageName;

            EddieApplication.applicationContext().grantUriPermission(packageName, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }

        EddieApplication.mainActivity().startActivity(chooserIntent);
    }

    public void removeShareFile()
    {
        if(shareFile != null)
        {
            shareFile.delete();

            shareFile = null;
        }
    }

    public String hexColorCode(int colorResource)
    {
        int color;
        String hexColor;

        try
        {
            color = EddieApplication.applicationContext().getResources().getColor(colorResource);
        }
        catch(Exception e)
        {
            EddieLogger.error("SupportTools.hexColorCode(): %d color resource not found", colorResource);

            color = 0;
        }

        hexColor = String.format("#%06X", 0xFFFFFF & color);

        return hexColor;
    }

    public boolean isNightModeOn()
    {
        return ((EddieApplication.applicationContext().getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK) == Configuration.UI_MODE_NIGHT_YES) ||
               (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) ||
               settingsManager.getSystemApplicationTheme().equals(SettingsManager.SYSTEM_OPTION_APPLICATION_THEME_DARK);
    }

    public synchronized boolean saveState(Bundle bundle)
    {
        File statusFile = null;
        FileOutputStream statusFileStream = null;

        if(bundle == null)
            return false;

        try
        {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("context", EddieApplication.baseContext().toString());

            for(String key : bundle.keySet())
            {
                Object object = bundle.get(key);

                if(object instanceof Integer || object instanceof String)
                    jsonObject.put(key, object);
                else
                    EddieLogger.warning("SupportTools.saveStatus(): Unknown data type '%s'", key);
            }

            statusFile = new File(EddieApplication.context().getExternalFilesDir(null), STATUS_FILE_NAME);

            statusFileStream = new FileOutputStream(statusFile);

            statusFileStream.write(jsonObject.toString().getBytes());

            statusFileStream.close();
        }
        catch(Exception e)
        {
            EddieLogger.error("SupportTools.saveStatus(): Failed to save status. %s", e.getMessage());

            return false;
        }

        return true;
    }

    public synchronized Bundle loadState()
    {
        Bundle bundle = null;
        JSONObject jsonObject = null;
        File statusFile = null;
        FileInputStream statusFileStream = null;
        BufferedReader bufferedReader = null;
        StringBuilder statusData = null;
        String line = "";

        try
        {
            statusFile = new File(EddieApplication.context().getExternalFilesDir(null), STATUS_FILE_NAME);
            statusFileStream = new FileInputStream(statusFile);

            bufferedReader = new BufferedReader(new InputStreamReader(statusFileStream));

            statusData = new StringBuilder();

            while((line = bufferedReader.readLine()) != null)
                statusData.append(line).append("\n");

            bufferedReader.close();
            statusFileStream.close();

            bundle = new Bundle();
            jsonObject = new JSONObject(statusData.toString());

            Iterator<?> keys = jsonObject.keys();

            while(keys.hasNext())
            {
                String key = (String)keys.next();

                Object object = jsonObject.get(key);

                if(object instanceof String)
                    bundle.putString(key, (String)object);
                else if(object instanceof Integer)
                    bundle.putInt(key, (Integer)object);
                else
                    EddieLogger.warning("SupportTools.loadStatus(): Unknown data type '%s'", key);
            }
        }
        catch(Exception e)
        {
            EddieLogger.error("SupportTools.loadStatus(): Failed to load status. %s", e.getMessage());

            return null;
        }

        if(bundle.containsKey("context"))
        {
            if(bundle.get("context").equals(EddieApplication.baseContext().toString()) == false)
            {
                statusFile.delete();

                return null;
            }
        }
        else
            return null;

        return bundle;
    }
}
