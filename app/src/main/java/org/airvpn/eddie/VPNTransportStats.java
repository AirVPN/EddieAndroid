// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 20 June 2018 - author: ProMIND - initial release. Based on revised code from com.eddie.android. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class VPNTransportStats
{
    public int resultCode;
    public String resultDescription;
    public long bytesIn;
	public long bytesOut;
	public long packetsIn;
	public long packetsOut;
	public int lastPacketReceived;

	// Wireguard

	public String privateKey;
	public String publicKey;
	public String presharedKey;
	public int listenPort;
	public int protocolVersion;
	public String endPoint;
	public long lastHandshakeTimeSec;
	public long lastHandshakeTimeNsec;
	public int persistentKeepaliveInterval;
	public String allowedIp;

	public VPNTransportStats()
	{
		init();
	}

	public VPNTransportStats(String s)
	{
		fromString(s);
	}

	private void init()
	{
		resultCode = EddieLibraryResult.ERROR;
		resultDescription = "";
		bytesIn = 0;
		bytesOut = 0;
		packetsIn = 0;
		packetsOut = 0;
		lastPacketReceived = 0;

		privateKey = "";
		publicKey = "";
		presharedKey = "";
		listenPort = -1;
		protocolVersion = -1;
		endPoint = "";
		lastHandshakeTimeSec = -1;
		lastHandshakeTimeNsec = -1;
		persistentKeepaliveInterval = -1;
		allowedIp = "";
	}

	public void fromString(String s)
	{
		List<String> item = Arrays.asList(s.split(";"));

		if(item.size() != 17)
		{
			init();

			return;
		}

		try
		{
			resultCode = Integer.parseInt(item.get(0));
		}
		catch(Exception e)
		{
			resultCode = -1;
		}

		resultDescription = item.get(1);

		try
		{
			bytesIn = Long.parseLong(item.get(2));
		}
		catch(Exception e)
		{
			bytesIn = 0;
		}

		try
		{
			bytesOut = Long.parseLong(item.get(3));
		}
		catch(Exception e)
		{
			bytesOut = 0;
		}

		try
		{
			packetsIn = Long.parseLong(item.get(4));
		}
		catch(Exception e)
		{
			packetsIn = 0;
		}

		try
		{
			packetsOut = Long.parseLong(item.get(5));
		}
		catch(Exception e)
		{
			packetsOut = 0;
		}

		try
		{
			lastPacketReceived = Integer.parseInt(item.get(6));
		}
		catch(Exception e)
		{
			lastPacketReceived = 0;
		}

		privateKey = item.get(7);
		publicKey = item.get(8);
		presharedKey = item.get(9);

		try
		{
			listenPort = Integer.parseInt(item.get(10));
		}
		catch(Exception e)
		{
			listenPort = -1;
		}

		try
		{
			protocolVersion = Integer.parseInt(item.get(11));
		}
		catch(Exception e)
		{
			protocolVersion = -1;
		}

		endPoint = item.get(12);

		try
		{
			lastHandshakeTimeSec = Long.parseLong(item.get(13));
		}
		catch(Exception e)
		{
			lastHandshakeTimeSec = 0;
		}

		try
		{
			lastHandshakeTimeNsec = Long.parseLong(item.get(14));
		}
		catch(Exception e)
		{
			lastHandshakeTimeNsec = 0;
		}

		try
		{
			persistentKeepaliveInterval = Integer.parseInt(item.get(15));
		}
		catch(Exception e)
		{
			lastHandshakeTimeSec = 0;
		}

		allowedIp = item.get(16);
	}

	public String toString()
	{
		return String.format(Locale.getDefault(), "%d;%s;%ld;%ld;%ld;%ld;%d;%s;%s;%s;%d;%d;%s;%ld;%ld;%d;%s",
			   	resultCode,
			   	resultDescription,
			   	bytesIn,
				bytesOut,
				packetsIn,
				packetsOut,
				lastPacketReceived,
				privateKey,
				publicKey,
				presharedKey,
				listenPort,
				protocolVersion,
				endPoint,
				lastHandshakeTimeSec,
				lastHandshakeTimeNsec,
				persistentKeepaliveInterval,
				allowedIp);
	}
}
