// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 7 January 2019 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.content.Context;
import android.util.Base64;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class VPNProfileDatabase
{
    public enum ProtocolType
    {
        UNKNOWN,
        UDPv4,
        TCPv4,
        UDPv6,
        TCPv6;

        public static ProtocolType fromInteger(int t)
        {
            ProtocolType result = UNKNOWN;

            switch(t)
            {
                case 0:
                {
                    result = UNKNOWN;
                }
                break;

                case 1:
                {
                    result = UDPv4;
                }
                break;

                case 2:
                {
                    result = TCPv4;
                }
                break;

                case 3:
                {
                    result = UDPv6;
                }
                break;

                case 4:
                {
                    result = TCPv6;
                }
                break;

                default:
                {
                    result = UNKNOWN;
                }
                break;
            }

            return result;
        }

        public int toInteger()
        {
            int result = 0;

            switch(this)
            {
                case UNKNOWN:
                {
                    result = 0;
                }
                break;

                case UDPv4:
                {
                    result = 1;
                }
                break;

                case TCPv4:
                {
                    result = 2;
                }
                break;

                case UDPv6:
                {
                    result = 3;
                }
                break;

                case TCPv6:
                {
                    result = 4;
                }
                break;

                default:
                {
                    result = 0;
                }
                break;
            }

            return result;
        }

        @Override
        public String toString()
        {
            String result = "";

            switch(this)
            {
                case UNKNOWN:
                {
                    result = "Unknown";
                }
                break;

                case UDPv4:
                {
                    result = "UDPv4";
                }
                break;

                case TCPv4:
                {
                    result = "TCPv4";
                }
                break;

                case UDPv6:
                {
                    result = "UDPv6";
                }
                break;

                case TCPv6:
                {
                    result = "TCPv6";
                }
                break;

                default:
                {
                    result = "Unknown";
                }
                break;
            }

            return result;
        }
    }

    public enum ProfileType
    {
        UNKNOWN,
        OPENVPN,
        WIREGUARD;

        public static ProfileType fromInteger(int t)
        {
            ProfileType result = UNKNOWN;

            switch(t)
            {
                case 0:
                {
                    result = UNKNOWN;
                }
                break;

                case 1:
                {
                    result = OPENVPN;
                }
                break;

                case 2:
                {
                    result = WIREGUARD;
                }
                break;

                default:
                {
                    result = UNKNOWN;
                }
                break;
            }

            return result;
        }

        public int toInteger()
        {
            int result = 0;

            switch(this)
            {
                case UNKNOWN:
                {
                    result = 0;
                }
                break;

                case OPENVPN:
                {
                    result = 1;
                }
                break;

                case WIREGUARD:
                {
                    result = 2;
                }
                break;

                default:
                {
                    result = 0;
                }
                break;
            }

            return result;
        }

        @Override
        public String toString()
        {
            String result = "";

            switch(this)
            {
                case UNKNOWN:
                {
                    result = "Unknown";
                }
                break;

                case OPENVPN:
                {
                    result = "OpenVPN";
                }
                break;

                case WIREGUARD:
                {
                    result = "WireGuard";
                }
                break;

                default:
                {
                    result = "Unknown";
                }
                break;
            }

            return result;
        }
    }

    public enum SortMode
    {
        NO_SORT,
        SORT_NAMES
    }

    private final String VPN_PROFILE_DATABASE_FILE_NAME = "VPNProfileDatabase.xml";
    private final String VPN_PROFILE_GROUP = "vpn_group";
    private final String VPN_PROFILE_ITEM = "vpn_profile";
    private final String VPN_PROFILE_NAME = "name";
    private final String VPN_PROFILE_TYPE = "type";
    private final String VPN_PROFILE_IP_ADDRESS = "ip_address";
    private final String VPN_PROFILE_PORT = "port";
    private final String VPN_PROFILE_PROTOCOL = "protocol";
    private final String VPN_PROFILE_PROFILE = "profile";
    private final String VPN_PROFILE_AIRVPN_SERVER_NAME = "airvpn_server_name";
    private final String VPN_PROFILE_AIRVPN_SERVER_LOCATION = "airvpn_server_location";
    private final String VPN_PROFILE_AIRVPN_SERVER_COUNTRY = "airvpn_server_country";
    private final String VPN_PROFILE_BOOT = "boot";

    private static HashMap<String, VPNProfile> profileDatabase = null;

    private Context appContext = null;

    private SupportTools supportTools = null;

    public class VPNProfile
    {
        private String name = "";
        private ProfileType type = ProfileType.UNKNOWN;
        private String ipAddress = "";
        private int port = 0;
        private ProtocolType protocol = ProtocolType.UNKNOWN;
        private String profile = "";
        private String airVPNServerName = "";
        private String airVPNServerLocation = "";
        private String airVPNServerCountry = "";
        private Boolean boot = false;

        public VPNProfile()
        {
            name = "";
            type = ProfileType.UNKNOWN;
            ipAddress = "";
            port = 0;
            protocol = ProtocolType.UNKNOWN;
            profile = "";
            airVPNServerName = "";
            airVPNServerLocation = "";
            airVPNServerCountry = "";
            boot = false;
        }

        public String getName()
        {
            return name;
        }

        public void setName(String n)
        {
            name = n;
        }

        public ProfileType getType()
        {
            return type;
        }

        public void setType(ProfileType p)
        {
            type = p;
        }

        public String getIpAddress()
        {
            return ipAddress;
        }

        public void setIpAddress(String s)
        {
            ipAddress = s;
        }

        public int getPort()
        {
            return port;
        }

        public void setPort(int p)
        {
            port = p;
        }

        public ProtocolType getProtocol()
        {
            return protocol;
        }

        public void setProtocol(ProtocolType p)
        {
            protocol = p;
        }

        public String getProfile()
        {
            return profile;
        }

        public void setProfile(String p)
        {
            profile = p;
        }

        public String getAirVPNServerName()
        {
            return airVPNServerName;
        }

        public void setAirVPNServerName(String p)
        {
            airVPNServerName = p;
        }

        public String getAirVPNServerLocation()
        {
            return airVPNServerLocation;
        }

        public void setAirVPNServerLocation(String p)
        {
            airVPNServerLocation = p;
        }

        public String getAirVPNServerCountry()
        {
            return airVPNServerCountry;
        }

        public void setAirVPNServerCountry(String p)
        {
            airVPNServerCountry = p;
        }

        public String getAirVPNServerDescription()
        {
            String description = "";

            if(!airVPNServerName.isEmpty())
            {
                description = airVPNServerName;

                if(airVPNServerLocation.isEmpty() == false)
                    description += " - " + airVPNServerLocation;

                if(airVPNServerCountry.isEmpty() == false);
                    description += ", " + airVPNServerCountry;
            }
            else
                description = "";

            return description;
        }

        public Boolean getBoot()
        {
            return boot;
        }

        public void setBoot(Boolean b)
        {
            boot = b;
        }
    }

    public VPNProfileDatabase(Context c)
    {
        appContext = c;

        supportTools = EddieApplication.supportTools();

        if(profileDatabase == null)
        {
            profileDatabase = new HashMap<String, VPNProfile>();

            loadOpenVPNProfileDatabase();
        }
    }

    public boolean exists(String n)
    {
        return profileDatabase.containsKey(n);
    }

    public VPNProfile getProfile(String n)
    {
        VPNProfile profile = null;

        if(profileDatabase.containsKey(n))
            profile = profileDatabase.get(n);

        return profile;
    }

    public void setProfile(String n, VPNProfile p)
    {
        setProfile(n, p, false);
    }

    public void setProfile(String n, VPNProfile p, boolean replace)
    {
        if(n.isEmpty() || p == null)
            return;

        if(replace == false && profileDatabase.containsKey(n))
            return;

        profileDatabase.put(n, p);

        saveOpenVPNProfileDatabase();
    }

    public Boolean setBoot(String key)
    {
        Boolean found = false;

        for(Map.Entry<String, VPNProfile> entry : profileDatabase.entrySet())
        {
            if(entry.getKey().equals(key))
            {
                entry.getValue().setBoot(true);

                found = true;
            }
            else
                entry.getValue().setBoot(false);

            profileDatabase.put(entry.getKey(), entry.getValue());
        }

        saveOpenVPNProfileDatabase();

        return found;
    }

    public Boolean unsetBoot(String key)
    {
        VPNProfile profile;
        Boolean found = false;

        if(profileDatabase.containsKey(key))
        {
            profile = getProfile(key);

            profile.setBoot(false);

            profileDatabase.put(key, profile);

            saveOpenVPNProfileDatabase();

            found = true;
        }

        return found;
    }

    public VPNProfile getStartupProfile()
    {
        VPNProfile bootProfile = null;

        for(Map.Entry<String, VPNProfile> entry : profileDatabase.entrySet())
        {
            if(entry.getValue().getBoot() == true)
                bootProfile = entry.getValue();
        }

        return bootProfile;
    }

    public boolean renameProfile(String oldName, String newName)
    {
        VPNProfile profile;
        boolean result = false;

        if(profileDatabase.containsKey(oldName))
        {
            profile = getProfile(oldName);

            profile.setName(newName);

            profileDatabase.remove(oldName);

            profileDatabase.put(newName, profile);

            saveOpenVPNProfileDatabase();

            result = true;
        }

        return result;
    }

    public boolean deleteProfile(String n)
    {
        boolean result = false;

        if(profileDatabase.containsKey(n))
        {
            profileDatabase.remove(n);

            saveOpenVPNProfileDatabase();

            result = true;
        }

        return result;
    }

    public ArrayList<String> getProfileNameList(SortMode sortMode)
    {
        ArrayList<String> profileList = new ArrayList<String>();

        for(Map.Entry<String, VPNProfile> entry : profileDatabase.entrySet())
            profileList.add(entry.getKey());

        if(sortMode == SortMode.SORT_NAMES)
            Collections.sort(profileList);

        return profileList;
    }

    public int size()
    {
        return profileDatabase.size();
    }

    private void loadOpenVPNProfileDatabase()
    {
        File openVPNProfileFile = null;
        Document openVPNProfileDocument = null;
        NodeList nodeList = null;
        NamedNodeMap namedNodeMap = null;
        byte[] b64 = null;
        String value;
        int val;

        if(appContext == null)
            return;

        openVPNProfileFile = new File(appContext.getFilesDir(), VPN_PROFILE_DATABASE_FILE_NAME);

        if(!openVPNProfileFile.exists())
            return;

        openVPNProfileDocument = supportTools.loadXmlFileToDocument(VPN_PROFILE_DATABASE_FILE_NAME);

        if(openVPNProfileDocument == null)
            return;

        nodeList = openVPNProfileDocument.getElementsByTagName(VPN_PROFILE_ITEM);

        if(nodeList != null && nodeList.getLength() > 0)
        {
            for(int i = 0; i < nodeList.getLength(); i++)
            {
                VPNProfile openVPNProfile = new VPNProfile();

                namedNodeMap = nodeList.item(i).getAttributes();

                if(namedNodeMap != null)
                {
                    openVPNProfile.setName(supportTools.getXmlItemNodeValue(namedNodeMap, VPN_PROFILE_NAME));

                    try
                    {
                        val = Integer.parseInt(supportTools.getXmlItemNodeValue(namedNodeMap, VPN_PROFILE_TYPE));

                        openVPNProfile.setType(ProfileType.fromInteger(val));
                    }
                    catch(NumberFormatException e)
                    {
                        openVPNProfile.setType(ProfileType.UNKNOWN);
                    }

                    openVPNProfile.setIpAddress(supportTools.getXmlItemNodeValue(namedNodeMap, VPN_PROFILE_IP_ADDRESS));

                    try
                    {
                        val = Integer.parseInt(supportTools.getXmlItemNodeValue(namedNodeMap, VPN_PROFILE_PORT));
                    }
                    catch(NumberFormatException e)
                    {
                        val = 0;
                    }

                    openVPNProfile.setPort(val);

                    try
                    {
                        val = Integer.parseInt(supportTools.getXmlItemNodeValue(namedNodeMap, VPN_PROFILE_PROTOCOL));

                        openVPNProfile.setProtocol(ProtocolType.fromInteger(val));
                    }
                    catch(NumberFormatException e)
                    {
                        openVPNProfile.setProtocol(ProtocolType.UNKNOWN);
                    }

                    value = supportTools.getXmlItemNodeValue(namedNodeMap, VPN_PROFILE_PROFILE);

                    if(!value.equals(""))
                    {
                        b64 = Base64.decode(value, Base64.NO_WRAP);

                        openVPNProfile.setProfile(new String(b64, StandardCharsets.UTF_8));
                    }
                    else
                        openVPNProfile.setProfile("");

                    openVPNProfile.setAirVPNServerName(supportTools.getXmlItemNodeValue(namedNodeMap, VPN_PROFILE_AIRVPN_SERVER_NAME));
                    openVPNProfile.setAirVPNServerLocation(supportTools.getXmlItemNodeValue(namedNodeMap, VPN_PROFILE_AIRVPN_SERVER_LOCATION));
                    openVPNProfile.setAirVPNServerCountry(supportTools.getXmlItemNodeValue(namedNodeMap, VPN_PROFILE_AIRVPN_SERVER_COUNTRY));

                    if(supportTools.getXmlItemNodeValue(namedNodeMap, VPN_PROFILE_BOOT).equals("true"))
                        openVPNProfile.setBoot(true);
                    else
                        openVPNProfile.setBoot(false);

                    profileDatabase.put(openVPNProfile.getName(), openVPNProfile);
                }
            }
        }
    }

    private boolean saveOpenVPNProfileDatabase()
    {
        DocumentBuilderFactory documentBuilderFactory = null;
        DocumentBuilder documentBuilder = null;
        Document profileDocument = null;

        try
        {
            documentBuilderFactory = DocumentBuilderFactory.newInstance();

            documentBuilder = documentBuilderFactory.newDocumentBuilder();
        }
        catch(ParserConfigurationException e)
        {
            return false;
        }

        profileDocument = documentBuilder.newDocument();

        if(profileDocument == null)
            return false;

        Element rootElement = profileDocument.createElement(VPN_PROFILE_GROUP);
        profileDocument.appendChild(rootElement);

        for(Map.Entry<String, VPNProfile> entry : profileDatabase.entrySet())
        {
            VPNProfile profile = entry.getValue();

            Element profileElement = profileDocument.createElement(VPN_PROFILE_ITEM);

            profileElement.setAttribute(VPN_PROFILE_NAME, profile.getName());
            profileElement.setAttribute(VPN_PROFILE_TYPE, String.format("%d", profile.getType().toInteger()));
            profileElement.setAttribute(VPN_PROFILE_IP_ADDRESS, profile.getIpAddress());
            profileElement.setAttribute(VPN_PROFILE_PORT, String.format("%d", profile.getPort()));
            profileElement.setAttribute(VPN_PROFILE_PROTOCOL, String.format("%d", profile.getProtocol().toInteger()));
            profileElement.setAttribute(VPN_PROFILE_PROFILE, Base64.encodeToString(profile.getProfile().getBytes(StandardCharsets.UTF_8), Base64.NO_WRAP));
            profileElement.setAttribute(VPN_PROFILE_AIRVPN_SERVER_NAME, profile.getAirVPNServerName());
            profileElement.setAttribute(VPN_PROFILE_AIRVPN_SERVER_LOCATION, profile.getAirVPNServerLocation());
            profileElement.setAttribute(VPN_PROFILE_AIRVPN_SERVER_COUNTRY, profile.getAirVPNServerCountry());
            profileElement.setAttribute(VPN_PROFILE_BOOT, profile.getBoot().toString());

            rootElement.appendChild(profileElement);
        }

        supportTools.saveXmlDocumentToFile(profileDocument, VPN_PROFILE_DATABASE_FILE_NAME);

        return true;
    }
}
