// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 12 October 2018 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.content.res.AssetManager;
import android.os.Bundle;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Locale;

public class AirVPNManifest implements NetworkStatusListener, EddieEventListener
{
    public enum ManifestType
    {
        NOT_SET,
        PROCESSING,
        DEFAULT,
        STORED,
        FROM_SERVER
    }

    public static final String AIRVPN_MANIFEST_FILE_NAME = "AirVPNManifest.dat";
    private static final int AIRVPN_MANIFEST_DEFAULT_UPDATE_INTERVAL_MINUTES = 15;

    private static Document airVPNManifestDocument = null;
    private static AirVPNServerGroup airVPNServerGroup = null;
    private static CipherDatabase cipherDatabase = null;

    private static ManifestType manifestType = ManifestType.NOT_SET;

    private static SupportTools supportTools = null;
    private static EddieEvent eddieEvent = null;
    private static NetworkStatusReceiver networkStatusReceiver = null;
    private static SettingsManager settingsManager = null;

    private static long processTimeTS = 0;
    private static long manifestTimeTS = 0;
    private static long manifestNextUpdateTS = 0;
    private static int nextUpdateIntervalMinutes = 0;
    private static ArrayList<Message> manifestMessage = null;
    private static ModeList manifestMode = null;
    private static String dnsCheckHost = "";
    private static String dnsCheckRes1 = "";
    private static String dnsCheckRes2 = "";
    private static double speedFactor = 1.0;
    private static double loadFactor = 1.0;
    private static double userFactor = 1.0;
    private static int latencyFactor = 0;
    private static int penalityFactor = 0;
    private static int pingerDelay = 0;
    private static int pingerRetry = 0;
    private static String checkDomain = "";
    private static String checkDnsQuery = "";
    private static String openVpnDirectives = "";
    private static String modeProtocol = "";
    private static int modePort = 0;
    private static int modeAlt = 0;
    private static ArrayList<BootServer> bootStrapServerList = null, bootStrapServerBackupList = null;
    private static ArrayList<AirVPNServer> airVpnServerList = null;
    private static ArrayList<String> openVPNPortList = null;
    private static ArrayList<String> wireGuardPortList = null;
    private static int bootServerEntryIndex = 0;
    private static HashMap<String, CountryStats> countryStats = null;
    private static HashMap<String, ContinentStats> continentStats = null;
    private static String rsaPublicKeyModulus = "", rsaPublicKeyModulusBackup = "";
    private static String rsaPublicKeyExponent = "", rsaPublicKeyExponentBackup = "";
    private static boolean refreshPending = false;

    public static class BootServer implements Comparable<BootServer>
    {
        private String url;
        private int entry;
        private Boolean ipv6;

        void setUrl(String u)
        {
            url = u;
        }

        String getUrl()
        {
            return url;
        }

        void setEntry(int e)
        {
            entry = e;
        }

        int getEntry()
        {
            return entry;
        }

        void setIPv6(Boolean i)
        {
            ipv6 = i;
        }

        Boolean getIpv6()
        {
            return ipv6;
        }

        public int compareTo(BootServer s)
        {
            int result = 0;

            if(ipv6 == s.ipv6 && entry < s.entry)
                result = -1;
            else if(ipv6 == false && s.ipv6 == true)
                result = 1;
            else if(ipv6 == true && s.ipv6 == false)
                return -1;

            return result;
        }
    };

    public class Message
    {
        private String text = "";
        private String url = "";
        private String link = "";
        private String html = "";
        private long fromUTCTimeTS = 0;
        private long toUTCTimeTS = 0;
        private boolean shown = false;

        public void setText(String s)
        {
            text = s;
        }

        public String getText()
        {
            return text;
        }

        public void setUrl(String u)
        {
            url = u;
        }

        public String getUrl()
        {
            return url;
        }

        public void setLink(String l)
        {
            link = l;
        }

        public String getLink()
        {
            return link;
        }

        public void setHtml(String h)
        {
            html = h;
        }

        public String getHtml()
        {
            return html;
        }

        public void setFromUTCTimeTS(long ts)
        {
            fromUTCTimeTS = ts;
        }

        public long getFromUTCTimeTS()
        {
            return fromUTCTimeTS;
        }

        public void setToUTCTimeTS(long ts)
        {
            toUTCTimeTS = ts;
        }

        public long getToUTCTimeTS()
        {
            return toUTCTimeTS;
        }

        public void setShown(boolean s)
        {
            shown = s;
        }

        public boolean isShown()
        {
            return shown;
        }

        public void setDoNotShowAgain(boolean s)
        {
            if(s == true)
                settingsManager.addDoNotShowAgainBanners(toUTCTimeTS);
            else
                settingsManager.removeDoNotShowAgainBanners(toUTCTimeTS);
        }

        public boolean isToBeShownAgain()
        {
            ArrayList<Long> shownBanner = settingsManager.getDoNotShowAgainBanners();

            return !shownBanner.contains(toUTCTimeTS);
        }
    }

    public class Mode
    {
        private int code = 0;
        private String title = "";
        private String protocol="";
        private String tlsVersion = "";
        private String tlsMode = "";
        private int port = -1;
        private int entryIndex = -1;
        private String specifications = "";
        private VPN.Type vpnType = VPN.Type.UNKNOWN;
        private String openVpnAuth = "";
        private String openVpnKeyDirection = "";
        private String openVpnMinVersion = "";
        private String openVpnDirectives = "";
        private String sshDestination = "";

        public void setTitle(String t)
        {
            if(t.isEmpty() == true)
                return;

            title = t;
        }

        public int getCode()
        {
            return code;
        }

        public String getTitle()
        {
            return title;
        }

        public void setProtocol(String p)
        {
            if(p.equals(SettingsManager.AIRVPN_PROTOCOL_UDP) == false && p.equals(SettingsManager.AIRVPN_PROTOCOL_TCP) == false)
                return;

            protocol = p;
        }

        public String getProtocol()
        {
            return protocol;
        }

        public void setTlsVersion(String v)
        {
            if(v.equals(SettingsManager.OPENVPN_TLS_1_0) == false && v.equals(SettingsManager.OPENVPN_TLS_1_1) == false &&
               v.equals(SettingsManager.OPENVPN_TLS_1_2) == false && v.equals(SettingsManager.OPENVPN_TLS_1_3) == false)
                return;

            tlsVersion = v;
        }

        public String getTlsVersion()
        {
            return tlsVersion;
        }

        public void setTlsMode(String m)
        {
            if(m.equals(SettingsManager.OPENVPN_TLS_MODE_AUTH) == false && m.equals(SettingsManager.OPENVPN_TLS_MODE_CRYPT) == false)
                return;

            tlsMode = m;
        }

        public String getTlsMode()
        {
            return tlsMode;
        }

        public void setPort(int p)
        {
            if(p <1 || p > 65535)
                return;

            port = p;
        }

        public int getPort()
        {
            return port;
        }

        public void setEntryIndex(int e)
        {
            if(e < 0 || e > 3)
                return;

            entryIndex = e;
        }

        public int getEntryIndex()
        {
            return entryIndex;
        }

        public void setSpecifications(String s)
        {
            specifications = s;
        }

        public String getSpecifications()
        {
            return specifications;
        }

        public void setVpnType(VPN.Type t)
        {
            vpnType = t;
        }

        public VPN.Type getVpnType()
        {
            return vpnType;
        }

        public void setOpenVpnAuth(String v)
        {
            openVpnAuth = v;
        }

        public String getOpenVpnAuth()
        {
            return openVpnAuth;
        }

        public void setOpenVpnKeyDirection(String v)
        {
            openVpnKeyDirection = v;
        }

        public String getOpenVpnKeyDirection()
        {
            return openVpnKeyDirection;
        }

        public void setOpenVpnMinVersion(String v)
        {
            openVpnMinVersion = v;
        }

        public String getOpenVpnMinVersion()
        {
            return openVpnMinVersion;
        }

        public void setOpenVpnDirectives(String d)
        {
            openVpnDirectives = d;
        }

        public String getOpenVpnDirectives()
        {
            return openVpnDirectives;
        }

        public void setSshDestination(String d)
        {
            sshDestination = d;
        }

        public String getSshDestination()
        {
            return sshDestination;
        }
    }

    public class ModeList extends ArrayList<Mode>
    {
        public boolean add(Mode m, int c)
        {
            boolean valid = true;

            for(Mode cm : this)
            {
                if(cm.code == c)
                    valid = false;
            }

            if(valid == true)
            {
                m.code = c;

                valid = super.add(m);
            }

            return valid;
        }

        @Override
        public boolean add(Mode m)
        {
            int c = 0;

            for(Mode cm : this)
            {
                if(cm.code > c)
                    c = cm.code;
            }

            m.code = c + 1;

            return super.add(m);
        }

        public Mode getMode(int c)
        {
            Mode m = null;

            for(Mode cm : this)
            {
                if(cm.code > c)
                    m = cm;
            }

            return m;
        }

        public Mode getMode(String t)
        {
            Mode m = null;

            for(Mode cm : this)
            {
                if(cm.title.equalsIgnoreCase(t) == true)
                    m = cm;
            }

            return m;
        }

        public ModeList getModes(VPN.Type t)
        {
            ModeList m = null;
            boolean include = false;

            for(Mode mode : this)
            {
                if(t == VPN.Type.UNKNOWN)
                    include = true;
                else if(mode.vpnType == t)
                    include = true;
                else
                    include = false;

                if(include == true)
                {
                    if(m == null)
                        m = new ModeList();

                    m.add(mode, mode.code);
                }
            }

            return m;
        }
    }

    public class CountryStats
    {
        private String countryISOCode = "";
        private int servers = 0;
        private int users = 0;
        private long bandWidth;
        private long maxBandWidth;

        public CountryStats()
        {
            countryISOCode = "";
            servers = 0;
            users = 0;
            bandWidth = 0;
            maxBandWidth = 0;
        }

        public void setCountryISOCode(String code)
        {
            countryISOCode = code;
        }

        public String getCountryISOCode()
        {
            return countryISOCode;
        }

        public void setServers(int s)
        {
            servers = s;
        }

        public int getServers()
        {
            return servers;
        }

        public void setUsers(int u)
        {
            users = u;
        }

        public int getUsers()
        {
            return users;
        }

        public long getBandWidth()
        {
            return bandWidth;
        }

        public void setBandWidth(long b)
        {
            bandWidth = b;
        }

        public long getMaxBandWidth()
        {
            return maxBandWidth;
        }

        public void setMaxBandWidth(long b)
        {
            maxBandWidth = b;
        }

        public int getLoad()
        {
            return supportTools.getTrafficLoad(bandWidth, maxBandWidth);
        }
    }

    public class ContinentStats
    {
        private String continentCode = "";
        private int servers = 0;
        private int users = 0;
        private long bandWidth;
        private long maxBandWidth;

        public ContinentStats()
        {
            continentCode = "";
            servers = 0;
            users = 0;
            bandWidth = 0;
            maxBandWidth = 0;
        }

        public void setContinentCode(String code)
        {
            continentCode = code;
        }

        public String getContinentCode()
        {
            return continentCode;
        }

        public void setServers(int s)
        {
            servers = s;
        }

        public int getServers()
        {
            return servers;
        }

        public void setUsers(int u)
        {
            users = u;
        }

        public int getUsers()
        {
            return users;
        }

        public long getBandWidth()
        {
            return bandWidth;
        }

        public void setBandWidth(long b)
        {
            bandWidth = b;
        }

        public long getMaxBandWidth()
        {
            return maxBandWidth;
        }

        public void setMaxBandWidth(long b)
        {
            maxBandWidth = b;
        }

        public int getLoad()
        {
            return supportTools.getTrafficLoad(bandWidth, maxBandWidth);
        }
    }

    class CompareStringAsInteger implements Comparator<String>
    {
        public int compare(String o1, String o2)
        {
            return new Integer(o1).compareTo(new Integer(o2));
        }
    }

    public AirVPNManifest()
    {
        if(supportTools == null)
            supportTools = EddieApplication.supportTools();

        if(airVPNServerGroup == null)
            airVPNServerGroup = new AirVPNServerGroup();

        if(cipherDatabase == null)
            cipherDatabase = new CipherDatabase();

        if(settingsManager == null)
            settingsManager = EddieApplication.settingsManager();

        if(networkStatusReceiver == null)
        {
            networkStatusReceiver = EddieApplication.networkStatusReceiver();

            networkStatusReceiver.subscribeListener(this);
        }

        if(eddieEvent == null)
        {
            eddieEvent = EddieApplication.eddieEvent();

            eddieEvent.subscribeListener(this);
        }

        if(processTimeTS == 0)
        {
            processTimeTS = System.currentTimeMillis() / 1000;

            Document manifestDocument = supportTools.stringToXmlDocument(getDefaultManifest());

            manifestType = ManifestType.DEFAULT;

            processXmlManifest(manifestDocument);

            loadManifest();
        }

        refreshPending = false;
    }

    @Override
    protected void finalize() throws Throwable
    {
        try
        {
            if(networkStatusReceiver != null)
                networkStatusReceiver.unsubscribeListener(this);

            if(eddieEvent != null)
                eddieEvent.unsubscribeListener(this);
        }
        catch(Exception e)
        {
            EddieLogger.error("AirVPNManifest.finalize() Exception: %s", e);
        }
        finally
        {
            try
            {
                super.finalize();
            }
            catch(Exception e)
            {
                EddieLogger.error("AirVPNManifest.finalize() Exception: %s", e);
            }
        }
    }

    public static boolean isEncrypted()
    {
        boolean result = false;

        try
        {
            result = SupportTools.isFileXML(AIRVPN_MANIFEST_FILE_NAME);
        }
        catch(FileNotFoundException e)
        {
            return false;
        }

        return !result;
    }

    public ArrayList<Message> getMessages()
    {
        return manifestMessage;
    }

    public void setMessageShown(int m)
    {
        if(m >= manifestMessage.size())
            return;

        manifestMessage.get(m).setShown(true);
    }

    public Mode getMode(int c)
    {
        return manifestMode.getMode(c);
    }

    public Mode getMode(String t)
    {
        return manifestMode.getMode(t);
    }

    public ModeList getModes()
    {
        return getModes(VPN.Type.UNKNOWN);
    }

    public ModeList getModes(VPN.Type t)
    {
        return manifestMode.getModes(t);
    }

    public void setDoNotShowAgain(int m)
    {
        if(m >= manifestMessage.size())
            return;

        manifestMessage.get(m).setDoNotShowAgain(true);
    }

    public long getManifestTimeTS()
    {
        return manifestTimeTS;
    }

    public void setManifestTimeTS(long t)
    {
        manifestTimeTS = t;
    }

    public long getManifestNextUpdateTS()
    {
        return manifestNextUpdateTS;
    }

    public void setManifestNextUpdateTS(long t)
    {
        manifestNextUpdateTS = t;
    }

    public int getNextUpdateIntervalMinutes()
    {
        return nextUpdateIntervalMinutes;
    }

    public void setNextUpdateIntervalMinutes(int n)
    {
        nextUpdateIntervalMinutes = n;
    }

    public String getDnsCheckHost()
    {
        return dnsCheckHost;
    }

    public void setDnsCheckHost(String d)
    {
        dnsCheckHost = d;
    }

    public String getDnsCheckRes1()
    {
        return dnsCheckRes1;
    }

    public void setDnsCheckRes1(String d)
    {
        dnsCheckRes1 = d;
    }

    public String getDnsCheckRes2()
    {
        return dnsCheckRes2;
    }

    public void setDnsCheckRes2(String d)
    {
        dnsCheckRes2 = d;
    }

    public static double getSpeedFactor()
    {
        return speedFactor;
    }

    public void setSpeedFactor(double s)
    {
        speedFactor = s;
    }

    public static double getLoadFactor()
    {
        return loadFactor;
    }

    public void setLoadFactor(double l)
    {
        loadFactor = l;
    }

    public static double getUserFactor()
    {
        return userFactor;
    }

    public void setUserFactor(double u)
    {
        userFactor = u;
    }

    public int getLatencyFactor()
    {
        return latencyFactor;
    }

    public void setLatencyFactor(int l)
    {
        latencyFactor = l;
    }

    public int getPenalityFactor()
    {
        return penalityFactor;
    }

    public void setPenalityFactor(int p)
    {
        penalityFactor = p;
    }

    public int getPingerDelay()
    {
        return pingerDelay;
    }

    public void setPingerDelay(int p)
    {
        pingerDelay = p;
    }

    public int getPingerRetry()
    {
        return pingerRetry;
    }

    public void setPingerRetry(int p)
    {
        pingerRetry = p;
    }

    public String getCheckDomain()
    {
        return checkDomain;
    }

    public void setCheckDomain(String c)
    {
        checkDomain = c;
    }

    public static String getCheckDnsQuery()
    {
        return checkDnsQuery;
    }

    public void setCheckDnsQuery(String c)
    {
        checkDnsQuery = c;
    }

    public String getOpenVpnDirectives()
    {
        return openVpnDirectives;
    }

    public void setOpenVpnDirectives(String o)
    {
        openVpnDirectives = o;
    }

    public String getModeProtocol()
    {
        return modeProtocol;
    }

    public void setModeProtocol(String m)
    {
        modeProtocol = m;
    }

    public int getModePort()
    {
        return modePort;
    }

    public void setModePort(int m)
    {
        modePort = m;
    }

    public int getModeAlt()
    {
        return modeAlt;
    }

    public void setModeAlt(int m)
    {
        modeAlt = m;
    }

    public static ArrayList<BootServer> getBootStrapServerList()
    {
        if(bootStrapServerList == null || bootStrapServerList.isEmpty())
            return bootStrapServerBackupList;

        return bootStrapServerList;
    }

    public void setBootStrapServerList(ArrayList<BootServer> b)
    {
        bootStrapServerList = b;

        bootStrapServerBackupList = b;
    }

    void resetBootServers()
    {
        if(bootStrapServerList == null)
            bootStrapServerList = new ArrayList<BootServer>();

        bootServerEntryIndex = 0;

        bootStrapServerList.clear();
    }

    public void addBootStrapServer(String url)
    {
        String path;
        BootServer bootServer;
        int schemePosition;

        if(bootStrapServerList == null)
            bootStrapServerList = new ArrayList<BootServer>();

        bootServer = new BootServer();

        bootServer.url = url;
        bootServer.entry = bootServerEntryIndex++;

        schemePosition = url.indexOf("://");

        if(schemePosition > 0)
            path = url.substring(schemePosition + 3);
        else
            path = url;

        if(path.indexOf(":") > -1)
            bootServer.ipv6 = true;
        else
            bootServer.ipv6 = false;

        bootStrapServerList.add(bootServer);
    }

    public ArrayList<AirVPNServer> getAirVpnServerList()
    {
        return airVpnServerList;
    }

    public void setAirVpnServerList(ArrayList<AirVPNServer> a)
    {
        airVpnServerList = a;
    }

    public synchronized void addAirVpnServer(AirVPNServer server)
    {
        if(airVpnServerList == null)
            airVpnServerList = new ArrayList<AirVPNServer>();

        if(server == null)
            return;

        airVpnServerList.add(server);
    }

    public static ArrayList<String> getOpenVPNPortList()
    {
        if(openVPNPortList == null)
            openVPNPortList = new ArrayList<String>();

        if(openVPNPortList.size() == 0)
            Collections.addAll(openVPNPortList, EddieApplication.context().getResources().getStringArray(R.array.airvpn_openvpn_port_values));

        return openVPNPortList;
    }

    public static ArrayList<String> getWireGuardPortList()
    {
        if(wireGuardPortList == null)
            wireGuardPortList = new ArrayList<String>();

        if(wireGuardPortList.size() == 0)
            Collections.addAll(wireGuardPortList, EddieApplication.context().getResources().getStringArray(R.array.airvpn_wireguard_port_values));

        return wireGuardPortList;
    }

    public static HashMap<String, ContinentStats> getContinentStats()
    {
        return continentStats;
    }

    public static HashMap<String, CountryStats> getCountryStats()
    {
        return countryStats;
    }

    public static ArrayList<AirVPNServer> getServerListByCountry(String code)
    {
        if(airVpnServerList == null)
            return null;

        AirVPNServer server = null;
        ArrayList<AirVPNServer> serverList = new ArrayList<AirVPNServer>();

        for(int i = 0; i < airVpnServerList.size(); i++)
        {
            server = airVpnServerList.get(i);

            if(server != null && server.getCountryCode().equals(code))
                serverList.add(server);
        }

        return serverList;
    }

    public static AirVPNServer getServerByName(String name)
    {
        if(airVpnServerList == null)
            return null;

        AirVPNServer server = null;

        for(int i = 0; i < airVpnServerList.size() && server == null; i++)
        {
            if(airVpnServerList.get(i).getName().equals(name))
                server = airVpnServerList.get(i);
        }

        return server;
    }

    public static AirVPNServer getServerByIP(String ip)
    {
        AirVPNServer server = null;
        HashMap<Integer, String> serverIP = null;

        if(airVpnServerList == null)
            return null;

        for(int i = 0; i < airVpnServerList.size() && server == null; i++)
        {
            serverIP = airVpnServerList.get(i).getEntryIPv4();

            if(serverIP != null)
            {
                for(String entryIP : serverIP.values())
                {
                    if(entryIP.equals(ip))
                        server = airVpnServerList.get(i);
                }
            }

            if(server == null)
            {
                serverIP = airVpnServerList.get(i).getEntryIPv6();

                if(serverIP != null)
                {
                    for (String entryIP : serverIP.values())
                    {
                        if(entryIP.equals(ip))
                            server = airVpnServerList.get(i);
                    }
                }
            }
        }

        return server;
    }

    public static String getFullServerDescription(String name)
    {
        CountryContinent countryContinent = EddieApplication.countryContinent();

        String description = "";

        if(name.isEmpty())
            return "";

        AirVPNServer airVPNServer = getServerByName(name);

        if(airVPNServer != null)
        {
            description = String.format("%s - %s", airVPNServer.getName(), airVPNServer.getLocation());

            if(!countryContinent.getCountryName(airVPNServer.getCountryCode()).isEmpty())
                description += ", " + countryContinent.getCountryName(airVPNServer.getCountryCode());
        }
        else
            description = "";

        return description;
    }

    public static String getFullServerDescriptionByIP(String ip)
    {
        CountryContinent countryContinent = EddieApplication.countryContinent();

        String description = "";

        if(ip.isEmpty())
            return "";

        AirVPNServer airVPNServer = getServerByIP(ip);

        if(airVPNServer != null)
            description = String.format("%s - %s, %s", airVPNServer.getName(), airVPNServer.getLocation(), countryContinent.getCountryName(airVPNServer.getCountryCode()));
        else
            description = "";

        return description;
    }

    public static String getRsaPublicKeyModulus()
    {
        if(rsaPublicKeyModulus.isEmpty())
            return rsaPublicKeyModulusBackup;

        return rsaPublicKeyModulus;
    }

    public void setRsaPublicKeyModulus(String r)
    {
        rsaPublicKeyModulus = r;

        rsaPublicKeyModulusBackup = r;
    }

    public static String getRsaPublicKeyExponent()
    {
        if(rsaPublicKeyExponent.isEmpty())
            return rsaPublicKeyExponentBackup;

        return rsaPublicKeyExponent;
    }

    public void setRsaPublicKeyExponent(String r)
    {
        rsaPublicKeyExponent = r;

        rsaPublicKeyExponentBackup = r;
    }

    public static ManifestType getManifestType()
    {
        return manifestType;
    }

    private void initializeManifestData()
    {
        processTimeTS = 0;
        manifestTimeTS = 0;
        manifestNextUpdateTS = 0;
        nextUpdateIntervalMinutes = 0;
        dnsCheckHost = "";
        dnsCheckRes1 = "";
        dnsCheckRes2 = "";
        speedFactor = 1.0;
        loadFactor = 1.0;
        userFactor = 1.0;
        latencyFactor = 0;
        penalityFactor = 0;
        pingerDelay = 0;
        pingerRetry = 0;
        checkDomain = "";
        checkDnsQuery = "";
        openVpnDirectives = "";
        modeProtocol = "";
        modePort = 0;
        modeAlt = 0;
        bootStrapServerList = null;
        airVpnServerList = null;
        rsaPublicKeyModulus = "";
        rsaPublicKeyExponent = "";
    }

    private void loadManifest()
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                Document manifestDocument = null;
                File manifestFile = null;

                if((!rsaPublicKeyModulus.isEmpty() && !rsaPublicKeyExponent.isEmpty()) && supportTools.isNetworkConnectionActive() == true)
                {
                    HashMap<String, String> parameters = new HashMap<String, String>();

                    if(EddieApplication.vpnManager().vpn().getConnectionStatus() != VPN.Status.CONNECTING && EddieApplication.vpnManager().isVpnConnectionReserved() == false)
                    {
                        parameters.put("login", AirVPNUser.getUserName());
                        parameters.put("password", AirVPNUser.getUserPassword());
                        parameters.put("act", "manifest");

                        if(settingsManager.getAirVPNDefaultIPVersion().equals(SettingsManager.AIRVPN_IP_VERSION_6))
                            SupportTools.setBootServerIPv6Mode(true);
                        else
                            SupportTools.setBootServerIPv6Mode(false);

                        SupportTools.RequestAirVPNDocument manifestRequest = supportTools.new RequestAirVPNDocument();

                        manifestRequest.execute(parameters);
                    }
                    else
                        refreshPending = true;
                }
                else if(!AirVPNUser.masterPassword().isEmpty() && settingsManager.isMasterPasswordEnabled() == true)
                {
                    if(AirVPNUser.getUserName().isEmpty() || AirVPNUser.getUserPassword().isEmpty())
                    {
                        manifestFile = new File(EddieApplication.context().getFilesDir(), AIRVPN_MANIFEST_FILE_NAME);

                        if(manifestFile.exists())
                        {
                            manifestDocument = supportTools.decryptFileToXmlDocument(AIRVPN_MANIFEST_FILE_NAME, AirVPNUser.masterPassword());

                            manifestType = ManifestType.STORED;
                        }
                        else
                        {
                            manifestDocument = supportTools.stringToXmlDocument(getDefaultManifest());

                            manifestType = ManifestType.DEFAULT;
                        }

                        processXmlManifest(manifestDocument);
                    }
                }
                else if(settingsManager.isMasterPasswordEnabled() == false)
                {
                    manifestFile = new File(EddieApplication.context().getFilesDir(), AIRVPN_MANIFEST_FILE_NAME);

                    if(manifestFile.exists())
                    {
                        manifestDocument = supportTools.loadXmlFileToDocument(AIRVPN_MANIFEST_FILE_NAME);

                        manifestType = ManifestType.STORED;
                    }

                    processXmlManifest(manifestDocument);
                }
                else
                {
                    if(manifestType != ManifestType.DEFAULT && manifestType != ManifestType.PROCESSING)
                    {
                        manifestDocument = supportTools.stringToXmlDocument(getDefaultManifest());

                        manifestType = ManifestType.DEFAULT;

                        processXmlManifest(manifestDocument);
                    }
                }
            }
        };

        supportTools.startThread(runnable);
    }

    private String getDefaultManifest()
    {
        AssetManager assetManager = EddieApplication.context().getAssets();
        InputStream inputStream = null;
	    String manifest = "";

        try
        {
	        inputStream = assetManager.open("manifest.xml");

            int size = inputStream.available();
            byte[] buffer = new byte[size];
	        inputStream.read(buffer);
	        inputStream.close();

	        manifest = new String(buffer);
	    }
	    catch(Exception e)
        {
            EddieLogger.warning("AirVPNManifest.getDefaultManifest() Exception: %s", e);

            manifest = "";
	    }

	    return manifest;
    }

    public void refreshManifestFromAirVPN()
    {
        if(!AirVPNUser.isUserValid())
            return;

        if(supportTools.isNetworkConnectionActive() == false || EddieApplication.vpnManager().vpn().getConnectionStatus() == VPN.Status.CONNECTING || EddieApplication.vpnManager().isVpnConnectionReserved() == true)
        {
            refreshPending = true;

            return;
        }

        if(!rsaPublicKeyModulus.isEmpty() && !rsaPublicKeyExponent.isEmpty())
        {
            EddieLogger.info("Refreshing AirVPN manifest");

            HashMap<String, String> parameters = new HashMap<String, String>();

            parameters.put("login", AirVPNUser.getUserName());
            parameters.put("password", AirVPNUser.getUserPassword());
            parameters.put("act", "manifest");

            if(settingsManager.getAirVPNDefaultIPVersion().equals(SettingsManager.AIRVPN_IP_VERSION_6))
                SupportTools.setBootServerIPv6Mode(true);
            else
                SupportTools.setBootServerIPv6Mode(false);

            SupportTools.RequestAirVPNDocument manifestRequest = supportTools.new RequestAirVPNDocument();

            manifestRequest.execute(parameters);

            refreshPending = false;
        }
    }

    private void processXmlManifest(Document manifestDocument)
    {
        NodeList nodeList = null;
        NamedNodeMap namedNodeMap = null;
        ManifestType newManifestType = manifestType;
        ArrayList<AirVPNServer> localVpnServerList = new ArrayList<AirVPNServer>();
        AirVPNServerGroup localVpnServerGroup = null;
        AirVPNServerGroup.ServerGroup serverGroup = null;
        AirVPNServer localAirVPNServer = null;
        CountryStats cStats = null;
        ContinentStats conStats = null;
        String value, row[], row1[], logMessage = "", vpnModeType, vpnModeProtocol, vpnModePort;
        int intVal = 0, i = 0, j = 0, group = -1;
        long longVal = 0;
        boolean found = false;

        if(manifestDocument == null)
        {
            EddieLogger.error("AirVPNManifest.processXmlManifest(): manifestDocument is null.");

            return;
        }

        newManifestType = manifestType;

        manifestType = ManifestType.PROCESSING;

        switch(newManifestType)
        {
            case DEFAULT:
            {
                logMessage = "Setting manifest to default instance";
            }
            break;

            case STORED:
            {
                logMessage = "Setting manifest to the locally stored instance";
            }
            break;

            case FROM_SERVER:
            {
                logMessage = "Setting manifest to the instance downloaded from AirVPN server";
            }
            break;

            case PROCESSING:
            {
            }
            break;

            default:
            {
                logMessage = "Manifest type is not set. Using current manifest instance (if any).";
            }
            break;
        }

        EddieLogger.info(logMessage);

        if(newManifestType == ManifestType.NOT_SET)
        {
            eddieEvent.onAirVPNManifestChanged();

            manifestType = newManifestType;

            return;
        }

        processTimeTS = System.currentTimeMillis()/1000;

        countryStats = new HashMap<String, CountryStats>();
        continentStats = new HashMap<String, ContinentStats>();

        // manifest attributes

        nodeList = manifestDocument.getElementsByTagName("manifest");

        if(nodeList != null)
        {
            namedNodeMap = nodeList.item(0).getAttributes();

            if(namedNodeMap == null)
            {
                EddieLogger.error("AirVPNManifest.processXmlManifest(): \"manifest\" node has no attributes");

                initializeManifestData();

                return;
            }

            try
            {
                value = supportTools.getXmlItemNodeValue(namedNodeMap, "time");

                manifestTimeTS = Long.parseLong(value);
            }
            catch(NumberFormatException e)
            {
                EddieLogger.warning("AirVPNManifest.processXmlManifest(): manifest.time not found");

                manifestTimeTS = 0;
            }

            try
            {
                value = supportTools.getXmlItemNodeValue(namedNodeMap, "next");

                manifestNextUpdateTS = Long.parseLong(value);
            }
            catch(NumberFormatException e)
            {
                EddieLogger.warning("AirVPNManifest.processXmlManifest(): manifest.next not found");

                manifestNextUpdateTS = 0;
            }

            try
            {
                value = supportTools.getXmlItemNodeValue(namedNodeMap, "next_update");

                nextUpdateIntervalMinutes = Integer.parseInt(value);
            }
            catch(NumberFormatException e)
            {
                EddieLogger.warning("AirVPNManifest.processXmlManifest(): manifest.next_update not found");

                nextUpdateIntervalMinutes = AIRVPN_MANIFEST_DEFAULT_UPDATE_INTERVAL_MINUTES;
            }

            dnsCheckHost = supportTools.getXmlItemNodeValue(namedNodeMap, "dnscheck_host");
            dnsCheckRes1 = supportTools.getXmlItemNodeValue(namedNodeMap, "dnscheck_res1");
            dnsCheckRes2 = supportTools.getXmlItemNodeValue(namedNodeMap, "dnscheck_res2");

            try
            {
                value = supportTools.getXmlItemNodeValue(namedNodeMap, "speed_factor");

                speedFactor = Double.parseDouble(value);
            }
            catch(NumberFormatException e)
            {
                speedFactor = 1.0;
            }

            try
            {
                value = supportTools.getXmlItemNodeValue(namedNodeMap, "load_factor");

                loadFactor = Double.parseDouble(value);
            }
            catch(NumberFormatException e)
            {
                loadFactor = 1.0;
            }

            try
            {
                value = supportTools.getXmlItemNodeValue(namedNodeMap, "users_factor");

                userFactor = Double.parseDouble(value);
            }
            catch(NumberFormatException e)
            {
                userFactor = 1.0;
            }

            try
            {
                value = supportTools.getXmlItemNodeValue(namedNodeMap, "latency_factor");

                latencyFactor = Integer.parseInt(value);
            }
            catch(NumberFormatException e)
            {
                latencyFactor = 0;
            }

            try
            {
                value = supportTools.getXmlItemNodeValue(namedNodeMap, "penality_factor");

                penalityFactor = Integer.parseInt(value);
            }
            catch(NumberFormatException e)
            {
                penalityFactor = 0;
            }

            try
            {
                value = supportTools.getXmlItemNodeValue(namedNodeMap, "pinger_delay");

                pingerDelay = Integer.parseInt(value);
            }
            catch(NumberFormatException e)
            {
                pingerDelay = 0;
            }

            try
            {
                value = supportTools.getXmlItemNodeValue(namedNodeMap, "pinger_retry");

                pingerRetry = Integer.parseInt(value);
            }
            catch(NumberFormatException e)
            {
                pingerRetry = 0;
            }

            checkDomain = supportTools.getXmlItemNodeValue(namedNodeMap, "check_domain");
            checkDnsQuery = supportTools.getXmlItemNodeValue(namedNodeMap, "check_dns_query");
            openVpnDirectives = supportTools.convertXmlEntities(supportTools.getXmlItemNodeValue(namedNodeMap, "openvpn_directives"));
            modeProtocol = supportTools.getXmlItemNodeValue(namedNodeMap, "mode_protocol");

            try
            {
                value = supportTools.getXmlItemNodeValue(namedNodeMap, "mode_port");

                modePort = Integer.parseInt(value);
            }
            catch(NumberFormatException e)
            {
                modePort = 0;
            }

            try
            {
                value = supportTools.getXmlItemNodeValue(namedNodeMap, "mode_alt");

                modeAlt = Integer.parseInt(value);
            }
            catch(NumberFormatException e)
            {
                modeAlt = 0;
            }

            // RSA public key

            rsaPublicKeyExponent = supportTools.getXmlItemNodeValue(namedNodeMap, "auth_rsa_exponent");

            if(rsaPublicKeyExponent.isEmpty())
            {
                nodeList = manifestDocument.getElementsByTagName("Exponent");

                if(nodeList != null)
                    rsaPublicKeyExponent = nodeList.item(0).getTextContent();
                else
                    rsaPublicKeyExponent = "";
            }

            rsaPublicKeyExponentBackup = rsaPublicKeyExponent;

            rsaPublicKeyModulus = supportTools.getXmlItemNodeValue(namedNodeMap, "auth_rsa_modulus");

            if(rsaPublicKeyModulus.isEmpty())
            {
                nodeList = manifestDocument.getElementsByTagName("Modulus");

                if(nodeList != null)
                    rsaPublicKeyModulus = nodeList.item(0).getTextContent();
                else
                    rsaPublicKeyModulus = "";
            }

            rsaPublicKeyModulusBackup = rsaPublicKeyModulus;

            if(rsaPublicKeyExponent.isEmpty() || rsaPublicKeyModulus.isEmpty())
                EddieLogger.error("AirVPNManifest.processXmlManifest(): RSA public key is incomplete or empty in manifest");

            // Manifest messages

            nodeList = manifestDocument.getElementsByTagName("message");

            if(nodeList != null && nodeList.getLength() > 0)
            {
                if(manifestMessage == null)
                    manifestMessage = new ArrayList<Message>();

                manifestMessage.clear();

                for(i = 0; i < nodeList.getLength(); i++)
                {
                    Message message = new Message();

                    namedNodeMap = nodeList.item(i).getAttributes();

                    if(namedNodeMap != null)
                    {
                        value = supportTools.getXmlItemNodeValue(namedNodeMap, "text");

                        if(!value.isEmpty())
                            message.setText(value);

                        value = supportTools.getXmlItemNodeValue(namedNodeMap, "url");

                        if(!value.isEmpty())
                            message.setUrl(value);

                        value = supportTools.getXmlItemNodeValue(namedNodeMap, "link");

                        if(!value.isEmpty())
                            message.setLink(value);

                        value = supportTools.getXmlItemNodeValue(namedNodeMap, "html");

                        if(!value.isEmpty())
                            message.setHtml(value);

                        try
                        {
                            longVal = Long.parseLong(supportTools.getXmlItemNodeValue(namedNodeMap, "from_time"));
                        }
                        catch(NumberFormatException e)
                        {
                            longVal = 0;
                        }

                        message.setFromUTCTimeTS(longVal);

                        try
                        {
                            longVal = Long.parseLong(supportTools.getXmlItemNodeValue(namedNodeMap, "to_time"));
                        }
                        catch(NumberFormatException e)
                        {
                            longVal = 0;
                        }

                        message.setToUTCTimeTS(longVal);

                        message.setShown(false);

                        found = false;

                        for(Message m : manifestMessage)
                        {
                            if(m.getText().equals(message.getText()))
                                found = true;
                        }

                        if(found == false && !message.getText().isEmpty())
                            manifestMessage.add(message);
                    }
                }
            }

            // Manifest messages

            nodeList = manifestDocument.getElementsByTagName("mode");

            if(nodeList != null && nodeList.getLength() > 0)
            {
                if(manifestMode == null)
                    manifestMode = new ModeList();

                manifestMode.clear();

                for(i = 0; i < nodeList.getLength(); i++)
                {
                    Mode mode = new Mode();

                    namedNodeMap = nodeList.item(i).getAttributes();

                    if(namedNodeMap != null)
                    {
                        if(supportTools.getXmlItemNodeValue(namedNodeMap, "protocol").equalsIgnoreCase("udp") || supportTools.getXmlItemNodeValue(namedNodeMap, "protocol").equalsIgnoreCase("tcp"))
                        {
                            value = supportTools.getXmlItemNodeValue(namedNodeMap, "title");

                            if(!value.isEmpty())
                                mode.setTitle(value);

                            value = supportTools.getXmlItemNodeValue(namedNodeMap, "protocol");

                            if(value.equalsIgnoreCase("udp") == true)
                                mode.setProtocol(SettingsManager.AIRVPN_PROTOCOL_UDP);
                            else if(value.equalsIgnoreCase("tcp") == true)
                                mode.setProtocol(SettingsManager.AIRVPN_PROTOCOL_TCP);
                            else
                                mode.setProtocol(SettingsManager.AIRVPN_PROTOCOL_UDP);

                            try
                            {
                                intVal = Integer.parseInt(supportTools.getXmlItemNodeValue(namedNodeMap, "port"));
                            }
                            catch(NumberFormatException e)
                            {
                                intVal = -1;
                            }

                            mode.setPort(intVal);

                            try
                            {
                                intVal = Integer.parseInt(supportTools.getXmlItemNodeValue(namedNodeMap, "entry_index"));
                            }
                            catch(NumberFormatException e)
                            {
                                intVal = -1;
                            }

                            mode.setEntryIndex(intVal);

                            value = supportTools.getXmlItemNodeValue(namedNodeMap, "specs");

                            row = value.split(",");

                            for(String s : row)
                            {
                                s = s.trim();

                                found = false;

                                if(s.equalsIgnoreCase("tls1.0") == true)
                                {
                                    mode.setTlsVersion(SettingsManager.OPENVPN_TLS_1_0);

                                    found = true;
                                }
                                else if(s.equalsIgnoreCase("tls1.1") == true)
                                {
                                    mode.setTlsVersion(SettingsManager.OPENVPN_TLS_1_1);

                                    found = true;
                                }
                                else if(s.equalsIgnoreCase("tls1.2") == true)
                                {
                                    mode.setTlsVersion(SettingsManager.OPENVPN_TLS_1_2);

                                    found = true;
                                }
                                else if(s.equalsIgnoreCase("tls1.3") == true)
                                {
                                    mode.setTlsVersion(SettingsManager.OPENVPN_TLS_1_3);

                                    found = true;
                                }
                                else if(s.equalsIgnoreCase("tls-auth") == true)
                                {
                                    mode.setTlsMode(SettingsManager.OPENVPN_TLS_MODE_AUTH);
                                    mode.setOpenVpnKeyDirection("1");

                                    found = true;
                                }
                                else if(s.equalsIgnoreCase("tls-crypt") == true)
                                {
                                    mode.setTlsMode(SettingsManager.OPENVPN_TLS_MODE_CRYPT);
                                    mode.setOpenVpnAuth("SHA512");

                                    found = true;
                                }

                                if(found == true)
                                {
                                    value = value.replace(s, "");

                                    if(value.charAt(0) == ',')
                                        value = value.replaceFirst(",", "");
                                }
                            }

                            mode.setSpecifications(value.trim());

                            value = supportTools.getXmlItemNodeValue(namedNodeMap, "type");

                            if(value.equalsIgnoreCase("wireguard") == true)
                                mode.setVpnType(VPN.Type.WIREGUARD);
                            else if(value.equalsIgnoreCase("openvpn") == true)
                                mode.setVpnType(VPN.Type.OPENVPN);
                            else
                                mode.setVpnType(VPN.Type.UNKNOWN);

                            value = supportTools.getXmlItemNodeValue(namedNodeMap, "openvpn_minversion");

                            mode.setOpenVpnMinVersion(value);

                            value = supportTools.getXmlItemNodeValue(namedNodeMap, "openvpn_directives");

                            row = value.split("\r\n");

                            for(String s : row)
                            {
                                s = s.trim();

                                found = false;

                                if(s.equalsIgnoreCase("proto udp") == true)
                                {
                                    mode.setProtocol(SettingsManager.AIRVPN_PROTOCOL_UDP);

                                    found = true;
                                }
                                else if(s.equalsIgnoreCase("proto tcp") == true)
                                {
                                    mode.setProtocol(SettingsManager.AIRVPN_PROTOCOL_TCP);

                                    found = true;
                                }
                                else if(s.toLowerCase().startsWith("auth") == true)
                                {
                                    row1 = value.split(" ");

                                    if(row1.length == 2)
                                    {
                                        mode.setOpenVpnAuth(row1[1]);

                                        found = true;
                                    }
                                }
                                else if(s.toLowerCase().startsWith("key-direction") == true)
                                {
                                    row1 = value.split(" ");

                                    if(row1.length == 2)
                                    {
                                        mode.setOpenVpnKeyDirection(row1[1]);

                                        found = true;
                                    }
                                }
                                else if(s.toLowerCase().startsWith("<tls-auth>") == true)
                                {
                                    mode.setTlsMode(SettingsManager.OPENVPN_TLS_MODE_AUTH);
                                    mode.setOpenVpnKeyDirection("1");

                                    found = true;
                                }
                                else if(s.toLowerCase().startsWith("<tls-crypt>") == true)
                                {
                                    mode.setTlsMode(SettingsManager.OPENVPN_TLS_MODE_CRYPT);
                                    mode.setOpenVpnAuth("SHA512");

                                    found = true;
                                }

                                if(found == true)
                                {
                                    value = value.replace(s, "");

                                    value = value.trim();
                                }
                            }

                            mode.setOpenVpnDirectives(value.trim());

                            value = supportTools.getXmlItemNodeValue(namedNodeMap, "ssh_destination");

                            mode.setSshDestination(value);

                            // mode.setTlsVersion(SettingsManager.OPENVPN_TLS_1_3);  // TLS needs to be overridden in order to comply to OpenVPN3

                            found = false;

                            for(Mode m : manifestMode)
                            {
                                if(m.getTitle().equalsIgnoreCase(mode.getTitle()))
                                    found = true;
                            }

                            if(found == false && !mode.getTitle().isEmpty())
                                manifestMode.add(mode);
                        }
                    }
                }
            }

            // bootstrap urls

            nodeList = manifestDocument.getElementsByTagName("url");

            if(nodeList != null)
            {
                resetBootServers();

                for(i = 0; i < nodeList.getLength(); i++)
                {
                    namedNodeMap = nodeList.item(i).getAttributes();

                    if(namedNodeMap != null)
                    {
                        value = supportTools.getXmlItemNodeValue(namedNodeMap, "address");

                        if(!value.isEmpty())
                            addBootStrapServer(value);
                        else
                            EddieLogger.warning("AirVPNManifest.processXmlManifest(): found empty \"url\" node in manifest");
                    }
                    else
                        EddieLogger.warning("AirVPNManifest.processXmlManifest(): found \"url\" node in manifest with no attributes");
                }

                bootStrapServerBackupList = bootStrapServerList;
            }
            else
                EddieLogger.error("AirVPNManifest.processXmlManifest(): \"urls\" node not found in manifest");

            // Connection modes

            if(openVPNPortList == null)
                openVPNPortList = new ArrayList<String>();
            else
                openVPNPortList.clear();

            if(wireGuardPortList == null)
                wireGuardPortList = new ArrayList<String>();
            else
                wireGuardPortList.clear();

            nodeList = manifestDocument.getElementsByTagName("mode");

            if(nodeList != null)
            {
                for(i = 0; i < nodeList.getLength(); i++)
                {
                    namedNodeMap = nodeList.item(i).getAttributes();

                    if(namedNodeMap != null)
                    {
                        vpnModeType = supportTools.getXmlItemNodeValue(namedNodeMap, "type").toLowerCase(Locale.US);
                        vpnModeProtocol = supportTools.getXmlItemNodeValue(namedNodeMap, "protocol").toLowerCase(Locale.US);
                        vpnModePort = supportTools.getXmlItemNodeValue(namedNodeMap, "port");

                        if(vpnModeProtocol.equals("udp") || vpnModeProtocol.equals("tcp"))
                        {
                            if(vpnModeType.equals("openvpn"))
                            {
                                if(openVPNPortList.contains(vpnModePort) == false)
                                    openVPNPortList.add(vpnModePort);
                            }
                            else if(vpnModeType.equals("wireguard"))
                            {
                                if(wireGuardPortList.contains(vpnModePort) == false)
                                    wireGuardPortList.add(vpnModePort);
                            }
                        }
                    }
                }
            }

            if(openVPNPortList.size() == 0)
                Collections.addAll(openVPNPortList, EddieApplication.context().getResources().getStringArray(R.array.airvpn_openvpn_port_values));

            Collections.sort(openVPNPortList, new CompareStringAsInteger());

            if(wireGuardPortList.size() == 0)
                Collections.addAll(wireGuardPortList, EddieApplication.context().getResources().getStringArray(R.array.airvpn_wireguard_port_values));

            Collections.sort(wireGuardPortList, new CompareStringAsInteger());

            // AirVPN Server Groups (V >= 256)

            localVpnServerGroup = new AirVPNServerGroup();

            cipherDatabase.reset();

            nodeList = manifestDocument.getElementsByTagName("servers_group");

            if(nodeList != null)
            {
                for(i = 0; i < nodeList.getLength(); i++)
                {
                    namedNodeMap = nodeList.item(i).getAttributes();

                    if(namedNodeMap != null)
                    {
                        serverGroup = new AirVPNServerGroup().new ServerGroup();
                        value = "";
                        intVal = -1;

                        if(serverGroup != null)
                        {
                            try
                            {
                                value = supportTools.getXmlItemNodeValue(namedNodeMap, "group");

                                intVal = Integer.parseInt(value);
                            }
                            catch (NumberFormatException e)
                            {
                                intVal = -1;
                            }

                            if(intVal != -1)
                                serverGroup.setGroup(intVal);
                            else
                                EddieLogger.error("AirVPNManifest.processXmlManifest(): server_group.group \"%s\" is invalid", value);

                            value = supportTools.getXmlItemNodeValue(namedNodeMap, "support_ipv4");

                            if(value.equals("true"))
                                serverGroup.setIPv4Support(true);
                            else
                                serverGroup.setIPv4Support(false);

                            value = supportTools.getXmlItemNodeValue(namedNodeMap, "support_ipv6");

                            if(value.equals("true"))
                                serverGroup.setIPv6Support(true);
                            else
                                serverGroup.setIPv6Support(false);

                            value = supportTools.getXmlItemNodeValue(namedNodeMap, "support_check");

                            if(value.equals("true"))
                                serverGroup.setCheckSupport(true);
                            else
                                serverGroup.setCheckSupport(false);

                            value = supportTools.getXmlItemNodeValue(namedNodeMap, "ciphers_tls");

                            serverGroup.setTlsCiphers(cipherDatabase.getCodeArrayList(value, CipherDatabase.CipherType.TLS));

                            value = supportTools.getXmlItemNodeValue(namedNodeMap, "ciphers_tlssuites");

                            serverGroup.setTlsSuiteCiphers(cipherDatabase.getCodeArrayList(value, CipherDatabase.CipherType.TLS_SUITE));

                            value = supportTools.getXmlItemNodeValue(namedNodeMap, "ciphers_data");

                            serverGroup.setDataCiphers(cipherDatabase.getCodeArrayList(value, CipherDatabase.CipherType.DATA));

                            localVpnServerGroup.add(serverGroup);
                        }
                        else
                            EddieLogger.error("AirVPNManifest.processXmlManifest(): Failed to instantiate a new ServerGroup");

                    }
                }

                airVPNServerGroup = localVpnServerGroup;
            }
            else
                EddieLogger.error("AirVPNManifest.processXmlManifest(): \"server_groups\" node not found in manifest");

            // AirVPN servers

            nodeList = manifestDocument.getElementsByTagName("server");

            if(nodeList != null)
            {
                localVpnServerList.clear();

                for(int position = 0; position < nodeList.getLength(); position++)
                {
                    localAirVPNServer = new AirVPNServer();

                    namedNodeMap = nodeList.item(position).getAttributes();

                    if(namedNodeMap != null)
                    {
                        value = supportTools.getXmlItemNodeValue(namedNodeMap, "name");

                        if(!value.isEmpty())
                        {
                            localAirVPNServer.setName(value);
                            localAirVPNServer.setCountryCode(supportTools.getXmlItemNodeValue(namedNodeMap, "country_code").toUpperCase(Locale.US));
                            localAirVPNServer.setLocation(supportTools.getXmlItemNodeValue(namedNodeMap, "location"));

                            try
                            {
                                value = supportTools.getXmlItemNodeValue(namedNodeMap, "bw");

                                intVal = Integer.parseInt(value);
                            }
                            catch(NumberFormatException e)
                            {
                                intVal = 0;
                            }

                            localAirVPNServer.setBandWidth(intVal);

                            try
                            {
                                value = supportTools.getXmlItemNodeValue(namedNodeMap, "bw_max");

                                intVal = Integer.parseInt(value);
                            }
                            catch(NumberFormatException e)
                            {
                                intVal = 0;
                            }

                            localAirVPNServer.setMaxBandWidth(intVal);

                            try
                            {
                                value = supportTools.getXmlItemNodeValue(namedNodeMap, "users");

                                intVal = Integer.parseInt(value);
                            }
                            catch(NumberFormatException e)
                            {
                                intVal = 0;
                            }

                            localAirVPNServer.setUsers(intVal);

                            try
                            {
                                value = supportTools.getXmlItemNodeValue(namedNodeMap, "users_max");

                                intVal = Integer.parseInt(value);
                            }
                            catch(NumberFormatException e)
                            {
                                intVal = 0;
                            }

                            localAirVPNServer.setMaxUsers(intVal);

                            try
                            {
                                value = supportTools.getXmlItemNodeValue(namedNodeMap, "group");

                                if(!value.isEmpty())
                                    group = Integer.parseInt(value);
                                else
                                    group = -1;
                            }
                            catch(NumberFormatException e)
                            {
                                group = -1;
                            }

                            if(group != -1)
                            {
                                localAirVPNServer.setSupportIPv4(airVPNServerGroup.getGroupIPv4Support(group));
                                localAirVPNServer.setSupportIPv6(airVPNServerGroup.getGroupIPv6Support(group));
                                localAirVPNServer.setSupportCheck(airVPNServerGroup.getGroupSupportCheck(group));
                                localAirVPNServer.setTlsCiphers(airVPNServerGroup.getGroupTlsCiphers(group));
                                localAirVPNServer.setTlsSuiteCiphers(airVPNServerGroup.getGroupTlsSuiteCiphers(group));
                                localAirVPNServer.setDataCiphers(airVPNServerGroup.getGroupDataCiphers(group));
                            }
                            else
                                EddieLogger.error("AirVPNManifest.processXmlManifest(): Server \"%s\" has no group", localAirVPNServer.getName());

                            localAirVPNServer.setWarningOpen(supportTools.getXmlItemNodeValue(namedNodeMap, "warning_open"));
                            localAirVPNServer.setWarningClosed(supportTools.getXmlItemNodeValue(namedNodeMap, "warning_closed"));

                            try
                            {
                                value = supportTools.getXmlItemNodeValue(namedNodeMap, "scorebase");

                                intVal = Integer.parseInt(value);
                            }
                            catch(NumberFormatException e)
                            {
                                intVal = 0;
                            }

                            localAirVPNServer.setScoreBase(intVal);

                            value = supportTools.getXmlItemNodeValue(namedNodeMap, "ips_entry");

                            if(!value.isEmpty())
                            {
                                row = value.split(",");

                                if(localAirVPNServer.getSupportIPv6())
                                {
                                    j = row.length / 2;

                                    for(i = 0; i < j; i++)
                                    {
                                        localAirVPNServer.setEntryIPv4(i, row[i].trim());
                                        localAirVPNServer.setEntryIPv6(i, row[i + j].trim());
                                    }
                                }
                                else
                                {
                                    for(i = 0; i < row.length; i++)
                                        localAirVPNServer.setEntryIPv4(i, row[i]);
                                }
                            }

                            value = supportTools.getXmlItemNodeValue(namedNodeMap, "ips_exit");

                            if(!value.isEmpty())
                            {
                                if(localAirVPNServer.getSupportIPv6())
                                {
                                    row = value.split(",");

                                    localAirVPNServer.setExitIPv4(row[0].trim());
                                    localAirVPNServer.setExitIPv6(row[1].trim());
                                }
                                else
                                    localAirVPNServer.setExitIPv4(value.trim());
                            }

                            localVpnServerList.add(localAirVPNServer);

                            if(localAirVPNServer.isAvailable())
                            {
                                if(continentStats.containsKey(localAirVPNServer.getContinentCode()))
                                {
                                    conStats = continentStats.get(localAirVPNServer.getContinentCode());

                                    conStats.setServers(conStats.getServers() + 1);
                                    conStats.setUsers(conStats.getUsers() + localAirVPNServer.getUsers());
                                    conStats.setBandWidth(conStats.getBandWidth() + localAirVPNServer.getBandWidth());
                                    conStats.setMaxBandWidth(conStats.getMaxBandWidth() + localAirVPNServer.getMaxBandWidth());

                                    continentStats.put(localAirVPNServer.getContinentCode(), conStats);
                                }
                                else
                                {
                                    conStats = new ContinentStats();

                                    conStats.setContinentCode(localAirVPNServer.getContinentCode());
                                    conStats.setServers(1);
                                    conStats.setUsers(localAirVPNServer.getUsers());
                                    conStats.setBandWidth(localAirVPNServer.getBandWidth());
                                    conStats.setMaxBandWidth(localAirVPNServer.getMaxBandWidth());

                                    continentStats.put(localAirVPNServer.getContinentCode(), conStats);
                                }

                                if(countryStats.containsKey(localAirVPNServer.getCountryCode()))
                                {
                                    cStats = countryStats.get(localAirVPNServer.getCountryCode());

                                    cStats.setServers(cStats.getServers() + 1);
                                    cStats.setUsers(cStats.getUsers() + localAirVPNServer.getUsers());
                                    cStats.setBandWidth(cStats.getBandWidth() + localAirVPNServer.getBandWidth());
                                    cStats.setMaxBandWidth(cStats.getMaxBandWidth() + localAirVPNServer.getMaxBandWidth());

                                    countryStats.put(localAirVPNServer.getCountryCode(), cStats);
                                }
                                else
                                {
                                    cStats = new CountryStats();

                                    cStats.setCountryISOCode(localAirVPNServer.getCountryCode());
                                    cStats.setServers(1);
                                    cStats.setUsers(localAirVPNServer.getUsers());
                                    cStats.setBandWidth(localAirVPNServer.getBandWidth());
                                    cStats.setMaxBandWidth(localAirVPNServer.getMaxBandWidth());

                                    countryStats.put(localAirVPNServer.getCountryCode(), cStats);
                                }
                            }
                        }
                        else
                            EddieLogger.warning("AirVPNManifest.processXmlManifest(): found unnamed \"server\" in manifest");
                    }
                    else
                        EddieLogger.warning("AirVPNManifest.processXmlManifest(): found \"server\" node in manifest with no attributes");
                }

                airVpnServerList = localVpnServerList;
            }
            else
                EddieLogger.error("AirVPNManifest.processXmlManifest(): \"urls\" node not found in manifest");
        }
        else
        {
            EddieLogger.error("AirVPNManifest.processXmlManifest(): \"manifest\" node not found in manifest");

            initializeManifestData();

            return;
        }

        manifestType = newManifestType;

        airVPNManifestDocument = manifestDocument;

        if(eddieEvent != null)
            eddieEvent.onAirVPNManifestChanged();

        if(manifestType == ManifestType.FROM_SERVER)
        {
            if(!AirVPNUser.masterPassword().isEmpty() && settingsManager.isMasterPasswordEnabled() == true)
            {
                if(supportTools.encryptXmlDocumentToFile(airVPNManifestDocument, AirVPNUser.masterPassword(), AIRVPN_MANIFEST_FILE_NAME) == true)
                    EddieLogger.info("Successfully encrypted and saved updated AirVPN manifest to local storage");
                else
                    EddieLogger.error("Error while saving AirVPN manifest to local storage");
            }
            else if(settingsManager.isMasterPasswordEnabled() == false)
            {
                if(supportTools.saveXmlDocumentToFile(airVPNManifestDocument, AIRVPN_MANIFEST_FILE_NAME) == true)
                    EddieLogger.info("Successfully saved updated AirVPN manifest to local storage");
                else
                    EddieLogger.error("Error while saving AirVPN manifest to local storage");
            }
        }
    }

    // Eddie events

    public void onVpnConnectionStatsChanged(final VPNConnectionStats connectionData)
    {
    }

    public void onVpnStatusChanged(VPN.Status status, String message)
    {
        if(refreshPending == true && status == VPN.Status.CONNECTED)
            refreshManifestFromAirVPN();
    }

    public void onVpnAuthFailed(final VPNEvent oe)
    {
    }

    public void onVpnError(final VPNEvent oe)
    {
    }

    public void onVpnReconnect(final VPNEvent oe)
    {
    }

    public void onMasterPasswordChanged()
    {
        if(manifestType == ManifestType.NOT_SET || manifestType == ManifestType.DEFAULT)
            loadManifest();
        else
        {
            if(!AirVPNUser.masterPassword().isEmpty() && airVPNManifestDocument != null)
            {
                if(!supportTools.encryptXmlDocumentToFile(airVPNManifestDocument, AirVPNUser.masterPassword(), AIRVPN_MANIFEST_FILE_NAME))
                    supportTools.infoDialog(R.string.manifest_save_error, true);
            }
        }
    }

    public void onAirVPNLogin()
    {
        loadManifest();
    }

    public void onAirVPNLoginFailed(String message)
    {
    }

    public void onAirVPNLogout()
    {
    }

    public void onAirVPNUserDataChanged()
    {
    }

    public void onAirVPNUserProfileReceived(Document document)
    {
    }

    public void onAirVPNUserProfileDownloadError()
    {
    }

    public void onAirVPNUserProfileChanged()
    {
    }

    public void onAirVPNManifestReceived(final Document document)
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                manifestType = ManifestType.FROM_SERVER;

                processXmlManifest(document);
            }
        };

        supportTools.startThread(runnable);
    }

    public void onAirVPNManifestDownloadError()
    {
        if(supportTools.isNetworkConnectionActive() == true)
        {
            supportTools.infoDialog(String.format("%s%s", EddieApplication.context().getResources().getString(R.string.manifest_download_error), EddieApplication.context().getResources().getString(R.string.bootstrap_server_error)), true);

            EddieLogger.error("Error while retrieving AirVPN manifest from server");
        }
        else
            refreshPending = true;
    }

    public void onAirVPNManifestChanged()
    {
    }

    public void onAirVPNIgnoredManifestDocumentRequest()
    {
    }

    public void onAirVPNIgnoredUserDocumentRequest()
    {
    }

    public void onAirVPNRequestError(String message)
    {
        if(supportTools.isNetworkConnectionActive() == false)
            refreshPending = true;
    }

    public void onCancelConnection()
    {
    }

    public void onSaveState()
    {
    }

    public void onRestoreState(Bundle bundle)
    {
    }

    // NetworkStatusReceiver

    public void onNetworkStatusNotAvailable()
    {
    }

    public void onNetworkStatusConnected()
    {
        if(refreshPending == true)
            refreshManifestFromAirVPN();
    }

    public void onNetworkStatusIsConnecting()
    {
    }

    public void onNetworkStatusIsDisconnecting()
    {
    }

    public void onNetworkStatusSuspended()
    {
    }

    public void onNetworkStatusNotConnected()
    {
    }

    public void onNetworkTypeChanged()
    {
        if(refreshPending == true)
            refreshManifestFromAirVPN();
    }
}
