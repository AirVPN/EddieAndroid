// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 15 October 2018 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.res.ResourcesCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

public class AirVPNServerSettingsActivity extends AppCompatActivity
{
    private SupportTools supportTools = null;
    private SettingsManager settingsManager = null;
    private Activity thisActivity = this;
    private Typeface typeface = null;

    private TextView txtTitle = null;

    private LinearLayout llAirVPNConfirmServerConnection = null;
    private LinearLayout llAirVPNDefaultVPNType = null;
    private LinearLayout llAirVPNOpenVPNMode = null;
    private LinearLayout llAirVPNOpenVPNProtocol = null;
    private LinearLayout llAirVPNOpenVPNPort = null;
    private LinearLayout llAirVPNWireGuardMode = null;
    private LinearLayout llAirVPNWireGuardPort = null;
    private LinearLayout llAirVPNIPVersion = null;
    private LinearLayout llAirVPNOpenVPNTLSMode = null;
    private LinearLayout llAirVPNOpenVPNEncryptionAlgorithm = null;
    private LinearLayout llUserProfile = null;
    private Spinner spnAirVPNKey = null;

    private Switch swAirVPNConfirmServerConnection = null;

    private RadioGroup rgSortBy = null;
    private RadioGroup rgSortMode = null;

    private AirVPNUser airVPNUser = null;

    private static String selectedUserProfile = "";

    private int settingsResult = RESULT_CANCELED;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        int radioButtonId = 0;

        super.onCreate(savedInstanceState);

        supportTools = EddieApplication.supportTools();

        supportTools.setLocale(getBaseContext());

        setContentView(R.layout.airvpn_server_settings_layout);

        settingsManager = EddieApplication.settingsManager();

        airVPNUser = EddieApplication.airVPNUser();

        selectedUserProfile = airVPNUser.getCurrentKey();

        typeface = ResourcesCompat.getFont(this, R.font.default_font);

        txtTitle = (TextView)findViewById(R.id.title);

        txtTitle.setTypeface(typeface);

        llAirVPNConfirmServerConnection = (LinearLayout)findViewById(R.id.settings_airvpn_confirm_server_connection);
        llAirVPNDefaultVPNType = (LinearLayout)findViewById(R.id.settings_airvpn_default_vpn_type);
        llAirVPNOpenVPNMode = (LinearLayout)findViewById(R.id.settings_airvpn_openvpn_mode);
        llAirVPNOpenVPNProtocol = (LinearLayout)findViewById(R.id.settings_airvpn_default_openvpn_protocol);
        llAirVPNOpenVPNPort = (LinearLayout)findViewById(R.id.settings_airvpn_default_openvpn_port);
        llAirVPNWireGuardMode = (LinearLayout)findViewById(R.id.settings_airvpn_wireguard_mode);
        llAirVPNWireGuardPort = (LinearLayout)findViewById(R.id.settings_airvpn_default_wireguard_port);
        llAirVPNIPVersion = (LinearLayout)findViewById(R.id.settings_airvpn_default_ip_version);
        llAirVPNOpenVPNTLSMode = (LinearLayout)findViewById(R.id.settings_airvpn_default_openvpn_tls_mode);
        llAirVPNOpenVPNEncryptionAlgorithm = (LinearLayout)findViewById(R.id.settings_airvpn_default_openvpn_encryption_algorithm);
        llUserProfile = (LinearLayout)findViewById(R.id.user_key);
        spnAirVPNKey = (Spinner)findViewById(R.id.spn_user_key);

        swAirVPNConfirmServerConnection = (Switch)findViewById(R.id.switch_airvpn_confirm_server_connection);

        setupUserProfileSpinner(selectedUserProfile);

        rgSortBy = (RadioGroup)findViewById(R.id.settings_airvpn_sort_by_group);
        rgSortMode = (RadioGroup)findViewById(R.id.settings_airvpn_sort_mode_group);

        if(settingsManager.getAirVPNOpenVPNMode().equals(SettingsManager.AIRVPN_OPENVPN_MODE_DEFAULT))
        {
            llAirVPNOpenVPNProtocol.setVisibility(View.VISIBLE);
            llAirVPNOpenVPNPort.setVisibility(View.VISIBLE);
            llAirVPNOpenVPNTLSMode.setVisibility(View.VISIBLE);
        }
        else
        {
            llAirVPNOpenVPNProtocol.setVisibility(View.GONE);
            llAirVPNOpenVPNPort.setVisibility(View.GONE);
            llAirVPNOpenVPNTLSMode.setVisibility(View.GONE);
        }

        if(settingsManager.getAirVPNWireGuardMode().equals(SettingsManager.AIRVPN_WIREGUARD_MODE_DEFAULT))
            llAirVPNWireGuardPort.setVisibility(View.VISIBLE);
        else
            llAirVPNWireGuardPort.setVisibility(View.GONE);

        llAirVPNConfirmServerConnection.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectAirVPNConfirmServerConnection();
            }
        });

        llAirVPNDefaultVPNType.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectAirVPNDefaultVPNType();
            }
        });

        llAirVPNOpenVPNMode.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectAirVPNOpenVPNMode();
            }
        });

        llAirVPNOpenVPNProtocol.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectAirVPNOpenVPNProtocol();
            }
        });

        llAirVPNOpenVPNPort.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectAirVPNOpenVPNPort();
            }
        });

        llAirVPNWireGuardMode.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectAirVPNWireGuardMode();
            }
        });

        llAirVPNWireGuardPort.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectAirVPNWireGuardPort();
            }
        });

        llAirVPNIPVersion.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectAirVPNIPVersion();
            }
        });

        llAirVPNOpenVPNTLSMode.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectAirVPNTLSMode();
            }
        });

        llAirVPNOpenVPNEncryptionAlgorithm.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectAirVPNEncryptionAlgorithm();
            }
        });

        spnAirVPNKey.setAccessibilityDelegate(new View.AccessibilityDelegate()
        {
            @Override
            public void sendAccessibilityEvent(View host, int eventType)
            {
                super.sendAccessibilityEvent(host, eventType);

                spnAirVPNKey.setContentDescription(String.format("%s %s", getString(R.string.accessibility_selected_airvpn_key), spnAirVPNKey.getSelectedItem().toString()));
            }
        });

        swAirVPNConfirmServerConnection.setChecked(settingsManager.isAirVPNConfirmServerConnectionEnabled());

        switch(settingsManager.getAirVPNSortBy())
        {
            case SettingsManager.AIRVPN_SERVER_SORT_BY_NAME:
            {
                radioButtonId = R.id.settings_airvpn_sort_by_name;
            }
            break;

            case SettingsManager.AIRVPN_SERVER_SORT_BY_LOCATION:
            {
                radioButtonId = R.id.settings_airvpn_sort_by_location;
            }
            break;

            case SettingsManager.AIRVPN_SERVER_SORT_BY_SCORE:
            {
                radioButtonId = R.id.settings_airvpn_sort_by_score;
            }
            break;

            case SettingsManager.AIRVPN_SERVER_SORT_BY_LOAD:
            {
                radioButtonId = R.id.settings_airvpn_sort_by_load;
            }
            break;

            case SettingsManager.AIRVPN_SERVER_SORT_BY_USERS:
            {
                radioButtonId = R.id.settings_airvpn_sort_by_users;
            }
            break;

            case SettingsManager.AIRVPN_SERVER_SORT_BY_BANDWIDTH:
            {
                radioButtonId = R.id.settings_airvpn_sort_by_bandwidth;
            }
            break;

            case SettingsManager.AIRVPN_SERVER_SORT_BY_MAX_BANDWIDTH:
            {
                radioButtonId = R.id.settings_airvpn_sort_by_max_bandwidth;
            }
            break;

            default:
            {
                radioButtonId = R.id.settings_airvpn_sort_by_name;
            }
            break;
        }

        rgSortBy.check(radioButtonId);

        rgSortBy.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                String sortBy = "";

                switch(checkedId)
                {
                    case R.id.settings_airvpn_sort_by_name:
                    {
                        sortBy = SettingsManager.AIRVPN_SERVER_SORT_BY_NAME;
                    }
                    break;

                    case R.id.settings_airvpn_sort_by_location:
                    {
                        sortBy = SettingsManager.AIRVPN_SERVER_SORT_BY_LOCATION;
                    }
                    break;

                    case R.id.settings_airvpn_sort_by_score:
                    {
                        sortBy = SettingsManager.AIRVPN_SERVER_SORT_BY_SCORE;
                    }
                    break;

                    case R.id.settings_airvpn_sort_by_load:
                    {
                        sortBy = SettingsManager.AIRVPN_SERVER_SORT_BY_LOAD;
                    }
                    break;

                    case R.id.settings_airvpn_sort_by_users:
                    {
                        sortBy = SettingsManager.AIRVPN_SERVER_SORT_BY_USERS;
                    }
                    break;

                    case R.id.settings_airvpn_sort_by_bandwidth:
                    {
                        sortBy = SettingsManager.AIRVPN_SERVER_SORT_BY_BANDWIDTH;
                    }
                    break;

                    case R.id.settings_airvpn_sort_by_max_bandwidth:
                    {
                        sortBy = SettingsManager.AIRVPN_SERVER_SORT_BY_MAX_BANDWIDTH;
                    }
                    break;

                    default:
                    {
                        sortBy = SettingsManager.AIRVPN_SERVER_SORT_BY_NAME;
                    }
                    break;
                }

                settingsManager.setAirVPNSortBy(sortBy);

                settingsResult = RESULT_OK;
            }
        });

        switch(settingsManager.getAirVPNSortMode())
        {
            case SettingsManager.AIRVPN_SERVER_SORT_MODE_ASCENDING:
            {
                radioButtonId = R.id.settings_airvpn_sort_servers_ascending;
            }
            break;

            case SettingsManager.AIRVPN_SERVER_SORT_MODE_DESCENDING:
            {
                radioButtonId = R.id.settings_airvpn_sort_servers_descending;
            }
            break;

            default:
            {
                radioButtonId = R.id.settings_airvpn_sort_servers_ascending;
            }
            break;
        }

        rgSortMode.check(radioButtonId);

        rgSortMode.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                String sortMode = "";

                switch(checkedId)
                {
                    case R.id.settings_airvpn_sort_servers_ascending:
                    {
                        sortMode = SettingsManager.AIRVPN_SERVER_SORT_MODE_ASCENDING;
                    }
                    break;

                    case R.id.settings_airvpn_sort_servers_descending:
                    {
                        sortMode = SettingsManager.AIRVPN_SERVER_SORT_MODE_DESCENDING;
                    }
                    break;

                    default:
                    {
                        sortMode = SettingsManager.AIRVPN_SERVER_SORT_MODE_ASCENDING;
                    }
                    break;
                }

                settingsManager.setAirVPNSortMode(sortMode);

                settingsResult = RESULT_OK;
            }
        });

        OnBackPressedCallback onBackPressedCallback = new OnBackPressedCallback(true)
        {
            @Override
            public void handleOnBackPressed()
            {
                setResult(settingsResult, null);

                supportTools.setLocale(getBaseContext());

                thisActivity.finish();
            }
        };

        getOnBackPressedDispatcher().addCallback(this, onBackPressedCallback);
    }

    @Override
    public void onResume()
    {
        super.onResume();

        selectedUserProfile = airVPNUser.getCurrentKey();

        if(airVPNUser.isUserValid())
            setupUserProfileSpinner(selectedUserProfile);
        else
            llUserProfile.setVisibility(View.GONE);
    }

    @Override
    public void onStart()
    {
        super.onStart();

        if(settingsManager.getSystemApplicationTheme().equals(SettingsManager.SYSTEM_OPTION_APPLICATION_THEME_SYSTEM) == false)
        {
            switch(settingsManager.getSystemApplicationTheme())
            {
                case "Dark":
                {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                }
                break;

                case "Light":
                {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                }
                break;

                default:
                {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
                }
                break;
            }
        }
    }

    @Override
    public void onStop()
    {
        super.onStop();

        supportTools.setLocale(getBaseContext());
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        supportTools.setLocale(getBaseContext());
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        switch(newConfig.uiMode & Configuration.UI_MODE_NIGHT_MASK)
        {
            case Configuration.UI_MODE_NIGHT_YES:
            {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            }
            break;

            case Configuration.UI_MODE_NIGHT_NO:
            {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            }
            break;

            default:
            {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
            }
            break;
        }

        supportTools.setLocale(getBaseContext());

        recreate();

        super.onConfigurationChanged(newConfig);
    }

    private void selectAirVPNConfirmServerConnection()
    {
        settingsManager.setAirVPNConfirmServerConnection(!settingsManager.isAirVPNConfirmServerConnectionEnabled());

        swAirVPNConfirmServerConnection.setChecked(settingsManager.isAirVPNConfirmServerConnectionEnabled());

        settingsResult = RESULT_OK;
    }

    private void selectAirVPNDefaultVPNType()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getOptionFromListDialog(thisActivity,
                        R.string.airvpn_server_settings_vpn_type_title,
                        R.array.airvpn_vpn_type_labels,
                        R.array.airvpn_vpn_type_values,
                        settingsManager.getAirVPNDefaultVPNType());

                if(!value.equals(settingsManager.getAirVPNDefaultVPNType()))
                {
                    settingsManager.setAirVPNDefaultVPNType(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        SupportTools.runOnUiActivity(this, uiRunnable);
    }

    private void selectAirVPNOpenVPNMode()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";
                String l[], v[];
                int i = 1;
                AirVPNManifest.ModeList mode = EddieApplication.airVPNManifest().getModes(VPN.Type.OPENVPN);

                l = new String[mode.size() + 1];
                v = new String[mode.size() + 1];

                l[0] = getResources().getString(R.string.none);
                v[0] = SettingsManager.AIRVPN_OPENVPN_MODE_DEFAULT;

                for(AirVPNManifest.Mode m : mode)
                {
                    l[i] = m.getTitle();
                    v[i] = m.getTitle();

                    i++;
                }

                value = supportTools.getOptionFromListDialog(thisActivity,
                        R.string.settings_airvpn_openvpn_mode_title,
                        l,
                        v,
                        settingsManager.getAirVPNOpenVPNMode());

                if(!value.equals(settingsManager.getAirVPNOpenVPNMode()))
                {
                    settingsManager.setAirVPNOpenVPNMode(value);

                    if(value.equals(SettingsManager.AIRVPN_OPENVPN_MODE_DEFAULT) == true)
                    {
                        llAirVPNOpenVPNProtocol.setVisibility(View.VISIBLE);
                        llAirVPNOpenVPNPort.setVisibility(View.VISIBLE);
                        llAirVPNOpenVPNTLSMode.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        llAirVPNOpenVPNProtocol.setVisibility(View.GONE);
                        llAirVPNOpenVPNPort.setVisibility(View.GONE);
                        llAirVPNOpenVPNTLSMode.setVisibility(View.GONE);
                    }

                    settingsResult = RESULT_OK;
                }
            }
        };

        SupportTools.runOnUiActivity(this, uiRunnable);
    }

    private void selectAirVPNOpenVPNProtocol()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getOptionFromListDialog(thisActivity,
                                                             R.string.airvpn_server_settings_openvpn_protocol_title,
                                                             R.array.airvpn_protocol_labels,
                                                             R.array.airvpn_protocol_values,
                                                             settingsManager.getAirVPNDefaultOpenVPNProtocol());

                if(!value.equals(settingsManager.getAirVPNDefaultOpenVPNProtocol()))
                {
                    settingsManager.setAirVPNDefaultOpenVPNProtocol(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        SupportTools.runOnUiActivity(this, uiRunnable);
    }

    private void setupUserProfileSpinner(String selectedItem)
    {
        if(llUserProfile == null || spnAirVPNKey == null)
            return;

        ArrayList<String> keyNames = airVPNUser.getUserKeyNames();
        int selectedPosition = 0;

        Collections.sort(keyNames);

        if(keyNames != null && keyNames.size() > 0 && llUserProfile != null && spnAirVPNKey != null)
        {
            ArrayList<String> items = new ArrayList<String>();

            for(String profile : keyNames)
                items.add(profile);

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items);

            if(spnAirVPNKey != null)
            {
                spnAirVPNKey.setAdapter(adapter);

                for(int i = 0; i < items.size(); i++)
                {
                    if(spnAirVPNKey.getItemAtPosition(i).toString().equals(selectedItem))
                        selectedPosition = i;
                }

                spnAirVPNKey.setSelection(selectedPosition);

                spnAirVPNKey.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
                {
                    @Override
                    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id)
                    {
                        selectedUserProfile = spnAirVPNKey.getItemAtPosition(position).toString();

                        airVPNUser.setCurrentKey(thisActivity, selectedUserProfile);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parentView)
                    {
                    }
                });
            }

            if(llUserProfile != null)
                llUserProfile.setVisibility(View.VISIBLE);
        }
        else
        {
            if(llUserProfile != null)
                llUserProfile.setVisibility(View.GONE);
        }
    }

    private void selectAirVPNOpenVPNPort()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";
                String ports[] = AirVPNManifest.getOpenVPNPortList().toArray(new String[0]);
                int intVal = 0;

                value = supportTools.getOptionFromListDialog(thisActivity,
                                                             R.string.airvpn_server_settings_openvpn_port_title,
                                                             ports,
                                                             ports,
                                                             String.format("%d", settingsManager.getAirVPNDefaultOpenVPNPort()));

                try
                {
                    intVal = Integer.parseInt(value);
                }
                catch(NumberFormatException e)
                {
                    intVal = settingsManager.AIRVPN_OPENVPN_PORT_DEFAULT;
                }

                if(intVal != settingsManager.getAirVPNDefaultOpenVPNPort())
                {
                    settingsManager.setAirVPNDefaultOpenVPNPort(intVal);

                    settingsResult = RESULT_OK;
                }
            }
        };

        SupportTools.runOnUiActivity(this, uiRunnable);
    }

    private void selectAirVPNWireGuardMode()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";
                String l[], v[];
                int i = 1;
                AirVPNManifest.ModeList mode = EddieApplication.airVPNManifest().getModes(VPN.Type.WIREGUARD);

                l = new String[mode.size() + 1];
                v = new String[mode.size() + 1];

                l[0] = getResources().getString(R.string.none);
                v[0] = SettingsManager.AIRVPN_WIREGUARD_MODE_DEFAULT;

                for(AirVPNManifest.Mode m : mode)
                {
                    l[i] = m.getTitle();
                    v[i] = m.getTitle();

                    i++;
                }

                value = supportTools.getOptionFromListDialog(thisActivity,
                        R.string.settings_airvpn_wireguard_mode_title,
                        l,
                        v,
                        settingsManager.getAirVPNWireGuardMode());

                if(!value.equals(settingsManager.getAirVPNWireGuardMode()))
                {
                    settingsManager.setAirVPNWireGuardMode(value);

                    if(value.equals(SettingsManager.AIRVPN_WIREGUARD_MODE_DEFAULT) == true)
                        llAirVPNWireGuardPort.setVisibility(View.VISIBLE);
                    else
                        llAirVPNWireGuardPort.setVisibility(View.GONE);

                    settingsResult = RESULT_OK;
                }
            }
        };

        SupportTools.runOnUiActivity(this, uiRunnable);
    }

    private void selectAirVPNWireGuardPort()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";
                String ports[] = AirVPNManifest.getWireGuardPortList().toArray(new String[0]);
                int intVal = 0;

                value = supportTools.getOptionFromListDialog(thisActivity,
                        R.string.airvpn_server_settings_wireguard_port_title,
                        ports,
                        ports,
                        String.format("%d", settingsManager.getAirVPNDefaultWireGuardPort()));

                try
                {
                    intVal = Integer.parseInt(value);
                }
                catch(NumberFormatException e)
                {
                    intVal = settingsManager.AIRVPN_WIREGUARD_PORT_DEFAULT;
                }

                if(intVal != settingsManager.getAirVPNDefaultWireGuardPort())
                {
                    settingsManager.setAirvpnDefaultWireguardPort(intVal);

                    settingsResult = RESULT_OK;
                }
            }
        };

        SupportTools.runOnUiActivity(this, uiRunnable);
    }

    private void selectAirVPNIPVersion()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getOptionFromListDialog(thisActivity,
                                                             R.string.airvpn_server_settings_ip_version_title,
                                                             R.array.airvpn_ip_version_labels,
                                                             R.array.airvpn_ip_version_values,
                                                             settingsManager.getAirVPNDefaultIPVersion());

                if(!value.equals(settingsManager.getAirVPNDefaultIPVersion()))
                {
                    settingsManager.setAirVPNDefaultIPVersion(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        SupportTools.runOnUiActivity(this, uiRunnable);
    }

    private void selectAirVPNTLSMode()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getOptionFromListDialog(thisActivity,
                                                             R.string.airvpn_server_settings_openvpn_tls_mode_title,
                                                             R.array.airvpn_tls_mode_labels,
                                                             R.array.airvpn_tls_mode_values,
                                                             settingsManager.getAirVPNDefaultTLSMode());

                if(!value.equals(settingsManager.getAirVPNDefaultTLSMode()))
                {
                    settingsManager.setAirVPNDefaultTLSMode(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        SupportTools.runOnUiActivity(this, uiRunnable);
    }

    private void selectAirVPNEncryptionAlgorithm()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getOptionFromListDialog(thisActivity,
                                                             R.string.settings_airvpn_openvpn_encryption_algorithm_title,
                                                             R.array.airvpn_encryption_algorithm_labels,
                                                             R.array.airvpn_encryption_algorithm_values,
                                                             settingsManager.getAirVPNOpenVPNCipher());

                if(!value.equals(settingsManager.getAirVPNOpenVPNCipher()))
                {
                    settingsManager.setAirVPNOpenVPNCipher(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        SupportTools.runOnUiActivity(this, uiRunnable);
    }
}
