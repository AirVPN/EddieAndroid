// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 20 June 2018 - author: ProMIND - initial release. Based on revised code from com.eddie.android. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Base64;

import org.json.JSONArray;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class SettingsManager
{
    public static class VPNBootOption
    {
        String code;
        String description;
        ArrayList<String> condition;
    }

    public static final String VPN_BOOT_OPTION_AIRVPN_BEST_SERVER = "AirVPN-Best-Server";
    public static final String VPN_BOOT_OPTION_AIRVPN_STARTUP_SERVER = "AirVPN-Startup-Server";
    public static final String VPN_BOOT_OPTION_AIRVPN_STARTUP_COUNTRY = "AirVPN-Startup-Country";
    public static final String VPN_BOOT_OPTION_DEFAULT_VPN_PROFILE = "Default-VPN-Profile";
    public static final String VPN_BOOT_OPTION_LAST_CONNECTED_PROFILE = "Last-Connected-Profile";

    public static final String VPN_BOOT_CONDITION_MP_DISABLED = "MP-Disabled";
    public static final String VPN_BOOT_CONDITION_AIRVPN_CREDENTIALS = "AirVPN-Credentials";
    public static final String VPN_BOOT_CONDITION_STARTUP_AIRVPN_SERVER = "Default-AirVPN-Server";
    public static final String VPN_BOOT_CONDITION_STARTUP_AIRVPN_COUNTRY = "Default-AirVPN-Country";
    public static final String VPN_BOOT_CONDITION_STARTUP_VPN_PROFILE = "Default-VPN-Profile";
    public static final String VPN_BOOT_CONDITION_LAST_CONNECTED_PROFILE = "Last-Connected-Profile";

    public static final String OPENVPN_TLS_1_0 = "tls_1_0";
    public static final String OPENVPN_TLS_1_1 = "tls_1_1";
    public static final String OPENVPN_TLS_1_2 = "tls_1_2";
    public static final String OPENVPN_TLS_1_3 = "tls_1_3";

    public static final String OPENVPN_TLS_MODE_AUTH = "tls-auth";
    public static final String OPENVPN_TLS_MODE_CRYPT = "tls-crypt";

    public static final String ON = "on";
    public static final String OFF = "off";
    public static final String UNKNOWN = "unknown";
    public static final String YES = "yes";
    public static final String NO = "no";

    public static final String OVPN3_OPTION_TLS_MIN_VERSION = "ovpn3_tls_version_min";
    public static final String OVPN3_OPTION_TLS_MIN_VERSION_NATIVE = "tls_version_min";
    public static final String OVPN3_OPTION_TLS_MIN_VERSION_DEFAULT = OPENVPN_TLS_1_3;

    public static final String OVPN3_OPTION_PROTOCOL = "ovpn3_protocol";
    public static final String OVPN3_OPTION_PROTOCOL_NATIVE = "protocol";
    public static final String OVPN3_OPTION_PROTOCOL_DEFAULT = "";

    public static final String OVPN3_OPTION_ALLOWUAF = "ovpn3_ipv6";
    public static final String OVPN3_OPTION_ALLOWUAF_NATIVE = "allowuaf";
    public static final String OVPN3_OPTION_ALLOWUAF_DEFAULT = "";

    public static final String OVPN3_OPTION_TIMEOUT = "ovpn3_timeout";
    public static final String OVPN3_OPTION_TIMEOUT_NATIVE = "timeout";
    public static final String OVPN3_OPTION_TIMEOUT_DEFAULT = "60";

    public static final String OVPN3_OPTION_TUN_PERSIST = "ovpn3_tun_persist";
    public static final String OVPN3_OPTION_TUN_PERSIST_NATIVE = "tun_persist";
    public static final boolean OVPN3_OPTION_TUN_PERSIST_DEFAULT = true;

    public static final String OVPN3_OPTION_COMPRESSION_MODE = "ovpn3_compression_mode";
    public static final String OVPN3_OPTION_COMPRESSION_MODE_NATIVE = "compression_mode";
    public static final String OVPN3_OPTION_COMPRESSION_MODE_DEFAULT = NO;

    public static final String SYSTEM_OPTION_VPN_LOCK = "system_vpn_lock";
    public static final boolean SYSTEM_OPTION_VPN_LOCK_DEFAULT = false;

    public static final String SYSTEM_OPTION_VPN_RECONNECT = "system_vpn_reconnect";
    public static final boolean SYSTEM_OPTION_VPN_RECONNECT_DEFAULT = true;

    public static final String SYSTEM_OPTION_METERED_VPN = "system_metered_vpn";
    public static final boolean SYSTEM_OPTION_METERED_VPN_DEFAULT = false;

    public static final String SYSTEM_OPTION_VPN_RECONNECTION_RETRIES = "system_vpn_reconnect_retries";
    public static final int SYSTEM_OPTION_VPN_RECONNECTION_RETRIES_INFINITE = -1;
    public static final int SYSTEM_OPTION_VPN_RECONNECTION_RETRIES_DEFAULT = SYSTEM_OPTION_VPN_RECONNECTION_RETRIES_INFINITE;

    public static final String OVPN3_OPTION_USERNAME = "ovpn3_username";
    public static final String OVPN3_OPTION_USERNAME_NATIVE = "username";
    public static final String OVPN3_OPTION_USERNAME_DEFAULT = "";

    public static final String OVPN3_OPTION_PASSWORD = "ovpn3_password";
    public static final String OVPN3_OPTION_PASSWORD_NATIVE = "password";
    public static final String OVPN3_OPTION_PASSWORD_DEFAULT = "";

    public static final String OVPN3_OPTION_SYNCHRONOUS_DNS_LOOKUP = "ovpn3_synchronous_dns_lookup";
    public static final String OVPN3_OPTION_SYNCHRONOUS_DNS_LOOKUP_NATIVE = "synchronous_dns_lookup";
    public static final boolean OVPN3_OPTION_SYNCHRONOUS_DNS_LOOKUP_DEFAULT = false;

    public static final String OVPN3_OPTION_AUTOLOGIN_SESSIONS = "ovpn3_autologin_sessions";
    public static final String OVPN3_OPTION_AUTOLOGIN_SESSIONS_NATIVE = "autologin_sessions";
    public static final boolean OVPN3_OPTION_AUTOLOGIN_SESSIONS_DEFAULT = true;

    public static final String OVPN3_OPTION_DISABLE_CLIENT_CERT = "ovpn3_disable_client_cert";
    public static final String OVPN3_OPTION_DISABLE_CLIENT_CERT_NATIVE = "disable_client_cert";
    public static final boolean OVPN3_OPTION_DISABLE_CLIENT_CERT_DEFAULT = false;

    public static final String OVPN3_OPTION_SSL_DEBUG_LEVEL = "ovpn3_ssl_debug_level";
    public static final String OVPN3_OPTION_SSL_DEBUG_LEVEL_NATIVE = "ssl_debug_level";
    public static final String OVPN3_OPTION_SSL_DEBUG_LEVEL_DEFAULT = "0";

    public static final String OVPN3_OPTION_PRIVATE_KEY_PASSWORD = "ovpn3_private_key_password";
    public static final String OVPN3_OPTION_PRIVATE_KEY_PASSWORD_NATIVE = "private_key_password";
    public static final String OVPN3_OPTION_PRIVATE_KEY_PASSWORD_DEFAULT = "";

    public static final String OVPN3_OPTION_DEFAULT_KEY_DIRECTION = "ovpn3_default_key_direction";
    public static final String OVPN3_OPTION_DEFAULT_KEY_DIRECTION_NATIVE = "default_key_direction";
    public static final String OVPN3_OPTION_DEFAULT_KEY_DIRECTION_DEFAULT = "-1";

    public static final String OVPN3_OPTION_TLS_CERT_PROFILE = "ovpn3_tls_cert_profile";
    public static final String OVPN3_OPTION_TLS_CERT_PROFILE_NATIVE = "tls_cert_profile";
    public static final String OVPN3_OPTION_TLS_CERT_PROFILE_DEFAULT = "";

    public static final String OVPN3_OPTION_PROXY_HOST = "ovpn3_proxy_host";
    public static final String OVPN3_OPTION_PROXY_HOST_NATIVE = "proxy_host";
    public static final String OVPN3_OPTION_PROXY_HOST_DEFAULT = "";

    public static final String OVPN3_OPTION_PROXY_PORT = "ovpn3_proxy_port";
    public static final String OVPN3_OPTION_PROXY_PORT_NATIVE = "proxy_port";
    public static final String OVPN3_OPTION_PROXY_PORT_DEFAULT = "";

    public static final String OVPN3_OPTION_PROXY_USERNAME = "ovpn3_proxy_username";
    public static final String OVPN3_OPTION_PROXY_USERNAME_NATIVE = "proxy_username";
    public static final String OVPN3_OPTION_PROXY_USERNAME_DEFAULT = "";

    public static final String OVPN3_OPTION_PROXY_PASSWORD = "ovpn3_proxy_password";
    public static final String OVPN3_OPTION_PROXY_PASSWORD_NATIVE = "proxy_password";
    public static final String OVPN3_OPTION_PROXY_PASSWORD_DEFAULT = "";

    public static final String OVPN3_OPTION_PROXY_ALLOW_CLEARTEXT_AUTH = "ovpn3_proxy_allow_cleartext_auth";
    public static final String OVPN3_OPTION_PROXY_ALLOW_CLEARTEXT_AUTH_NATIVE = "proxy_allow_cleartext_auth";
    public static final boolean OVPN3_OPTION_PROXY_ALLOW_CLEARTEXT_AUTH_DEFAULT = false;

    public static final String OVPN3_OPTION_CUSTOM_DIRECTIVES = "ovpn3_custom_directives";
    public static final String OVPN3_OPTION_CUSTOM_DIRECTIVES_DEFAULT = "";

    public static final int WIREGUARD_DEFAULT_MTU = 1320;
    public static final int WIREGUARD_HANDSHAKING_UPDATE_WAIT_SECONDS = 60;
    public static final int WIREGUARD_HANDSHAKING_TIMEOUT_SECONDS = 150;
    public static final String WIREGUARD_IGNORE_HANDSHAKING_TIMEOUT = "wireguard_ignore_handshaking_timeout";
    public static final boolean WIREGUARD_IGNORE_HANDSHAKING_TIMEOUT_DEFAULT = true;

    public static final String SYSTEM_OPTION_ENABLE_MASTER_PASSWORD = "enable_master_password";
    public static final boolean SYSTEM_OPTION_ENABLE_MASTER_PASSWORD_DEFAULT = false;

    public static final String SYSTEM_OPTION_DNS_OVERRIDE_ENABLE = "system_dns_override_enable";
    public static final boolean SYSTEM_OPTION_DNS_OVERRIDE_ENABLE_DEFAULT = false;

    public static final String SYSTEM_OPTION_DNS_CUSTOM = "system_dns_custom";
    public static final String SYSTEM_OPTION_DNS_CUSTOM_DEFAULT = "";

    public static final String SYSTEM_OPTION_PROXY_ENABLE = "system_proxy_enable";
    public static final boolean SYSTEM_OPTION_PROXY_ENABLE_DEFAULT = false;

    public static final String SYSTEM_OPTION_NOTIFICATION_SOUND = "system_notification_sound";
    public static final boolean SYSTEM_OPTION_NOTIFICATION_SOUND_DEFAULT = true;

    public static final String SYSTEM_OPTION_SHOW_MESSAGE_DIALOGS = "system_show_message_dialogs";
    public static final boolean SYSTEM_OPTION_SHOW_MESSAGE_DIALOGS_DEFAULT = true;

    public static final String SYSTEM_CUSTOM_MTU = "system_forced_mtu";
    public static final String SYSTEM_CUSTOM_MTU_DEFAULT = "";

    public static final String SYSTEM_OPTION_APPLICATION_FILTER_TYPE = "system_application_filter_type";
    public static final String SYSTEM_OPTION_APPLICATION_FILTER_TYPE_NONE = "0";
    public static final String SYSTEM_OPTION_APPLICATION_FILTER_TYPE_WHITELIST = "1";
    public static final String SYSTEM_OPTION_APPLICATION_FILTER_TYPE_BLACKLIST = "2";
    public static final String SYSTEM_OPTION_APPLICATION_FILTER_TYPE_DEFAULT = SYSTEM_OPTION_APPLICATION_FILTER_TYPE_NONE;

    public static final String SYSTEM_OPTION_APPLICATION_FILTER = "system_application_filter";
    public static final String SYSTEM_OPTION_APPLICATION_FILTER_DEFAULT = "";

    public static final String SYSTEM_OPTION_APPLICATION_LANGUAGE = "system_application_language";
    public static final String SYSTEM_OPTION_APPLICATION_LANGUAGE_DEFAULT = "";

    public static final String SYSTEM_OPTION_APPLICATION_THEME = "system_application_theme";
    public static final String SYSTEM_OPTION_APPLICATION_THEME_SYSTEM = "System";
    public static final String SYSTEM_OPTION_APPLICATION_THEME_LIGHT = "Light";
    public static final String SYSTEM_OPTION_APPLICATION_THEME_DARK = "Dark";
    public static final String SYSTEM_OPTION_APPLICATION_THEME_DEFAULT = SYSTEM_OPTION_APPLICATION_THEME_SYSTEM;

    public static final String SYSTEM_OPTION_GPS_SPOOFING = "system_gps_spoofing";
    public static final boolean SYSTEM_OPTION_GPS_SPOOFING_DEFAULT = false;

    public static final String SYSTEM_OPTION_GPS_SPOOFING_INTERVAL = "system_gps_spoofing_interval";
    public static final int SYSTEM_OPTION_GPS_SPOOFING_INTERVAL_DEFAULT = 1000;

    public static final String SYSTEM_OPTION_FIRST_RUN = "system_first_run";
    public static final boolean SYSTEM_OPTION_FIRST_RUN_DEFAULT = true;

    public static final String SYSTEM_OPTION_VPN_BOOT_PRIORITY = "system_vpn_boot_priority";
    public static final String SYSTEM_OPTION_VPN_BOOT_PRIORITY_DEFAULT = VPN_BOOT_OPTION_AIRVPN_BEST_SERVER + "," + VPN_BOOT_OPTION_AIRVPN_STARTUP_SERVER + "," + VPN_BOOT_OPTION_AIRVPN_STARTUP_COUNTRY + "," + VPN_BOOT_OPTION_DEFAULT_VPN_PROFILE + "," + VPN_BOOT_OPTION_LAST_CONNECTED_PROFILE;

    public static final String SYSTEM_OPTION_START_VPN_AT_STARTUP = "system_start_vpn_at_startup";
    public static final boolean SYSTEM_OPTION_START_VPN_AT_STARTUP_DEFAULT = false;

    public static final String SYSTEM_OPTION_LAST_PROFILE_IS_CONNECTED = "system_last_profile_is_connected";
    public static final boolean SYSTEM_OPTION_LAST_PROFILE_IS_CONNECTED_DEFAULT = false;

    public static final String SYSTEM_OPTION_LAST_PROFILE = "system_last_profile";

    public static final String SYSTEM_OPTION_LAST_PROFILE_INFO = "system_last_profile_info";

    public static final String SYSTEM_IS_ALWAYS_ON_VPN = "is_always_on_vpn";
    public static final String SYSTEM_IS_ALWAYS_ON_VPN_DEFAULT = UNKNOWN;

    public static final String SYSTEM_VPN_LOCKOWN = "vpn_lockdown";
    public static final String SYSTEM_VPN_LOCKOWN_DEFAULT = UNKNOWN;

    public static final String SYSTEM_OPTION_EXCLUDE_LOCAL_NETWORKS = "system_exclude_local_networks";
    public static final boolean SYSTEM_OPTION_EXCLUDE_LOCAL_NETWORKS_DEFAULT = false;

    public static final String SYSTEM_OPTION_PAUSE_VPN_WHEN_SCREEN_IS_OFF = "system_pause_vpn_when_screen_is_off";
    public static final boolean SYSTEM_OPTION_PAUSE_VPN_WHEN_SCREEN_IS_OFF_DEFAULT = true;

    public static final String SYSTEM_AIRVPN_REMEMBER_ME = "system_airvpn_remember_me";
    public static final boolean SYSTEM_AIRVPN_REMEMBER_ME_DEFAULT = false;

    public static final String SYSTEM_AIRVPN_AUTOLOGIN = "system_airvpn_autologin";
    public static final boolean SYSTEM_AIRVPN_AUTOLOGIN_DEFAULT = false;

    public static final String AIRVPN_CONFIRM_SERVER_CONNECTION = "airvpn_confirm_server_connection";
    public static final boolean AIRVPN_CONFIRM_SERVER_CONNECTION_DEFAULT = false;

    public static final String AIRVPN_MASTER_PASSWORD_HASHCODE = "system_mp";
    public static final int AIRVPN_MASTER_PASSWORD_HASHCODE_DEFAULT = -1;

    public static final String AIRVPN_CURRENT_LOCAL_COUNTRY = "airvpn_current_local_country";
    public static final String AIRVPN_CURRENT_LOCAL_COUNTRY_DEFAULT = "Auto";

    public static final String VPN_TYPE_OPENVPN = "OpenVPN";
    public static final String VPN_TYPE_WIREGUARD = "WireGuard";

    public static final String AIRVPN_DEFAULT_VPN_TYPE = "airvpn_default_vpn_type";
    public static final String AIRVPN_VPN_TYPE_DEFAULT = VPN_TYPE_WIREGUARD;

    public static final String AIRVPN_PROTOCOL_UDP = "UDP";
    public static final String AIRVPN_PROTOCOL_TCP = "TCP";

    public static final String AIRVPN_OPENVPN_MODE = "airvpn_openvpn_mode";
    public static final String AIRVPN_OPENVPN_MODE_DEFAULT = "";

    public static final String AIRVPN_DEFAULT_OPENVPN_PROTOCOL = "airvpn_default_openvpn_protocol";
    public static final String AIRVPN_OPENVPN_PROTOCOL_DEFAULT = AIRVPN_PROTOCOL_UDP;

    public static final String AIRVPN_DEFAULT_OPENVPN_PORT = "airvpn_default_openvpn_port";
    public static final int AIRVPN_OPENVPN_PORT_DEFAULT = 443;

    public static final String AIRVPN_WIREGUARD_MODE = "airvpn_wireguard_mode";
    public static final String AIRVPN_WIREGUARD_MODE_DEFAULT = "";

    public static final String AIRVPN_DEFAULT_WIREGUARD_PORT = "airvpn_default_wireguard_port";
    public static final int AIRVPN_WIREGUARD_PORT_DEFAULT = 1637;

    public static final String AIRVPN_IP_VERSION_4 = "IPv4";
    public static final String AIRVPN_IP_VERSION_6 = "IPv6";
    public static final String AIRVPN_IP_VERSION_4_OVER_6 = "IPv6overIPv4";

    public static final String AIRVPN_DEFAULT_IP_VERSION = "airvpn_default_ip_version";
    public static final String AIRVPN_IP_VERSION_DEFAULT = AIRVPN_IP_VERSION_4_OVER_6;

    public static final String AIRVPN_DEFAULT_TLS_MODE = "airvpn_default_tls_mode";
    public static final String AIRVPN_TLS_MODE_DEFAULT = OPENVPN_TLS_MODE_CRYPT;

    public static final String AIRVPN_OPENVPN_CIPHER = "airvpn_openvpn_cipher";
    public static final String AIRVPN_CIPHER_SERVER = "SERVER";
    public static final String AIRVPN_CIPHER_CHACHA20_POLY1305 = "CHACHA20-POLY1305";
    public static final String AIRVPN_CIPHER_AES_256_GCM = "AES-256-GCM";
    public static final String AIRVPN_CIPHER_AES_128_GCM = "AES-128-GCM";
    public static final String AIRVPN_OPENVPN_CIPHER_DEFAULT = AIRVPN_CIPHER_SERVER;

    public static final String AIRVPN_QUICK_CONNECT_MODE = "airvpn_quick_connect_mode";
    public static final String AIRVPN_QUICK_CONNECT_MODE_DEFAULT_OPTIONS = "default";
    public static final String AIRVPN_QUICK_CONNECT_MODE_AUTO = "auto";
    public static final String AIRVPN_QUICK_CONNECT_MODE_DEFAULT = AIRVPN_QUICK_CONNECT_MODE_AUTO;

    public static final String AIRVPN_FORBID_QUICK_CONNECTION_TO_USER_COUNTRY = "airvpn_forbid_quick_connection_to_user_country";
    public static final boolean AIRVPN_FORBID_QUICK_CONNECTION_TO_USER_COUNTRY_DEFAULT = true;

    public static final String AIRVPN_CUSTOM_BOOTSTRAP_SERVERS = "airvpn_custom_bootstrap_servers";
    public static final String AIRVPN_CUSTOM_BOOTSTRAP_SERVERS_DEFAULT = "";

    public static final String AIRVPN_SERVER_WHITELIST = "airvpn_server_whitelist";
    public static final String AIRVPN_SERVER_WHITELIST_DEFAULT = "";
    public static final String AIRVPN_SERVER_BLACKLIST = "airvpn_server_blacklist";
    public static final String AIRVPN_SERVER_BLACKLIST_DEFAULT = "";

    public static final String STARTUP_AIRVPN_SERVER = "startup_airvpn_server";
    public static final String STARTUP_AIRVPN_SERVER_DEFAULT = "";

    public static final String AIRVPN_COUNTRY_WHITELIST = "airvpn_country_whitelist";
    public static final String AIRVPN_COUNTRY_WHITELIST_DEFAULT = "";
    public static final String AIRVPN_COUNTRY_BLACKLIST = "airvpn_country_blacklist";
    public static final String AIRVPN_COUNTRY_BLACKLIST_DEFAULT = "";

    public static final String STARTUP_AIRVPN_COUNTRY = "startup_airvpn_country";
    public static final String STARTUP_AIRVPN_COUNTRY_DEFAULT = "";

    public static final String AIRVPN_SERVER_SORT_BY_NAME = "sort_name";
    public static final String AIRVPN_SERVER_SORT_BY_LOCATION = "sort_location";
    public static final String AIRVPN_SERVER_SORT_BY_SCORE = "sort_score";
    public static final String AIRVPN_SERVER_SORT_BY_LOAD = "sort_load";
    public static final String AIRVPN_SERVER_SORT_BY_USERS = "sort_users";
    public static final String AIRVPN_SERVER_SORT_BY_BANDWIDTH = "sort_bandwidth";
    public static final String AIRVPN_SERVER_SORT_BY_MAX_BANDWIDTH = "sort_max_bandwidth";

    public static final String AIRVPN_SERVER_SORT_BY = "airvpn_server_sort_by";
    public static final String AIRVPN_SERVER_SORT_BY_DEFAULT = AIRVPN_SERVER_SORT_BY_NAME;

    public static final String AIRVPN_SERVER_SORT_MODE_ASCENDING = "sort_ascending";
    public static final String AIRVPN_SERVER_SORT_MODE_DESCENDING = "sort_descending";

    public static final String AIRVPN_SERVER_SORT_MODE = "airvpn_server_sort_mode";
    public static final String AIRVPN_SERVER_SORT_MODE_DEFAULT = AIRVPN_SERVER_SORT_MODE_ASCENDING;

    public static final String AIRVPN_DO_NOT_SHOW_AGAIN_MESSAGES = "airvpn_do_not_show_again_messages";
    public static final String AIRVPN_DO_NOT_SHOW_AGAIN_MESSAGES_DEFAULT = "";

    public static final String DEFAULT_SPLIT_SEPARATOR = ",";

    private SharedPreferences appPrefs = null;
    private SharedPreferences.Editor prefsEditor = null;

    public SettingsManager()
    {
        appPrefs = PreferenceManager.getDefaultSharedPreferences(EddieApplication.context());
        
        prefsEditor = appPrefs.edit();
    }

    public String getOvpn3TLSMinVersion()
    {
        return getString(OVPN3_OPTION_TLS_MIN_VERSION, OVPN3_OPTION_TLS_MIN_VERSION_DEFAULT);
    }

    public void setOvpn3TLSMinVersion(String value)
    {
        saveString(OVPN3_OPTION_TLS_MIN_VERSION, value);
    }

    public String getOvpn3Protocol()
    {
        return getString(OVPN3_OPTION_PROTOCOL, OVPN3_OPTION_PROTOCOL_DEFAULT);
    }

    public void setOvpn3Protocol(String value)
    {
        saveString(OVPN3_OPTION_PROTOCOL, value);
    }

    public String getOvpn3AllowUnusedAddressFamilies()
    {
        return getString(OVPN3_OPTION_ALLOWUAF, OVPN3_OPTION_ALLOWUAF_DEFAULT);
    }

    public void setOvpn3AllowUnusedAddressFamilies(String value)
    {
        saveString(OVPN3_OPTION_ALLOWUAF, value);
    }

    public String getOvpn3Timeout()
    {
        return getString(OVPN3_OPTION_TIMEOUT, OVPN3_OPTION_TIMEOUT_DEFAULT);
    }

    public void setOvpn3Timeout(String value)
    {
        saveString(OVPN3_OPTION_TIMEOUT, value);
    }

    public boolean isOvpn3TunPersist()
    {
        return getBoolean(OVPN3_OPTION_TUN_PERSIST, OVPN3_OPTION_TUN_PERSIST_DEFAULT);
    }

    public void setOvpn3TunPersist(boolean value)
    {
        saveBoolean(OVPN3_OPTION_TUN_PERSIST, value);
    }

    public String getOvpn3CompressionMode()
    {
        return getString(OVPN3_OPTION_COMPRESSION_MODE, OVPN3_OPTION_COMPRESSION_MODE_DEFAULT);
    }

    public void setOvpn3CompressionMode(String value)
    {
        saveString(OVPN3_OPTION_COMPRESSION_MODE, value);
    }

    public boolean isWireGuardHandshakingTimeoutIgnored()
    {
        return getBoolean(WIREGUARD_IGNORE_HANDSHAKING_TIMEOUT, WIREGUARD_IGNORE_HANDSHAKING_TIMEOUT_DEFAULT);
    }

    public void setWireGuardHandshakingTimeoutIgnored(boolean value)
    {
        saveBoolean(WIREGUARD_IGNORE_HANDSHAKING_TIMEOUT, value);
    }

    public boolean isMasterPasswordEnabled()
    {
        return getBoolean(SYSTEM_OPTION_ENABLE_MASTER_PASSWORD, SYSTEM_OPTION_ENABLE_MASTER_PASSWORD_DEFAULT);
    }

    public void setEnableMasterPassword(boolean value)
    {
        saveBoolean(SYSTEM_OPTION_ENABLE_MASTER_PASSWORD, value);
    }

    public boolean isVpnLockEnabled()
    {
        return getBoolean(SYSTEM_OPTION_VPN_LOCK, SYSTEM_OPTION_VPN_LOCK_DEFAULT);
    }

    public void setVpnLock(boolean value)
    {
        saveBoolean(SYSTEM_OPTION_VPN_LOCK, value);
    }

    public boolean isVpnReconnectEnabled()
    {
        return getBoolean(SYSTEM_OPTION_VPN_RECONNECT, SYSTEM_OPTION_VPN_RECONNECT_DEFAULT);
    }

    public void setVpnReconnect(boolean value)
    {
        saveBoolean(SYSTEM_OPTION_VPN_RECONNECT, value);
    }

    public int getVpnReconnectionRetries()
    {
        return getInt(SYSTEM_OPTION_VPN_RECONNECTION_RETRIES, SYSTEM_OPTION_VPN_RECONNECTION_RETRIES_DEFAULT);
    }

    public void setVpnReconnectionRetries(int value)
    {
        saveInt(SYSTEM_OPTION_VPN_RECONNECTION_RETRIES, value);
    }

    public boolean isVpnMetered()
    {
        return getBoolean(SYSTEM_OPTION_METERED_VPN, SYSTEM_OPTION_METERED_VPN_DEFAULT);
    }

    public void setMeteredVpn(boolean value)
    {
        saveBoolean(SYSTEM_OPTION_METERED_VPN, value);
    }

    public String getOvpn3Username()
    {
        return getString(OVPN3_OPTION_USERNAME, OVPN3_OPTION_USERNAME_DEFAULT);
    }

    public void setOvpn3Username(String value)
    {
        saveString(OVPN3_OPTION_USERNAME, value);
    }

    public String getOvpn3Password()
    {
        return getString(OVPN3_OPTION_PASSWORD, OVPN3_OPTION_PASSWORD_DEFAULT);
    }

    public void setOvpn3Password(String value)
    {
        saveString(OVPN3_OPTION_PASSWORD, value);
    }

    public boolean isOvpn3SynchronousDNSLookup()
    {
        return getBoolean(OVPN3_OPTION_SYNCHRONOUS_DNS_LOOKUP, OVPN3_OPTION_SYNCHRONOUS_DNS_LOOKUP_DEFAULT);
    }

    public void setOvpn3SynchronousDNSLookup(boolean value)
    {
        saveBoolean(OVPN3_OPTION_SYNCHRONOUS_DNS_LOOKUP, value);
    }

    public boolean isOvpn3AutologinSessions()
    {
        return getBoolean(OVPN3_OPTION_AUTOLOGIN_SESSIONS, OVPN3_OPTION_AUTOLOGIN_SESSIONS_DEFAULT);
    }

    public void setOvpn3AutologinSessions(boolean value)
    {
        saveBoolean(OVPN3_OPTION_AUTOLOGIN_SESSIONS, value);
    }

    public boolean isOvpn3DisableClientCert()
    {
        return getBoolean(OVPN3_OPTION_DISABLE_CLIENT_CERT, OVPN3_OPTION_DISABLE_CLIENT_CERT_DEFAULT);
    }

    public void setOvpn3DisableClientCert(boolean value)
    {
        saveBoolean(OVPN3_OPTION_DISABLE_CLIENT_CERT, value);
    }

    public String getOvpn3SSLDebugLevel()
    {
        return getString(OVPN3_OPTION_SSL_DEBUG_LEVEL, OVPN3_OPTION_SSL_DEBUG_LEVEL_DEFAULT);
    }

    public void setOvpn3SSLDebugLevel(String value)
    {
        saveString(OVPN3_OPTION_SSL_DEBUG_LEVEL, value);
    }

    public long getOvpn3SSLDebugLevelValue()
    {
        long defVal = 0;

        try
        {
            defVal = Long.parseLong(OVPN3_OPTION_SSL_DEBUG_LEVEL_DEFAULT);
        }
        catch(NumberFormatException e)
        {
            defVal = 0;
        }

        return getLong(OVPN3_OPTION_SSL_DEBUG_LEVEL, defVal);
    }

    public void setOvpn3SSLDebugLevelValue(long value)
    {
        String prefVal = "";

        if(value <= 0)
            prefVal = OVPN3_OPTION_SSL_DEBUG_LEVEL_DEFAULT;
        else
            prefVal = String.format(Locale.getDefault(), "%d", value);

        saveString(OVPN3_OPTION_SSL_DEBUG_LEVEL, prefVal);
    }

    public String getOvpn3PrivateKeyPassword()
    {
        return getString(OVPN3_OPTION_PRIVATE_KEY_PASSWORD, OVPN3_OPTION_PRIVATE_KEY_PASSWORD_DEFAULT);
    }

    public void setOvpn3PrivateKeyPassword(String value)
    {
        saveString(OVPN3_OPTION_PRIVATE_KEY_PASSWORD, value);
    }

    public String getOvpn3DefaultKeyDirection()
    {
        return getString(OVPN3_OPTION_DEFAULT_KEY_DIRECTION, OVPN3_OPTION_DEFAULT_KEY_DIRECTION_DEFAULT);
    }

    public void setOvpn3DefaultKeyDirection(String value)
    {
        saveString(OVPN3_OPTION_DEFAULT_KEY_DIRECTION, value);
    }

    public String getOvpn3TLSCertProfile()
    {
        return getString(OVPN3_OPTION_TLS_CERT_PROFILE, OVPN3_OPTION_TLS_CERT_PROFILE_DEFAULT);
    }

    public void setOvpn3TLSCertProfile(String value)
    {
        saveString(OVPN3_OPTION_TLS_CERT_PROFILE, value);
    }

    public String getOvpn3ProxyHost()
    {
        return getString(OVPN3_OPTION_PROXY_HOST, OVPN3_OPTION_PROXY_HOST_DEFAULT);
    }
    
    public void setOvpn3ProxyHost(String value)
    {
        saveString(OVPN3_OPTION_PROXY_HOST, value);
    }

    public String getOvpn3ProxyPort()
    {
        return getString(OVPN3_OPTION_PROXY_PORT, OVPN3_OPTION_PROXY_PORT_DEFAULT);
    }

    public void setOvpn3ProxyPort(String value)
    {
        saveString(OVPN3_OPTION_PROXY_PORT, value);
    }

    public long getOvpn3ProxyPortValue()
    {
        long defVal = 0;

        try
        {
            defVal = Long.parseLong(OVPN3_OPTION_PROXY_PORT_DEFAULT);
        }
        catch(NumberFormatException e)
        {
            defVal = 0;
        }

        return getLong(OVPN3_OPTION_PROXY_PORT, defVal);
    }

    public void setOvpn3ProxyPortValue(long value)
    {
        String prefVal = "";

        if(value <= 0)
            prefVal = OVPN3_OPTION_PROXY_PORT_DEFAULT;
        else
            prefVal = String.format(Locale.getDefault(), "%d", value);

        saveString(OVPN3_OPTION_PROXY_PORT, prefVal);
    }

    public String getOvpn3ProxyUsername()
    {
        return getString(OVPN3_OPTION_PROXY_USERNAME, OVPN3_OPTION_PROXY_USERNAME_DEFAULT);
    }

    public void setOvpn3ProxyUsername(String value)
    {
        saveString(OVPN3_OPTION_PROXY_USERNAME, value);
    }

    public String getOvpn3ProxyPassword()
    {
        return getString(OVPN3_OPTION_PROXY_PASSWORD, OVPN3_OPTION_PROXY_PASSWORD_DEFAULT);
    }

    public void setOvpn3ProxyPassword(String value)
    {
        saveString(OVPN3_OPTION_PROXY_PASSWORD, value);
    }

    public boolean isOvpn3ProxyAllowCleartextAuth()
    {
        return getBoolean(OVPN3_OPTION_PROXY_ALLOW_CLEARTEXT_AUTH, OVPN3_OPTION_PROXY_ALLOW_CLEARTEXT_AUTH_DEFAULT);
    }

    public void setOvpn3ProxyAllowCleartextAuth(boolean value)
    {
        saveBoolean(OVPN3_OPTION_PROXY_ALLOW_CLEARTEXT_AUTH, value);
    }

    public String getOvpn3CustomDirectives()
    {
        return getString(OVPN3_OPTION_CUSTOM_DIRECTIVES, OVPN3_OPTION_CUSTOM_DIRECTIVES_DEFAULT);
    }

    public void setOvpn3CustomDirectives(String value)
    {
        saveString(OVPN3_OPTION_CUSTOM_DIRECTIVES, value);
    }

    public boolean isSystemDNSOverrideEnable()
    {
        return getBoolean(SYSTEM_OPTION_DNS_OVERRIDE_ENABLE, SYSTEM_OPTION_DNS_OVERRIDE_ENABLE_DEFAULT);
    }

    public void setSystemDNSOverrideEnable(boolean value)
    {
        saveBoolean(SYSTEM_OPTION_DNS_OVERRIDE_ENABLE, value);
    }

    public String getSystemCustomDNS()
    {
        return getString(SYSTEM_OPTION_DNS_CUSTOM, SYSTEM_OPTION_DNS_CUSTOM_DEFAULT);
    }

    public void setSystemCustomDNS(String value)
    {
        saveString(SYSTEM_OPTION_DNS_CUSTOM, value);
    }

    public ArrayList<String> getSystemDNSCustomList()
    {
        ArrayList<String> list = new ArrayList<String>();
        String[] valArray = null;
        String prefVal = "";

        prefVal = getString(SYSTEM_OPTION_DNS_CUSTOM, SYSTEM_OPTION_DNS_CUSTOM_DEFAULT);

        valArray = prefVal.split(DEFAULT_SPLIT_SEPARATOR);

        for(String item : valArray)
            list.add(item);

        return list;
    }
        
    public void setSystemDNSCustomList(ArrayList<String> value)
    {
        String prefVal = "";

        for(String item : value)
        {
            if(!prefVal.equals(""))
                prefVal += DEFAULT_SPLIT_SEPARATOR;

            prefVal += item;
        }

        if(prefVal != null)
            saveString(SYSTEM_OPTION_DNS_CUSTOM, prefVal);
    }

    public boolean isSystemProxyEnabled()
    {
        return getBoolean(SYSTEM_OPTION_PROXY_ENABLE, SYSTEM_OPTION_PROXY_ENABLE_DEFAULT);
    }

    public void setSystemProxyEnabled(boolean value)
    {
        saveBoolean(SYSTEM_OPTION_PROXY_ENABLE, value);
    }

    public boolean isSystemNotificationSound()
    {
        return getBoolean(SYSTEM_OPTION_NOTIFICATION_SOUND, SYSTEM_OPTION_NOTIFICATION_SOUND_DEFAULT);
    }

    public void setSystemNotificationSound(boolean value)
    {
        saveBoolean(SYSTEM_OPTION_NOTIFICATION_SOUND, value);
    }

    public boolean areMessageDialogsEnabled()
    {
        return getBoolean(SYSTEM_OPTION_SHOW_MESSAGE_DIALOGS, SYSTEM_OPTION_SHOW_MESSAGE_DIALOGS_DEFAULT);
    }

    public void setMessageDialogsEnabled(boolean value)
    {
        saveBoolean(SYSTEM_OPTION_SHOW_MESSAGE_DIALOGS, value);
    }

    public String getSystemCustomMTU()
    {
        return getString(SYSTEM_CUSTOM_MTU, SYSTEM_CUSTOM_MTU_DEFAULT);
    }

    public void setSystemCustomMTU(String value)
    {
        saveString(SYSTEM_CUSTOM_MTU, value);
    }

    public int getSystemCustomMTUValue()
    {
        int defVal = 0;

        try
        {
            defVal = Integer.parseInt(SYSTEM_CUSTOM_MTU_DEFAULT);
        }
        catch(NumberFormatException e)
        {
            defVal = 0;
        }

        return getInt(SYSTEM_CUSTOM_MTU, defVal);
    }

    public void setSystemCustomMTUValue(long value)
    {
        String prefVal = "";

        if(value <= 0)
            prefVal = SYSTEM_CUSTOM_MTU_DEFAULT;
        else
            prefVal = String.format(Locale.getDefault(), "%d", value);

        saveString(SYSTEM_CUSTOM_MTU, prefVal);
    }

    public String getSystemApplicationFilterType()
    {
        return getString(SYSTEM_OPTION_APPLICATION_FILTER_TYPE, SYSTEM_OPTION_APPLICATION_FILTER_TYPE_DEFAULT);
    }

    public void setSystemApplicationFilterType(String value)
    {
        saveString(SYSTEM_OPTION_APPLICATION_FILTER_TYPE, value);
    }

    public String getSystemApplicationLanguage()
    {
        return getString(SYSTEM_OPTION_APPLICATION_LANGUAGE, SYSTEM_OPTION_APPLICATION_LANGUAGE_DEFAULT);
    }

    public void setSystemApplicationLanguage(String value)
    {
        saveString(SYSTEM_OPTION_APPLICATION_LANGUAGE, value);
    }

    public String getSystemApplicationTheme()
    {
        return getString(SYSTEM_OPTION_APPLICATION_THEME, SYSTEM_OPTION_APPLICATION_THEME_DEFAULT);
    }

    public void setSystemApplicationTheme(String value)
    {
        saveString(SYSTEM_OPTION_APPLICATION_THEME, value);
    }

    public boolean isGpsSpoofingEnabled()
    {
        return getBoolean(SYSTEM_OPTION_GPS_SPOOFING, SYSTEM_OPTION_GPS_SPOOFING_DEFAULT);
    }

    public void setGpsSpoofingInterval(int value)
    {
        saveInt(SYSTEM_OPTION_GPS_SPOOFING_INTERVAL, value);
    }

    public int getGpsSpoofingInterval()
    {
        return getInt(SYSTEM_OPTION_GPS_SPOOFING_INTERVAL, SYSTEM_OPTION_GPS_SPOOFING_INTERVAL_DEFAULT);
    }

    public void setGpsSpoofing(boolean value)
    {
        saveBoolean(SYSTEM_OPTION_GPS_SPOOFING, value);
    }


    public String getSystemApplicationFilter()
    {
        return getString(SYSTEM_OPTION_APPLICATION_FILTER, SYSTEM_OPTION_APPLICATION_FILTER_DEFAULT);
    }

    public void setSystemApplicationFilter(String value)
    {
        saveString(SYSTEM_OPTION_APPLICATION_FILTER, value);
    }

    public ArrayList<String> getSystemApplicationFilterList()
    {
        ArrayList<String> list = new ArrayList<String>();
        String[] valArray = null;
        String prefVal = "";

        prefVal = getString(SYSTEM_OPTION_APPLICATION_FILTER, SYSTEM_OPTION_APPLICATION_FILTER_DEFAULT);

        if(!prefVal.equals(""))
        {
            valArray = prefVal.split(DEFAULT_SPLIT_SEPARATOR);

            for(String item : valArray)
                list.add(item);
        }

        return list;
    }
        
    public void setSystemApplicationFilterList(ArrayList<String> value)
    {
        String prefVal = "";

        for(String item : value)
        {
            if(!prefVal.equals(""))
                prefVal += DEFAULT_SPLIT_SEPARATOR;

            prefVal += item;
        }

        if(prefVal != null)
            saveString(SYSTEM_OPTION_APPLICATION_FILTER, prefVal);
    }

    public boolean isSystemFirstRun()
    {
        return getBoolean(SYSTEM_OPTION_FIRST_RUN, SYSTEM_OPTION_FIRST_RUN_DEFAULT);
    }

    public void setSystemFirstRun(boolean value)
    {
        saveBoolean(SYSTEM_OPTION_FIRST_RUN, value);
    }

    public boolean isStartVpnAtStartupEnabled()
    {
        return getBoolean(SYSTEM_OPTION_START_VPN_AT_STARTUP, SYSTEM_OPTION_START_VPN_AT_STARTUP_DEFAULT);
    }

    public void setStartVpnAtStartup(boolean value)
    {
        saveBoolean(SYSTEM_OPTION_START_VPN_AT_STARTUP, value);
    }

    public void setVPNBootPriorityList(ArrayList<VPNBootOption> vpnBootOptions)
    {
        String vpnBootPriorityList = "";

        if(vpnBootOptions != null)
        {
            for(VPNBootOption option : vpnBootOptions)
                vpnBootPriorityList += option.code + ",";

            if(!vpnBootPriorityList.isEmpty())
                vpnBootPriorityList.substring(0, vpnBootPriorityList.length() - 1);
        }

        saveString(SYSTEM_OPTION_VPN_BOOT_PRIORITY, vpnBootPriorityList);
    }

    public void setVPNBootPriorityList(String vpnBootPriorityList)
    {
        saveString(SYSTEM_OPTION_VPN_BOOT_PRIORITY, vpnBootPriorityList);
    }

    public ArrayList<VPNBootOption> getVPNBootPriorityList()
    {
        int i;
        String currentVPNBootPriorityList = getString(SYSTEM_OPTION_VPN_BOOT_PRIORITY, SYSTEM_OPTION_VPN_BOOT_PRIORITY_DEFAULT);
        ArrayList<VPNBootOption> vpnBootPriorityItem = new ArrayList<VPNBootOption>();
        ArrayList<VPNBootOption> defaultVpnBootPriorityItem = new ArrayList<VPNBootOption>();
        HashMap<String, VPNBootOption> defaultOption = new HashMap<String, VPNBootOption>();
        String[] bootPriorityLabel = EddieApplication.context().getResources().getStringArray(R.array.vpn_boot_priority_labels);
        String[] bootPriorityValue = EddieApplication.context().getResources().getStringArray(R.array.vpn_boot_priority_values);
        String[] bootPriorityCondition = EddieApplication.context().getResources().getStringArray(R.array.vpn_boot_priority_conditions);

        for(i = 0; i < bootPriorityLabel.length; i++)
        {
            VPNBootOption bootOption = new VPNBootOption();
            ArrayList<String> condition = new ArrayList<String>();

            String[] bootCondition = bootPriorityCondition[i].split(",");

            for(String item: bootCondition)
                condition.add(item);

            bootOption.code = bootPriorityValue[i];
            bootOption.description = bootPriorityLabel[i];
            bootOption.condition = condition;

            defaultVpnBootPriorityItem.add(bootOption);

            defaultOption.put(bootOption.code, bootOption);
        }

        String[] currentOptions = currentVPNBootPriorityList.split(",");

        for(String item : currentOptions)
        {
            VPNBootOption option = defaultOption.get(item);

            if(isVpnBootOptionValid(option))
                vpnBootPriorityItem.add(option);

            defaultVpnBootPriorityItem.remove(option);
        }

        for(VPNBootOption option : defaultVpnBootPriorityItem)
        {
            if(isVpnBootOptionValid(option))
                vpnBootPriorityItem.add(option);
        }

        return vpnBootPriorityItem;
    }

    public boolean isVpnBootOptionValid(VPNBootOption option)
    {
        boolean isValid = true;

        if(option == null)
            return false;

        for(String condition : option.condition)
        {
            switch(condition)
            {
                case VPN_BOOT_CONDITION_MP_DISABLED:
                {
                    if(isMasterPasswordEnabled() == true)
                        isValid = false;
                }
                break;

                case VPN_BOOT_CONDITION_AIRVPN_CREDENTIALS:
                {
                    if(EddieApplication.airVPNUser().areCredendialsUsable() == false)
                        isValid = false;
                }
                break;

                case VPN_BOOT_CONDITION_STARTUP_AIRVPN_SERVER:
                {
                    if(getStartupAirvpnServer().isEmpty() == true)
                        isValid = false;
                }
                break;

                case VPN_BOOT_CONDITION_STARTUP_AIRVPN_COUNTRY:
                {
                    if(getStartupAirVPNCountry().isEmpty() == true)
                        isValid = false;
                }
                break;

                case VPN_BOOT_CONDITION_STARTUP_VPN_PROFILE:
                {
                    VPNProfileDatabase vpnProfileDatabase = new VPNProfileDatabase(EddieApplication.context());

                    if(vpnProfileDatabase.getStartupProfile() == null)
                        isValid = false;
                }
                break;

                case VPN_BOOT_CONDITION_LAST_CONNECTED_PROFILE:
                {
                    if(isStartVpnAtStartupEnabled() == false)
                        isValid = false;
                }
                break;
            }
        }

        return isValid;
    }

    public boolean isSystemLastProfileIsConnected()
    {
        return getBoolean(SYSTEM_OPTION_LAST_PROFILE_IS_CONNECTED, SYSTEM_OPTION_LAST_PROFILE_IS_CONNECTED_DEFAULT);
    }

    public void setSystemLastProfileIsConnected(boolean value)
    {
        saveBoolean(SYSTEM_OPTION_LAST_PROFILE_IS_CONNECTED, value);

        if(value == false)
        {
            setLastVPNProfile("");

            setSystemLastProfileInfo(new HashMap<String, String>());
        }
    }

    public String getAlwaysOnVpnStatus()
    {
        return getString(SYSTEM_IS_ALWAYS_ON_VPN, SYSTEM_IS_ALWAYS_ON_VPN_DEFAULT);
    }

    public void setAlwaysOnVpn(String value)
    {
        if(!value.equals(ON) && !value.equals(OFF) && !value.equals(UNKNOWN))
            return;

        saveString(SYSTEM_IS_ALWAYS_ON_VPN, value);
    }

    public boolean isAlwaysOnVpnSet()
    {
        return getAlwaysOnVpnStatus().equals(SettingsManager.ON);
    }

    public String getVpnLockdownStatus()
    {
        return getString(SYSTEM_VPN_LOCKOWN, SYSTEM_VPN_LOCKOWN_DEFAULT);
    }

    public void setVpnLockdown(String value)
    {
        if(!value.equals(ON) && !value.equals(OFF) && !value.equals(UNKNOWN))
            return;

        saveString(SYSTEM_VPN_LOCKOWN, value);
    }

    public boolean isVpnLockdownOn()
    {
        return getVpnLockdownStatus().equals(SettingsManager.ON);
    }

    public int getAirVPNMasterPasswordHashCode()
    {
        return getInt(AIRVPN_MASTER_PASSWORD_HASHCODE, AIRVPN_MASTER_PASSWORD_HASHCODE_DEFAULT);
    }

    public void setAirVPNMasterPasswordHashCode(int value)
    {
        saveInt(AIRVPN_MASTER_PASSWORD_HASHCODE, value);
    }

    public boolean isAirVPNRememberMe()
    {
        return getBoolean(SYSTEM_AIRVPN_REMEMBER_ME, SYSTEM_AIRVPN_REMEMBER_ME_DEFAULT);
    }

    public void setAirVPNRememberMe(boolean value)
    {
        saveBoolean(SYSTEM_AIRVPN_REMEMBER_ME, value);
    }

    public boolean isAirVPNAutologinEnabled()
    {
        return getBoolean(SYSTEM_AIRVPN_AUTOLOGIN, SYSTEM_AIRVPN_AUTOLOGIN_DEFAULT);
    }

    public void setAirVPNAutologin(boolean value)
    {
        saveBoolean(SYSTEM_AIRVPN_AUTOLOGIN, value);
    }

    public String getAirVPNCurrentLocalCountry()
    {
        return getString(AIRVPN_CURRENT_LOCAL_COUNTRY, AIRVPN_CURRENT_LOCAL_COUNTRY_DEFAULT);
    }

    public void setAirVPNCurrentLocalCountry(String value)
    {
        saveString(AIRVPN_CURRENT_LOCAL_COUNTRY, value);
    }

    public String getAirVPNDefaultVPNType()
    {
        return getString(AIRVPN_DEFAULT_VPN_TYPE, AIRVPN_VPN_TYPE_DEFAULT);
    }

    public void setAirVPNDefaultVPNType(String value)
    {
        saveString(AIRVPN_DEFAULT_VPN_TYPE, value);
    }

    public String getAirVPNOpenVPNMode()
    {
        return getString(AIRVPN_OPENVPN_MODE, AIRVPN_OPENVPN_MODE_DEFAULT);
    }

    public void setAirVPNOpenVPNMode(String value)
    {
        saveString(AIRVPN_OPENVPN_MODE, value);
    }

    public String getAirVPNDefaultOpenVPNProtocol()
    {
        return getString(AIRVPN_DEFAULT_OPENVPN_PROTOCOL, AIRVPN_OPENVPN_PROTOCOL_DEFAULT);
    }

    public void setAirVPNDefaultOpenVPNProtocol(String value)
    {
        saveString(AIRVPN_DEFAULT_OPENVPN_PROTOCOL, value);
    }

    public int getAirVPNDefaultOpenVPNPort()
    {
        return getInt(AIRVPN_DEFAULT_OPENVPN_PORT, AIRVPN_OPENVPN_PORT_DEFAULT);
    }

    public void setAirVPNDefaultOpenVPNPort(int value)
    {
        saveInt(AIRVPN_DEFAULT_OPENVPN_PORT, value);
    }

    public String getAirVPNWireGuardMode()
    {
        return getString(AIRVPN_WIREGUARD_MODE, AIRVPN_WIREGUARD_MODE_DEFAULT);
    }

    public void setAirVPNWireGuardMode(String value)
    {
        saveString(AIRVPN_WIREGUARD_MODE, value);
    }

    public int getAirVPNDefaultWireGuardPort()
    {
        return getInt(AIRVPN_DEFAULT_WIREGUARD_PORT, AIRVPN_WIREGUARD_PORT_DEFAULT);
    }

    public void setAirvpnDefaultWireguardPort(int value)
    {
        saveInt(AIRVPN_DEFAULT_WIREGUARD_PORT, value);
    }

    public String getAirVPNDefaultIPVersion()
    {
        return getString(AIRVPN_DEFAULT_IP_VERSION, AIRVPN_IP_VERSION_DEFAULT);
    }

    public void setAirVPNDefaultIPVersion(String value)
    {
        saveString(AIRVPN_DEFAULT_IP_VERSION, value);
    }

    public String getAirVPNDefaultTLSMode()
    {
        return getString(AIRVPN_DEFAULT_TLS_MODE, AIRVPN_TLS_MODE_DEFAULT);
    }

    public void setAirVPNDefaultTLSMode(String value)
    {
        saveString(AIRVPN_DEFAULT_TLS_MODE, value);
    }

    public String getAirVPNOpenVPNCipher()
    {
        return getString(AIRVPN_OPENVPN_CIPHER, AIRVPN_OPENVPN_CIPHER_DEFAULT);
    }

    public void setAirVPNOpenVPNCipher(String value)
    {
        saveString(AIRVPN_OPENVPN_CIPHER, value);
    }

    public String getAirVPNSortBy()
    {
        return getString(AIRVPN_SERVER_SORT_BY, AIRVPN_SERVER_SORT_BY_DEFAULT);
    }

    public void setAirVPNSortBy(String value)
    {
        saveString(AIRVPN_SERVER_SORT_BY, value);
    }

    public String getAirVPNSortMode()
    {
        return getString(AIRVPN_SERVER_SORT_MODE, AIRVPN_SERVER_SORT_MODE_DEFAULT);
    }

    public void setAirVPNSortMode(String value)
    {
        saveString(AIRVPN_SERVER_SORT_MODE, value);
    }

    public String getAirVPNQuickConnectMode()
    {
        return getString(AIRVPN_QUICK_CONNECT_MODE, AIRVPN_QUICK_CONNECT_MODE_DEFAULT);
    }

    public void setAirVPNQuickConnectMode(String value)
    {
        saveString(AIRVPN_QUICK_CONNECT_MODE, value);
    }

    public void setAirVPNForbidQuickConnectionToUserCountry(boolean value)
    {
        saveBoolean(AIRVPN_FORBID_QUICK_CONNECTION_TO_USER_COUNTRY, value);
    }

    public boolean isAirVPNForbidQuickConnectionToUserCountry()
    {
        return getBoolean(AIRVPN_FORBID_QUICK_CONNECTION_TO_USER_COUNTRY, AIRVPN_FORBID_QUICK_CONNECTION_TO_USER_COUNTRY_DEFAULT);
    }

    public String getAirVPNCustomBootstrap()
    {
        return getString(AIRVPN_CUSTOM_BOOTSTRAP_SERVERS, AIRVPN_CUSTOM_BOOTSTRAP_SERVERS_DEFAULT);
    }

    public void setAirVPNCustomBootstrap(String value)
    {
        saveString(AIRVPN_CUSTOM_BOOTSTRAP_SERVERS, value);
    }

    public ArrayList<String> getAirVPNCustomBootstrapList()
    {
        ArrayList<String> list = new ArrayList<String>();
        String[] valArray = null;
        String prefVal = "";

        prefVal = getString(AIRVPN_CUSTOM_BOOTSTRAP_SERVERS, AIRVPN_CUSTOM_BOOTSTRAP_SERVERS_DEFAULT);

        valArray = prefVal.split(DEFAULT_SPLIT_SEPARATOR);

        for(String item : valArray)
            list.add(item);

        return list;
    }

    public void setAirVPNCustomBootstrapList(ArrayList<String> value)
    {
        String prefVal = "";

        for(String item : value)
        {
            if(!prefVal.equals(""))
                prefVal += DEFAULT_SPLIT_SEPARATOR;

            prefVal += item;
        }

        if(prefVal != null)
            saveString(AIRVPN_CUSTOM_BOOTSTRAP_SERVERS, prefVal);
    }

    public boolean isAirVPNConfirmServerConnectionEnabled()
    {
        return getBoolean(AIRVPN_CONFIRM_SERVER_CONNECTION, AIRVPN_CONFIRM_SERVER_CONNECTION_DEFAULT);
    }

    public void setAirVPNConfirmServerConnection(boolean value)
    {
        saveBoolean(AIRVPN_CONFIRM_SERVER_CONNECTION, value);
    }

    public String getLastVPNProfile()
    {
        byte[] b64 = null;
        String profile = getString(SYSTEM_OPTION_LAST_PROFILE, "");

        if(!profile.equals(""))
        {
            b64 = Base64.decode(profile, Base64.NO_WRAP);

            return new String(b64, StandardCharsets.UTF_8);
        }
        else
            return "";
    }

    public void setLastVPNProfile(String value)
    {
        if(value != null)
        {
            String profile = Base64.encodeToString(value.getBytes(StandardCharsets.UTF_8), Base64.NO_WRAP);

            saveString(SYSTEM_OPTION_LAST_PROFILE, profile);
        }
    }

    public HashMap<String, String> getSystemLastProfileInfo()
    {
        HashMap<String, String> pData = null;
        String info, profile = "";
        String[] items = null, entry = null;
        byte[] b64 = null;

        profile = getString(SYSTEM_OPTION_LAST_PROFILE_INFO, "");

        if(!profile.equals(""))
        {
            b64 = Base64.decode(profile, Base64.NO_WRAP);

            info = new String(b64, StandardCharsets.UTF_8);

            if(!info.equals(""))
            {
                pData = new HashMap<>();

                items = info.split("\\|");

                for(String item : items)
                {
                    entry = item.split(":");

                    if(entry.length == 2)
                        pData.put(entry[0], entry[1]);
                }
            }
        }

        return pData;
    }

    public void setSystemLastProfileInfo(HashMap<String, String> value)
    {
        String info = "";

        for(Map.Entry<String, String> item : value.entrySet())
        {
            if(!info.equals(""))
                info += "|";

            info += item.getKey() + ":" + item.getValue();
        }

        if(info != null)
            saveString(SYSTEM_OPTION_LAST_PROFILE_INFO, Base64.encodeToString(info.getBytes(StandardCharsets.UTF_8), Base64.NO_WRAP));
    }

    public boolean areLocalNetworksExcluded()
    {
        return getBoolean(SYSTEM_OPTION_EXCLUDE_LOCAL_NETWORKS, SYSTEM_OPTION_EXCLUDE_LOCAL_NETWORKS_DEFAULT);
    }

    public void setExcludeLocalNetworks(boolean value)
    {
        saveBoolean(SYSTEM_OPTION_EXCLUDE_LOCAL_NETWORKS, value);
    }

    public boolean isSystemPauseVpnWhenScreenIsOff()
    {
        return getBoolean(SYSTEM_OPTION_PAUSE_VPN_WHEN_SCREEN_IS_OFF, SYSTEM_OPTION_PAUSE_VPN_WHEN_SCREEN_IS_OFF_DEFAULT);
    }

    public void setSystemPauseVpnWhenScreenIsOff(boolean value)
    {
        saveBoolean(SYSTEM_OPTION_PAUSE_VPN_WHEN_SCREEN_IS_OFF, value);
    }

    public ArrayList<String> getAirVPNServerWhitelist()
    {
        ArrayList<String> list = new ArrayList<String>();
        String[] valArray = null;
        String prefVal = "";

        prefVal = getString(AIRVPN_SERVER_WHITELIST, AIRVPN_SERVER_WHITELIST_DEFAULT);

        if(!prefVal.equals(""))
        {
            valArray = prefVal.split(DEFAULT_SPLIT_SEPARATOR);

            for(String item : valArray)
                list.add(item);
        }

        return list;
    }

    public void setAirVPNServerWhitelist(ArrayList<String> value)
    {
        String prefVal = "";

        if(value != null)
        {
            for(String item : value)
            {
                if(!prefVal.equals(""))
                    prefVal += DEFAULT_SPLIT_SEPARATOR;

                prefVal += item;
            }
        }

        if(prefVal != null)
            saveString(AIRVPN_SERVER_WHITELIST, prefVal);
    }

    public boolean isAirVPNServerWhitelisted(String name)
    {
        String prefVal = "";

        prefVal = getString(AIRVPN_SERVER_WHITELIST, AIRVPN_SERVER_WHITELIST_DEFAULT);

        return prefVal.contains(name);
    }

    public ArrayList<String> getAirVPNServerBlacklist()
    {
        ArrayList<String> list = new ArrayList<String>();
        String[] valArray = null;
        String prefVal = "";

        prefVal = getString(AIRVPN_SERVER_BLACKLIST, AIRVPN_SERVER_BLACKLIST_DEFAULT);

        if(!prefVal.equals(""))
        {
            valArray = prefVal.split(DEFAULT_SPLIT_SEPARATOR);

            for(String item : valArray)
                list.add(item);
        }

        return list;
    }

    public void setAirVPNServerBlacklist(ArrayList<String> value)
    {
        String prefVal = "";

        if(value != null)
        {
            for(String item : value)
            {
                if(!prefVal.equals(""))
                    prefVal += DEFAULT_SPLIT_SEPARATOR;

                prefVal += item;
            }
        }

        if(prefVal != null)
            saveString(AIRVPN_SERVER_BLACKLIST, prefVal);
    }

    public boolean isAirVPNServerBlacklisted(String name)
    {
        String prefVal = "";

        prefVal = getString(AIRVPN_SERVER_BLACKLIST, AIRVPN_SERVER_BLACKLIST_DEFAULT);

        return prefVal.contains(name);
    }

    public void setStartupAirVPNServer(String name)
    {
        if(name.isEmpty())
            return;

        saveString(STARTUP_AIRVPN_SERVER, name);
    }

    public void removeStartupAirVPNServer()
    {
        saveString(STARTUP_AIRVPN_SERVER, STARTUP_AIRVPN_SERVER_DEFAULT);
    }

    public String getStartupAirvpnServer()
    {
        return getString(STARTUP_AIRVPN_SERVER, STARTUP_AIRVPN_SERVER_DEFAULT);
    }

    public ArrayList<String> getAirVPNCountryWhitelist()
    {
        ArrayList<String> list = new ArrayList<String>();
        String[] valArray = null;
        String prefVal = "";

        prefVal = getString(AIRVPN_COUNTRY_WHITELIST, AIRVPN_COUNTRY_WHITELIST_DEFAULT);

        if(!prefVal.equals(""))
        {
            valArray = prefVal.split(DEFAULT_SPLIT_SEPARATOR);

            for(String item : valArray)
                list.add(item);
        }

        return list;
    }

    public void setAirVPNCountryWhitelist(ArrayList<String> value)
    {
        String prefVal = "";

        if(value != null)
        {
            for(String item : value)
            {
                if(!prefVal.equals(""))
                    prefVal += DEFAULT_SPLIT_SEPARATOR;

                prefVal += item;
            }
        }

        if(prefVal != null)
            saveString(AIRVPN_COUNTRY_WHITELIST, prefVal);
    }

    public boolean isAirVPNCountryWhitelisted(String name)
    {
        String prefVal = "";

        prefVal = getString(AIRVPN_COUNTRY_WHITELIST, AIRVPN_COUNTRY_WHITELIST_DEFAULT);

        return prefVal.contains(name);
    }

    public ArrayList<String> getAirVPNCountryBlacklist()
    {
        ArrayList<String> list = new ArrayList<String>();
        String[] valArray = null;
        String prefVal = "";

        prefVal = getString(AIRVPN_COUNTRY_BLACKLIST, AIRVPN_COUNTRY_BLACKLIST_DEFAULT);

        if(!prefVal.equals(""))
        {
            valArray = prefVal.split(DEFAULT_SPLIT_SEPARATOR);

            for(String item : valArray)
                list.add(item);
        }

        return list;
    }

    public void setAirVPNCountryBlacklist(ArrayList<String> value)
    {
        String prefVal = "";

        if(value != null)
        {
            for(String item : value)
            {
                if(!prefVal.equals(""))
                    prefVal += DEFAULT_SPLIT_SEPARATOR;

                prefVal += item;
            }
        }

        if(prefVal != null)
            saveString(AIRVPN_COUNTRY_BLACKLIST, prefVal);
    }

    public boolean isAirVPNCountryBlacklisted(String name)
    {
        String prefVal = "";

        prefVal = getString(AIRVPN_COUNTRY_BLACKLIST, AIRVPN_COUNTRY_BLACKLIST_DEFAULT);

        return prefVal.contains(name);
    }

    public void setStartupAirVPNCountry(String code)
    {
        if(code.isEmpty())
            return;

        saveString(STARTUP_AIRVPN_COUNTRY, code);
    }

    public void removeStartupAirVPNCountry()
    {
        saveString(STARTUP_AIRVPN_COUNTRY, STARTUP_AIRVPN_COUNTRY_DEFAULT);
    }

    public String getStartupAirVPNCountry()
    {
        return getString(STARTUP_AIRVPN_COUNTRY, STARTUP_AIRVPN_COUNTRY_DEFAULT);
    }

    public ArrayList<Long> getDoNotShowAgainBanners()
    {
        ArrayList<Long> shownBanner = new ArrayList<Long>();

        try
        {
            JSONArray bannerJsonArray = new JSONArray(getString(AIRVPN_DO_NOT_SHOW_AGAIN_MESSAGES, AIRVPN_DO_NOT_SHOW_AGAIN_MESSAGES_DEFAULT));

            for(int i = 0; i < bannerJsonArray.length(); i++)
                shownBanner.add(bannerJsonArray.getLong(i));
        }
        catch(Exception e)
        {
        }

        return shownBanner;
    }

    public void addDoNotShowAgainBanners(long timeStamp)
    {
        ArrayList<Long> shownBanner = getDoNotShowAgainBanners();
        JSONArray bannerJsonArray = new JSONArray();
        long utcTimestamp = System.currentTimeMillis() / 1000;

        for(int i = 0; i < shownBanner.size(); i++)
        {
            if(shownBanner.get(i) < utcTimestamp)
                shownBanner.remove(i);
        }

        shownBanner.add(timeStamp);

        for(Long ts : shownBanner)
            bannerJsonArray.put(ts);

        saveString(AIRVPN_DO_NOT_SHOW_AGAIN_MESSAGES, bannerJsonArray.toString());
    }

    public void removeDoNotShowAgainBanners(long timeStamp)
    {
        ArrayList<Long> shownBanner = getDoNotShowAgainBanners();
        JSONArray bannerJsonArray = new JSONArray();

        for(int i = 0; i < shownBanner.size(); i++)
        {
            if(shownBanner.get(i) == timeStamp)
                shownBanner.remove(i);
        }

        for(Long ts : shownBanner)
            bannerJsonArray.put(ts);

        saveString(AIRVPN_DO_NOT_SHOW_AGAIN_MESSAGES, bannerJsonArray.toString());
    }

    public String dump()
    {
        String varDump = "";
        AirVPNUser airVPNUser = EddieApplication.airVPNUser();

        if(airVPNUser != null)
        {
            varDump += String.format("AirVPN logged in user: %s" + System.getProperty("line.separator"), airVPNUser.getUserName());
            varDump += String.format("AirVPN selected user key: %s" + System.getProperty("line.separator"), airVPNUser.getCurrentKey());
        }

        varDump += String.format("%s: %s" + System.getProperty("line.separator"), AIRVPN_DEFAULT_VPN_TYPE, getString(AIRVPN_DEFAULT_VPN_TYPE, AIRVPN_VPN_TYPE_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), AIRVPN_OPENVPN_MODE, getString(AIRVPN_OPENVPN_MODE, AIRVPN_OPENVPN_MODE_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), AIRVPN_DEFAULT_OPENVPN_PROTOCOL, getString(AIRVPN_DEFAULT_OPENVPN_PROTOCOL, AIRVPN_OPENVPN_PROTOCOL_DEFAULT));
        varDump += String.format(Locale.getDefault(),"%s: %d" + System.getProperty("line.separator"), AIRVPN_DEFAULT_OPENVPN_PORT, getInt(AIRVPN_DEFAULT_OPENVPN_PORT, AIRVPN_OPENVPN_PORT_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), AIRVPN_WIREGUARD_MODE, getString(AIRVPN_WIREGUARD_MODE, AIRVPN_WIREGUARD_MODE_DEFAULT));
        varDump += String.format(Locale.getDefault(),"%s: %d" + System.getProperty("line.separator"), AIRVPN_DEFAULT_WIREGUARD_PORT, getInt(AIRVPN_DEFAULT_WIREGUARD_PORT, AIRVPN_WIREGUARD_PORT_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), AIRVPN_DEFAULT_IP_VERSION, getString(AIRVPN_DEFAULT_IP_VERSION, AIRVPN_IP_VERSION_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), AIRVPN_DEFAULT_TLS_MODE, getString(AIRVPN_DEFAULT_TLS_MODE, AIRVPN_TLS_MODE_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), AIRVPN_QUICK_CONNECT_MODE, getString(AIRVPN_QUICK_CONNECT_MODE, AIRVPN_QUICK_CONNECT_MODE_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), AIRVPN_OPENVPN_CIPHER, getString(AIRVPN_OPENVPN_CIPHER, AIRVPN_OPENVPN_CIPHER_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), AIRVPN_FORBID_QUICK_CONNECTION_TO_USER_COUNTRY, getBoolean(AIRVPN_FORBID_QUICK_CONNECTION_TO_USER_COUNTRY, AIRVPN_FORBID_QUICK_CONNECTION_TO_USER_COUNTRY_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), AIRVPN_CUSTOM_BOOTSTRAP_SERVERS, getString(AIRVPN_CUSTOM_BOOTSTRAP_SERVERS, AIRVPN_CUSTOM_BOOTSTRAP_SERVERS_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), AIRVPN_SERVER_WHITELIST, getString(AIRVPN_SERVER_WHITELIST, AIRVPN_SERVER_WHITELIST_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), AIRVPN_SERVER_BLACKLIST, getString(AIRVPN_SERVER_BLACKLIST, AIRVPN_SERVER_BLACKLIST_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), AIRVPN_COUNTRY_WHITELIST, getString(AIRVPN_COUNTRY_WHITELIST, AIRVPN_COUNTRY_WHITELIST_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), AIRVPN_COUNTRY_BLACKLIST, getString(AIRVPN_COUNTRY_BLACKLIST, AIRVPN_COUNTRY_BLACKLIST_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), STARTUP_AIRVPN_SERVER, getString(STARTUP_AIRVPN_SERVER, STARTUP_AIRVPN_SERVER_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), STARTUP_AIRVPN_COUNTRY, getString(STARTUP_AIRVPN_COUNTRY, STARTUP_AIRVPN_COUNTRY_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), AIRVPN_CURRENT_LOCAL_COUNTRY, getString(AIRVPN_CURRENT_LOCAL_COUNTRY, AIRVPN_CURRENT_LOCAL_COUNTRY_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), AIRVPN_CONFIRM_SERVER_CONNECTION, getBoolean(AIRVPN_CONFIRM_SERVER_CONNECTION, AIRVPN_CONFIRM_SERVER_CONNECTION_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), SYSTEM_OPTION_ENABLE_MASTER_PASSWORD, getBoolean(SYSTEM_OPTION_ENABLE_MASTER_PASSWORD, SYSTEM_OPTION_ENABLE_MASTER_PASSWORD_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), SYSTEM_OPTION_VPN_LOCK, getBoolean(SYSTEM_OPTION_VPN_LOCK, SYSTEM_OPTION_VPN_LOCK_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), SYSTEM_OPTION_VPN_BOOT_PRIORITY, getString(SYSTEM_OPTION_VPN_BOOT_PRIORITY, SYSTEM_OPTION_VPN_BOOT_PRIORITY_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), SYSTEM_OPTION_VPN_RECONNECT, getBoolean(SYSTEM_OPTION_VPN_RECONNECT, SYSTEM_OPTION_VPN_RECONNECT_DEFAULT));
        varDump += String.format("%s: %d" + System.getProperty("line.separator"), SYSTEM_OPTION_VPN_RECONNECTION_RETRIES, getInt(SYSTEM_OPTION_VPN_RECONNECTION_RETRIES, SYSTEM_OPTION_VPN_RECONNECTION_RETRIES_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), SYSTEM_OPTION_DNS_OVERRIDE_ENABLE, getBoolean(SYSTEM_OPTION_DNS_OVERRIDE_ENABLE, SYSTEM_OPTION_DNS_OVERRIDE_ENABLE_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), SYSTEM_OPTION_DNS_CUSTOM, getString(SYSTEM_OPTION_DNS_CUSTOM, SYSTEM_OPTION_DNS_CUSTOM_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), SYSTEM_OPTION_PROXY_ENABLE, getBoolean(SYSTEM_OPTION_PROXY_ENABLE, SYSTEM_OPTION_PROXY_ENABLE_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), SYSTEM_OPTION_NOTIFICATION_SOUND, getBoolean(SYSTEM_OPTION_NOTIFICATION_SOUND, SYSTEM_OPTION_NOTIFICATION_SOUND_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), SYSTEM_OPTION_SHOW_MESSAGE_DIALOGS, getBoolean(SYSTEM_OPTION_SHOW_MESSAGE_DIALOGS, SYSTEM_OPTION_SHOW_MESSAGE_DIALOGS_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), SYSTEM_CUSTOM_MTU, getString(SYSTEM_CUSTOM_MTU, SYSTEM_CUSTOM_MTU_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), SYSTEM_OPTION_APPLICATION_FILTER_TYPE, getString(SYSTEM_OPTION_APPLICATION_FILTER_TYPE, SYSTEM_OPTION_APPLICATION_FILTER_TYPE_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), SYSTEM_OPTION_APPLICATION_FILTER, getString(SYSTEM_OPTION_APPLICATION_FILTER, SYSTEM_OPTION_APPLICATION_FILTER_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), SYSTEM_OPTION_APPLICATION_LANGUAGE, getString(SYSTEM_OPTION_APPLICATION_LANGUAGE, SYSTEM_OPTION_APPLICATION_LANGUAGE_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), SYSTEM_OPTION_APPLICATION_THEME, getString(SYSTEM_OPTION_APPLICATION_THEME, SYSTEM_OPTION_APPLICATION_THEME_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), SYSTEM_OPTION_GPS_SPOOFING, getBoolean(SYSTEM_OPTION_GPS_SPOOFING, SYSTEM_OPTION_GPS_SPOOFING_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), SYSTEM_OPTION_GPS_SPOOFING_INTERVAL, getInt(SYSTEM_OPTION_GPS_SPOOFING_INTERVAL, SYSTEM_OPTION_GPS_SPOOFING_INTERVAL_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), SYSTEM_OPTION_FIRST_RUN, getBoolean(SYSTEM_OPTION_FIRST_RUN, SYSTEM_OPTION_FIRST_RUN_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), SYSTEM_OPTION_START_VPN_AT_STARTUP, getBoolean(SYSTEM_OPTION_START_VPN_AT_STARTUP, SYSTEM_OPTION_START_VPN_AT_STARTUP_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), SYSTEM_OPTION_LAST_PROFILE_IS_CONNECTED, getBoolean(SYSTEM_OPTION_LAST_PROFILE_IS_CONNECTED, SYSTEM_OPTION_LAST_PROFILE_IS_CONNECTED_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), SYSTEM_IS_ALWAYS_ON_VPN, getString(SYSTEM_IS_ALWAYS_ON_VPN, SYSTEM_IS_ALWAYS_ON_VPN_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), SYSTEM_VPN_LOCKOWN, getString(SYSTEM_VPN_LOCKOWN, SYSTEM_VPN_LOCKOWN_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), SYSTEM_OPTION_EXCLUDE_LOCAL_NETWORKS, getBoolean(SYSTEM_OPTION_EXCLUDE_LOCAL_NETWORKS, SYSTEM_OPTION_EXCLUDE_LOCAL_NETWORKS_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), SYSTEM_OPTION_PAUSE_VPN_WHEN_SCREEN_IS_OFF, getBoolean(SYSTEM_OPTION_PAUSE_VPN_WHEN_SCREEN_IS_OFF, SYSTEM_OPTION_PAUSE_VPN_WHEN_SCREEN_IS_OFF_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), SYSTEM_AIRVPN_REMEMBER_ME, getBoolean(SYSTEM_AIRVPN_REMEMBER_ME, SYSTEM_AIRVPN_REMEMBER_ME_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), SYSTEM_AIRVPN_AUTOLOGIN, getBoolean(SYSTEM_AIRVPN_AUTOLOGIN, SYSTEM_AIRVPN_AUTOLOGIN_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), OVPN3_OPTION_TLS_MIN_VERSION, getString(OVPN3_OPTION_TLS_MIN_VERSION, OVPN3_OPTION_TLS_MIN_VERSION_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), OVPN3_OPTION_PROTOCOL, getString(OVPN3_OPTION_PROTOCOL, OVPN3_OPTION_PROTOCOL_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), OVPN3_OPTION_ALLOWUAF, getString(OVPN3_OPTION_ALLOWUAF, OVPN3_OPTION_ALLOWUAF_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), OVPN3_OPTION_TIMEOUT, getString(OVPN3_OPTION_TIMEOUT, OVPN3_OPTION_TIMEOUT_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), OVPN3_OPTION_TUN_PERSIST, getBoolean(OVPN3_OPTION_TUN_PERSIST, OVPN3_OPTION_TUN_PERSIST_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), OVPN3_OPTION_COMPRESSION_MODE, getString(OVPN3_OPTION_COMPRESSION_MODE, OVPN3_OPTION_COMPRESSION_MODE_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), OVPN3_OPTION_SYNCHRONOUS_DNS_LOOKUP, getBoolean(OVPN3_OPTION_SYNCHRONOUS_DNS_LOOKUP, OVPN3_OPTION_SYNCHRONOUS_DNS_LOOKUP_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), OVPN3_OPTION_AUTOLOGIN_SESSIONS, getBoolean(OVPN3_OPTION_AUTOLOGIN_SESSIONS, OVPN3_OPTION_AUTOLOGIN_SESSIONS_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), OVPN3_OPTION_DISABLE_CLIENT_CERT, getBoolean(OVPN3_OPTION_DISABLE_CLIENT_CERT, OVPN3_OPTION_DISABLE_CLIENT_CERT_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), OVPN3_OPTION_SSL_DEBUG_LEVEL, getString(OVPN3_OPTION_SSL_DEBUG_LEVEL, OVPN3_OPTION_SSL_DEBUG_LEVEL_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), OVPN3_OPTION_DEFAULT_KEY_DIRECTION, getString(OVPN3_OPTION_DEFAULT_KEY_DIRECTION, OVPN3_OPTION_DEFAULT_KEY_DIRECTION_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), OVPN3_OPTION_TLS_CERT_PROFILE, getString(OVPN3_OPTION_TLS_CERT_PROFILE, OVPN3_OPTION_TLS_CERT_PROFILE_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), OVPN3_OPTION_PROXY_HOST, getString(OVPN3_OPTION_PROXY_HOST, OVPN3_OPTION_PROXY_HOST_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), OVPN3_OPTION_PROXY_PORT, getString(OVPN3_OPTION_PROXY_PORT, OVPN3_OPTION_PROXY_PORT_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), OVPN3_OPTION_PROXY_ALLOW_CLEARTEXT_AUTH, getBoolean(OVPN3_OPTION_PROXY_ALLOW_CLEARTEXT_AUTH, OVPN3_OPTION_PROXY_ALLOW_CLEARTEXT_AUTH_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), OVPN3_OPTION_CUSTOM_DIRECTIVES, getString(OVPN3_OPTION_CUSTOM_DIRECTIVES, OVPN3_OPTION_CUSTOM_DIRECTIVES_DEFAULT));
        varDump += String.format("%s: %s" + System.getProperty("line.separator"), WIREGUARD_IGNORE_HANDSHAKING_TIMEOUT, getBoolean(WIREGUARD_IGNORE_HANDSHAKING_TIMEOUT, WIREGUARD_IGNORE_HANDSHAKING_TIMEOUT_DEFAULT));

        return varDump;
    }

    public String getString(String key, String defValue)
    {
        return appPrefs.getString(key, defValue).trim();
    }

    public void saveString(String key, String value)
    {
        prefsEditor.putString(key, value);

        prefsEditor.apply();
    }

    public int getInt(String key, int defValue)
    {
        int returnValue = 0;
        String value = "";

        value = appPrefs.getString(key, "");

        if(value.equals(""))
            returnValue = defValue;
        else
        {
            try
            {
                returnValue = Integer.parseInt(value);
            }
            catch(NumberFormatException e)
            {
                returnValue = 0;
            }
        }

        return returnValue;
    }

    public void saveInt(String key, int value)
    {
        prefsEditor.putString(key, String.format(Locale.getDefault(), "%d", value));

        prefsEditor.apply();
    }

    public long getLong(String key, long defValue)
    {
        long returnValue = 0;
        String value = "";

        value = appPrefs.getString(key, "");

        if(value.equals(""))
            returnValue = defValue;
        else
        {
            try
            {
                returnValue = Long.parseLong(value);
            }
            catch(NumberFormatException e)
            {
                returnValue = 0;
            }
        }

        return returnValue;
    }

    public void saveLong(String key, long value)
    {
        prefsEditor.putString(key, String.format(Locale.getDefault(), "%d", value));

        prefsEditor.apply();
    }

    public boolean getBoolean(String key, boolean defValue)
    {
        boolean returnValue = false;
        String value = "";

        value = appPrefs.getString(key, "");

        if(value.equals(""))
            returnValue = defValue;
        else if(value.equals("true"))
            returnValue = true;
        else
            returnValue = false;

        return returnValue;
    }

    public void saveBoolean(String key, boolean value)
    {
        String val = "";

        if(value)
            val = "true";
        else
            val = "false";

        prefsEditor.putString(key, val);

        prefsEditor.apply();
    }
}
