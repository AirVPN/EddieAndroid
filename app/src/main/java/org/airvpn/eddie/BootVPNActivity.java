// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 13 January 2019 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class BootVPNActivity extends AppCompatActivity implements NetworkStatusListener, EddieEventListener
{
    private static VPNManager vpnManager = null;
    private static AirVPNServerProvider airVPNServerProvider = null;
    private static SettingsManager settingsManager = null;
    private static SupportTools supportTools = null;
    private NetworkStatusReceiver networkStatusReceiver = null;
    private EddieEvent eddieEvent = null;
    private Intent tileServiceIntent = null;
    private static final int loopWaitMilliSeconds = 2000;
    private static final int userWaitLoops = 60; // 120 seconds
    private static boolean bootConnectionPending = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        initialize(this);

        networkStatusReceiver = EddieApplication.networkStatusReceiver();

        networkStatusReceiver.subscribeListener(this);

        eddieEvent = EddieApplication.eddieEvent();

        eddieEvent.subscribeListener(this);

        if(vpnManager.isVpnConnectionStopped() == true)
        {
            bootConnectionPending = true;

            tryBootConnection();
        }

        finish();
    }

    @Override
    public void onStart()
    {
        super.onStart();

        initialize(this);

        if(settingsManager.getSystemApplicationTheme().equals(SettingsManager.SYSTEM_OPTION_APPLICATION_THEME_SYSTEM) == false)
        {
            switch(settingsManager.getSystemApplicationTheme())
            {
                case "Dark":
                {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                }
                break;

                case "Light":
                {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                }
                break;

                default:
                {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
                }
                break;
            }
        }
    }

    @Override
    protected void onStop()
    {
        super.onStop();

        supportTools.setLocale(getBaseContext());
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        if(networkStatusReceiver != null)
            networkStatusReceiver.unsubscribeListener(this);

        if(eddieEvent != null)
            eddieEvent.unsubscribeListener(this);

        supportTools.setLocale(getBaseContext());
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        switch(newConfig.uiMode & Configuration.UI_MODE_NIGHT_MASK)
        {
            case Configuration.UI_MODE_NIGHT_YES:
            {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            }
            break;

            case Configuration.UI_MODE_NIGHT_NO:
            {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            }
            break;

            default:
            {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
            }
            break;
        }

        supportTools.setLocale(getBaseContext());

        recreate();

        super.onConfigurationChanged(newConfig);
    }

    private static void initialize(Context context)
    {
        if(settingsManager == null)
            settingsManager = EddieApplication.settingsManager();

        if(supportTools == null)
            supportTools = EddieApplication.supportTools();

        supportTools.setLocale(EddieApplication.baseContext());

        if(vpnManager == null)
            vpnManager = EddieApplication.vpnManager();
    }

    private void tryBootConnection()
    {
        if(bootConnectionPending == false)
            return;

        if(settingsManager.isStartVpnAtStartupEnabled() == false)
        {
            EddieLogger.info("Start VPN connection at device startup is disabled");

            return;
        }

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
        {
            tileServiceIntent = new Intent(getApplicationContext(), AirVPNTileService.class);

            startService(tileServiceIntent);
        }

        SupportTools.startThread(new Runnable()
        {
            @Override
            public void run()
            {
                boolean result;

                bootConnectionPending = false;

                result = startConnection(getApplicationContext());

                if(result == false)
                    EddieLogger.error("Cannot start VPN startup connection");
            }
        });
    }

    public static boolean startConnection(Context context)
    {
        ArrayList<SettingsManager.VPNBootOption> vpnBootOptionList = null;
        SettingsManager.VPNBootOption vpnBootOption = null;
        HashMap<String, String> profileInfo = null;
        String vpnProfile = "", vpnType = "", profileString = "";
        boolean validVpnBootOptionFound = false;
        int i;

        initialize(context);

        if(vpnManager.isVpnPermissionGranted() == false)
        {
            EddieLogger.error("BootVPNActivity.startConnection(): VPN permission not granted. Please start an interactive VPN connection and grant VPN permission to the app.");

            return false;
        }

        if(settingsManager.isStartVpnAtStartupEnabled() == false)
        {
            EddieLogger.info("BootVPNActivity.startConnection(): Start VPN connection at device startup is disabled");

            return false;
        }

        vpnManager.setContext(context);

        if(supportTools.waitForNetwork() == false)
        {
            EddieLogger.error("BootVPNActivity.startConnection(): Network timeout. Network connection is not available");

            return false;
        }

        if(supportTools.waitForManifest() == false)
        {
            EddieLogger.error("BootVPNActivity.startConnection(): AirVPN Manifest timeout. AirVPN Manifest is not available.");

            return false;
        }

        vpnBootOptionList = settingsManager.getVPNBootPriorityList();

        if(vpnBootOptionList == null || vpnBootOptionList.isEmpty())
        {
            EddieLogger.warning("BootVPNActivity.startConnection(): VPN boot priority list is empty");

            return false;
        }

        for(i = 0; i < vpnBootOptionList.size() && validVpnBootOptionFound == false; i++)
        {
            vpnBootOption = vpnBootOptionList.get(i);

            if(vpnBootOption == null)
            {
                EddieLogger.error("BootVPNActivity.startConnection(): VPN boot option is empty");

                return false;
            }

            if(supportTools.waitForNetwork() == false)
            {
                EddieLogger.error("BootVPNActivity.startConnection(): Network timeout. Network connection is not available");

                return false;
            }

            if(supportTools.waitForManifest() == false)
            {
                EddieLogger.error("BootVPNActivity.startConnection(): AirVPN Manifest timeout. AirVPN Manifest is not available.");

                return false;
            }

            EddieLogger.info("Starting VPN startup connection");

            vpnManager.reserveVpnConnection();

            switch(vpnBootOption.code)
            {
                case SettingsManager.VPN_BOOT_OPTION_AIRVPN_BEST_SERVER:
                case SettingsManager.VPN_BOOT_OPTION_AIRVPN_STARTUP_SERVER:
                case SettingsManager.VPN_BOOT_OPTION_AIRVPN_STARTUP_COUNTRY:
                {
                    AirVPNManifest airVPNManifest = EddieApplication.airVPNManifest();
                    AirVPNUser airVPNUser = EddieApplication.airVPNUser();
                    AirVPNServer airVPNServer = null;
                    AirVPNManifest.Mode manifestMode = null;
                    CountryContinent countryContinent = EddieApplication.countryContinent();
                    String bootServerName = "", serverIP = "", countryCode = "", allowedIPs = "";
                    String userKeyName = "", tlsMode = "", protocol = "";
                    int entry, port;
                    HashMap<Integer, String> serverEntryIP = null;
                    boolean connectIPv6 = false, mode6to4 = false, connectCountry = false;
                    ArrayList<String> list = new ArrayList<String>();
                    ArrayList<AirVPNServer> airVPNServerList = null;

                    airVPNServerProvider = new AirVPNServerProvider();

                    if(airVPNUser.areCredendialsUsable() == true)
                    {
                        EddieLogger.info("Trying to login to AirVPN");

                        for(i = 0; i < userWaitLoops && AirVPNUser.isUserValid() == false; i++)
                        {
                            try
                            {
                                Thread.sleep(loopWaitMilliSeconds);
                            }
                            catch (Exception e)
                            {
                            }
                        }

                        if(AirVPNUser.isUserValid() == true)
                        {
                            userKeyName = airVPNUser.getCurrentKey();

                            switch(settingsManager.getAirVPNDefaultVPNType())
                            {
                                case SettingsManager.VPN_TYPE_OPENVPN:
                                {
                                    if(settingsManager.getAirVPNOpenVPNMode().equals(SettingsManager.AIRVPN_OPENVPN_MODE_DEFAULT) == false)
                                    {
                                        manifestMode = EddieApplication.airVPNManifest().getMode(settingsManager.getAirVPNOpenVPNMode());

                                        entry = manifestMode.getEntryIndex();
                                        tlsMode = manifestMode.getTlsMode();
                                    }
                                    else
                                    {
                                        tlsMode = settingsManager.getAirVPNDefaultTLSMode();

                                        if(tlsMode.equals(SettingsManager.OPENVPN_TLS_MODE_AUTH))
                                            entry = 0;
                                        else
                                            entry = 2;
                                    }
                                }
                                break;

                                case SettingsManager.VPN_TYPE_WIREGUARD:
                                {
                                    if(settingsManager.getAirVPNWireGuardMode().equals(SettingsManager.AIRVPN_WIREGUARD_MODE_DEFAULT) == false)
                                    {
                                        manifestMode = EddieApplication.airVPNManifest().getMode(settingsManager.getAirVPNWireGuardMode());

                                        entry = manifestMode.getEntryIndex();

                                        tlsMode = manifestMode.getTlsMode();
                                    }
                                    else
                                    {
                                        entry = 0;

                                        tlsMode = SettingsManager.OPENVPN_TLS_MODE_AUTH;
                                    }
                                }
                                break;

                                default:
                                {
                                    entry = 0;

                                    tlsMode = SettingsManager.OPENVPN_TLS_MODE_AUTH;
                                }
                                break;
                            }

                            if(vpnBootOption.code.equals(SettingsManager.VPN_BOOT_OPTION_AIRVPN_BEST_SERVER) || vpnBootOption.code.equals(SettingsManager.VPN_BOOT_OPTION_AIRVPN_STARTUP_SERVER))
                            {
                                connectCountry = false;

                                countryCode = "";

                                if(vpnBootOption.code.equals(SettingsManager.VPN_BOOT_OPTION_AIRVPN_BEST_SERVER))
                                {
                                    airVPNServerProvider.reset();

                                    airVPNServerProvider.setUserCountry(airVPNUser.getUserCountry());

                                    if(settingsManager.getAirVPNQuickConnectMode().equals(SettingsManager.AIRVPN_QUICK_CONNECT_MODE_AUTO))
                                    {
                                        airVPNServerProvider.setTlsMode(AirVPNServerProvider.TLSMode.TLS_CRYPT);

                                        airVPNServerProvider.setSupportIPv4(true);
                                        airVPNServerProvider.setSupportIPv6(false);
                                    }
                                    else
                                    {
                                        if(settingsManager.getAirVPNDefaultTLSMode().equals(SettingsManager.OPENVPN_TLS_MODE_CRYPT))
                                            airVPNServerProvider.setTlsMode(AirVPNServerProvider.TLSMode.TLS_CRYPT);
                                        else
                                            airVPNServerProvider.setTlsMode(AirVPNServerProvider.TLSMode.TLS_AUTH);

                                        switch(settingsManager.getAirVPNDefaultIPVersion())
                                        {
                                            case SettingsManager.AIRVPN_IP_VERSION_4:
                                            {
                                                airVPNServerProvider.setSupportIPv4(true);
                                                airVPNServerProvider.setSupportIPv6(false);
                                            }
                                            break;

                                            case SettingsManager.AIRVPN_IP_VERSION_6:
                                            case SettingsManager.AIRVPN_IP_VERSION_4_OVER_6:
                                            {
                                                airVPNServerProvider.setSupportIPv4(true);
                                                airVPNServerProvider.setSupportIPv6(true);
                                            }
                                            break;

                                            default:
                                            {
                                                airVPNServerProvider.setSupportIPv4(true);
                                                airVPNServerProvider.setSupportIPv6(false);
                                            }
                                            break;
                                        }
                                    }

                                    airVPNServerList = airVPNServerProvider.getFilteredServerList();

                                    if(airVPNServerList != null && airVPNServerList.size() > 0)
                                        bootServerName = airVPNServerList.get(0).getName();
                                    else
                                        bootServerName = "";
                                }
                                else
                                    bootServerName = settingsManager.getStartupAirvpnServer();

                                airVPNServer = airVPNManifest.getServerByName(bootServerName);

                                if(airVPNServer != null)
                                {
                                    switch(settingsManager.getAirVPNDefaultIPVersion())
                                    {
                                        case SettingsManager.AIRVPN_IP_VERSION_4:
                                        {
                                            serverEntryIP = airVPNServer.getEntryIPv4();

                                            connectIPv6 = false;
                                            mode6to4 = false;
                                        }
                                        break;

                                        case SettingsManager.AIRVPN_IP_VERSION_6:
                                        {
                                            serverEntryIP = airVPNServer.getEntryIPv6();

                                            connectIPv6 = true;
                                            mode6to4 = false;
                                        }
                                        break;

                                        case SettingsManager.AIRVPN_IP_VERSION_4_OVER_6:
                                        {
                                            serverEntryIP = airVPNServer.getEntryIPv4();

                                            connectIPv6 = true;
                                            mode6to4 = true;
                                        }
                                        break;

                                        default:
                                        {
                                            serverEntryIP = airVPNServer.getEntryIPv4();

                                            connectIPv6 = false;
                                            mode6to4 = false;
                                        }
                                        break;
                                    }

                                    if(serverEntryIP == null)
                                    {
                                        EddieLogger.error("BootVPNActivity.startConnection(): serverEntryIP is null. 2");

                                        vpnManager.cancelVpnConnectionReservation();

                                        return false;
                                    }

                                    serverIP = serverEntryIP.get(entry);
                                }
                                else
                                {
                                    EddieLogger.error("BootVPNActivity.startConnection(): default AirVPN Server '%s' does not exist", bootServerName);

                                    validVpnBootOptionFound = false;
                                }
                            }
                            else
                            {
                                connectCountry = true;

                                airVPNServerProvider.reset();

                                airVPNServerProvider.setUserCountry(airVPNUser.getUserCountry());

                                countryCode = settingsManager.getStartupAirVPNCountry();

                                list.clear();

                                if(countryContinent.isContinent(countryCode) == false)
                                    list.add(countryCode);
                                else
                                {
                                    if(countryCode.length() != 3)
                                        countryCode = countryContinent.getContinentCode(countryCode);

                                    list.add(countryCode);
                                }

                                airVPNServerProvider.setCountryWhitelist(list);

                                airVPNServerProvider.setTlsMode(((tlsMode.equals(SettingsManager.OPENVPN_TLS_MODE_AUTH) == true) ? AirVPNServerProvider.TLSMode.TLS_AUTH : AirVPNServerProvider.TLSMode.TLS_CRYPT));

                                switch(settingsManager.getAirVPNDefaultIPVersion())
                                {
                                    case SettingsManager.AIRVPN_IP_VERSION_4:
                                    {
                                        airVPNServerProvider.setSupportIPv4(true);
                                        airVPNServerProvider.setSupportIPv6(false);

                                        connectIPv6 = false;
                                        mode6to4 = false;
                                    }
                                    break;

                                    case SettingsManager.AIRVPN_IP_VERSION_6:
                                    {
                                        airVPNServerProvider.setSupportIPv4(true);
                                        airVPNServerProvider.setSupportIPv6(true);

                                        connectIPv6 = true;
                                        mode6to4 = false;
                                    }
                                    break;

                                    case SettingsManager.AIRVPN_IP_VERSION_4_OVER_6:
                                    {
                                        airVPNServerProvider.setSupportIPv4(true);
                                        airVPNServerProvider.setSupportIPv6(true);

                                        connectIPv6 = true;
                                        mode6to4 = true;
                                    }
                                    break;

                                    default:
                                    {
                                        airVPNServerProvider.setSupportIPv4(true);
                                        airVPNServerProvider.setSupportIPv6(false);

                                        connectIPv6 = false;
                                        mode6to4 = false;
                                    }
                                    break;
                                }

                                airVPNServerList = airVPNServerProvider.getFilteredServerList();

                                if(airVPNServerList.size() > 0)
                                {
                                    if(settingsManager.getAirVPNDefaultIPVersion().equals(SettingsManager.AIRVPN_IP_VERSION_6) == true)
                                        serverEntryIP = airVPNServerList.get(0).getEntryIPv6();
                                    else
                                        serverEntryIP = airVPNServerList.get(0).getEntryIPv4();

                                    if(serverEntryIP == null)
                                    {
                                        EddieLogger.error("BootVPNActivity.startConnection(): serverEntryIP is null. 1");

                                        vpnManager.cancelVpnConnectionReservation();

                                        return false;
                                    }

                                    serverIP = serverEntryIP.get(entry);

                                    bootServerName = airVPNServerList.get(0).getName();
                                }
                                else
                                {
                                    EddieLogger.error("BootVPNActivity.startConnection(): no AirVPN Server found for country '%s'", countryCode);

                                    validVpnBootOptionFound = false;

                                    serverIP = "";

                                    bootServerName = "";
                                }
                            }

                            if(serverIP.isEmpty() == false)
                            {
                                vpnType = settingsManager.getAirVPNDefaultVPNType();

                                if(vpnType.equals(SettingsManager.VPN_TYPE_OPENVPN))
                                {
                                    if(settingsManager.getAirVPNOpenVPNMode().equals(SettingsManager.AIRVPN_OPENVPN_MODE_DEFAULT) == false)
                                    {
                                        manifestMode = EddieApplication.airVPNManifest().getMode(settingsManager.getAirVPNOpenVPNMode());

                                        port = manifestMode.getPort();
                                        protocol = manifestMode.getProtocol();
                                        tlsMode = manifestMode.getTlsMode();
                                    }
                                    else
                                    {
                                        port = settingsManager.getAirVPNDefaultOpenVPNPort();
                                        protocol = settingsManager.getAirVPNDefaultOpenVPNProtocol();
                                    }

                                    vpnProfile = airVPNUser.getOpenVPNProfile(userKeyName, serverIP, port, protocol, tlsMode, settingsManager.getAirVPNOpenVPNCipher(), connectIPv6, mode6to4, false, "");
                                }
                                else
                                {
                                    port = settingsManager.getAirVPNDefaultWireGuardPort();

                                    protocol = "UDP";

                                    allowedIPs = "";

                                    if(connectIPv6 == false || mode6to4 == true)
                                        allowedIPs = "0.0.0.0/0";

                                    if(connectIPv6 == true || mode6to4 == true)
                                    {
                                        if(allowedIPs.isEmpty() == false)
                                            allowedIPs += ", ";

                                        allowedIPs += "::/0";
                                    }

                                    vpnProfile = airVPNUser.getWireGuardProfile(userKeyName, serverIP, port, 0, 0, allowedIPs, 15, connectIPv6, mode6to4, false, "");
                                }

                                if(connectCountry == false)
                                {
                                    if(vpnBootOption.code.equals(SettingsManager.VPN_BOOT_OPTION_AIRVPN_BEST_SERVER))
                                        EddieLogger.info(String.format(Locale.getDefault(), "Trying to connect best AirVPN server %s at %s, %s - %s, Protocol %s, Port %d", airVPNServer.getName(), airVPNServer.getLocation(), countryContinent.getCountryName(airVPNServer.getCountryCode()), vpnType, protocol, port));
                                    else
                                        EddieLogger.info(String.format(Locale.getDefault(), "Trying to connect default AirVPN server %s at %s, %s - %s, Protocol %s, Port %d", airVPNServer.getName(), airVPNServer.getLocation(), countryContinent.getCountryName(airVPNServer.getCountryCode()), vpnType, protocol, port));
                                }
                                else
                                    EddieLogger.info(String.format(Locale.getDefault(), "Trying to connect best server in default AirVPN country %s - %s, Protocol %s, Port %d", countryContinent.getCountryName(countryCode), vpnType, protocol, port));

                                EddieLogger.info(String.format(Locale.getDefault(), "Using user key '%s'", userKeyName));

                                if(vpnProfile.isEmpty() == false)
                                {
                                    profileInfo = new HashMap<String, String>();

                                    profileInfo.put("mode", String.format(Locale.getDefault(), "%d", VPN.ConnectionMode.BOOT_CONNECT.getValue()));
                                    profileInfo.put("vpn_type", vpnType);
                                    profileInfo.put("name", "airvpn_server_connect");
                                    profileInfo.put("profile", "airvpn_server_connect");
                                    profileInfo.put("status", "ok");
                                    profileInfo.put("server", serverIP);

                                    if(connectCountry == false)
                                    {
                                        if(vpnBootOption.code.equals(SettingsManager.VPN_BOOT_OPTION_AIRVPN_BEST_SERVER))
                                            profileInfo.put("description", airVPNManifest.getFullServerDescription(bootServerName) + " (" + context.getResources().getString(R.string.airvpn_best_server_cap) + ")");
                                        else
                                            profileInfo.put("description", airVPNManifest.getFullServerDescription(bootServerName) + " (" + context.getResources().getString(R.string.airvpn_default_server_cap) + ")");
                                    }
                                    else
                                        profileInfo.put("description", airVPNManifest.getFullServerDescription(bootServerName) + " (" + context.getResources().getString(R.string.airvpn_default_country_cap) + ")");

                                    profileInfo.put("port", String.format(Locale.getDefault(), "%d", port));
                                    profileInfo.put("protocol", protocol);
                                    profileInfo.put("vpn_profile", vpnProfile);

                                    vpnManager.vpn().setConnectionMode(VPN.ConnectionMode.BOOT_CONNECT);

                                    vpnManager.vpn().setConnectionModeDescription(context.getResources().getString(R.string.conn_type_vpn_startup_connection));

                                    vpnManager.vpn().setUserName(airVPNUser.getUserName());

                                    vpnManager.vpn().setUserProfileDescription(userKeyName);

                                    validVpnBootOptionFound = true;
                                }
                                else
                                {
                                    EddieLogger.error("BootVPNActivity.startConnection(): vpnProfile for default AirVPN server or country is empty");

                                    validVpnBootOptionFound = false;
                                }
                            }
                            else
                            {
                                EddieLogger.error("BootVPNActivity.startConnection(): IP address for default AirVPN server or country is undefined");

                                validVpnBootOptionFound = false;
                            }
                        }
                        else
                        {
                            EddieLogger.error("BootVPNActivity.startConnection(): Internet connection timeout. Failed to login to AirVPN");

                            validVpnBootOptionFound = false;
                        }
                    }
                    else
                    {
                        EddieLogger.error("BootVPNActivity.startConnection(): AirVPN user credentials not usable or available. (Did you set the \"Remember me\" option on?) Failed to login to AirVPN.");

                        validVpnBootOptionFound = false;
                    }
                }
                break;

                case SettingsManager.VPN_BOOT_OPTION_DEFAULT_VPN_PROFILE:
                case SettingsManager.VPN_BOOT_OPTION_LAST_CONNECTED_PROFILE:
                {
                    String profileType = "";

                    if(vpnBootOption.code.equals(SettingsManager.VPN_BOOT_OPTION_DEFAULT_VPN_PROFILE))
                        profileType = SupportTools.BOOT_PROFILE_TYPE_DEFAULT_VPN_PROFILE;
                    else
                        profileType = SupportTools.BOOT_PROFILE_TYPE_LAST_ACTIVE_CONNECTION;

                    profileInfo = supportTools.getBootVPNProfile(profileType);

                    if(profileInfo != null)
                    {
                        if(profileInfo.get("type").equals(SupportTools.BOOT_PROFILE_TYPE_LAST_ACTIVE_CONNECTION))
                        {
                            EddieLogger.info("Previous VPN connection found to be started at boot time");

                            EddieLogger.info("Trying to start last active VPN connection (%s)", profileInfo.get("description"));

                            validVpnBootOptionFound = true;
                        }
                        else if(profileInfo.get("type").equals(SupportTools.BOOT_PROFILE_TYPE_DEFAULT_VPN_PROFILE))
                        {
                            EddieLogger.info("Trying to start connection using default VPN profile (%s)", profileInfo.get("name"));

                            validVpnBootOptionFound = true;
                        }
                        else
                        {
                            EddieLogger.info("No previously connected or VPN startup profile found");

                            validVpnBootOptionFound = false;
                        }

                        if(validVpnBootOptionFound == true)
                        {
                            vpnManager.vpn().setConnectionMode(VPN.ConnectionMode.BOOT_CONNECT);

                            vpnManager.vpn().setConnectionModeDescription(context.getResources().getString(R.string.conn_type_vpn_startup_connection));

                            vpnManager.vpn().setUserName("");

                            vpnManager.vpn().setUserProfileDescription("");
                        }
                    }
                    else
                    {
                        if(vpnBootOption.code.equals(SettingsManager.VPN_BOOT_OPTION_DEFAULT_VPN_PROFILE))
                            EddieLogger.info("No VPN startup profile set.");
                        else
                            EddieLogger.info("No VPN connection was active during the last run.");

                        validVpnBootOptionFound = false;
                    }
                }
                break;

                default:
                {
                    EddieLogger.error("Unknown VPN Boot option '" + vpnBootOption.code + "'");

                    vpnManager.cancelVpnConnectionReservation();

                    return false;
                }
            }

            if(validVpnBootOptionFound == true)
            {
                vpnManager.vpn().setType(VPN.Type.fromString(profileInfo.get("vpn_type")));

                vpnManager.vpn().setProfileInfo(profileInfo);

                if(profileInfo.get("vpn_profile").isEmpty() == false)
                {
                    vpnManager.clearProfile();

                    vpnManager.setProfile(profileInfo.get("vpn_profile"));

                    vpnManager.vpn().setVpnProfile(profileInfo.get("vpn_profile"));

                    if(vpnType.equals(SettingsManager.VPN_TYPE_OPENVPN) == true)
                    {
                        profileString = settingsManager.getOvpn3CustomDirectives().trim();

                        if(profileString.length() > 0)
                            vpnManager.addProfileString(profileString);
                    }

                    vpnManager.startConnection();
                }
                else
                {
                    EddieLogger.debug("VPNService.bootVPNActivity(): VPN startup profile is empty");

                    validVpnBootOptionFound = false;
                }
            }
        }

        return true;
    }

    // Eddie events

    public void onVpnConnectionStatsChanged(final VPNConnectionStats connectionData)
    {
    }

    public void onVpnStatusChanged(final VPN.Status status, final String message)
    {
    }

    public void onVpnAuthFailed(final VPNEvent oe)
    {
    }

    public void onVpnError(final VPNEvent oe)
    {
    }

    public void onVpnReconnect(final VPNEvent oe)
    {
    }

    public void onMasterPasswordChanged()
    {
    }

    public void onAirVPNLogin()
    {
    }

    public void onAirVPNLoginFailed(String message)
    {
    }

    public void onAirVPNLogout()
    {
    }

    public void onAirVPNUserDataChanged()
    {
    }

    public void onAirVPNUserProfileReceived(Document document)
    {
    }

    public void onAirVPNUserProfileDownloadError()
    {
    }

    public void onAirVPNUserProfileChanged()
    {
    }

    public void onAirVPNManifestReceived(Document document)
    {
        if(bootConnectionPending == true)
            tryBootConnection();
    }

    public void onAirVPNManifestDownloadError()
    {
    }

    public void onAirVPNManifestChanged()
    {
        if(bootConnectionPending == true)
            tryBootConnection();
    }

    public void onAirVPNIgnoredManifestDocumentRequest()
    {
    }

    public void onAirVPNIgnoredUserDocumentRequest()
    {
    }

    public void onAirVPNRequestError(String message)
    {
    }

    public void onCancelConnection()
    {
    }

    public void onSaveState()
    {
    }

    public void onRestoreState(Bundle bundle)
    {
    }

    // NetworkStatusReceiver

    public void onNetworkStatusNotAvailable()
    {
    }

    public void onNetworkStatusConnected()
    {
        if(bootConnectionPending == true)
            tryBootConnection();
    }

    public void onNetworkStatusIsConnecting()
    {
    }

    public void onNetworkStatusIsDisconnecting()
    {
    }

    public void onNetworkStatusSuspended()
    {
    }

    public void onNetworkStatusNotConnected()
    {
    }

    public void onNetworkTypeChanged()
    {
        if(bootConnectionPending == true)
            tryBootConnection();
    }
}
