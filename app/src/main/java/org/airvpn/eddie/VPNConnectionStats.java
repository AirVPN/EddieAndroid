// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 20 June 2018 - author: ProMIND - initial release. Based on revised code from com.eddie.android. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class VPNConnectionStats
{
	public int defined;
	public String user;
	public String serverHost;
	public String serverPort;
	public String serverProto;
	public String serverIp;
	public String vpnIp4;
	public String vpnIp6;
	public String gw4;
	public String gw6;
	public String clientIp;
	public String tunName;
	public String topology;
	public String cipher;
	public int ping;
	public int pingRestart;

	public VPNConnectionStats()
	{
		init();
	}

	public VPNConnectionStats(String s)
	{
		fromString(s);
	}

	private void init()
	{
		defined = -1;
		user = "";
		serverHost = "";
		serverPort = "";
		serverProto = "";
		serverIp = "";
		vpnIp4 = "";
		vpnIp6 = "";
		gw4 = "";
		gw6 = "";
		clientIp = "";
		tunName = "";
		topology = "";
		cipher = "";
		ping = -1;
		pingRestart = -1;
	}

	public void fromString(String s)
	{
		List<String> item = Arrays.asList(s.split(";"));

		if(item.size() != 16)
		{
			init();

			return;
		}

		try
		{
			defined = Integer.parseInt(item.get(0));
		}
		catch(Exception e)
		{
			defined = -1;
		}

		user = item.get(1);
		serverHost = item.get(2);
		serverPort = item.get(3);
		serverProto = item.get(4);
		serverIp = item.get(5);
		vpnIp4 = item.get(6);
		vpnIp6 = item.get(7);
		gw4 = item.get(8);
		gw6 = item.get(9);
		clientIp = item.get(10);
		tunName = item.get(11);
		topology = item.get(12);
		cipher = item.get(13);

		try
		{
			ping = Integer.parseInt(item.get(14));
		}
		catch(Exception e)
		{
			ping = -1;
		}

		try
		{
			pingRestart = Integer.parseInt(item.get(15));
		}
		catch(Exception e)
		{
			pingRestart = -1;
		}
	}

	public String toString()
	{
		return String.format(Locale.getDefault(), "%d;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%d;%d",
				defined,
				user,
				serverHost,
				serverPort,
				serverProto,
				serverIp,
				vpnIp4,
				vpnIp6,
				gw4,
				gw6,
				clientIp,
				tunName,
				topology,
				cipher,
				ping,
				pingRestart);
	}
}