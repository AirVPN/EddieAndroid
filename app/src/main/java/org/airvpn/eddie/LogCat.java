// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>

package org.airvpn.eddie;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class LogCat
{
    public LogCat()
    {
    }

    public static String dump()
    {
        return dump(-1);
    }

    public static String dump(int maxSize)
    {
        Process process = null;
        BufferedReader bufferedReader = null;
        StringBuilder logcatText = new StringBuilder();
        String line = "";

        try
        {
            process = new ProcessBuilder().command("logcat", "-d", "-b", "all", "-v", "threadtime", "*:V")
                    .start();

            bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            while((line = bufferedReader.readLine()) != null)
            {
                if(logcatText.length() > 0)
                    logcatText.append(System.getProperty("line.separator"));

                logcatText.append(line);
            }

            bufferedReader.close();
        }
        catch(Exception e)
        {
            EddieLogger.error("Failed to get logcat. %s", e.getMessage());
        }

        if(maxSize > -1 && logcatText.length() > maxSize)
            logcatText.delete(0, logcatText.length() - maxSize);

        return logcatText.toString();
    }
}
