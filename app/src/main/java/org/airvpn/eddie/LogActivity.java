// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 4 September 2018 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.res.ResourcesCompat;

import android.provider.Settings;
import android.text.Html;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;

public class LogActivity extends AppCompatActivity
{
    private Typeface typeface = null;
    private ArrayList<String> logEntry = null;
    private ListView listLogView = null;
    private TextView txtTitle = null;
    private WebView webLogView = null;
    private Button btnRealtime = null, btnSave = null, btnClear = null, btnShare = null, btnSend = null;
    private LinearLayout llProgressSpinner = null;
    private LogListAdapter logListAdapter = null;
    private ViewMode defaultViewMode = ViewMode.LISTVIEW;
    private ViewMode viewMode = defaultViewMode;
    private Timer logTimer = null;

    private Activity thisActivity = this;
    private SettingsManager settingsManager = null;
    private EddieLogger eddieLogger = null;
    private SupportTools supportTools = null;

    private static boolean isRealtimeEnabled = false;

    public static final int LOG_UPDATE_INTERVAL_SECONDS = 5;

    public enum ViewMode
    {
        DEFAULT,
        LISTVIEW,
        WEBVIEW
    }

    private enum FormatType
    {
        HTML,
        PLAIN_TEXT
    }

    private enum LogTime
    {
        UTC,
        LOCAL
    }

    private class LogListAdapter extends BaseAdapter
    {
        private Activity activity = null;
        private ArrayList<Spanned> logEntry = null;

        private class ListViewHolder
        {
            public TextView txtLogEntry;
        }

        public LogListAdapter(Activity a, ArrayList<Spanned> entryList)
        {
            activity = a;

            logEntry = entryList;
        }

        public void dataSet(ArrayList<Spanned> entryList)
        {
            logEntry = entryList;

            notifyDataSetChanged();
        }

        @Override
        public boolean isEnabled(int position)
        {
            return false;
        }

        @Override
        public int getCount()
        {
            int entries = 0;

            if(logEntry != null)
                entries = logEntry.size();

            return entries;
        }

        @TargetApi(24)
        @Override
        public Spanned getItem(int position)
        {
            Spanned item = null;

            if(logEntry != null && position < logEntry.size())
                item = logEntry.get(position);
            else
            {
                String txt = "<font color='red'>logEntry is invalid</font>";

                if(Build.VERSION.SDK_INT < Build.VERSION_CODES.N)
                    item = Html.fromHtml(txt);
                else
                    item = Html.fromHtml(txt, Html.FROM_HTML_MODE_LEGACY);
            }

            return item;
        }

        @Override
        public long getItemId(int position)
        {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            ListViewHolder listViewHolder = null;

            if(convertView != null)
                listViewHolder = (ListViewHolder)convertView.getTag();

            if(listViewHolder == null)
            {
                listViewHolder = new ListViewHolder();

                convertView = activity.getLayoutInflater().inflate(R.layout.log_activity_listitem, null);

                listViewHolder.txtLogEntry = (TextView)convertView.findViewById(R.id.log_entry);

                convertView.setTag(listViewHolder);
            }

            listViewHolder.txtLogEntry.setText(logEntry.get(position));
            listViewHolder.txtLogEntry.setTypeface(typeface);

            return convertView;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        settingsManager = EddieApplication.settingsManager();

        eddieLogger = EddieApplication.logger();

        supportTools = EddieApplication.supportTools();

        viewMode = (ViewMode)getIntent().getSerializableExtra("ViewMode");

        typeface = ResourcesCompat.getFont(this, R.font.default_font);

        supportTools.setLocale(getBaseContext());

        if(viewMode == ViewMode.DEFAULT)
            viewMode = defaultViewMode;

        if(viewMode == ViewMode.LISTVIEW)
        {
            setContentView(R.layout.log_activity_layout);

            listLogView = (ListView)findViewById(R.id.log);
        }
        else
        {
            setContentView(R.layout.log_activity_weblayout);

            webLogView = (WebView)findViewById(R.id.logwebview);
        }

        txtTitle = (TextView)findViewById(R.id.title);

        txtTitle.setTypeface(typeface);

        btnRealtime = (Button)findViewById(R.id.btn_realtime);

        btnRealtime.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                int msg = -1;

                isRealtimeEnabled = !isRealtimeEnabled;

                if(isRealtimeEnabled == true)
                    msg = R.string.realtime_log_enabled_cap;
                else
                    msg = R.string.realtime_log_disabled_cap;

                Toast.makeText(thisActivity, getResources().getString(msg), Toast.LENGTH_LONG).show();

                realtimeLog();
            }
        });

        btnRealtime.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                if(!btnRealtime.isEnabled())
                    return;

                if(hasFocus)
                {
                    if(isRealtimeEnabled == true)
                        btnRealtime.setBackgroundResource(R.drawable.realtime_on_focus);
                    else
                        btnRealtime.setBackgroundResource(R.drawable.realtime_off_focus);
                }
                else
                {
                    if(isRealtimeEnabled == true)
                        btnRealtime.setBackgroundResource(R.drawable.realtime_on);
                    else
                        btnRealtime.setBackgroundResource(R.drawable.realtime_off);
                }
            }
        });

        btnRealtime.setAccessibilityDelegate(new View.AccessibilityDelegate()
        {
            @Override
            public void sendAccessibilityEvent(View host, int eventType)
            {
                super.sendAccessibilityEvent(host, eventType);

                btnClear.setContentDescription(getString(R.string.accessibility_realtime_log));
            }
        });

        btnSave = (Button)findViewById(R.id.btn_save);

        btnSave.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                saveLog();
            }
        });

        btnSave.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                if(!btnSave.isEnabled())
                    return;

                if(hasFocus)
                    btnSave.setBackgroundResource(R.drawable.save_focus);
                else
                    btnSave.setBackgroundResource(R.drawable.save);
            }
        });

        btnSave.setAccessibilityDelegate(new View.AccessibilityDelegate()
        {
            @Override
            public void sendAccessibilityEvent(View host, int eventType)
            {
                super.sendAccessibilityEvent(host, eventType);

                btnClear.setContentDescription(getString(R.string.accessibility_save_log));
            }
        });

        btnClear = (Button)findViewById(R.id.btn_clear);

        btnClear.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                clearLog();
            }
        });

        btnClear.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                if(!btnClear.isEnabled())
                    return;

                if(hasFocus)
                    btnClear.setBackgroundResource(R.drawable.delete_focus);
                else
                    btnClear.setBackgroundResource(R.drawable.delete);
            }
        });

        btnClear.setAccessibilityDelegate(new View.AccessibilityDelegate()
        {
            @Override
            public void sendAccessibilityEvent(View host, int eventType)
            {
                super.sendAccessibilityEvent(host, eventType);

                btnClear.setContentDescription(getString(R.string.accessibility_clear_log));
            }
        });

        btnShare = (Button)findViewById(R.id.btn_share);

        btnShare.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                shareLog();
            }
        });

        btnShare.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                if(!btnShare.isEnabled())
                    return;

                if(hasFocus)
                    btnShare.setBackgroundResource(R.drawable.share_focus);
                else
                    btnShare.setBackgroundResource(R.drawable.share);
            }
        });

        btnShare.setAccessibilityDelegate(new View.AccessibilityDelegate()
        {
            @Override
            public void sendAccessibilityEvent(View host, int eventType)
            {
                super.sendAccessibilityEvent(host, eventType);

                btnShare.setContentDescription(getString(R.string.accessibility_share_log));
            }
        });

        btnSend = (Button)findViewById(R.id.btn_send);

        btnSend.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                sendLog();
            }
        });

        btnSend.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                if(!btnSend.isEnabled())
                    return;

                if(hasFocus)
                    btnSend.setBackgroundResource(R.drawable.send_focus);
                else
                    btnSend.setBackgroundResource(R.drawable.send);
            }
        });

        btnSend.setAccessibilityDelegate(new View.AccessibilityDelegate()
        {
            @Override
            public void sendAccessibilityEvent(View host, int eventType)
            {
                super.sendAccessibilityEvent(host, eventType);

                btnClear.setContentDescription(getString(R.string.accessibility_send_log));
            }
        });

        llProgressSpinner = (LinearLayout)findViewById(R.id.llProgressSpinner);

        OnBackPressedCallback onBackPressedCallback = new OnBackPressedCallback(true)
        {
            @Override
            public void handleOnBackPressed()
            {
                supportTools.setLocale(getBaseContext());

                thisActivity.finish();
            }
        };

        getOnBackPressedDispatcher().addCallback(this, onBackPressedCallback);
    }

    @Override
    public void onStart()
    {
        super.onStart();

        if(settingsManager.getSystemApplicationTheme().equals(SettingsManager.SYSTEM_OPTION_APPLICATION_THEME_SYSTEM) == false)
        {
            switch(settingsManager.getSystemApplicationTheme())
            {
                case "Dark":
                {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                }
                break;

                case "Light":
                {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                }
                break;

                default:
                {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
                }
                break;
            }
        }

        new loadLogView().execute();
    }

    @Override
    public void onStop()
    {
        super.onStop();

        if(logTimer != null)
            logTimer.cancel();

        logTimer = null;

        supportTools.setLocale(getBaseContext());
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        supportTools.setLocale(getBaseContext());
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        realtimeLog();

        supportTools.removeShareFile();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        switch(newConfig.uiMode & Configuration.UI_MODE_NIGHT_MASK)
        {
            case Configuration.UI_MODE_NIGHT_YES:
            {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            }
            break;

            case Configuration.UI_MODE_NIGHT_NO:
            {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            }
            break;

            default:
            {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
            }
            break;
        }

        supportTools.setLocale(getBaseContext());

        new loadLogView().execute();

        recreate();

        super.onConfigurationChanged(newConfig);
    }

    private void realtimeLog()
    {
        int updateInterval = LOG_UPDATE_INTERVAL_SECONDS * 1000;

        if(btnRealtime.hasFocus() == true)
        {
            if(isRealtimeEnabled == true)
                btnRealtime.setBackgroundResource(R.drawable.realtime_on_focus);
            else
                btnRealtime.setBackgroundResource(R.drawable.realtime_off_focus);
        }
        else
        {
            if(isRealtimeEnabled == true)
                btnRealtime.setBackgroundResource(R.drawable.realtime_on);
            else
                btnRealtime.setBackgroundResource(R.drawable.realtime_off);
        }

        if(logTimer != null)
            logTimer.cancel();

        if(isRealtimeEnabled == true)
        {
            logTimer = new Timer();

            logTimer.schedule(new TimerTask()
            {
                @Override
                public void run()
                {
                    final Runnable uiRunnable = new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            new loadLogView().execute();
                        }
                    };

                    SupportTools.runOnUiActivity(thisActivity, uiRunnable);
                }
            }, updateInterval, updateInterval);
        }
        else
            logTimer = null;
    }

    private ArrayList<String> getCurrentLog(FormatType formatType, LogTime logTime)
    {
        SimpleDateFormat dateFormatter = null;
        Date logCurrenTimeZone = null;
        Calendar calendar = null;
        ArrayList<String> logItem = null;
        ArrayList<EddieLogger.LogItem> eddieLog = null;
        long utcTimeStamp = 0;
        String log = "", logColor = "", levelDescription = "";
        boolean nightMode = false;

        nightMode = supportTools.isNightModeOn();

        calendar = Calendar.getInstance();

        dateFormatter = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");

        if(logTime == LogTime.UTC)
            dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        else
            dateFormatter.setTimeZone(TimeZone.getDefault());

        eddieLog = eddieLogger.getLog();

        if(eddieLog == null)
            return null;

        logItem = new ArrayList<String>();

        for(EddieLogger.LogItem item : eddieLog)
        {
            utcTimeStamp = item.utcUnixTimestamp;

            calendar.setTimeInMillis(utcTimeStamp * 1000);
            logCurrenTimeZone = (Date)calendar.getTime();

            if(item.logLevel == Level.INFO)
            {
                if(nightMode == false)
                    logColor = supportTools.hexColorCode(R.color.logInfo);
                else
                    logColor = supportTools.hexColorCode(R.color.logInfoNight);

                levelDescription = "Info";
            }
            else if(item.logLevel == Level.WARNING)
            {
                if(nightMode == false)
                    logColor = supportTools.hexColorCode(R.color.logWarning);
                else
                    logColor = supportTools.hexColorCode(R.color.logWarningNight);

                levelDescription = "Warning";
            }
            else if(item.logLevel == Level.FINE)
            {
                if(nightMode == false)
                    logColor = supportTools.hexColorCode(R.color.logDebug);
                else
                    logColor = supportTools.hexColorCode(R.color.logDebugNight);

                levelDescription = "Debug";
            }
            else if(item.logLevel == Level.SEVERE)
            {
                if(nightMode == false)
                    logColor = supportTools.hexColorCode(R.color.logError);
                else
                    logColor = supportTools.hexColorCode(R.color.logErrorNight);

                levelDescription = "Error";
            }
            else
            {
                if(nightMode == false)
                    logColor = supportTools.hexColorCode(R.color.logDefault);
                else
                    logColor = supportTools.hexColorCode(R.color.logDefaultNight);

                levelDescription = item.logLevel.toString();
            }

            switch(formatType)
            {
                case HTML:
                {
                    log = "<font color='" + (nightMode == false ? supportTools.hexColorCode(R.color.logTitleColor) : supportTools.hexColorCode(R.color.logTitleColorNight));

                    log += "'>" + dateFormatter.format(logCurrenTimeZone) + "</font>";
                    log += " [<font color='" + logColor + "'>" + levelDescription + "</font>]: ";
                    log += "<font color='" + (nightMode == false ? supportTools.hexColorCode(R.color.logTextColor) : supportTools.hexColorCode(R.color.logTextColorNight)) + "'>";
                    log += item.message.replace("\n", "<br>");
                    log += "</font>";
                }
                break;

                case PLAIN_TEXT:
                {
                    log = dateFormatter.format(logCurrenTimeZone) + " UTC [" + levelDescription +"] " + item.message;
                }
                break;

                default:
                {
                    log = "";
                }
                break;
            }

            logItem.add(log);
        }

        return logItem;
    }

    private ArrayList<Spanned> getSpannedLog()
    {
        ArrayList<Spanned> spannedList = new ArrayList<Spanned>();

        if(logEntry != null && logEntry.size() > 0)
        {
            Spanned logSpanned = null;

            for(String item : logEntry)
            {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N)
                    logSpanned = Html.fromHtml(item);
                else
                    logSpanned = Html.fromHtml(item, Html.FROM_HTML_MODE_LEGACY);

                spannedList.add(logSpanned);
            }
        }
        else
        {
            Spanned logSpanned = null;
            String logHtml;

            logHtml = "<p><bold><big><big>";
            logHtml += "<font color='" + (supportTools.isNightModeOn() == false ? supportTools.hexColorCode(R.color.logTextColor) : supportTools.hexColorCode(R.color.logTextColorNight)) + "'>";
            logHtml += "--- " + getResources().getString(R.string.log_is_empty) + " ---</font></big></big></bold></p>";

            logSpanned = Html.fromHtml(logHtml);

            spannedList.add(logSpanned);
        }

        return spannedList;
    }

    private void loadLogWebView()
    {
        String logHtml = "<html><body bgcolor=\"" + (supportTools.isNightModeOn() == false ? supportTools.hexColorCode(R.color.logBackgroundColor) : supportTools.hexColorCode(R.color.logBackgroundColorNight)) + "\">" + System.getProperty("line.separator");

        if(logEntry != null && logEntry.size() > 0)
        {
            for(String entry : logEntry)
                logHtml += "<p>" + entry + "</p>";

            if(logHtml.isEmpty())
            {
                logHtml = "<p><bold><big><big>";
                logHtml += "<font color='" + (supportTools.isNightModeOn() == false ? supportTools.hexColorCode(R.color.logTextColor) : supportTools.hexColorCode(R.color.logTextColorNight)) + "'>";
                logHtml += "--- " + getResources().getString(R.string.log_is_empty) + " ---</font></big></big></bold></p>";
            }
        }
        else
        {
            logHtml += "<p><bold><big><big>";
            logHtml += "<font color='" + (supportTools.isNightModeOn() == false ? supportTools.hexColorCode(R.color.logTextColor) : supportTools.hexColorCode(R.color.logTextColorNight)) + "'>";
            logHtml += "--- " + getResources().getString(R.string.log_is_empty) + " ---</font></big></big></bold></p>";
        }

        logHtml += "</body></html>";

        webLogView.loadData(logHtml, "text/html", null);
    }

    private class loadLogView extends AsyncTask<Void, Void, Void>
    {
        @Override
        protected void onPreExecute()
        {
            llProgressSpinner.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... p)
        {
            logEntry = getCurrentLog(FormatType.HTML, LogTime.LOCAL);

            return null;
        }

        @Override
        protected void onPostExecute(Void param)
        {
            if(viewMode == ViewMode.DEFAULT)
                viewMode = defaultViewMode;

            if(viewMode == ViewMode.LISTVIEW)
            {
                logListAdapter = new LogListAdapter(LogActivity.this, getSpannedLog());

                listLogView.setAdapter(logListAdapter);
            }
            else
            {
                webLogView.getSettings().setJavaScriptEnabled(false);
                webLogView.getSettings().setBuiltInZoomControls(true);
                webLogView.getSettings().setDisplayZoomControls(false);

                webLogView.setWebViewClient(new WebViewClient()
                    {
                        @Override
                        public void onPageFinished(WebView view, String url)
                        {
                            view.scrollTo(0, view.getContentHeight());
                        }
                    });

                loadLogWebView();
            }

            llProgressSpinner.setVisibility(View.GONE);
        }
    }

    private void saveLog()
    {
        HashMap<String, String> appLog = null;
        File saveFile = null;
        FileOutputStream saveFileStream = null;
        String fileName = "";

        llProgressSpinner.setVisibility(View.VISIBLE);

        appLog = createExportLog(true, true, true, true, true);

        fileName = appLog.get("subject");
        fileName = fileName.replace(":", "_");
        fileName = fileName.replace(" ", "_");

        saveFile = new File(getExternalFilesDir(null), fileName);

        try
        {
            saveFileStream = new FileOutputStream(saveFile);

            saveFileStream.write(appLog.get("log").getBytes());

            saveFileStream.close();
        }
        catch(Exception e)
        {
            EddieLogger.error("Failed to save Eddie Log. %s", e.getMessage());
        }

        llProgressSpinner.setVisibility(View.GONE);

        supportTools.infoDialog(thisActivity, String.format(Locale.getDefault(), getResources().getString(R.string.log_successfully_saved), saveFile.toString()), false);
    }

    private void clearLog()
    {
        if(supportTools.confirmationDialog(thisActivity, getResources().getString(R.string.clear_log_confirmation)))
        {
            eddieLogger.clear();

            new loadLogView().execute();
        }
    }

    private void shareLog()
    {
        HashMap<String, String> appLog = null;

        llProgressSpinner.setVisibility(View.VISIBLE);

        appLog = createExportLog(true, true, true, true, true);

        llProgressSpinner.setVisibility(View.GONE);

        supportTools.sharePlainText(appLog.get("log"), appLog.get("subject"), R.string.share_with_cap);
    }

    private void sendLog()
    {
        if(supportTools.confirmationDialog(thisActivity, getResources().getString(R.string.send_log_confirmation)))
        {
            new SendReportToAirVPN().execute("https://eddie.website/report/new/");
        }
    }

    private HashMap<String, String> createExportLog(boolean systemReport, boolean settingsLog, boolean vpnStatusLog, boolean appLog, boolean logcat)
    {
        SimpleDateFormat dateFormatter = null;
        ArrayList<String> currentLog = null;
        HashMap<String, String> exportLog = null;
        String logText = "", logSubject = "";
        DisplayMetrics appDisplayMetrics = getResources().getDisplayMetrics();
        final int maxLogCatSize = 150 * 1024;

        dateFormatter = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");

        dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));

        logSubject = String.format(Locale.getDefault(), getResources().getString(R.string.log_subject), dateFormatter.format(new Date()));

        logText = logSubject + System.getProperty("line.separator") + System.getProperty("line.separator") + "Eddie for Android ";

        logText += String.format(Locale.getDefault(), "%s Version Code %s" + System.getProperty("line.separator"), BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE);

        if(systemReport == true)
        {
            logText += System.getProperty("line.separator") + System.getProperty("line.separator") + "--- App and device information ---" + System.getProperty("line.separator") + System.getProperty("line.separator");

            logText += String.format("Eddie Version Code: %s" + System.getProperty("line.separator"), BuildConfig.VERSION_CODE);
            logText += String.format("Eddie Version Name: %s" + System.getProperty("line.separator"), BuildConfig.VERSION_NAME);
            logText += String.format("%s - %s" + System.getProperty("line.separator"), EddieLibrary.qualifiedName(), EddieLibrary.releaseDate());
            logText += String.format("Eddie Library API level: %s" + System.getProperty("line.separator") + System.getProperty("line.separator"), EddieLibrary.apiLevel());
            logText += String.format("Architecture: %s" + System.getProperty("line.separator"), EddieLibrary.architecture());
            logText += String.format("Platform: %s" + System.getProperty("line.separator"), EddieLibrary.platform());
            logText += String.format("%s" + System.getProperty("line.separator"), EddieLibrary.openVPNInfo());
            logText += String.format("%s" + System.getProperty("line.separator"), EddieLibrary.openVPNCopyright());
            logText += String.format("SSL Library Version: %s" + System.getProperty("line.separator"), EddieLibrary.sslLibraryVersion());
            logText += String.format("WireGuard Version: %s" + System.getProperty("line.separator"), EddieLibrary.wireGuardVersion());

            logText += String.format(System.getProperty("line.separator") + "Manufacturer: %s" + System.getProperty("line.separator"), Build.MANUFACTURER);
            logText += String.format("Model: %s" + System.getProperty("line.separator"), Build.MODEL);
            logText += String.format("Device: %s" + System.getProperty("line.separator"), Build.DEVICE);
            logText += String.format("Board: %s" + System.getProperty("line.separator"), Build.BOARD);
            logText += String.format("Brand: %s" + System.getProperty("line.separator"), Build.BRAND);
            logText += String.format("Product: %s" + System.getProperty("line.separator"), Build.PRODUCT);
            logText += String.format("Hardware: %s" + System.getProperty("line.separator"), Build.HARDWARE);
            logText += String.format("Android TV: %s" + System.getProperty("line.separator"), (SupportTools.isTVDevice() == true) ? "Yes" : "No");
            logText += String.format("Rooted device: %s" + System.getProperty("line.separator"), SupportTools.isDeviceRooted());
            logText += String.format("Developer Options Enabled: %s" + System.getProperty("line.separator"), (Settings.Secure.getInt(this.getContentResolver(), Settings.Global.DEVELOPMENT_SETTINGS_ENABLED , 0) == 0) ? "No" : "Yes");

            logText += "Supported ABIs:";

            String[] supportedAbis = Build.SUPPORTED_ABIS;

            for(String abi : supportedAbis)
                logText += " " + abi;

            logText += String.format(System.getProperty("line.separator") + "Host: %s" + System.getProperty("line.separator"), Build.HOST);
            logText += String.format("ID: %s" + System.getProperty("line.separator"), Build.ID);
            logText += String.format("Display: %s" + System.getProperty("line.separator"), Build.DISPLAY);
            logText += String.format(Locale.getDefault(), "Android API Level: %d" + System.getProperty("line.separator"), Build.VERSION.SDK_INT);
            logText += String.format("Android Codename: %s" + System.getProperty("line.separator"), Build.VERSION.CODENAME);
            logText += String.format("Android Version Release: %s" + System.getProperty("line.separator"), Build.VERSION.RELEASE);
            logText += String.format(Locale.getDefault(), "Android Version Incremental: %s" + System.getProperty("line.separator"), Build.VERSION.INCREMENTAL);
            logText += String.format("Fingerprint: %s" + System.getProperty("line.separator"), Build.FINGERPRINT);
            logText += String.format("OS Name: %s" + System.getProperty("line.separator"), System.getProperty("os.name"));
            logText += String.format("OS Architecture: %s" + System.getProperty("line.separator"), System.getProperty("os.arch"));
            logText += String.format("OS Version: %s" + System.getProperty("line.separator"), System.getProperty("os.version"));
            logText += String.format("Type: %s" + System.getProperty("line.separator"), Build.TYPE);
            logText += String.format("Tags: %s" + System.getProperty("line.separator"), Build.TAGS);
            logText += String.format("User: %s" + System.getProperty("line.separator"), Build.USER);
            logText += String.format("Language: %s" + System.getProperty("line.separator"), Locale.getDefault().getLanguage());
            logText += String.format("Network: %s" + System.getProperty("line.separator"), NetworkStatusReceiver.getNetworkTypeName());

            logText += String.format(Locale.getDefault(), "Screen Width: %d" + System.getProperty("line.separator"), appDisplayMetrics.widthPixels);
            logText += String.format(Locale.getDefault(), "Screen Height: %d" + System.getProperty("line.separator"), appDisplayMetrics.heightPixels);
            logText += String.format(Locale.getDefault(), "Screen Density: %.0f" + System.getProperty("line.separator"), appDisplayMetrics.density);
            logText += String.format(Locale.getDefault(), "Screen Density DPI: %d" + System.getProperty("line.separator"), appDisplayMetrics.densityDpi);
            logText += String.format(Locale.getDefault(), "Screen XDPI: %.0f" + System.getProperty("line.separator"), appDisplayMetrics.xdpi);
            logText += String.format(Locale.getDefault(), "Screen YDPI: %.0f" + System.getProperty("line.separator"), appDisplayMetrics.ydpi);

            // System memory

            ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
            ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);

            try
            {
                activityManager.getMemoryInfo(memoryInfo);

                long totalMemory = memoryInfo.totalMem / 1048576L;
                long memory = memoryInfo.availMem / 1048576L;

                logText += String.format(Locale.getDefault(), "Total RAM: %d Mb" + System.getProperty("line.separator"), totalMemory);
                logText += String.format(Locale.getDefault(), "Free RAM: %d Mb" + System.getProperty("line.separator"), memory);

                memory = memoryInfo.threshold / 1048576L;

                logText += String.format(Locale.getDefault(), "Threshold Memory: %d Mb" + System.getProperty("line.separator"), memory);
                logText += String.format("Low Memory: %s" + System.getProperty("line.separator"), memoryInfo.lowMemory);

                // App memory

                memory = Runtime.getRuntime().totalMemory() / 1048576L;

                logText += String.format(Locale.getDefault(), "App Total Memory: %d Mb" + System.getProperty("line.separator"), memory);

                memory = Runtime.getRuntime().maxMemory() / 1048576L;

                logText += String.format(Locale.getDefault(), "App Max Memory: %d Mb" + System.getProperty("line.separator"), memory);

                memory = Runtime.getRuntime().freeMemory() / 1048576L;

                logText += String.format(Locale.getDefault(), "App Free Memory: %d Mb" + System.getProperty("line.separator"), memory);
            }
            catch(NullPointerException e)
            {
                memoryInfo = null;
            }

            logText += String.format("Bootloader: %s" + System.getProperty("line.separator"), Build.BOOTLOADER);

            // Permissions

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            {
                logText += System.getProperty("line.separator") + System.getProperty("line.separator") + "--- App permissions ---" + System.getProperty("line.separator") + System.getProperty("line.separator");

                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
                    logText += String.format("Notification permission: %s" + System.getProperty("line.separator"), (EddieApplication.applicationContext().checkSelfPermission(android.Manifest.permission.POST_NOTIFICATIONS) == PackageManager.PERMISSION_GRANTED) ? "Granted" : "Not granted");

                logText += String.format("Coarse Localization Permission: %s" + System.getProperty("line.separator"), (EddieApplication.applicationContext().checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) ? "Granted" : "Not granted");
                logText += String.format("Fine Localization Permission: %s" + System.getProperty("line.separator"), (EddieApplication.applicationContext().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) ? "Granted" : "Not granted");
            }
        }

        if(settingsLog == true)
        {
            logText += System.getProperty("line.separator") + System.getProperty("line.separator") + "--- Settings dump ---" + System.getProperty("line.separator") + System.getProperty("line.separator");
            logText += settingsManager.dump();
        }

        if(vpnStatusLog == true)
        {
            VPN vpn = EddieApplication.vpnManager().vpn();

            if(vpn != null)
            {
                HashMap<String, String> profileInfo = vpn.getProfileInfo();

                logText += System.getProperty("line.separator") + System.getProperty("line.separator") + "--- VPN status ---" + System.getProperty("line.separator") + System.getProperty("line.separator");

                logText += "Type: " + vpn.getType().toString();
                logText += System.getProperty("line.separator") + "Status: " + vpn.getConnectionStatus().getDescription();
                logText += System.getProperty("line.separator") + "Connection Mode: " + vpn.getConnectionModeDescription();
                logText += System.getProperty("line.separator") + "User: " + vpn.getUserName();
                logText += System.getProperty("line.separator") + "User Key: " + vpn.getUserProfileDescription() + System.getProperty("line.separator");

                if(profileInfo != null)
                {
                    logText += System.getProperty("line.separator") + System.getProperty("line.separator") + "--- VPN profile ---" + System.getProperty("line.separator");

                    if(profileInfo.containsKey("name"))
                        logText += System.getProperty("line.separator") + "Name: " + profileInfo.get("name");

                    if(profileInfo.containsKey("profile"))
                        logText += System.getProperty("line.separator") + "Profile: " + profileInfo.get("profile");

                    if(profileInfo.containsKey("status"))
                        logText += System.getProperty("line.separator") + "Status: " + profileInfo.get("status");

                    if(profileInfo.containsKey("description"))
                        logText += System.getProperty("line.separator") + "Description: " + profileInfo.get("description");

                    if(profileInfo.containsKey("vpn_type"))
                        logText += System.getProperty("line.separator") + "Type: " + profileInfo.get("vpn_type");

                    if(profileInfo.containsKey("server"))
                        logText += System.getProperty("line.separator") + "Server: " + profileInfo.get("server");

                    if(profileInfo.containsKey("port"))
                        logText += System.getProperty("line.separator") + "Port: " + profileInfo.get("port");

                    if(profileInfo.containsKey("protocol"))
                        logText += System.getProperty("line.separator") + "Protocol: " + profileInfo.get("protocol");

                    logText += System.getProperty("line.separator");
                }
            }
        }

        if(appLog == true)
        {
            logText += System.getProperty("line.separator") + System.getProperty("line.separator") + "--- Log dump ---" + System.getProperty("line.separator") + System.getProperty("line.separator");

            currentLog = getCurrentLog(FormatType.PLAIN_TEXT, LogTime.UTC);

            if(currentLog == null)
                return null;

            for(String entry : currentLog)
                logText += entry + System.getProperty("line.separator");
        }

        if(logcat == true)
        {
            String logcatDump = LogCat.dump(maxLogCatSize);

            logText += System.getProperty("line.separator") + System.getProperty("line.separator") + "--- logcat ";

            if(logcatDump.length() >= maxLogCatSize)
                logText += "(last " + maxLogCatSize + " bytes) ";

            logText += "---" + System.getProperty("line.separator") + System.getProperty("line.separator");

            logText += logcatDump;
        }

        exportLog = new HashMap<String, String>();

        exportLog.put("subject", logSubject + ".txt");
        exportLog.put("log", logText);

        return exportLog;
    }

    private class SendReportToAirVPN extends AsyncTask<String, String, JSONObject>
    {
        protected void onPreExecute()
        {
            super.onPreExecute();

            supportTools.showProgressDialog(thisActivity, getResources().getString(R.string.send_log_wait));
        }

        protected JSONObject doInBackground(String... params)
        {
            HashMap<String, String> appLog = null;
            HttpURLConnection httpURLConnection = null;
            BufferedReader bufferedReader = null;
            BufferedWriter bufferedWriter = null;
            InputStream inputStream = null;
            OutputStream outputStream = null;
            Uri.Builder uriBuilder = null;
            URL url = null;
            StringBuffer stringBuffer = null;
            String line = "";
            JSONObject jsonObject = null;

            try
            {
                appLog = createExportLog(true, true, true, true, true);

                url = new URL(params[0]);
                httpURLConnection = (HttpURLConnection) url.openConnection();

                httpURLConnection.setReadTimeout(SupportTools.HTTP_READ_TIMEOUT);
                httpURLConnection.setConnectTimeout(SupportTools.HTTP_CONNECTION_TIMEOUT);
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoInput(true);
                httpURLConnection.setDoOutput(true);

                uriBuilder = new Uri.Builder().appendQueryParameter("body", appLog.get("log"));

                outputStream = httpURLConnection.getOutputStream();

                bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                bufferedWriter.write(uriBuilder.build().getEncodedQuery());
                bufferedWriter.flush();
                bufferedWriter.close();

                outputStream.close();

                httpURLConnection.connect();

                inputStream = httpURLConnection.getInputStream();

                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                stringBuffer = new StringBuffer();

                while((line = bufferedReader.readLine()) != null)
                    stringBuffer.append(line + System.getProperty("line.separator"));
            }
            catch(MalformedURLException e)
            {
                EddieLogger.error("LogActivity.SendReportToAirVPN: %s", e);

                return null;
            }
            catch(IOException e)
            {
                EddieLogger.error("LogActivity.SendReportToAirVPN: %s", e);

                return null;
            }
            catch(Exception e)
            {
                EddieLogger.error("LogActivity.SendReportToAirVPN: %s", e);

                return null;
            }
            finally
            {
                if (httpURLConnection != null)
                    httpURLConnection.disconnect();

                try
                {
                    if(bufferedReader != null)
                        bufferedReader.close();
                }
                catch(IOException e)
                {
                    EddieLogger.error("LogActivity.SendReportToAirVPN: %s", e);

                    return null;
                }
            }

            if(stringBuffer != null && stringBuffer.length() > 0)
            {
                try
                {
                    jsonObject = new JSONObject(stringBuffer.toString());
                }
                catch(JSONException e)
                {
                    EddieLogger.error("LogActivity.SendReportToAirVPN: %s", e);

                    jsonObject = null;
                }
            }
            else
                jsonObject = null;

            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject response)
        {
            supportTools.dismissProgressDialog();

            if(response != null)
            {
                try
                {
                    if(supportTools.confirmationDialog(thisActivity, String.format(Locale.getDefault(), getResources().getString(R.string.send_log_success), response.getString("url")), getResources().getString(R.string.ok), getResources().getString(R.string.copy_to_clipboard)) == false)
                    {
                        ClipboardManager clipboardManager = (ClipboardManager)getSystemService(Context.CLIPBOARD_SERVICE);

                        ClipData clipData = ClipData.newPlainText("AirVPN log link", response.getString("url"));

                        clipboardManager.setPrimaryClip(clipData);
                    }
                }
                catch(JSONException e)
                {
                    EddieLogger.error("Malformed JSON reply: %s", e);

                    supportTools.infoDialog(thisActivity, getResources().getString(R.string.send_log_failure), true);
                }
            }
            else
                supportTools.infoDialog(thisActivity, getResources().getString(R.string.send_log_failure), true);
        }
    }
}
