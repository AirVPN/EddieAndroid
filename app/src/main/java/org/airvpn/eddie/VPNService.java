// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 20 June 2018 - author: ProMIND - initial release. Based on revised code from com.eddie.android. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.net.VpnService;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.provider.Settings;

import androidx.core.app.NotificationCompat;

import org.w3c.dom.Document;

import java.util.HashMap;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class VPNService extends VpnService implements MessageHandlerListener, NetworkStatusListener, EddieEventListener
{
    public static final int SERVICE_RUNNING_NOTIFICATION_ID = 10000;
    public static final String COMMAND_START = "START";
    public static final String COMMAND_STOP = "STOP";
    public static final String COMMAND_PROFILE = "PROFILE";

    public static final String EXTRA_RUN_ARGS = "RUN_ARGS";
    public static final String MESSAGE_TEXT = "MESSAGE";

    public static final String INTENT_ACTION = "INTENT_ACTION";

    public static final int MSG_BIND = 50000;
    public static final int MSG_START = 50001;
    public static final int MSG_STOP = 50002;
    public static final int MSG_PAUSE = 50003;
    public static final int MSG_RESUME = 50004;
    public static final int MSG_STATUS = 50005;
    public static final int MSG_REVOKE = 50006;

    public static final int MSG_BIND_ARG_REMOVE = 60000;
    public static final int MSG_BIND_ARG_ADD = 60001;

    public static final int THREAD_MAX_JOIN_TIME = 15000;  // 15 seconds

    private VPN.Status targetVpnDisconnectionStatus = VPN.Status.UNDEFINED;
    private String currentNotificationText = "";

    private NotificationCompat.Builder notificationBuilder = null;
    private int alertNotificationId = 2000;
    private PendingIntent pausePendingIntent;
    private PendingIntent resumePendingIntent;
    private PendingIntent disconnectPendingIntent;
    private Intent pauseIntent;
    private Intent resumeIntent;
    private Intent disconnectIntent;
    private int intentRequestCode = 1;

    private Messenger serviceMessenger = null;
    private Thread vpnThread = null;
    private VPNTunnel vpnTunnel = null;
    private VPNManager vpnManager = null;

    private Messenger clientMessenger = null;

    private ScreenReceiver screenReceiver = null;
    private SettingsManager settingsManager = null;
    private NetworkStatusReceiver networkStatusReceiver = null;
    private EddieEvent eddieEvent = null;
    private SupportTools supportTools = null;

    public static final int STATS_UPDATE_INTERVAL_SECONDS = 2; // Two seconds
    private Timer vpnStatTimer = null;
    private long statVpnPrevBytesIn = 0, statVpnPrevBytesOut = 0;
    private long vpnInDiff = 0, vpnOutDiff = 0;
    private long vpnInRate = 0, vpnOutRate = 0;
    private VPNTransportStats vpnStats = null;
    private VPN.Status lastChangedStatus = VPN.Status.UNDEFINED;
    private static boolean bootConnectionPending = false, stopConnectionPending = false;

    private class ScreenReceiver extends BroadcastReceiver
    {
        private VPNService vpnService = null;

        public ScreenReceiver(VPNService service)
        {
            vpnService = service;
        }

        @Override
        public void onReceive(Context context, Intent intent)
        {
            String action = intent.getAction();

            EddieLogger.debug(String.format(Locale.getDefault(), "ScreenReceiver.onReceive() action: '%s'", action));

            if(vpnService != null)
            {
                if(action == Intent.ACTION_SCREEN_ON)
                    vpnService.onScreenChanged(true);
                else if(action == Intent.ACTION_SCREEN_OFF)
                    vpnService.onScreenChanged(false);
                else
                    EddieLogger.error(String.format(Locale.getDefault(), "Unhandled action '%s' received in ScreenReceiver", action));
            }
            else
                EddieLogger.error("ScreenReceiver.onReceive(): vpnService is null");
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        int result = START_STICKY;

        if(intent != null)
        {
            if(intent.getBooleanExtra(COMMAND_START, false))
            {
                EddieLogger.debug("VPNService: Received start command");

                updateService(VPN.Status.CONNECTING, intent.getBundleExtra(EXTRA_RUN_ARGS));
            }
            else if(intent.getBooleanExtra(COMMAND_STOP, false))
            {
                EddieLogger.debug("VPNService: Received stop command");

                updateService(VPN.Status.DISCONNECTING, intent.getBundleExtra(EXTRA_RUN_ARGS));
            }
            else
            {
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N && (vpnManager.vpn().getConnectionStatus() == VPN.Status.NOT_CONNECTED || vpnManager.vpn().getConnectionStatus() == VPN.Status.CONNECTION_REVOKED_BY_SYSTEM))
                {
                    if(vpnManager.isVpnConnectionStopped() == true)
                    {
                        EddieLogger.info("Received \"VPN Always On\" action: Check whether it is needed to start the VPN startup connection.");

                        bootConnectionPending = true;

                        tryBootConnection();
                    }
                }
                else
                    result = START_NOT_STICKY;
            }
        }
        else
        {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            {
                updateService(VPN.Status.CONNECTING, null);
            }
            else
                result = START_NOT_STICKY;
        }

        if(vpnManager.vpn().getConnectionStatus() == VPN.Status.CONNECTING || bootConnectionPending == true)
            startForeground();

        return result;
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        String action = null;
        IBinder binder = null;

        if(intent != null)
            action = intent.getAction();

        if(action != null && action.equals(VpnService.SERVICE_INTERFACE))
            binder = super.onBind(intent);
        else
            binder = serviceMessenger != null ? serviceMessenger.getBinder() : null;

        return binder;
    }

    private void init()
    {
        serviceMessenger = new Messenger(new MessageHandler(this));
    }

    @Override
    public void onCreate()
    {
        super.onCreate();

        settingsManager = EddieApplication.settingsManager();
        supportTools = EddieApplication.supportTools();
        vpnManager = EddieApplication.vpnManager();

        init();

        networkStatusReceiver = EddieApplication.networkStatusReceiver();

        networkStatusReceiver.subscribeListener(this);

        eddieEvent = EddieApplication.eddieEvent();

        eddieEvent.subscribeListener(this);

        lastChangedStatus = VPN.Status.UNDEFINED;
    }

    @Override
    public void onDestroy()
    {
        if(serviceMessenger != null)
            serviceMessenger = null;

        if(networkStatusReceiver != null)
            networkStatusReceiver.unsubscribeListener(this);

        if(eddieEvent != null)
            eddieEvent.unsubscribeListener(this);

        super.onDestroy();
    }

    @Override
    public void onRevoke()
    {
        alertNotification(getResources().getString(R.string.action_system_revoked_vpn));

        EddieLogger.warning("System revoked VPN connection, probably because another app requested VPN connection. Eddie has been disconnected.");

        targetVpnDisconnectionStatus = VPN.Status.CONNECTION_REVOKED_BY_SYSTEM;

        if(clientMessenger != null)
        {
            Message message = Message.obtain(null, VPNService.MSG_REVOKE, 0, 0);

            sendMessage(clientMessenger, message);
        }

        updateService(VPN.Status.DISCONNECTING, null);
    }

    public synchronized void onMessage(Message msg)
    {
        if(msg == null)
            return;

        switch(msg.what)
        {
            case MSG_BIND:
            {
                if(msg.arg1 == MSG_BIND_ARG_ADD)
                {
                    clientMessenger = msg.replyTo;

                    sendMessage(clientMessenger, createStatusMessage(vpnManager.vpn().getConnectionStatus(), vpnManager.vpn().connectionStatusDescription()));
                }
                else
                    clientMessenger = null;
            }
            break;

            case MSG_START:
            {
                updateService(VPN.Status.CONNECTING, msg.getData());
            }
            break;

            case MSG_STOP:
            {
                if(targetVpnDisconnectionStatus != VPN.Status.CONNECTION_ERROR)
                    targetVpnDisconnectionStatus = VPN.Status.NOT_CONNECTED;

                updateService(VPN.Status.DISCONNECTING, null);
            }
            break;

            case MSG_PAUSE:
            {
                pauseService();
            }
            break;

            case MSG_RESUME:
            {
                resumeService();
            }
            break;
        }
    }

    private synchronized boolean updateService(VPN.Status status, Bundle data)
    {
        boolean running = (vpnManager.vpn().getConnectionStatus() == VPN.Status.CONNECTED) || (vpnManager.vpn().getConnectionStatus() == VPN.Status.CONNECTING) || (vpnManager.vpn().getConnectionStatus() == VPN.Status.PAUSED_BY_USER) || (vpnManager.vpn().getConnectionStatus() == VPN.Status.PAUSED_BY_SYSTEM) || (vpnManager.vpn().getConnectionStatus() == VPN.Status.LOCKED);

        if(running && status == VPN.Status.CONNECTED)
            return true;

        switch(status)
        {
            case CONNECTING:
            {
                start(data);
            }
            break;

            case DISCONNECTING:
            {
                stop(status);
            }
            break;

            default:
            {
                return false;
            }
        }

        return true;
    }

    private void onServiceStarted()
    {
        if(settingsManager.isSystemPauseVpnWhenScreenIsOff())
            registerScreenReceiver();
    }

    private void onServiceStopped()
    {
        unregisterScreenReceiver();
    }

    private void onScreenChanged(boolean active)
    {
        EddieLogger.debug(String.format(Locale.getDefault(), "VPNService.onScreenChanged(): active is %s", Boolean.toString(active)));

        try
        {
            if(vpnTunnel != null)
            {
                VPN.Status status;

                status = vpnTunnel.handleScreenChanged(active);

                changeStatus(status);
            }
        }
        catch(Exception e)
        {
            EddieLogger.error("VPNService.onScreenChanged() Exception: %s", e);
        }
    }

    private void networkStatusChanged(VPNTunnel.Action action)
    {
        EddieLogger.debug(String.format(Locale.getDefault(), "VPNService.networkStatusChanged() action: '%s'", action.toString()));

        try
        {
            if(vpnTunnel != null)
                vpnTunnel.networkStatusChanged(action);

            if(action == VPNTunnel.Action.SYSTEM_PAUSE)
            {
                changeStatus(VPN.Status.PAUSED_BY_SYSTEM);

                if(vpnTunnel != null)
                    vpnTunnel.updateNotification(VPN.Status.PAUSED_BY_SYSTEM);
            }
        }
        catch(Exception e)
        {
            EddieLogger.error("VPNService.networkStatusChanged() Exception: %s", e);
        }
    }

    private void registerScreenReceiver()
    {
        if(screenReceiver != null || vpnManager.vpn().getType() != VPN.Type.OPENVPN)
            return;

        IntentFilter intentFilter = new IntentFilter();

        intentFilter.addAction(Intent.ACTION_SCREEN_ON);
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF);

        screenReceiver = new ScreenReceiver(this);

        try
        {
            registerReceiver(screenReceiver, intentFilter);
        }
        catch(Exception e)
        {
            EddieLogger.error("VPNService.registerScreenReceiver(): Error while registering screenReceiver. Exception: %s", e);
        }
    }

    private void unregisterScreenReceiver()
    {
        if(screenReceiver != null)
        {
            try
            {
                unregisterReceiver(screenReceiver);
            }
            catch(Exception e)
            {
            }

            screenReceiver = null;
        }
    }

    public void handleThreadStarted()
    {
        onServiceStarted();

        changeStatus(VPN.Status.CONNECTING);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
        {
            if(isAlwaysOn())
                settingsManager.setAlwaysOnVpn(SettingsManager.ON);
            else
                settingsManager.setAlwaysOnVpn(SettingsManager.OFF);

            if(isLockdownEnabled())
                settingsManager.setVpnLockdown(SettingsManager.ON);
            else
                settingsManager.setVpnLockdown(SettingsManager.OFF);
        }
        else
        {
            String p;

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            {
                p = Settings.Secure.getString(getApplicationContext().getContentResolver(), "always_on_vpn_app" ); // Settings.Secure.ALWAYS_ON_VPN_APP

                if(p != null)
                {
                    if(p.equals(getApplicationContext().getPackageName()))
                        settingsManager.setAlwaysOnVpn(SettingsManager.ON);
                    else
                        settingsManager.setAlwaysOnVpn(SettingsManager.OFF);
                }
                else
                    settingsManager.setAlwaysOnVpn(SettingsManager.UNKNOWN);
            }
            else
                settingsManager.setAlwaysOnVpn(SettingsManager.UNKNOWN);

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            {
                if(settingsManager.isAlwaysOnVpnSet() == true)
                {
                    p = Settings.Secure.getString(getApplicationContext().getContentResolver(), "always_on_vpn_lockdown"); // Settings.Secure.ALWAYS_ON_VPN_LOCKDOWN

                    if(p != null)
                    {
                        if(p != null && p.equals("1"))
                            settingsManager.setVpnLockdown(SettingsManager.ON);
                        else
                            settingsManager.setVpnLockdown(SettingsManager.OFF);
                    }
                    else
                        settingsManager.setVpnLockdown(SettingsManager.UNKNOWN);
                }
                else
                    settingsManager.setVpnLockdown(SettingsManager.OFF);
            }
            else
                settingsManager.setVpnLockdown(SettingsManager.UNKNOWN);
        }
    }

    public void handleConnectionError()
    {
        VPNEvent vpnEvent = new VPNEvent();

        stop(VPN.Status.CONNECTION_ERROR);

        changeStatus(VPN.Status.CONNECTION_ERROR);

        vpnEvent.type = VPNEvent.FATAL_ERROR;
        vpnEvent.notifyUser = true;
        vpnEvent.name = "";
        vpnEvent.info = "VpnService: Connection error";

        eddieEvent.onVpnError(vpnEvent);

        EddieLogger.error(vpnEvent.info);

        changeStatus(VPN.Status.NOT_CONNECTED);
    }

    public void handleThreadException(Exception e)
    {
        EddieLogger.error("VPNService.handleThreadException() Exception: %s", e);

        stop(VPN.Status.NOT_CONNECTED);
    }

    private void stopService(VPN.Status status)
    {
        if(stopConnectionPending == true)
            return;

        stopConnectionPending = true;

        cleanupTunnel();

        stopForeground();

        onServiceStopped();

        stopSelf();

        changeStatus(status);
    }

    private void pauseService()
    {
        try
        {
            if(vpnTunnel != null)
            {
                vpnTunnel.networkStatusChanged(VPNTunnel.Action.USER_PAUSE);

                changeStatus(VPN.Status.PAUSED_BY_USER);
            }
        }
        catch(Exception e)
        {
            EddieLogger.error("VPNService.pauseService() Exception: %s", e);
        }
    }

    private void resumeService()
    {
        try
        {
            if(vpnTunnel != null)
            {
                vpnTunnel.networkStatusChanged(VPNTunnel.Action.USER_RESUME);

                changeStatus(VPN.Status.CONNECTED);
            }
        }
        catch(Exception e)
        {
            EddieLogger.error("VPNService.resumeService() Exception: %s", e);
        }
    }

    private void cleanupTunnel()
    {
        try
        {
            if(vpnTunnel != null)
                vpnTunnel.cleanup();
        }
        catch(Exception e)
        {
            EddieLogger.error("VPNService.cleanupTunnel() exception: %s", e.getMessage());
        }
        finally
        {
            vpnTunnel = null;
        }
    }

    public synchronized void changeStatus(VPN.Status status)
    {
        if(lastChangedStatus == status)
            return;

        lastChangedStatus = status;

        vpnManager.vpn().setConnectionStatus(status);

        if(clientMessenger != null)
            sendMessage(clientMessenger, createStatusMessage(status, getResources().getString(vpnManager.vpn().connectionStatusResourceDescription(status))));
        else
        {
            EddieEvent eddieEvent = EddieApplication.eddieEvent();

            eddieEvent.onVpnStatusChanged(status, getResources().getString(vpnManager.vpn().connectionStatusResourceDescription(status)));
        }

        switch(status)
        {
            case CONNECTED:
            {
                startVPNStats();
            }
            break;

            case PAUSED_BY_USER:
            case PAUSED_BY_SYSTEM:
            case LOCKED:
            case NOT_CONNECTED:
            case CONNECTION_REVOKED_BY_SYSTEM:
            case CONNECTION_ERROR:
            {
                stopVPNStats();
            }
            break;

            default:
            {
            }
            break;
        }
    }

    private Message createStatusMessage(VPN.Status status, String messageText)
    {
        Message message = Message.obtain(null, VPNService.MSG_STATUS, vpnManager.vpn().getConnectionStatus().getValue(), 0);

        message.getData().putString(MESSAGE_TEXT, messageText);

        return message;
    }

    private void sendMessage(Messenger client, Message message)
    {
        try
        {
            client.send(message);
        }
        catch(RemoteException e)
        {
            EddieLogger.error("VPNService.sendMessage(): error in sending message. Exception: %s", e);
        }
    }

    private void start(Bundle data)
    {
        if(EddieApplication.isInitialized() == true)
        {
            try
            {
                tunnelSetup(data);
            }
            catch(Exception e)
            {
                VPNEvent vpnEvent = new VPNEvent();

                stopService(VPN.Status.NOT_CONNECTED);

                vpnEvent.type = VPNEvent.ERROR;
                vpnEvent.notifyUser = true;
                vpnEvent.name = "";
                vpnEvent.info = String.format("VPNService.doStart() exception: %s", e.getMessage());

                eddieEvent.onVpnError(vpnEvent);

                EddieLogger.error(vpnEvent.info);

                return;
            }

            Thread newVpnTask = SupportTools.startThread(new Runnable()
            {
                @Override
                public void run()
                {
                    if(vpnTunnel != null)
                    {
                        EddieLogger.info("Starting VPN thread");

                        vpnTunnel.run();
                    }
                }
            });

            if(newVpnTask != null)
                vpnThread = newVpnTask;

            stopConnectionPending = false;

            lastChangedStatus = VPN.Status.UNDEFINED;
        }
        else
        {
            EddieLogger.error("VPNService.doStart() initialization failed");

            stopService(VPN.Status.NOT_CONNECTED);
        }
    }

    private void stop(final VPN.Status status)
    {
        if(stopConnectionPending == true)
            return;

        changeStatus(VPN.Status.NOT_CONNECTED);

        SupportTools.startThread(new Runnable()
        {
            @Override
            public void run()
            {
                stopService(targetVpnDisconnectionStatus);

                currentNotificationText = "";

                waitForVpnThreadToFinish();
            }
        });
    }

    public PendingIntent createConfigIntent()
    {
        Intent configIntent = new Intent(this, SettingsActivity.class);

        configIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

        return PendingIntent.getActivity(this, intentRequestCode++, configIntent, PendingIntent.FLAG_IMMUTABLE);
    }

    private void tunnelSetup(Bundle data) throws Exception
    {
        if(vpnTunnel != null)
            throw new Exception("VPNService.tunnelSetup(): vpnTunnel already initialized");

        if(data == null)
            throw new Exception("VPNService.tunnelSetup(): data bundle is null)");

        switch(vpnManager.vpn().getType())
        {
            case OPENVPN:
            {
                vpnTunnel = new OpenVPNTunnel(this);
            }
            break;

            case WIREGUARD:
            {
                vpnTunnel = new WireGuardTunnel(this);
            }
            break;

            default:
            {
                throw new Exception("VPNService.tunnelSetup(): Unknown tunnel type " + vpnManager.vpn().getType());
            }
        }

        try
        {
            vpnTunnel.init();
        }
        catch(Exception e)
        {
            VPNEvent vpnEvent = new VPNEvent();

            stopService(VPN.Status.NOT_CONNECTED);

            vpnEvent.type = VPNEvent.ERROR;
            vpnEvent.notifyUser = true;
            vpnEvent.name = "";
            vpnEvent.info = String.format("VPNService.tunnelSetup() exception: %s", e.getMessage());

            eddieEvent.onVpnError(vpnEvent);

            EddieLogger.error(vpnEvent.info);

            return;
        }

        String profile = data.getString(COMMAND_PROFILE, "");

        if(profile.length() == 0)
            throw new Exception("VPNService.tunnelSetup(): profile is empty");

        try
        {
            if(vpnTunnel.loadProfileString(profile) == false)
            {
                VPNEvent vpnEvent = new VPNEvent();

                stopService(VPN.Status.NOT_CONNECTED);

                vpnEvent.type = VPNEvent.ERROR;
                vpnEvent.notifyUser = true;
                vpnEvent.name = "";
                vpnEvent.info = "VPNService.tunnelSetup(): profile error";

                eddieEvent.onVpnError(vpnEvent);

                EddieLogger.error(vpnEvent.info);

                return;
            }
        }
        catch(Exception e)
        {
            VPNEvent vpnEvent = new VPNEvent();

            stopService(VPN.Status.NOT_CONNECTED);

            vpnEvent.type = VPNEvent.ERROR;
            vpnEvent.notifyUser = true;
            vpnEvent.name = "";
            vpnEvent.info = String.format("VPNService.tunnelSetup() exception: %s", e.getMessage());

            eddieEvent.onVpnError(vpnEvent);

            EddieLogger.error(vpnEvent.info);

            return;
        }

        vpnTunnel.bindOptions();

        vpnManager.vpn().setVpnProfile(profile);
    }

    private void waitForVpnThreadToFinish()
    {
        if(vpnThread == null)
            return;

        switch(vpnManager.vpn().getType())
        {
            case OPENVPN:
            {
                try
                {
                    vpnThread.join(THREAD_MAX_JOIN_TIME);
                }
                catch(InterruptedException e)
                {
                    EddieLogger.error("VPNService.waitVpnThreadToFinish(): VPN thread has been interrupted");
                }
                finally
                {
                    if(vpnThread.isAlive())
                        EddieLogger.error("VPNService.waitVpnThreadToFinish(): VPN thread did not end");
                    else
                        EddieLogger.info("VPN thread execution has completed");
                }
            }
            break;

            case WIREGUARD:
            {
                if(vpnThread.isAlive())
                    vpnThread.interrupt();
            }
            break;
        }
    }

    private void startForeground()
    {
        if(notificationBuilder == null)
        {
            String text, server = "";
            HashMap<String, String> profileInfo = vpnManager.vpn().getProfileInfo();

            String channelId = getResources().getString(R.string.notification_channel_id);
            String channelName = getResources().getString(R.string.notification_channel_name);

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            {
                NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

                NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH);

                notificationManager.createNotificationChannel(notificationChannel);
            }

            if(profileInfo != null)
            {
                if(vpnManager.vpn().getConnectionMode() == VPN.ConnectionMode.AIRVPN_SERVER || vpnManager.vpn().getConnectionMode() == VPN.ConnectionMode.QUICK_CONNECT)
                    server = String.format("AirVPN %s (%s)", profileInfo.get("description"), profileInfo.get("server"));
                else
                {
                    if(!profileInfo.get("description").isEmpty())
                        server = profileInfo.get("description");
                    else
                        server = profileInfo.get("server");
                }
            }

            text = String.format(Locale.getDefault(), getResources().getString(R.string.connecting_to_server), server);

            if(!NetworkStatusReceiver.getNetworkDescription().equals(""))
                text += " " + String.format(Locale.getDefault(), getResources().getString(R.string.network_info), NetworkStatusReceiver.getNetworkDescription());

            notificationBuilder = new NotificationCompat.Builder(this, channelId);

            if(notificationBuilder != null)
            {
                pauseIntent = new Intent(this, MainActivity.class);
                pauseIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                pauseIntent.putExtra(INTENT_ACTION, MSG_PAUSE);
                pausePendingIntent = PendingIntent.getActivity(this, intentRequestCode++, pauseIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

                resumeIntent = new Intent(this, MainActivity.class);
                resumeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                resumeIntent.putExtra(INTENT_ACTION, MSG_RESUME);
                resumePendingIntent = PendingIntent.getActivity(this, intentRequestCode++, resumeIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

                disconnectIntent = new Intent(this, MainActivity.class);
                disconnectIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                disconnectIntent.putExtra(INTENT_ACTION, MSG_STOP);
                disconnectPendingIntent = PendingIntent.getActivity(this, intentRequestCode++, disconnectIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

                notificationBuilder.setContentTitle(getResources().getString(R.string.notification_title))
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                        .setContentText(text)
                        .setSmallIcon(R.drawable.notification_icon)
                        .setColor(getResources().getColor(R.color.notificationColor))
                        .setContentIntent(buildMainActivityIntent())
                        .setPriority(NotificationCompat.PRIORITY_MAX)
                        .setOnlyAlertOnce(true)
                        .setOngoing(true);

                try
                {
                    if(Build.VERSION.SDK_INT < Build.VERSION_CODES.O)
                    {
                        if(settingsManager.isSystemNotificationSound())
                            notificationBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
                        else
                            notificationBuilder.setSound(Uri.parse("android.resource://" + getApplicationContext().getPackageName() + "/" + R.raw.silence));
                    }

                    startForeground(SERVICE_RUNNING_NOTIFICATION_ID, notificationBuilder.build());

                    currentNotificationText = text;
                }
                catch(Exception e)
                {
                    EddieLogger.error("VPNService.doStartForeground(): Cannot start foreground service. %s", e.getMessage());
                }
            }
        }
    }

    public void updateNotification(String text)
    {
        updateNotification(text, true);
    }

    public void updateNotification(String text, boolean highPriority)
    {
        if(notificationBuilder != null && !text.equals("") && !text.equals(currentNotificationText))
        {
            String channelId = getResources().getString(R.string.notification_channel_id);
            String channelName = getResources().getString(R.string.notification_channel_name);

            NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            {
                NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, highPriority ? NotificationManager.IMPORTANCE_HIGH : NotificationManager.IMPORTANCE_LOW);

                notificationManager.createNotificationChannel(notificationChannel);
            }

            notificationBuilder = new NotificationCompat.Builder(this, channelId);

            if(notificationBuilder != null)
            {
                notificationBuilder.setContentTitle(getResources().getString(R.string.notification_title))
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                        .setContentText(text)
                        .setSmallIcon(R.drawable.notification_icon)
                        .setColor(getResources().getColor(R.color.notificationColor))
                        .setContentIntent(buildMainActivityIntent())
                        .setPriority(highPriority ? NotificationCompat.PRIORITY_HIGH : NotificationCompat.PRIORITY_LOW)
                        .setOnlyAlertOnce(true)
                        .setOngoing(true);

                notificationBuilder.addAction(android.R.drawable.ic_lock_power_off, getString(R.string.conn_disconnect_profile), disconnectPendingIntent);

                if(vpnManager.vpn().getType() == VPN.Type.OPENVPN && NetworkStatusReceiver.getNetworkStatus() == NetworkStatusReceiver.Status.CONNECTED)
                {
                    if(vpnManager.isVpnConnectionPaused() == true)
                        notificationBuilder.addAction(android.R.drawable.ic_media_play, getString(R.string.conn_resume_profile), resumePendingIntent);
                    else if(vpnManager.isVpnConnectionStarted() == true)
                        notificationBuilder.addAction(android.R.drawable.ic_media_pause, getString(R.string.conn_pause_profile), pausePendingIntent);
                }

                try
                {
                    if(Build.VERSION.SDK_INT < Build.VERSION_CODES.O)
                    {
                        if(settingsManager.isSystemNotificationSound())
                            notificationBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
                        else
                            notificationBuilder.setSound(Uri.parse("android.resource://" + getApplicationContext().getPackageName() + "/" + R.raw.silence));
                    }

                    notificationManager.notify(SERVICE_RUNNING_NOTIFICATION_ID, notificationBuilder.build());

                    currentNotificationText = text;
                }
                catch(Exception e)
                {
                }
            }
        }
    }

    public void alertNotification(String message)
    {
        if(message.equals("") && !message.equals(currentNotificationText))
            return;

        if(vpnManager.vpn().getConnectionMode() != VPN.ConnectionMode.QUICK_CONNECT)
            supportTools.dismissConnectionProgressDialog();

        supportTools.dismissProgressDialog();

        String channelId = getResources().getString(R.string.notification_channel_id);
        String channelName = getResources().getString(R.string.notification_channel_name);

        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH);

            notificationManager.createNotificationChannel(notificationChannel);
        }

        NotificationCompat.Builder alertNotification = new NotificationCompat.Builder(this, channelId);

        if(alertNotification != null)
        {
            alertNotification.setContentTitle(getResources().getString(R.string.notification_title))
                    .setSmallIcon(R.drawable.notification_icon)
                    .setColor(getResources().getColor(R.color.notificationColor))
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setContentText(message)
                    .setContentIntent(buildMainActivityIntent())
                    .setChannelId(channelId)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setAutoCancel(true);

            if(Build.VERSION.SDK_INT < Build.VERSION_CODES.O && notificationBuilder != null)
            {
                if(settingsManager.isSystemNotificationSound())
                    notificationBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
                else
                    notificationBuilder.setSound(Uri.parse("android.resource://" + getApplicationContext().getPackageName() + "/" + R.raw.silence));
            }

            try
            {
                notificationManager.notify(alertNotificationId, alertNotification.build());
            }
            catch(Exception e)
            {
                EddieLogger.error("VPNService.alertNotification(): Cannot raise notification: %s", e.getMessage());
            }

            alertNotificationId++;

            currentNotificationText = message;
        }
    }

    private PendingIntent buildMainActivityIntent()
    {
        Intent intent = new Intent(this, MainActivity.class);

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        return PendingIntent.getActivity(this, intentRequestCode++, intent, PendingIntent.FLAG_IMMUTABLE);
    }

    private void stopForeground()
    {
        if(notificationBuilder != null)
        {
            stopForeground(true);

            notificationBuilder = null;
        }
    }

    private void startVPNStats()
    {
        int updateInterval = STATS_UPDATE_INTERVAL_SECONDS * 1000;

        statVpnPrevBytesIn = 0;
        statVpnPrevBytesOut = 0;

        if(vpnStatTimer != null)
            vpnStatTimer.cancel();

        vpnStatTimer = new Timer();

        vpnStatTimer.schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                if(vpnTunnel == null)
                    return;

                try
                {
                    vpnStats = vpnTunnel.getTransportStats();
                }
                catch(Exception e)
                {
                    EddieLogger.error("VPNService: Cannot get VPN Client Transport Stats: %s", e.getMessage());

                    vpnStats = null;
                }

                if(vpnStats != null)
                {
                    if(vpnStats.resultCode == EddieLibrary.SUCCESS)
                    {
                        vpnInDiff = vpnStats.bytesIn - statVpnPrevBytesIn;
                        vpnOutDiff = vpnStats.bytesOut - statVpnPrevBytesOut;

                        if(statVpnPrevBytesIn > 0)
                        {
                            vpnInRate = (vpnInDiff * 8) / STATS_UPDATE_INTERVAL_SECONDS;

                            vpnManager.vpn().setInRate(vpnInRate);

                            if(vpnInRate > vpnManager.vpn().getMaxInRate())
                                vpnManager.vpn().setMaxInRate(vpnInRate);
                        }

                        if(statVpnPrevBytesOut > 0)
                        {
                            vpnOutRate = (vpnOutDiff * 8) / STATS_UPDATE_INTERVAL_SECONDS;

                            vpnManager.vpn().setOutRate(vpnOutRate);

                            if(vpnOutRate > vpnManager.vpn().getMaxOutRate())
                                vpnManager.vpn().setMaxOutRate(vpnOutRate);
                        }

                        vpnManager.vpn().addSecondsConnectionTime(STATS_UPDATE_INTERVAL_SECONDS);

                        vpnManager.vpn().setVpnTransportStats(vpnStats);

                        statVpnPrevBytesIn = vpnStats.bytesIn;
                        statVpnPrevBytesOut = vpnStats.bytesOut;

                        if(vpnTunnel != null)
                            vpnTunnel.updateNotification(VPN.Status.CONNECTED);
                    }
                    else
                        EddieLogger.warning("VPNService: getVPNClientTransportStats() error: %s", vpnStats.resultDescription);
                }
            }
        }, updateInterval, updateInterval);
    }

    public void stopVPNStats()
    {
        if(vpnStatTimer != null)
            vpnStatTimer.cancel();

        vpnStatTimer = null;

        vpnManager.vpn().resetSessionTime();
        vpnManager.vpn().setInRate(0);
        vpnManager.vpn().setOutRate(0);
    }

    private void tryBootConnection()
    {
        Intent tileServiceIntent = null;

        if(bootConnectionPending == false)
            return;

        if(settingsManager.isStartVpnAtStartupEnabled() == false)
        {
            EddieLogger.info("Start VPN connection at device startup is disabled");

            return;
        }

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
        {
            tileServiceIntent = new Intent(getApplicationContext(), AirVPNTileService.class);

            startService(tileServiceIntent);
        }

        SupportTools.startThread(new Runnable()
        {
            @Override
            public void run()
            {
                boolean result;

                bootConnectionPending = false;

                result = BootVPNActivity.startConnection(getApplicationContext());

                if(result == false)
                    EddieLogger.error("Cannot start VPN startup connection");
            }
        });
    }

    // Eddie events

    public void onVpnConnectionStatsChanged(final VPNConnectionStats connectionStats)
    {
    }

    public void onVpnStatusChanged(final VPN.Status status, final String message)
    {
    }

    public void onVpnAuthFailed(final VPNEvent oe)
    {
    }

    public void onVpnError(final VPNEvent oe)
    {
    }

    public void onVpnReconnect(final VPNEvent oe)
    {
    }

    public void onMasterPasswordChanged()
    {
    }

    public void onAirVPNLogin()
    {
    }

    public void onAirVPNLoginFailed(String message)
    {
    }

    public void onAirVPNLogout()
    {
    }

    public void onAirVPNUserDataChanged()
    {
    }

    public void onAirVPNUserProfileReceived(Document document)
    {
    }

    public void onAirVPNUserProfileDownloadError()
    {
    }

    public void onAirVPNUserProfileChanged()
    {
    }

    public void onAirVPNManifestReceived(Document document)
    {
        if(bootConnectionPending == true)
            tryBootConnection();
    }

    public void onAirVPNManifestDownloadError()
    {
    }

    public void onAirVPNManifestChanged()
    {
        if(bootConnectionPending == true)
            tryBootConnection();
    }

    public void onAirVPNIgnoredManifestDocumentRequest()
    {
    }

    public void onAirVPNIgnoredUserDocumentRequest()
    {
    }

    public void onAirVPNRequestError(String message)
    {
    }

    public void onCancelConnection()
    {
    }

    public void onSaveState()
    {
    }

    public void onRestoreState(Bundle bundle)
    {
    }

    // NetworkStatusReceiver

    public void onNetworkStatusNotAvailable()
    {
        EddieLogger.info("Network is not available");

        if(vpnManager.vpn().getType() == VPN.Type.OPENVPN && vpnManager.isVpnConnectionPaused() == false)
            networkStatusChanged(VPNTunnel.Action.SYSTEM_PAUSE);

    }

    public void onNetworkStatusConnected()
    {
        String text, server = "";
        HashMap<String, String> profileInfo = vpnManager.vpn().getProfileInfo();

        EddieLogger.info("Network is connected to %s", NetworkStatusReceiver.getNetworkDescription());

        if(vpnManager.vpn().getType() == VPN.Type.OPENVPN && vpnManager.vpn().getConnectionStatus() != VPN.Status.PAUSED_BY_USER)
        {
            networkStatusChanged(VPNTunnel.Action.SYSTEM_RESUME);

            if(bootConnectionPending == true)
                tryBootConnection();

            if(profileInfo != null)
            {
                if(vpnManager.vpn().getConnectionMode() == VPN.ConnectionMode.AIRVPN_SERVER || vpnManager.vpn().getConnectionMode() == VPN.ConnectionMode.QUICK_CONNECT)
                    server = String.format("AirVPN %s (%s)", profileInfo.get("description"), profileInfo.get("server"));
                else
                {
                    if(!profileInfo.get("description").isEmpty())
                        server = profileInfo.get("description");
                    else
                        server = profileInfo.get("server");
                }

                server += " (" + profileInfo.get("vpn_type") + ")";
            }

            text = String.format(Locale.getDefault(), getResources().getString(R.string.connected_to_server), server);

            if(!NetworkStatusReceiver.getNetworkDescription().equals(""))
                text += " " + String.format(Locale.getDefault(), getResources().getString(R.string.network_info), NetworkStatusReceiver.getNetworkDescription());

            if(vpnManager.isGpsSpoofingEnabled() == true)
            {
                text += " (" + this.getResources().getString(R.string.conn_gps_spoofing_cap);
                text += " " + CountryContinent.getCountryName(vpnManager.getGpsSpoofingCoordinates().getCountryCode()) + ")";
            }

            updateNotification(text);
        }
    }

    public void onNetworkStatusIsConnecting()
    {
        EddieLogger.info("Network is connecting");

        if(vpnManager.vpn().getType() == VPN.Type.OPENVPN && vpnManager.isVpnConnectionPaused() == false)
            networkStatusChanged(VPNTunnel.Action.SYSTEM_PAUSE);
    }

    public void onNetworkStatusIsDisconnecting()
    {
        EddieLogger.info("Network is disconnecting");

        if(vpnManager.vpn().getType() == VPN.Type.OPENVPN && vpnManager.isVpnConnectionPaused() == false)
            networkStatusChanged(VPNTunnel.Action.SYSTEM_PAUSE);
    }

    public void onNetworkStatusSuspended()
    {
        EddieLogger.info("Network is suspended");

        if(vpnManager.vpn().getType() == VPN.Type.OPENVPN && vpnManager.isVpnConnectionPaused() == false)
            networkStatusChanged(VPNTunnel.Action.SYSTEM_PAUSE);
    }

    public void onNetworkStatusNotConnected()
    {
        EddieLogger.info("Network is not connected");

        if(vpnManager.vpn().getType() == VPN.Type.OPENVPN && vpnManager.isVpnConnectionPaused() == false)
            networkStatusChanged(VPNTunnel.Action.SYSTEM_PAUSE);
    }

    public void onNetworkTypeChanged()
    {
        if(NetworkStatusReceiver.getNetworkDescription().isEmpty() == false)
            EddieLogger.info("Network type has changed to %s", NetworkStatusReceiver.getNetworkDescription());

        networkStatusChanged(VPNTunnel.Action.NETWORK_TYPE_CHANGED);

        if(vpnManager.vpn().getType() == VPN.Type.OPENVPN)
        {
            if(bootConnectionPending == true)
                tryBootConnection();
        }
    }
}
