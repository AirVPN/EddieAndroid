// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 20 June 2018 - author: ProMIND - initial release. Based on revised code from com.eddie.android. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class IPAddress
{
    private String ipAddress = "";
    private IPFamily ipFamily = IPFamily.UNDEFINED;
    private int prefixLength = -1;

    public enum IPFamily
    {
        UNDEFINED,
        IPv4,
        IPv6;

        @Override
        public String toString()
        {
            String val = "";

            switch(this)
            {
                case IPv4:
                {
                    val = "IPv4";
                }
                break;

                case IPv6:
                {
                    val = "IPv6";
                }
                break;

                default:
                {
                    val = "Unknown";
                }
                break;
            }

            return val;
        }

        public static IPFamily fromString(String s)
        {
            IPFamily f = UNDEFINED;

            if(s == null)
                return UNDEFINED;

            s = s.toLowerCase(Locale.US);

            switch(s)
            {
                case "ipv4":
                {
                    f = IPv4;
                }
                break;

                case "ipv6":
                {
                    f = IPv6;
                }
                break;

                default:
                {
                    f = UNDEFINED;
                }
                break;
            }

            return f;
        }
    }

    public IPAddress(String address, IPFamily family, int prefixLen)
    {
        if(family == null)
            family = IPFamily.UNDEFINED;

        ipAddress = address;
        ipFamily = family;
        prefixLength = prefixLen;
    }

    public IPAddress(String address, IPFamily family)
    {
        if(family == null)
            family = IPFamily.UNDEFINED;

        ipAddress = address;
        ipFamily = family;

        if(family == IPFamily.IPv4)
            prefixLength = 32;
        else
            prefixLength = 128;
    }

    public IPAddress(String s)
    {
        fromString(s);
    }

    private void init()
    {
        ipAddress = "";
        ipFamily = IPFamily.UNDEFINED;
        prefixLength = -1;
    }

    public void setIpAddress(String s)
    {
        ipAddress = s;
    }

    public String getIpAddress()
    {
        return ipAddress;
    }

    public void setIpFamily(IPFamily f)
    {
        ipFamily = f;
    }

    public IPFamily getIpFamily()
    {
        return ipFamily;
    }

    public void setPrefixLength(int p)
    {
        prefixLength = p;
    }

    public int getPrefixLength()
    {
        return prefixLength;
    }

    public void fromString(String s)
    {
        List<String> item = Arrays.asList(s.split(";"));

        if(item.size() != 3)
        {
            init();

            return;
        }

        ipAddress = item.get(0);
        ipFamily = IPFamily.fromString(item.get(1));

        try
        {
            prefixLength = Integer.parseInt(item.get(2));
        }
        catch(Exception e)
        {
            if(ipFamily == IPFamily.IPv4)
                prefixLength = 32;
            else
                prefixLength = 128;
        }
    }

    public String toString()
    {
        return ipAddress + ";" + ipFamily.toString() + ";" + String.format(Locale.getDefault(), "%d", prefixLength);
    }
}
