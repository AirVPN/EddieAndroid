// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 5 September 2018 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import java.util.ArrayList;

public class WebViewerActivity extends AppCompatActivity
{
    private int iconResource = 0;
    private String title = "";
    private String uri = "";
    private String html = "";
    private ArrayList<Integer> manifestMessageId = null;

    private LinearLayout llMainLayout = null;
    private LinearLayout llDoNotShowAgainLayout = null;

    private ImageView imgIcon = null;
    private TextView txtTitle = null;
    private Button btnBack = null;
    private CheckBox chkDoNotShowAgain = null;
    private WebView webView = null;

    private Activity thisActivity = this;
    private SupportTools supportTools = null;
    private SettingsManager settingsManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        supportTools = EddieApplication.supportTools();
        settingsManager = EddieApplication.settingsManager();

        supportTools.setLocale(getBaseContext());

        setContentView(R.layout.webviewer_activity_layout);

        Bundle extras = getIntent().getExtras();

        iconResource = extras.getInt("icon");
        title = extras.getString("title");
        uri = extras.getString("uri");
        html = extras.getString("html");
        manifestMessageId = extras.getIntegerArrayList("manifestMessageID");

        llMainLayout = (LinearLayout)findViewById(R.id.main_layout);
        llDoNotShowAgainLayout = (LinearLayout)findViewById(R.id.do_not_show_again_bar);

        imgIcon = (ImageView)findViewById(R.id.icon);
        txtTitle = (TextView)findViewById(R.id.title);
        btnBack = (Button)findViewById(R.id.btn_back);
        chkDoNotShowAgain = (CheckBox)findViewById(R.id.chk_do_not_show_again);

        if(iconResource > 0)
            imgIcon.setImageDrawable(getDrawable(iconResource));

        txtTitle.setText(title);

        btnBack.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(webView != null)
                {
                    if(webView.canGoBack())
                        webView.goBack();
                    else
                    {
                        checkDoNotShowAgainManifestMessages();

                        supportTools.setLocale(getBaseContext());

                        WebViewerActivity.this.finish();
                    }
                }
            }
        });

        btnBack.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                if(!btnBack.isEnabled())
                    return;

                if(hasFocus)
                    btnBack.setBackgroundResource(R.drawable.arrow_back_focus);
                else
                    btnBack.setBackgroundResource(R.drawable.arrow_back);
            }
        });

        btnBack.setAccessibilityDelegate(new View.AccessibilityDelegate()
        {
            @Override
            public void sendAccessibilityEvent(View host, int eventType)
            {
                super.sendAccessibilityEvent(host, eventType);

                btnBack.setContentDescription(getString(R.string.accessibility_go_back));
            }
        });

        webView = (WebView)findViewById(R.id.webview);

        webView.setClickable(true);
        webView.getSettings().setJavaScriptEnabled(true);

        CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true);

        webView.setWebViewClient(new EddieWebViewClient(true));

        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);

        if(manifestMessageId != null && manifestMessageId.isEmpty() == false)
            llDoNotShowAgainLayout.setVisibility(View.VISIBLE);
        else
            llDoNotShowAgainLayout.setVisibility(View.GONE);

        if(uri != null & !uri.isEmpty())
            webView.loadUrl(uri);
        else if(html != null & !html.isEmpty())
            webView.loadDataWithBaseURL(null, html, "text/html", "UTF-8", null);
        else
        {
            checkDoNotShowAgainManifestMessages();

            supportTools.setLocale(getBaseContext());

            this.finish();
        }

        OnBackPressedCallback onBackPressedCallback = new OnBackPressedCallback(true)
        {
            @Override
            public void handleOnBackPressed()
            {
                checkDoNotShowAgainManifestMessages();

                supportTools.setLocale(getBaseContext());

                thisActivity.finish();
            }
        };

        getOnBackPressedDispatcher().addCallback(this, onBackPressedCallback);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        webView.saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);

        if(savedInstanceState != null)
            webView.restoreState(savedInstanceState);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState)
    {
        super.onPostCreate(savedInstanceState);

        supportTools.setLocale(getBaseContext());

        chkDoNotShowAgain.setText(EddieApplication.applicationContext().getString(R.string.do_not_show_again));
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        llMainLayout.removeView(webView);

        llMainLayout.addView(webView);

        switch(newConfig.uiMode & Configuration.UI_MODE_NIGHT_MASK)
        {
            case Configuration.UI_MODE_NIGHT_YES:
            {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            }
            break;

            case Configuration.UI_MODE_NIGHT_NO:
            {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            }
            break;

            default:
            {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
            }
            break;
        }

        supportTools.setLocale(getBaseContext());

        recreate();

        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onStart()
    {
        super.onStart();

        if(settingsManager.getSystemApplicationTheme().equals(SettingsManager.SYSTEM_OPTION_APPLICATION_THEME_SYSTEM) == false)
        {
            switch(settingsManager.getSystemApplicationTheme())
            {
                case "Dark":
                {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                }
                break;

                case "Light":
                {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                }
                break;

                default:
                {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
                }
                break;
            }
        }
    }

    @Override
    public void onStop()
    {
        super.onStop();

        supportTools.setLocale(getBaseContext());
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        supportTools.setLocale(getBaseContext());
    }

    private void checkDoNotShowAgainManifestMessages()
    {
        if(chkDoNotShowAgain.isChecked() == true && manifestMessageId != null && manifestMessageId.isEmpty() == false)
        {
            AirVPNManifest airVPNManifest = EddieApplication.airVPNManifest();
            ArrayList<AirVPNManifest.Message> message = airVPNManifest.getMessages();

            for(Integer id : manifestMessageId)
                message.get(id).setDoNotShowAgain(true);
        }
    }

    private class EddieWebViewClient extends WebViewClient
    {
        private boolean openLinkInExternalBrowser = true;

        public EddieWebViewClient(boolean useExternalBrowser)
        {
            openLinkInExternalBrowser = useExternalBrowser;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            if(openLinkInExternalBrowser == false)
                return false;

            if(url != null && (url.startsWith("http://") || url.startsWith("https://")))
            {
                Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));

                if(webIntent != null)
                {
                    try
                    {
                        view.getContext().startActivity(webIntent);

                        return true;
                    }
                    catch(ActivityNotFoundException e)
                    {
                        EddieLogger.error("WebViewerActivity.shouldOverrideUrlLoading(): No helper app found to open external link.");

                        supportTools.infoDialog(thisActivity, EddieApplication.context().getString(R.string.cannot_open_web_browser), true);

                        return true;
                    }
                }
                else
                    return false;
            }
            else
                return false;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon)
        {
            super.onPageStarted(view, url, favicon);

            if(url != null && (url.startsWith("http://") || url.startsWith("https://")))
                supportTools.showProgressDialog(thisActivity, getResources().getString(R.string.loading_page_wait));
        }

        @Override
        public void onPageFinished(WebView view, String url)
        {
            super.onPageFinished(view, url);

            if(url != null && (url.startsWith("http://") || url.startsWith("https://")))
                supportTools.dismissProgressDialog();
        }
    }
}
