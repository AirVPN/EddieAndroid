// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 20 June 2018 - author: ProMIND - initial release. Based on revised code from com.eddie.android. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.res.ResourcesCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PackageChooserActivity extends AppCompatActivity
{
    public static final String PARAM_PACKAGES = "PACKAGES";
    public static final String CHOOSER_TITLE = "TITLE";

    private Activity thisActivity = this;
    private SupportTools supportTools = null;
    private SettingsManager settingsManager = null;

    private ArrayList<ApplicationItem> applicationList = new ArrayList<ApplicationItem>();
    private ApplicationListAdapter applicationListAdapter = null;
    private PackageManager packageManager = null;
    private Typeface typeface = null;

    private ListView applicationListView = null;
    private TextView txtTitle = null;
    private LinearLayout ppProgressSpinner = null;

    private String title = "", paramPackages = "";

    private class ApplicationItem
    {
        private ApplicationInfo applicationInfo = null;
        private boolean selected = false;
        private Drawable icon = null;
        private String name = "";
        private String description = "";

        public ApplicationItem(ApplicationInfo info)
        {
            applicationInfo = info;

            name = info.packageName;
            description = packageManager.getApplicationLabel(info).toString();
            selected = false;
            icon = info.loadIcon(packageManager);
        }

        public ApplicationInfo getInfo()
        {
            return applicationInfo;
        }

        public void setInfo(ApplicationInfo a)
        {
            applicationInfo = a;
        }

        public boolean isSelected()
        {
            return selected;
        }

        public void setSelected(boolean s)
        {
            selected = s;
        }

        public Drawable getIcon()
        {
            return icon;
        }

        public void setIcon(Drawable i)
        {
            icon = i;
        }

        public String getName()
        {
            return name;
        }

        public void setName(String s)
        {
            name = s;
        }

        public String getDescription()
        {
            return description;
        }

        public void setDescription(String s)
        {
            description = s;
        }
    }

    private class ApplicationListAdapter extends BaseAdapter
    {
        private Activity activity = null;
        private ArrayList<ApplicationItem> appItems = null;

        private class ListViewHolder
        {
            public ImageView itemIcon;
            public CheckBox itemCheckbox;
            public TextView itemName;
            public TextView itemDescription;
        }

        public ApplicationListAdapter(Activity a, ArrayList<ApplicationItem> items)
        {
            activity = a;

            appItems = items;
        }

        public void dataSet(ArrayList<ApplicationItem> appList)
        {
            appItems = appList;

            notifyDataSetChanged();
        }

        @Override
        public int getCount()
        {
            int entries = 0;

            if(appItems != null)
                entries = appItems.size();

            return entries;
        }

        @Override
        public ApplicationItem getItem(int position)
        {
                return appItems.get(position);
        }

        @Override
        public long getItemId(int position)
        {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            ApplicationItem item = appItems.get(position);

            ListViewHolder listViewHolder = null;

            if(convertView != null)
                listViewHolder = (ListViewHolder)convertView.getTag();

            if(listViewHolder == null)
            {
                listViewHolder = new ListViewHolder();

                convertView = activity.getLayoutInflater().inflate(R.layout.package_chooser_item, null);

                listViewHolder.itemIcon = (ImageView)convertView.findViewById(R.id.packages_picker_item_icon);
                listViewHolder.itemCheckbox = (CheckBox)convertView.findViewById(R.id.packages_picker_item_selection);
                listViewHolder.itemDescription = (TextView)convertView.findViewById(R.id.packages_picker_item_description);
                listViewHolder.itemName = (TextView)convertView.findViewById(R.id.packages_picker_item_name);

                convertView.setTag(listViewHolder);
            }

            listViewHolder.itemIcon.setImageDrawable(item.getIcon());

            listViewHolder.itemCheckbox.setChecked(item.isSelected());
            listViewHolder.itemCheckbox.setClickable(false);
            listViewHolder.itemCheckbox.setSelected(false);

            listViewHolder.itemDescription.setText(item.getDescription());
            listViewHolder.itemDescription.setTypeface(typeface);

            listViewHolder.itemName.setText(item.getName());
            listViewHolder.itemName.setTypeface(typeface);

            return convertView;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        supportTools = EddieApplication.supportTools();
        settingsManager = EddieApplication.settingsManager();

        Bundle bundleExtras = getIntent().getExtras();

        supportTools.setLocale(getBaseContext());

        title = bundleExtras.getString(CHOOSER_TITLE);
        paramPackages = bundleExtras.getString(PARAM_PACKAGES);

        typeface = ResourcesCompat.getFont(this, R.font.default_font);

        packageManager = getApplicationContext().getPackageManager();

        initUI();

        OnBackPressedCallback onBackPressedCallback = new OnBackPressedCallback(true)
        {
            @Override
            public void handleOnBackPressed()
            {
                Intent resultIntent = new Intent(thisActivity, PackageChooserActivity.class);
                resultIntent.putExtra(PARAM_PACKAGES, getSelectedApplicationList());

                setResult(RESULT_OK, resultIntent);

                supportTools.setLocale(getBaseContext());

                thisActivity.finish();
            }
        };

        getOnBackPressedDispatcher().addCallback(this, onBackPressedCallback);
    }

    @Override
    public void onStart()
    {
        super.onStart();

        if(settingsManager.getSystemApplicationTheme().equals(SettingsManager.SYSTEM_OPTION_APPLICATION_THEME_SYSTEM) == false)
        {
            switch(settingsManager.getSystemApplicationTheme())
            {
                case "Dark":
                {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                }
                break;

                case "Light":
                {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                }
                break;

                default:
                {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
                }
                break;
            }
        }

        new ApplicationListLoader().execute();
    }

    @Override
    public void onStop()
    {
        super.onStop();

        supportTools.setLocale(getBaseContext());
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        supportTools.setLocale(getBaseContext());
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        switch(newConfig.uiMode & Configuration.UI_MODE_NIGHT_MASK)
        {
            case Configuration.UI_MODE_NIGHT_YES:
            {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            }
            break;

            case Configuration.UI_MODE_NIGHT_NO:
            {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            }
            break;

            default:
            {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
            }
            break;
        }

        supportTools.setLocale(getBaseContext());

        recreate();

        super.onConfigurationChanged(newConfig);
    }

    private void initUI()
    {
        setContentView(R.layout.package_chooser_activity_layout);

        txtTitle = (TextView)findViewById(R.id.chooser_title);
        applicationListView = (ListView)findViewById(R.id.packages_picker_applications);
        ppProgressSpinner = (LinearLayout)findViewById(R.id.ppProgressSpinner);

        txtTitle.setText(title);
        txtTitle.setTypeface(typeface);

        applicationListAdapter = new ApplicationListAdapter(this, applicationList);

        applicationListView.setAdapter(applicationListAdapter);

        applicationListAdapter.notifyDataSetChanged();

        applicationListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
           @Override
           public void onItemClick(AdapterView<?> parent, View view, int position, long id)
           {
               applicationList.get(position).setSelected(!applicationList.get(position).isSelected());

               if(applicationListAdapter != null)
                   applicationListAdapter.notifyDataSetChanged();
           }
        });
    }

    private String getSelectedApplicationList()
    {
        String selectedApplications = "";

        for(ApplicationItem app : applicationList)
        {
            if(app.isSelected())
            {
                if(selectedApplications.length() > 0)
                    selectedApplications += SettingsManager.DEFAULT_SPLIT_SEPARATOR;

                selectedApplications += app.getName();
            }
        }

        return selectedApplications;
    }

    private void loadApplications(String appList)
    {
        ArrayList<String> selectedPackages = new ArrayList<String>();
        String eddiePackage = getApplicationContext().getPackageName();

        if(appList != null)
        {
            String[] valArray = appList.split(SettingsManager.DEFAULT_SPLIT_SEPARATOR);

            for(String item : valArray)
                selectedPackages.add(item);
        }

        applicationList.clear();

        List<ApplicationInfo> applications = packageManager.getInstalledApplications(0);

        for(ApplicationInfo app : applications)
        {
            if(!app.packageName.equals(eddiePackage))
            {
                ApplicationItem item = new ApplicationItem(app);

                item.setSelected(selectedPackages.contains(app.packageName));

                applicationList.add(item);
            }
        }

         Collections.sort(applicationList, new Comparator<ApplicationItem>()
         {
             public int compare(ApplicationItem a, ApplicationItem b)
             {
                 return a.getDescription().compareToIgnoreCase(b.getDescription());
             }
         });
    }

    private class ApplicationListLoader extends AsyncTask<Void, Void, Void>
    {
        @Override
        protected void onPreExecute()
        {
            ppProgressSpinner.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... p)
        {
            Runnable runnable = new Runnable()
            {
                @Override
                public void run()
                {
                    loadApplications(paramPackages);

                    applicationListAdapter.dataSet(applicationList);

                    ppProgressSpinner.setVisibility(View.GONE);
                }
            };

            SupportTools.runOnUiActivity(PackageChooserActivity.this, runnable);

            return null;
        }

        @Override
        protected void onPostExecute(Void param)
        {
        }
    }
}
