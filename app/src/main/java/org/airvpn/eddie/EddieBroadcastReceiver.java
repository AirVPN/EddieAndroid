// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 20 June 2018 - author: ProMIND - initial release. Based on revised code from com.eddie.android. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import java.util.Locale;

public class EddieBroadcastReceiver extends BroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent)
    {
        String action = intent.getAction();
        SettingsManager settingsManager = EddieApplication.settingsManager();

        if(action.equals(Intent.ACTION_BOOT_COMPLETED) || action.equals(Intent.ACTION_MY_PACKAGE_REPLACED))
        {
            if(Build.VERSION.SDK_INT < Build.VERSION_CODES.Q && settingsManager.isAlwaysOnVpnSet() == false)
            {
                if(EddieApplication.vpnManager().isVpnConnectionStopped() == true)
                {
                    EddieLogger.info("Device startup completed. Check whether it is needed to start a VPN startup connection.");

                    Intent bootVPNActivityIntent = new Intent(context, BootVPNActivity.class);

                    bootVPNActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    context.startActivity(bootVPNActivityIntent);
                }
            }
        }
        else
            EddieLogger.error(String.format(Locale.getDefault(), "EddieBroadcastReceiver.onReceive(): Received unhandled action '%s'", action));
    }
}
