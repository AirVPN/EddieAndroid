// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>

package org.airvpn.eddie;

public class WireGuardClient
{
    private int tunnelHandle = -1;
    private int mtu = -1;

    public WireGuardClient()
    {
        tunnelHandle = -1;
    }

    public VPNTransportStats getTransportStats() throws Exception
    {
        String row[];

        if(tunnelHandle == -1)
            throw new Exception("WireGuardClient.getTransportStats(): tunnel is not started.");

        VPNTransportStats vpnTransportStats = new VPNTransportStats();

        String config = EddieLibrary.wireGuardGetConfig(tunnelHandle);

        vpnTransportStats.resultCode = EddieLibraryResult.SUCCESS;

        for(final String line : config.split("\\n"))
        {
            row = line.split("=");

            switch(row[0])
            {
                case "private_key":
                {
                    if(row.length > 1)
                        vpnTransportStats.privateKey = row[1];
                }
                break;

                case "public_key":
                {
                    if(row.length > 1)
                        vpnTransportStats.publicKey = row[1];
                }
                break;

                case "preshared_key":
                {
                    if(row.length > 1)
                        vpnTransportStats.presharedKey = row[1];
                }
                break;

                case "endpoint":
                {
                    if(row.length > 1)
                    {
                        if(!vpnTransportStats.endPoint.isEmpty())
                            vpnTransportStats.endPoint += ", ";

                        vpnTransportStats.endPoint += row[1];
                    }
                }
                break;

                case "allowed_ip":
                {
                    if(row.length > 1)
                    {
                        if(!vpnTransportStats.allowedIp.isEmpty())
                            vpnTransportStats.allowedIp += ", ";

                        vpnTransportStats.allowedIp += row[1];
                    }
                }
                break;

                case "rx_bytes":
                {
                    try
                    {
                        vpnTransportStats.bytesIn = Long.parseLong(row[1]);

                        if(mtu > 0)
                            vpnTransportStats.packetsIn = vpnTransportStats.bytesIn / mtu;
                    }
                    catch(final Exception ignored)
                    {
                        vpnTransportStats.bytesIn = -1;
                    }
                }
                break;

                case "tx_bytes":
                {
                    try
                    {
                        vpnTransportStats.bytesOut = Long.parseLong(row[1]);

                        if(mtu > 0)
                            vpnTransportStats.packetsOut = vpnTransportStats.bytesOut / mtu;
                    }
                    catch(final Exception ignored)
                    {
                        vpnTransportStats.bytesOut = -1;
                    }
                }
                break;

                case "listen_port":
                {
                    try
                    {
                        vpnTransportStats.listenPort = Integer.parseInt(row[1]);
                    }
                    catch(final Exception ignored)
                    {
                        vpnTransportStats.listenPort = -1;
                    }
                }
                break;

                case "protocol_version":
                {
                    try
                    {
                        vpnTransportStats.protocolVersion = Integer.parseInt(row[1]);
                    }
                    catch(final Exception ignored)
                    {
                        vpnTransportStats.protocolVersion = -1;
                    }
                }
                break;

                case "persistent_keepalive_interval":
                {
                    try
                    {
                        vpnTransportStats.persistentKeepaliveInterval = Integer.parseInt(row[1]);
                    }
                    catch(final Exception ignored)
                    {
                        vpnTransportStats.persistentKeepaliveInterval = -1;
                    }
                }
                break;

                case "last_handshake_time_sec":
                {
                    try
                    {
                        vpnTransportStats.lastHandshakeTimeSec = Long.parseLong(row[1]);
                    }
                    catch(final Exception ignored)
                    {
                        vpnTransportStats.lastHandshakeTimeSec = -1;
                    }
                }
                break;

                case "last_handshake_time_nsec":
                {
                    try
                    {
                        vpnTransportStats.lastHandshakeTimeNsec = Long.parseLong(row[1]);
                    }
                    catch(final Exception ignored)
                    {
                        vpnTransportStats.lastHandshakeTimeNsec = -1;
                    }
                }
                break;
            }
        }

        return vpnTransportStats;
    }

    public int start(String interfaceName, int tunFd, String settings) throws Exception
    {
        return start(interfaceName, tunFd, settings, -1);
    }

    public int start(String interfaceName, int tunFd, String settings, int m) throws Exception
    {
        if(tunnelHandle != -1)
            throw new Exception("WireGuardClient.stop(): tunnel has already started.");

        tunnelHandle = EddieLibrary.wireGuardTurnOn(interfaceName, tunFd, settings);

        mtu = m;

        return tunnelHandle;
    }

    public void stop() throws Exception
    {
        if(tunnelHandle == -1)
            throw new Exception("WireGuardClient.stop(): tunnel is not started.");

        EddieLibrary.wireGuardTurnOff(tunnelHandle);

        tunnelHandle = -1;
    }
}
