// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>

package org.airvpn.eddie;

import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Stack;

abstract public class VPNTunnel implements VPNTunnelInterface
{
    public enum Action
    {
        SYSTEM_PAUSE,
        USER_PAUSE,
        SYSTEM_RESUME,
        USER_RESUME,
        LOCK,
        NETWORK_TYPE_CHANGED
    }

    public enum Type
    {
        UNDEFINED,
        OPENVPN,
        WIREGUARD
    }

    protected VPNService vpnService = null;
    protected EddieEvent eddieEvent = null;
    protected VPNManager vpnManager = null;
    protected Stack<VPNContext> vpnContext = new Stack<VPNContext>();
    protected SettingsManager settingsManager = null;

    protected Type tunnelType = Type.UNDEFINED;

    protected boolean isVPNLockEnabled = true;

    protected static final String vpnLockDisabledLogWarning = "VPN Lock has been disabled. Best effort leaks prevention explicitly turned off by user. Eddie will ignore this error.";

    public VPNTunnel(VPNService service, Type type)
    {
        vpnService = service;

        settingsManager = EddieApplication.settingsManager();

        eddieEvent = EddieApplication.eddieEvent();

        vpnManager = EddieApplication.vpnManager();

        tunnelType = type;

        isVPNLockEnabled = settingsManager.isVpnLockEnabled();
    }

    public synchronized VPNContext getActiveContext() throws Exception
    {
        VPNContext context = null;

        try
        {
            context = vpnContext.peek();

            if(context == null)
                throw new Exception("internal error (cannot get a valid VPN context)");
        }
        catch(EmptyStackException e)
        {
            context = null;
        }

        return context;
    }

    protected synchronized void clearContexts()
    {
        EddieLogger.debug("VPNTunnel.clearContexts(): Clearing VPN contexts");

        while(vpnContext.size() > 0)
            vpnContext.pop();
    }

    public boolean loadProfileFile(String filename) throws Exception
    {
        boolean result = false;

        try
        {
            result = loadProfile(filename, false);
        }
        catch(Exception e)
        {
            EddieLogger.error("VPNTunnel.loadProfileFile() Exception: %s", e.getMessage());

            throw e;
        }

        return result;
    }

    public boolean loadProfileString(String profile) throws Exception
    {
        boolean result = false;

        try
        {
            result = loadProfile(profile, true);
        }
        catch(Exception e)
        {
            EddieLogger.error("VPNTunnel.loadProfileString() Exception: %s", e.getMessage());

            throw e;
        }

        return result;
    }

    protected void alertNotification(String message)
    {
        if(message.equals(""))
            return;

        vpnService.alertNotification(message);
    }

    protected void updateNotification(VPN.Status status)
    {
        String text, serverDescription = "";
        VPNTransportStats vpnStats = null;

        if(status != VPN.Status.CONNECTED && status != VPN.Status.PAUSED_BY_USER && status != VPN.Status.PAUSED_BY_SYSTEM && status != VPN.Status.LOCKED)
            return;

        HashMap<String, String> profileInfo = vpnManager.vpn().getProfileInfo();

        if(profileInfo != null)
        {
            if(vpnManager.vpn().getConnectionMode() == VPN.ConnectionMode.AIRVPN_SERVER || vpnManager.vpn().getConnectionMode() == VPN.ConnectionMode.QUICK_CONNECT)
            {
                if(!profileInfo.get("server").isEmpty())
                    serverDescription = vpnManager.vpn().getServerDescription();
                else
                    serverDescription = EddieApplication.context().getResources().getString(R.string.vpn_status_connected);
            }
            else
            {
                if(!profileInfo.get("description").isEmpty())
                    serverDescription = profileInfo.get("description");
                else
                    serverDescription = profileInfo.get("server");
            }

            serverDescription += " (" + profileInfo.get("vpn_type") + ")";
        }

        text = "";

        if(status == VPN.Status.PAUSED_BY_USER || status == VPN.Status.PAUSED_BY_SYSTEM)
            text += "(" + vpnService.getResources().getString(R.string.vpn_status_paused) + ") ";

        if(status == VPN.Status.LOCKED)
            text += "(" + vpnService.getResources().getString(R.string.vpn_status_locked) + ") ";

        text += String.format(Locale.getDefault(), vpnService.getResources().getString(R.string.connected_to_server), serverDescription);

        if(!NetworkStatusReceiver.getNetworkDescription().equals(""))
            text += " " + String.format(Locale.getDefault(), vpnService.getResources().getString(R.string.network_info), NetworkStatusReceiver.getNetworkDescription());

        if(vpnManager.isGpsSpoofingEnabled() == true)
        {
            text += " (" + EddieApplication.applicationContext().getResources().getString(R.string.conn_gps_spoofing_cap);
            text += " " + CountryContinent.getCountryName(vpnManager.getGpsSpoofingCoordinates().getCountryCode()) + ")";
        }

        vpnStats = vpnManager.vpn().getVpnTransportStats();

        if(vpnStats != null && status == VPN.Status.CONNECTED)
        {
            text += System.getProperty("line.separator") + System.getProperty("line.separator");

            text += String.format(Locale.getDefault(), "%s %s - %s" + System.getProperty("line.separator"), vpnService.getResources().getString(R.string.stats_in_cap), EddieApplication.supportTools().formatDataVolume(vpnStats.bytesIn), EddieApplication.supportTools().formatTransferRate(vpnManager.vpn().getInRate()));

            text += String.format(Locale.getDefault(), "%s %s - %s" + System.getProperty("line.separator"), vpnService.getResources().getString(R.string.stats_out_cap), EddieApplication.supportTools().formatDataVolume(vpnStats.bytesOut), EddieApplication.supportTools().formatTransferRate(vpnManager.vpn().getOutRate()));

            text += String.format(Locale.getDefault(), "%s %s",vpnService.getResources().getString(R.string.stats_session_time_cap), vpnManager.vpn().getFormattedSessionTime());
        }

        vpnService.updateNotification(text, false);
    }

    public VPNService getService()
    {
        return vpnService;
    }

    public Type getType()
    {
        return tunnelType;
    }

    abstract public void init() throws Exception;

    abstract public void run();

    abstract public void cleanup() throws Exception;

    abstract public VPN.Status handleScreenChanged(boolean active);

    abstract public void networkStatusChanged(Action action);

    abstract protected boolean loadProfile(String profile, boolean isString) throws Exception;

    abstract public void bindOptions() throws Exception;

    abstract public VPNTransportStats getTransportStats() throws Exception;
}
