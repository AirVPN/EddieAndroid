// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>

package org.airvpn.eddie;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CipherDatabase
{
    public final int CIPHER_NOT_FOUND = -1;
    public final int INVALID_CIPHER = -2;
    public final int CIPHERDB_NOT_INITIALIZED = -2;

    public enum CipherType
    {
        TLS,
        TLS_SUITE,
        DATA
    };

    private static HashMap<String, Integer> tlsCipherDB = null;
    private static HashMap<String, Integer> tlsSuiteCipherDB = null;
    private static HashMap<String, Integer> dataCipherDB = null;
    private static int lastCode = 0;

    CipherDatabase()
    {
        if(tlsCipherDB == null)
            tlsCipherDB = new HashMap<String, Integer>();

        if(tlsSuiteCipherDB == null)
            tlsSuiteCipherDB = new HashMap<String, Integer>();

        if(dataCipherDB == null)
            dataCipherDB = new HashMap<String, Integer>();
    }

    public void reset()
    {
        tlsCipherDB.clear();
        tlsSuiteCipherDB.clear();
        dataCipherDB.clear();

        lastCode = 0;
    }

    public void addCipherString(String ciphers, CipherType type)
    {
        String[] cipher = ciphers.split(":");

        for(String c : cipher)
            addCipher(c, type);
    }

    public int addCipher(String cipher, CipherType type)
    {
        int code = CIPHER_NOT_FOUND;
        HashMap<String, Integer> cipherDB = getCipherDB(type);

        if(cipherDB == null)
            return CIPHERDB_NOT_INITIALIZED;

        if(cipher.isEmpty())
            return INVALID_CIPHER;

        if(cipherDB.containsKey(cipher))
        {
            code = cipherDB.get(cipher);
        }
        else
        {
            lastCode++;

            code = lastCode;

            cipherDB.put(cipher, code);
        }

        return code;
    }

    public int getCode(String cipher, CipherType type)
    {
        int code = CIPHER_NOT_FOUND;
        HashMap<String, Integer> cipherDB = getCipherDB(type);

        if(cipherDB.containsKey(cipher))
            code = cipherDB.get(cipher);

        return code;
    }

    public ArrayList<Integer> getCodeArrayList(String ciphers, CipherType type)
    {
        int code;
        ArrayList<Integer> cipherList = new ArrayList<Integer>();

        String[] cipher = ciphers.split(":");

        for(String c : cipher)
        {
            if(!c.isEmpty())
            {
                code = getCode(c, type);

                if(code != CIPHER_NOT_FOUND)
                    cipherList.add(code);
                else
                {
                    code = addCipher(c, type);

                    if(code != CIPHER_NOT_FOUND)
                        cipherList.add(code);
                }
            }
        }

        return cipherList;
    }

    public String getMatchingCiphers(String pattern, CipherType type)
    {
        String ciphers = "";
        HashMap<String, Integer> cipherDB = getCipherDB(type);

        for(Map.Entry<String, Integer> map : cipherDB.entrySet())
        {
            if(map.getKey().contains(pattern))
            {
                if(!ciphers.isEmpty())
                    ciphers += ":";

                ciphers += map.getKey();
            }
        }

        return ciphers;
    }

    public ArrayList<Integer> getMatchingCiphersArrayList(String pattern, CipherType type)
    {
        ArrayList<Integer> cipherList = new ArrayList<Integer>();
        HashMap<String, Integer> cipherDB = getCipherDB(type);

        for(Map.Entry<String, Integer> map : cipherDB.entrySet())
        {
            if(map.getKey().contains(pattern))
                cipherList.add(map.getValue());
        }

        return cipherList;
    }

    public String getCipher(int code, CipherType type)
    {
        int val;
        String cipher = "";
        HashMap<String, Integer> cipherDB = getCipherDB(type);

        for(Map.Entry<String, Integer> map : cipherDB.entrySet())
        {
            val = map.getValue();

            if(code == val)
                cipher = map.getKey();
        }

        return cipher;
    }

    private HashMap<String, Integer> getCipherDB(CipherType type)
    {
        HashMap<String, Integer> cipherDB = null;

        switch (type)
        {
            case TLS:
            {
                cipherDB = tlsCipherDB;
            }
            break;

            case TLS_SUITE:
            {
                cipherDB = tlsSuiteCipherDB;
            }
            break;

            case DATA:
            {
                cipherDB = dataCipherDB;
            }
            break;

            default:
            {
                cipherDB = dataCipherDB;
            }
            break;
        }

        return cipherDB;
    }
}
