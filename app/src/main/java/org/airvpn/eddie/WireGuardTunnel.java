// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2024 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>

package org.airvpn.eddie;

import android.os.ParcelFileDescriptor;
import android.util.Base64;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class WireGuardTunnel extends VPNTunnel
{
    private enum Status
    {
        STOPPED,
        RUNNING,
        HOLD,
        WAIT
    };

    private enum ConfigSection
    {
        UNDEFINED,
        INTERFACE,
        PEER
    };

    private WireGuardClient wireGuardClient = null;

    private String wireGuardProfile = "";
    private final String interfaceName = "wg0";

    private boolean ipv6 = false, excludeLocalNetwork = false;
    private int tunnelHandle = -1;
    private final long handshakingUpdateWaitSeconds = SettingsManager.WIREGUARD_HANDSHAKING_UPDATE_WAIT_SECONDS;
    private final long handshakingTimeoutSeconds = SettingsManager.WIREGUARD_HANDSHAKING_TIMEOUT_SECONDS;
    private String privateKey, publicKey, presharedKey;
    private int endPointPort, persistentKeepalive, customMTU;
    private ArrayList<IPAddress> dnsAddress = null;
    private ArrayList<IPAddress> ipAddress = null;
    private ArrayList<IPAddress> allowedIpAddress = null;
    private IPAddress endPoint;
    private VPNEvent vpnEvent = null;
    private InetAddress inetAddress = null;
    private Status status = Status.STOPPED;
    private long holdTS = 0, waitTS = 0;
    private boolean isWireGuardHandshakingTimeoutIgnored = false;
    public WireGuardTunnel(VPNService service)
    {
        super(service, Type.WIREGUARD);
    }

    public void init() throws Exception
    {
        VPNContext newVpnContext = null;

        status = Status.STOPPED;

        if(wireGuardClient != null)
            throw new Exception("WireGuardTunnel.init(): Client already initialized");

        wireGuardClient = new WireGuardClient();

        if(wireGuardClient == null)
            throw new Exception("WireGuardTunnel.init(): Failed to create a new WireGuard client");

        ipAddress = new ArrayList<IPAddress>();
        dnsAddress = new ArrayList<IPAddress>();
        allowedIpAddress = new ArrayList<IPAddress>();

        customMTU = 0;

        isWireGuardHandshakingTimeoutIgnored = settingsManager.isWireGuardHandshakingTimeoutIgnored();

        EddieLogger.info("WireGuardTunnel: successfully created a new WireGuard client");

        try
        {
            newVpnContext = new VPNContext(vpnService);

            if(newVpnContext != null)
                vpnContext.push(newVpnContext);
            else
            {
                EddieLogger.error("WireGuardTunnel.init():  cannot create a new VPNContext.");

                return;
            }
        }
        catch(Exception e)
        {
            EddieLogger.error("WireGuardTunnel.init(): %s", e);

            return;
        }

        if(settingsManager.areLocalNetworksExcluded() && NetworkStatusReceiver.getNetworkType() != NetworkStatusReceiver.NetworkType.MOBILE)
            excludeLocalNetwork = true;
        else
            excludeLocalNetwork = false;
    }

    public void run()
    {
        ParcelFileDescriptor fileDescriptor = null;
        VPNContext vpnContext = null;
        String serverInfo = "";
        HashMap<String, String> currentProfile = vpnManager.vpn().getProfileInfo();
        VPNConnectionStats connectionStats;
        boolean includeAddress = true;

        if(wireGuardClient == null)
        {
            EddieLogger.error("WireGuardTunnel.run(): WireGuard client is not initialized");

            vpnService.handleConnectionError();

            return;
        }

        if(isWireGuardHandshakingTimeoutIgnored == true)
            EddieLogger.warning("WireGuard Handshaking Timeout will be ignored");

        try
        {
            vpnContext = getActiveContext();

            if(vpnContext == null)
            {
                EddieLogger.error("WireGuardTunnel.run(): vpnContext is null");

                vpnService.handleConnectionError();

                return;
            }

            ipv6 = false;

            for(IPAddress address : ipAddress)
            {
                EddieLogger.info("Adding server %s address %s/%d", address.getIpFamily().toString(), address.getIpAddress(), address.getPrefixLength());

                vpnContext.getBuilder().addAddress(address.getIpAddress(), address.getPrefixLength());

                if(address.getIpFamily() == IPAddress.IPFamily.IPv6)
                    ipv6 = true;

                allowedIpAddress.add(new IPAddress(address.getIpAddress(), address.getIpFamily(), address.getPrefixLength()));
            }

            for(IPAddress dns : dnsAddress)
            {
                EddieLogger.info("Adding DNS %s address %s", dns.getIpFamily().toString(), dns.getIpAddress());

                vpnContext.addDNSServer(dns.getIpAddress(), dns.getIpFamily() == IPAddress.IPFamily.IPv4 ? false : true);

                vpnManager.vpn().addDnsEntry(dns.getIpAddress(), dns.getIpFamily());

                if(dns.getIpFamily() == IPAddress.IPFamily.IPv6)
                    ipv6 = true;

                allowedIpAddress.add(new IPAddress(dns.getIpAddress(), dns.getIpFamily(), dns.getPrefixLength()));
            }

            if(excludeLocalNetwork == true)
                exemptPrivateNetworkFromRoute();

            for(IPAddress allowedIP : allowedIpAddress)
            {
                includeAddress = true;

                if(excludeLocalNetwork == true && isGlobalRoute(allowedIP) == true)
                    includeAddress = false;

                if(includeAddress == true)
                {
                    EddieLogger.info("Adding %s route %s/%d", allowedIP.getIpFamily().toString(), allowedIP.getIpAddress(), allowedIP.getPrefixLength());

                    vpnContext.getBuilder().addRoute(allowedIP.getIpAddress(), allowedIP.getPrefixLength());

                    if(allowedIP.getIpFamily() == IPAddress.IPFamily.IPv6)
                        ipv6 = true;
                }
            }

            fileDescriptor = vpnContext.establish();
        }
        catch(Exception e)
        {
            EddieLogger.error("WireGuardTunnel.run(): %s", e.getMessage());

            vpnService.handleConnectionError();

            return;
        }

        if(fileDescriptor == null)
        {
            EddieLogger.error("WireGuardTunnel.run(): VPNService.Builder.establish() failed");

            vpnService.handleConnectionError();

            return;
        }

        if(customMTU > 0)
        {
            EddieLogger.info("Setting MTU to custom value %d", customMTU);

            vpnContext.setMTU(customMTU);
        }

        EddieLogger.info("Starting VPN connection to server");

        vpnService.handleThreadStarted();

        try
        {
            tunnelHandle = wireGuardClient.start(interfaceName, fileDescriptor.detachFd(), getUserSpaceStringConfig(), customMTU);
        }
        catch(Exception e)
        {
            EddieLogger.error("WireGuardTunnel.run(): %s", e.getMessage());

            vpnService.handleConnectionError();

            return;
        }

        if(tunnelHandle < 0)
        {
            String errMsg = String.format(Locale.getDefault(), "WireGuardTunnel.run(): Failed to start WireGuard tunnel.");

            EddieLogger.error(errMsg);

            vpnService.handleConnectionError();

            return;
        }

        EddieLogger.info("Protecting IPv4 socket");

        vpnService.protect(EddieLibrary.wireGuardGetSocketV4(tunnelHandle));

        if(ipv6 == true)
        {
            EddieLogger.info("Protecting IPv6 socket");

            vpnService.protect(EddieLibrary.wireGuardGetSocketV6(tunnelHandle));
        }

        switch(vpnManager.vpn().getConnectionMode())
        {
            case AIRVPN_SERVER:
            case QUICK_CONNECT:
            {
                if(currentProfile != null)
                {
                    if(currentProfile.containsKey("description"))
                        serverInfo = String.format(" (AirVPN server %s) %s", currentProfile.get("description"), currentProfile.get("server"));
                    else
                        serverInfo = String.format(" (AirVPN server) %s", currentProfile.get("server"));
                }
                else
                    serverInfo = " (AirVPN server) UNKNOWN";
            }
            break;

            case OPENVPN_PROFILE:
            {
                serverInfo = String.format(" server %s (OpenVPN profile) %s", currentProfile.get("description"), currentProfile.get("server"));
            }
            break;

            case WIREGUARD_PROFILE:
            {
                serverInfo = String.format(" server %s (WireGuard profile) %s", currentProfile.get("description"), currentProfile.get("server"));
            }
            break;

            case BOOT_CONNECT:
            {
                serverInfo = String.format(" server %s (VPN startup connection) %s", currentProfile.get("description"), currentProfile.get("server"));
            }
            break;

            default:
            {
                serverInfo = "";
            }
            break;
        }

        serverInfo += " (" + currentProfile.get("vpn_type") + ")";

        String connectionMessage = String.format("CONNECTED to VPN%s" + System.getProperty("line.separator") + "Server Host: %s" + System.getProperty("line.separator") + "Server Port: %s", serverInfo, endPoint.getIpAddress(), endPointPort);

        for(IPAddress address : ipAddress)
            connectionMessage += String.format(System.getProperty("line.separator") + "%s address: %s/%d", address.getIpFamily().toString(), address.getIpAddress(), address.getPrefixLength());

        for(IPAddress dns : dnsAddress)
            connectionMessage += String.format(System.getProperty("line.separator") + "DNS %s: %s/%d", dns.getIpFamily().toString(), dns.getIpAddress(), dns.getPrefixLength());

        EddieLogger.info(connectionMessage);

        connectionStats = new VPNConnectionStats();

        connectionStats.defined = -1;
        connectionStats.user = "--";

        if(currentProfile != null)
            connectionStats.serverHost = currentProfile.containsKey("description") ? currentProfile.get("description") : currentProfile.get("server");
        else
            connectionStats.serverHost = endPoint.getIpAddress();

        connectionStats.serverPort = String.format("%d", endPointPort);
        connectionStats.serverProto = endPoint.getIpFamily() == IPAddress.IPFamily.IPv4 ? "UDPv4" : "UDPv6";
        connectionStats.serverIp = endPoint.getIpAddress();

        connectionStats.vpnIp4 = "";
        connectionStats.vpnIp6 = "";

        for(IPAddress address : ipAddress)
        {
            if(connectionStats.vpnIp4.isEmpty() && address.getIpFamily() == IPAddress.IPFamily.IPv4)
                connectionStats.vpnIp4 = address.getIpAddress();

            if(connectionStats.vpnIp6.isEmpty() && address.getIpFamily() == IPAddress.IPFamily.IPv6)
                connectionStats.vpnIp6 = address.getIpAddress();
        }

        if(connectionStats.vpnIp4.isEmpty())
            connectionStats.vpnIp4 = "--";

        if(connectionStats.vpnIp6.isEmpty())
            connectionStats.vpnIp6 = "--";

        connectionStats.gw4 = "--";
        connectionStats.gw6 = "--";
        connectionStats.clientIp = "";
        connectionStats.tunName = interfaceName;
        connectionStats.topology = "";
        connectionStats.cipher = "CHACHA20-POLY1305";
        connectionStats.ping = -1;
        connectionStats.pingRestart = -1;

        vpnService.changeStatus(VPN.Status.CONNECTED);

        updateNotification(VPN.Status.CONNECTED);

        status = Status.RUNNING;

        vpnManager.vpn().setVpnConnectionStats(connectionStats);

        eddieEvent.onVpnConnectionStatsChanged(connectionStats);
    }

    public void cleanup() throws Exception
    {
        if(wireGuardClient == null)
        {
            String errMsg = "WireGuardTunnel.cleanup(): WireGuard client is not initialized";

            EddieLogger.error(errMsg);

            throw new Exception(errMsg);
        }

        EddieLogger.warning("WireGuard: Disconnecting VPN");

        try
        {
            wireGuardClient.stop();
        }
        catch(Exception e)
        {
            EddieLogger.error("WireGuardTunnel.cleanup(): %s", e.getMessage());
        }

        clearContexts();

        wireGuardClient = null;

        status = Status.STOPPED;

        EddieLogger.info("WireGuard: Successfully disconnected from VPN");

        EddieLogger.info(vpnManager.vpn().getFormattedStats());
    }

    public VPN.Status handleScreenChanged(boolean active)
    {
        return vpnManager.vpn().getConnectionStatus();
    }

    public void networkStatusChanged(Action action)
    {
        EddieLogger.info("Network status %s is ignored by WireGuard", action.toString());
    }

    protected synchronized boolean loadProfile(String profile, boolean isString) throws Exception
    {
        String errMsg, row[], item[];
        boolean result = true;
        IPAddress ipEntry;
        ConfigSection configSection = ConfigSection.UNDEFINED;

        if(wireGuardClient == null)
            throw new Exception("WireGuardTunnel.loadProfile(): Client is not initialized");

        if(isString)
        {
            if(profile.isEmpty() == false)
                wireGuardProfile = profile;
            else
            {
                errMsg = "WireGuardTunnel.loadProfile(): profile string is empty.";

                EddieLogger.error(errMsg);

                throw new Exception(errMsg);
            }
        }
        else
        {
            errMsg = "WireGuardTunnel.loadProfile(): Loading a profile from file is not supported.";

            EddieLogger.error(errMsg);

            throw new Exception(errMsg);
        }

        if(wireGuardProfile.isEmpty() == false)
        {
            row = wireGuardProfile.split("\n");

            for(String line : row)
            {
                line = line.trim();

                if(line.isEmpty() == false && line.charAt(0) != '#')
                {
                    if(line.indexOf('=') > -1)
                        line = line.replace('=', ' ');

                    line = line.replaceAll(",", " ");
                    line = line.replaceAll(" +", " ");
                    line = line.replaceAll("\t+", " ");

                    item = line.split(" ");

                    item[0] = item[0].toLowerCase(Locale.US);

                    switch(item[0])
                    {
                        case "[interface]":
                        {
                            configSection = ConfigSection.INTERFACE;
                        }
                        break;

                        case "[peer]":
                        {
                            configSection = ConfigSection.PEER;
                        }
                        break;

                        case "privatekey":
                        {
                            if(configSection == ConfigSection.INTERFACE)
                                privateKey = item[1];
                            else
                            {
                                EddieLogger.error("WireGuardTunnel.loadProfile(): 'PrivateKey' directive must be in 'Interface' section.");

                                result = false;
                            }
                        }
                        break;

                        case "address":
                        {
                            if(configSection == ConfigSection.INTERFACE)
                            {
                                for(int i = 1; i < item.length; i++)
                                {
                                    ipEntry = parseIpSpecification(item[i]);

                                    if(ipEntry != null)
                                    {
                                        ipEntry.setPrefixLength(ipEntry.getIpFamily() == IPAddress.IPFamily.IPv4 ? 32 : 128);

                                        ipAddress.add(ipEntry);
                                    }
                                }
                            }
                            else
                            {
                                EddieLogger.error("WireGuardTunnel.loadProfile(): 'Address' directive must be in 'Interface' section.");

                                result = false;
                            }
                        }
                        break;

                        case "dns":
                        {
                            if(configSection == ConfigSection.INTERFACE)
                            {
                                for(int i = 1; i < item.length; i++)
                                {
                                    ipEntry = parseIpSpecification(item[i]);

                                    if(ipEntry != null)
                                    {
                                        ipEntry.setPrefixLength(ipEntry.getIpFamily() == IPAddress.IPFamily.IPv4 ? 32 : 128);

                                        dnsAddress.add(ipEntry);
                                    }
                                }
                            }
                            else
                            {
                                EddieLogger.error("WireGuardTunnel.loadProfile(): 'DNS' directive must be in 'Interface' section.");

                                result = false;
                            }
                        }
                        break;

                        case "mtu":
                        {
                            if(configSection == ConfigSection.INTERFACE)
                            {
                                try
                                {
                                    customMTU = Integer.parseInt(item[1]);
                                }
                                catch(NumberFormatException e)
                                {
                                    customMTU = 0;
                                }
                            }
                            else
                            {
                                EddieLogger.error("WireGuardTunnel.loadProfile(): 'MTU' directive must be in 'Interface' section.");

                                result = false;
                            }
                        }
                        break;

                        case "publickey":
                        {
                            if(configSection == ConfigSection.PEER)
                                publicKey = item[1];
                            else
                            {
                                EddieLogger.error("WireGuardTunnel.loadProfile(): 'PublicKey' directive must be in 'Peer' section.");

                                result = false;
                            }
                        }
                        break;

                        case "presharedkey":
                        {
                            if(configSection == ConfigSection.PEER)
                                presharedKey = item[1];
                            else
                            {
                                EddieLogger.error("WireGuardTunnel.loadProfile(): 'PresharedKey' directive must be in 'Peer' section.");

                                result = false;
                            }
                        }
                        break;

                        case "persistentkeepalive":
                        {
                            if(configSection == ConfigSection.PEER)
                            {
                                try
                                {
                                    persistentKeepalive = Integer.parseInt(item[1]);
                                }
                                catch(NumberFormatException e)
                                {
                                    persistentKeepalive = 0;
                                }
                            }
                            else
                            {
                                EddieLogger.error("WireGuardTunnel.loadProfile(): 'PersistentKeepAlive' directive must be in 'Peer' section.");

                                result = false;
                            }
                        }
                        break;

                        case "allowedips":
                        {
                            if(configSection == ConfigSection.PEER)
                            {
                                for(int i = 1; i < item.length; i++)
                                {
                                    ipEntry = parseIpSpecification(item[i]);

                                    if(ipEntry != null)
                                        allowedIpAddress.add(ipEntry);
                                }
                            }
                            else
                            {
                                EddieLogger.error("WireGuardTunnel.loadProfile(): 'AllowedIPs' directive must be in 'Peer' section.");

                                result = false;
                            }
                        }
                        break;

                        case "endpoint":
                        {
                            if(configSection == ConfigSection.PEER)
                            {
                                String endPointSpec[];
                                int portSepPos = item[1].lastIndexOf(":");

                                if(portSepPos != -1)
                                {
                                    endPointSpec = new String[2];

                                    endPointSpec[0] = item[1].substring(0, portSepPos);
                                    endPointSpec[1] = item[1].substring(portSepPos + 1, item[1].length());
                                }
                                else
                                {
                                    endPointSpec = new String[1];

                                    endPointSpec[0] = item[1];
                                }

                                if(endPointSpec.length > 0)
                                {
                                    endPoint = parseIpSpecification(endPointSpec[0]);

                                    if(endPointSpec.length > 1)
                                    {
                                        try
                                        {
                                            endPointPort = Integer.parseInt(endPointSpec[1]);
                                        }
                                        catch(NumberFormatException e)
                                        {
                                            endPointPort = 0;
                                        }
                                    }
                                    else
                                        endPointPort = 0;
                                }
                                else
                                {
                                    endPoint = new IPAddress("", IPAddress.IPFamily.IPv4);

                                    endPointPort = 0;
                                }
                            }
                            else
                            {
                                EddieLogger.error("WireGuardTunnel.loadProfile(): 'Endpoint' directive must be in 'Peer' section.");

                                result = false;
                            }
                        }
                        break;

                        default:
                        {
                            errMsg = String.format(Locale.getDefault(), "WireGuardTunnel.loadProfile(): unrecognized directive '%s'.", item[0]);

                            EddieLogger.error(errMsg);

                            throw new Exception(errMsg);
                        }
                    }
                }
            }
        }

        return result;
    }

    public void bindOptions() throws Exception
    {
    }

    public VPNTransportStats getTransportStats() throws Exception
    {
        VPNTransportStats vpnTransportStats;
        long now = System.currentTimeMillis() / 1000;

        if(wireGuardClient == null)
            return null;

        vpnTransportStats = wireGuardClient.getTransportStats();

        if(vpnManager.isVpnConnectionStarted() == true && isWireGuardHandshakingTimeoutIgnored == false)
        {
            if(NetworkStatusReceiver.getNetworkStatus() == NetworkStatusReceiver.Status.CONNECTED)
            {
                switch(status)
                {
                    case RUNNING:
                    {
                        if(vpnTransportStats != null)
                        {
                            if(vpnTransportStats.lastHandshakeTimeSec <= 0)
                            {
                                EddieLogger.info("Waiting for WireGuard handshaking update");

                                waitTS = now;

                                status = Status.WAIT;
                            }
                            else if((now - vpnTransportStats.lastHandshakeTimeSec) > handshakingTimeoutSeconds)
                            {
                                vpnEvent = new VPNEvent();

                                vpnEvent.type = VPNEvent.FATAL_ERROR;
                                vpnEvent.notifyUser = true;
                                vpnEvent.name = "";
                                vpnEvent.info = "WireGuard handshake expired. Connection will be terminated.";

                                eddieEvent.onVpnError(vpnEvent);

                                status = Status.STOPPED;
                            }
                        }
                    }
                    break;

                    case STOPPED:
                    {
                    }
                    break;

                    case HOLD:
                    {
                        if(now - holdTS > handshakingUpdateWaitSeconds)
                        {
                            EddieLogger.info("Resuming WireGuard handshaking check");

                            status = Status.RUNNING;
                        }
                    }
                    break;

                    case WAIT:
                    {
                        if(now - waitTS > handshakingUpdateWaitSeconds && vpnTransportStats != null)
                        {
                            if(vpnTransportStats.lastHandshakeTimeSec > 0)
                            {
                                EddieLogger.info("WireGuard handshaking updated. Resuming check.");

                                status = Status.RUNNING;
                            }
                            else
                            {
                                vpnEvent = new VPNEvent();

                                vpnEvent.type = VPNEvent.FATAL_ERROR;
                                vpnEvent.notifyUser = true;
                                vpnEvent.name = "";
                                vpnEvent.info = "WireGuard handshake failed to resume. Connection will be terminated.";

                                eddieEvent.onVpnError(vpnEvent);

                                status = Status.STOPPED;
                            }
                        }
                    }
                    break;
                }
            }
            else
            {
                if(vpnManager.isVpnConnectionStarted() == false)
                    status = Status.STOPPED;
                else
                {
                    if(status != Status.HOLD)
                        EddieLogger.warning("Network is not currently available: holding VPN on and waiting for network connection to resume. WireGuard handshaking check is suspended.");

                    status = Status.HOLD;

                    holdTS = now;
                }
            }
        }

        return vpnTransportStats;
    }

    private IPAddress parseIpSpecification(String ipSpec)
    {
        String ipAddress = "";
        int prefixLength = -1, sep = 0;
        IPAddress.IPFamily ipFamily = IPAddress.IPFamily.IPv4;

        if(ipSpec.isEmpty())
            return null;

        sep = ipSpec.lastIndexOf('/');

        if(sep < 0)
            ipAddress = ipSpec;
        else
            ipAddress = ipSpec.substring(0, sep);

        if(ipAddress.indexOf(':') > -1)
        {
            // Cleanup IPv6 address

            ipAddress = ipAddress.replace("[", "");
            ipAddress = ipAddress.replace("]", "");

            ipFamily = IPAddress.IPFamily.IPv6;
        }
        else
            ipFamily = IPAddress.IPFamily.IPv4;

        if(sep >= 0)
        {
            try
            {
                prefixLength = Integer.parseInt(ipSpec.substring(sep + 1));
            }
            catch(NumberFormatException e)
            {
                prefixLength = -1;
            }
        }

        if(prefixLength == -1)
        {
            if(ipFamily == IPAddress.IPFamily.IPv4)
                prefixLength = 32;
            else
                prefixLength = 128;
        }

        if(SupportTools.isValidIpAddress(ipAddress) == false)
        {
            inetAddress = EddieApplication.supportTools().resolveHostName(ipAddress);

            if(inetAddress != null)
                ipAddress = inetAddress.getHostAddress();
            else
                EddieLogger.error("WireGuardTunnel: Cannot resolve %s", ipAddress);
        }

        return new IPAddress(ipAddress, ipFamily, prefixLength);
    }

    private String getUserSpaceStringConfig()
    {
        String config = "replace_peers=true\n";
        boolean includeAddress = true, excludeLocalNetwork = false;

        if(settingsManager.areLocalNetworksExcluded() && NetworkStatusReceiver.getNetworkType() != NetworkStatusReceiver.NetworkType.MOBILE)
            excludeLocalNetwork = true;
        else
            excludeLocalNetwork = false;

        config += "private_key=" + StringToHex(privateKey) + "\n";
        config += "public_key=" + StringToHex(publicKey) + "\n";

        for(IPAddress address : allowedIpAddress)
        {
            includeAddress = true;

            if(excludeLocalNetwork == true && isGlobalRoute(address) == true)
                includeAddress = false;

            if(includeAddress == true)
                config += "allowed_ip=" + address.getIpAddress() + "/" + address.getPrefixLength() + "\n";
        }

        if(endPoint.getIpFamily() == IPAddress.IPFamily.IPv6)
            config += "endpoint=[" + endPoint.getIpAddress() + "]";
        else
            config += "endpoint=" + endPoint.getIpAddress();

        config += ":" + endPointPort + "\n";
        config += "persistent_keepalive_interval=" + persistentKeepalive + "\n";
        config += "preshared_key=" + StringToHex(presharedKey) + "\n";

        return config;
    }

    private String StringToHex(String s)
    {
        final char[] output = new char[64];
        byte[] key = Base64.decode(s.getBytes(), Base64.NO_WRAP);

        for(int i = 0; i < key.length; ++i)
        {
            output[i * 2] = (char) (87 + (key[i] >> 4 & 0xf) + ((((key[i] >> 4 & 0xf) - 10) >> 8) & ~38));

            output[i * 2 + 1] = (char) (87 + (key[i] & 0xf) + ((((key[i] & 0xf) - 10) >> 8) & ~38));
        }

        return new String(output);
    }

    private void exemptPrivateNetworkFromRoute()
    {
        if(allowedIpAddress == null)
            return;

        // IPv4: exclude private address ranges 10.0.0.0/8, 172.16.0.0/12, 192.168.0.0/16

        // Allowed ranges: 0.0.0.0/5, 8.0.0.0/7, 11.0.0.0/8, 12.0.0.0/6, 16.0.0.0/4, 32.0.0.0/3,
        // 64.0.0.0/2, 128.0.0.0/3, 160.0.0.0/5, 168.0.0.0/6, 172.0.0.0/12, 172.32.0.0/11,
        // 172.64.0.0/10, 172.128.0.0/9, 173.0.0.0/8, 174.0.0.0/7, 176.0.0.0/4, 192.0.0.0/9,
        // 192.128.0.0/11, 192.160.0.0/13, 192.169.0.0/16, 192.170.0.0/15, 192.172.0.0/14,
        // 192.176.0.0/12, 192.192.0.0/10, 193.0.0.0/8, 194.0.0.0/7, 196.0.0.0/6, 200.0.0.0/5,
        // 208.0.0.0/4, 224.0.0.0/3

        allowedIpAddress.add(new IPAddress("0.0.0.0", IPAddress.IPFamily.IPv4, 5));
        allowedIpAddress.add(new IPAddress("8.0.0.0", IPAddress.IPFamily.IPv4, 7));
        allowedIpAddress.add(new IPAddress("11.0.0.0", IPAddress.IPFamily.IPv4, 8));
        allowedIpAddress.add(new IPAddress("12.0.0.0", IPAddress.IPFamily.IPv4, 6));
        allowedIpAddress.add(new IPAddress("16.0.0.0", IPAddress.IPFamily.IPv4, 4));
        allowedIpAddress.add(new IPAddress("32.0.0.0", IPAddress.IPFamily.IPv4, 3));
        allowedIpAddress.add(new IPAddress("64.0.0.0", IPAddress.IPFamily.IPv4, 2));
        allowedIpAddress.add(new IPAddress("128.0.0.0", IPAddress.IPFamily.IPv4, 3));
        allowedIpAddress.add(new IPAddress("160.0.0.0", IPAddress.IPFamily.IPv4, 5));
        allowedIpAddress.add(new IPAddress("168.0.0.0", IPAddress.IPFamily.IPv4, 6));
        allowedIpAddress.add(new IPAddress("172.0.0.0", IPAddress.IPFamily.IPv4, 12));
        allowedIpAddress.add(new IPAddress("172.32.0.0", IPAddress.IPFamily.IPv4, 11));
        allowedIpAddress.add(new IPAddress("172.64.0.0", IPAddress.IPFamily.IPv4, 10));
        allowedIpAddress.add(new IPAddress("172.128.0.0", IPAddress.IPFamily.IPv4, 9));
        allowedIpAddress.add(new IPAddress("173.0.0.0", IPAddress.IPFamily.IPv4, 8));
        allowedIpAddress.add(new IPAddress("174.0.0.0", IPAddress.IPFamily.IPv4, 7));
        allowedIpAddress.add(new IPAddress("176.0.0.0", IPAddress.IPFamily.IPv4, 4));
        allowedIpAddress.add(new IPAddress("192.0.0.0", IPAddress.IPFamily.IPv4, 9));
        allowedIpAddress.add(new IPAddress("192.128.0.0", IPAddress.IPFamily.IPv4, 11));
        allowedIpAddress.add(new IPAddress("192.160.0.0", IPAddress.IPFamily.IPv4, 13));
        allowedIpAddress.add(new IPAddress("192.169.0.0", IPAddress.IPFamily.IPv4, 16));
        allowedIpAddress.add(new IPAddress("192.170.0.0", IPAddress.IPFamily.IPv4, 15));
        allowedIpAddress.add(new IPAddress("192.172.0.0", IPAddress.IPFamily.IPv4, 14));
        allowedIpAddress.add(new IPAddress("192.176.0.0", IPAddress.IPFamily.IPv4, 12));
        allowedIpAddress.add(new IPAddress("192.192.0.0", IPAddress.IPFamily.IPv4, 10));
        allowedIpAddress.add(new IPAddress("193.0.0.0", IPAddress.IPFamily.IPv4, 8));
        allowedIpAddress.add(new IPAddress("194.0.0.0", IPAddress.IPFamily.IPv4, 7));
        allowedIpAddress.add(new IPAddress("196.0.0.0", IPAddress.IPFamily.IPv4, 6));
        allowedIpAddress.add(new IPAddress("200.0.0.0", IPAddress.IPFamily.IPv4, 5));
        allowedIpAddress.add(new IPAddress("208.0.0.0", IPAddress.IPFamily.IPv4, 4));
        allowedIpAddress.add(new IPAddress("224.0.0.0", IPAddress.IPFamily.IPv4, 3));

        // IPv6 exclude private address range fd00::/8

        // Allowed ranges:  ::/1, 8000::/2, c000::/3, e000::/4, f000::/5, f800::/6, fc00::/8, fe00::/7

        allowedIpAddress.add(new IPAddress("::", IPAddress.IPFamily.IPv6, 1));
        allowedIpAddress.add(new IPAddress("8000::", IPAddress.IPFamily.IPv6, 2));
        allowedIpAddress.add(new IPAddress("c000::", IPAddress.IPFamily.IPv6, 3));
        allowedIpAddress.add(new IPAddress("e000::", IPAddress.IPFamily.IPv6, 4));
        allowedIpAddress.add(new IPAddress("f000::", IPAddress.IPFamily.IPv6, 5));
        allowedIpAddress.add(new IPAddress("f800::", IPAddress.IPFamily.IPv6, 6));
        allowedIpAddress.add(new IPAddress("fc00::", IPAddress.IPFamily.IPv6, 8));
        allowedIpAddress.add(new IPAddress("fe00::", IPAddress.IPFamily.IPv6, 7));
    }

    private boolean isGlobalRoute(IPAddress address)
    {
        return (address.getIpFamily() == IPAddress.IPFamily.IPv4 && address.getIpAddress().equals("0.0.0.0") == true && address.getPrefixLength() == 0) || (address.getIpFamily() == IPAddress.IPFamily.IPv6 && (address.getIpAddress().equals("::0") == true || address.getIpAddress().equals("::") == true) && address.getPrefixLength() == 0);
    }
}
