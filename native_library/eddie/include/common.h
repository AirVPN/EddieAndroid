// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>

#ifndef EDDIE_ANDROID_NATIVE_COMMON_H
#define EDDIE_ANDROID_NATIVE_COMMON_H

#include <stdio.h>
#include <string>
#include <list>
#include <memory>
#include <stdint.h>

#define EDDIE_LIBRARY_NAME                  "Eddie for Android Library"
#define EDDIE_LIBRARY_VERSION               "2.0"
#define EDDIE_LIBRARY_COMPLETE_NAME         "Eddie for Android Library 2.0"
#define EDDIE_LIBRARY_RELEASE_DATE          "15 June 2022"
#define EDDIE_LIBRARY_API_LEVEL             8

#ifdef __arm__
#define EDDIE_LIBRARY_ARCHITECTURE      "armeabi-v7a (Arm 32 bit)"
#elif defined(__aarch64__)
#define EDDIE_LIBRARY_ARCHITECTURE      "arm64-v8a (Arm 64 bit)"
#elif defined(__i386__)
#define EDDIE_LIBRARY_ARCHITECTURE      "x86 (32 bit)"
#elif defined(__x86_64__)
#define EDDIE_LIBRARY_ARCHITECTURE      "x86-64 (64 bit)"
#else
#define EDDIE_LIBRARY_ARCHITECTURE      "Unknown"
#endif

#ifdef __ANDROID__
#define EDDIE_LIBRARY_PLATFORM          "Android"
#elif defined(__linux__)
#define EDDIE_LIBRARY_PLATFORM          "Linux"
#elif defined( __FreeBSD__)
#define EDDIE_LIBRARY_PLATFORM          "FreeBSD"
#else
#define EDDIE_LIBRARY_PLATFORM          "Unknown"
#endif

#define ZEROMEMORY(address, len)        memset((address), 0, (len))

#define EDDIE_LOG_TAG                   "org.airvpn.eddie.native"

#define TRUE_IF_SUCCEEDED(v)            ((v) >= 0)

#define BOOL_TO_INT                     ((v) ? 1 : 0)

#endif // EDDIE_ANDROID_NATIVE_COMMON_H
