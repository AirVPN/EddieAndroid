// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>

#ifndef EDDIE_ANDROID_NATIVE_CLIENT_H
#define EDDIE_ANDROID_NATIVE_CLIENT_H

#include "api.h"

#include "client/ovpncli.hpp"
#include "openvpn/error/error.hpp"
#include "openvpn/options/merge.hpp"

class OpenVPNClient
{
    public:

#ifdef __JNI
        OpenVPNClient(JNIEnv *env, jobject callbackClient);
#else
        OpenVPNClient(ovpn3_client_callback *callbackClient);
#endif
    
        virtual ~OpenVPNClient();

        void getTransportStats(ovpn3_transport_stats &stats) const;
        EddieLibraryResult setOption(const std::string &name, const std::string &value);
        EddieLibraryResult loadProfileFile(const std::string &filename);
        EddieLibraryResult loadProfileString(const std::string &str);
        EddieLibraryResult start();
        void stop();
        void pause(const std::string &reason);
        void resume();
        void logEvent(int level, const char *name, std::string info);
        static std::string openVPNInfo();
        static std::string openVPNCopyright();
        static std::string sslLibraryVersion();
        void setGuiVersion(std::string version);

#ifdef __JNI
        void setJniCallbackObject(jobject o);
#endif

    private:

        class OpenVPN3Client;

        EddieLibraryResult applyProfiles();
        void init();
        void cleanup();
        std::unique_ptr<OpenVPN3Client> openVPN3Client;
        std::string openVpnProfile;
        EddieLibraryResult libResult;
        std::string logMessage;
        std::string guiVersion;
};

class OpenVPNClient::OpenVPN3Client : public openvpn::ClientAPI::OpenVPNClient
{
    typedef openvpn::ClientAPI::OpenVPNClient BaseClass;

    typedef std::unordered_map<std::string, std::string> OptionsMap;

    // Construction
    public:

#ifdef __JNI
        OpenVPN3Client(JNIEnv *env, jobject callbackClient);
#else
        OpenVPN3Client(ovpn3_client_callback *callbackClient);
#endif

        ~OpenVPN3Client();

        const std::string &getOption(const std::string name, const std::string &defValue = "") const;
        EddieLibraryResult setOption(const std::string name, const std::string &value, bool check = false);

        template <typename T> T getOptionValue(const std::string name) const;
        template <typename T> T getOptionValue(const std::string name, const T &defValue) const;

        // Operations

        void initOptions();

        void applyConfig(openvpn::ClientAPI::Config &config);
        void applyCredentials(openvpn::ClientAPI::ProvideCreds &credentials);

        static void logInfo(const std::string &message);
        static void logDebug(const std::string &message);
        static void logError(const std::string &message);
        void logEvent(int level, const char *name, std::string info);

#ifdef __JNI
        void setupJavaVM(JNIEnv *e);
        void setJniCallbackObject(jobject o);
        void setJniCallbackInterface(ovpn3_client_callback_jni *callbackClient);
        JNIEnv *getThreadJniEnv();
#endif

        // OpenVPNClient interface

        virtual void log(const openvpn::ClientAPI::LogInfo &li) override;
        virtual bool socket_protect(int socket, std::string remote, bool ipv6) override;
        virtual bool pause_on_connection_timeout() override;
        virtual void event(const openvpn::ClientAPI::Event &e) override;
        virtual void acc_event(const openvpn::ClientAPI::AppCustomControlMessageEvent &acev) override;
        virtual void external_pki_cert_request(openvpn::ClientAPI::ExternalPKICertRequest &) override;
        virtual void external_pki_sign_request(openvpn::ClientAPI::ExternalPKISignRequest &) override;

        // TunBuilderBase overrides

        // Tun builder methods, loosely based on the Android VpnService.Builder
        // abstraction.  These methods comprise an abstraction layer that
        // allows the OpenVPN C++ core to call out to external methods for
        // establishing the tunnel, adding routes, etc.

        // All methods returning bool use the return
        // value to indicate success (true) or fail (false).
        // tun_builder_new() should be called first, then arbitrary setter methods,
        // and finally tun_builder_establish to return the socket descriptor
        // for the session.  IP addresses are pre-validated before being passed to
        // these methods.
        // This interface is based on Android's VpnService.Builder.

        // Callback to construct a new tun builder
        // Should be called first.

        virtual bool tun_builder_new() override;

        // Optional callback that indicates OSI layer, should be 2 or 3.
        // Defaults to 3.

        virtual bool tun_builder_set_layer(int layer) override;

        // Callback to set address of remote server
        // Never called more than once per tun_builder session.

        virtual bool tun_builder_set_remote_address(const std::string &address, bool ipv6) override;

        // Callback to add network address to VPN interface
        // May be called more than once per tun_builder session

        virtual bool tun_builder_add_address(const std::string &address, int prefix_length, const std::string &gateway, bool ipv6, bool net30) override;

        // Optional callback to set default value for route metric.
        // Guaranteed to be called before other methods that deal
        // with routes such as tun_builder_add_route and
        // tun_builder_reroute_gw.  Route metric is ignored
        // if < 0.

        virtual bool tun_builder_set_route_metric_default(int metric) override;

        // Callback to reroute default gateway to VPN interface.
        // ipv4 is true if the default route to be added should be IPv4.
        // ipv6 is true if the default route to be added should be IPv6.
        // flags are defined in RGWFlags (rgwflags.hpp).
        // Never called more than once per tun_builder session.

        virtual bool tun_builder_reroute_gw(bool ipv4, bool ipv6, unsigned int flags) override;

        // Callback to add route to VPN interface
        // May be called more than once per tun_builder session
        // metric is optional and should be ignored if < 0

        virtual bool tun_builder_add_route(const std::string &address, int prefix_length, int metric, bool ipv6) override;

        // Callback to exclude route from VPN interface
        // May be called more than once per tun_builder session
        // metric is optional and should be ignored if < 0

        virtual bool tun_builder_exclude_route(const std::string &address, int prefix_length, int metric, bool ipv6) override;

        // Callback to add DNS server to VPN interface
        // May be called more than once per tun_builder session
        // If reroute_dns is true, all DNS traffic should be routed over the
        // tunnel, while if false, only DNS traffic that matches an added search
        // domain should be routed.
        // Guaranteed to be called after tun_builder_reroute_gw.

        virtual bool tun_builder_add_dns_server(const std::string &address, bool ipv6) override;

        // Callback to add search domain to DNS resolver
        // May be called more than once per tun_builder session
        // See tun_builder_add_dns_server above for description of
        // reroute_dns parameter.
        // Guaranteed to be called after tun_builder_reroute_gw.

        virtual bool tun_builder_add_search_domain(const std::string &domain) override;

        // Callback to set MTU of the VPN interface
        // Never called more than once per tun_builder session.

        virtual bool tun_builder_set_mtu(int mtu) override;

        // Callback to set the session name
        // Never called more than once per tun_builder session.

        virtual bool tun_builder_set_session_name(const std::string &name) override;

        // Callback to add a host which should bypass the proxy
        // May be called more than once per tun_builder session

        virtual bool tun_builder_add_proxy_bypass(const std::string &bypass_host) override;

        // Callback to set the proxy "Auto Config URL"
        // Never called more than once per tun_builder session.

        virtual bool tun_builder_set_proxy_auto_config_url(const std::string &url) override;

        // Callback to set the HTTP proxy
        // Never called more than once per tun_builder session.

        virtual bool tun_builder_set_proxy_http(const std::string &host, int port) override;

        // Callback to set the HTTPS proxy
        // Never called more than once per tun_builder session.

        virtual bool tun_builder_set_proxy_https(const std::string &host, int port) override;

        // Callback to add Windows WINS server to VPN interface.
        // WINS server addresses are always IPv4.
        // May be called more than once per tun_builder session.
        // Guaranteed to be called after tun_builder_reroute_gw.

        virtual bool tun_builder_add_wins_server(const std::string &address) override;

        // Optional callback that indicates whether traffic of a certain
        // address family (AF_INET or AF_INET6) should be
        // blocked or allowed, to prevent unencrypted packet leakage when
        // the tunnel is IPv4-only/IPv6-only, but the local machine
        // has connectivity with the other protocol to the internet.
        // Controlled by "block-ipv6" and block-ipv6 config var.
        // If addresses are added for a family this setting should be
        // ignored for that family
        // See also Android's VPNService.Builder.allowFamily method

        virtual bool tun_builder_set_allow_family(int af, bool allow) override;

        // Optional callback to set a DNS suffix on tun/tap adapter.
        // Currently only implemented on Windows, where it will
        // set the "Connection-specific DNS Suffix" property on
        // the TAP driver.

        virtual bool tun_builder_set_adapter_domain_suffix(const std::string &name) override;

        // Callback to establish the VPN tunnel, returning a file descriptor
        // to the tunnel, which the caller will henceforth own.  Returns -1
        // if the tunnel could not be established.
        // Always called last after tun_builder session has been configured.

        virtual int tun_builder_establish() override;

        // Return true if tun interface may be persisted, i.e. rolled
        // into a new session with properties untouched.  This method
        // is only called after all other tests of persistence
        // allowability succeed, therefore it can veto persistence.
        // If persistence is ultimately enabled,
        // tun_builder_establish_lite() will be called.  Otherwise,
        // tun_builder_establish() will be called.

        virtual bool tun_builder_persist() override;

        // Indicates a reconnection with persisted tun state.

        virtual void tun_builder_establish_lite() override;

        // Indicates that tunnel is being torn down.
        // If disconnect == true, then the teardown is occurring
        // prior to final disconnect.

        virtual void tun_builder_teardown(bool disconnect) override;

    private:

        openvpn::ClientAPI::ConnectionInfo connectionInfo;

        void dumpConfig(const openvpn::ClientAPI::Config &config);
        void dumpCredentials(const openvpn::ClientAPI::ProvideCreds &credentials);
        void logConfig(const openvpn::ClientAPI::Config &config);
        void logCredentials(const openvpn::ClientAPI::ProvideCreds &credentials);
        bool logFilter(std::string s);
        bool logFilter(char *cs);
        int getEventType(char *name);
        bool findErrorInString(std::string s);
        bool findFatalErrorInString(std::string s);
        bool isUdpPartialSendError(std::string s);
        openvpn::Error::Type getOpenVPNErrorInString(const std::string &s);
        bool strcontains(const char *haystack, const char *needle);

#ifdef __JNI
        JavaVM *javaVM;
        jobject jniCallbackObject;

        bool setJniBooleanField(jclass jniClass, jobject jniObject, const char *fieldName, jboolean value);
        bool setJniIntField(jclass jniClass, jobject jniObject, const char *fieldName, int value);
        bool setJniStringField(jclass jniClass, jobject jniObject, const char *fieldName, const std::string &value);
        bool setJniStringField(jclass jniClass, jobject jniObject, const char *fieldName, const char *value);
        bool setJniObjectField(jclass jniClass, jobject jniObject, const char *fieldName, jobject value);
        bool isJniCallbackValid();
        void releaseJniCallbackObject(JNIEnv *jniEnv);

        ovpn3_client_callback_jni *openVPNClientJniCallbackInterface;

#else
        ovpn3_client_callback openVPNClientCallbackInterface;
#endif

        OptionsMap m_options;
        EddieLibraryResult libResult;
        std::string logMessage;

    protected:

        virtual void connect_attach() override;
        virtual void connect_pre_run() override;
        virtual void connect_run() override;
        virtual void connect_session_stop() override;
};

#endif // EDDIE_ANDROID_NATIVE_CLIENT_H
