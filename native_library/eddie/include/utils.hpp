// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>

#ifndef EDDIE_ANDROID_NATIVE_UTILS_H
#define EDDIE_ANDROID_NATIVE_UTILS_H

class Utils
{
    public:

    static std::string formatString(const char *s, ...);

    static void trimString(std::string &str);
    static void trimStringLeft(std::string &str);
    static void trimStringRight(std::string &str);
    static std::string trimStringCopy(const std::string &str);

    static void logDebug(const std::string &message);
    static void logInfo(const std::string &message);
    static void logError(const std::string &message);

    template<typename T> static T fromString(const std::string &v);
    template<typename T> static T fromString(const std::string &v, const T &defaultValue);

    private:

    static const int FORMAT_BUFFER_LENGTH = 2048;

    static void logMessage(int level, const std::string &message);
};

#endif // EDDIE_ANDROID_NATIVE_UTILS_H
