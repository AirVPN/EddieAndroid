LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_C_INCLUDES := openvpn3-airvpn openvpn3-airvpn/client asio/asio/include breakpad/src
LOCAL_CFLAGS := -funwind-tables -DUSE_ASIO
LOCAL_SHARED_LIBRARIES := openvpn3 wireguard
LOCAL_STATIC_LIBRARIES := breakpad_client

LOCAL_SRC_FILES := \
	api.cpp \
	breakpad.cpp \
	openvpnclient.cpp \
	utils.cpp

LOCAL_MODULE = eddie

include $(BUILD_SHARED_LIBRARY)
