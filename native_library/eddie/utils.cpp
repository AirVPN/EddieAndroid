// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// Version 1.4 - xx September 2021, ProMIND
//
// Complete revision of code, structure and naming scheme
//

#include "include/common.h"
#include "include/utils.hpp"
#include <memory>
#include <exception>

#include <android/log.h>

std::string Utils::formatString(const char *s, ...)
{
    char *output;
    std::string result;

    va_list args;

    output = (char *)malloc(FORMAT_BUFFER_LENGTH);

    strcpy(output, s);

    va_start(args, s);
    
    vsnprintf(output, FORMAT_BUFFER_LENGTH, s, args);
    
    va_end(args);

    result = output;

    free(output);

    return result;
}

void Utils::trimString(std::string &str)
{
    trimStringLeft(str);

    trimStringRight(str);
}

std::string Utils::trimStringCopy(const std::string &str)
{
    std::string copy(str);

    trimString(copy);

    return copy;
}

void Utils::trimStringLeft(std::string &str)
{
    str.erase(str.begin(), std::find_if(str.begin(), str.end(), [](int c) { return !std::isspace(c); }));
}

void Utils::trimStringRight(std::string &str)
{
    str.erase(std::find_if(str.rbegin(), str.rend(), [](int c) { return !std::isspace(c); }).base(), str.end());
}

void Utils::logDebug(const std::string &message)
{
    logMessage(ANDROID_LOG_DEBUG, message);
}

void Utils::logInfo(const std::string &message)
{
    logMessage(ANDROID_LOG_INFO, message);
}

void Utils::logError(const std::string &message)
{
    logMessage(ANDROID_LOG_ERROR, message);
}

void Utils::logMessage(int level, const std::string &message)
{
    __android_log_write(level, EDDIE_LOG_TAG, message.c_str());
}

template<typename T> T Utils::fromString(const std::string &v, const T &defaultValue)
{
    if(v.empty())
        return defaultValue;

    return fromString<T>(v);
}

template<> bool Utils::fromString<bool>(const std::string &v)
{
    return v == "true";
}

template<> int Utils::fromString<int>(const std::string &v)
{
    int value = 0;
    
    try
    {
        value = std::stoi(v);
    }
    catch(std::exception &e)
    {
        value = 0;
    }

    return value;
}
