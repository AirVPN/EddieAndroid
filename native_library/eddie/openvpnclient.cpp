// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>

#include "include/common.h"
#include "include/options.h"
#include "include/openvpnclient.hpp"
#include "include/utils.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "openvpn/common/arraysize.hpp"
#include "openvpn/client/clievent.hpp"
#include "openvpn/client/cliconstants.hpp"

// OpenVPNClient class methods

#ifdef __JNI

OpenVPNClient::OpenVPNClient(JNIEnv *env, jobject callbackClient) : openVPN3Client(new OpenVPN3Client(env, callbackClient))
{
    init();
}

#else

OpenVPNClient::OpenVPNClient(ovpn3_client_callback *callbackClient) : openVPN3Client(new OpenVPN3Client(callbackClient))
{
    init();
}

#endif

OpenVPNClient::~OpenVPNClient()
{
#ifdef __JNI
    JNIEnv *jniEnv;

    jniEnv = openVPN3Client->getThreadJniEnv();
#endif

    logMessage = "Releasing OpenVPN Client";

    openVPN3Client->logInfo(logMessage);

    openVPN3Client->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

    openVPN3Client.reset();
    
    cleanup();
}

#ifdef __JNI

void OpenVPNClient::setJniCallbackObject(jobject o)
{
    openVPN3Client->setJniCallbackObject(o);
}

#endif

void OpenVPNClient::getTransportStats(ovpn3_transport_stats &stats) const
{
    ZEROMEMORY(&stats, sizeof(ovpn3_transport_stats));

    openvpn::ClientAPI::TransportStats ts = openVPN3Client->transport_stats();

    stats.bytes_in = ts.bytesIn;
    stats.bytes_out = ts.bytesOut;
    stats.packets_in = ts.packetsIn;
    stats.packets_out = ts.packetsOut;
    stats.last_packet_received = ts.lastPacketReceived;
}

EddieLibraryResult OpenVPNClient::setOption(const std::string &name, const std::string &value)
{
    return openVPN3Client->setOption(name, value, true);
}

void OpenVPNClient::init()
{
    guiVersion = EDDIE_LIBRARY_NAME;
    guiVersion += " ";
    guiVersion += EDDIE_LIBRARY_VERSION;
}

void OpenVPNClient::cleanup()
{
}

EddieLibraryResult OpenVPNClient::loadProfileFile(const std::string &filename)
{
    openvpn::ProfileMerge pm(filename.c_str(), "ovpn", "", openvpn::ProfileMerge::FOLLOW_FULL, openvpn::ProfileParseLimits::MAX_LINE_SIZE, openvpn::ProfileParseLimits::MAX_PROFILE_SIZE);

    if(pm.status() == openvpn::ProfileMerge::MERGE_SUCCESS)
        libResult = loadProfileString(pm.profile_content());
    else
    {
        logMessage = Utils::formatString("OpenVPNClient::loadProfileFile(): merge config error: '%s' ('%s')", pm.status_string(), pm.error().c_str());

        openVPN3Client->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        openVPN3Client->logError(logMessage);

        libResult.code = LR_OPENVPN_PROFILE_ERROR;
        libResult.description = logMessage.c_str();
    }

    return libResult;
}

EddieLibraryResult OpenVPNClient::loadProfileString(const std::string &str)
{
    std::string profile = Utils::trimStringCopy(str);

    if(profile.empty() == false)
    {
        openVpnProfile = profile;

        libResult.code = LR_SUCCESS;
        libResult.description = LIBRARY_RESULT_OK;
    }
    else
    {
        logMessage = "OpenVPNClient::loadProfileString(): Profile is empty";

        openVPN3Client->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        openVPN3Client->logError(logMessage);

        libResult.code = LR_OPENVPN_PROFILE_ERROR;
        libResult.description = logMessage.c_str();
    }

    return libResult;
}

EddieLibraryResult OpenVPNClient::start()
{
    logMessage = "Loading profile to OpenVPN Client";

    OpenVPN3Client::logInfo(logMessage);

    openVPN3Client->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

    libResult = applyProfiles();

    if(libResult.code != LR_SUCCESS)
        return libResult;

    logMessage = "Profile successfully loaded. Connecting to server.";

    openVPN3Client->logInfo(logMessage);

    openVPN3Client->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

    openvpn::ClientAPI::Status connectStatus = openVPN3Client->connect();

    if(!connectStatus.error)
    {
        libResult.code = LR_SUCCESS;
        libResult.description = LIBRARY_RESULT_OK;
    }
    else
    {
        logMessage = "OpenVPNClient::start(): OpenVPN Connection error";

        if(!connectStatus.status.empty())
            logMessage += ": " + connectStatus.status;

        openVPN3Client->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        openVPN3Client->logError(logMessage);

        libResult.code = LR_OPENVPN_CONNECTION_ERROR;
        libResult.description = logMessage.c_str();
    }

    return libResult;
}

void OpenVPNClient::stop()
{
    openVPN3Client->stop();

    logMessage = "OpenVPN Connection stopped";

    openVPN3Client->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

    openVPN3Client->logInfo(logMessage);
}

void OpenVPNClient::pause(const std::string &reason)
{
    openVPN3Client->pause(reason);

    logMessage = Utils::formatString("OpenVPN Connection paused (%s)", reason.c_str());

    openVPN3Client->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

    openVPN3Client->logInfo(logMessage);
}

void OpenVPNClient::resume()
{
    openVPN3Client->resume();

    logMessage = "OpenVPN Connection resumed";

    openVPN3Client->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

    openVPN3Client->logInfo(logMessage);
}

EddieLibraryResult OpenVPNClient::applyProfiles()
{
    if(openVpnProfile == "")
    {
        logMessage = "OpenVPNClient::applyProfiles(): Profile is empty";

        openVPN3Client->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        libResult.code = LR_OPENVPN_PROFILE_IS_EMPTY;
        libResult.description = logMessage.c_str();

        return libResult;
    }

    std::string epki_cert_fn;

    // Create the config from the loaded profiles

    openvpn::ClientAPI::Config config;
    config.guiVersion = guiVersion;
    config.info = true;
    config.altProxy = false;
    config.dco = false;
    config.googleDnsFallback = false;

    config.content = openVpnProfile;

    openVPN3Client->applyConfig(config);

    if(!epki_cert_fn.empty())
        config.externalPkiAlias = "epki"; // dummy

    openvpn::ClientAPI::EvalConfig eval = openVPN3Client->eval_config(config);

    if(eval.error)
    {
        logMessage = "OpenVPNClient::applyProfiles(): Eval config error (" + eval.message + ")";

        openVPN3Client->logError(logMessage);

        openVPN3Client->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        libResult.code = LR_OPENVPN_CONFIG_EVAL_ERROR;
        libResult.description = logMessage.c_str();

        return libResult;
    }

    if(!eval.autologin)
    {
        logMessage = "Loading credentials";

        openVPN3Client->logDebug(logMessage);

        openVPN3Client->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

        openvpn::ClientAPI::ProvideCreds credentials;

        credentials.response = "";
        credentials.dynamicChallengeCookie = "";
        credentials.replacePasswordWithSessionID = true;
        credentials.cachePassword = false;

        openVPN3Client->applyCredentials(credentials);

        if(credentials.username.empty() || credentials.password.empty())
        {
            logMessage = "Credentials are invalid or empty";

            openVPN3Client->logError(logMessage);

            openVPN3Client->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

            libResult.code = LR_OPENVPN_CREDS_ERROR;
            libResult.description = logMessage.c_str();

            return libResult;
        }

        openvpn::ClientAPI::Status credentials_status = openVPN3Client->provide_creds(credentials);

        if(credentials_status.error)
        {
            logMessage = "OpenVPNClient::applyProfiles(): Credentials error (" + credentials_status.message + ")";

            openVPN3Client->logError(logMessage);

            openVPN3Client->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

            libResult.code = LR_OPENVPN_CONFIG_EVAL_ERROR;
            libResult.description = logMessage.c_str();

            return libResult;
        }
    }

    libResult.code = LR_SUCCESS;
    libResult.description = LIBRARY_RESULT_OK;

    return libResult;
}

void OpenVPNClient::logEvent(int level, const char *name, std::string info)
{
    openVPN3Client->logEvent(level, name, info);
}

std::string OpenVPNClient::openVPNInfo()
{
    return openvpn::ClientAPI::OpenVPNClientHelper::platform();
}

std::string OpenVPNClient::openVPNCopyright()
{
    return openvpn::ClientAPI::OpenVPNClientHelper::copyright();
}

std::string OpenVPNClient::sslLibraryVersion()
{
    return openvpn::ClientAPI::OpenVPNClientHelper::ssl_library_version();
}

void OpenVPNClient::setGuiVersion(std::string version)
{
    guiVersion = version;
}

#ifdef __JNI

// OpenVPNClient::OpenVPN3Client class methods

OpenVPNClient::OpenVPN3Client::OpenVPN3Client(JNIEnv *env, jobject callbackClient)
{
    javaVM = nullptr;
    jniCallbackObject = nullptr;
    openVPNClientJniCallbackInterface = nullptr;

    setupJavaVM(env);

    setJniCallbackObject(callbackClient);

    initOptions();
}

#else

OpenVPNClient::OpenVPN3Client::OpenVPN3Client(ovpn3_client_callback *callbackClient)
{
    memcpy(&openVPNClientCallbackInterface, callbackClient, sizeof(ovpn3_client_callback));

    initOptions();
}

#endif

OpenVPNClient::OpenVPN3Client::~OpenVPN3Client()
{
#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniCallbackObject != nullptr && jniEnv != nullptr)
    {
        logDebug("Deleting reference of jniCallbackObject");

        releaseJniCallbackObject(jniEnv);
    }

    if(openVPNClientJniCallbackInterface != nullptr)
    {
        delete openVPNClientJniCallbackInterface;
        
        openVPNClientJniCallbackInterface = nullptr;
    }
        
#endif

    logMessage = "Releasing OpenVPN3 Client";
    
    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);
}

const std::string &OpenVPNClient::OpenVPN3Client::getOption(const std::string name, const std::string &defValue) const
{
    OptionsMap::const_iterator i = m_options.find(name);

    if(i != m_options.end())
        return i->second;

    return defValue;
}

EddieLibraryResult OpenVPNClient::OpenVPN3Client::setOption(std::string name, const std::string &value, bool check)
{
    if(check && m_options.find(name) == m_options.end())
    {
        logMessage = Utils::formatString("OpenVPNClient::OpenVPN3Client::setOption(): Unknown option '%s'", name.c_str());

        logError(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        libResult.code = LR_OPENVPN_UNKNOWN_OPTION;
        libResult.description = logMessage.c_str();
    }
    else
    {
        m_options[name] = value;

        libResult.code = LR_SUCCESS;
        libResult.description = LIBRARY_RESULT_OK;
    }

    return libResult;
}

template<typename T> T OpenVPNClient::OpenVPN3Client::getOptionValue(std::string name) const
{
    return Utils::fromString<T>(getOption(name));
}

template<typename T> T OpenVPNClient::OpenVPN3Client::getOptionValue(std::string name, const T &defValue) const
{
    OptionsMap::const_iterator i = m_options.find(name);
    
    if(i != m_options.end())
        return Utils::fromString<T>(i->second);

    return defValue;
}

void OpenVPNClient::OpenVPN3Client::initOptions()
{
    setOption(EDDIE_OVPN3_OPTION_TLS_VERSION_MIN, EDDIE_OVPN3_OPTION_TLS_VERSION_MIN_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_PROTOCOL, EDDIE_OVPN3_OPTION_PROTOCOL_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_ALLOWUAF, EDDIE_OVPN3_OPTION_ALLOWUAF_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_TIMEOUT, EDDIE_OVPN3_OPTION_TIMEOUT_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_TUN_PERSIST, EDDIE_OVPN3_OPTION_TUN_PERSIST_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_COMPRESSION_MODE, EDDIE_OVPN3_OPTION_COMPRESSION_MODE_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_USERNAME, EDDIE_OVPN3_OPTION_USERNAME_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_PASSWORD, EDDIE_OVPN3_OPTION_PASSWORD_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_SYNCHRONOUS_DNS_LOOKUP, EDDIE_OVPN3_OPTION_SYNCHRONOUS_DNS_LOOKUP_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_AUTOLOGIN_SESSIONS, EDDIE_OVPN3_OPTION_AUTOLOGIN_SESSIONS_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_DISABLE_CLIENT_CERT, EDDIE_OVPN3_OPTION_DISABLE_CLIENT_CERT_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_SSL_DEBUG_LEVEL, EDDIE_OVPN3_OPTION_SSL_DEBUG_LEVEL_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_PRIVATE_KEY_PASSWORD, EDDIE_OVPN3_OPTION_PRIVATE_KEY_PASSWORD_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_DEFAULT_KEY_DIRECTION, EDDIE_OVPN3_OPTION_DEFAULT_KEY_DIRECTION_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_TLS_CERT_PROFILE, EDDIE_OVPN3_OPTION_TLS_CERT_PROFILE_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_PROXY_HOST, EDDIE_OVPN3_OPTION_PROXY_HOST_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_PROXY_PORT, EDDIE_OVPN3_OPTION_PROXY_PORT_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_PROXY_USERNAME, EDDIE_OVPN3_OPTION_PROXY_USERNAME_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_PROXY_PASSWORD, EDDIE_OVPN3_OPTION_PROXY_PASSWORD_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_PROXY_ALLOW_CLEARTEXT_AUTH, EDDIE_OVPN3_OPTION_PROXY_ALLOW_CLEARTEXT_AUTH_DEFAULT);
}

void OpenVPNClient::OpenVPN3Client::applyConfig(openvpn::ClientAPI::Config &config)
{
    config.protoOverride = getOption(EDDIE_OVPN3_OPTION_PROTOCOL);
    config.connTimeout = getOptionValue<int>(EDDIE_OVPN3_OPTION_TIMEOUT);
    config.compressionMode = getOption(EDDIE_OVPN3_OPTION_COMPRESSION_MODE);
    config.allowUnusedAddrFamilies = getOption(EDDIE_OVPN3_OPTION_ALLOWUAF);
    config.privateKeyPassword = getOption(EDDIE_OVPN3_OPTION_PRIVATE_KEY_PASSWORD);
    config.tlsVersionMinOverride = getOption(EDDIE_OVPN3_OPTION_TLS_VERSION_MIN);
    config.tlsCertProfileOverride = getOption(EDDIE_OVPN3_OPTION_TLS_CERT_PROFILE);
    config.disableClientCert = getOptionValue<bool>(EDDIE_OVPN3_OPTION_DISABLE_CLIENT_CERT);
    config.proxyHost = getOption(EDDIE_OVPN3_OPTION_PROXY_HOST);
    config.proxyPort = getOption(EDDIE_OVPN3_OPTION_PROXY_PORT);
    config.proxyUsername = getOption(EDDIE_OVPN3_OPTION_PROXY_USERNAME);
    config.proxyPassword = getOption(EDDIE_OVPN3_OPTION_PROXY_PASSWORD);
    config.proxyAllowCleartextAuth = getOptionValue<bool>(EDDIE_OVPN3_OPTION_PROXY_ALLOW_CLEARTEXT_AUTH);
    config.defaultKeyDirection = getOptionValue<int>(EDDIE_OVPN3_OPTION_DEFAULT_KEY_DIRECTION);
    config.sslDebugLevel = getOptionValue<int>(EDDIE_OVPN3_OPTION_SSL_DEBUG_LEVEL);
    config.autologinSessions = getOptionValue<bool>(EDDIE_OVPN3_OPTION_AUTOLOGIN_SESSIONS);
    config.tunPersist = getOptionValue<bool>(EDDIE_OVPN3_OPTION_TUN_PERSIST);
    config.synchronousDnsLookup = getOptionValue<bool>(EDDIE_OVPN3_OPTION_SYNCHRONOUS_DNS_LOOKUP);

    dumpConfig(config);

    logConfig(config);
}

void OpenVPNClient::OpenVPN3Client::applyCredentials(openvpn::ClientAPI::ProvideCreds &credentials)
{
    credentials.username = getOption(EDDIE_OVPN3_OPTION_USERNAME);
    credentials.password = getOption(EDDIE_OVPN3_OPTION_PASSWORD);

    dumpCredentials(credentials);

    logCredentials(credentials);
}

void OpenVPNClient::OpenVPN3Client::dumpConfig(const openvpn::ClientAPI::Config &config)
{
    logDebug("config.protoOverride: " + config.protoOverride);
    logDebug("config.connTimeout: " + std::to_string(config.connTimeout));
    logDebug("config.compressionMode: " + config.compressionMode);
    logDebug("config.allowUnusedAddrFamilies: " + config.allowUnusedAddrFamilies);
    logDebug("config.privateKeyPassword: ********");
    logDebug("config.tlsVersionMinOverride: " + config.tlsVersionMinOverride);
    logDebug("config.tlsCertProfileOverride: " + config.tlsCertProfileOverride);
    logDebug("config.disableClientCert: " + std::to_string(config.disableClientCert));
    logDebug("config.proxyHost: " + config.proxyHost);
    logDebug("config.proxyPort: " + config.proxyPort);
    logDebug("config.proxyUsername: ********");
    logDebug("config.proxyPassword: ********");
    logDebug("config.proxyAllowCleartextAuth: " + std::to_string(config.proxyAllowCleartextAuth));
    logDebug("config.defaultKeyDirection: " + std::to_string(config.defaultKeyDirection));
    logDebug("config.sslDebugLevel: " + std::to_string(config.sslDebugLevel));
    logDebug("config.autologinSessions: " + std::to_string(config.autologinSessions));
    logDebug("config.tunPersist: " + std::to_string(config.tunPersist));
    logDebug("config.synchronousDnsLookup: " + std::to_string(config.synchronousDnsLookup));
}

void OpenVPNClient::OpenVPN3Client::logConfig(const openvpn::ClientAPI::Config &config)
{
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.protoOverride: " + config.protoOverride);
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.connTimeout: " + std::to_string(config.connTimeout));
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.compressionMode: " + config.compressionMode);
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.allowUnusedAddrFamilies: " + config.allowUnusedAddrFamilies);
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.privateKeyPassword: ********");
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.tlsVersionMinOverride: " + config.tlsVersionMinOverride);
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.tlsCertProfileOverride: " + config.tlsCertProfileOverride);
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.disableClientCert: " + std::to_string(config.disableClientCert));
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.proxyHost: " + config.proxyHost);
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.proxyPort: " + config.proxyPort);
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.proxyUsername: ********");
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.proxyPassword: ********");
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.proxyAllowCleartextAuth: " + std::to_string(config.proxyAllowCleartextAuth));
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.defaultKeyDirection: " + std::to_string(config.defaultKeyDirection));
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.sslDebugLevel: " + std::to_string(config.sslDebugLevel));
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.autologinSessions: " + std::to_string(config.autologinSessions));
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.tunPersist: " + std::to_string(config.tunPersist));
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.synchronousDnsLookup: " + std::to_string(config.synchronousDnsLookup));
}

void OpenVPNClient::OpenVPN3Client::dumpCredentials(const openvpn::ClientAPI::ProvideCreds &credentials)
{
    logDebug("credentials.username: ********");
    logDebug("credentials.password: ********");
}

void OpenVPNClient::OpenVPN3Client::logCredentials(const openvpn::ClientAPI::ProvideCreds &credentials)
{
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "credentials.username: ********");
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "credentials.password: ********");
}

void OpenVPNClient::OpenVPN3Client::log(const openvpn::ClientAPI::LogInfo &li)
{
    logInfo(li.text);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, li.text);
}

#ifdef __JNI

void OpenVPNClient::OpenVPN3Client::setupJavaVM(JNIEnv *e)
{
    e->GetJavaVM(&javaVM);
}

void OpenVPNClient::OpenVPN3Client::setJniCallbackObject(jobject o)
{
    jmethodID mid = nullptr;
    jclass cls = nullptr;

    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
    {
        jniCallbackObject = nullptr;

        return;
    }

    if(o == nullptr)
    {
        logDebug("callback object is null. Events will be turned off.");

        releaseJniCallbackObject(jniEnv);

        return;
    }

    logDebug("Setting a new callback object");

    releaseJniCallbackObject(jniEnv);

    jniCallbackObject = jniEnv->NewGlobalRef(o);

    cls = jniEnv->GetObjectClass(jniCallbackObject);

    if(openVPNClientJniCallbackInterface != nullptr)
        delete openVPNClientJniCallbackInterface;

    openVPNClientJniCallbackInterface = new ovpn3_client_callback_jni();

    // Client callbacks

    openVPNClientJniCallbackInterface->socket_protect = jniEnv->GetMethodID(cls, "onSocketProtect", "(I)Z");
    openVPNClientJniCallbackInterface->on_event = jniEnv->GetMethodID(cls, "onEvent", "(Ljava/lang/Object;)V");

    // TUN callbacks

    openVPNClientJniCallbackInterface->tun_builder_new = jniEnv->GetMethodID(cls, "onTunBuilderNew", "()Z");
    openVPNClientJniCallbackInterface->tun_builder_set_layer = jniEnv->GetMethodID(cls, "onTunBuilderSetLayer", "(I)Z");
    openVPNClientJniCallbackInterface->tun_builder_set_remote_address = jniEnv->GetMethodID(cls, "onTunBuilderSetRemoteAddress", "(Ljava/lang/String;Z)Z");
    openVPNClientJniCallbackInterface->tun_builder_add_address = jniEnv->GetMethodID(cls, "onTunBuilderAddAddress", "(Ljava/lang/String;ILjava/lang/String;ZZ)Z");
    openVPNClientJniCallbackInterface->tun_builder_set_route_metric_default = jniEnv->GetMethodID(cls, "onTunBuilderSetRouteMetricDefault", "(I)Z");
    openVPNClientJniCallbackInterface->tun_builder_reroute_gw = jniEnv->GetMethodID(cls, "onTunBuilderRerouteGW", "(ZZI)Z");
    openVPNClientJniCallbackInterface->tun_builder_add_route = jniEnv->GetMethodID(cls, "onTunBuilderAddRoute", "(Ljava/lang/String;IIZ)Z");
    openVPNClientJniCallbackInterface->tun_builder_exclude_route = jniEnv->GetMethodID(cls, "onTunBuilderExcludeRoute", "(Ljava/lang/String;IIZ)Z");
    openVPNClientJniCallbackInterface->tun_builder_add_dns_server = jniEnv->GetMethodID(cls, "onTunBuilderAddDNSServer", "(Ljava/lang/String;Z)Z");
    openVPNClientJniCallbackInterface->tun_builder_add_search_domain = jniEnv->GetMethodID(cls, "onTunBuilderAddSearchDomain", "(Ljava/lang/String;)Z");
    openVPNClientJniCallbackInterface->tun_builder_set_mtu = jniEnv->GetMethodID(cls, "onTunBuilderSetMTU", "(I)Z");
    openVPNClientJniCallbackInterface->tun_builder_set_session_name = jniEnv->GetMethodID(cls, "onTunBuilderSetSessionName", "(Ljava/lang/String;)Z");
    openVPNClientJniCallbackInterface->tun_builder_add_proxy_bypass = jniEnv->GetMethodID(cls, "onTunBuilderAddProxyBypass", "(Ljava/lang/String;)Z");
    openVPNClientJniCallbackInterface->tun_builder_set_proxy_auto_config_url = jniEnv->GetMethodID(cls, "onTunBuilderSetProxyAutoConfigUrl", "(Ljava/lang/String;)Z");
    openVPNClientJniCallbackInterface->tun_builder_set_proxy_http = jniEnv->GetMethodID(cls, "onTunBuilderSetProxyHttp", "(Ljava/lang/String;I)Z");
    openVPNClientJniCallbackInterface->tun_builder_set_proxy_https = jniEnv->GetMethodID(cls, "onTunBuilderSetProxyHttps", "(Ljava/lang/String;I)Z");
    openVPNClientJniCallbackInterface->tun_builder_add_wins_server = jniEnv->GetMethodID(cls, "onTunBuilderAddWinsServer", "(Ljava/lang/String;)Z");
    openVPNClientJniCallbackInterface->tun_builder_set_allow_family = jniEnv->GetMethodID(cls, "onTunBuilderSetAllowFamily", "(IZ)Z");
    openVPNClientJniCallbackInterface->tun_builder_set_adapter_domain_suffix = jniEnv->GetMethodID(cls, "onTunBuilderSetAdapterDomainSuffix", "(Ljava/lang/String;)Z");
    openVPNClientJniCallbackInterface->tun_builder_establish = jniEnv->GetMethodID(cls, "onTunBuilderEstablish", "()I");
    openVPNClientJniCallbackInterface->tun_builder_persist = jniEnv->GetMethodID(cls, "onTunBuilderPersist", "()Z");
    openVPNClientJniCallbackInterface->tun_builder_establish_lite = jniEnv->GetMethodID(cls, "onTunBuilderEstablishLite", "()V");
    openVPNClientJniCallbackInterface->tun_builder_teardown = jniEnv->GetMethodID(cls, "onTunBuilderTeardown", "(Z)V");

    // Connection callbacks

    openVPNClientJniCallbackInterface->connect_attach = jniEnv->GetMethodID(cls, "onConnectAttach", "()V");
    openVPNClientJniCallbackInterface->connect_pre_run = jniEnv->GetMethodID(cls, "onConnectPreRun", "()V");
    openVPNClientJniCallbackInterface->connect_run = jniEnv->GetMethodID(cls, "onConnectRun", "()V");
    openVPNClientJniCallbackInterface->connect_session_stop = jniEnv->GetMethodID(cls, "onConnectSessionStop", "()V");
}

bool OpenVPNClient::OpenVPN3Client::setJniBooleanField(jclass jniClass, jobject jniObject, const char *fieldName, jboolean value)
{
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr || jniClass == nullptr || jniObject == nullptr)
    {
        if(jniEnv == nullptr)
            logDebug("setJniStringField(): jniEnv is null");

        if(jniClass == nullptr)
            logDebug("setJniStringField(): jniClass is null");

        if(jniObject == nullptr)
            logDebug("setJniStringField(): jniObject is null");

        return false;
    }

    jfieldID fieldId = jniEnv->GetFieldID(jniClass, fieldName, "Z");

    if(fieldId == nullptr)
        return false;

    jniEnv->SetBooleanField(jniObject, fieldId, value);

    return true;
}

void OpenVPNClient::OpenVPN3Client::setJniCallbackInterface(ovpn3_client_callback_jni *callbackClient)
{
    openVPNClientJniCallbackInterface = callbackClient;
}

bool OpenVPNClient::OpenVPN3Client::setJniIntField(jclass jniClass, jobject jniObject, const char *fieldName, int value)
{
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr || jniClass == nullptr || jniObject == nullptr)
    {
        if(jniEnv == nullptr)
            logDebug("setJniStringField(): jniEnv is null");

        if(jniClass == nullptr)
            logDebug("setJniStringField(): jniClass is null");

        if(jniObject == nullptr)
            logDebug("setJniStringField(): jniObject is null");

        return false;
    }

    jfieldID fieldId = jniEnv->GetFieldID(jniClass, fieldName, "I");

    if(fieldId == nullptr)
        return false;

    jniEnv->SetIntField(jniObject, fieldId, value);

    return true;
}

bool OpenVPNClient::OpenVPN3Client::setJniStringField(jclass jniClass, jobject jniObject, const char *fieldName, const std::string &value)
{
    return setJniStringField(jniClass, jniObject, fieldName, value.c_str());
}

bool OpenVPNClient::OpenVPN3Client::setJniStringField(jclass jniClass, jobject jniObject, const char *fieldName, const char *value)
{
    JNIEnv *jniEnv = getThreadJniEnv();
    jstring jValue;

    if(jniEnv == nullptr || jniClass == nullptr || jniObject == nullptr)
    {
        if(jniEnv == nullptr)
            logDebug("setJniStringField(): jniEnv is null");

        if(jniClass == nullptr)
            logDebug("setJniStringField(): jniClass is null");

        if(jniObject == nullptr)
            logDebug("setJniStringField(): jniObject is null");

        return false;
    }

    jfieldID fieldId = jniEnv->GetFieldID(jniClass, fieldName, "Ljava/lang/String;");

    if(fieldId == nullptr)
        return false;

    jValue = jniEnv->NewStringUTF(value);

    jniEnv->SetObjectField(jniObject, fieldId, jValue);

    jniEnv->DeleteLocalRef(jValue);

    return true;
}

bool OpenVPNClient::OpenVPN3Client::setJniObjectField(jclass jniClass, jobject jniObject, const char *fieldName, jobject value)
{
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr || jniClass == nullptr || jniObject == nullptr)
    {
        if(jniEnv == nullptr)
            logDebug("setJniStringField(): jniEnv is null");

        if(jniClass == nullptr)
            logDebug("setJniStringField(): jniClass is null");

        if(jniObject == nullptr)
            logDebug("setJniStringField(): jniObject is null");

        return false;
    }

    jfieldID fieldId = jniEnv->GetFieldID(jniClass, fieldName, "Ljava/lang/Object;");

    if(fieldId == nullptr)
        return false;

    jniEnv->SetObjectField(jniObject, fieldId, value);

    return true;
}

JNIEnv *OpenVPNClient::OpenVPN3Client::getThreadJniEnv()
{
    JNIEnv *currentThreadEnv;
    JavaVMAttachArgs args;

    if(javaVM == nullptr)
    {
        logError("OpenVPNClient::OpenVPN3Client::getThreadJniEnv(): javaVM is null");

        return nullptr;
    }

    args.version = JNI_VERSION_1_6;
    args.name = NULL;
    args.group = NULL;

    javaVM->AttachCurrentThread(&currentThreadEnv, &args);

    return currentThreadEnv;
}

#endif

bool OpenVPNClient::OpenVPN3Client::socket_protect(int socket, std::string remote, bool ipv6)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->socket_protect)
#else
    if(!openVPNClientCallbackInterface.socket_protect)
#endif
    {
        logMessage = "OpenVPNClient::OpenVPN3Client::socket_protect(): socket_protect is not implemented";

        logError(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = Utils::formatString("socket_protect(socket = %d)", socket);

    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
        return false;

    return jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->socket_protect, socket);
#else
    return TRUE_IF_SUCCEEDED(openVPNClientCallbackInterface.socket_protect(socket));
#endif
}

bool OpenVPNClient::OpenVPN3Client::pause_on_connection_timeout()
{
    return false;
}

void OpenVPNClient::OpenVPN3Client::event(const openvpn::ClientAPI::Event &e)
{
    ovpn3_connection_stats connection_stats;
    openvpn::Error::Type openVPNNameError, openVPNInfoError;
    std::string message, info;

#ifdef __JNI
    if(!isJniCallbackValid())
        return;
#endif

    if(e.fatal)
        message = "FATAL ERROR";
    else if(e.error)
        message = "ERROR";
    else
        message = "EVENT";

    message += ": " + e.name;

    if(!e.info.empty())
        message += " - " + e.info;

    if(e.fatal || e.error || findErrorInString(e.name) || findErrorInString(e.info))
        logError(message);
    else
        logDebug(message);

#ifdef __JNI
    if(openVPNClientJniCallbackInterface->on_event)
#else
    if(openVPNClientCallbackInterface.on_event)
#endif
    {
        ovpn3_event ce;

        ZEROMEMORY(&ce, sizeof(ovpn3_event));

        if(e.fatal)
            ce.type = EDDIE_EVENT_TYPE_FATAL_ERROR;
        else if(e.error)
            ce.type = EDDIE_EVENT_TYPE_ERROR;
        else if(isUdpPartialSendError(e.name) || isUdpPartialSendError(e.info))
            ce.type = UDP_PARTIAL_SEND_ERROR;
        else if(findErrorInString(e.name) || findErrorInString(e.info))
            ce.type = EDDIE_EVENT_TYPE_ERROR;
        else
            ce.type = getEventType((char *)e.name.c_str());

        openVPNNameError = getOpenVPNErrorInString(e.name);
        openVPNInfoError = getOpenVPNErrorInString(e.info);

        if(openVPNNameError != openvpn::Error::SUCCESS || openVPNInfoError != openvpn::Error::SUCCESS)
        {
            if(openVPNNameError != openvpn::Error::TLS_AUTH_FAIL &&
               openVPNNameError != openvpn::Error::AUTH_FAILED &&
               openVPNInfoError != openvpn::Error::TLS_AUTH_FAIL &&
               openVPNInfoError != openvpn::Error::AUTH_FAILED)
                ce.type = EDDIE_EVENT_TYPE_FORMAL_WARNING;
            else
                ce.type = openvpn::ClientEvent::AUTH_FAILED;

            info = e.name + " " + e.info;

            ce.info = info.c_str();

            if(openVPNNameError != openvpn::Error::SUCCESS)
                ce.name = openvpn::Error::name(openVPNNameError);
            else
                ce.name = openvpn::Error::name(openVPNInfoError);

            ce.data = nullptr;
        }
        else if(ce.type == openvpn::ClientEvent::Type::CONNECTED)
        {
            connectionInfo = connection_info();

            ZEROMEMORY(&connection_stats, sizeof(ovpn3_connection_stats));

            connection_stats.defined = connectionInfo.defined;
            connection_stats.user = connectionInfo.user.c_str();
            connection_stats.serverHost = connectionInfo.serverHost.c_str();
            connection_stats.serverPort = connectionInfo.serverPort.c_str();
            connection_stats.serverProto = connectionInfo.serverProto.c_str();
            connection_stats.serverIp = connectionInfo.serverIp.c_str();
            connection_stats.vpnIp4 = connectionInfo.vpnIp4.c_str();
            connection_stats.vpnIp6 = connectionInfo.vpnIp6.c_str();
            connection_stats.gw4 = connectionInfo.gw4.c_str();
            connection_stats.gw6 = connectionInfo.gw6.c_str();
            connection_stats.clientIp = connectionInfo.clientIp.c_str();
            connection_stats.tunName = connectionInfo.tunName.c_str();
            connection_stats.topology = connectionInfo.topology.c_str();
            connection_stats.cipher = connectionInfo.cipher.c_str();
            connection_stats.ping = connectionInfo.ping;
            connection_stats.pingRestart = connectionInfo.ping_restart;

            ce.data = &connection_stats;
        }
        else
        {
            ce.name = e.name.c_str();
            ce.info = e.info.c_str();
            ce.data = nullptr;
        }

#ifdef __JNI
        JNIEnv *jniEnv;
        jclass cls = nullptr;
        jmethodID constructorID = nullptr;
        jobject event = nullptr;
        jobject connectionStats = nullptr;

        jniEnv = getThreadJniEnv();

        if(jniEnv == nullptr)
        {
            logDebug("OpenVPNClient::OpenVPN3Client::event(): jniEnv is null");

            return;
        }

        // OpenVPN3ConnectionData

        if(ce.type == openvpn::ClientEvent::Type::CONNECTED)
        {
            jniEnv->ExceptionClear();

            cls = jniEnv->FindClass("org/airvpn/eddie/VPNConnectionStats");

            if(cls == nullptr)
            {
                logDebug("OpenVPNClient::OpenVPN3Client::event(): Can't find VPNConnectionStats class");

                return;
            }

            constructorID = jniEnv->GetMethodID(cls, "<init>","()V");

            connectionStats = jniEnv->NewObject(cls, constructorID);

            if(connectionStats != nullptr)
            {
                if(!setJniIntField(cls, connectionStats, "defined", connection_stats.defined))
                    return;

                if(!setJniStringField(cls, connectionStats, "user", connection_stats.user))
                    return;

                if(!setJniStringField(cls, connectionStats, "serverHost", connection_stats.serverHost))
                    return;

                if(!setJniStringField(cls, connectionStats, "serverPort", connection_stats.serverPort))
                    return;

                if(!setJniStringField(cls, connectionStats, "serverProto", connection_stats.serverProto))
                    return;

                if(!setJniStringField(cls, connectionStats, "serverIp", connection_stats.serverIp))
                    return;

                if(!setJniStringField(cls, connectionStats, "vpnIp4", connection_stats.vpnIp4))
                    return;

                if(!setJniStringField(cls, connectionStats, "vpnIp6", connection_stats.vpnIp6))
                    return;

                if(!setJniStringField(cls, connectionStats, "gw4", connection_stats.gw4))
                    return;

                if(!setJniStringField(cls, connectionStats, "gw6", connection_stats.gw6))
                    return;

                if(!setJniStringField(cls, connectionStats, "clientIp", connection_stats.clientIp))
                    return;

                if(!setJniStringField(cls, connectionStats, "tunName", connection_stats.tunName))
                    return;

                if(!setJniStringField(cls, connectionStats, "topology", connection_stats.topology))
                    return;

                if(!setJniStringField(cls, connectionStats, "cipher", connection_stats.cipher))
                    return;

                if(!setJniIntField(cls, connectionStats, "ping", connection_stats.ping))
                    return;

                if(!setJniIntField(cls, connectionStats, "pingRestart", connection_stats.pingRestart))
                    return;
            }

            jniEnv->DeleteLocalRef(cls);
        }
        else
            connectionStats = nullptr;

        // OpenVPN3Event

        jniEnv->ExceptionClear();

        cls = jniEnv->FindClass("org/airvpn/eddie/VPNEvent");

        if(cls == nullptr)
        {
            if(connectionStats != nullptr)
                jniEnv->DeleteLocalRef(connectionStats);

            logDebug("OpenVPNClient::OpenVPN3Client::event(): Can't find VPNEvent class");

            return;
        }

        constructorID = jniEnv->GetMethodID(cls, "<init>","()V");

        event = jniEnv->NewObject(cls, constructorID);

        if(event == nullptr)
        {
            logDebug("OpenVPNClient::OpenVPN3Client::event(): Can't create a VPNEvent object");

            return;
        }

        if(!setJniIntField(cls, event, "type", ce.type))
            return;

        if(!setJniBooleanField(cls, event, "notifyUser", JNI_FALSE))
            return;

        if(!setJniStringField(cls, event, "name", ce.name))
            return;

        if(!setJniStringField(cls, event, "info", ce.info))
            return;

        if(!setJniObjectField(cls, event, "data", connectionStats))
            return;

        jniEnv->CallVoidMethod(jniCallbackObject, openVPNClientJniCallbackInterface->on_event, event);

        if(connectionStats != nullptr)
            jniEnv->DeleteLocalRef(connectionStats);

        jniEnv->DeleteLocalRef(event);

        jniEnv->DeleteLocalRef(cls);
#else
        openVPNClientCallbackInterface.on_event(&ce);
#endif
    }
}
void OpenVPNClient::OpenVPN3Client::acc_event(const openvpn::ClientAPI::AppCustomControlMessageEvent &acev)
{
    // event ignored
}

bool OpenVPNClient::OpenVPN3Client::findErrorInString(std::string s)
{
    std::transform(s.begin(), s.end(), s.begin(), ::tolower);

    return(s.find("error") != std::string::npos) | (s.find("exception") != std::string::npos);
}

bool OpenVPNClient::OpenVPN3Client::findFatalErrorInString(std::string s)
{
    std::transform(s.begin(), s.end(), s.begin(), ::tolower);

    return(s.find("fatal error") != std::string::npos);
}

bool OpenVPNClient::OpenVPN3Client::isUdpPartialSendError(std::string s)
{
    std::transform(s.begin(), s.end(), s.begin(), ::tolower);

    return(s.find("udp partial send error") != std::string::npos);
}

openvpn::Error::Type OpenVPNClient::OpenVPN3Client::getOpenVPNErrorInString(const std::string &s)
{
    int i;
    openvpn::Error::Type type=openvpn::Error::UNDEF;

    for(i=0; i <= openvpn::Error::Type::N_ERRORS && type == openvpn::Error::SUCCESS; i++)
    {
        if(s.find(openvpn::Error::name((openvpn::Error::Type)i)) != std::string::npos)
            type = (openvpn::Error::Type)i;
    }

    if(s.find("TCP recv EOF") != std::string::npos)
        type = openvpn::Error::NETWORK_EOF_ERROR;

    if(s.find("TCP recv error") != std::string::npos)
        type = openvpn::Error::NETWORK_RECV_ERROR;

    return type;
}

int OpenVPNClient::OpenVPN3Client::getEventType(char *name)
{
    int i, type = EDDIE_EVENT_TYPE_ERROR;

    for(i=0; i <= openvpn::ClientEvent::Type::N_TYPES && type == EDDIE_EVENT_TYPE_ERROR; i++)
    {
        if(strcmp(openvpn::ClientEvent::event_name((openvpn::ClientEvent::Type)i), name) == 0)
            type = i;
    }

    return type;
}

void OpenVPNClient::OpenVPN3Client::external_pki_cert_request(openvpn::ClientAPI::ExternalPKICertRequest &req)
{
    logMessage = "external_pki_cert_request()";

    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);
}

void OpenVPNClient::OpenVPN3Client::external_pki_sign_request(openvpn::ClientAPI::ExternalPKISignRequest &req)
{
    logMessage = "external_pki_sign_request()";

    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);
}

void OpenVPNClient::OpenVPN3Client::connect_attach()
{
    logMessage = "connect_attach()";

    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

    BaseClass::connect_attach();

#ifdef __JNI
    if(!isJniCallbackValid())
        return;
#endif

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
        return;

    if(openVPNClientJniCallbackInterface->connect_attach)
        jniEnv->CallVoidMethod(jniCallbackObject, openVPNClientJniCallbackInterface->connect_attach);
#else
    if(openVPNClientCallbackInterface.connect_attach)
        open4CallbackInterface.connect_attach();
#endif
}

void OpenVPNClient::OpenVPN3Client::connect_pre_run()
{
    logMessage = "connect_pre_run()";

    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

    BaseClass::connect_pre_run();

#ifdef __JNI
    if(!isJniCallbackValid())
        return;
#endif

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
        return;

    if(openVPNClientJniCallbackInterface->connect_pre_run)
        jniEnv->CallVoidMethod(jniCallbackObject, openVPNClientJniCallbackInterface->connect_pre_run);
#else
    if(openVPNClientCallbackInterface.connect_pre_run)
        openVPNClientCallbackInterface.connect_pre_run();
#endif
}

void OpenVPNClient::OpenVPN3Client::connect_run()
{
    logMessage = "connect_run()";

    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

    BaseClass::connect_run();

#ifdef __JNI
    if(!isJniCallbackValid())
        return;
#endif

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
        return;

    if(openVPNClientJniCallbackInterface->connect_run)
        jniEnv->CallVoidMethod(jniCallbackObject, openVPNClientJniCallbackInterface->connect_run);
#else
    if(openVPNClientCallbackInterface.connect_run)
        openVPNClientCallbackInterface.connect_run();
#endif
}

void OpenVPNClient::OpenVPN3Client::connect_session_stop()
{
    logMessage = "connect_session_stop()";

    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

    BaseClass::connect_session_stop();

#ifdef __JNI
    if(!isJniCallbackValid())
        return;
#endif

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
        return;

    if(openVPNClientJniCallbackInterface->connect_session_stop)
        jniEnv->CallVoidMethod(jniCallbackObject, openVPNClientJniCallbackInterface->connect_session_stop);
#else
    if(openVPNClientCallbackInterface.connect_session_stop)
        openVPNClientCallbackInterface.connect_session_stop();
#endif
}

bool OpenVPNClient::OpenVPN3Client::tun_builder_new()
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_new)
#else
    if(!openVPNClientCallbackInterface.tun_builder_new)
#endif
    {
        logMessage = "tun_builder_new() is not implemented";

        logError(logMessage);

        logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = "tun_builder_new()";

    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
        return false;

    return jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_new);
#else
    return TRUE_IF_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_new());
#endif
}

bool OpenVPNClient::OpenVPN3Client::tun_builder_set_layer(int layer)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_set_layer)
#else
    if(!openVPNClientCallbackInterface.tun_builder_set_layer)
#endif
    {
        logMessage = "tun_builder_set_layer() is not implemented";

        logError(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = Utils::formatString("tun_builder_set_layer(layer = %d)", layer);

    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
        return false;

    return jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_set_layer, layer);
#else
    return TRUE_IF_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_set_layer(layer));
#endif
}

bool OpenVPNClient::OpenVPN3Client::tun_builder_set_remote_address(const std::string &address, bool ipv6)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_set_remote_address)
#else
    if(!openVPNClientCallbackInterface.tun_builder_set_remote_address)
#endif
    {
        logMessage = "tun_builder_set_remote_address() is not implemented";

        logError(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = Utils::formatString("tun_builder_set_remote_address(address = %s, ipv6 = %s)", address.c_str(), std::to_string(ipv6).c_str());

    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();
    jstring jAddress;
    bool retval;

    if(jniEnv == nullptr)
        return false;

    jAddress = jniEnv->NewStringUTF(address.c_str());

    retval = jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_set_remote_address,
                jAddress,
                ipv6);

    jniEnv->DeleteLocalRef(jAddress);

    return retval;
#else
    return TRUE_IF_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_set_remote_address(address.c_str(), BOOL_TO_INT(ipv6)));
#endif
}

bool OpenVPNClient::OpenVPN3Client::tun_builder_add_address(const std::string &address, int prefix_length, const std::string &gateway, /*optional*/ bool ipv6, bool net30)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_add_address)
#else
    if(!openVPNClientCallbackInterface.tun_builder_add_address)
#endif
    {
        logMessage = "tun_builder_add_address() is not implemented";

        logError(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = Utils::formatString("tun_builder_add_address(address = %s, prefix_length = %d, gateway = %s, ipv6 = %s, net30 = %s)", address.c_str(), prefix_length, gateway.c_str(), std::to_string(ipv6).c_str(), std::to_string(net30).c_str());

    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();
    jstring jAddress, jGateway;
    bool retval;

    if(jniEnv == nullptr)
        return false;

    jAddress = jniEnv->NewStringUTF(address.c_str());
    jGateway = jniEnv->NewStringUTF(gateway.c_str());

    retval = jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_add_address,
                jAddress,
                prefix_length,
                jGateway,
                ipv6,
                net30);

    jniEnv->DeleteLocalRef(jAddress);
    jniEnv->DeleteLocalRef(jGateway);

    return retval;
#else
    return TRUE_IF_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_add_address(address.c_str(), prefix_length, gateway.c_str(), BOOL_TO_INT(ipv6), BOOL_TO_INT(net30)));
#endif
}

bool OpenVPNClient::OpenVPN3Client::tun_builder_set_route_metric_default(int metric)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_set_route_metric_default)
#else
    if(!openVPNClientCallbackInterface.tun_builder_set_route_metric_default)
#endif
    {
        logMessage = "tun_builder_set_route_metric_default() is not implemented";

        logError(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = Utils::formatString("tun_builder_set_route_metric_default(metric = %d)", metric);

    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
        return false;

    return jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_set_route_metric_default, metric);
#else
    return TRUE_IF_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_set_route_metric_default(metric));
#endif
}

bool OpenVPNClient::OpenVPN3Client::tun_builder_reroute_gw(bool ipv4, bool ipv6, unsigned int flags)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_reroute_gw)
#else
    if(!openVPNClientCallbackInterface.tun_builder_reroute_gw)
#endif
    {
        logMessage = "tun_builder_reroute_gw() is not implemented";

        logError(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = Utils::formatString("tun_builder_reroute_gw(ipv4 = %s, ipv6 = %s, flags = %d)", std::to_string(ipv4).c_str(), std::to_string(ipv6).c_str(), flags);

    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
        return false;

    return jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_reroute_gw, ipv4, ipv6, flags);
#else
    return TRUE_IF_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_reroute_gw(BOOL_TO_INT(ipv4), BOOL_TO_INT(ipv6), flags));
#endif
}

bool OpenVPNClient::OpenVPN3Client::tun_builder_add_route(const std::string &address, int prefix_length, int metric, bool ipv6)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_add_route)
#else
    if(!openVPNClientCallbackInterface.tun_builder_add_route)
#endif
    {
        logMessage = "tun_builder_add_route() is not implemented";

        logError(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = Utils::formatString("tun_builder_add_route(address = %s, prefix_length = %d, metric = %d, ipv6 = %s)", address.c_str(), prefix_length, metric, std::to_string(ipv6).c_str());

    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();
    jstring jAddress;
    bool retval;

    if(jniEnv == nullptr)
        return false;

    jAddress = jniEnv->NewStringUTF(address.c_str());

    retval = jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_add_route,
                jAddress,
                prefix_length,
                metric,
                ipv6);

    jniEnv->DeleteLocalRef(jAddress);

    return retval;
#else
    return TRUE_IF_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_add_route(address.c_str(), prefix_length, metric, BOOL_TO_INT(ipv6)));
#endif
}

bool OpenVPNClient::OpenVPN3Client::tun_builder_exclude_route(const std::string &address, int prefix_length, int metric, bool ipv6)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_exclude_route)
#else
    if(!openVPNClientCallbackInterface.tun_builder_exclude_route)
#endif
    {
        logMessage = "tun_builder_exclude_route() is not implemented";

        logError(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = Utils::formatString("tun_builder_exclude_route(address = %s, prefix_length = %d, metric = %d, ipv6 = %s)", address.c_str(), prefix_length, metric, std::to_string(ipv6).c_str());

    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();
    jstring jAddress;
    bool retval;

    if(jniEnv == nullptr)
        return false;

    jAddress = jniEnv->NewStringUTF(address.c_str());

    retval = jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_exclude_route,
                jAddress,
                prefix_length,
                metric,
                ipv6);

    jniEnv->DeleteLocalRef(jAddress);

    return retval;
#else
    return TRUE_IF_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_exclude_route(address.c_str(), prefix_length, metric, BOOL_TO_INT(ipv6)));
#endif
}

bool OpenVPNClient::OpenVPN3Client::tun_builder_add_dns_server(const std::string &address, bool ipv6)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_add_dns_server)
#else
    if(!openVPNClientCallbackInterface.tun_builder_add_dns_server)
#endif
    {
        logMessage = "tun_builder_add_dns_server() is not implemented";

        logError(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = Utils::formatString("tun_builder_add_dns_server(address = %s, ipv6 = %s)", address.c_str(), std::to_string(ipv6).c_str());

    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();
    jstring jAddress;
    bool retval;

    if(jniEnv == nullptr)
        return false;

    jAddress = jniEnv->NewStringUTF(address.c_str());

    retval = jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_add_dns_server,
                jAddress,
                ipv6);

    jniEnv->DeleteLocalRef(jAddress);

    return retval;
#else
    return TRUE_IF_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_add_dns_server(address.c_str(), BOOL_TO_INT(ipv6)));
#endif
}

bool OpenVPNClient::OpenVPN3Client::tun_builder_add_search_domain(const std::string &domain)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_add_search_domain)
#else
    if(!openVPNClientCallbackInterface.tun_builder_add_search_domain)
#endif
    {
        logMessage = "tun_builder_add_search_domain() is not implemented";

        logError(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = Utils::formatString("tun_builder_add_search_domain(domain = %s)", domain.c_str());

    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();
    jstring jDomain;
    bool retval;

    if(jniEnv == nullptr)
        return false;

    jDomain = jniEnv->NewStringUTF(domain.c_str());

    retval = jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_add_search_domain, jDomain);

    jniEnv->DeleteLocalRef(jDomain);

    return retval;
#else
    return TRUE_IF_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_add_search_domain(domain.c_str()));
#endif
}

bool OpenVPNClient::OpenVPN3Client::tun_builder_set_mtu(int mtu)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_set_mtu)
#else
    if(!openVPNClientCallbackInterface.tun_builder_set_mtu)
#endif
    {
        logMessage = "tun_builder_set_mtu() is not implemented";

        logError(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = Utils::formatString("tun_builder_set_mtu(mtu = %d)", mtu);

    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
        return false;

    return jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_set_mtu, mtu);
#else
    return TRUE_IF_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_set_mtu(mtu));
#endif
}

bool OpenVPNClient::OpenVPN3Client::tun_builder_set_session_name(const std::string &name)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_set_session_name)
#else
    if(!openVPNClientCallbackInterface.tun_builder_set_session_name)
#endif
    {
        logMessage = "tun_builder_set_session_name() is not implemented";

        logError(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = Utils::formatString("tun_builder_set_session_name(name = %s)", name.c_str());

    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();
    jstring jName;
    bool retval;

    if(jniEnv == nullptr)
        return false;

    jName = jniEnv->NewStringUTF(name.c_str());

    retval = jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_set_session_name, jName);

    jniEnv->DeleteLocalRef(jName);

    return retval;
#else
    return TRUE_IF_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_set_session_name(name.c_str()));
#endif
}

bool OpenVPNClient::OpenVPN3Client::tun_builder_add_proxy_bypass(const std::string &bypass_host)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_add_proxy_bypass)
#else
    if(!openVPNClientCallbackInterface.tun_builder_add_proxy_bypass)
#endif
    {
        logMessage = "tun_builder_add_proxy_bypass() is not implemented";

        logError(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = Utils::formatString("tun_builder_add_proxy_bypass(bypass_host = %s)", bypass_host.c_str());

    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();
    jstring jBypassHost;
    bool retval;

    if(jniEnv == nullptr)
        return false;

    jBypassHost = jniEnv->NewStringUTF(bypass_host.c_str());

    retval = jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_add_proxy_bypass, jBypassHost);

    jniEnv->DeleteLocalRef(jBypassHost);

    return retval;
#else
    return TRUE_IF_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_add_proxy_bypass(bypass_host.c_str()));
#endif
}

bool OpenVPNClient::OpenVPN3Client::tun_builder_set_proxy_auto_config_url(const std::string &url)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_set_proxy_auto_config_url)
#else
    if(!openVPNClientCallbackInterface.tun_builder_set_proxy_auto_config_url)
#endif
    {
        logMessage = "tun_builder_set_proxy_auto_config_url() is not implemented";

        logError(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = Utils::formatString("tun_builder_set_proxy_auto_config_url(url = %s)", url.c_str());

    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();
    jstring jUrl;
    bool retval;

    if(jniEnv == nullptr)
        return false;

    jUrl = jniEnv->NewStringUTF(url.c_str());

    retval =  jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_set_proxy_auto_config_url, jUrl);

    jniEnv->DeleteLocalRef(jUrl);

    return retval;

#else
    return TRUE_IF_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_set_proxy_auto_config_url(url.c_str()));
#endif
}

bool OpenVPNClient::OpenVPN3Client::tun_builder_set_proxy_http(const std::string &host, int port)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_set_proxy_http)
#else
    if(!openVPNClientCallbackInterface.tun_builder_set_proxy_http)
#endif
    {
        logMessage = "tun_builder_set_proxy_http() is not implemented";

        logError(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = Utils::formatString("tun_builder_set_proxy_http(host = %s, port = %d)", host.c_str(), port);

    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();
    jstring jHost;
    bool retval;

    if(jniEnv == nullptr)
        return false;

    jHost = jniEnv->NewStringUTF(host.c_str());

    retval = jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_set_proxy_http,
                jHost,
                port);

    jniEnv->DeleteLocalRef(jHost);

    return retval;
#else
    return TRUE_IF_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_set_proxy_http(host.c_str(), port));
#endif
}

bool OpenVPNClient::OpenVPN3Client::tun_builder_set_proxy_https(const std::string &host, int port)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_set_proxy_https)
#else
    if(!openVPNClientCallbackInterface.tun_builder_set_proxy_https)
#endif
    {
        logMessage = "tun_builder_set_proxy_https() is not implemented";

        logError(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = Utils::formatString("tun_builder_set_proxy_https(host = %s, port = %d)", host.c_str(), port);

    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();
    jstring jHost;
    bool retval;

    if(jniEnv == nullptr)
        return false;

    jHost = jniEnv->NewStringUTF(host.c_str());

    retval = jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_set_proxy_https,
                jHost,
                port);

    jniEnv->DeleteLocalRef(jHost);

    return retval;
#else
    return TRUE_IF_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_set_proxy_https(host.c_str(), port));
#endif
}

bool OpenVPNClient::OpenVPN3Client::tun_builder_add_wins_server(const std::string &address)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_add_wins_server)
#else
    if(!openVPNClientCallbackInterface.tun_builder_add_wins_server)
#endif
    {
        logMessage = "tun_builder_add_wins_server() is not implemented";

        logError(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = Utils::formatString("tun_builder_add_wins_server(address = %s)", address.c_str());

    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();
    jstring jAddress;
    bool retval;

    if(jniEnv == nullptr)
        return false;

    jAddress = jniEnv->NewStringUTF(address.c_str());

    retval = jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_add_wins_server, jAddress);

    jniEnv->DeleteLocalRef(jAddress);

    return retval;
#else
    return TRUE_IF_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_add_wins_server(address.c_str()));
#endif
}

bool OpenVPNClient::OpenVPN3Client::tun_builder_set_allow_family(int af, bool allow)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_set_allow_family)
#else
    if(!openVPNClientCallbackInterface.tun_builder_set_allow_family)
#endif
    {
        logMessage = "tun_builder_set_allow_family() is not implemented";

        logError(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = Utils::formatString("tun_builder_set_allow_family(af = %d, allow = %s)", af, std::to_string(allow).c_str());

    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
        return false;

    return jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_set_allow_family, af, allow);
#else
    return TRUE_IF_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_set_allow_family(af, BOOL_TO_INT(block_ipv6)));
#endif
}

bool OpenVPNClient::OpenVPN3Client::tun_builder_set_adapter_domain_suffix(const std::string &name)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_set_adapter_domain_suffix)
#else
    if(!openVPNClientCallbackInterface.tun_builder_set_adapter_domain_suffix)
#endif
    {
        logMessage = "tun_builder_set_adapter_domain_suffix() is not implemented";

        logError(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = Utils::formatString("tun_builder_set_adapter_domain_suffix(name = %s)", name.c_str());

    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();
    jstring jName;
    bool retval;

    if(jniEnv == nullptr)
        return false;

    jName = jniEnv->NewStringUTF(name.c_str());

    retval = jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_set_adapter_domain_suffix, jName);

    jniEnv->DeleteLocalRef(jName);

    return retval;
#else
    return TRUE_IF_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_set_adapter_domain_suffix(name.c_str()));
#endif
}

int OpenVPNClient::OpenVPN3Client::tun_builder_establish()
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return 0;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_establish)
#else
    if(!openVPNClientCallbackInterface.tun_builder_establish)
#endif
    {
        logMessage = "tun_builder_establish() is not implemented";

        logError(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return 0;
    }

    logMessage = "tun_builder_establish()";

    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
        return 0;

    return jniEnv->CallIntMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_establish);
#else
    return openVPNClientCallbackInterface.tun_builder_establish();
#endif
}

bool OpenVPNClient::OpenVPN3Client::tun_builder_persist()
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_persist)
#else
    if(!openVPNClientCallbackInterface.tun_builder_persist)
#endif
    {
        logMessage = "tun_builder_persist() is not implemented";

        logError(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = "tun_builder_persist()";

    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
        return false;

    return jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_persist);
#else
    return TRUE_IF_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_persist());
#endif
}

void OpenVPNClient::OpenVPN3Client::tun_builder_establish_lite()
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_establish_lite)
#else
    if(!openVPNClientCallbackInterface.tun_builder_establish_lite)
#endif
    {
        logMessage = "tun_builder_establish_lite() is not implemented";

        logError(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return;
    }

    logMessage = "tun_builder_establish_lite()";

    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
        return;

    jniEnv->CallVoidMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_establish_lite);
#else
    openVPNClientCallbackInterface.tun_builder_establish_lite();
#endif
}

void OpenVPNClient::OpenVPN3Client::tun_builder_teardown(bool disconnect)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_teardown)
#else
    if(!openVPNClientCallbackInterface.tun_builder_teardown)
#endif
    {
        logMessage = "tun_builder_teardown() is not implemented";

        logError(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return;
    }

    logMessage = Utils::formatString("tun_builder_teardown(disconnect = %s)", std::to_string(disconnect).c_str());

    logInfo(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
        return;

    jniEnv->CallVoidMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_teardown, disconnect);
#else
    openVPNClientCallbackInterface.tun_builder_teardown(BOOL_TO_INT(disconnect));
#endif
}

void OpenVPNClient::OpenVPN3Client::logInfo(const std::string &message)
{
    Utils::logInfo(Utils::formatString("%s: %s", EDDIE_EVENT_NAME_TAG, message.c_str()));
}

void OpenVPNClient::OpenVPN3Client::logDebug(const std::string &message)
{
    Utils::logDebug(Utils::formatString("%s: %s", EDDIE_EVENT_NAME_TAG, message.c_str()));
}

void OpenVPNClient::OpenVPN3Client::logError(const std::string &message)
{
    Utils::logError(Utils::formatString("%s: %s", EDDIE_EVENT_NAME_TAG, message.c_str()));
}

//
// Send a log event to the host. [ProMIND]
//

void OpenVPNClient::OpenVPN3Client::logEvent(int level, const char *name, std::string info)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return;
#endif

#ifdef __JNI
    if(openVPNClientJniCallbackInterface->on_event)
#else
    if(openVPNClientCallbackInterface.on_event)
#endif
    {
        openvpn::Error::Type openVPNNameError, openVPNInfoError;
        ovpn3_event ce;
        std::string evInfo;

        ZEROMEMORY(&ce, sizeof(ovpn3_event));

        ce.type = level;

        Utils::trimString(info);

        openVPNNameError = getOpenVPNErrorInString(name);
        openVPNInfoError = getOpenVPNErrorInString(info);

        if(openVPNNameError != openvpn::Error::SUCCESS || openVPNInfoError != openvpn::Error::SUCCESS)
        {
            ce.type = EDDIE_EVENT_TYPE_FORMAL_WARNING;

            evInfo = name;
            evInfo += " " + info;

            ce.info = evInfo.c_str();

            if(openVPNNameError != openvpn::Error::SUCCESS)
                ce.name = openvpn::Error::name(openVPNNameError);
            else
                ce.name = openvpn::Error::name(openVPNInfoError);

            ce.data = nullptr;
        }
        else if(findFatalErrorInString(name) || findFatalErrorInString(info))
        {
            ce.type = EDDIE_EVENT_TYPE_FATAL_ERROR;
            ce.name = name;
            ce.info = info.c_str();
            ce.data = nullptr;
        }
        else if(findErrorInString(name) || findErrorInString(info))
        {
            ce.type = EDDIE_EVENT_TYPE_ERROR;
            ce.name = name;
            ce.info = info.c_str();
            ce.data = nullptr;
        }
        else
        {
            ce.name = name;
            ce.info = info.c_str();
            ce.data = nullptr;
        }

        if(logFilter(ce.name) && logFilter(info))
        {
#ifdef __JNI
            JNIEnv *jniEnv = nullptr;
            jclass cls = nullptr;
            jmethodID constructorID = nullptr;
            jobject event = nullptr;

            jniEnv = getThreadJniEnv();

            if(jniEnv == nullptr)
                return;

            jniEnv->ExceptionClear();

            cls = jniEnv->FindClass("org/airvpn/eddie/VPNEvent");

            if(cls == nullptr)
            {
                logDebug("OpenVPNClient::OpenVPN3Client::logEvent(): Can't find VPNEvent class");

                return;
            }

            constructorID = jniEnv->GetMethodID(cls, "<init>","()V");

            event = jniEnv->NewObject(cls, constructorID);

            if(event == nullptr)
            {
                logDebug("OpenVPNClient::OpenVPN3Client::logEvent(): Can't create a VPNEvent object");

                return;
            }

            if(!setJniIntField(cls, event, "type", ce.type))
                return;

            if(!setJniBooleanField(cls, event, "notifyUser", JNI_FALSE))
                return;

            if(!setJniStringField(cls, event, "name", ce.name))
                return;

            if(!setJniStringField(cls, event, "info", ce.info))
                return;

            if(!setJniObjectField(cls, event, "data", nullptr))
                return;

            jniEnv->CallVoidMethod(jniCallbackObject, openVPNClientJniCallbackInterface->on_event, event);

            jniEnv->DeleteLocalRef(event);

            jniEnv->DeleteLocalRef(cls);
#else
            openVPNClientCallbackInterface.on_event(&ce);
#endif
        }
    }
}

#ifdef __JNI

bool OpenVPNClient::OpenVPN3Client::isJniCallbackValid()
{
    JNIEnv *jniEnv;
    bool result = false;

    if(openVPNClientJniCallbackInterface != nullptr && jniCallbackObject != nullptr)
    {
        jniEnv = getThreadJniEnv();

        if(jniEnv != nullptr)
        {
            if(jniEnv->GetObjectRefType(jniCallbackObject) == JNIGlobalRefType)
                result = true;
            else
            {
                logError("OpenVPNClient::OpenVPN3Client::isJniCallbackValid() jniCallbackObjectis not a valid object");

                result = false;
            }
        }
        else
        {
            logError("OpenVPNClient::OpenVPN3Client::isJniCallbackValid() Cannot get a valid JNIEnv");

            result = false;
        }
    }
    else
    {
        if(openVPNClientJniCallbackInterface == nullptr)
            logError("OpenVPNClient::OpenVPN3Client::openVPNClientJniCallbackInterface is null");

        if(jniCallbackObject == nullptr)
            logError("OpenVPNClient::OpenVPN3Client::jniCallbackObject is null. Events are turned off.");

        result = false;
    }

    return result;
}

void OpenVPNClient::OpenVPN3Client::releaseJniCallbackObject(JNIEnv *jniEnv)
{
    if(jniEnv == nullptr)
    {
        logError("OpenVPNClient::OpenVPN3Client::releaseJniCallbackObject(): jniEnv is null");
        
        return;
    }

    if(jniCallbackObject != nullptr)
    {
        switch(jniEnv->GetObjectRefType(jniCallbackObject))
        {
            case JNILocalRefType:
            {
                jniEnv->DeleteLocalRef(jniCallbackObject);
            }
            break;

            case JNIGlobalRefType:
            {
                jniEnv->DeleteGlobalRef(jniCallbackObject);
            }
            break;

            case JNIWeakGlobalRefType:
            {
                jniEnv->DeleteWeakGlobalRef(jniCallbackObject);
            }
            break;

            default:
            {
                logError("OpenVPNClient::OpenVPN3Client::releaseJniCallbackObject(): Unknown jniCallbackObject type");
            }
            break;
        }
    }

    jniCallbackObject = nullptr;
}

#endif

//
// Check whether a string can be sent to the log or not. It simply
// evaluates the presence of "forbidden" keys in the string
//

bool OpenVPNClient::OpenVPN3Client::logFilter(char *cs)
{
    std::string s = cs;

    return logFilter(s);
}

bool OpenVPNClient::OpenVPN3Client::logFilter(std::string s)
{
    bool result = true;
    int key_count = 0;
    std::string key[] = {"password", "username"};

    key_count = sizeof(key) / sizeof(key[0]);

    std::transform(s.begin(), s.end(), s.begin(), ::tolower);

    for(int i=0; i < key_count; i++)
    {
        if(s.find(key[i]) != std::string::npos)
            result = false;
    }

    return result;
}

