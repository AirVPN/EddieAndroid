// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>

#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include "include/common.h"
#include "include/api.h"

#include "include/breakpad.h"
#include "include/openvpnclient.hpp"
#include "include/utils.hpp"

static OpenVPNClient *openVPNClient = nullptr;

static bool library_initialized = false;

std::string apiMessage;
std::string openvpnGuiVersion;

EddieLibraryResult libResult;
std::string logMessage;

#ifdef __JNI

jobject jniLibResult = nullptr;
jobject jniTransportStats = nullptr;

#endif

#ifdef __cplusplus
extern "C" {
#endif

const char *name()
{
    const char *c = EDDIE_LIBRARY_NAME;

    return c;
}

#ifdef __JNI

JNIEXPORT jstring JNICALL Java_org_airvpn_eddie_EddieLibrary_name(JNIEnv *env, jobject obj)
{
    return env->NewStringUTF(name());
}

#endif

const char *version()
{
    const char *c = EDDIE_LIBRARY_VERSION;

    return c;
}

#ifdef __JNI

JNIEXPORT jstring JNICALL Java_org_airvpn_eddie_EddieLibrary_version(JNIEnv *env, jobject obj)
{
    return env->NewStringUTF(version());
}

#endif

const char *qualifiedName()
{
    const char *c = EDDIE_LIBRARY_COMPLETE_NAME;

    return c;
}

#ifdef __JNI

JNIEXPORT jstring JNICALL Java_org_airvpn_eddie_EddieLibrary_qualifiedName(JNIEnv *env, jobject obj)
{
    return env->NewStringUTF(qualifiedName());
}

#endif

const char *releaseDate()
{
    const char *c = EDDIE_LIBRARY_RELEASE_DATE;

    return c;
}

#ifdef __JNI

JNIEXPORT jstring JNICALL Java_org_airvpn_eddie_EddieLibrary_releaseDate(JNIEnv *env, jobject obj)
{
    return env->NewStringUTF(releaseDate());
}

#endif

int apiLevel()
{
    return EDDIE_LIBRARY_API_LEVEL;
}

#ifdef __JNI

JNIEXPORT jint JNICALL Java_org_airvpn_eddie_EddieLibrary_apiLevel(JNIEnv *env, jobject obj)
{
    return apiLevel();
}

#endif

const char *architecture()
{
    const char *c = EDDIE_LIBRARY_ARCHITECTURE;

    return c;
}

#ifdef __JNI

JNIEXPORT jstring JNICALL Java_org_airvpn_eddie_EddieLibrary_architecture(JNIEnv *env, jobject obj)
{
    return env->NewStringUTF(architecture());
}

#endif

const char *platform()
{
    const char *c = EDDIE_LIBRARY_PLATFORM;

    return c;
}

#ifdef __JNI

JNIEXPORT jstring JNICALL Java_org_airvpn_eddie_EddieLibrary_platform(JNIEnv *env, jobject obj)
{
    return env->NewStringUTF(platform());
}

#endif

const char *sslLibraryVersion()
{
    apiMessage = OpenVPNClient::sslLibraryVersion();

    return apiMessage.c_str();
}

#ifdef __JNI

JNIEXPORT jstring JNICALL Java_org_airvpn_eddie_EddieLibrary_sslLibraryVersion(JNIEnv *env, jobject obj)
{
    return env->NewStringUTF(sslLibraryVersion());
}

#endif

EddieLibraryResult init(const char *gui_version)
{
    if(library_initialized == true)
    {
        libResult.code = LR_LIBRARY_ALREADY_INITIALIZED;
        libResult.description = "init(): Eddie for Android library is already initialized";

        return libResult;
    }

    try
    {
        Utils::logInfo("Initializing Eddie for Android Library");

        if(breakpad_init() == false)
        {
            libResult.code = LR_BREAKPAD_INITIALIZATION_ERROR;
            libResult.description = "init(): Breakpad initialization failed";

            return libResult;
        }

        if(strcmp(gui_version, "") != 0)
            openvpnGuiVersion = gui_version;

        Utils::logInfo("Library initialization complete");

        Utils::logInfo(Utils::formatString("Eddie library version: %s", version()));

        Utils::logInfo(Utils::formatString("OpenVPN library version: %s", openVPNInfo()));

        Utils::logInfo(Utils::formatString("OpenSSL library version: %s", sslLibraryVersion()));

        Utils::logInfo(Utils::formatString("WireGuard library version: %s", wireGuardVersion()));

        library_initialized = true;

        libResult.code = LR_SUCCESS;
        libResult.description = LIBRARY_RESULT_OK;
    }
    catch(std::exception &e)
    {
        logMessage = Utils::formatString("init() failed: %s", e.what());

        Utils::logError(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
    }
    catch(...)
    {
        logMessage = "init(): Unknown exception";

        Utils::logError(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
    }

    return libResult;
}

#ifdef __JNI

JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_init(JNIEnv *env, jobject obj, jstring guiVersion)
{
    const char *s;

    s = env->GetStringUTFChars(guiVersion, NULL);

    if(setJniLibResult(env, init(s)))
        return jniLibResult;
    else
        return nullptr;
}

#endif

EddieLibraryResult cleanUp()
{
    if(library_initialized == false)
    {
        libResult.code = LR_LIBRARY_NOT_INITIALIZED;
        libResult.description = "cleanUp(): Eddie for Android Library is not initialized";

        return libResult;
    }

    try
    {
        Utils::logInfo("Cleaning up Eddie for Android Library");

        breakpad_cleanup();

        Utils::logInfo("Eddie for Android Library clean up completed");

        library_initialized = false;

        libResult.code = LR_SUCCESS;
        libResult.description = LIBRARY_RESULT_OK;
    }
    catch(std::exception &e)
    {
        logMessage = Utils::formatString("cleanUpOpenVPN() failed: %s", e.what());

        Utils::logError(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
    }
    catch(...)
    {
        logMessage = "cleanUpOpenVPN(): Unknown exception";

        Utils::logError(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
    }

    return libResult;
}

#ifdef __JNI

JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_cleanUpOpenVPN(JNIEnv *env, jobject obj)
{
    jobject retObj = nullptr;

    if(jniTransportStats != nullptr)
        env->DeleteGlobalRef(jniTransportStats);

    if(setJniLibResult(env, cleanUp()))
    {
        memcpy(&retObj, jniLibResult, sizeof(_jobject));

        env->DeleteGlobalRef(jniLibResult);

        jniLibResult = nullptr;

        return retObj;
    }
    else
        return nullptr;
}

#endif

#ifdef __JNI

bool setJniLibResult(JNIEnv *env, EddieLibraryResult result)
{
    jobject obj;
    static jfieldID fcode, fdesc;
    jstring jDescription;

    if(jniLibResult == nullptr)
    {
        env->ExceptionClear();

        jclass cls = env->FindClass("org/airvpn/eddie/EddieLibraryResult");

        if(cls == nullptr)
        {
            Utils::logDebug("setJniLibResult(): Can't find EddieLibraryResult class");

            return false;
        }

        jmethodID constructorID = env->GetMethodID(cls, "<init>","()V");

        if(constructorID == nullptr)
            return false;

        obj = env->NewObject(cls, constructorID);

        if(obj == nullptr)
            return false;

        jniLibResult = env->NewGlobalRef(obj);

        env->DeleteLocalRef(obj);

        fcode = env->GetFieldID(cls, "code", "I");
        fdesc = env->GetFieldID(cls, "description", "Ljava/lang/String;");

        env->DeleteLocalRef(cls);
    }

    if(jniLibResult == nullptr || fcode == nullptr || fdesc == nullptr)
        return false;

    jDescription = env->NewStringUTF(result.description);

    env->SetIntField(jniLibResult, fcode, result.code);
    env->SetObjectField(jniLibResult, fdesc, jDescription);

    env->DeleteLocalRef(jDescription);

    return true;
}

EddieLibraryResult setCallbackObject(JNIEnv *env, jobject callbackObject)
{
    if(openVPNClient == nullptr)
    {
        libResult.code = LR_OPENVPN_POINTER_IS_NULL;
        libResult.description = "CreateOpenVPNClient(): OpenVPN3 client is null";

        return libResult;
    }

    openVPNClient->setJniCallbackObject(callbackObject);

    libResult.code = LR_SUCCESS;
    libResult.description = LIBRARY_RESULT_OK;

    return libResult;
}

JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_setCallback(JNIEnv *env, jobject obj, jobject callbackObject)
{
    if(setJniLibResult(env, setCallbackObject(env, callbackObject)))
        return jniLibResult;
    else
        return nullptr;
}

#endif

const char *openVPNInfo()
{
    apiMessage = OpenVPNClient::openVPNInfo();

    return apiMessage.c_str();
}

#ifdef __JNI

JNIEXPORT jstring JNICALL Java_org_airvpn_eddie_EddieLibrary_openVPNInfo(JNIEnv *env, jobject obj)
{
    return env->NewStringUTF(openVPNInfo());
}

#endif

const char *openVPNCopyright()
{
    apiMessage = OpenVPNClient::openVPNCopyright();

    return apiMessage.c_str();
}

#ifdef __JNI

JNIEXPORT jstring JNICALL Java_org_airvpn_eddie_EddieLibrary_openVPNCopyright(JNIEnv *env, jobject obj)
{
    return env->NewStringUTF(openVPNCopyright());
}

#endif


#ifdef __JNI
EddieLibraryResult createOpenVPNClient(JNIEnv *env, jobject callbackClient)
#else
EddieLibraryResult createOpenVPNClient(ovpn3_client_callback *callbackClient)
#endif
{
    try
    {
        Utils::logInfo("Creating a new OpenVPN client");

#ifndef __JNI
        if(callbackClient == nullptr)
        {
            libResult.code = LR_OPENVPN_POINTER_IS_NULL;
            libResult.description = "createOpenVPNClient(): OpenVPN3 client callback is null";

            return libResult;
        }
#endif

        if(library_initialized == false)
        {
            libResult.code = LR_LIBRARY_NOT_INITIALIZED;
            libResult.description = "createOpenVPNClient(): Eddie for Android Library is not initialized";

            return libResult;
        }

        if(openVPNClient != nullptr)
            disposeOpenVPNClient();

#ifdef __JNI
        openVPNClient = new OpenVPNClient(env, callbackClient);
#else
        openVPNClient = new OpenVPNClient(callbackClient);
#endif

        if(openVPNClient != nullptr)
        {
            openVPNClient->setGuiVersion(openvpnGuiVersion);

            libResult.code = LR_SUCCESS;
            libResult.description = LIBRARY_RESULT_OK;

            openVPNClient->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "Successfully created a new OpenVPN client");
        }
        else
        {
            libResult.code = LR_FAILED_TO_CREATE_OPENVPN_CLIENT;
            libResult.description = "createOpenVPNClient(): Failed to create a new OpenVPN client";
        }
    }
    catch(std::exception &e)
    {
        logMessage = Utils::formatString("createOpenVPNClient() failed: %s", e.what());

        Utils::logError(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
    }
    catch(...)
    {
        logMessage = "createOpenVPNClient(): Unknown exception";

        Utils::logError(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
    }

    return libResult;
}

#ifdef __JNI

JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_createOpenVPNClient(JNIEnv *env, jobject obj, jobject clientCallback)
{
    if(setJniLibResult(env, createOpenVPNClient(env, clientCallback)))
        return jniLibResult;
    else
        return nullptr;
}

#endif

EddieLibraryResult disposeOpenVPNClient()
{
    try
    {
        Utils::logInfo("Disposing OpenVPN client");

        if(library_initialized == false)
        {
            libResult.code = LR_LIBRARY_NOT_INITIALIZED;
            libResult.description = "disposeOpenVPNClient(): Eddie for Android Library is not initialized";

            return libResult;
        }

        if(openVPNClient == nullptr)
        {
            libResult.code = LR_OPENVPN_POINTER_IS_NULL;
            libResult.description = "disposeOpenVPNClient(): OpenVPN3 client is null";

            return libResult;
        }

        openVPNClient->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "Disposing current OpenVPN client");

        delete openVPNClient;

        openVPNClient = nullptr;

        libResult.code = LR_SUCCESS;
        libResult.description = LIBRARY_RESULT_OK;
    }
    catch(std::exception &e)
    {
        logMessage = Utils::formatString("disposeOpenVPNClient() failed: %s", e.what());

        Utils::logError(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
    }
    catch(...)
    {
        logMessage = "disposeOpenVPNClient(): Unknown exception";

        Utils::logError(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
    }

    return libResult;
}

#ifdef __JNI

JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_disposeOpenVPNClient(JNIEnv *env, jobject obj, jobject callbackObject)
{
    libResult = setCallbackObject(env, callbackObject);

     if(libResult.code != LR_SUCCESS)
        return nullptr;

    if(setJniLibResult(env, disposeOpenVPNClient()))
        return jniLibResult;
    else
        return nullptr;
}

#endif

EddieLibraryResult startOpenVPNClient()
{
    try
    {
        if(openVPNClient == nullptr)
        {
            libResult.code = LR_OPENVPN_POINTER_IS_NULL;
            libResult.description = "startOpenVPNClient(): OpenVPN3 client is null";

            return libResult;
        }

        logMessage = "Starting OpenVPN Client";

        Utils::logInfo(logMessage);

        openVPNClient->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

        libResult = openVPNClient->start();
    }
    catch(std::exception &e)
    {
        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, Utils::formatString("StartOpenVPNClient() failed: %s", e.what()));

        logMessage = Utils::formatString("startOpenVPNClient() failed: %s", e.what());

        Utils::logError(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
    }
    catch(...)
    {
        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, "Unknown error in StartOpenVPNClient()");

        logMessage = "startOpenVPNClient(): Unknown exception";

        Utils::logError(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
    }

    return libResult;
}

#ifdef __JNI

JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_startOpenVPNClient(JNIEnv *env, jobject obj, jobject callbackObject)
{
    libResult = setCallbackObject(env, callbackObject);

     if(libResult.code != LR_SUCCESS)
        return nullptr;

    if(setJniLibResult(env, startOpenVPNClient()))
        return jniLibResult;
    else
        return nullptr;
}

#endif

EddieLibraryResult stopOpenVPNClient()
{
    try
    {
        if(openVPNClient == nullptr)
        {
            libResult.code = LR_OPENVPN_POINTER_IS_NULL;
            libResult.description = "stopOpenVPNClient(): OpenVPN3 client is null";

            return libResult;
        }

        logMessage = "Stopping OpenVPN client";

        Utils::logInfo(logMessage);

        openVPNClient->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

        openVPNClient->stop();

        logMessage = "OpenVPN client successfully stopped";

        Utils::logInfo(logMessage);

        openVPNClient->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

        libResult.code = LR_SUCCESS;
        libResult.description = LIBRARY_RESULT_OK;
    }
    catch(std::exception &e)
    {
        logMessage = Utils::formatString("stopOpenVPNClient() failed: %s", e.what());

        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        Utils::logError(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
    }
    catch(...)
    {
        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, "Unknown error in stopOpenVPNClient()");

        logMessage = "stopOpenVPNClient(): Unknown exception";

        Utils::logError(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
    }

    return libResult;
}

#ifdef __JNI

JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_stopOpenVPNClient(JNIEnv *env, jobject obj, jobject callbackObject)
{
    libResult = setCallbackObject(env, callbackObject);

     if(libResult.code != LR_SUCCESS)
        return nullptr;

    if(setJniLibResult(env, stopOpenVPNClient()))
        return jniLibResult;
    else
        return nullptr;
}

#endif

EddieLibraryResult pauseOpenVPNClient(const char *reason)
{
    try
    {
        if(openVPNClient == nullptr)
        {
            libResult.code = LR_OPENVPN_POINTER_IS_NULL;
            libResult.description = "PauseOpenVPNClient(): OpenVPN3 client is null";

            return libResult;
        }

        logMessage = "Pausing OpenVPN client";

        Utils::logInfo(logMessage);

        openVPNClient->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

        openVPNClient->pause(reason != nullptr ? reason : "");

        logMessage = "OpenVPN client successfully paused";

        Utils::logInfo(logMessage);

        openVPNClient->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

        libResult.code = LR_SUCCESS;
        libResult.description = LIBRARY_RESULT_OK;
    }
    catch(std::exception &e)
    {
        logMessage = Utils::formatString("pauseOpenVPNClient() failed: %s", e.what());

        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        Utils::logError(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
    }
    catch(...)
    {
        logMessage = "pauseOpenVPNClient(): Unknown exception";

        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        Utils::logError(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
    }

    return libResult;
}

#ifdef __JNI

JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_pauseOpenVPNClient(JNIEnv *env, jobject obj, jobject callbackObject, jstring reason)
{
    const char *s;

    libResult = setCallbackObject(env, callbackObject);

     if(libResult.code != LR_SUCCESS)
        return nullptr;

    s = env->GetStringUTFChars(reason, NULL);

    if(setJniLibResult(env, pauseOpenVPNClient(s)))
        return jniLibResult;
    else
        return nullptr;
}

#endif

EddieLibraryResult resumeOpenVPNClient()
{
    try
    {
        if(openVPNClient == nullptr)
        {
            libResult.code = LR_OPENVPN_POINTER_IS_NULL;
            libResult.description = "resumeOpenVPNClient(): OpenVPN3 client is null";

            return libResult;
        }

        logMessage = "Resuming OpenVPN client";

        Utils::logInfo(logMessage);

        openVPNClient->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

        openVPNClient->resume();

        logMessage = "OpenVPN client successfully resumed";

        Utils::logInfo(logMessage);

        openVPNClient->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

        libResult.code = LR_SUCCESS;
        libResult.description = LIBRARY_RESULT_OK;
    }
    catch(std::exception &e)
    {
        logMessage = Utils::formatString("resumeOpenVPNClient() failed: %s", e.what());

        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        Utils::logError(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
    }
    catch(...)
    {
        logMessage = "resumeOpenVPNClient(): Unknown exception";

        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        Utils::logError(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
    }

    return libResult;
}

#ifdef __JNI

JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_resumeOpenVPNClient(JNIEnv *env, jobject obj, jobject callbackObject)
{
    libResult = setCallbackObject(env, callbackObject);

     if(libResult.code != LR_SUCCESS)
        return nullptr;

    if(setJniLibResult(env, resumeOpenVPNClient()))
        return jniLibResult;
    else
        return nullptr;
}

#endif

EddieLibraryResult getOpenVPNClientTransportStats(ovpn3_transport_stats *stats)
{
    try
    {
        if(openVPNClient == nullptr)
        {
            libResult.code = LR_OPENVPN_POINTER_IS_NULL;
            libResult.description = "getOpenVPNClientTransportStats(): OpenVPN3 client is null";

            return libResult;
        }

        if(stats == nullptr)
        {
            libResult.code = LR_OPENVPN_TRANSPORT_STATS_POINTER_IS_NULL;
            libResult.description = "getOpenVPNClientTransportStats(): stats is null";

            return libResult;
        }

        openVPNClient->getTransportStats(*stats);

        libResult.code = LR_SUCCESS;
        libResult.description = LIBRARY_RESULT_OK;
    }
    catch(std::exception &e)
    {
        logMessage = Utils::formatString("getOpenVPNClientTransportStats() failed: %s", e.what());

        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        Utils::logError(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
    }
    catch(...)
    {
        logMessage = "getOpenVPNClientTransportStats(): Unknown exception";

        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        Utils::logError(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
    }

    return libResult;
}

#ifdef __JNI

JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_getOpenVPNClientTransportStats(JNIEnv *env, jobject obj)
{
    ovpn3_transport_stats tstats;
    EddieLibraryResult result;
    static jfieldID fcode, fdesc, fbin, fbout, fpin, fpout, flpr;
    jstring jDescription;

    result = getOpenVPNClientTransportStats(&tstats);

     if(result.code != LR_SUCCESS)
        return nullptr;

    env->ExceptionClear();

    if(jniTransportStats == nullptr)
    {
        jclass cls = env->FindClass("org/airvpn/eddie/VPNTransportStats");

        if(cls == nullptr)
        {
            Utils::logDebug("getOpenVPNClientTransportStats(): Can't find VPNTransportStats class");

            return nullptr;
        }

        jmethodID constructorID = env->GetMethodID(cls, "<init>","()V");

        if(constructorID == nullptr)
        {
            env->DeleteLocalRef(cls);

            return nullptr;
        }

        jobject statObject = env->NewObject(cls, constructorID);

        if(statObject == nullptr)
        {
            env->DeleteLocalRef(cls);

            return nullptr;
        }

        jniTransportStats = env->NewGlobalRef(statObject);

        env->DeleteLocalRef(statObject);

        fcode = env->GetFieldID(cls, "resultCode", "I");
        fdesc = env->GetFieldID(cls, "resultDescription", "Ljava/lang/String;");
        fbin = env->GetFieldID(cls, "bytesIn", "J");
        fbout = env->GetFieldID(cls, "bytesOut", "J");
        fpin = env->GetFieldID(cls, "packetsIn", "J");
        fpout = env->GetFieldID(cls, "packetsOut", "J");
        flpr = env->GetFieldID(cls, "lastPacketReceived", "I");

        if(fcode == nullptr ||
           fdesc == nullptr ||
           fbin == nullptr ||
           fbout == nullptr ||
           fpin == nullptr ||
           fpout == nullptr ||
           flpr == nullptr)
            return nullptr;

        env->DeleteLocalRef(cls);
    }

    jDescription = env->NewStringUTF(result.description);

    env->SetIntField(jniTransportStats, fcode, result.code);
    env->SetObjectField(jniTransportStats, fdesc, jDescription);
    env->SetLongField(jniTransportStats, fbin, tstats.bytes_in);
    env->SetLongField(jniTransportStats, fbout, tstats.bytes_out);
    env->SetLongField(jniTransportStats, fpin, tstats.packets_in);
    env->SetLongField(jniTransportStats, fpout, tstats.packets_out);
    env->SetIntField(jniTransportStats, flpr, tstats.last_packet_received);

    env->DeleteLocalRef(jDescription);

    return jniTransportStats;
}

#endif

EddieLibraryResult loadProfileToOpenVPNClient(const char *filename)
{
    try
    {
        if(filename == nullptr)
        {
            libResult.code = LR_PROFILE_FILENAME_IS_NULL;
            libResult.description = "loadProfileToOpenVPNClient(): filename is null";

            return libResult;
        }

        Utils::logInfo(Utils::formatString("Loading OpenVPN profile '%s'", filename));

        if(openVPNClient == nullptr)
        {
            libResult.code = LR_OPENVPN_POINTER_IS_NULL;
            libResult.description = "loadProfileToOpenVPNClient(): OpenVPN3 client is null";

            return libResult;
        }

        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, Utils::formatString("Load '%s' profile to OpenVPN client", filename));

        libResult = openVPNClient->loadProfileFile(filename);
    }
    catch(std::exception &e)
    {
        logMessage = Utils::formatString("loadProfileToOpenVPNClient() failed: %s", e.what());

        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        Utils::logError(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
    }
    catch(...)
    {
        logMessage = "loadProfileToOpenVPNClient(): Unknown exception";

        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        Utils::logError(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
    }

    return libResult;
}

#ifdef __JNI

JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_loadProfileToOpenVPNClient(JNIEnv *env, jobject obj, jobject callbackObject, jstring filename)
{
    const char *s;

    libResult = setCallbackObject(env, callbackObject);

     if(libResult.code != LR_SUCCESS)
        return nullptr;

    s = env->GetStringUTFChars(filename, NULL);

    if(setJniLibResult(env, loadProfileToOpenVPNClient(s)))
        return jniLibResult;
    else
        return nullptr;
}

#endif

EddieLibraryResult loadStringProfileToOpenVPNClient(const char *str)
{
    try
    {
        if(str == nullptr)
        {
            libResult.code = LR_PROFILE_STRING_IS_NULL;
            libResult.description = "LoadStringProfileToOpenVPNClient(): string is null";

            return libResult;
        }

        if(openVPNClient == nullptr)
        {
            libResult.code = LR_OPENVPN_POINTER_IS_NULL;
            libResult.description = "LoadProfileToOpenVPNClient(): OpenVPN3 client is null";

            return libResult;
        }

        Utils::logInfo("Setting OpenVPN custom profile");

        libResult = openVPNClient->loadProfileString(str);
    }
    catch(std::exception &e)
    {
        logMessage = Utils::formatString("loadStringProfileToOpenVPNClient() failed: %s", e.what());

        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        Utils::logError(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
    }
    catch(...)
    {
        logMessage = "loadStringProfileToOpenVPNClient(): Unknown exception";

        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        Utils::logError(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
    }

    return libResult;
}

#ifdef __JNI

JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_loadStringProfileToOpenVPNClient(JNIEnv *env, jobject obj, jobject callbackObject, jstring profile)
{
    const char *s;

    libResult = setCallbackObject(env, callbackObject);

     if(libResult.code != LR_SUCCESS)
        return nullptr;

    s = env->GetStringUTFChars(profile, NULL);

    if(setJniLibResult(env, loadStringProfileToOpenVPNClient(s)))
        return jniLibResult;
    else
        return nullptr;
}

#endif

EddieLibraryResult setOpenVPNClientOption(const char *option, const char *value)
{
    try
    {
        if(option == nullptr)
        {
            libResult.code = LR_OPENVPN_OPTION_NAME_IS_NULL;
            libResult.description = "setOpenVPNClientOption(): Option name is null";

            return libResult;
        }

        if(value == nullptr)
        {
            libResult.code = LR_OPENVPN_OPTION_VALUE_IS_NULL;
            libResult.description = "setOpenVPNClientOption(): Option value is null";

            return libResult;
        }

        Utils::logInfo(Utils::formatString("Setting OpenVPN option '%s' to '%s'", option, value));

        if(openVPNClient == nullptr)
        {
            libResult.code = LR_OPENVPN_POINTER_IS_NULL;
            libResult.description = "setOpenVPNClientOption(): OpenVPN3 client is null";

            return libResult;
        }

        openVPNClient->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, Utils::formatString("OpenVPN client: option '%s' set to '%s'", option, value));

        libResult = openVPNClient->setOption(option, value);
    }
    catch(std::exception &e)
    {
        logMessage = Utils::formatString("setOpenVPNClientOption() failed: %s", e.what());

        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        Utils::logError(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
    }
    catch(...)
    {
        logMessage = "setOpenVPNClientOption(): Unknown exception";

        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        Utils::logError(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
    }

    return libResult;
}

#ifdef __JNI

JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_setOpenVPNClientOption(JNIEnv *env, jobject obj, jobject callbackObject, jstring option, jstring value)
{
    const char *opt, *val;

    libResult = setCallbackObject(env, callbackObject);

     if(libResult.code != LR_SUCCESS)
        return nullptr;

    opt = env->GetStringUTFChars(option, NULL);
    val = env->GetStringUTFChars(value, NULL);

    if(setJniLibResult(env, setOpenVPNClientOption(opt, val)))
        return jniLibResult;
    else
        return nullptr;
}

#endif

char *wireGuardVersion()
{
    return wgVersion();
}

#ifdef __JNI

JNIEXPORT jstring JNICALL Java_org_airvpn_eddie_EddieLibrary_wireGuardVersion(JNIEnv *env, jobject obj)
{
    char *version;
    jstring jVersion;

    version = wireGuardVersion();

    if(version != NULL)
        jVersion = env->NewStringUTF(version);
    else
        jVersion = env->NewStringUTF("");

    free(version);

    return jVersion;
}

#endif

char *wireGuardGetConfig(int handle)
{
    return wgGetConfig(handle);
}

#ifdef __JNI

JNIEXPORT jstring JNICALL Java_org_airvpn_eddie_EddieLibrary_wireGuardGetConfig(JNIEnv *env, jobject obj, jint handle)
{
    char *config;
    jstring jConfig;
    
    config = wireGuardGetConfig(handle);

    if(config != NULL)
        jConfig = env->NewStringUTF(config);
    else
        jConfig = env->NewStringUTF("");

    free(config);

    return jConfig;
}

#endif

int wireGuardTurnOn(const char *interface_name, int tun_fd, const char *settings)
{
    struct go_string gs_ifname, gs_settings;

    gs_ifname.str = interface_name;
    gs_ifname.n = (long)strlen(interface_name);

    gs_settings.str = settings;
    gs_settings.n = (long)strlen(settings);

    return wgTurnOn(gs_ifname, tun_fd, gs_settings);
}

#ifdef __JNI

JNIEXPORT jint JNICALL Java_org_airvpn_eddie_EddieLibrary_wireGuardTurnOn(JNIEnv *env, jobject obj, jstring interface_name, jint tun_fd, jstring settings)
{
    jint retval;

    const char *ifname_str = env->GetStringUTFChars(interface_name, 0);
    const char *settings_str = env->GetStringUTFChars(settings, 0);

    retval = wireGuardTurnOn(ifname_str, tun_fd, settings_str);

    env->ReleaseStringUTFChars(interface_name, ifname_str);
    env->ReleaseStringUTFChars(settings, settings_str);

    return retval;
}

#endif

void wireGuardTurnOff(int handle)
{
    wgTurnOff(handle);
}

#ifdef __JNI

JNIEXPORT void JNICALL Java_org_airvpn_eddie_EddieLibrary_wireGuardTurnOff(JNIEnv *env, jobject obj, jint handle)
{
    wireGuardTurnOff(handle);
}

#endif

int wireGuardGetSocketV4(int handle)
{
    return wgGetSocketV4(handle);
}

#ifdef __JNI

JNIEXPORT jint JNICALL JNICALL Java_org_airvpn_eddie_EddieLibrary_wireGuardGetSocketV4(JNIEnv *env, jobject obj, jint handle)
{
    return wireGuardGetSocketV4(handle);
}

#endif

int wireGuardGetSocketV6(int handle)
{
    return wgGetSocketV6(handle);
}

#ifdef __JNI

JNIEXPORT jint JNICALL JNICALL Java_org_airvpn_eddie_EddieLibrary_wireGuardGetSocketV6(JNIEnv *env, jobject obj, jint handle)
{
    return wireGuardGetSocketV6(handle);
}

#endif

unsigned long long getAvailableMemory()
{
    return sysconf(_SC_AVPHYS_PAGES) * sysconf(_SC_PAGE_SIZE);
}

#ifdef __cplusplus
}
#endif
