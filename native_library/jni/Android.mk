include breakpad/android/google_breakpad/Android.mk 
include lzo/Android.mk
include lz4/Android.mk

ifeq ($(SSL_LIBRARY), mbedTLS)
    include mbedtls/Android.mk
else ifeq ($(SSL_LIBRARY), openSSL)
    include openssl/Android-ssl.mk
    include openssl/Android-crypto.mk
endif

include openvpn3-airvpn/Android.mk
include wg/Android-wg.mk
include eddie/Android.mk
