LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE:= libssl_static
LOCAL_SRC_FILES := lib/arm/libssl.a

ifeq ($(TARGET_ARCH),arm)
    LOCAL_SRC_FILES := lib/arm/libssl.a
else ifeq ($(TARGET_ARCH),arm64)
    LOCAL_SRC_FILES := lib/arm64/libssl.a
else ifeq ($(TARGET_ARCH),x86)
    LOCAL_SRC_FILES := lib/x86/libssl.a
else ifeq ($(TARGET_ARCH),x86_64)
    LOCAL_SRC_FILES := lib/x86_64/libssl.a
endif

include $(PREBUILT_STATIC_LIBRARY)
