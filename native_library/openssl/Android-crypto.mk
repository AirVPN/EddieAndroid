LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE:= libcrypto_static
LOCAL_SRC_FILES := lib/arm/libcrypto.a

ifeq ($(TARGET_ARCH),arm)
    LOCAL_SRC_FILES := lib/arm/libcrypto.a
else ifeq ($(TARGET_ARCH),arm64)
    LOCAL_SRC_FILES := lib/arm64/libcrypto.a
else ifeq ($(TARGET_ARCH),x86)
    LOCAL_SRC_FILES := lib/x86/libcrypto.a
else ifeq ($(TARGET_ARCH),x86_64)
    LOCAL_SRC_FILES := lib/x86_64/libcrypto.a
endif

include $(PREBUILT_STATIC_LIBRARY)
