LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

common_SRC_FILES :=  \
	lib/lz4.c \
	lib/lz4frame.c

common_C_INCLUDES += $(LOCAL_PATH)/lib

# static library
# =====================================================

LOCAL_SRC_FILES := $(common_SRC_FILES)
LOCAL_C_INCLUDES := $(common_C_INCLUDES)
LOCAL_MODULE := liblz4_static
LOCAL_PRELINK_MODULE := false
LOCAL_MODULE_TAGS := optional

include $(BUILD_STATIC_LIBRARY)
