#!/bin/sh

SSL_LIB="openSSL"

# make eddie library and dependencies [ProMIND]

PLATFORM=`uname`
NDK_BUILD=ndk-build
NDK_BUILD_OPTIONS="V=0"
NDK_PATH=""
LIB_PATH="../app/src/main/jniLibs"
SSL_LIB_DESC=""

#
# Edit paths according to your system and configuration
#

export NDK_PROJECT_PATH=.
LINUX_NDK_PATH=/opt/android-sdk-linux/ndk-bundle/
DARWIN_NDK_PATH=~/Library/Developer/Xamarin/android-sdk-macosx/ndk-bundle/

############################################################################

case $SSL_LIB in
    mbedTLS)
        SSL_LIB_DESC=$(grep -r "MBEDTLS_VERSION_STRING_FULL" mbedtls/include/mbedtls//version.h | cut -d"\""  -f2)
        break;
        ;;
    
    openSSL)
        SSL_LIB_DESC=$(grep -r "OPENSSL_VERSION_TEXT" openssl/include/openssl/opensslv.h | cut -d"\""  -f2)
        break;
        ;;

    *)
        echo "Unsupported SSL Library ${SSL_LIB}"
        break
        exit
        ;;
esac

echo "System is $PLATFORM"

if [[ -z "$NDK_PATH" ]]; then
    case $PLATFORM in
        Linux)
            NDK_PATH=$LINUX_NDK_PATH
            break
            ;;
        Darwin)
            NDK_PATH=$DARWIN_NDK_PATH
            break
            ;;
        *)
            echo "Unknown platform ${PLATFORM}"
            break
            exit
            ;;
    esac

    echo "NDK path: ${NDK_PATH}"
else
    echo "${NDK_BUILD} is in system path"
    NDK_PATH=
fi

echo "Project path: ${NDK_PROJECT_PATH}"

if [[ $1 == "clean" ]]; then
    echo
    echo "Cleaning project"
    echo

    ${NDK_PATH}${NDK_BUILD} clean

    rm -rf obj

    echo
    echo "Done"
    
    exit
fi

echo
echo "Making eddie native libraries"
echo "Output path: ${LIB_PATH}"
echo "SSL Library: ${SSL_LIB_DESC}"
echo

"${NDK_PATH}${NDK_BUILD}" ${NDK_BUILD_OPTIONS} NDK_LIBS_OUT=${LIB_PATH} NDK_APPLICATION_MK=jni/Application.mk SSL_LIBRARY=${SSL_LIB} SSL_LIBRARY_DESC="${SSL_LIB_DESC}"

echo
echo "Done"
echo "Libraries have been written in $LIB_PATH"
